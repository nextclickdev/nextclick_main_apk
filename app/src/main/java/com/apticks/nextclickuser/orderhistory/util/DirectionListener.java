package com.apticks.nextclickuser.orderhistory.util;

import com.apticks.nextclickuser.orderhistory.model.DirectionObject;

public interface DirectionListener {
    void onDirectionResult(DirectionObject result);
}