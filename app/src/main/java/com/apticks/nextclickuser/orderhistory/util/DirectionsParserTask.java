package com.apticks.nextclickuser.orderhistory.util;


import android.graphics.Color;
import android.os.AsyncTask;

import com.apticks.nextclickuser.orderhistory.model.DirectionObject;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DirectionsParserTask extends
        AsyncTask<String,Integer, DirectionObject> {

    DirectionListener listener;
    public DirectionsParserTask(DirectionListener listener) {
        this.listener=listener;
    }

    // Parsing the data in non-ui thread
    @Override
    protected DirectionObject doInBackground(String... jsonData) {

        JSONObject jObject;
        DirectionObject directionObject=null;

        try {
            jObject = new JSONObject(jsonData[0]);
            DirectionParser parser = new DirectionParser(true);
            // Starts parsing data
            directionObject = parser.parse(jObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return directionObject;
    }

    // Executes in UI thread, after the parsing process
    @Override
    protected void onPostExecute(DirectionObject result) {

        if (result == null)
            return;

        if(result.getRoutes()!=null) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            for (int i = 0; i < result.getRoutes().size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.getRoutes().get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.BLACK);
            }
            if(lineOptions != null) {
                result.setLineOptions(lineOptions);
            }
        }

        if(listener!=null) {
            listener.onDirectionResult(result);
        }
    }
}
