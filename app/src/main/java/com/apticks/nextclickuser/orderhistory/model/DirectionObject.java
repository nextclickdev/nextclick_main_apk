package com.apticks.nextclickuser.orderhistory.model;

import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class DirectionObject {

    private String travelDistance,duration;
    List<List<HashMap<String, String>>> routes;
    private PolylineOptions lineOptions;

    public void setTravelDistance(String distance) {
        this.travelDistance=distance;
    }
    public String getTravelDistance() {
        return  travelDistance;
    }
    public void setDuration(String duration) {
        this.duration=duration;
    }
    public String getDuration() {
        return  duration;
    }
    public void setRoutes(List<List<HashMap<String, String>>>  routes) {
        this.routes=routes;
    }
    public List<List<HashMap<String, String>>> getRoutes() {
        return routes;
    }

    public void setLineOptions(PolylineOptions lineOptions) {
        this.lineOptions=lineOptions;
    }
    public PolylineOptions getLineOptions()
    {
        return lineOptions;
    }


    Integer durationInSeconds;
    public void setDurationInSeconds(Integer durationInSeconds) {
        this.durationInSeconds=durationInSeconds;
    }
    public Integer getDurationInSeconds() {
        return  durationInSeconds;
    }
}
