package com.apticks.nextclickuser.orderhistory.model;
public class OrderStatus {
    String id,delivery_mode_id,status,serial_number;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDelivery_mode_id() {
        return delivery_mode_id;
    }

    public void setDelivery_mode_id(String delivery_mode_id) {
        this.delivery_mode_id = delivery_mode_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }
}