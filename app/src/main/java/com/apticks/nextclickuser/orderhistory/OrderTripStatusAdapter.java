package com.apticks.nextclickuser.orderhistory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.orderhistory.util.OrderTripStatus;

import java.util.List;

public class OrderTripStatusAdapter extends RecyclerView.Adapter<OrderTripStatusAdapter.ViewHolder> {

    private Context context;
    private List<OrderTripStatus> list;

    public OrderTripStatusAdapter(Context context, List<OrderTripStatus> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public OrderTripStatusAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.view_order_trip_status, parent, false);
        return new OrderTripStatusAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderTripStatusAdapter.ViewHolder holder, int position) {
        OrderTripStatus vendorOrderStatus = list.get(position);

        holder.tv_vendor_order_status_message.setText(vendorOrderStatus.getorderStatus());
        holder.img_vendor_order_status.setImageResource(vendorOrderStatus.getorderStatusResource());
       // holder.vendor_order_status_layout.setBackgroundColor(vendorOrderStatus.getorderStatusBackground());

        holder.view_divider.setBackgroundColor(vendorOrderStatus.getorderStatusBackground());

        holder.tv_vendor_order_status_message.setTextColor(vendorOrderStatus.getorderStatusForeground());
        holder.tv_vendor_order_finished.setTextColor(vendorOrderStatus.getorderStatusForeground());
        holder.tv_vendor_order_finished.setText(vendorOrderStatus.getTime());
        if(position==list.size()-1 || !vendorOrderStatus.getDividerStatus())
        {
            holder.view_divider.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_vendor_order_status_message,tv_vendor_order_finished;
        public ImageView img_vendor_order_status;
        LinearLayout vendor_order_status_layout;
        View view_divider;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_vendor_order_status_message = itemView.findViewById(R.id.tv_vendor_order_status_message);
            img_vendor_order_status = itemView.findViewById(R.id.img_vendor_order_status);
            vendor_order_status_layout = itemView.findViewById(R.id.vendor_order_status_layout);

            tv_vendor_order_finished= itemView.findViewById(R.id.tv_vendor_order_finished);
            view_divider = itemView.findViewById(R.id.view_divider);
        }
    }
}
