package com.apticks.nextclickuser.orderhistory.model;

import java.io.Serializable;

public class UserLocationDetails implements Serializable {

    String id,vendor_user_id,name,unique_id,location_id;
    Double latitude,longitude;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getvendor_user_id() {
        return vendor_user_id;
    }

    public void setvendor_user_id(String vendor_user_id) {
        this.vendor_user_id = vendor_user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
