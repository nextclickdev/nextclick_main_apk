package com.apticks.nextclickuser.orderhistory;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Config.Config;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.LocationHelper.GpsTracker;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.map.MapParser;
import com.apticks.nextclickuser.checkout.map.MapRouteNavigationHelper;
import com.apticks.nextclickuser.orderhistory.model.AssignedUserAddress;
import com.apticks.nextclickuser.orderhistory.model.CustomerModel;
import com.apticks.nextclickuser.orderhistory.model.DeleveryModesmodel;
import com.apticks.nextclickuser.orderhistory.model.DeliveryBoyObject;
import com.apticks.nextclickuser.orderhistory.model.DeliveryJobObject;
import com.apticks.nextclickuser.orderhistory.model.DirectionObject;
import com.apticks.nextclickuser.orderhistory.model.ImagesModel;
import com.apticks.nextclickuser.orderhistory.model.ItemModel;
import com.apticks.nextclickuser.orderhistory.model.LocationObject;
import com.apticks.nextclickuser.orderhistory.model.OrderDetailsmodel;
import com.apticks.nextclickuser.orderhistory.model.OrderStatus;
import com.apticks.nextclickuser.orderhistory.model.PaymentModel;
import com.apticks.nextclickuser.orderhistory.model.ProductDetailsModel;
import com.apticks.nextclickuser.orderhistory.model.SectionItemModel;
import com.apticks.nextclickuser.orderhistory.model.VarientModel;
import com.apticks.nextclickuser.orderhistory.util.DirectionListener;
import com.apticks.nextclickuser.orderhistory.util.LocationDetailUtil;
import com.apticks.nextclickuser.orderhistory.util.OrderTripStatus;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.apticks.nextclickuser.utils.MyUtil;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.apticks.nextclickuser.Config.Config.ORDERDETAILS;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class OrderDetailHistroy extends AppCompatActivity implements OnMapReadyCallback, MapParser {


    private Context mContext;
    private PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;
    private GpsTracker gpsTracker;
    String OrderStatus = "6";//ideally should come from db
    String orderId, updatetime = "";
    Boolean isSelfPickup;
    String delivery_partner_user_id = "", job_id;
    Boolean isDeliveryBoyAssigned = false;
    Timer repeatLocationTask, repeatorderDetailsTask;
    private OrderDetailsmodel orderDetailsmodel;
    TextView seller_details, address_details, cust_address_details, tv_order_id, tv_order_earnings,
            tv_delivery_distance, tv_pick_up_distance, tv_preparation_time, order_receive_otp,
            tv_vendor_name, tv_vendor_address, tv_delivery_boy_name, tv_delivery_boy_rating;
    LinearLayout order_receive_otp_layout, lay_pickup_address, layout_deleveryboy, order_layout;
    ImageView img_product, img_deleveryboy, img_call, img_vendor_call, img_delivery_boy;
    private GoogleMap mMap;
    private boolean isTrackingInitiated;
    LinearLayout map_layout;
    int preptime = 0, deliverytime = 20;
    private Double shoplatitude, shoplongitude;

    Polyline mPolyline;
    MapRouteNavigationHelper mapRouteNavigationHelper;
    Marker prevMarker = null, homeMarker;
    AssignedUserAddress shippingAddr, vendorAddress;

    RecyclerView recycler_view_trip_status;
    private TextView tv_rejected, tv_oder_id, tv_trip_header;
    private String message, vendorphoneenumber = "", deleveryphonenumber = "";
    private ArrayList<OrderTripStatus> vendorOrderStatusList;
    private RecyclerView recycle_orderitems;
    ArrayList<ProductDetailsModel> productlist;
    RelativeLayout layout_discount;
    OrderItemsAdapter adapter;
    private TextView tv_sub_total, tv_item_grand_total_total, tv_discount, tv_tax,
            tv_preparationtime, tv_deliveryfee, tv_on_time;
    private boolean isRatingInitiaised;
    private int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    LinearLayout layout_pickup_image, layout_delivery_image, layout_track_shipping_address;
    ImageView img_pick_up, img_delivery;
    private String delivery_job_id;
    private int deliveryType = 1;
    private boolean canShowMapView;
    private Integer OrderPrepartionTime;

    ProgressBar pBar;
    RelativeLayout layout_order_delivery_time;
    private CountDownTimer prepTimeTimer;
    private int previousStatus;
    private int counter;
    private String mapType;//shopAddress,shopToCustomer,deliveryBoyToCustomer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail_histroy);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        init();
    }

    private void init() {
        mContext = OrderDetailHistroy.this;
        preferenceManager = new PreferenceManager(mContext);
        mCustomDialog = new CustomDialog(mContext);


        tv_oder_id = findViewById(R.id.tv_oder_id);
        orderId = getIntent().getStringExtra("order_id");
        if (getIntent().hasExtra("order_track_id")) {
            String order_track_id = getIntent().getStringExtra("order_track_id");
            tv_oder_id.setText("ORDER #" + order_track_id);
        } else
            tv_oder_id.setText("");
        order_layout = findViewById(R.id.order_layout);
        vendorOrderStatusList = new ArrayList<>();
        recycler_view_trip_status = findViewById(R.id.recycler_view_trip_status);
        tv_rejected = findViewById(R.id.tv_rejected);
        tv_trip_header = findViewById(R.id.tv_trip_header);
        map_layout = findViewById(R.id.map_layout);
        tv_vendor_name = findViewById(R.id.tv_vendor_name);
        tv_vendor_address = findViewById(R.id.tv_vendor_address);
        tv_delivery_boy_name = findViewById(R.id.tv_delivery_boy_name);
        img_delivery_boy = findViewById(R.id.img_delivery_boy);
        tv_delivery_boy_rating = findViewById(R.id.tv_delivery_boy_rating);
        img_product = findViewById(R.id.img_product);
        img_deleveryboy = findViewById(R.id.img_deleveryboy);
        recycle_orderitems = findViewById(R.id.recycle_orderitems);
        img_call = findViewById(R.id.img_call);
        img_vendor_call = findViewById(R.id.img_vendor_call);
        layout_deleveryboy = findViewById(R.id.layout_deleveryboy);
        tv_sub_total = findViewById(R.id.tv_sub_total);
        tv_tax = findViewById(R.id.tv_tax);
        tv_deliveryfee = findViewById(R.id.tv_deliveryfee);
        tv_item_grand_total_total = findViewById(R.id.tv_item_grand_total_total);
        tv_discount = findViewById(R.id.tv_discount);
        layout_discount = findViewById(R.id.layout_discount);
        layout_track_shipping_address = findViewById(R.id.layout_track_shipping_address);
        layout_pickup_image = findViewById(R.id.layout_pickup_image);
        layout_delivery_image = findViewById(R.id.layout_delivery_image);
        img_pick_up = findViewById(R.id.img_pick_up);
        img_delivery = findViewById(R.id.img_delivery);

        layout_order_delivery_time = findViewById(R.id.layout_order_delivery_time);
        pBar = findViewById(R.id.pBar);
        tv_on_time = findViewById(R.id.tv_on_time);
        seller_details = findViewById(R.id.seller_details);
        address_details = findViewById(R.id.address_details);
        //cust_address_details= findViewById(R.id.cust_address_details);

        tv_preparation_time = findViewById(R.id.tv_preparation_time);
        tv_pick_up_distance = findViewById(R.id.tv_pick_up_distance);
        // tv_delivery_distance= findViewById(R.id.tv_delivery_distance);
        order_receive_otp = findViewById(R.id.order_receive_otp);
        order_receive_otp_layout = findViewById(R.id.order_receive_otp_layout);
        productlist = new ArrayList<>();

        recycle_orderitems.setLayoutManager(new GridLayoutManager(mContext, 1));
        adapter = new OrderItemsAdapter(mContext, productlist);
        recycle_orderitems.setAdapter(adapter);
        recycle_orderitems.setHasFixedSize(true);
        //  recycle_orderitems.setNestedScrollingEnabled(false);
        ImageView back_image = findViewById(R.id.back_image);

        img_pick_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageDialog(img_pick_up);
            }
        });
        img_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageDialog(img_delivery);
            }
        });
        layout_track_shipping_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoshop();
            }
        });
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},
                    101);
        }

        img_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("aaaaaaaaa vendorphoneenumber  " + vendorphoneenumber);
                if (!deleveryphonenumber.isEmpty() || !deleveryphonenumber.equalsIgnoreCase("")) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + deleveryphonenumber));
                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                        startActivity(callIntent);
                    }
                } else {
                    Toast.makeText(mContext, "Mobile number not updated", Toast.LENGTH_SHORT).show();
                }

            }
        });
        img_vendor_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("aaaaaaaaa deleveryphonenumber  " + deleveryphonenumber);
                if (!vendorphoneenumber.isEmpty() || !vendorphoneenumber.equalsIgnoreCase("")) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"));
                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        startActivity(callIntent);
                    }

                } else {
                    Toast.makeText(mContext, "Mobile number not updated", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 101: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //  callPhone();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (canShowMapView) {
            if (mapType == "shopAddress" && vendorAddress != null)
                showtracking(vendorAddress.getLocation());
            else if (vendorAddress != null && shippingAddr != null)
                showtracking(shippingAddr.getLocation(), vendorAddress.getLocation());
        }
    }

    @Override
    public void onPolylineOptionsUpdated(PolylineOptions lineOptions) {
    }

    private Marker addMarkerObject(LatLng latLng, String Address, int id) {

        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(Address)
                .icon(BitmapFromVector(getApplicationContext(), id)));
        /*CameraPosition position = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));*/
        return marker;
    }


    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);
        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    public void gotoshop() {
        if (shoplongitude != 0.0 && shoplongitude != 0.0) {
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + shoplatitude +
                    "," + shoplongitude + "&mode=l");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
    }

    private void setDeliverytime(LocationObject vendorLocation, LocationObject customerLocation, String preparation_time) {
        if (vendorLocation != null && customerLocation != null &&
                vendorLocation.getLatitude() != null && !vendorLocation.getLatitude().equals("null") &&
                customerLocation.getLatitude() != null && !customerLocation.getLatitude().equals("null")) {
            LatLng origin = new LatLng(Double.valueOf(vendorLocation.getLatitude()), Double.valueOf(vendorLocation.getlongitude()));
            LatLng destination = new LatLng(Double.valueOf(customerLocation.getLatitude()), Double.valueOf(customerLocation.getlongitude()));

            DirectionListener shopDistanceListener = new DirectionListener() {
                @Override
                public void onDirectionResult(DirectionObject result) {
                    if (tv_preparation_time != null) {
                        try {
                            preptime = Integer.parseInt(preparation_time);
                            deliverytime = getMinutesFromSeconds(result.getDurationInSeconds());
                            System.out.println("aaaaaaaaaa preparationtime  " + preptime + " delviry  " + deliverytime);
                            // tv_preparation_time.setText("Your order is coming in " + (preptime + deliverytime) + " minutes");
                            timerset();
                        } catch (Exception ex) {
                            tv_preparation_time.setText("Your order is coming in " + preparation_time + " minutes");
                        }
                    }
                }
            };
            LocationDetailUtil.GetDirectionData(this, origin, destination, shopDistanceListener);
        } else {
            tv_preparation_time.setText("Your order will be ready in " + orderDetailsmodel.getPreparation_time() + " minutes");
        }
    }

    private Integer getMinutesFromSeconds(Integer durationInSeconds) {
        int minutes = durationInSeconds / 60;
        return minutes;
    }

    private AssignedUserAddress getAssignedUserAddress(JSONObject shippingobj) {
        AssignedUserAddress assignedUserAddress = new AssignedUserAddress();
        try {
            assignedUserAddress.setId(shippingobj.getString("id"));
            if (shippingobj.has("phone"))
                assignedUserAddress.setPhone(shippingobj.getString("phone"));
            else
                assignedUserAddress.setPhone("");

            if (shippingobj.has("email"))
                assignedUserAddress.setEmail(shippingobj.getString("email"));
            assignedUserAddress.setName(shippingobj.getString("name"));
            if (shippingobj.has("landmark"))
                assignedUserAddress.setLandmark(shippingobj.getString("landmark"));
            if (shippingobj.has("address"))
                assignedUserAddress.setAddress(shippingobj.getString("address"));
            assignedUserAddress.setLocation_id(shippingobj.getString("location_id"));
            if (shippingobj.has("vendor_user_id"))
                assignedUserAddress.setVendorUserID(shippingobj.getString("vendor_user_id"));
            if (shippingobj.has("unique_id"))
                assignedUserAddress.setUniqueID(shippingobj.getString("unique_id"));


            if (shippingobj.has("cover_image"))
                assignedUserAddress.setCoverImage(shippingobj.getString("cover_image"));

            if (shippingobj.has("location")) {
                JSONObject locationObject = shippingobj.getJSONObject("location");

                LocationObject vendorLocation = new LocationObject();
                vendorLocation.setLocationID(locationObject.getString("id"));
                vendorLocation.setLatitude(locationObject.getString("latitude"));
                vendorLocation.setLongitude(locationObject.getString("longitude"));
                //vendorLocation.setAddress(locationObject.getString("address"));
                try {
                    shoplatitude = Double.parseDouble(locationObject.getString("latitude"));
                    shoplongitude = Double.parseDouble(locationObject.getString("longitude"));
                } catch (NumberFormatException e) {

                }

                if (assignedUserAddress.getAddress() == null || assignedUserAddress.getAddress().equals("null")
                        || assignedUserAddress.getAddress().equals(null))
                    assignedUserAddress.setAddress(locationObject.getString("address"));

                vendorLocation.setAddress(assignedUserAddress.getAddress());

                assignedUserAddress.setLocation(vendorLocation);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return assignedUserAddress;
    }

    private void showtracking(LocationObject restLocationObject) {
        canShowMapView = false;
        if (restLocationObject == null
                || restLocationObject.getlongitude() == null || restLocationObject.getlongitude().equals("null")) {
            return;
        }
        LatLng restLatLong = new LatLng(Double.valueOf(restLocationObject.getLatitude()), Double.valueOf(restLocationObject.getlongitude()));

        if (prevMarker != null)
            prevMarker.setVisible(false);
        prevMarker = addMarkerObject(restLatLong, vendorAddress.getName(), R.drawable.ic_baseline_store_24);
        prevMarker.showInfoWindow();


        CameraPosition position = new CameraPosition.Builder()
                .target(restLatLong)
                .zoom(15.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
    }

    private void showtracking(LocationObject custLocationObject, LocationObject restLocationObject) {
        canShowMapView = false;
        if (custLocationObject == null || restLocationObject == null
                || custLocationObject.getlongitude() == null || custLocationObject.getlongitude().equals("null")
                || restLocationObject.getlongitude() == null || restLocationObject.getlongitude().equals("null")) {
            return;
        }
        LatLng custLatLong = new LatLng(Double.valueOf(custLocationObject.getLatitude()), Double.valueOf(custLocationObject.getlongitude()));
        LatLng restLatLong = new LatLng(Double.valueOf(restLocationObject.getLatitude()), Double.valueOf(restLocationObject.getlongitude()));

        if (homeMarker == null)
            homeMarker = addMarkerObject(custLatLong, custLocationObject.getAddress(), R.drawable.ic_baseline_pin_drop_24);
        if (prevMarker != null) {
            prevMarker.hideInfoWindow();
            prevMarker.setVisible(false);
        }

        prevMarker = addMarkerObject(restLatLong, vendorAddress.getName(), R.drawable.ic_baseline_store_24);

        DirectionListener shopDistanceListener = new DirectionListener() {
            @Override
            public void onDirectionResult(DirectionObject result) {
                if (result != null) {
                    if (result.getLineOptions() != null) {
                        if (mPolyline != null) {
                            mPolyline.remove();
                        }
                        mPolyline = mMap.addPolyline(result.getLineOptions());
                        ZoomToPoints(custLatLong, restLatLong);


                    }
                }
            }
        };
        LocationDetailUtil.getTravelDistanceFromCurrentLocation(this, restLatLong, shopDistanceListener,
                custLatLong);
    }
    private void ZoomToPoints(LatLng custLatLong, LatLng restLatLong) {
        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(custLatLong);
            builder.include(restLatLong);
            LatLngBounds bounds = builder.build();

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
            mMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                public void onCancel() {
                }

                public void onFinish() {
                    CameraUpdate zout = CameraUpdateFactory.zoomBy(-1.0f);//-3.0f
                    mMap.animateCamera(zout);
                }
            });
        } catch (Exception ex) {
            Log.e("failed in map", ex.getMessage());
        }
    }

    private void setTravelDistance(LocationObject locationObject, TextView view, int pin, Boolean showTracking, int status) {
        if (mMap == null || locationObject == null
                || locationObject.getlongitude() == null ||locationObject.getlongitude().isEmpty() || locationObject.getlongitude().equals("null") ||
                shippingAddr == null || shippingAddr.getLocation() == null ||
                shippingAddr.getLocation().getlongitude() == null || shippingAddr.getLocation().getlongitude().equals("null")) {
            view.setText("--");
            Toast.makeText(mContext, "Location is not available, so map will not be shown.", Toast.LENGTH_SHORT).show();
            return;
        }

        LatLng custLatLong = new LatLng(Double.valueOf(shippingAddr.getLocation().getLatitude()), Double.valueOf(shippingAddr.getLocation().getlongitude()));

        isTrackingInitiated = true;
        LatLng destination = new LatLng(Double.valueOf(locationObject.getLatitude()), Double.valueOf(locationObject.getlongitude()));

        //    if(homeMarker!=null)
        //     homeMarker.setVisible(false);
        if (homeMarker == null)
            homeMarker = addMarkerObject(custLatLong, shippingAddr.getAddress(), R.drawable.ic_baseline_pin_drop_24);

        if (prevMarker != null)
            prevMarker.setVisible(false);

        prevMarker = addMarkerObject(destination, locationObject.getAddress(), pin);
        DirectionListener shopDistanceListener = new DirectionListener() {
            @Override
            public void onDirectionResult(DirectionObject result) {
                if (view != null) {
                    if (result != null) {
                        if (status >= 504 && result.getTravelDistance() != null) {
                            view.setText(result.getTravelDistance());
                            showProgress(result);
                        }
                        if (result.getLineOptions() != null && showTracking) {
                            if (mPolyline != null) {
                                mPolyline.remove();
                            }
                            mPolyline = mMap.addPolyline(result.getLineOptions());
                            ZoomToPoints(custLatLong, destination);
                        }
                    } else
                        view.setText("--");
                }
            }
        };


        LocationDetailUtil.getTravelDistanceFromCurrentLocation(this, custLatLong, shopDistanceListener,
                locationObject.getlongitude(),
                locationObject.getLatitude());
    }

    private void showRatingDialog() {

        cancelLocationTimer();
        isRatingInitiaised = true;

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.customer_rating_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        EditText et_feedback = (EditText) dialog.findViewById(R.id.et_feedback);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                submitRating(ratingBar.getRating(), et_feedback.getText().toString());
                //finish();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //finish();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void submitRating(float rating, String feedback) {
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("delivery_job_id", delivery_job_id);
        uploadMap.put("rating", "" + rating);
        uploadMap.put("feedback", feedback);

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("CUSTOMER_RATING request" + json.toString());

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.CUSTOMER_RATING,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("VolleyResponse", "CUSTOMER_RATING response : " + response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject.toString());
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                //
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrders(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (prepTimeTimer != null)
            prepTimeTimer.cancel();
        cancelLocationTimer();
        cancelOrderDetailTimer();
    }

    private void startLocationTimer() {
        cancelLocationTimer();
        repeatLocationTask = new Timer();
        int repeatInterval = 15000;//60000; // 1 min
        repeatLocationTask.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getDeliveryBoyCurrentLocation();
            }
        }, 0, repeatInterval);

    }

    private void cancelLocationTimer() {
        if (repeatLocationTask != null)
            repeatLocationTask.cancel();

        if (prepTimeTimer != null)
            prepTimeTimer.cancel();
    }

    private void startOrderDetailTimer() {
        cancelOrderDetailTimer();
        repeatorderDetailsTask = new Timer();
        int repeatInterval = 15000; // 15 sec////60000; // 1 min
        repeatorderDetailsTask.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //  getOrderStaus();
                getOrders(false);
            }
        }, 0, repeatInterval);

    }

    private void cancelOrderDetailTimer() {
        if (repeatorderDetailsTask != null)
            repeatorderDetailsTask.cancel();
    }

    private void getDeliveryBoyCurrentLocation() {
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("delivery_partner_user_id", delivery_partner_user_id);//"delivery_partner_user_id":"17"
        uploadMap.put("job_id", job_id);

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("GET_DELIVERY_BOY_LOCATION request" + json.toString());

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_DELIVERY_BOY_LOCATION,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("VolleyResponse", "GET_DELIVERY_BOY_LOCATION: " + response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject.toString());
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataobj = jsonObject.getJSONObject("data");

                                boolean finishedJob = false;
                                int _status = 0;
                                if (dataobj.has("job")) {
                                    JSONObject job = dataobj.getJSONObject("job");
                                    refreshVendorStatuses(job.getInt("status"));
                                    _status = job.getInt("status");
                                    if (_status == 508 && !isRatingInitiaised) {
                                        finishedJob = true;
                                        //  showRatingDialog();
                                        // hideViewsAfterOrderFinish();
                                    }
                                }
                                if (!finishedJob && dataobj.has("current_location")) {
                                    JSONObject locationObject = dataobj.getJSONObject("current_location");

                                    LocationObject vendorLocation = new LocationObject();
                                    vendorLocation.setLocationID(locationObject.getString("id"));
                                    vendorLocation.setLatitude(locationObject.getString("latitude"));
                                    vendorLocation.setLongitude(locationObject.getString("longitude"));
                                    vendorLocation.setAddress(locationObject.getString("address"));

                                    mapType="deliveryBoyToCustomer";
                                    int pin =  R.drawable.motor_pin;
                                            //deliveryType == 1 ? R.drawable.byke_map_pin : R.drawable.auto_map_pin_show;
                                    setTravelDistance(vendorLocation, tv_pick_up_distance,
                                            pin, true, _status);
                                }

                                if (previousStatus != _status) {
                                    previousStatus = _status;
                                    getOrders(false);
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void refreshVendorStatuses(int status) {
        recycler_view_trip_status.setAdapter(null);
        for (int i = 0; i < vendorOrderStatusList.size(); i++) {
            OrderTripStatus orderStatus = vendorOrderStatusList.get(i);
            orderStatus.ChangeOrderTripStatus(status);
        }
        if (status == 16) {
            tv_rejected.setText("Rejected \n Reason : " + message);
            tv_rejected.setVisibility(View.VISIBLE);
            recycler_view_trip_status.setVisibility(View.GONE);
        } else {
            tv_rejected.setVisibility(View.GONE);
            recycler_view_trip_status.setVisibility(View.VISIBLE);
            RecyclerView.Adapter adapter = new OrderTripStatusAdapter(mContext, vendorOrderStatusList);
            recycler_view_trip_status.setAdapter(adapter);
        }
    }

    private void showImageDialog(ImageView img) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.image_dialog);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        TextView txt_upload_message = (TextView) dialog.findViewById(R.id.txt_upload_message);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        ImageView img_preview = (ImageView) dialog.findViewById(R.id.img_preview);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Drawable drawable = img.getDrawable();
        Glide.with(this)
                .load(drawable)
                .placeholder(R.drawable.no_image)
                .into(img_preview);


        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void showProgress(DirectionObject result) {
        if (prepTimeTimer != null)
            prepTimeTimer.cancel();
        tv_on_time.setText("ON TIME");
        Integer deliverytime = getMinutesFromSeconds(result.getDurationInSeconds());
        tv_preparation_time.setText("Your order is coming in " + deliverytime + " minutes");
        pBar.setMax(OrderPrepartionTime + deliverytime);
        pBar.setProgress(OrderPrepartionTime + 1);
    }

    public void timerset() {
        String[] splited = updatetime.split(" ");
        String time = splited[1];
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = df.format(currentTime.getTime());
        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
        try {
            Date date1 = format.parse(splited[1]);
            Date date2 = format.parse("" + formattedDate);
            long mills = date2.getTime() - date1.getTime();
            int hours = (int) (mills / (1000 * 60 * 60));
            int minutes = (int) ((mills / (1000 * 60)) % 60);
            long total = preptime - minutes;
            if (hours > 0 || total <= 0 || minutes <= 0) {
                pBar.setMax(OrderPrepartionTime + deliverytime);
                pBar.setProgress(OrderPrepartionTime);
                tv_preparation_time.setText("Your order is coming in " + (String.format("%02d", deliverytime)) + " minutes");
                tv_on_time.setText("Little Delay");
            } else {
                tv_on_time.setText("ON TIME");
                pBar.setMax(OrderPrepartionTime + deliverytime);
                pBar.setProgress(minutes);
                tv_preparation_time.setText("Your order is coming in " + (String.format("%02d", OrderPrepartionTime + deliverytime - minutes)) + " minutes");

                if(!isTimerSet) {
                    showCountDownTimer(total + 1,minutes);
                }

               /* long millisecInFuture = 60000 * total;
                counter = 0;

                prepTimeTimer = new CountDownTimer(millisecInFuture, 60000) {

                    public void onTick(long millisUntilFinished) {
                        counter = counter + 1;
                        if ((minutes + counter) <= preptime) {
                            pBar.setProgress(minutes + counter);
                            tv_preparation_time.setText("Your order is coming in " + (String.format("%02d", OrderPrepartionTime + deliverytime - minutes - counter)) + " minutes");
                        }
                    }

                    public void onFinish() {
                    }
                }.start();*/
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void showTimerforselfPickup() {
        String[] splited = updatetime.split(" ");
        String time = splited[1];
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = df.format(currentTime.getTime());
        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");

        try {
            Date date1 = format.parse(splited[1]);
            Date date2 = format.parse("" + formattedDate);
            long mills = date2.getTime() - date1.getTime();
            int hours = (int) (mills / (1000 * 60 * 60));
            int minutes = (int) ((mills / (1000 * 60)) % 60);
            long total = preptime - minutes;
            if (hours > 0 || total <= 0 || minutes <= 0) {
                pBar.setMax(OrderPrepartionTime);
                pBar.setProgress(OrderPrepartionTime);
                tv_preparation_time.setText("Your order will be ready to Pick up");
                tv_on_time.setText("Ready");
            } else {
                pBar.setMax(OrderPrepartionTime);
                pBar.setProgress(minutes);
                tv_preparation_time.setText("Your order will be ready in " + (String.format("%02d", OrderPrepartionTime - minutes)) + " minutes");

                long millisecInFuture = 60000 * total;
                counter = 0;

                prepTimeTimer = new CountDownTimer(millisecInFuture, 60000) {

                    public void onTick(long millisUntilFinished) {
                        counter = counter + 1;
                        if ((minutes + counter) <= preptime) {
                            pBar.setProgress(minutes + counter);
                            tv_preparation_time.setText("Your order will be ready in " + (String.format("%02d", OrderPrepartionTime - minutes - counter)) + " minutes");
                        }
                    }

                    public void onFinish() {
                    }
                }.start();

                System.out.println("aaaaaaaa splited[1]  " + splited[1]);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    /*
    Order Details API
     */

    Integer CurrentStatus = -1;
    public void getOrders(Boolean showDialog) {
        if(productlist==null)
            productlist = new ArrayList<>();
        productlist.clear();


        if (showDialog)
            mCustomDialog.show();

        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("order_id", orderId);
        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaaaa request" + json.toString());
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERDETAILS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        CurrentStatus=-1;
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("VolleyResponse", "order_details: " + response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject.toString());
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataobj = jsonObject.getJSONObject("data");

                                orderDetailsmodel = new OrderDetailsmodel();
                                shippingAddr = null;
                                vendorAddress = null;

                                orderDetailsmodel.setTrack_id(dataobj.getString("track_id"));
                                orderDetailsmodel.setDelivery_fee(dataobj.getString("delivery_fee"));
                                orderDetailsmodel.setTotal(dataobj.getString("total"));
                                orderDetailsmodel.setUsed_wallet_amount(dataobj.getString("used_wallet_amount"));
                                orderDetailsmodel.setMessage(dataobj.getString("message"));
                                orderDetailsmodel.setPreparation_time(dataobj.getString("preparation_time"));
                                orderDetailsmodel.setShipping_address_id(dataobj.getString("shipping_address_id"));
                                orderDetailsmodel.setCreated_at(dataobj.getString("created_at"));
                                orderDetailsmodel.setDelivery_mode_id(dataobj.getString("delivery_mode_id"));
                                orderDetailsmodel.setCreated_user_id(dataobj.getString("created_user_id"));
                                orderDetailsmodel.setOrder_status_id(dataobj.getString("order_status_id"));
                                orderDetailsmodel.setPayment_id(dataobj.getString("payment_id"));
                                orderDetailsmodel.setId(dataobj.getString("id"));
                                if (dataobj.has("order_pickup_otp"))
                                    orderDetailsmodel.setOrderPickupOTP(dataobj.getString("order_pickup_otp"));
                                if (dataobj.has("order_delivery_otp"))
                                    orderDetailsmodel.setOrderDeliveryOTP(dataobj.getString("order_delivery_otp"));
                                message = dataobj.getString("message");

                                try {
                                    JSONObject shippingobj = dataobj.getJSONObject("shipping_address");
                                    shippingAddr = getAssignedUserAddress(shippingobj);
                                    //orderDetailsmodel.setShippingAddress(shippingAddr);
                                    if(dataobj.has("vendor")) {
                                        //vendor accpted the order
                                        JSONObject vendorAddressObj = dataobj.getJSONObject("vendor");
                                        vendorAddress = getAssignedUserAddress(vendorAddressObj);
                                        orderDetailsmodel.setVendorAddress(vendorAddress);
                                    }

                                    try {
                                        Object aObj =null;
                                        if(dataobj.has("delivery_job"))
                                            aObj = dataobj.get("delivery_job");
                                        if (aObj instanceof JSONObject) {

                                            JSONObject delivery_job = (JSONObject) aObj;
                                            DeliveryJobObject deliveryJobObject = new DeliveryJobObject();
                                            deliveryJobObject.setID(delivery_job.getString("id"));
                                            deliveryJobObject.setJobID(delivery_job.getString("job_id"));
                                            deliveryJobObject.setRating(delivery_job.getString("rating"));
                                            deliveryJobObject.setFeedback(delivery_job.getString("feedback"));
                                            deliveryJobObject.setJobType(delivery_job.getString("job_type"));
                                            deliveryJobObject.setDeliveryBoyUserID(delivery_job.getString("delivery_boy_user_id"));
                                            deliveryJobObject.setStatus(delivery_job.getString("status"));

                                            if (delivery_job.has("delivery_boy")) {
                                                JSONObject delivery_boy = delivery_job.getJSONObject("delivery_boy");
                                                DeliveryBoyObject deliveryBoyObject = new DeliveryBoyObject();
                                                deliveryBoyObject.setId(delivery_boy.getString("id"));
                                                deliveryBoyObject.setFirst_name(delivery_boy.getString("first_name"));
                                                deliveryBoyObject.setLast_name(delivery_boy.getString("last_name"));
                                                deliveryBoyObject.setPhone(delivery_boy.getString("phone"));
                                                deliveryBoyObject.setUnique_id(delivery_boy.getString("unique_id"));
                                                deliveryBoyObject.setProfileImage(delivery_boy.getString("profile_image"));
                                                if (delivery_boy.has("delivery_boy_pickup_image"))
                                                    deliveryBoyObject.setDelivery_boy_pickup_image(delivery_boy.getString("delivery_boy_pickup_image"));
                                                if (delivery_boy.has("delivery_boy_delivery_image"))
                                                    deliveryBoyObject.setDelivery_boy_delivery_image(delivery_boy.getString("delivery_boy_delivery_image"));
                                                deliveryJobObject.setDeliveryBoy(deliveryBoyObject);
                                            }
                                            orderDetailsmodel.setDeliveryBoyJob(deliveryJobObject);
                                        }
                                    } catch (Exception ex) {
                                    }

                                } catch (JSONException e1) {
                                }

                                try {
                                    JSONObject deleveryong = dataobj.getJSONObject("delivery_mode");
                                    DeleveryModesmodel deleveryModesmodel = new DeleveryModesmodel();
                                    deleveryModesmodel.setId(deleveryong.getString("id"));
                                    deleveryModesmodel.setName(deleveryong.getString("name"));
                                    deleveryModesmodel.setDesc(deleveryong.getString("desc"));
                                    orderDetailsmodel.setDeleveryModesmodel(deleveryModesmodel);
                                    // tv_deleverymode.setText(""+deleveryong.getString("name"));
                                } catch (JSONException e2) {
                                }

                                try {
                                    JSONObject customerobj = dataobj.getJSONObject("customer");
                                    CustomerModel customerModel = new CustomerModel();
                                    customerModel.setId(customerobj.getString("id"));
                                    customerModel.setUnique_id(customerobj.getString("unique_id"));
                                    customerModel.setFirst_name(customerobj.getString("first_name"));
                                    customerModel.setPhone(customerobj.getString("phone"));
                                    orderDetailsmodel.setCustomerModel(customerModel);
                                } catch (JSONException e2) {

                                }
                                try
                                {
                                    JSONObject orderostatusobj = dataobj.getJSONObject("order_status");
                                    OrderStatus orderStatus = new OrderStatus();
                                    orderStatus.setId(orderostatusobj.getString("id"));
                                    orderStatus.setDelivery_mode_id(orderostatusobj.getString("delivery_mode_id"));
                                    orderStatus.setSerial_number(orderostatusobj.getString("serial_number"));
                                    orderStatus.setStatus(orderostatusobj.getString("status"));
                                    orderDetailsmodel.setOrderStatus(orderStatus);

                                    JSONObject deliveryModeObject = dataobj.getJSONObject("delivery_mode");
                                    JSONArray deliveryModeStatuses = deliveryModeObject.getJSONArray("order_statuses");


                                    if(CurrentStatus==-1)
                                        CurrentStatus = orderostatusobj.getInt("id");

                                    vendorOrderStatusList.clear();
                                    if (deliveryModeStatuses != null && deliveryModeStatuses.length() > 0) {
                                        for (int i = 0; i < deliveryModeStatuses.length(); i++) {
                                            JSONObject statusObject = deliveryModeStatuses.getJSONObject(i);

                                            int _id = statusObject.getInt("id");
                                            if (/*statusObject.getInt("id") != 1 &&*/ statusObject.getInt("id") != 2 &&
                                                    /*statusObject.getInt("id") != 9 && */statusObject.getInt("id") != 10
                                                /* && statusObject.getInt("id") != 12*/) {
                                                OrderTripStatus status1 = new OrderTripStatus(CurrentStatus);
                                                status1.setorderStatusID(_id);
                                                if (_id == 1 || _id == 9)
                                                    status1.setorderStatus("Order Received");
                                                else if (_id == 3 || _id == 11)
                                                    status1.setorderStatus("Order is been preparing");
                                                else if (_id == 4)
                                                    status1.setorderStatus("Ready to Pick Up");
                                                else if (_id == 5)
                                                    status1.setorderStatus("Picked Up");
                                                else
                                                    status1.setorderStatus(statusObject.getString("status"));
                                                status1.setTime("9.15 AM");
                                                vendorOrderStatusList.add(status1);
                                            }
                                        }

                                        if (orderDetailsmodel.getDelivery_mode_id().toString().equals("2")) {
                                            //  4 = Reached to pickup point, 5 = Picked the order, 6 = Reached to delivery point, 7 = Delivery is on hold, 8 = Delivered
                                            //delivery
                                            OrderTripStatus status1 = new OrderTripStatus(CurrentStatus);
                                            status1.setorderStatusID(504);
                                            status1.setorderStatus("Reached to pickup point");
                                            status1.setTime("9.15 AM");
                                            //vendorOrderStatusList.add(status1);

                                            status1 = new OrderTripStatus(CurrentStatus);
                                            status1.setorderStatusID(505);
                                            status1.setorderStatus("Picked the order");
                                            status1.setTime("9.15 AM");
                                            //vendorOrderStatusList.add(status1);

                                            status1 = new OrderTripStatus(CurrentStatus);
                                            status1.setorderStatusID(506);
                                            status1.setorderStatus("Reached to delivery point");
                                            status1.setTime("9.15 AM");
                                            vendorOrderStatusList.add(status1);

                                            status1 = new OrderTripStatus(CurrentStatus);
                                            status1.setorderStatusID(508);
                                            status1.setorderStatus("Delivered");
                                            status1.setTime("9.15 AM");
                                            vendorOrderStatusList.add(status1);
                                        }
                                    }
                                } catch (JSONException e2) {
                                }

                                try {
                                    JSONObject paymentobj = dataobj.getJSONObject("payment");
                                    PaymentModel paymentModel = new PaymentModel();
                                    paymentModel.setId(paymentobj.getString("id"));
                                    paymentModel.setPayment_method_id(paymentobj.getString("payment_method_id"));
                                    paymentModel.setTxn_id(paymentobj.getString("txn_id"));
                                    paymentModel.setAmount(paymentobj.getString("amount"));
                                    paymentModel.setCreated_at(paymentobj.getString("created_at"));
                                    paymentModel.setMessage(paymentobj.getString("message"));
                                    paymentModel.setStatus(paymentobj.getString("status"));
                                    orderDetailsmodel.setPaymentModel(paymentModel);
                                    // payment_type.setText(paymentobj.getString("txn_id"));
                                } catch (JSONException e2) {

                                }

                                JSONArray productarray = dataobj.getJSONArray("ecom_order_details");
                                for (int k = 0; k < productarray.length(); k++) {
                                    JSONObject productonj = productarray.getJSONObject(k);
                                    ProductDetailsModel productDetailsModel = new ProductDetailsModel();

                                    productDetailsModel.setId(productonj.getString("id"));
                                    productDetailsModel.setEcom_order_id(productonj.getString("ecom_order_id"));
                                    productDetailsModel.setId(productonj.getString("item_id"));
                                    productDetailsModel.setVendor_product_variant_id(productonj.getString("vendor_product_variant_id"));
                                    productDetailsModel.setQty(productonj.getString("qty"));
                                    productDetailsModel.setPrice(productonj.getString("price"));
                                    productDetailsModel.setRate_of_discount(productonj.getString("rate_of_discount"));
                                    productDetailsModel.setSub_total(productonj.getString("sub_total"));
                                    productDetailsModel.setDiscount(productonj.getString("discount"));
                                    productDetailsModel.setTax(productonj.getString("tax"));
                                    productDetailsModel.setTotal(productonj.getString("total"));
                                    productDetailsModel.setCancellation_message(productonj.getString("cancellation_message"));
                                    productDetailsModel.setStatus(productonj.getString("status"));


                                    try {
                                        JSONObject itemobj = productonj.getJSONObject("item");
                                        ItemModel itemModel = new ItemModel();
                                        itemModel.setId(itemobj.getString("id"));
                                        itemModel.setName(itemobj.getString("name"));
                                        itemModel.setDesc(itemobj.getString("desc"));


                                        ArrayList<ImagesModel> imageslist = new ArrayList<>();
                                        try {
                                            JSONArray jsonArray = itemobj.getJSONArray("item_images");
                                            for (int M = 0; M < jsonArray.length(); M++) {
                                                JSONObject jsonObject1 = jsonArray.getJSONObject(M);
                                                ImagesModel imagesModel = new ImagesModel();
                                                imagesModel.setId(jsonObject1.getString("id"));
                                                imagesModel.setImage(jsonObject1.getString("image"));
                                                imageslist.add(imagesModel);
                                            }
                                            itemModel.setImagelist(imageslist);
                                        } catch (Exception e) {

                                        }


                                        JSONObject varientobj = itemobj.getJSONObject("varinat");
                                        VarientModel varientModel = new VarientModel();
                                        varientModel.setId(varientobj.getString("id"));
                                        varientModel.setSku(varientobj.getString("sku"));
                                        varientModel.setPrice(varientobj.getString("price"));
                                        varientModel.setStock(varientobj.getString("stock"));
                                        varientModel.setDiscount(varientobj.getString("discount"));
                                        varientModel.setTax_id(varientobj.getString("tax_id"));
                                        varientModel.setStatus(varientobj.getString("status"));
                                        varientModel.setSection_item_id(varientobj.getString("section_item_id"));

                                        JSONObject sectionobj = varientobj.getJSONObject("section_item");
                                        SectionItemModel sectionItemModel = new SectionItemModel();
                                        sectionItemModel.setId(sectionobj.getString("id"));
                                        sectionItemModel.setName(sectionobj.getString("name"));
                                        sectionItemModel.setWeight(sectionobj.getString("weight"));

                                        varientModel.setSectionItemModel(sectionItemModel);

                                        itemModel.setVarientModel(varientModel);
                                        productDetailsModel.setItemModel(itemModel);

                                        productlist.add(productDetailsModel);
                                    } catch (JSONException e2) {

                                    }

                                }

                                if (productlist.size() > 0) {
                                    System.out.println("aaaaaaaaa productlist  " + productlist.size());

                                    adapter.setrefresh(productlist);

                                    double subtotal = 0.0, discount = 0.0, rateofdiscount = 0.0, tax = 0.0;

                                    for (int i = 0; i < productlist.size(); i++) {
                                        subtotal = subtotal + Double.parseDouble(productlist.get(i).getSub_total());
                                        discount = discount + Double.parseDouble(productlist.get(i).getDiscount());
                                        rateofdiscount = rateofdiscount + Double.parseDouble(productlist.get(i).getRate_of_discount());
                                        tax = tax + Double.parseDouble(productlist.get(i).getTax());
                                    }
                                    layout_discount.setVisibility(View.VISIBLE);
                                    if (discount <= 0) {
                                        layout_discount.setVisibility(View.GONE);
                                    }
                                    tv_sub_total.setText("" + subtotal);
                                    tv_discount.setText("" + rateofdiscount + "% off( " + discount + " )");
                                    tv_tax.setText("" + tax);

                                    //price after discount
                                    // tv_price_product_amount_total.setText(""+(subtotal-discount));

                                    int delFee = Integer.parseInt(orderDetailsmodel.getDelivery_fee());
                                    tv_item_grand_total_total.setText("" + (delFee + tax + subtotal - discount));
                                    tv_deliveryfee.setText(orderDetailsmodel.getDelivery_fee() + "/-Rs");
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (showDialog)
                                mCustomDialog.dismiss();
                        } finally {
                            bindOrderToView();
                            if (showDialog)
                                mCustomDialog.dismiss();

                            //Ideally delivery boy location should fetch once he received the order from vendor
                            if (showDialog) {
                                previousStatus = Integer.parseInt(OrderStatus);
                                if (isDeliveryBoyAssigned)
                                    startLocationTimer();
                                else if (CurrentStatus != 5 && CurrentStatus != 508)//if (map_layout.getVisibility() == View.VISIBLE/* && isSelfPickup*/)
                                    startOrderDetailTimer();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (showDialog)
                    mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    /**
     * Order Status List
     */
    private void bindOrderToView() {
        try {
            if (orderDetailsmodel != null) {
                tv_oder_id.setText("ORDER #" + orderDetailsmodel.getTrack_id());
                updatetime = orderDetailsmodel.getCreated_at();
                if (orderDetailsmodel.getPreparation_time() != null && !orderDetailsmodel.getPreparation_time().equals("null"))
                    OrderPrepartionTime = Integer.parseInt(orderDetailsmodel.getPreparation_time());

                tv_item_grand_total_total.setText(orderDetailsmodel.getTotal());
                tv_deliveryfee.setText(orderDetailsmodel.getDelivery_fee());

                boolean isOrderAcceptedByVendor = false;
                String delivery_mode_id = orderDetailsmodel.getDelivery_mode_id();
                if (delivery_mode_id.equals("1")) {
                    //selfpickup
                    map_layout.setVisibility(View.GONE);
                    isSelfPickup = true;
                } else {
                    isSelfPickup = false;
                    layout_track_shipping_address.setVisibility(View.GONE);
                }
                if (isSelfPickup) {
                    String otp = orderDetailsmodel.getOrderPickupOTP();
                    if (!MyUtil.isValidValue(otp))
                        order_receive_otp_layout.setVisibility(View.GONE);
                    else {
                        isOrderAcceptedByVendor = true;
                        order_receive_otp.setText(otp);
                        order_receive_otp_layout.setVisibility(View.VISIBLE);
                    }
                } else if (MyUtil.isValidValue(orderDetailsmodel.getOrderDeliveryOTP())) {
                    String otp = orderDetailsmodel.getOrderDeliveryOTP();
                    if (!MyUtil.isValidValue(otp))
                        order_receive_otp_layout.setVisibility(View.GONE);
                    else {
                        isOrderAcceptedByVendor = true;
                        order_receive_otp.setText(otp);
                        order_receive_otp_layout.setVisibility(View.VISIBLE);
                    }
                } else {
                    //order not acceted yet, so should show map pin on shop address
                    order_receive_otp_layout.setVisibility(View.GONE);

                    String otp = orderDetailsmodel.getOrderPickupOTP();
                    if (MyUtil.isValidValue(otp))
                        isOrderAcceptedByVendor = true;

                    mapType="shopAddress";
                    if (mMap == null) {
                        //map is not initialised yet, so based on this flag after map is loaded then vendor details can shown on map
                        canShowMapView = true;
                    } else if(!isOrderAcceptedByVendor){
                        showtracking(vendorAddress.getLocation());
                    }
                }
                if (vendorAddress.getCoverImage() != null) {
                    Glide.with(getApplicationContext())
                            .load(vendorAddress.getCoverImage())
                            .placeholder(R.drawable.no_image)
                            .into(img_product);
                }
                address_details.setText(vendorAddress.getAddress());
                tv_vendor_name.setText(vendorAddress.getName());
                tv_vendor_address.setText(vendorAddress.getAddress());
                String text = "<font color=#9D9D9D>" + vendorAddress.getName() +
                        "</font> <font color=#141414>" + vendorAddress.getPhone() + "</font>";
                seller_details.setText(Html.fromHtml(text));
                // cust_address_details.setText(shippingAddr.getAddress());

                DeliveryJobObject deliveryJobObject = orderDetailsmodel.getDeliveryBoyJob();

                if (deliveryJobObject == null) {
                    if(isOrderAcceptedByVendor) {
                        // no delivery boy assigned
                        if (mMap == null) {
                            //map is not initialised yet, so based on this flag after map is loaded then vendor details can shown on map
                            canShowMapView = true;
                        } else
                            showtracking(shippingAddr.getLocation(), vendorAddress.getLocation());
                    }
                } else {
                    canShowMapView = false;

                    if (deliveryJobObject.getStatus().equals("508")) {
                        CurrentStatus = 508;
                        isDeliveryBoyAssigned = false;
                        map_layout.setVisibility(View.GONE);
                        order_receive_otp_layout.setVisibility(View.GONE);
                        layout_order_delivery_time.setVisibility(View.GONE);

                        cancelLocationTimer();
                        cancelOrderDetailTimer();
                        img_call.setVisibility(View.GONE);
                    } else {
                        //Ideally delivery boy location should fetch once he received the order from vendor
                        isDeliveryBoyAssigned = true;
                        CurrentStatus = Integer.parseInt(deliveryJobObject.getStatus());
                    }
                    delivery_job_id = deliveryJobObject.getID();
                    OrderStatus = deliveryJobObject.getStatus();
                    refreshVendorStatuses(Integer.parseInt(deliveryJobObject.getStatus()));
                    job_id = deliveryJobObject.getID();
                    String deliveryBoyCurrentStatus = " is on the way to pick up your order";
                    if (CurrentStatus >= 508)
                        deliveryBoyCurrentStatus = " is successfully delivered your order";
                    else if (CurrentStatus >= 505)
                        deliveryBoyCurrentStatus = " is on the way to deliver your order";
                    else if (CurrentStatus >= 504)
                        deliveryBoyCurrentStatus = " is waiting at the restaurant to pick up your order";


                    DeliveryBoyObject deliveryBoyObject = deliveryJobObject.getDeliveryBoy();
                    if (deliveryBoyObject != null) {
                        deleveryphonenumber = deliveryBoyObject.getPhone();
                        delivery_partner_user_id = deliveryBoyObject.getId();
                        String delivery_boy_pickup_image = deliveryBoyObject.getDelivery_boy_pickup_image();
                        Glide.with(mContext)
                                .load(delivery_boy_pickup_image)
                                .placeholder(R.drawable.no_image)
                                .into(img_pick_up);
                        layout_pickup_image.setVisibility(View.VISIBLE);
                        String delivery_boy_delivery_image = deliveryBoyObject.getDelivery_boy_delivery_image();
                        Glide.with(mContext)
                                .load(delivery_boy_delivery_image)
                                .placeholder(R.drawable.no_image)
                                .into(img_delivery);
                        layout_delivery_image.setVisibility(View.VISIBLE);


                        String deliveryBoyDetails = "<font color=#141414>" + deliveryBoyObject.getFirst_name() + " " + deliveryBoyObject.getLast_name() +
                                "</font> <font color=#9D9D9D>" + deliveryBoyCurrentStatus + "</font>";
                        tv_delivery_boy_name.setText(Html.fromHtml(deliveryBoyDetails));
                        //  tv_delivery_boy_name.setText(deliveryBoyObject.getFirst_name()+" "+deliveryBoyObject.getLast_name());

                        if (deliveryBoyObject.getProfileImage() != null && !deliveryBoyObject.getProfileImage().equals("null")) {
                            Glide.with(getApplicationContext())
                                    .load(deliveryBoyObject.getProfileImage())
                                    .placeholder(R.drawable.ui_profile_pic)
                                    .into(img_delivery_boy);
                        }
                        layout_deleveryboy.setVisibility(View.VISIBLE);
                    }
                    if (deliveryJobObject.getRating().equals(null) || deliveryJobObject.getRating().equals("null")) {
                        tv_delivery_boy_rating.setText("****");
                        if (CurrentStatus == 508) {
                            showRatingDialog();
                        }
                    } else
                        tv_delivery_boy_rating.setText(deliveryJobObject.getRating());
                }

                if (CurrentStatus != 508 && orderDetailsmodel.getPreparation_time() != null &&
                        !orderDetailsmodel.getPreparation_time().equals("null")) {
                    if (delivery_mode_id.equals("2"))//delivery
                        setDeliverytime(vendorAddress.getLocation(), shippingAddr.getLocation(), orderDetailsmodel.getPreparation_time());
                    else {
                        preptime = Integer.parseInt(orderDetailsmodel.getPreparation_time());
                        tv_preparation_time.setText("Your order will be ready in " + orderDetailsmodel.getPreparation_time() + " minutes");
                        showTimerforselfPickup();
                    }
                    layout_order_delivery_time.setVisibility(View.VISIBLE);
                } else {
                    layout_order_delivery_time.setVisibility(View.GONE);
                }
                OrderStatus orderStatus = orderDetailsmodel.getOrderStatus();
                if (CurrentStatus == -1)
                    CurrentStatus = Integer.parseInt(orderStatus.getId());

                if (isSelfPickup && Integer.parseInt(orderStatus.getId()) == 5) {
                    //deleivered
                    layout_track_shipping_address.setVisibility(View.GONE);
                }
                if (!orderDetailsmodel.getDelivery_mode_id().toString().equals("2")) {
                    if (CurrentStatus == 5) {
                        //Delivered
                        layout_order_delivery_time.setVisibility(View.GONE);
                        order_receive_otp_layout.setVisibility(View.GONE);
                    }
                }

                if (CurrentStatus == 16) {
                    tv_trip_header.setVisibility(View.GONE);
                    recycler_view_trip_status.setVisibility(View.GONE);
                    tv_rejected.setVisibility(View.VISIBLE);
                    tv_rejected.setText("Rejected \n Reason : " + message);
                } else {
                    recycler_view_trip_status.setVisibility(View.VISIBLE);
                    tv_trip_header.setVisibility(View.VISIBLE);
                    tv_rejected.setVisibility(View.GONE);
                    RecyclerView.Adapter adapter = new OrderTripStatusAdapter(mContext, vendorOrderStatusList);
                    recycler_view_trip_status.setAdapter(adapter);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recycler_view_trip_status.setHasFixedSize(true);
                    recycler_view_trip_status.setLayoutManager(linearLayoutManager);
                }
            }
        } catch (Exception ex) {
            Log.e("exception in order detail",ex.getMessage());
        }
        order_layout.setVisibility(View.VISIBLE);
    }

    long previousMinutesRemaining=0;
    boolean isTimerSet =false;
    private void showCountDownTimer(long totalTimeInMinutes,int minutes) {
        isTimerSet = true;
        long totalTimeInSeconds=totalTimeInMinutes * 60 * 1000;//30000
        long countDownInterval =1000;
        prepTimeTimer =  new CountDownTimer(totalTimeInSeconds,countDownInterval ) {

            public void onTick(long millisUntilFinished) {
                long secondsRemaining =millisUntilFinished / 1000;
                long minutesRemaining =secondsRemaining /  60;
                if(previousMinutesRemaining!= minutesRemaining) {
                    previousMinutesRemaining = minutesRemaining;
                    try {
                        if ((minutes + minutesRemaining) <= preptime) {
                            pBar.setProgress(minutes + ((int) minutesRemaining));
                            tv_preparation_time.setText("Your order is coming in " +
                                    (String.format("%02d", OrderPrepartionTime + deliverytime - minutesRemaining)) + " minutes");
                        }
                    }catch (Exception ex){}
                    //Log.d("Timer ", "minutes remaining: " + minutesRemaining + ", seconds remaining: " + secondsRemaining);
                }
                //Log.d("Time ", "minutes remaining: " + minutesRemaining + ", seconds remaining: " + secondsRemaining);
            }
            public void onFinish() {
                Log.d("Timer ","done ");
                try {
                    pBar.setMax(OrderPrepartionTime + deliverytime);
                    pBar.setProgress(OrderPrepartionTime);
                    tv_preparation_time.setText("Your order is coming in " + (String.format("%02d", deliverytime)) + " minutes");
                    tv_on_time.setText("Little Delay");
                }catch (Exception ex){}
            }
        }.start();
    }

    private void showCountDownTimer_Backup(long totalTimeInMinutes) {
        long totalTimeInSeconds=totalTimeInMinutes * 60 * 1000;//30000
        new CountDownTimer(totalTimeInSeconds, 1000) {

            public void onTick(long millisUntilFinished) {
                long secondsRemaining =millisUntilFinished / 1000;
                long minutesRemaining =secondsRemaining /  60;
                Log.d("Timer ","minutes remaining: " +minutesRemaining+", seconds remaining: " +secondsRemaining );
            }
            public void onFinish() {
                Log.d("Timer ","done ");
            }
        }.start();
    }
}