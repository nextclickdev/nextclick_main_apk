package com.apticks.nextclickuser.orderhistory.model;

public class DeliveryJobObject {

    String ID;
    private DeliveryBoyObject deliveryBoyObject;

    public void setID(String title) { this.ID = title; }
    public String getID() { return ID; }

    String DeliveryBoyUserID;
    public void setDeliveryBoyUserID(String title) { this.DeliveryBoyUserID = title; }
    public String getDeliveryBoyUserID() { return DeliveryBoyUserID; }

    String JobID;
    public void setJobID(String title) { this.JobID = title; }
    public String getJobID() { return JobID; }
    String delivery_fee="0";
    public String getDelivery_fee() {
        return delivery_fee;
    }
    public void setDelivery_fee(String delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    String JobType;
    public void setJobType(String title) { this.JobType = title; }
    public String getJobType() { return JobType; }

    String Status;
    public void setStatus(String title) { this.Status = title; }
    public String getStatus() { return Status; }

    String Feedback;
    public void setFeedback(String title) { this.Feedback = title; }
    public String getFeedback() { return Feedback; }

    String Rating;
    public void setRating(String title) { this.Rating = title; }
    public String getRating() { return Rating; }

    public void setDeliveryBoy(DeliveryBoyObject deliveryBoyObject) {
        this.deliveryBoyObject=deliveryBoyObject;
    }
    public DeliveryBoyObject getDeliveryBoy()
    {
        return deliveryBoyObject;
    }
}
