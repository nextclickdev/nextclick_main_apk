package com.apticks.nextclickuser.orderhistory.model;

public class DeliveryOrder {
    String DeliveryJobID;
    public void setDeliveryJobID(String title) { this.DeliveryJobID = title; }
    public String getDeliveryJobID() { return DeliveryJobID; }
    String JobID;
    public void setJobID(String title) { this.JobID = title; }
    public String getJobID() { return JobID; }
    String delivery_fee="0";
    public String getDelivery_fee() {
        return delivery_fee;
    }
    public void setDelivery_fee(String delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    String Status;
    public void setStatus(String title) { this.Status = title; }
    public String getStatus() { return Status; }


    public String getStatusMessage() {

        //0 = Rejected, 1 = Received, 2 = Accepted, 3 = Cancelled, 4 = Reached to pickup point,
        // 5 = Picked the order, 6 = Reached to delivery point, 7 = Delivery is on hold, 8 = Delivered
        switch (Status)
        {
            case "0": return  "Rejected";
            case "1": return  "Received";
            case "2": return  "Accepted";
            case "3": return  "Cancelled";
            case "4": return  "Reached to pickup point";
            case "5": return  " Picked the order";
            case "6": return  "Reached to delivery point";
            case "7": return  "Delivery is on hold";
            case "8": return  "Delivered";
            default: return "Received";
        }
    }

    String ID;
    public void setID(String title) { this.ID = title; }
    public String getID() { return ID; }
    String TrackID;
    public void setTrackID(String title) { this.TrackID = title; }
    public String getTrackID() { return TrackID; }
    String OrderPickupOtp;
    public void setOrderPickupOtp(String title) { this.OrderPickupOtp = title; }
    public String getOrderPickupOtp() { return OrderPickupOtp; }
    String PreparationTime;
    public void setPreparationTime(String title) { this.PreparationTime = title; }
    public String getPreparationTime() { return PreparationTime; }
    String ShippingAddressID;
    public void setShippingAddressID(String title) { this.ShippingAddressID = title; }
    public String getShippingAddressID() { return ShippingAddressID; }
    String PaymentID;
    public void setPaymentID(String title) { this.PaymentID = title; }
    public String getPaymentID() { return PaymentID; }
    String Total;
    public void setTotal(String title) { this.Total = title; }
    public String getTotal() { return Total; }
    String Message;
    public void setMessage(String title) { this.Message = title; }
    public String getMessage() { return Message; }
    String CreatedUserID;
    public void setCreatedUserID(String CreatedUserID) { this.CreatedUserID = CreatedUserID; }
    public String getCreatedUserID() { return CreatedUserID; }
    String VendorUserID;
    public void setVendorUserID(String VendorUserID) { this.VendorUserID = VendorUserID; }
    public String getVendorUserID() { return VendorUserID; }
    String CreatedAt;
    public void setCreatedAt(String CreatedAt) { this.CreatedAt = CreatedAt; }
    public String getCreatedAt() { return CreatedAt; }
    String UpdatedAt;
    public void setUpdatedAt(String UpdatedAt) { this.UpdatedAt = UpdatedAt; }
    public String getUpdatedAt() { return UpdatedAt; }
    String OrderStatusID;
    public void setOrderStatusID(String OrderStatusID) { this.OrderStatusID = OrderStatusID; }
    public String getOrderStatusID() { return OrderStatusID; }


    DeliveryOrderPayment deliveryOrderPayment;
    public void setDeliveryOrderPayment(DeliveryOrderPayment deliveryOrderPayment) { this.deliveryOrderPayment = deliveryOrderPayment; }
    public DeliveryOrderPayment getDeliveryOrderPayment() { return deliveryOrderPayment; }


    DeliveryOrderStatus deliveryOrderStatus;
    public void setDeliveryOrderStatus(DeliveryOrderStatus deliveryOrderStatus) { this.deliveryOrderStatus = deliveryOrderStatus; }
    public DeliveryOrderStatus getDeliveryOrderStatus() { return deliveryOrderStatus; }


    String OrderDeliveryOtp;
    public void setOrderDeliveryOtp(String OrderDeliveryOtp) { this.OrderDeliveryOtp = OrderDeliveryOtp; }
    public String getOrderDeliveryOtp() { return OrderDeliveryOtp; }
}
