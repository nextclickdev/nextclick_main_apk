package com.apticks.nextclickuser.orderhistory.model;

import java.util.ArrayList;

public class OrderDetailsmodel {

    String id,track_id,delivery_fee,total,used_wallet_amount,message,preparation_time,created_at,shipping_address_id,delivery_mode_id,
            created_user_id,order_status_id,payment_id;

    AssignedUserAddress shippingAddress;
    AssignedUserAddress vendorAddress;

    DeleveryModesmodel deleveryModesmodel;
    CustomerModel customerModel;
    OrderStatus orderStatus;
    PaymentModel paymentModel;

    ArrayList<ProductDetailsModel> productlist;
    private String order_pickup_otp;
    private String order_delivery_otp;
    private DeliveryJobObject deliveryJobObject;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(String delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUsed_wallet_amount() {
        return used_wallet_amount;
    }

    public void setUsed_wallet_amount(String used_wallet_amount) {
        this.used_wallet_amount = used_wallet_amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPreparation_time() {
        return preparation_time;
    }

    public void setPreparation_time(String preparation_time) {
        this.preparation_time = preparation_time;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getShipping_address_id() {
        return shipping_address_id;
    }

    public void setShipping_address_id(String shipping_address_id) {
        this.shipping_address_id = shipping_address_id;
    }

    public String getDelivery_mode_id() {
        return delivery_mode_id;
    }

    public void setDelivery_mode_id(String delivery_mode_id) {
        this.delivery_mode_id = delivery_mode_id;
    }

    public String getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(String created_user_id) {
        this.created_user_id = created_user_id;
    }

    public String getOrder_status_id() {
        return order_status_id;
    }

    public void setOrder_status_id(String order_status_id) {
        this.order_status_id = order_status_id;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public AssignedUserAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AssignedUserAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public AssignedUserAddress getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(AssignedUserAddress vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    public DeleveryModesmodel getDeleveryModesmodel() {
        return deleveryModesmodel;
    }

    public void setDeleveryModesmodel(DeleveryModesmodel deleveryModesmodel) {
        this.deleveryModesmodel = deleveryModesmodel;
    }

    public CustomerModel getCustomerModel() {
        return customerModel;
    }

    public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public PaymentModel getPaymentModel() {
        return paymentModel;
    }

    public void setPaymentModel(PaymentModel paymentModel) {
        this.paymentModel = paymentModel;
    }

    public ArrayList<ProductDetailsModel> getProductlist() {
        return productlist;
    }

    public void setProductlist(ArrayList<ProductDetailsModel> productlist) {
        this.productlist = productlist;
    }

    public void setOrderPickupOTP(String order_pickup_otp) {
        this.order_pickup_otp=order_pickup_otp;
    }

    public String getOrderPickupOTP() {
        return order_pickup_otp;
    }

    public String getOrderDeliveryOTP() {
        return order_delivery_otp;
    }

    public void setOrderDeliveryOTP(String order_delivery_otp) {
        this.order_delivery_otp=order_delivery_otp;
    }

    public void setDeliveryBoyJob(DeliveryJobObject deliveryJobObject) {
        this.deliveryJobObject=deliveryJobObject;
    }

    public DeliveryJobObject getDeliveryBoyJob() {
        return deliveryJobObject;
    }
}
