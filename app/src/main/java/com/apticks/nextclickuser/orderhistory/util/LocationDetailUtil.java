package com.apticks.nextclickuser.orderhistory.util;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class LocationDetailUtil {

    public static void getTravelDistanceFromCurrentLocation(Context context, LatLng origin,
                                                            DirectionListener listener,
                                                            String longitude, String latitude) {
        if (latitude != null && !latitude.equals("null")) {
            LatLng destination = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));

            DirectionUtil.getDirectionData(context, origin, destination, listener);
        } else {
            listener.onDirectionResult(null);
        }
    }

    public static void GetDirectionData(Context context, LatLng origin, LatLng destination, DirectionListener listener) {
        DirectionUtil.getDirectionData(context, origin, destination, listener);
    }

    public static void getTravelDistanceFromCurrentLocation(Context context, LatLng source,
                                                            DirectionListener listener,
                                                            LatLng destination) {
        DirectionUtil.getDirectionData(context, source, destination, listener);

    }
}
