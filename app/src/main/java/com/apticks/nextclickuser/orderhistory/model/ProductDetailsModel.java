package com.apticks.nextclickuser.orderhistory.model;


public class ProductDetailsModel {
    String ecom_order_id,id,item_id,vendor_product_variant_id,qty,price,rate_of_discount,sub_total,discount,tax,total,cancellation_message,
            status;

    ItemModel itemModel;

    public String getEcom_order_id() {
        return ecom_order_id;
    }

    public void setEcom_order_id(String ecom_order_id) {
        this.ecom_order_id = ecom_order_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getVendor_product_variant_id() {
        return vendor_product_variant_id;
    }

    public void setVendor_product_variant_id(String vendor_product_variant_id) {
        this.vendor_product_variant_id = vendor_product_variant_id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRate_of_discount() {
        return rate_of_discount;
    }

    public void setRate_of_discount(String rate_of_discount) {
        this.rate_of_discount = rate_of_discount;
    }

    public String getSub_total() {
        return sub_total;
    }

    public void setSub_total(String sub_total) {
        this.sub_total = sub_total;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCancellation_message() {
        return cancellation_message;
    }

    public void setCancellation_message(String cancellation_message) {
        this.cancellation_message = cancellation_message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ItemModel getItemModel() {
        return itemModel;
    }

    public void setItemModel(ItemModel itemModel) {
        this.itemModel = itemModel;
    }

    Boolean productCheckState = false;
    public boolean getProductCheckState() {
        return productCheckState;
    }
    public void setProductCheckState(Boolean productCheckState) {
        this.productCheckState=productCheckState;
    }
}
