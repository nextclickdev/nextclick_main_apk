package com.apticks.nextclickuser.orderhistory.util;

import android.os.AsyncTask;

import com.apticks.nextclickuser.R;

public class OrderTripStatus {

    public Integer orderVendorStatusID = 103;
    public Integer orderStatusID;
    public String orderStatus;
    private String time;

    public OrderTripStatus(Integer status)
    {
        orderVendorStatusID=status;
    }

    public void ChangeOrderTripStatus(Integer status)
    {
        orderVendorStatusID=status;
    }


    public Integer getorderStatusID() {
        return orderStatusID;
    }

    public void setorderStatusID(Integer title) {
        this.orderStatusID = title;
    }

    public String getorderStatus() {
        return orderStatus;
    }

    public void setorderStatus(String title) {
        this.orderStatus = title;
    }



    public Integer getorderStatusResource() {
        if(orderStatusID == 12 && orderVendorStatusID >= 500 && orderVendorStatusID < 505)
        {
            return R.drawable.ic_baseline_cancel_24;
        }
        else if(orderVendorStatusID >= orderStatusID)
            return R.drawable.ic_accpeted;
        else
            return R.drawable.ic_baseline_cancel_24;//ic_reject;
    }

    Integer disableTextColor=0XFF000000;//0XFFE2E2E2
    Integer enableTextColor=0XFF3F4D5F;//0XFF3F4D5F
    Integer enableBackground=0XFFF26B35;//0XFFF26B35
    public Integer getorderStatusForeground() {
        if(orderStatusID == 12 && orderVendorStatusID >= 500 && orderVendorStatusID < 505)
        {
            return  disableTextColor;
        }
        else if(orderVendorStatusID >= orderStatusID)
            return  enableBackground;//enableTextColor;
        else
            return disableTextColor;
    }


    public Integer getorderStatusBackground() {
        if(orderStatusID == 12 && orderVendorStatusID >= 500 && orderVendorStatusID < 505)
        {
            return  disableTextColor;
        }
        else if(orderVendorStatusID >= orderStatusID)
            return  enableBackground;
        else
            return disableTextColor;
    }

    Boolean status=true;
    public void setDividerStatus(boolean status) {
        this.status=status;
    }
    public  boolean getDividerStatus()
    {
        return  this.status;
    }

    public void setTime(String s) {
        this.time=s;
    }
    public  String getTime()
    {
        return  this.time;
    }
}
