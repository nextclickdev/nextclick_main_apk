package com.apticks.nextclickuser.orderhistory.util;


import com.google.android.gms.maps.model.PolylineOptions;

public interface MapParser {
    public void onPolylineOptionsUpdated(PolylineOptions lineOptions);
}