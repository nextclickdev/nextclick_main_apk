package com.apticks.nextclickuser.orderhistory.model;

public class DeliveryOrderPayment {


    DeliveryOrderPaymentMethod deliveryOrderPaymentMethod;
    public void setDeliveryOrderPaymentMethod(DeliveryOrderPaymentMethod deliveryOrderPaymentMethod) { this.deliveryOrderPaymentMethod = deliveryOrderPaymentMethod; }
    public DeliveryOrderPaymentMethod getDeliveryOrderPaymentMethod() { return deliveryOrderPaymentMethod; }
    String ID;
    public void setID(String ID) { this.ID = ID; }
    public String getID() { return ID; }
    String TransactionID;
    public void setTransactionID(String TransactionID) { this.TransactionID = TransactionID; }
    public String getTransactionID() { return TransactionID; }
    String Amount;
    public void setAmount(String Amount) { this.Amount = Amount; }
    public String getAmount() { return Amount; }
    String CreatedAt;
    public void setCreatedAt(String CreatedAt) { this.CreatedAt = CreatedAt; }
    public String getCreatedAt() { return CreatedAt; }
    String Message;
    public void setMessage(String Message) { this.Message = Message; }
    public String getMessage() { return Message; }
    String Status;
    public void setStatus(String Status) { this.Status = Status; }
    public String getStatus() { return Status; }
    String PaymentMethodID;
    public void setPaymentMethodID(String PaymentMethodID) { this.PaymentMethodID = PaymentMethodID; }
    public String getPaymentMethodID() { return PaymentMethodID; }
}
