package com.apticks.nextclickuser.orderhistory.model;

public class DeliveryOrderStatus {

    String ID;
    public void setID(String ID) { this.ID = ID; }
    public String getID() { return ID; }
    String DeliveryModeID;
    public void setDeliveryModeID(String DeliveryModeID) { this.DeliveryModeID = DeliveryModeID; }
    public String getDeliveryModeID() { return DeliveryModeID; }
    String Status;
    public void setStatus(String Status) { this.Status = Status; }
    public String getStatus() { return Status; }
    String SerialNumber;
    public void setSerialNumber(String SerialNumber) { this.SerialNumber = SerialNumber; }
    public String getSerialNumber() { return SerialNumber; }
}
