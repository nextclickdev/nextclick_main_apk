package com.apticks.nextclickuser.Activities.MultiCatActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.MultiCatAdapters.AmenitiesAdapter;
import com.apticks.nextclickuser.Adapters.MultiCatAdapters.ServicesAdapter;
import com.apticks.nextclickuser.Adapters.ReviewAdapters.ReviewsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.AmenitiesPojo;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.Pojo.Review_POJO.ReviewsPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;
import static com.apticks.nextclickuser.Config.Config.REVIEW_RETRIEVAL;

public class MultiCat_Vendor_Overview extends AppCompatActivity {

    private TextView multicat_vendor_name, multicat_vendor_location, multicat_vendor_primarycontact,
            multicat_vendor_about, multicat_vendor_timings, multicat_vendor_address, multicat_vendor_whatsapp,
            multicat_vendor_landline, multicat_vendor_helpline;
    private RecyclerView multicat_vendor_amenitiesrecycler, multicat_vendor_servicesrecycler;
    private ViewPager multicat_vendor_viewpager;
    private Context mContext;
    private String vendor_id;
    private ArrayList<AmenitiesPojo> amenitiesList;
    private ArrayList<ServicesPojo> servicesList;

    private ArrayList<ReviewsPojo> reviewsList;
    private RecyclerView multicat_vendor_reviewsRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_cat_vendor_overview);
        getSupportActionBar().hide();
        mContext = getApplicationContext();
        vendor_id = getIntent().getStringExtra("vendor_id");
        init();
        detailsFetcher();
        reviewsFetcher();
    }

    private void init() {

        multicat_vendor_name = findViewById(R.id.multicat_overview_vendor_name);
        multicat_vendor_location = findViewById(R.id.multicat_overview_vendor_location);
        multicat_vendor_primarycontact = findViewById(R.id.multicat_overview_vendor_primary_contact);
        multicat_vendor_about = findViewById(R.id.multicat_about_data);
        multicat_vendor_timings = findViewById(R.id.multicat_timings_data);
        multicat_vendor_address = findViewById(R.id.multicat_overview_complete_address_data);
        multicat_vendor_whatsapp = findViewById(R.id.multicat_overview_whatsapp);
        multicat_vendor_landline = findViewById(R.id.multicat_overview_landline);
        multicat_vendor_helpline = findViewById(R.id.multicat_overview_helpline);
        multicat_vendor_amenitiesrecycler = findViewById(R.id.multicat_overview_amenities_recycler);
        multicat_vendor_servicesrecycler = findViewById(R.id.multicat_overview_services_recycler);
        multicat_vendor_viewpager = findViewById(R.id.multicat_vendorItemPager);
        multicat_vendor_reviewsRecycler = findViewById(R.id.multicatVendor_review_recycler);

    }


    private void detailsFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + vendor_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");

                            multicat_vendor_name.setText(dataObject.getString("name"));
                            multicat_vendor_about.setText(dataObject.getString("email"));
                            multicat_vendor_location.setText(dataObject.getString("address"));
                            multicat_vendor_address.setText(dataObject.getJSONObject("location").getString("address"));
                            JSONArray contactsArray = dataObject.getJSONArray("contacts");
                            for (int i = 0; i < contactsArray.length(); i++) {
                                JSONObject contactObject = contactsArray.getJSONObject(i);
                                if (contactObject.getString("type").equalsIgnoreCase("1")) {
                                    multicat_vendor_primarycontact.setText(contactObject.getString("number"));
                                }
                                if (contactObject.getString("type").equalsIgnoreCase("2")) {
                                    multicat_vendor_landline.setText(contactObject.getString("std_code") + " " + contactObject.getString("number"));
                                }
                                if (contactObject.getString("type").equalsIgnoreCase("3")) {
                                    multicat_vendor_whatsapp.setText(contactObject.getString("number"));
                                }
                                if (contactObject.getString("type").equalsIgnoreCase("4")) {
                                    multicat_vendor_helpline.setText(contactObject.getString("number"));
                                }

                            }

                            JSONObject amenitiesObject = dataObject.getJSONObject("amenities");
                            Iterator x = amenitiesObject.keys();
                            JSONArray amenitiesArray = new JSONArray();

                            while (x.hasNext()) {
                                String key = (String) x.next();
                                amenitiesArray.put(amenitiesObject.get(key));
                            }
                            if (amenitiesArray.length() > 0) {
                                amenitiesList = new ArrayList<>();
                                for (int i = 0; i < amenitiesArray.length(); i++) {

                                    JSONObject amenitiesDataObject = amenitiesArray.getJSONObject(i);
                                    AmenitiesPojo amenitiesPojo = new AmenitiesPojo();
                                    amenitiesPojo.setId(amenitiesDataObject.getString("id"));
                                    amenitiesPojo.setList_id(amenitiesDataObject.getString("list_id"));
                                    amenitiesPojo.setName(amenitiesDataObject.getString("name"));
                                    amenitiesList.add(amenitiesPojo);
                                }

                                AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(mContext, amenitiesList);
                                GridLayoutManager layoutManager = new GridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false) {

                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                            private static final float SPEED = 300f;// Change this value (default=25f)

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }

                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }

                                };
                                multicat_vendor_amenitiesrecycler.setLayoutManager(layoutManager);
                                multicat_vendor_amenitiesrecycler.setAdapter(amenitiesAdapter);

                            }

                            JSONObject servicesObject = dataObject.getJSONObject("services");
                            Iterator x1 = servicesObject.keys();
                            JSONArray servicesArray = new JSONArray();

                            while (x1.hasNext()) {
                                String key = (String) x1.next();
                                servicesArray.put(servicesObject.get(key));
                            }
                            if (servicesArray.length() > 0) {
                                servicesList = new ArrayList<>();
                                for (int i = 0; i < servicesArray.length(); i++) {

                                    JSONObject servicesDataObject = servicesArray.getJSONObject(i);
                                    ServicesPojo servicesPojo = new ServicesPojo();
                                    servicesPojo.setId(servicesDataObject.getString("id"));
                                    servicesPojo.setList_id(servicesDataObject.getString("list_id"));
                                    servicesPojo.setName(servicesDataObject.getString("name"));
                                    servicesList.add(servicesPojo);
                                }

                                ServicesAdapter servicesAdapter = new ServicesAdapter(mContext, servicesList);
                                GridLayoutManager layoutManager = new GridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false) {

                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                            private static final float SPEED = 300f;// Change this value (default=25f)

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }

                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }

                                };
                                multicat_vendor_servicesrecycler.setLayoutManager(layoutManager);
                                multicat_vendor_servicesrecycler.setAdapter(servicesAdapter);

                            }


                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);

    }


    public void reviewsFetcher() {

        Map<String, String> maptoparse = new HashMap<>();
        maptoparse.put("vendor_id", vendor_id);
        JSONObject jsonObject = new JSONObject(maptoparse);
        final String dataStr = jsonObject.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REVIEW_RETRIEVAL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {

                                JSONArray dataArray = jsonObject.getJSONArray("data");

                                if (dataArray.length() > 0) {
                                    reviewsList = new ArrayList<>();
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        JSONObject dataObject = dataArray.getJSONObject(i);
                                        ReviewsPojo reviewsPojo = new ReviewsPojo();
                                        reviewsPojo.setReview(dataObject.getString("review"));
                                        reviewsPojo.setRating(dataObject.getString("rating"));
                                        reviewsPojo.setFname(dataObject.getJSONObject("user").getString("first_name"));
                                        reviewsPojo.setLname(dataObject.getJSONObject("user").getString("last_name"));
                                        reviewsPojo.setImage(dataObject.getJSONObject("user").getString("image"));
                                        reviewsList.add(reviewsPojo);
                                    }

                                    ReviewsAdapter reviewsAdapter = new ReviewsAdapter(mContext, reviewsList);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                    multicat_vendor_reviewsRecycler.setLayoutManager(layoutManager);
                                    multicat_vendor_reviewsRecycler.setAdapter(reviewsAdapter);

                                }
                            }
                            else{

                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        requestQueue.add(stringRequest);

    }

}
