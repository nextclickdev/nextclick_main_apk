package com.apticks.nextclickuser.Activities.addtocart;

public class SecItems {
    String id,name,description,price,status,sec_item_status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSec_item_status() {
        return sec_item_status;
    }

    public void setSec_item_status(String sec_item_status) {
        this.sec_item_status = sec_item_status;
    }
}
