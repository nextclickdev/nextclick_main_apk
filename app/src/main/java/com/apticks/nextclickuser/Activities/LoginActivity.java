package com.apticks.nextclickuser.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.utils.NetworkUtil.NetworkManager;
import com.apticks.nextclickuser.utils.NetworkUtil.ResponseDO;
import com.apticks.nextclickuser.utils.NetworkUtil.ResponseListner;
import com.apticks.nextclickuser.utils.NetworkUtil.ServiceMethods;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import com.apticks.nextclickuser.Constants.IErrors;
import com.apticks.nextclickuser.Helpers.Validations;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import retrofit2.Call;
import retrofit2.Callback;

import static android.view.KeyEvent.KEYCODE_DEL;
import static com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity.googleSignInClient;
import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.Constants.APP_ID;
import static com.apticks.nextclickuser.Constants.Constants.APP_ID_VALUE;
import static com.apticks.nextclickuser.Constants.Constants.AUTH_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.FCM_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.USER_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.WARNING;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


//fb


public class LoginActivity extends AppCompatActivity implements ResponseListner {

    PreferenceManager preferenceManager;
    Validations validations;
    Animation animation;
    LoginButton loginButton;
    CallbackManager callbackManager;
    static Context mContext;
    static MainCategoriesActivity mActivity;
    String token,fcm_token;
    private FirebaseAuth mAuth;
    String TAG = "LOGINACTIVITY";
    int RC_SIGN_IN = 101;
    int RC_SIGN_IN_one = 1001;
    // [END declare_auth]

  //  public static GoogleSignInClient mGoogleSignInClient;
    private TextView btn_sendotp,signInButton, registerHere, signUpTextView, click_here, forgot_password, sign_in_button_forget;
    private ImageView gmail_button,gmail_button_one;

    private EditText et_mobileno,userId, password, first_name, last_name, email_id, Mobile_no, reg_password, confirm_password, user_id_forget;
    private RadioButton male_radio_button, female_radio_button;
    private String gender = null;
    private AVLoadingIndicatorView loginProgress, reg_progress;
    CountDownTimer countDownTimer;
    private Dialog dialog;
    private static final String EMAIL = "email";

    public static ImageView fb_button;
    private LinearLayout layout_login_otp,layout_login_password,layout_with_otp,layout_password,layout_otpedittexts;

    EditText[] otpETs = new EditText[6];
    private EditText et_1,et_2,et_3,et_4,et_5,et_6;
    GenericTextWatcher watcher1;
    GenericTextWatcher watcher2;
    GenericTextWatcher watcher3;
    GenericTextWatcher watcher4;
    GenericTextWatcher watcher5;
    GenericTextWatcher watcher6;

    //Collections
    Map<String, String> mapToParse = new HashMap<>();

    public LoginActivity(Context mContext) {
        this.mContext = mContext;
        //alertDialogOrderPlaced();
    }

    public LoginActivity(Context mContext, MainCategoriesActivity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        //alertDialogOrderPlaced();
    }
    private void login(Map<String, String> json) {
        DialogOpener.dialogOpener(mContext);
        Map<String, String> map = new HashMap<>();
     //   map.put("Content-Type", "application/json");
        map.put(APP_ID, APP_ID_VALUE);
        try {
            dialog.dismiss();
            new NetworkManager(mContext, this).callWebServices(ServiceMethods.WS_LOGIN, map, json);
        }
        catch (Exception ex){
            DialogOpener.dialogOpener(mContext);
            System.out.println("aaaaaaaa fail to login "+ex.toString());
            Log.e("fail","fail");
        }
    }
    private void grantFCMPermissionretro(String token){
        DialogOpener.dialogOpener(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", token);

        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put(APP_ID, APP_ID_VALUE);
        map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
        try {
            new NetworkManager(mContext, this).callWebServices(ServiceMethods.FCM, map, fcmMap);
        }
        catch (Exception ex){
            Log.e("fail","fail");
        }
    }
    @Override
    public void onResponseReceived(ResponseDO responseDO) {
        switch (responseDO.getServiceMethods())
        {
            case WS_LOGIN:
                if(!responseDO.isError())
                {
                    parseLoginResponse(responseDO);
                }
                else
                {
                    DialogOpener.dialog.dismiss();
                    System.out.println("aaaaaaa catch loginres "+responseDO.getResponse());
                    UImsgs.showToast(mContext,  responseDO.getResponse());
                }
                break;
            case FCM:
                if(!responseDO.isError())
                {
                    parseFcmResponse(responseDO);
                }
                else
                {
                    DialogOpener.dialog.dismiss();
                    System.out.println("aaaaaaa catch fcmres "+responseDO.getResponse());
                    UImsgs.showToast(mContext,  responseDO.getResponse());
                }
                break;
            case FORGOT_PASSWORD:
                if(!responseDO.isError())
                {
                    parseForgotpasswordResponse(responseDO);
                }
                else
                {
                    DialogOpener.dialog.dismiss();
                    System.out.println("aaaaaaa catch forgotres "+responseDO.getResponse());
                    UImsgs.showToast(mContext,  responseDO.getResponse());
                }
                break;
        }
    }

    private void parseForgotpasswordResponse(ResponseDO responseDO) {
        if (responseDO != null) {
            try {
                DialogOpener.dialog.dismiss();
                JSONObject jsonObject = new JSONObject(responseDO.getResponse());
                boolean status = jsonObject.getBoolean("status");
                if (status) {
                    UImsgs.showToast(mContext, ""+jsonObject.getString("message").toString());
                } else {
                    UImsgs.showToast(mContext, jsonObject.getString("message"));
                }
            }
            catch (Exception e) {
                System.out.println("aaaaaaa catch forgot "+e.getMessage());
                DialogOpener.dialog.dismiss();
                UImsgs.showToast(mContext, "Something went wrong");
                e.printStackTrace();
            }
        } else {
            DialogOpener.dialog.dismiss();
            UImsgs.showToast(mContext, "Server under maintenance");
        }
    }

    private void parseFcmResponse(ResponseDO responseDO){
        if (responseDO != null) {
            try {
                DialogOpener.dialog.dismiss();
                JSONObject jsonObject = new JSONObject(responseDO.getResponse());
                boolean status = jsonObject.getBoolean("status");
                if (status) {
                 //   preferenceManager.putString(FCM_TOKEN, fcm_token);

                } else {
                    Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                }
            }
           catch (Exception e) {
                    System.out.println("aaaaaaa catch fem "+e.getMessage());
                    DialogOpener.dialog.dismiss();
                    UImsgs.showToast(mContext, "Something went wrong");
                    e.printStackTrace();
                }
            } else {
                DialogOpener.dialog.dismiss();
                UImsgs.showToast(mContext, "Server under maintenance");
            }
    }

    private void parseLoginResponse(ResponseDO responseDO){
        if (responseDO != null) {
            System.out.println("aaaaa  response "+responseDO.getResponse());
            try {
                DialogOpener.dialog.dismiss();
                JSONObject jsonObject = new JSONObject(responseDO.getResponse());
                System.out.println("aaaaaaaaa jsonobject "+jsonObject.toString());
                boolean status = jsonObject.getBoolean("status");
                if (status) {
                    String message = jsonObject.getString("message");
                    JSONObject dataObject = jsonObject.getJSONObject("data");
                    String token = dataObject.getString("token");
                    Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();
                    preferenceManager.putString(TOKEN_KEY, token);
                    try {
                        FirebaseApp.initializeApp(mContext);
                        FirebaseInstanceId.getInstance().getInstanceId()
                                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                        if (!task.isSuccessful()) {
                                            return;
                                        }
                                        fcm_token = task.getResult().getToken();
                                      //  String msg = getString(R.string.fcm_token, fcm_token);
                                        Log.d("TAG", fcm_token);
                                        grantFCMPermissionretro(fcm_token);
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                        UImsgs.showToast(mContext, "Firebase exception "+e.getMessage());
                    }
                } else {
                    Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                System.out.println("aaaaaaa catch login "+e.getMessage());
                DialogOpener.dialog.dismiss();
                UImsgs.showToast(mContext, "Something went wrong");
                e.printStackTrace();
            }
        } else {
            DialogOpener.dialog.dismiss();
            UImsgs.showToast(mContext, "Server under maintenance");
        }

    }

    public class GenericTextWatcher implements TextWatcher, View.OnKeyListener {
        private View view;
        String previousText = "";

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.et_1:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            et_1.removeTextChangedListener(watcher1);
                            et_1.setText(previousText);
                            et_1.addTextChangedListener(watcher1);

                            et_2.removeTextChangedListener(watcher2);
                            et_2.setText(text);
                            et_2.addTextChangedListener(watcher2);
                        }
                        et_2.requestFocus();
                    }
                    break;
                case R.id.et_2:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            et_2.removeTextChangedListener(watcher2);
                            et_2.setText(previousText);
                            et_2.addTextChangedListener(watcher2);

                            et_3.removeTextChangedListener(watcher3);
                            et_3.setText(text);
                            et_3.addTextChangedListener(watcher3);

                        }
                        et_3.requestFocus();

                    } else if (text.length() == 0)
                        et_1.requestFocus();
                    break;
                case R.id.et_3:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            et_3.removeTextChangedListener(watcher3);
                            et_3.setText(previousText);
                            et_3.addTextChangedListener(watcher3);

                            et_4.removeTextChangedListener(watcher4);
                            et_4.setText(text);
                            et_4.addTextChangedListener(watcher4);
                        }
                        et_4.requestFocus();
                    } else if (text.length() == 0)
                        et_2.requestFocus();
                    break;
                case R.id.et_4:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            et_4.removeTextChangedListener(watcher4);
                            et_4.setText(previousText);
                            et_4.addTextChangedListener(watcher4);

                            et_5.removeTextChangedListener(watcher5);
                            et_5.setText(text);
                            et_5.addTextChangedListener(watcher5);
                        }
                        et_5.requestFocus();
                    } else if (text.length() == 0)
                        et_3.requestFocus();
                    break;
                case R.id.et_5:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            et_5.removeTextChangedListener(watcher5);
                            et_5.setText(previousText);
                            et_5.addTextChangedListener(watcher5);

                            et_6.removeTextChangedListener(watcher6);
                            et_6.setText(text);
                            et_6.addTextChangedListener(watcher6);
                        }
                        et_6.requestFocus();
                    } else if (text.length() == 0)
                        et_4.requestFocus();
                    break;
                case R.id.et_6:
                    if (text.length() == 0) {

                        et_5.requestFocus();
                    } else {
                        try {
                            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            Log.e(TAG, "afterTextChanged: hide keyboard");
                        } catch (Exception e) {
                            Log.e(TAG, "afterTextChanged: " + e.toString());
                        }
                    }


                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            Log.d(TAG, "beforeTextChanged: " + arg0);
            if (arg0.length() > 0) {
                previousText = arg0.toString();
            }
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            previousText = "";
            Log.d(TAG, "onKey: keyCode = " + keyCode + ", event = " + event.toString());
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.et_2:
                        if (et_2.getText().toString().trim().length() == 0)
                            et_1.requestFocus();
                        break;
                    case R.id.et_3:
                        if (et_3.getText().toString().trim().length() == 0)
                            et_2.requestFocus();
                        break;
                    case R.id.et_4:
                        if (et_4.getText().toString().trim().length() == 0)
                            et_3.requestFocus();
                        break;
                    case R.id.et_5:
                        if (et_5.getText().toString().trim().length() == 0)
                            et_4.requestFocus();
                        break;
                    case R.id.et_6:
                        if (et_6.getText().toString().trim().length() == 0)
                            et_5.requestFocus();
                        else if (et_6.getText().toString().trim().length() == 1)
                            try {

                              //  ((BaseActivity) getActivity()).hideSoftKeyboard();
                            } catch (Exception e) {
                                Log.e(TAG, "afterTextChanged: " + e.toString());
                            }
                        break;
                }

            }
            return false;
        }
    }

    private void forgetPasswordClick(MainCategoriesActivity mActivity, CallbackManager callbackManager) {

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setContentView(R.layout.dialog_forget_password);
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                sign_in_button_forget = dialog.findViewById(R.id.sign_in_button_forget);
                user_id_forget = dialog.findViewById(R.id.user_id_forget);

                sign_in_button_forget.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validEmailForget()) {
                            signInButtonForgetretrofit();
                        }
                    }
                });

              //  signInButtonForget();

            }
        });
    }

    private void signInButtonForgetretrofit() {
        Map<String, String> mapToParse = new HashMap<>();
        mapToParse.put("identity", user_id_forget.getText().toString().trim());

        DialogOpener.dialogOpener(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("identity", user_id_forget.getText().toString().trim());

        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put(APP_ID, APP_ID_VALUE);
        map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
        try {
            new NetworkManager(mContext, this).callWebServices(ServiceMethods.FORGOT_PASSWORD, null, fcmMap);
        }
        catch (Exception ex){
            Log.e("fail","fail");
        }
    }

    private Map<String, String> fetcherForgetPassword() {
        mapToParse.put("identity", user_id_forget.getText().toString().trim());
        return mapToParse;
    }

    private void signInButtonForget() {
        sign_in_button_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validEmailForget()) {
                    JSONObject jsonObject = new JSONObject(fetcherForgetPassword());
                    final String data = jsonObject.toString();
                    RequestQueue requestQueue = Volley.newRequestQueue(mContext);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, FORGOT_PASSWORD, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObjectResponce = new JSONObject(response);
                                if (jsonObjectResponce.getBoolean("status")) {
                                    UImsgs.showSnackBar(v, String.valueOf(Html.fromHtml(jsonObjectResponce.getString("message").toString())));
                                } else if (!jsonObjectResponce.getBoolean("status")) {
                                    UImsgs.showSnackBar(v, jsonObjectResponce.getString("data"));
                                }
                            } catch (JSONException e) {
                                UImsgs.showCustomToast(mContext, "Server Under Maintenance ", INFO);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Toast.makeText(mContext, "Error " + error, Toast.LENGTH_SHORT).show();

                        }
                    }) {

                        @Override
                        public String getBodyContentType() {
                            return "application/json";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return data == null ? null : data.getBytes("utf-8");

                            } catch (Exception e) {
                                e.printStackTrace();
                                return null;
                            }
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);


                }

            }
        });
    }

    private boolean validEmailForget() {
        if (validations.isBlank(user_id_forget)) {
            user_id_forget.startAnimation(animation);
            user_id_forget.setError(IErrors.EMPTY);
            user_id_forget.requestFocus();
            return false;
        }
        return true;
    }

    // [START auth_with_facebook]
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        // [START_EXCLUDE silent]
        //showProgressBar();
        // [END_EXCLUDE]

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(mActivity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                        // hideProgressBar();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_facebook]

    public void dialogDismiss() {
        dialog.dismiss();
    }


    // [START on_start_check_user]
   /* @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        //FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            Log.d(TAG, "Currently Signed in: " + currentUser.getEmail());
            showToastMessage("Currently Logged in: " + currentUser.getEmail());
        }
    }*/
    // [END on_start_check_user]

    // [START onactivityresult]



    private void callLoginAPI(JSONObject jsonObject) {
        signInButton.setVisibility(View.GONE);
        loginProgress.setVisibility(View.VISIBLE);
        final String data = jsonObject.toString();
        System.out.println("aaaaaaaaaa  data  "+jsonObject);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObjectStatus = new JSONObject(response.toString());
                    String status = jsonObjectStatus.getString("status");
                    String messagae = jsonObjectStatus.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Login SuccessFully.!")) {

                        dialog.dismiss();
                        JSONObject dataObject = jsonObjectStatus.getJSONObject("data");
                        String token = dataObject.getString("token");
                        System.out.println("aaaaaaaaaa  response  "+dataObject.toString());
                        //UImsgs.showToast(mContext, token);
                        preferenceManager.putString(TOKEN_KEY, token);
                        try {
                            FirebaseInstanceId.getInstance().getInstanceId()
                                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                            if (!task.isSuccessful()) {
//To do//
                                                return;
                                            }

// Get the Instance ID token//
                                            String token = task.getResult().getToken();
                                            //String msg = getString(R.string.fcm_token, token);
                                            Log.d("TAG", token);
                                            grantFCMPermission(token);

                                        }
                                    });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        loginProgress.setVisibility(View.GONE);
                        signInButton.setVisibility(View.VISIBLE);


                    } else {
                        //dialog.dismiss();
                        UImsgs.showToast(mContext, messagae);
                        loginProgress.setVisibility(View.GONE);
                        signInButton.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //UImsgs.showToast(mContext,response);
                //dialog.dismiss();
                //dialog.cancel();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, String.valueOf(error));
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }


            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void sendOtp(final String msg) {

        DialogOpener.dialogOpener(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", msg);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("aaaaaaa response "+response.toString());
                if (response != null) {
                    try {
                        DialogOpener.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            String data=jsonObject.getString("data");
                          //  Toast.makeText(mActivity, ""+data, Toast.LENGTH_SHORT).show();
                            Intent i=new Intent(mActivity,OtpActivity.class);
                            i.putExtra("mobileno",et_mobileno.getText().toString().trim());
                            i.putExtra("data",data);
                            mActivity.startActivity(i);
                            et_mobileno.setText("");
                            dialog.dismiss();
                            finish();
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        DialogOpener.dialog.dismiss();
                        UImsgs.showCustomToast(mContext, "Something went wrong", WARNING);
                        e.printStackTrace();
                    }
                } else {
                    DialogOpener.dialog.dismiss();
                    UImsgs.showCustomToast(mContext, "Server under maintenance", INFO);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
              //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
              //  map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void grantFCMPermission(final String msg) {

        DialogOpener.dialogOpener(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", msg);
        System.out.println("aaaaaaaaa token  "+token);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                FCM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("fcm_res", response);
                if (response != null) {
                    try {
                        DialogOpener.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {

                            preferenceManager.putString(FCM_TOKEN, msg);


                            /*startActivity(new Intent(mContext,ServicesActivity.class));
                            finish();*/
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        DialogOpener.dialog.dismiss();
                        UImsgs.showCustomToast(mContext, "Something went wrong", WARNING);
                        e.printStackTrace();
                    }
                } else {
                    DialogOpener.dialog.dismiss();
                    UImsgs.showCustomToast(mContext, "Server under maintenance", INFO);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private boolean isValidInput() {
        if (validations.isBlank(userId)) {
            userId.startAnimation(animation);
            userId.setError(IErrors.EMPTY);
            userId.requestFocus();
            return false;
        } /*else if (validations.isValidEmail(userId.getText().toString())) {
            userId.startAnimation(animation);
            userId.setError(IErrors.INVALID_EMAIL);
            userId.requestFocus();
            return false;

        } */ else if (validations.isBlank(password)) {
            password.startAnimation(animation);
            password.setError(IErrors.PASSWORD_EMPTY);
            password.requestFocus();
            return false;
        }/*else if (validations.)*/
        return true;
    }

    public Map<String, String> fetcher() {

        mapToParse.put("identity", userId.getText().toString().trim());
        mapToParse.put("password", password.getText().toString().trim());
        return mapToParse;
    }


    public static void logOut(Context context) {
        googleSignInClient.signOut().addOnCompleteListener(mActivity,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        UImsgs.showCustomToast(mActivity, "Successfully Logged Out", INFO);
                    }
                });
        context.startActivity(new Intent(context, MainCategoriesActivity.class));


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("aaaaaaa  requestcode  "+requestCode);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                showToastMessage("Google Sign in Succeeded");
                System.out.println("aaaaaaa acount  "+account.getDisplayName()+"  email  "+account.getEmail());
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                showToastMessage("Google Sign in Failed " + e);
            }
        }
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        /*if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // [START_EXCLUDE]
                //updateUI(null);
                // [END_EXCLUDE]
            }
        }*/

        /*// Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);*/
    }
    // [END onactivityresult]

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        //showProgressBar();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            //Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                        //hideProgressBar();
                        // [END_EXCLUDE]
                    }
                });
    }


    private void showToastMessage(String message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
    }
    // [END auth_with_google]

    // [START signin]
    private void signIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        mActivity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signin]

    private void registerHereClick(MainCategoriesActivity mActivity, CallbackManager callbackManager) {
        registerHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mContext.startActivity(new Intent(mContext,SignUpActivity.class));

                dialog.setContentView(R.layout.activity_sign_up);
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                first_name = dialog.findViewById(R.id.first_name);
                last_name = dialog.findViewById(R.id.last_name);
                email_id = dialog.findViewById(R.id.email_id);
                Mobile_no = dialog.findViewById(R.id.Mobile_no);
                male_radio_button = dialog.findViewById(R.id.male_radio_button);
                female_radio_button = dialog.findViewById(R.id.female_radio_button);
                reg_password = dialog.findViewById(R.id.reg_password);
                confirm_password = dialog.findViewById(R.id.confirm_password);
                signUpTextView = dialog.findViewById(R.id.sign_up_text_view);
                click_here = dialog.findViewById(R.id.click_here);

                reg_progress = dialog.findViewById(R.id.reg_progress);


               // first_name.setFilters(new InputFilter[]{EMOJI_FILTER});
             //   last_name.setFilters(new InputFilter[]{EMOJI_FILTER});


                signUpTextViewClick();

                clickHereLogin(mActivity, callbackManager);


                //Intent intent = new Intent(get)

            }
        });
    }

    private void clickHereLogin(MainCategoriesActivity mActivity, CallbackManager callbackManager) {
        click_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogLogin(mActivity, callbackManager);
            }
        });
    }

    private void signUpTextViewClick() {
        signUpTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reg_progress.setVisibility(View.VISIBLE);
                signUpTextView.setVisibility(View.GONE);
                register(v);
            }
        });
    }

    public void register(View view) {
        if (isValidInputReg()) {
            hitRegistartionAPI(view);
        } else {
            //Toast.makeText(mContext, "Else", Toast.LENGTH_SHORT).show();
            reg_progress.setVisibility(View.GONE);
            signUpTextView.setVisibility(View.VISIBLE);
        }
    }

    private void hitRegistartionAPI(View view) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        Log.d("reg url", CreateUsers);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CreateUsers, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //UImsgs.showToast(mContext, "asd" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("aaaaaaaa response  "+jsonObject.toString());
                    if (jsonObject.getBoolean("status") && jsonObject.getInt("http_code") == 200) {
                        //UImsgs.showToast(mContext, jsonObject.getString("message"));
                        UImsgs.showToast(mContext, "Account created successfully.");//jsonObject.getString("message")
                        reg_progress.setVisibility(View.GONE);
                        signUpTextView.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                    } else {
                        JSONObject jsonObject1 = new JSONObject(response);
                        String message = jsonObject1.getString("message");
                        UImsgs.showToast(mContext, message);
                        //UImsgs.showToast(mContext, "Sorry for inconvenience! Registration not possible");
                        reg_progress.setVisibility(View.GONE);
                        signUpTextView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "Unable to register please try after some time");
                    reg_progress.setVisibility(View.GONE);
                    signUpTextView.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, "Unable to register please try after some time");
                reg_progress.setVisibility(View.GONE);
                signUpTextView.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("first_name", first_name.getText().toString());
                params.put("last_name", last_name.getText().toString());
                params.put("mobile", Mobile_no.getText().toString());
                params.put("gender", gender);
                params.put("email", email_id.getText().toString());
                params.put("password", reg_password.getText().toString());
                params.put("confirm_password", confirm_password.getText().toString());
                System.out.println("aaaaaaa create user  "+params.toString());
                return params;
            }
        };
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private boolean isValidInputReg() {

        if (male_radio_button.isChecked()) {
            gender = "Male";
        }
        if (female_radio_button.isChecked()) {
            gender = "FeMale";
        }

        if (validations.isBlank(first_name)) {
            first_name.startAnimation(animation);
            first_name.setError(IErrors.EMPTY);
            first_name.requestFocus();
            return false;
        } else if (validations.isValidEmailID(email_id.getText().toString())) {
            email_id.startAnimation(animation);
            email_id.setError(IErrors.INVALID_EMAIL);
            email_id.requestFocus();
            return false;
        } else if (validations.isValidPhone(Mobile_no.getText().toString())) {
            Mobile_no.startAnimation(animation);
            Mobile_no.setError(IErrors.MOBILE_INVALID);
            Mobile_no.requestFocus();
            return false;
        } else if (validations.isBlank(reg_password.getText().toString())) {
            reg_password.startAnimation(animation);
            reg_password.setError(IErrors.PASSWORD_EMPTY);
            reg_password.requestFocus();
            return false;
        } else if (validations.isBlank(confirm_password.getText().toString())) {
            confirm_password.startAnimation(animation);
            confirm_password.setError(IErrors.PASSWORD_EMPTY);
            confirm_password.requestFocus();
            return false;
        } else if (!validations.isMatching(reg_password, confirm_password)) {
            reg_password.startAnimation(animation);
            reg_password.setError(IErrors.PASSWORD_SAME);
            reg_password.requestFocus();

            confirm_password.startAnimation(animation);
            confirm_password.setError(IErrors.PASSWORD_SAME);
            confirm_password.requestFocus();

            return false;
        } else if (gender == null) {
            UImsgs.showCustomToast(mContext, "Please provide gender", WARNING);
            return false;
        }
        return true;
    }

    private void fbButtonClick() {
        fb_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void gmailButtonClick() {
        gmail_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        }); gmail_button_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
    }

    private void signInButtonClick() {
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidInput()) {
                    mapToParse.put("identity", userId.getText().toString().trim());
                    mapToParse.put("password", password.getText().toString().trim());
                     JSONObject json = new JSONObject(fetcher());
                        callLoginAPI(json);
                  //  login(mapToParse);
                }

            }
        });
    }

    public void alertDialogLogin(MainCategoriesActivity mActivity, CallbackManager callbackManager) {

        if(dialog==null)
            dialog = new Dialog(mActivity);
        Window window = dialog.getWindow();
        /*WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);*/
        preferenceManager = new PreferenceManager(mActivity);
        validations = new Validations();
        animation = AnimationUtils.loadAnimation(mActivity, R.anim.shake);
        dialog.setContentView(R.layout.activity_login);
        //dialog.onBackPressed();
        //dialog.getWindow();
        //dialog.
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        //dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;

        dialog.setCancelable(true);
        loginButton = dialog.findViewById(R.id.fblogin_button);
        signInButton = dialog.findViewById(R.id.sign_in_button);
        registerHere = dialog.findViewById(R.id.register_here);
        forgot_password = dialog.findViewById(R.id.forgot_password);
        userId = dialog.findViewById(R.id.user_id);
        password = dialog.findViewById(R.id.password);
        layout_login_otp = dialog.findViewById(R.id.layout_login_otp);
        layout_login_password = dialog.findViewById(R.id.layout_login_password);
        et_mobileno = dialog.findViewById(R.id.et_mobileno);
        btn_sendotp = dialog.findViewById(R.id.btn_sendotp);
        layout_with_otp = dialog.findViewById(R.id.layout_with_otp);
        layout_password = dialog.findViewById(R.id.layout_password);
        layout_otpedittexts = dialog.findViewById(R.id.layout_otpedittexts);

        loginProgress = dialog.findViewById(R.id.login_progress);
        gmail_button = dialog.findViewById(R.id.gmail_button);
        gmail_button_one = dialog.findViewById(R.id.gmail_button_one);

        FacebookSdk.sdkInitialize(mActivity.getApplicationContext());
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();

        et_1 = dialog.findViewById(R.id.et_1);
        et_2 = dialog.findViewById(R.id.et_2);
        et_3 = dialog.findViewById(R.id.et_3);
        et_4 = dialog.findViewById(R.id.et_4);
        et_5 = dialog.findViewById(R.id.et_5);
        et_6 = dialog.findViewById(R.id.et_6);

        watcher1 = new GenericTextWatcher(et_1);
        watcher2 = new GenericTextWatcher(et_2);
        watcher3 = new GenericTextWatcher(et_3);
        watcher4 = new GenericTextWatcher(et_4);
        watcher5 = new GenericTextWatcher(et_5);
        watcher6 = new GenericTextWatcher(et_6);
        et_1.addTextChangedListener(watcher1);
        et_1.setOnKeyListener(watcher1);
        et_2.addTextChangedListener(watcher2);
        et_2.setOnKeyListener(watcher2);
        et_3.addTextChangedListener(watcher3);
        et_3.setOnKeyListener(watcher3);
        et_4.addTextChangedListener(watcher4);
        et_4.setOnKeyListener(watcher4);
        et_5.addTextChangedListener(watcher5);
        et_5.setOnKeyListener(watcher5);
        et_6.addTextChangedListener(watcher6);
        et_6.setOnKeyListener(watcher6);

        //fb button
        layout_login_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_with_otp.setVisibility(View.VISIBLE);
                layout_password.setVisibility(View.GONE);
            }
        }); layout_login_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_with_otp.setVisibility(View.GONE);
                layout_password.setVisibility(View.VISIBLE);
                try {
                    countDownTimer.cancel();
                }catch (NullPointerException e){

                }
                layout_otpedittexts.setVisibility(View.GONE);
                et_mobileno.setText("");
                et_1.setText("");
                et_2.setText("");
                et_3.setText("");
                et_4.setText("");
                et_5.setText("");
                et_6.setText("");
                btn_sendotp.setText("Send Otp");
            }
        });

       /* try{
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.apticks.nextclickuser",
                    PackageManager.GET_SIGNATURES);
            for(Signature signature: packageInfo.signatures){
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.d("Key Hash", Base64.encodeToString(messageDigest.digest(),Base64.DEFAULT));
            }
        }catch (NullPointerException | PackageManager.NameNotFoundException | NoSuchAlgorithmException e){
            e.printStackTrace();
        }*/

//        AppEventsLogger.activateApp(mActivity);
        // callbackManager = CallbackManager.Factory.create();


        fb_button = dialog.findViewById(R.id.fb_button);
       /* fb_button.setReadPermissions(Arrays.asList(EMAIL));
        // Callback registration

        fb_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                Log.d(TAG, "facebook:onCancel");
                // [START_EXCLUDE]
                //updateUI(null);
                // [END_EXCLUDE]
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, "facebook:onError", exception);
            }
        });


        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.d(TAG, "facebook:loginmanager"+loginResult.toString());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d(TAG, "facebook:loginmanager cancelled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d(TAG, "facebook:loginmanager",exception);
                    }
                });*/

       /* GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(CLIENT_ID)//request Id Token needed
                .requestEmail()
                .build();
        // [END config_signin]


        mGoogleSignInClient = GoogleSignIn.getClient(mActivity, gso);*/

       /* GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(mActivity, gso);
*/

        // [START initialize_auth]
        //Initialize Firebase Auth
        //mAuth = FirebaseAuth.getInstance();

        signInButtonClick();//Sign IN
        registerHereClick(mActivity, callbackManager);// New Registaration
        //fbButtonClick();
        gmailButtonClick();
        forgetPasswordClick(mActivity, callbackManager);
        btn_sendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   mActivity.startActivity(new Intent(mActivity,SigninActivity.class));
                if (isValidPhoneNumber(et_mobileno.getText().toString().trim())){
                    if (et_mobileno.getText().toString().length()==10){

                        sendOtp(et_mobileno.getText().toString().trim());

                       /* layout_otpedittexts.setVisibility(View.VISIBLE);
                        if (btn_sendotp.getText().toString().equalsIgnoreCase("  Resend Otp  ") ||
                                btn_sendotp.getText().toString().equalsIgnoreCase("Send Otp")){
                            countDownTimer=  new CountDownTimer(30000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    btn_sendotp.setText("Resends : " + millisUntilFinished / 1000+"(s)");
                                    //here you can have your logic to set text to edittext
                                }

                                public void onFinish() {
                                    btn_sendotp.setText("  Resend Otp  ");
                                }

                            }.start();
                        }*/

                    }else {
                        Toast.makeText(mActivity, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                    }

                }else {
                    Toast.makeText(mActivity, "Please enter currect mobile number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
    }
    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }


    public static InputFilter EMOJI_FILTER = new InputFilter() {
        private String blockCharacterSet = "~#^|$%&*!";

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE) {
                    return "";
                }
                else if (source != null && blockCharacterSet.contains(("" + source))) {
                    return "";
                }
            }
            return null;
        }
    };
}
