package com.apticks.nextclickuser.Activities.VENDOR_SUB_CATEGORIES;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.VENDORSADAPTERS.VendorSubCatAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.LocationModel;
import com.apticks.nextclickuser.Pojo.Timmings;
import com.apticks.nextclickuser.Pojo.VendorSubCatPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.AddCartActivity;
import com.apticks.nextclickuser.products.activities.CartAddActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.Constants.WARNING;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;

public class VendorsSubCategoriesActivity extends AppCompatActivity {

    private String vendor_id,vendor_user_id;
    private ArrayList<VendorSubCatPojo> vendorSubCatPojos;
    private RecyclerView vendor_sub_cat_recycler;
    private ImageView back_imageView;
    Context mContext;
    private FrameLayout cart_layout;
    private ArrayList<Timmings> timelist;
    private TextView tv_timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendors_sub_categories);
        getSupportActionBar().hide();
        vendor_id = getIntent().getStringExtra(VENDOR_ID);
        vendor_user_id = getIntent().getStringExtra(vendorUserId);
     //   Log.d("ven subcat ven id",vendor_user_id);
     //   System.out.println("aaaaaaa vendorid "+vendor_user_id);
        mContext = VendorsSubCategoriesActivity.this;
        timelist=new ArrayList<>();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            }
        }*/

        init();
        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    private void init() {
        TextView tv_header= findViewById(R.id.tv_header);
        tv_header.setText("Shop By Category");
        tv_header.setAllCaps(true);
        vendor_sub_cat_recycler = findViewById(R.id.vendor_sub_cat_recycler);
        back_imageView = findViewById(R.id.img_back);
        cart_layout = findViewById(R.id.cart_layout);
        tv_timer = findViewById(R.id.tv_timer);
        profileFetcher();

      /*  Calendar start_calendar = Calendar.getInstance();
        Calendar end_calendar = Calendar.getInstance();

       String time= timelist.get(0).getEnd_time();
        String namepass[] = time.split(":");
        String hour = namepass[0];
        String minute = namepass[1];
        String seconds = namepass[2];

        end_calendar.add(Calendar.HOUR, Integer.parseInt(hour));
        end_calendar.add(Calendar.MINUTE, Integer.parseInt(minute));
        end_calendar.add(Calendar.SECOND, Integer.parseInt(seconds));

        long start_millis = start_calendar.getTimeInMillis();
        long end_millis = end_calendar.getTimeInMillis();
        long total_millis = (end_millis - start_millis);
        end_calendar.set(start_calendar.get(Calendar.YEAR), start_calendar.get(Calendar.MONTH), start_calendar.DAY_OF_MONTH);

        CountDownTimer cdt = new CountDownTimer(total_millis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                millisUntilFinished -= TimeUnit.DAYS.toMillis(days);

                long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

                long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                tv_timer.setText(days + ":" + hours + ":" + minutes + ":" + seconds); //You can compute the millisUntilFinished on hours/minutes/seconds
            }

            @Override
            public void onFinish() {
                tv_timer.setText("Finish!");
            }
        };
        cdt.start();
*/

        cart_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  startActivity(new Intent(mContext, AddCartActivity.class));
                startActivity(new Intent(mContext, CartAddActivity.class));
            }
        });
    }


    private void profileFetcher() {
        DialogOpener.dialogOpener(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(VendorsSubCategoriesActivity.this);
        Log.d("url", INDIVIDUAL_VENDOR + vendor_id);
        System.out.println("aaaaaaaaa url vendorsub "+INDIVIDUAL_VENDOR + vendor_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + vendor_id, new Response.Listener<String>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(String response) {
                Log.d("vendor response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {

                       try {
                          // String vendor_useeeer_id=jsonObject.getString("vendor_user_id");
                           JSONObject dataObject = jsonObject.getJSONObject("data");
                           System.out.println("aaaaaaaa  response sub "+dataObject.toString());

                           try {
                               try{
                                   JSONArray timingsarray = dataObject.getJSONArray("timings");
                                   for (int i=0;i<timingsarray.length();i++){
                                       JSONObject jsonObject1=timingsarray.getJSONObject(i);
                                       Timmings timmings=new Timmings();
                                       timmings.setListid(jsonObject1.getString("list_id"));
                                       timmings.setStart_time(jsonObject1.getString("start_time"));
                                       timmings.setEnd_time(jsonObject1.getString("end_time"));

                                       timelist.add(timmings);
                                   }
                               }catch (JSONException e){

                               }


                               JSONArray shopByCategoryArray = dataObject.getJSONArray("shop_by_categories");
                               if (shopByCategoryArray.length() > 0) {
                                   vendorSubCatPojos = new ArrayList<>();

                                   for (int i = 0; i < shopByCategoryArray.length(); i++) {
                                       VendorSubCatPojo vendorSubCatPojo = new VendorSubCatPojo();
                                       JSONObject subCatObject = shopByCategoryArray.getJSONObject(i);
                                       vendorSubCatPojo.setId(subCatObject.getString("id"));
                                       vendorSubCatPojo.setList_id(subCatObject.getString("cat_id"));
                                       vendorSubCatPojo.setName(subCatObject.getString("name"));
                                       vendorSubCatPojo.setImage(subCatObject.getString("image"));
                                       vendorSubCatPojo.setVendor_name(dataObject.getString("name"));
                                       JSONObject jsonObject1=dataObject.getJSONObject("location");
                                       LocationModel locationModel=new LocationModel();
                                       locationModel.setId(jsonObject1.getString("id"));
                                       locationModel.setAddress(jsonObject1.getString("address"));
                                       locationModel.setLatitude(jsonObject1.getString("latitude"));
                                       locationModel.setLongitude(jsonObject1.getString("longitude"));
                                       vendorSubCatPojo.setLocationModel(locationModel);
                                       vendorSubCatPojos.add(vendorSubCatPojo);
                                   }
                                   VendorSubCatAdapter subCatAdapter = new VendorSubCatAdapter(VendorsSubCategoriesActivity.this, vendorSubCatPojos,vendor_user_id);
                                   GridLayoutManager layoutManager = new GridLayoutManager(VendorsSubCategoriesActivity.this, 3, GridLayoutManager.VERTICAL, false) {

                                       @Override
                                       public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                           LinearSmoothScroller smoothScroller = new LinearSmoothScroller(VendorsSubCategoriesActivity.this) {

                                               private static final float SPEED = 300f;// Change this value (default=25f)

                                               @Override
                                               protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                   return SPEED / displayMetrics.densityDpi;
                                               }

                                           };
                                           smoothScroller.setTargetPosition(position);
                                           startSmoothScroll(smoothScroller);
                                       }

                                   };
                                   vendor_sub_cat_recycler.setLayoutManager(layoutManager);
                                   vendor_sub_cat_recycler.setAdapter(subCatAdapter);

                               }else{
                                   UImsgs.showCustomToast(mContext,"No category available",WARNING);
                               }
                           }catch (Exception e){
                               e.printStackTrace();
                               UImsgs.showCustomToast(getApplicationContext(), "Oops! Something went wrong with this vendor",WARNING);
                               onBackPressed();
                               finish();
                           }

                       }catch (Exception e){
                           e.printStackTrace();
                       }
                       DialogOpener.dialog.dismiss();
                    }
                    DialogOpener.dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    DialogOpener.dialog.dismiss();
                    //UImsgs.showToast(context, "Catch " +e);
                    UImsgs.showCustomToast(getApplicationContext(), "Oops! Something went wrong with this vendor",WARNING);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(getApplicationContext(), "Oops! Something went wrong ",ERROR);
            }
        });
        requestQueue.add(stringRequest);
    }

}
