package com.apticks.nextclickuser.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.apticks.nextclickuser.Fragments.FoodModule;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;

import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;
import static com.apticks.nextclickuser.Constants.IErrors.someErrorOccurred;

public class FoodActivity extends AppCompatActivity {

    Fragment selectedFragment = null;

    Context mContext;
    public static String sub_cat_id;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        getSupportActionBar().hide();

        mContext = getApplicationContext();


        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(FoodActivity.this, R.color.colorPrimary));
        }*/
        Bundle bundle = getIntent().getExtras();
        Bundle extras = getIntent().getExtras();
        Intent intent = getIntent();
        //String nameREstaurant =intent.getExtras().getString("name");
        String nameREstaurant = bundle.getString(reastaurantName);
        String Vender_Data = bundle.getString(vendorUserId);
        Log.d("vendor data id",Vender_Data);

        sub_cat_id = bundle.getString("sub_cat_id");
        System.out.println("aaaaaaaa  Vender_Data "+Vender_Data+"  sub_cat_id  "+sub_cat_id+"  nameREstaurant  "+nameREstaurant);
        //int vdata = bundle.getInt("vendorData");

        Log.e("grep", "" + Vender_Data);
        /*Log.e("babu", "" + Integer.toString(vdata));
        String str = Integer.toString(vdata);*/


        /*String data = intent.getStringExtra(reastaurantName);*/

        if (nameREstaurant != null) {
            Bundle bundle1 = new Bundle();
            bundle.putString(reastaurantName, nameREstaurant);
            /*bundle.putString("VernderUserId", data);*/
            /*bundle.putString("vendorData", str);*/
            bundle.putString(vendorUserId,Vender_Data);

            selectedFragment = new FoodModule(FoodActivity.this, nameREstaurant, Vender_Data, sub_cat_id);
            selectedFragment.setArguments(bundle1);
            getSupportFragmentManager().beginTransaction().replace(R.id.food_nav_host_fragment, selectedFragment).commit();
        } else {
            //Toast.makeText(mContext, "Else " + nameREstaurant, Toast.LENGTH_SHORT).show();
            View v = getWindow().getDecorView().getRootView();
            UImsgs.showSnackBar(v, someErrorOccurred);
        }


    }



}


