package com.apticks.nextclickuser.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.SearchView;
import android.widget.Toast;

import com.apticks.nextclickuser.R;

public class SearchActivity extends AppCompatActivity {

    private SearchView searchView;
    private Context mContext;
    private String searchString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().hide();
        init();
        searchView.requestFocus();
        searchView.onActionViewExpanded();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(mContext, query, Toast.LENGTH_SHORT).show();
                searchString = query;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        /*searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UImsgs.showCustomToast(mContext,searchString,INFO);
            }
        });*/
    }

    private void init() {
        mContext = SearchActivity.this;
        searchView = findViewById(R.id.searchView);
    }
}
