package com.apticks.nextclickuser.Activities.VENDORS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.apticks.nextclickuser.Activities.Beauty_Spa.Vendors_BS;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.NetworkUtil;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.interfaces.CloseCallBack;
import com.apticks.nextclickuser.products.activities.AddCartActivity;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.apticks.nextclickuser.Adapters.VENDORSADAPTERS.VendorsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.VENDORSPOJO.VendorsPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.BRANDID;
import static com.apticks.nextclickuser.Config.Config.CATID;
import static com.apticks.nextclickuser.Config.Config.LATITUDE;
import static com.apticks.nextclickuser.Config.Config.LEAD_GENERATE_ARRAY;
import static com.apticks.nextclickuser.Config.Config.LONGITUDE;
import static com.apticks.nextclickuser.Config.Config.SEARCH;
import static com.apticks.nextclickuser.Config.Config.SUBCATID;
//import static com.grepthor.nextclickuser.Config.Config.SUB_CAT_VENDOR_LIST;
import static com.apticks.nextclickuser.Config.Config.VENDOR_LIST;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.SUCCESS;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class VendorsActivity extends AppCompatActivity implements CloseCallBack {
    private String /*sub_cat_id=null,*/cat_id, brandId;
    static public String sub_cat_id = null;
    private RecyclerView vendors_recycelr;
    private ArrayList<VendorsPojo> vendorsList = new ArrayList<>();
    private ArrayList<ServicesPojo> serviceslist = new ArrayList<>();
    private LinearLayout no_vendors_data;
    private Switch lead_generation_switch;
    private Boolean lead = false;
    private ImageView back_imageView;
    private Map<String, Object> leadMap = new HashMap<>();
    PreferenceManager preferenceManager;
    String lat, lang;
    Context context;
    private ShimmerFrameLayout shimmer_layout;
    int main_offset = 0, main_count = 10, remaining_count = 0, level_count = 1;
    VendorsAdapter vendorsAdapter;
    LinearLayoutManager layoutManager;
    private ProgressBar vendors_progress;
    private CustomDialog customDialog;
    boolean isScrolling = false;
    int currentItems, totalItems, scrollOutItems,checkposition=0;
    String searchString;
    int SHOP_NOW = 1, BOOK_NOW = 2;
    int SHOP_BOOK = SHOP_NOW;

    int vendorsRange = 0;

    private FrameLayout cart_layout;

    private EditText etSearch;
    private ImageView ivSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendors);
        getSupportActionBar().hide();
        //....
        context = VendorsActivity.this;


        try {
            checkDeepLink();
        } catch (Exception e) {
            e.printStackTrace();
        }

        checkposition=getIntent().getIntExtra("checkposition",0);

        //....


        preferenceManager = new PreferenceManager(VendorsActivity.this);
       // System.out.println("aaaaaa token  "+preferenceManager.getString(TOKEN_KEY));
        try {
            sub_cat_id = getIntent().getStringExtra("sub_cat_id");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            brandId = getIntent().getStringExtra("brand_id");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            searchString = getIntent().getStringExtra("search");
        } catch (Exception e) {
            e.printStackTrace();
        }
        cat_id = getIntent().getStringExtra("cat_id");
        lat = preferenceManager.getString("lat");
        lang = preferenceManager.getString("lang");
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.Iconblue));
            }
        } */
        init();
        try {
            if (Integer.parseInt(getIntent().getStringExtra("lead")) == 0) {
                lead_generation_switch.setVisibility(View.GONE);
                SHOP_BOOK = SHOP_NOW;
            } else {
                SHOP_BOOK = BOOK_NOW;
            }
        } catch (Exception e) {
            e.printStackTrace();
            lead_generation_switch.setVisibility(View.GONE);
            SHOP_BOOK = SHOP_NOW;
        }

        SearchView search_view =findViewById(R.id.search_view);

        // Get the search close button image view
        ImageView closeButton = (ImageView)search_view.findViewById(R.id.search_close_btn);
        // Set on click listener
        closeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Find EditText view
                EditText et = (EditText) findViewById(R.id.search_src_text);
                //Clear the text from EditText view
                et.setText("");
                //Collapse the action view
                search_view.onActionViewCollapsed();
                //Collapse the search widget
                //search_view.collapseActionView();

                vendorsList.clear();
                searchString = "";
                startSearchView();
            }
        });

        etSearch = findViewById(R.id.etSearch);
        ivSearch = findViewById(R.id.ivSearch);
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSearchView();
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                searchString = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    startSearchView();
                    return true;
                }
                return false;
            }
        });

        /*search_view.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if (NetworkUtil.isNetworkConnected(VendorsActivity.this)) {
                    searchString = "";
                    main_offset = 0;
                    main_count = 10;
                    remaining_count = 0;
                    level_count = 1;
                    vendorsFetcher(main_count, main_offset, SHOP_BOOK);
                    search_view.clearFocus();
                }
                return false;
            }
        });*/
        search_view.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("","");
            }
        });
        search_view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (NetworkUtil.isNetworkConnected(VendorsActivity.this)) {
                    searchString = query;
                    search_view.clearFocus();
                    startSearchView();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        shimmer_layout.setVisibility(View.VISIBLE);
        shimmer_layout.startShimmer();
        if (NetworkUtil.isNetworkConnected(VendorsActivity.this)) {
            vendorsFetcher(main_count, main_offset, SHOP_BOOK);
        } else {
            Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
        }

        layoutManager = new LinearLayoutManager(this);
        adapterSetter();
        vendors_recycelr.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    remaining_count = remaining_count - (level_count * 10);
                    if (remaining_count > 0) {
                        main_offset = main_count + 1;
                        main_count = main_count + 10;
                      //  vendors_progress.setVisibility(View.VISIBLE);
                        if (NetworkUtil.isNetworkConnected(VendorsActivity.this)) {
                            vendorsFetcher(10, main_offset, SHOP_BOOK);
                            level_count++;
                        } else {
                            Toast.makeText(VendorsActivity.this, "No internet connection! ", Toast.LENGTH_SHORT).show();
                        }


                    }
                }
            }
        });


        lead_generation_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leadMap.size() > 0) {
                    if (!lead) {
                        lead_generation_switch.setChecked(true);
                        lead = true;
                        lead_generation_switch.setText("On ");

                        JSONObject jsonObject = new JSONObject(leadMap);
                        Log.d("leadmap", jsonObject.toString());
                        leadGenerator(jsonObject);
                    } else {
                        lead_generation_switch.setChecked(false);
                        lead = false;
                        lead_generation_switch.setText("Off ");
                    }
                } else {
                    UImsgs.showCustomToast(VendorsActivity.this, "Enquiry Not Possible", ERROR);
                }
            }

        });

        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        /*vendors_recycelr.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if(remaining_count>=10) {
                        main_offset=main_count+1;
                        main_count = main_count+10;
                        vendorsFetcher(10, main_offset);
                    }else{
                        main_offset=main_count+1;
                        vendorsFetcher(10, main_offset);
                    }
                }
            }
        });*/

    }

    private void startSearchView() {
        vendorsList.clear();
        main_offset = 0;
        main_count = 10;
        remaining_count = 0;
        level_count = 1;
        vendorsFetcher(main_count, main_offset, SHOP_BOOK);
    }

    private void leadGenerator(JSONObject jsonObject) {
        final String data = jsonObject.toString();
        DialogOpener.dialogOpener(context);

        RequestQueue requestQueue = Volley.newRequestQueue(VendorsActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LEAD_GENERATE_ARRAY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        String str = "";
                        Log.d("res", response);
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 201) {

                            DialogOpener.dialog.dismiss();
                            UImsgs.showCustomToast(VendorsActivity.this, jsonObject.getString("message"), SUCCESS);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        DialogOpener.dialog.dismiss();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void vendorsFetcher(int limit, int offset, int SHOP_BOOK) {
       /* final ProgressDialog progressDialog = new ProgressDialog(VendorsActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();*/
        //DialogOpener.dialogOpener(context);

        customDialog.show();
        String url = "";
        if (sub_cat_id != null) {
            //url =  VENDOR_LIST+cat_id+SUBCATID + sub_cat_id+LATITUDE+lat+LONGITUDE+lang;
            url = VENDOR_LIST + limit + "/" + offset + SUBCATID + sub_cat_id +BRANDID + brandId + LATITUDE + lat + LONGITUDE + lang + CATID + cat_id;
        } else {
            //url =  VENDOR_LIST+cat_id+BRANDID + brandId+LATITUDE+lat+LONGITUDE+lang;
            url = VENDOR_LIST + limit + "/" + offset + BRANDID + brandId + LATITUDE + lat + LONGITUDE + lang + CATID + cat_id;
        }
        try {
            if (searchString!=null && searchString.length() >= 1) {
                url = VENDOR_LIST + limit + "/" + offset + SEARCH + searchString + LATITUDE + lat + LONGITUDE + lang /*+ CATID + cat_id*/;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (checkposition==1) {
            if (searchString != null && searchString.length() >= 1)
                url = VENDOR_LIST + limit + "/" + offset + SEARCH + searchString + LATITUDE + lat + LONGITUDE + lang /*+ CATID + cat_id*/;
            else
                url = VENDOR_LIST + limit + "/" + offset + /*SEARCH + searchString +*/ "?latitude=" + lat + LONGITUDE + lang /*+ CATID + cat_id*/;
        }

        Log.d("Url", url);
        System.out.println("aaaaaaaaaa  url "+url);


        RequestQueue requestQueue = Volley.newRequestQueue(VendorsActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    customDialog.dismiss();
                    Log.d("brand vendors response", response);
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaa vendorslidt  "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status) {
                            Object dataObj = jsonObject.get("data");
                            if(dataObj instanceof JSONObject)
                            {
                                JSONObject dataObject = jsonObject.getJSONObject("data");
                                if (!dataObject.get("result").getClass().toString().equalsIgnoreCase(str.getClass().toString())) {
                                    try {
                                        remaining_count = dataObject.getInt("count");

                                        JSONArray resultArray = dataObject.getJSONArray("result");
                                        ArrayList<Object> leadVendorsList = new ArrayList<>();

                                        // progressDialog.dismiss();
//                                    DialogOpener.dialog.dismiss();
                                        vendorsRange++;
                                        for (int i = 0; i < resultArray.length(); i++) {
                                            Map<String, String> leadVendorsMapList = new HashMap<>();
                                            JSONObject vendorObj = resultArray.getJSONObject(i);
                                            VendorsPojo vendorsPojo = new VendorsPojo();
                                            vendorsPojo.setId(vendorObj.getString("id"));
                                            vendorsPojo.setVendor_user_id(vendorObj.getInt("vendor_user_id"));
                                            leadVendorsMapList.put("vendor_id", vendorObj.getString("vendor_user_id"));
                                            leadVendorsList.add(leadVendorsMapList);
                                            vendorsPojo.setName(vendorObj.getString("name"));
                                            vendorsPojo.setEmail(vendorObj.getString("email"));
                                            vendorsPojo.setAddress(vendorObj.getString("address"));
                                            vendorsPojo.setLandmark(vendorObj.getString("landmark"));
                                            vendorsPojo.setImage(vendorObj.getString("image"));
                                            vendorsPojo.setDistance(vendorObj.getDouble("distance"));
                                            vendorsPojo.setAvailability(vendorObj.getInt("availability"));
                                            vendorsPojo.setIs_having_lead(vendorObj.getString("is_having_lead"));

                                            try {
                                                vendorsPojo.setLattitude(vendorObj.getDouble("latitude"));
                                                vendorsPojo.setLongitude(vendorObj.getDouble("longitude"));
                                            } catch (Exception e) {

                                            }
                                            serviceslist=new ArrayList<>();
                                            try {
                                                for (int j=0;j<vendorObj.getJSONArray("services").length();j++){
                                                    JSONObject serviceobj=vendorObj.getJSONArray("services").getJSONObject(j);
                                                    JSONObject obj=serviceobj.getJSONObject("service");
                                                    ServicesPojo servicesPojo=new ServicesPojo();
                                                    servicesPojo.setId(obj.getString("id"));
                                                    servicesPojo.setName(obj.getString("name"));
                                                    servicesPojo.setDescription(obj.getString("desc"));
                                                    serviceslist.add(servicesPojo);
                                                }
                                                vendorsPojo.setServiceList(serviceslist);

                                            }catch (JSONException e){
                                                boolean servicebool=vendorObj.getBoolean("services");
                                                System.out.println("aaaaaaa  exp "+e.getMessage());
                                            }

                                            vendorsList.add(vendorsPojo);
                                            vendorsAdapter.notifyDataSetChanged();
                                            no_vendors_data.setVisibility(View.GONE);
                                            //  vendors_progress.setVisibility(View.GONE);
                                        }
                                        leadMap.put("vendors", leadVendorsList);


                                        //progressDialog.dismiss();
                                        //DialogOpener.dialog.dismiss();
                                        shimmer_layout.stopShimmer();
                                        shimmer_layout.setVisibility(View.GONE);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        System.out.println("aaaaaaaa  catch  "+e.getMessage());
                                        // progressDialog.dismiss();
                                        //DialogOpener.dialog.dismiss();

                                    }
                                }else {
                                    System.out.println("aaaaaaaa  else");
                                }
                            }
                            else
                            {
                                vendorsAdapter.notifyDataSetChanged();
                                no_vendors_data.setVisibility(View.VISIBLE);
                            }


                        }
                        //progressDialog.dismiss();
                        //DialogOpener.dialog.dismiss();
                        shimmer_layout.stopShimmer();
                        shimmer_layout.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        // progressDialog.dismiss();
                        if (vendorsRange == 0) {
                            no_vendors_data.setVisibility(View.VISIBLE);
                            //DialogOpener.dialog.dismiss();
                            shimmer_layout.stopShimmer();
                            shimmer_layout.setVisibility(View.GONE);
                            System.out.println("aaaaaa  error "+e.getMessage());
                        }
                    }
                }else {
                    customDialog.dismiss();
                }
                //DialogOpener.dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                //progressDialog.dismiss();
                //DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(context, "Something went wrong", ERROR);
                System.out.println("aaaaaa  error "+error.getMessage());

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public LatLng getLocationFromAddress(Context context,String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }
    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
    private void init() {
        back_imageView = findViewById(R.id.img_back);
        TextView tv_header= findViewById(R.id.tv_header);
        tv_header.setText("Related profiles");
        tv_header.setAllCaps(true);
        vendors_recycelr = findViewById(R.id.vendors_recycelr);
        no_vendors_data = findViewById(R.id.no_vendors_data);
        lead_generation_switch = findViewById(R.id.lead_generation_switch);
        shimmer_layout = findViewById(R.id.shimmer_layout);
       // vendors_progress = findViewById(R.id.vendors_progress);
        cart_layout = findViewById(R.id.cart_layout);

        customDialog=new CustomDialog(VendorsActivity.this);
        cart_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VendorsActivity.this, CartAddActivity.class));
            }
        });
    }


    //trail
    private void checkDeepLink() {
        if (getIntent() != null && getIntent().getData() != null) {
            Uri data = getIntent().getData();
            String scheme = data.getScheme();
            String host = data.getHost();
            String param = data.getQuery();
            Log.d("DeepLink", "Schema : " + scheme);
            Log.d("DeepLink", "Host : " + host);
            Log.d("DeepLink", "param : " + host);

            /*if (host.equals("page_details")){
                Intent intent = new Intent(this,DatadetailAcvity.class);
                intent.putExtra("detail_id",Long.valueOf(data.getQueryParameter("detail_id")));  // URL query values as string, you need to parse string to long.
                startActivity(intent);
            }else{
                // ... other logic
            }*/
        }
    }

    private void adapterSetter() {
        vendorsAdapter = new VendorsAdapter(VendorsActivity.this, vendorsList, SHOP_BOOK,serviceslist);
        layoutManager = new LinearLayoutManager(VendorsActivity.this, LinearLayoutManager.VERTICAL, false) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(VendorsActivity.this) {

                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };

        vendors_recycelr.setLayoutManager(layoutManager);
        vendors_recycelr.setAdapter(vendorsAdapter);
    }


    @Override
    public void close() {
        finish();
    }
}
