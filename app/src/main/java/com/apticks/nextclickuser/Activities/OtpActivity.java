package com.apticks.nextclickuser.Activities;

import androidx.annotation.RequiresApi;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.IncomingSms;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.view.KeyEvent.KEYCODE_DEL;
import static com.apticks.nextclickuser.Config.Config.SEND_OTP;
import static com.apticks.nextclickuser.Config.Config.USER_PROFILE_R;
import static com.apticks.nextclickuser.Config.Config.USER_PROFILE_U;
import static com.apticks.nextclickuser.Config.Config.VERIFY_OTP;
import static com.apticks.nextclickuser.Constants.Constants.APP_ID;
import static com.apticks.nextclickuser.Constants.Constants.APP_ID_VALUE;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.MOBILE;
import static com.apticks.nextclickuser.Constants.Constants.WARNING;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.IS_VENDOR;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.emailId;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.profilepic;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.uniqueId;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.userName;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.user_id;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.wallet;

public class OtpActivity extends Activity {

    private EditText et_1,et_2,et_3,et_4,et_5,et_6;
    private Button submit;
    private TextView tv_timer_resend,tv_otpnumber;
    EditText[] otpETs = new EditText[6];
    String mobileno,otpdata;
    private ImageView img_cancel;
    PreferenceManager preferenceManager;
    private static final int REQ_USER_CONSENT = 200;
    private int REQUEST_CODE = 1;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    String unique_id;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        submit=findViewById(R.id.submit);
        tv_timer_resend=findViewById(R.id.tv_timer_resend);
        tv_otpnumber=findViewById(R.id.tv_otpnumber);
        img_cancel=findViewById(R.id.img_cancel);


        permissions.add(READ_SMS);
        permissions.add(RECEIVE_SMS);
        permissions.add(SEND_SMS);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0)
            requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                    ALL_PERMISSIONS_RESULT);

        otpETs[0] = findViewById(R.id.et_1);
        otpETs[1] = findViewById(R.id.et_2);
        otpETs[2] = findViewById(R.id.et_3);
        otpETs[3] = findViewById(R.id.et_4);
        otpETs[4] = findViewById(R.id.et_5);
        otpETs[5] = findViewById(R.id.et_6);
        preferenceManager = new PreferenceManager(OtpActivity.this);
        mobileno=getIntent().getStringExtra("mobileno");

        otpdata=getIntent().getStringExtra("data");
        tv_otpnumber.setText(mobileno);

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                tv_timer_resend.setText("Resend Otp");
            }

        }.start();

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otpETs[0].getText().toString().trim().isEmpty() && otpETs[1].getText().toString().trim().isEmpty()&&
                        otpETs[2].getText().toString().trim().isEmpty() && otpETs[3].getText().toString().trim().isEmpty()
                        && otpETs[4].getText().toString().trim().isEmpty()&& otpETs[5].getText().toString().trim().isEmpty()){
                    Toast.makeText(OtpActivity.this, "Please enter correct otp", Toast.LENGTH_SHORT).show();
                }else {
                    String otp=otpETs[0].getText().toString().trim()+""+otpETs[1].getText().toString().trim()+""+
                            otpETs[2].getText().toString().trim()+""+otpETs[3].getText().toString().trim()+""+
                            otpETs[4].getText().toString().trim()+""+otpETs[5].getText().toString().trim();

                    sendOtp(mobileno,otp);
                   // Toast.makeText(OtpActivity.this, "Please enter correct otp", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tv_timer_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv_timer_resend.getText().toString().trim().equalsIgnoreCase("Resend Otp")){
                    resendOtp(mobileno);
                }else {

                }
            }
        });

    }
    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new
                                                        String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(OtpActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == 7 || keyCode == 8 ||
                keyCode == 9 || keyCode == 10 ||
                keyCode == 11 || keyCode == 12 ||
                keyCode == 13 || keyCode == 14 ||
                keyCode == 15 || keyCode == 16 ||
                keyCode == 67) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KEYCODE_DEL) {
                    int index = checkWhoHasFocus();
                    if (index != 123) {
                        if (Helpers.rS(otpETs[index]).equals("")) {
                            if (index != 0) {
                                otpETs[index - 1].requestFocus();
                            }
                        } else {
                            return super.dispatchKeyEvent(event);
                        }
                    }
                } else {
                    int index = checkWhoHasFocus();
                    if (index != 123) {
                        if (Helpers.rS(otpETs[index]).equals("")) {
                            return super.dispatchKeyEvent(event);
                        } else {
                            if (index != 5) {
                                otpETs[index + 1].requestFocus();
                            }
                        }
                    }
                    return super.dispatchKeyEvent(event);
                }
            }
        } else {
            return super.dispatchKeyEvent(event);
        }
        return true;
    }
    private int checkWhoHasFocus() {
        for (int i = 0; i < otpETs.length; i++) {
            EditText tempET = otpETs[i];
            if (tempET.hasFocus()) {
                return i;
            }
        }
        return 123;
    }
    public static class Helpers {

        public static String rS(EditText editText) {
            return editText.getText().toString().trim();
        }
    }

    private void sendOtp(final String msg,String otp) {

        DialogOpener.dialogOpener(OtpActivity.this);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", msg);
        fcmMap.put("otp", otp);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(OtpActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                VERIFY_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        DialogOpener.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            System.out.println("aaaaaaa response verify "+dataObject.toString());
                            String token = dataObject.getString("token");
                            //UImsgs.showToast(mContext, token);
                            preferenceManager.putString(TOKEN_KEY, token);
                             unique_id=dataObject.getString("unique_id");
                            String name=dataObject.getString("first_name");
                            if (name.equalsIgnoreCase("null")){
                                ShowAlert();
                            }else {
                                finish();
                            }

                          //  finish();

                        } else {
                            Toast.makeText(OtpActivity.this, Html.fromHtml(jsonObject.getString("message")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        DialogOpener.dialog.dismiss();
                        UImsgs.showCustomToast(OtpActivity.this, "Something went wrong", WARNING);
                        e.printStackTrace();
                    }
                } else {
                    DialogOpener.dialog.dismiss();
                    UImsgs.showCustomToast(OtpActivity.this, "Server under maintenance", INFO);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showToast(OtpActivity.this, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void resendOtp(final String msg) {

        DialogOpener.dialogOpener(OtpActivity.this);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", msg);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(OtpActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("aaaaaaa response resend "+response);
                if (response != null) {
                    try {
                        DialogOpener.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            new CountDownTimer(30000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
                                    //here you can have your logic to set text to edittext
                                }

                                public void onFinish() {
                                    tv_timer_resend.setText("Resend Otp");
                                }

                            }.start();
                            otpETs[0].setText("");
                            otpETs[1].setText("");
                            otpETs[2].setText("");
                            otpETs[3].setText("");
                            otpETs[4].setText("");
                            otpETs[5].setText("");
                            Toast.makeText(OtpActivity.this, Html.fromHtml(jsonObject.getString("message")), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(OtpActivity.this, Html.fromHtml(jsonObject.getString("message")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        DialogOpener.dialog.dismiss();
                        UImsgs.showCustomToast(OtpActivity.this, "Something went wrong", WARNING);
                        e.printStackTrace();
                    }
                } else {
                    DialogOpener.dialog.dismiss();
                    UImsgs.showCustomToast(OtpActivity.this, "Server under maintenance", INFO);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showToast(OtpActivity.this, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    public void ShowAlert(){

    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View alertLayout = inflater.inflate(R.layout.name_layout, null);

    final EditText mailtext = alertLayout.findViewById(R.id.forgot_mail);
    final EditText et_mail = alertLayout.findViewById(R.id.et_mail);
    final TextView ok = alertLayout.findViewById(R.id.ok);
        AlertDialog.Builder alert = new AlertDialog.Builder(OtpActivity.this);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setTitle("Fill your details");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);
        final AlertDialog dialog = alert.create();
        dialog.show();
        dialog.setCancelable(false);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String forgotmail = mailtext.getText().toString().trim();
                if (forgotmail.length() == 0) {
                    mailtext.setError("Should Not Be Empty");
                    mailtext.requestFocus();
                }else {
                    if (et_mail.length()==0){
                        et_mail.setError("Should Not Be Empty");
                        et_mail.requestFocus();
                    }else {
                        updateprofile(mailtext.getText().toString().trim(),et_mail.getText().toString(),dialog);
                    }

                }

            }
        });


    }
    public void updateprofile(String name,String mail, AlertDialog dialog){
        Map<String, String> datamap = new HashMap<>();
        datamap.put("first_name", name);
        datamap.put("email", mail);
        datamap.put("unique_id", unique_id);
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();
        
        postUpdateProfile(json,name,dialog);

    }
    private void postUpdateProfile(JSONObject jsonObject, String name, AlertDialog dialog) {

        final String data = jsonObject.toString();
        final ProgressDialog progressDialog = new ProgressDialog(OtpActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while updating your Name.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        final String dataStr = jsonObject.toString();
        System.out.println("aaaaaaa request nameupdate "+dataStr);
        RequestQueue requestQueue = Volley.newRequestQueue(OtpActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, USER_PROFILE_U, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, "", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                UImsgs.showToast(OtpActivity.this, "Your Name Has Been Updated Sucesfuly");
                dialog.dismiss();
                preferenceManager.putString(userName, ""+name);

                fetchUserDetails();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                UImsgs.showToast(OtpActivity.this, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
    private void fetchUserDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(OtpActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, USER_PROFILE_R, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    System.out.println("aaaaa data "+jsonObjectData.toString());
                    preferenceManager.putString(userName, jsonObjectData.getString("first_name"));
                    String lastname;
                    try {
                        lastname = jsonObjectData.getString("last_name");
                        if (!lastname.equalsIgnoreCase("null")) {
                            preferenceManager.putString(userName, jsonObjectData.getString("first_name") + " " + lastname);
                        } else {
                            preferenceManager.putString(userName, jsonObjectData.getString("first_name"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        preferenceManager.putInt(wallet, jsonObjectData.getInt("wallet"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        preferenceManager.putString(uniqueId, jsonObjectData.getString("unique_id"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONObject groupObject = jsonObjectData.getJSONObject("groups");
                        Iterator x = groupObject.keys();
                        JSONArray groupArray = new JSONArray();

                        while (x.hasNext()) {
                            String key = (String) x.next();
                            groupArray.put(groupObject.get(key));
                        }
                        preferenceManager.putString(user_id, groupArray.getJSONObject(0).getString("user_id"));
                        if (!groupArray.getJSONObject(0).getString("name").equalsIgnoreCase("admin") && !groupArray.getJSONObject(0).getString("name").equalsIgnoreCase("user")) {
                            preferenceManager.putString(IS_VENDOR, "yes");
                        } else {
                            preferenceManager.putString(IS_VENDOR, "no");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        preferenceManager.putString(profilepic, jsonObjectData.getString("image"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Log.d("mail", jsonObjectData.getString("email"));
                        preferenceManager.putString(emailId, jsonObjectData.getString("email"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Log.d("mobile", jsonObjectData.getString("phone"));
                        preferenceManager.putString(MOBILE, jsonObjectData.getString("phone"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("fetch user error", error.toString());
            }
        }) {
           /* @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return super.getBody();
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                //map.put("X_AUTH_TOKEN", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMDA4NCIsInVzZXJkZXRhaWwiOnsiaWQiOiIxMDAwODQiLCJpcF9hZGRyZXNzIjoiMjQwOTo0MDcwOjIxOWE6YTJjNDo6OGIwOmU4YTUiLCJ1c2VybmFtZSI6Imd1ampldGlzaHJhdmFuQGdtYWlsLmNvbSIsInVuaXF1ZV9pZCI6Ik5DVTAyMTEiLCJwYXNzd29yZCI6IiQyeSQwOCQ2b21iQXViMTFhTU9leFRCdUF2SDBPTVJsbFlTZ3FGa0JcL2luMzVQXC9USE94ZlwvUnFGNGhKLiIsInNhbHQiOm51bGwsImVtYWlsIjoiZ3VqamV0aXNocmF2YW5AZ21haWwuY29tIiwid2FsbGV0IjoiMC4wMCIsImFjdGl2YXRpb25fY29kZSI6bnVsbCwiZm9yZ290dGVuX3Bhc3N3b3JkX2NvZGUiOm51bGwsImZvcmdvdHRlbl9wYXNzd29yZF90aW1lIjpudWxsLCJyZW1lbWJlcl9jb2RlIjpudWxsLCJjcmVhdGVkX29uIjoiMTU5MDEyMTMxNSIsImxhc3RfbG9naW4iOm51bGwsImFjdGl2ZSI6IjEiLCJsaXN0X2lkIjoiMCIsImZpcnN0X25hbWUiOiJHdWpqZXRpIFNocmF2YW5rdW1hciIsImxhc3RfbmFtZSI6bnVsbCwiY29tcGFueSI6bnVsbCwicGhvbmUiOiIiLCJjcmVhdGVkX3VzZXJfaWQiOm51bGwsInVwZGF0ZWRfdXNlcl9pZCI6bnVsbCwiY3JlYXRlZF9hdCI6IjIwMjAtMDUtMjIgMDQ6MjE6NTUiLCJ1cGRhdGVkX2F0IjpudWxsLCJkZWxldGVkX2F0IjpudWxsLCJzdGF0dXMiOiIxIn0sInRpbWUiOjE1OTAxMjEzNjB9.gm-lTQiaLcLLYu4KIpjMorFcayjO77IZFulCRlwYlTk");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private IncomingSms receiver = new IncomingSms() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                System.out.println("aaaaaaaaa  message  "+message);
                getOtpFromMessage(message);
              //  your_edittext.setText(message);
                //Do whatever you want with the code here
            }
        }
    };
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("aaaaaaa  otp "+matcher.group(0));
            //otpText.setText(matcher.group(0));
            String otp=matcher.group(0);
            otpETs[0].setText(""+otp.charAt(0));
            otpETs[1].setText(""+otp.charAt(1));
            otpETs[2].setText(""+otp.charAt(2));
            otpETs[3].setText(""+otp.charAt(3));
            otpETs[4].setText(""+otp.charAt(4));
            otpETs[5].setText(""+otp.charAt(5));
          //  sendOtp(mobileno,otp);
        }
    }
    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


}