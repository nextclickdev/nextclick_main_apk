package com.apticks.nextclickuser.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;


import com.apticks.nextclickuser.Fragments.CartEcomerce;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.apticks.nextclickuser.Activities.ALL_CATEGORIES.AllCategoriesActivity;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Activities.NEWS.NewsActivity;
import com.apticks.nextclickuser.Activities.USERPROFILE.UserProfileActivity;
import com.apticks.nextclickuser.Fragments.EcommerceCartFragment;
import com.apticks.nextclickuser.Fragments.CartFoodFragments.FoodCartFragment;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.utilities.PreferenceManager;


public class CartActivity extends AppCompatActivity {
    Fragment selectedFragment = null;
    String type;
    Context mContext;
    PreferenceManager preferenceManager;
    CartDBHelper cartDBHelper;
    BottomNavigationView navigation;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        getSupportActionBar().hide();

        mContext = CartActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        cartDBHelper = new CartDBHelper(mContext);

        Intent intent = getIntent();
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));

        type = intent.getStringExtra("Type");
        System.out.println("aaaaaaa type "+type);

        if (type.equalsIgnoreCase("Food")) {

            /*String itemId = intent.getStringExtra("item_id");
            String quantity = intent.getStringExtra("quantity");
            String nameString = intent.getStringExtra("name_string");
            String imageString = intent.getStringExtra("image_string");
            String priceString = intent.getStringExtra("price_string");

            //addToCartActivity(itemId,quantity,nameString,imageString,priceString);
            type = intent.getStringExtra("Type");
            selectedFragment = new FoodCartFragment(CartActivity.this);

            Bundle bundle = new Bundle();
            bundle.putString("item_id", itemId);
            bundle.putString("quantity", quantity);

            selectedFragment.setArguments(bundle);*/


            selectedFragment = new CartEcomerce(getApplicationContext());
        }

        if (type.equalsIgnoreCase("Ecom")) {
            selectedFragment = new CartEcomerce(getApplicationContext());
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.cart_nav_host_fragment, selectedFragment).commit();

        navigation = (BottomNavigationView) findViewById(R.id.nav_view);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        try{
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) navigation.getLayoutParams();
        }catch (Exception e){
            e.printStackTrace();
        }
        navigation.setSelectedItemId(R.id.navigation_cart);

    }

    private void addToCartActivity(String itemId, String quantity, String nameString, String imageString, String priceString) {

        if (cartDBHelper.insertFoodCart(itemId, 5, nameString, imageString, priceString, quantity, "", "", "",0)) {
            Toast.makeText(mContext, "Added Succefully", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(mContext, "False", Toast.LENGTH_SHORT).show();
        }

    }

    /*{
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, FoodCartAdd, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",preferenceManager.getString(""));
                params.put("item_id",str_password);
                params.put("quantity",str_password);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("user_id",str_email);
                params.put("item_id",str_password);
                params.put("quantity",str_password);
                return params;
            }

            //here I want to post data to sever
        }

        ;
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }*/


    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent mainIntent = new Intent(mContext, MainCategoriesActivity.class);
                    startActivity(mainIntent);
                    return true;
                case R.id.navigation_news:
                    Intent newsIntent = new Intent(mContext, NewsActivity.class);
                    startActivity(newsIntent);

                    return true;

                case R.id.navigation_category:
                    Intent categoryIntent = new Intent(mContext, AllCategoriesActivity.class);
                    startActivity(categoryIntent);
                    return true;
                case R.id.navigation_profile:
                    Intent profileIntent = new Intent(mContext, UserProfileActivity.class);
                    startActivity(profileIntent);
                    return true;
                case R.id.navigation_cart:


                    return true;


            }

            return false;
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        navigation.setSelectedItemId(R.id.navigation_cart);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        navigation.setSelectedItemId(R.id.navigation_cart);
    }
}
