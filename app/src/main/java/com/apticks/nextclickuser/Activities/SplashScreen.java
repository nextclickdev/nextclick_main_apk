package com.apticks.nextclickuser.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.checkout.activities.MapTest;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.GET_USER_DETAIL;
import static com.apticks.nextclickuser.Config.Config.USER_PROFILE_R;
import static com.apticks.nextclickuser.Constants.Constants.MOBILE;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.*;

public class SplashScreen extends Activity {
    Context mContext;
    PreferenceManager preferenceManager;
    String TAG = "Splash Screen";
    AppUpdateManager mAppUpdateManager;
    private int RC_APP_UPDATE = 001;
    boolean updated = false;
    ImageView splashimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash*****", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        //getSupportActionBar().hide();
        mContext = getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);

        try {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE}, 101);

        }

       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, MainCategoriesActivity.class);
                startActivity(intent);
                finish();
            }
        },200);*/
    }


    @Override
    protected void onResume() {
        super.onResume();
       /* Intent intent = new Intent(mContext, MapTest.class);
        startActivity(intent);
        finish();*/

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE}, 101);


        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                   // preferenceManager.deleteString(TOKEN_KEY);
                    //checkUpdate();
                    if (preferenceManager.getString(TOKEN_KEY) != null) {
                        Log.d("tok", preferenceManager.getString(TOKEN_KEY));
                        System.out.println("aaaaa  token "+preferenceManager.getString(TOKEN_KEY));
                        /*checkWalletAmountUseerName();*/
                        fetchUserDetails();

                    } else {

                        Intent intent = new Intent(mContext, MainCategoriesActivity.class);
                        startActivity(intent);
                        finish();
                    }


                }
            }, 700);
        }
    }


    private void checkWalletAmountUseerName() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_USER_DETAIL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null)
                    try {
                        //Toast.makeText(mContext, " " + response, Toast.LENGTH_SHORT).show();4
                        Log.d("user response", response);
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("status")) {
                            JSONObject jsonArrayData = jsonObject.getJSONObject("data");

                            fetchUserDetails();

                        }
                        Log.v("Responce", "Responce");

                    } catch (JSONException e) {
                        //Toast.makeText(mContext, "Catch "+e, Toast.LENGTH_SHORT).show();
                        Log.v("JSON Exception", "Catch " + e);

                    } catch (Exception e) {

                    }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, "Error " + error, Toast.LENGTH_SHORT).show();
                Log.v("ResponceError", "ResponceError");
            }
        }) {
            /*@Override
            public String getBodyContentType() {
                return "application/json";
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }
        };
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void fetchUserDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, USER_PROFILE_R, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    System.out.println("aaaaa data "+jsonObjectData.toString());
                    String lastname;
                    try {
                        lastname = jsonObjectData.getString("last_name");
                        if (!lastname.equalsIgnoreCase("null")) {
                            preferenceManager.putString(userName, jsonObjectData.getString("first_name") + " " + lastname);
                        } else {
                            preferenceManager.putString(userName, jsonObjectData.getString("first_name"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        preferenceManager.putInt(wallet, jsonObjectData.getInt("wallet"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        preferenceManager.putString(uniqueId, jsonObjectData.getString("unique_id"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONObject groupObject = jsonObjectData.getJSONObject("groups");
                        Iterator x = groupObject.keys();
                        JSONArray groupArray = new JSONArray();

                        while (x.hasNext()) {
                            String key = (String) x.next();
                            groupArray.put(groupObject.get(key));
                        }
                        preferenceManager.putString(user_id, groupArray.getJSONObject(0).getString("user_id"));
                        if (!groupArray.getJSONObject(0).getString("name").equalsIgnoreCase("admin") && !groupArray.getJSONObject(0).getString("name").equalsIgnoreCase("user")) {
                            preferenceManager.putString(IS_VENDOR, "yes");
                        } else {
                            preferenceManager.putString(IS_VENDOR, "no");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        preferenceManager.putString(profilepic, jsonObjectData.getString("image"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Log.d("mail", jsonObjectData.getString("email"));
                        preferenceManager.putString(emailId, jsonObjectData.getString("email"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Log.d("mobile", jsonObjectData.getString("phone"));
                        preferenceManager.putString(MOBILE, jsonObjectData.getString("phone"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(mContext, MainCategoriesActivity.class);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaa  error  "+error.getMessage());
                Log.d("fetch user error", error.toString());
            }
        }) {
           /* @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return super.getBody();
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                //map.put("X_AUTH_TOKEN", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMDA4NCIsInVzZXJkZXRhaWwiOnsiaWQiOiIxMDAwODQiLCJpcF9hZGRyZXNzIjoiMjQwOTo0MDcwOjIxOWE6YTJjNDo6OGIwOmU4YTUiLCJ1c2VybmFtZSI6Imd1ampldGlzaHJhdmFuQGdtYWlsLmNvbSIsInVuaXF1ZV9pZCI6Ik5DVTAyMTEiLCJwYXNzd29yZCI6IiQyeSQwOCQ2b21iQXViMTFhTU9leFRCdUF2SDBPTVJsbFlTZ3FGa0JcL2luMzVQXC9USE94ZlwvUnFGNGhKLiIsInNhbHQiOm51bGwsImVtYWlsIjoiZ3VqamV0aXNocmF2YW5AZ21haWwuY29tIiwid2FsbGV0IjoiMC4wMCIsImFjdGl2YXRpb25fY29kZSI6bnVsbCwiZm9yZ290dGVuX3Bhc3N3b3JkX2NvZGUiOm51bGwsImZvcmdvdHRlbl9wYXNzd29yZF90aW1lIjpudWxsLCJyZW1lbWJlcl9jb2RlIjpudWxsLCJjcmVhdGVkX29uIjoiMTU5MDEyMTMxNSIsImxhc3RfbG9naW4iOm51bGwsImFjdGl2ZSI6IjEiLCJsaXN0X2lkIjoiMCIsImZpcnN0X25hbWUiOiJHdWpqZXRpIFNocmF2YW5rdW1hciIsImxhc3RfbmFtZSI6bnVsbCwiY29tcGFueSI6bnVsbCwicGhvbmUiOiIiLCJjcmVhdGVkX3VzZXJfaWQiOm51bGwsInVwZGF0ZWRfdXNlcl9pZCI6bnVsbCwiY3JlYXRlZF9hdCI6IjIwMjAtMDUtMjIgMDQ6MjE6NTUiLCJ1cGRhdGVkX2F0IjpudWxsLCJkZWxldGVkX2F0IjpudWxsLCJzdGF0dXMiOiIxIn0sInRpbWUiOjE1OTAxMjEzNjB9.gm-lTQiaLcLLYu4KIpjMorFcayjO77IZFulCRlwYlTk");
                System.out.println("aaaaaaaaa body  "+map.toString());
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    //internet availability
    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
        if (activeNetworkInfo != null) { // connected to the internet
            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        }
        Toast.makeText(context, /*R.string.no_Internet*/"No network connection", Toast.LENGTH_SHORT).show();
        return false;
    }


    //external storage permission checker
    private boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (mContext.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Storage permission is granted");
                return true;
            } else {
                Log.v(TAG, "Storage permission is revoked");
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Storage permission is granted");
            return true;
        }
    }

    //software update checker
    private void checkUpdate() {


        mAppUpdateManager = AppUpdateManagerFactory.create(this);

        InstallStateUpdatedListener installStateUpdatedListener = null;
        InstallStateUpdatedListener finalInstallStateUpdatedListener = installStateUpdatedListener;
        installStateUpdatedListener = new
                InstallStateUpdatedListener() {
                    @Override
                    public void onStateUpdate(InstallState state) {
                        if (state.installStatus() == InstallStatus.DOWNLOADED) {
                            popupSnackbarForCompleteUpdate();

                        } else if (state.installStatus() == InstallStatus.INSTALLED) {
                            if (mAppUpdateManager != null) {
                                mAppUpdateManager.unregisterListener(finalInstallStateUpdatedListener);

                            }

                        } else {
                            Log.i(TAG, "InstallStateUpdatedListener: state: " + state.installStatus());
                        }
                    }
                };

        mAppUpdateManager.registerListener(installStateUpdatedListener);

        mAppUpdateManager.getAppUpdateInfo().addOnSuccessListener(appUpdateInfo -> {

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                try {
                    mAppUpdateManager.startUpdateFlowForResult(
                            appUpdateInfo, AppUpdateType.FLEXIBLE, SplashScreen.this, RC_APP_UPDATE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }

            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackbarForCompleteUpdate();
            } else {
                Log.e(TAG, "checkForAppUpdateAvailability: something else");
            }
        });

    }


    private void popupSnackbarForCompleteUpdate() {

        Snackbar snackbar =
                Snackbar.make(
                        findViewById(R.id.splashlayout),
                        "New app is ready!",
                        Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction("Install", view -> {
            if (mAppUpdateManager != null) {
                mAppUpdateManager.completeUpdate();
            }
        });


        snackbar.setActionTextColor(getResources().getColor(R.color.MediumSeaGreen));
        snackbar.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_APP_UPDATE) {
            if (resultCode != RESULT_OK) {
                Log.e(TAG, "onActivityResult: app download failed");
            }
        }
    }

}
