package com.apticks.nextclickuser.Activities.addtocart;

import java.util.ArrayList;

public class CartModel {

    String id,name,required,item_field,sec_price;
    ArrayList<SecItems> sectionitemlist;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getItem_field() {
        return item_field;
    }

    public void setItem_field(String item_field) {
        this.item_field = item_field;
    }

    public String getSec_price() {
        return sec_price;
    }

    public void setSec_price(String sec_price) {
        this.sec_price = sec_price;
    }

    public ArrayList<SecItems> getSectionitemlist() {
        return sectionitemlist;
    }

    public void setSectionitemlist(ArrayList<SecItems> sectionitemlist) {
        this.sectionitemlist = sectionitemlist;
    }
}
