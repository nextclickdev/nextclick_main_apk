package com.apticks.nextclickuser.Activities.HospitalityActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.HospitalityAdapters.HospitalityDoctorsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.HospitalityPojos.DoctorPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.GET_DOCTORS;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;

public class HospitalityDoctorsActivity extends AppCompatActivity {

    private Context mContext;
    private ImageView back_imageView;
    private RecyclerView hospitality_doctors_recycler;
    private ArrayList<DoctorPojo> doctorPojoArrayList;
    private String vendorId, specaityName, specialityId;

    private String serviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospitality_doctors);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        try {
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra(VENDOR_ID))
                    vendorId = intent.getStringExtra(VENDOR_ID);
                if (intent.hasExtra("spec_name"))
                    specaityName = intent.getStringExtra("spec_name");
                if (intent.hasExtra("spec_id"))
                    specialityId = intent.getStringExtra("spec_id");
                if (intent.hasExtra(getString(R.string.service_id)))
                    serviceId = intent.getStringExtra(getString(R.string.service_id));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getDoctors();
    }

    private void getDoctors() {
        DialogOpener.dialogOpener(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_DOCTORS + vendorId + "/" + specialityId + "/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    DialogOpener.dialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            try {

                                JSONArray dataArray = jsonObject.getJSONArray("data");
                                if (dataArray.length() > 0) {
                                    doctorPojoArrayList = new ArrayList<>();
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        JSONObject doctorObject = dataArray.getJSONObject(i);
                                        DoctorPojo doctorPojo = new DoctorPojo();
                                        doctorPojo.setId(doctorObject.getString("id"));
                                        //doctorPojo.setSpeciality_id(doctorObject.getString("speciality_id"));
                                        doctorPojo.setSpeciality_id("");
                                        doctorPojo.setName(doctorObject.getString("name"));
                                        doctorPojo.setDesc(doctorObject.getString("desc"));
                                        doctorPojo.setQualification(doctorObject.getString("qualification"));
                                        doctorPojo.setExperience(doctorObject.getString("experience"));
                                        doctorPojo.setLanguages(doctorObject.getString("languages"));
                                        doctorPojo.setFee(doctorObject.getInt("fee"));
                                        doctorPojo.setDiscount(doctorObject.getInt("discount"));
                                        doctorPojo.setStatus(doctorObject.getInt("status"));
                                        doctorPojo.setImage(doctorObject.getString("image"));
                                        doctorPojo.setVendor_id(vendorId);
                                        doctorPojo.setSpecialization(specaityName);

                                        doctorPojoArrayList.add(doctorPojo);

                                    }

                                    HospitalityDoctorsAdapter hospitalityDoctorsAdapter = new HospitalityDoctorsAdapter(mContext, doctorPojoArrayList, specialityId, serviceId);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                    hospitality_doctors_recycler.setLayoutManager(layoutManager);
                                    hospitality_doctors_recycler.setAdapter(hospitalityDoctorsAdapter);

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UImsgs.showCustomToast(mContext, "No data available", INFO);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    DialogOpener.dialog.dismiss();
                    UImsgs.showCustomToast(mContext, "Server is under maintenance", INFO);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(mContext, "Oops! Something went wrong", ERROR);

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void init() {
        mContext = HospitalityDoctorsActivity.this;
        back_imageView = findViewById(R.id.back_imageView);
        hospitality_doctors_recycler = findViewById(R.id.hospitality_doctors_recycler);
    }
}