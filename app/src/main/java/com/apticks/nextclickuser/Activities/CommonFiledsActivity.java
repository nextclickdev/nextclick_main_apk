package com.apticks.nextclickuser.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.telephony.gsm.SmsManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.apticks.nextclickuser.Adapters.TimingListAdapter;
import com.apticks.nextclickuser.Helpers.Utility;
import com.apticks.nextclickuser.Helpers.Validations;
import com.apticks.nextclickuser.Helpers.netwrokHelpers.ConnectionDetector;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.apticks.nextclickuser.Config.Config.CATEGORY_LIST;
import static com.apticks.nextclickuser.Config.Config.MainAppVendorRegistration;
import static com.apticks.nextclickuser.Config.Config.States;
import static com.apticks.nextclickuser.Constants.Constants.EMAIL;
import static com.apticks.nextclickuser.Constants.Constants.MOBILE;
import static com.apticks.nextclickuser.Constants.Constants.SUCCESS;
import static com.apticks.nextclickuser.Constants.IErrors.EMPTY;
import static com.apticks.nextclickuser.Constants.IErrors.INVALID;
import static com.apticks.nextclickuser.Constants.IErrors.TIMEINAVLID;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class CommonFiledsActivity extends AppCompatActivity implements LocationListener {


    Validations validations;

    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    Double lattitude, longitude;
    String otp_str;
    Bundle bundle;
    Context mContext;
    SharedPreferences sharedPreferences;
    PreferenceManager preferenceManager;
    String token;
    String sID;
    int PLACE_PICKER_REQUEST = 1;
    int count = 0;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1, SELECT_MULTIPLE_FILE = 1;
    private String userChoosenTask;
    public static String coverPhotStringImageUrl1 = "";
    public static String bannerPhotStringImageUrl1 = "";
    public int flag = 0;
    char imageSelection;
    LocationManager locationManager;
    ConnectionDetector connectionDetector;

    ArrayList<String> openingTimeList = new ArrayList<String>();
    ArrayList<String> closingTimeList = new ArrayList<String>();


    ArrayList<String> selectedSubCategoryList = new ArrayList<String>();
    ArrayList<String> selectedAmenitiesList = new ArrayList<String>();
    ArrayList<String> selectedServicesList = new ArrayList<String>();
    ArrayList<String> holidayList = new ArrayList<>();
    ArrayList<String> statesId = new ArrayList<>();
    ArrayList<String> states = new ArrayList<>();
    ArrayList<String> districtsId = new ArrayList<>();
    ArrayList<String> districts = new ArrayList<>();
    ArrayList<String> constituenciesId = new ArrayList<>();
    ArrayList<String> constituencies = new ArrayList<>();
    ArrayList<String> categoryId = new ArrayList<>();
    ArrayList<String> categories = new ArrayList<>();
    ArrayList<String> timings = new ArrayList<>();
    Map<String, String> bannerBase64 = new HashMap<>();
    Map<String, String> servicesMap = new HashMap<>();
    Map<String, String> amenitiesMap = new HashMap<>();
    Map<String, Object> subCategorymap = new HashMap<>();
    Map<String, String> subCategorydatamap = new HashMap<>();
    Map<String, String> servicesdatamap = new HashMap<>();
    Map<String, String> amenitiesdatamap = new HashMap<>();
    ArrayList<String> amenitiesList = new ArrayList<>();
    ArrayList<String> servicesLits = new ArrayList<>();
    ArrayList<String> subcategoriesList = new ArrayList<>();

    ArrayList<String> servicesids = new ArrayList<>();
    ArrayList<String> selectedservicesid = new ArrayList<>();

    ArrayList<String> amenitiesids = new ArrayList<>();
    ArrayList<String> selectedamenitiesids = new ArrayList<>();

    ArrayList<String> subcategoryids = new ArrayList<>();
    ArrayList<String> selectedsubcategoryids = new ArrayList<>();


    EditText newListName, location_et, pincode, complete_address,
            landmark, std_code, landline, mobile, helpline, email,
            whatsapp, facebook_link, instagram_link, website_url,
            everyday_opening_time, everyday_closing_time, twitter_link,
            holiday_opening_time, holiday_closing_time, verify_mobile,
            otp_E_text, referal_id, business_pwd;
    TextView taptoopenmap, add_it;
    LinearLayout fieldsLayout, servicelayout, timings_list_layout;
    ListView timing_listView;
    ScrollView scroll;
    Button /*next,*/ submit;
    CheckBox terms, sun, mon, tue, wed, thu, fri, sat;
    ImageView back, cover_photo, add_cover_photo, add_banner_photo;
    ImageView banner_photo;
    Spinner state_spinner, district_spinner, constituency_spinner, categories_spinner;

    ListView servicesListview, aminitiesListView, subCategoryListView, selectedSubcategoryListView;

    WebView webView;

    String newListName_str, location_et_str, pincode_str,
            complete_address_str, landmark_str, std_code_str,
            landline_str, mobile_str, helpline_str, email_str,
            whatsapp_str, facebook_link_str, instagram_link_str,
            website_url_str, twitter_link_str, constId, catId,
            everyday_open_str, everyday_close_str, holiday_open_str,
            holiday_close_str, password_str;

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;


    ProgressDialog progressDialog;
    AlertDialog.Builder builder;

    String termshtml;


    //private String imagePath;
    private List<String> pathList;
    String imageEncoded;


    int range = 9;  // to generate a single number with this range, by default its 0..9
    int length = 4; // by default length is 4


    //firebase related
    private static final String TAG = "PhoneAuthActivity";

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private ViewGroup mPhoneNumberViews;
    private ViewGroup mSignedInViews;

    private TextView mStatusText;
    private TextView mDetailText;

    private EditText mPhoneNumberField;
    private EditText mVerificationField;

    private Button mStartButton;
    private Button mVerifyButton;
    private Button mResendButton;
    private Button mSignOutButton;


    @Override
    protected void onResume() {
        super.onResume();

        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UImsgs.showToast(mContext, "Check Your connection");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UImsgs.showToast(mContext, "Check Your connection");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_fileds);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        mContext = getApplicationContext();
        init();
        connectionDetector = new ConnectionDetector(mContext);

        validations = new Validations();
        preferenceManager = new PreferenceManager(mContext);

        servicelayout.setVisibility(View.GONE);

        //progressBar.setVisibility(View.VISIBLE);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching fields data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
        asyncTaskRunner.execute();


        //Chechking Versions
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk <= android.os.Build.VERSION_CODES.M) {
            //submit.setBackgroundResource( R.drawable.border_button);
            //submit.setBackground(mContext.getColor(#F26B35));
        } else {
            //submit.setBackground(ContextCompat.getDrawable(context, R.drawable.ready));
            //add.setBackgroundResource( R.drawable.border_button);
            add_it.setBackgroundResource(R.drawable.border_button);
            //next.setBackgroundResource( R.drawable.border_button);
            submit.setBackgroundResource(R.drawable.border_button);
        }


        getLocation();
        statesDataFetcher();
        categoryDataFetcher();
        builder = new AlertDialog.Builder(this);
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.activity_web, null);

                webView = alertLayout.findViewById(R.id.ss_web);

                AlertDialog.Builder alert = new AlertDialog.Builder(CommonFiledsActivity.this);
                alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle("T&C");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                alert.setCancelable(false);
                WebSettings webSettings = webView.getSettings();
                Resources res = getResources();
                int fontSize = 10;
                webSettings.setDefaultFontSize(fontSize);
                webSettings.setJavaScriptEnabled(true);
                webView.loadDataWithBaseURL(null, termshtml, "text/html; charset=UTF-8", "utf-8", null);
                alert.setCancelable(true);
                alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        terms.setChecked(true);
                    }
                })
                        .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                terms.setChecked(false);
                            }
                        });
                AlertDialog dialog = alert.create();
                dialog.show();

            }
        });


        token = preferenceManager.getString(TOKEN_KEY);

        System.out.println("aaaaaaaa  token  "+token);
        try{
            verify_mobile.setText(preferenceManager.getString(MOBILE));
        }catch (NullPointerException e){

        }
        try{
            email.setText(preferenceManager.getString(EMAIL));
        }catch (NullPointerException e){

        }

        bundle = getIntent().getExtras();


        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }


        add_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timings_list_layout.setVisibility(View.VISIBLE);

                String open = everyday_opening_time.getText().toString().trim();
                String close = everyday_closing_time.getText().toString().trim();
                if (open.length() > 0 && close.length() > 0) {

                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    Date opentime = null;
                    Date closetime = null;
                    try {
                        opentime = sdf.parse(open);
                        closetime = sdf.parse(close);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long reqOpen = opentime.getTime();
                    long reqClose = closetime.getTime();

                    if (openingTimeList.size() > 0 && closingTimeList.size() > 0) {
                        Date checkertime = null;
                        try {
                            checkertime = sdf.parse(closingTimeList.get(closingTimeList.size() - 1));

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long timechecker = checkertime.getTime();

                        if (reqOpen > timechecker) {

                            openingTimeList.add(open);
                            closingTimeList.add(close);

                        } else {
                            everyday_opening_time.requestFocus();
                            Toast.makeText(mContext, "please choose valid timings", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        openingTimeList.add(open);
                        closingTimeList.add(close);
                        everyday_opening_time.setText("".trim());
                        everyday_opening_time.setHint("00:00");
                        everyday_closing_time.setText("".trim());
                        everyday_closing_time.setHint("00:00");
                    }
                    ViewGroup.LayoutParams params;
                    params = timings_list_layout.getLayoutParams();
                    params.height = openingTimeList.size() * 135;
                    timings_list_layout.setLayoutParams(params);
                    TimingListAdapter timingListAdapter = new TimingListAdapter(getApplicationContext(), R.layout.timing_list_supporter, 1, openingTimeList, closingTimeList);
                    timing_listView.setAdapter(timingListAdapter);
                    timingListAdapter.notifyDataSetChanged();

                }


            }
        });

        /*verify_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 10) {
                    //Toast.makeText(MainActivity.this, "ok", Toast.LENGTH_SHORT).show();

                    sendSMS(verify_mobile.getText().toString().trim(),generateRandomNumber());

                    //Toast.makeText(MainActivity.this, s.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        timing_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openingTimeList.remove(position);
                closingTimeList.remove(position);
                TimingListAdapter timingListAdapter = new TimingListAdapter(getApplicationContext(), R.layout.timing_list_supporter, 1, openingTimeList, closingTimeList);
                timing_listView.setAdapter(timingListAdapter);
                timingListAdapter.notifyDataSetChanged();
                //Toast.makeText(mContext, timings.size() + "", Toast.LENGTH_SHORT).show();
            }
        });


        everyday_opening_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CommonFiledsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        everyday_opening_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        everyday_closing_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CommonFiledsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        everyday_closing_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        holiday_opening_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CommonFiledsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        holiday_opening_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        holiday_closing_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CommonFiledsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        holiday_closing_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();

            }
        });

        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ((position - 1) >= 0) {
                    sID = statesId.get(position - 1);
                    districtsId.clear();
                    districts.clear();
                    districtsDataFetcher(sID);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        district_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ((position - 1) >= 0 && sID != null) {
                    String dID = districtsId.get(position - 1);
                    constituencies.clear();
                    constituenciesId.clear();
                    constituenciesDataFetcher(sID, dID);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        add_cover_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'c';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
            }
        });

        add_banner_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'b';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
               /* Intent mIntent = new Intent(getApplicationContext(), PickImageActivity.class);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 60);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 3);
                startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
*/

            }
        });

       /* next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dataFetcherFromFields();
                if (validator()) {

                    servicesDataFetcher(catId);
                     //fieldsLayout.setVisibility(View.GONE);
                    servicelayout.setVisibility(View.VISIBLE);
                    servicelayout.requestFocus();
                }
            }
        });*/

        categories_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!categories_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    servicesDataFetcher(categoryId.get(categories_spinner.getSelectedItemPosition() - 1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dataFetcherFromFields();
                if (validator()) {
                    if (terms.isChecked()) {
                        /*progressDialog = new ProgressDialog(CommonFiledsActivity.this);
                        progressDialog.setIcon(R.drawable.nextclick_logo_black);
                        progressDialog.setMessage("Please wait while sending otp.....");
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.setProgress(0);
                        progressDialog.setCancelable(false);
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.show();*/
                        mailValidator(progressDialog);
                    } else {
                        UImsgs.showToast(mContext, "Please accept terms and conditions");
                    }
                }
            }
        });

       /* timing_listView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        aminitiesListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        servicesListview.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        subCategoryListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        selectedSubcategoryListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });*/
        /*Log.i("CHECK", subCategoryListView.getChildCount()+"");*/
        final ArrayAdapter<String> subcategoryadapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line, selectedSubCategoryList);
        subCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int len = subCategoryListView.getCount();
                SparseBooleanArray checked = subCategoryListView.getCheckedItemPositions();

                String item = subcategoriesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedSubCategoryList.add(item);
                    selectedsubcategoryids.add(subcategoryids.get(position));
                    subcategoryadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                } else {
                    selectedSubCategoryList.remove(item);
                    selectedsubcategoryids.remove(subcategoryids.get(position));
                    subcategoryadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                }
            }
        });
        aminitiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int len = aminitiesListView.getCount();
                SparseBooleanArray checked = aminitiesListView.getCheckedItemPositions();

                String item = amenitiesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedamenitiesids.add(amenitiesids.get(position));
                } else {
                    selectedamenitiesids.remove(amenitiesids.get(position));
                }

            }
        });

        servicesListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int len = servicesListview.getCount();
                SparseBooleanArray checked = servicesListview.getCheckedItemPositions();

                //String item = amenitiesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedservicesid.add(servicesids.get(position));
                } else {
                    selectedservicesid.remove(servicesids.get(position));
                }


            }
        });
        /*selectedSubcategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int len = subCategoryListView.getCount();
                SparseBooleanArray checked = subCategoryListView.getCheckedItemPositions();

                if (checked.get(position)) {
                    String item = timings.get(position);
                    *//* do whatever you want with the checked item *//*
                    selectedSubCategoryList.add(item);

                    subcategoryadapter.setDropDownViewResource(android.R.fieldsLayout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                }
            }
        });*/
    }

    class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("No Connection..."); // Calls onProgressUpdate()
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                /*Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }

            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
               /* progressDialog.dismiss();
                finalResult.setText(result);*/
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                /*Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }


        }


        @Override
        protected void onPreExecute() {
               /* progressDialog = ProgressDialog.show(MainActivity.this,
                        "ProgressDialog",
                        "Wait for "+time.getText().toString()+ " seconds");*/
           /* if(!connectionDetector.isConnectingToInternet()){
                resp="No Internet";
                Intent intent = new Intent(mContext,ConnectionAlert.class);
                startActivity(intent);
                finish();
            }
            else {
                resp = "Connected";
            }
*/
        }


        @Override
        protected void onProgressUpdate(String... text) {
            /*finalResult.setText(text[0]);*/
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
               /* Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }


        }
    }


    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            /*Toast.makeText(this, arr.length+"", Toast.LENGTH_SHORT).show();*/

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            lattitude = lat1;
            longitude = lang1;
            location_et.setText(address);


        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

//Location End


    //Image Selection Start

    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CommonFiledsActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)

            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CommonFiledsActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            if (imageSelection == 'c') {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                cover_photo.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) cover_photo.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();
                coverPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            }
            if (imageSelection == 'b') {
                bannerBase64.clear();
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                banner_photo.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) banner_photo.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();
                bannerPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                bannerBase64.put(0 + "".trim(), bannerPhotStringImageUrl1);
            }
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }



       /* if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {

            this.pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");

                ArrayList<Bitmap> bitmapList = new ArrayList<>();
                for (int i = 0; i < pathList.size(); i++) {
                   *//* sb.append("Photo"+(i+1)+":"+pathList.get(i));
                    sb.append("\n");*//*
                    Bitmap bitmap1 = BitmapFactory.decodeFile(pathList.get(i));//assign your bitmap;
                   *//* Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;
                    Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;
                    Bitmap bitmap4 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;*//*


                    bitmapList.add(bitmap1);

                    Bitmap mergedImg = mergeMultiple(bitmapList);

                    banner_photo.setImageBitmap(mergedImg);

                }
                //tvResult.setText(sb.toString()); // here this is textview for sample use...

                onSelectFromGalleryResult(bitmapList);
                // Toast.makeText(mContext, sb.toString() + "", Toast.LENGTH_SHORT).show();
            }
        }*/

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);

        if (imageSelection == 'c') {

            cover_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) cover_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);

            byte[] byteArray = baos.toByteArray();
            coverPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }
        if (imageSelection == 'b') {

            banner_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) banner_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);

            byte[] byteArray = baos.toByteArray();
            bannerPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);
            bannerBase64.put(0 + "".trim(), bannerPhotStringImageUrl1);

            flag = 1;
        }

    }

    private void onSelectFromGalleryResult(ArrayList data) {


        bannerBase64.clear();

        for (int i = 0; i < data.size(); i++) {

            Bitmap bitmap = (Bitmap) data.get(i);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            String bannerBitString = Base64.encodeToString(byteArray, Base64.DEFAULT);
            bannerBase64.put(i + "".trim(), bannerBitString);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);


            flag = 1;
        }


    }


    //Image Selection End


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(CommonFiledsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getLocation();
        }
    }


    public void statesDataFetcher() {


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            statesId.add(dataObject.getString("id"));
                            states.add(dataObject.getString("name"));
                        }
                        // progressBar.setVisibility(View.GONE);

                    } else {
                        UImsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void districtsDataFetcher(String stateid) {
        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("districts");
                        districts.add("Select");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            districtsId.add(districtObject.getString("id"));
                            districts.add(districtObject.getString("name"));
                        }

                    } else {
                        UImsgs.showToast(getApplicationContext(), messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, districts);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void constituenciesDataFetcher(String stateid, String districtid) {
        progressDialog.show();
        final int err = 0;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid + "/" + districtid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("constituencies_resp", response);

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        constituencies.add("Select");
                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("constituenceis");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            constituenciesId.add(districtObject.getString("id"));
                            constituencies.add(districtObject.getString("name"));
                        }

                    } else {
                        UImsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, constituencies);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    constituency_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    // UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();


            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void categoryDataFetcher() {


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            categoryId.add(dataObject.getString("id"));
                            categories.add(dataObject.getString("name"));
                        }

                    } else {
                        UImsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, categories);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    categories_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CommonFiledsActivity.this, error + "", Toast.LENGTH_SHORT).show();
                categoryDataFetcher();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    public void servicesDataFetcher(final String categoryId) {
        //String tempCategories = Categories + "1";
        String tempCategories = CATEGORY_LIST + categoryId;

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, tempCategories, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    Boolean type;
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    String datatype = jsonData.get("data").getClass().getName();
                    if (datatype.equalsIgnoreCase("java.lang.Boolean")) {
                        type = false;
                    } else
                        type = true;
                    //Toast.makeText(mContext, String.valueOf(messagae), Toast.LENGTH_SHORT).show();
                    if (type) {
                        if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                            amenitiesdatamap.clear();
                            amenitiesList.clear();
                            amenitiesids.clear();
                            amenitiesMap.clear();
                            subcategoryids.clear();
                            subcategoriesList.clear();
                            subCategorydatamap.clear();
                            subCategorymap.clear();
                            servicesids.clear();
                            servicesLits.clear();
                            servicesdatamap.clear();
                            servicesMap.clear();

                            String data = jsonData.getString("data");

                            JSONObject jsonObject = jsonData.getJSONObject("data");

                            termshtml = jsonObject.getString("terms");
                            LinearLayout aminities_layout,
                                    services_layout, sub_categories_layout;
                            aminities_layout = findViewById(R.id.aminities_layout);
                            services_layout = findViewById(R.id.services_layout);
                            sub_categories_layout = findViewById(R.id.sub_categories_layout);


                            JSONArray amenitiesJsonArray = jsonObject.getJSONArray("amenities");
                            //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                            if (amenitiesJsonArray.length() > 0) {

                                for (int i = 0; i < amenitiesJsonArray.length(); i++) {
                                    JSONObject ammenitiesArray = (JSONObject) amenitiesJsonArray.get(i);
                                    String amenityname = ammenitiesArray.getString("name");
                                    String amenityid = ammenitiesArray.getString("id");
                                    String amenitycatId = ammenitiesArray.getString("cat_id");
                                    amenitiesdatamap.put(amenityid, amenityname);
                                    amenitiesList.add(amenityname);
                                    amenitiesids.add(amenityid);

                                }

                                ViewGroup.LayoutParams params;
                                params = aminities_layout.getLayoutParams();
                                params.height = amenitiesList.size() * 250;
                                aminities_layout.setLayoutParams(params);

                                aminitiesListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                ArrayAdapter<String> amenitiesAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, amenitiesList);
                                amenitiesAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                aminitiesListView.setAdapter(amenitiesAdapter);
                            } else {
                                UImsgs.showToast(mContext, "Sorry...! No Amenities Found");
                            }


                            JSONArray subCategoryJsonArray = jsonObject.getJSONArray("sub_categories");
                            if (subCategoryJsonArray.length() > 0) {
                                for (int i = 0; i < subCategoryJsonArray.length(); i++) {
                                    JSONObject subCategoryObject = (JSONObject) subCategoryJsonArray.get(i);
                                    String subcategoryname = subCategoryObject.getString("name");
                                    String subcategoryid = subCategoryObject.getString("id");
                                    String subcategorycatId = subCategoryObject.getString("cat_id");
                                    subCategorydatamap.put(subcategoryid, subcategoryname);
                                    subcategoriesList.add(subcategoryname);
                                    subcategoryids.add(subcategoryid);


                                }

                                ViewGroup.LayoutParams params;
                                params = sub_categories_layout.getLayoutParams();
                                /* if(subcategoriesList.size()<=3){*/
                                params.height = subcategoriesList.size() * 300;
                               /* }else{
                                    params.height = subcategoriesList.size()*155;
                                }*/

                                sub_categories_layout.setLayoutParams(params);

                                ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, subcategoriesList);
                                subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                subCategoryListView.setAdapter(subcategoryAdapter);
                            } else {
                                UImsgs.showToast(mContext, "Sorry...! No SubCategories Found");
                            }


                            JSONObject serviceJsonobject = jsonObject.getJSONObject("services");

                            Iterator x = serviceJsonobject.keys();
                            JSONArray servicesjsonArray = new JSONArray();


                            while (x.hasNext()) {
                                String key = (String) x.next();
                                servicesjsonArray.put(serviceJsonobject.get(key));
                            }
                            //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                            if (servicesjsonArray.length() > 0) {
                                for (int i = 0; i < servicesjsonArray.length(); i++) {
                                    JSONObject servicesData = (JSONObject) servicesjsonArray.get(i);
                                    String servicename = servicesData.getString("name");
                                    String serviceid = servicesData.getString("id");
                                    String servicecatId = servicesData.getString("cat_id");
                                    servicesdatamap.put(serviceid, servicename);
                                    servicesLits.add(servicename);
                                    servicesids.add(serviceid);

                                }

                                ViewGroup.LayoutParams serviceparams = services_layout.getLayoutParams();
                                if (servicesLits.size() <= 2) {
                                    serviceparams.height = servicesLits.size() * 500;
                                } else {
                                    serviceparams.height = servicesLits.size() * 250;
                                }

                                //Toast.makeText(mContext, serviceparams.height+"", Toast.LENGTH_SHORT).show();
                                services_layout.setLayoutParams(serviceparams);
                                ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, servicesLits);
                                servicesListview.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                serviceAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                servicesListview.setAdapter(serviceAdapter);
                            } else {
                                UImsgs.showToast(mContext, "Sorry...! No Services Found");
                            }


                        } else {
                            UImsgs.showToast(mContext, messagae);
                            //Toast.makeText(ServicesAminitiesActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        UImsgs.showToast(mContext, "Sorry..! No data found for the selected category");
                        fieldsLayout.setVisibility(View.VISIBLE);
                        servicelayout.setVisibility(View.GONE);
                    }


                    /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.fieldsLayout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.fieldsLayout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);*/


                } catch (JSONException e) {
                    e.printStackTrace();
                    //UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
              /*  ServicesAminitiesAdapter servicesAminitiesAdapter = new ServicesAminitiesAdapter(mContext,serviceAminitesList);
                amenitiesRecycle.setAdapter(servicesAminitiesAdapter);*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CommonFiledsActivity.this, error + "", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };

        /*stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
        requestQueue.add(stringRequest);


    }


    public void dataFetcherFromFields() {

        newListName_str = newListName.getText().toString().trim();
        location_et_str = location_et.getText().toString().trim();
        pincode_str = pincode.getText().toString().trim();
        complete_address_str = complete_address.getText().toString().trim();
        landmark_str = landmark.getText().toString().trim();
        std_code_str = std_code.getText().toString().trim();
        landline_str = landline.getText().toString().trim();
        mobile_str = mobile.getText().toString().trim();
        helpline_str = helpline.getText().toString().trim();
        email_str = email.getText().toString().trim();
        password_str = business_pwd.getText().toString().trim();
        whatsapp_str = whatsapp.getText().toString().trim();
        facebook_link_str = facebook_link.getText().toString().trim();
        instagram_link_str = instagram_link.getText().toString().trim();
        website_url_str = website_url.getText().toString().trim();
        twitter_link_str = twitter_link.getText().toString().trim();
        /*constId= constituenciesId.get(constituency_spinner.getSelectedItemPosition()-1);
        catId= categoryId.get(categories_spinner.getSelectedItemPosition()-1);*/
        if (constituencies.size() > 0) {
            if (!constituency_spinner.getSelectedItem().toString().equalsIgnoreCase("select"))
                constId = constituenciesId.get(constituency_spinner.getSelectedItemPosition() - 1);
            else
                constId = null;
        } else
            constId = null;
        if (categories.size() > 0) {
            if (!categories_spinner.getSelectedItem().toString().equalsIgnoreCase("select"))
                catId = categoryId.get(categories_spinner.getSelectedItemPosition() - 1);
            else
                catId = null;
        } else
            catId = null;


        everyday_open_str = everyday_opening_time.getText().toString().trim();
        everyday_close_str = everyday_closing_time.getText().toString().trim();
        holiday_open_str = holiday_opening_time.getText().toString().trim();
        holiday_close_str = holiday_closing_time.getText().toString().trim();


        if (sun.isChecked()) {
            holidayList.add("1");
        }
        if (mon.isChecked()) {
            holidayList.add("2");
        }
        if (tue.isChecked()) {
            holidayList.add("3");
        }
        if (wed.isChecked()) {
            holidayList.add("4");
        }
        if (thu.isChecked()) {
            holidayList.add("5");
        }
        if (fri.isChecked()) {
            holidayList.add("6");
        }
        if (sat.isChecked()) {
            holidayList.add("7");
        }


    }

    public void dataSender() {
        progressDialog = new ProgressDialog(CommonFiledsActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while uploading the data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        Map<String, String> socialdata = new HashMap<>();
        socialdata.put("1", facebook_link_str);
        socialdata.put("2", twitter_link_str);
        socialdata.put("3", instagram_link_str);
        socialdata.put("4", website_url_str);

        Map<String, Map<String, String>> contact = new HashMap<>();
        Map<String, String> one = new HashMap<>();
        one.put("number", mobile_str);
        one.put("code", "+91");
        Map<String, String> two = new HashMap<>();
        two.put("number", landline_str);
        two.put("code", std_code_str);
        Map<String, String> three = new HashMap<>();
        three.put("number", whatsapp_str);
        three.put("code", "+91");
        Map<String, String> four = new HashMap<>();
        four.put("number", helpline_str);
        four.put("code", "");
        contact.put("1", one);
        contact.put("2", two);
        contact.put("3", three);
        contact.put("4", four);

        Map<String, String> holiday = new HashMap<>();
        if (holidayList.size() > 0) {
            for (int i = 0; i < holidayList.size(); i++) {
                holiday.put(i + "".trim(), holidayList.get(i));
            }
        }

        Map<String, Object> timingsMap = new HashMap<>();

        for (int i = 0; i < openingTimeList.size(); i++) {

            Map<String, Object> insideTimings = new HashMap<>();
            insideTimings.put("start_time", openingTimeList.get(i));
            insideTimings.put("end_time", closingTimeList.get(i));

            timingsMap.put(i + "".trim(), insideTimings);

        }


        Map<String, Object> mainData = new HashMap<>();

        mainData.put("ref_id", referal_id.getText().toString().trim());
        mainData.put("name", newListName_str);
        //mainData.put("password", password_str);
        mainData.put("email", email_str);
        mainData.put("constituency_id", constId);
        mainData.put("category_id", catId);
       /* mainData.put("constituency_id", "1");
        mainData.put("category_id", "5");*/
        mainData.put("address", complete_address_str);
        mainData.put("landmark", landmark_str);
        mainData.put("pincode", "000000");
        mainData.put("timings", timingsMap);
        if (holiday_open_str.equalsIgnoreCase("select") | holiday_close_str.equalsIgnoreCase("select")) {
            mainData.put("holiday_open_time", "");
            mainData.put("holiday_close_time", "");
        } else {
            mainData.put("holiday_open_time", holiday_open_str);
            mainData.put("holiday_close_time", holiday_close_str);
        }
        mainData.put("cover", coverPhotStringImageUrl1);
        Log.d("cover", coverPhotStringImageUrl1);

        mainData.put("banner", bannerBase64);
        Log.d("banner", bannerBase64.toString());

        mainData.put("location_address", location_et_str);
        mainData.put("latitude", lattitude);
        mainData.put("longitude", longitude);

        mainData.put("holidays", holiday);
        mainData.put("contacts", contact);
        mainData.put("social", socialdata);


        //JSONObject json = new JSONObject(mainData);
        //submission(json, progressDialog);
        ObjectMapper mapperObj = new ObjectMapper();
        try {
            String jsonString = mapperObj.writeValueAsString(mainData);
            JSONObject json = new JSONObject(mainData);
            submission(jsonString, progressDialog);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Please select less size images", Toast.LENGTH_SHORT).show();
        }



    }

    public void dataClearance() {


        selectedSubCategoryList.clear();
        selectedAmenitiesList.clear();
        selectedServicesList.clear();
        holidayList.clear();

        coverPhotStringImageUrl1 = "".trim();
        banner_photo.setImageBitmap(null);
        cover_photo.setImageBitmap(null);
        bannerBase64.clear();
        servicesMap.clear();
        amenitiesMap.clear();
        subCategorymap.clear();
        subCategorydatamap.clear();
        servicesdatamap.clear();
        amenitiesdatamap.clear();
        amenitiesList.clear();
        servicesLits.clear();
        subcategoriesList.clear();

        servicesids.clear();
        selectedservicesid.clear();

        amenitiesids.clear();
        selectedamenitiesids.clear();

        subcategoryids.clear();
        selectedsubcategoryids.clear();

    }


    public void mailValidator(final ProgressDialog progressDialog) {



        /*RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EMAIL_VERIFICATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {

                            try {

                                JSONObject object = new JSONObject(response);
                                String status = object.getString("status");
                                if (status.equalsIgnoreCase("true")) {

                                    Toast.makeText(CommonFiledsActivity.this, "OTP has been sent to your mail", Toast.LENGTH_SHORT).show();
                                    //clearCache();

                                    LayoutInflater inflater = getLayoutInflater();
                                    View alertLayout = inflater.inflate(R.layout.otp_layout, null);

                                    otp_E_text =alertLayout.findViewById(R.id.otp_next);

                                    AlertDialog.Builder alert = new AlertDialog.Builder(CommonFiledsActivity.this);
                                    alert.setIcon(R.mipmap.ic_launcher);
                                    alert.setTitle("OTP");
                                    // this is set the view from XML inside AlertDialog
                                    alert.setView(alertLayout);
                                    alert.setCancelable(false);


                                    alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            String otp_verify = otp_E_text.getText().toString().trim();
                                            if(otp_verify.length()==4){
                                                if(otp_str.equalsIgnoreCase(otp_verify)){
                                                    dataSender();

                                                }
                                                else{
                                                    Toast.makeText(mContext, "Invalid otp Please press submit button again", Toast.LENGTH_LONG).show();
                                                }
                                            }


                                        }
                                    });

                                    AlertDialog dialog = alert.create();
                                    dialog.show();




                                } else {
                                    progressDialog.dismiss();
                                    String message = object.getString("message");
                                    Toast.makeText(CommonFiledsActivity.this, Html.fromHtml(message), Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                Toast.makeText(CommonFiledsActivity.this, e + "", Toast.LENGTH_SHORT).show();

                            }
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(CommonFiledsActivity.this, *//*error +*//* "Please check the internet connection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email_str);
                params.put("sub", SUBJECT);
                params.put("mes", "HI SIR/MA'AM your asset : " +newListName_str.toUpperCase()+"is going to be connected with NEXTCLICK.\n Your OTP is : "+otp_str+" \n Please provide this otp to our executive to complete the REGISTRATION");

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);*/
        progressDialog.dismiss();
        dataSender();
/*

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_phone_auth, null);


        mPhoneNumberViews = alertLayout.findViewById(R.id.phoneAuthFields);
        mSignedInViews = alertLayout.findViewById(R.id.signedInButtons);

        mStatusText = alertLayout.findViewById(R.id.status);
        mDetailText = alertLayout.findViewById(R.id.detail);

        mPhoneNumberField = alertLayout.findViewById(R.id.fieldPhoneNumber);
        mVerificationField = alertLayout.findViewById(R.id.fieldVerificationCode);

        mStartButton = alertLayout.findViewById(R.id.buttonStartVerification);
        mVerifyButton = alertLayout.findViewById(R.id.buttonVerifyPhone);
        mResendButton = alertLayout.findViewById(R.id.buttonResend);
        mSignOutButton = alertLayout.findViewById(R.id.signOutButton);

        // Assign click listeners
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validatePhoneNumber()) {
                    return;
                }
                //signOut();

                startPhoneNumberVerification(mPhoneNumberField.getText().toString());
            }
        });
       */
/* mVerifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
            }
        });*//*

        mResendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendVerificationCode(mPhoneNumberField.getText().toString(), mResendToken);
            }
        });
        mSignOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        */
/*@Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.buttonStartVerification:
                    if (!validatePhoneNumber()) {
                        return;
                    }

                    startPhoneNumberVerification(mPhoneNumberField.getText().toString());
                    break;
                case R.id.buttonVerifyPhone:
                    String code = mVerificationField.getText().toString();
                    if (TextUtils.isEmpty(code)) {
                        mVerificationField.setError("Cannot be empty.");
                        return;
                    }

                    verifyPhoneNumberWithCode(mVerificationId, code);
                    break;
                case R.id.buttonResend:
                    resendVerificationCode(mPhoneNumberField.getText().toString(), mResendToken);
                    break;
                case R.id.signOutButton:
                    signOut();
                    break;
            }
        }*//*

        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                updateUI(STATE_VERIFY_SUCCESS, credential);
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    mPhoneNumberField.setError("Invalid phone number.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }

                // Show a message and update the UI
                // [START_EXCLUDE]
                updateUI(STATE_VERIFY_FAILED);
                // [END_EXCLUDE]
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                // [START_EXCLUDE]
                // Update UI
                updateUI(STATE_CODE_SENT);
                // [END_EXCLUDE]
            }
        };

        AlertDialog.Builder alert = new AlertDialog.Builder(CommonFiledsActivity.this);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setTitle("OTP");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        alert.setCancelable(false);


        alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                */
/*String otp_verify = otp_E_text.getText().toString().trim();
                if (otp_verify.length() == 4) {
                    if (otp_str.equalsIgnoreCase(otp_verify)) {
                        dataSender();

                    } else {
                        Toast.makeText(mContext, "Invalid otp Please press submit button again", Toast.LENGTH_LONG).show();
                    }
                }
*//*

                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
                //dataSender();

            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
*/


    }

    public void submission(final String json, final ProgressDialog progressDialog) {

        final String data = json;
        System.out.println("aaaaaaa  jsonn  "+json);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, MainAppVendorRegistration,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            Boolean status = jsonObject.getBoolean("status");
                            String http = jsonObject.getString("http_code");
                            String message = jsonObject.getString("message");
                            String data = jsonObject.getString("data");
                            System.out.println("aaaaaaa  "+jsonObject);
                            if (status && http.equalsIgnoreCase("201") && message.equalsIgnoreCase("Success..!")) {
                                dataClearance();
                                //signOut();//need to be check
                             //   Intent intent = new Intent(CommonFiledsActivity.this, FinishActivity.class);
                              //  startActivity(intent);
                                preferenceManager.putBoolean(getString(R.string.refresh_profile), true);
                                UImsgs.showCustomToast(CommonFiledsActivity.this, "Succefully Submitted", SUCCESS);
                              //  onBackPressed();
                                finish();

                            } else {
                                UImsgs.showToast(mContext, Html.fromHtml(data).toString());
                                Log.d("er msg", Html.fromHtml(data).toString());
                                progressDialog.dismiss();
                                servicelayout.setVisibility(View.GONE);
                                fieldsLayout.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            servicelayout.setVisibility(View.GONE);
                            fieldsLayout.setVisibility(View.VISIBLE);
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();
                UImsgs.showToast(mContext, "Some Error Occured.. Please provide data again...");
                progressDialog.dismiss();
                servicelayout.setVisibility(View.GONE);
                fieldsLayout.setVisibility(View.VISIBLE);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", token);

                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    public boolean validator() {

        boolean validity = true;


        if (newListName_str.length() == 0) {
            newListName.setError(EMPTY);
            newListName.requestFocus();
            validity = false;
        }
        if (location_et_str.length() == 0) {
            location_et.setError("Wait untill location fetched");
            location_et.requestFocus();
            validity = false;
        }
        /*if (pincode_str.length() == 0) {
            pincode.setError(EMPTY);
            pincode.requestFocus();
            validity = false;
        }
        if (pincode_str.length() < 6) {
            pincode.setError(PIN_INVALID);
            pincode.requestFocus();
            validity = false;
        }*/
        if (complete_address_str.length() == 0) {
            complete_address.setError(EMPTY);
            complete_address.requestFocus();
            validity = false;
        }
        if (complete_address_str.length() < 6) {
            complete_address.setError("Please provide complete details");
            complete_address.requestFocus();
            validity = false;
        }
        if (landmark_str.length() == 0) {
            landmark.setError(EMPTY);
            landmark.requestFocus();
            validity = false;
        }
        if (landmark_str.length() <= 6) {
            landmark.setError("Please provide clear details");
            landmark.requestFocus();
            validity = false;
        }
        if (openingTimeList.size() < 1) {
            Toast.makeText(mContext, "Please provide timings", Toast.LENGTH_SHORT).show();
            everyday_opening_time.requestFocus();
            validity = false;
        }
        if (std_code_str.length() > 0 && std_code_str.length() < 3) {
            std_code.setError(INVALID);
            std_code.requestFocus();
            validity = false;
        }
       /* if (landline_str.length() == 0) {
            landline.setError(EMPTY);
            landline.requestFocus();
            validity = false;
        }*/
        if (landline_str.length() > 0 && landline_str.length() < 6) {
            landline.setError(INVALID);
            landline.requestFocus();
            validity = false;
        }
        /*if (password_str.length() == 0) {
            business_pwd.setError(EMPTY);
            business_pwd.requestFocus();
            validity = false;
        }
        if (password_str.length() < 4) {
            business_pwd.setError("Minimum 4 Characters");
            business_pwd.requestFocus();
            validity = false;
        }*/
        if (mobile_str.length() == 0) {
            mobile.setError(EMPTY);
            mobile.requestFocus();
            validity = false;
        }
        if (mobile_str.length() < 10) {
            mobile.setError(INVALID);
            mobile.requestFocus();
            validity = false;
        }
        /*if (helpline_str.length() == 0) {
            helpline.setError(EMPTY);
            helpline.requestFocus();
            validity = false;
        }*/
        if (helpline_str.length() > 0 && helpline_str.length() < 10) {
            helpline.setError(INVALID);
            helpline.requestFocus();
            validity = false;
        }

        if (validations.isBlank(email_str)) {
            email.setError(EMPTY);
            email.requestFocus();
            validity = false;
        }

        if (validations.isValidEmail(email_str)) {
            email.setError(INVALID);
            email.requestFocus();
            validity = false;
        }
        if (whatsapp_str.length() == 0) {
            whatsapp.setError(EMPTY);
            whatsapp.requestFocus();
            validity = false;
        }
        if (whatsapp_str.length() < 10) {
            whatsapp.setError(INVALID);
            whatsapp.requestFocus();
            validity = false;
        }
        if (facebook_link_str.length() != 0) {

            if (facebook_link_str.length() < 15) {
                facebook_link.setError(INVALID);
                facebook_link.requestFocus();
                validity = false;
            }
        }

        if (instagram_link_str.length() != 0) {

            if (instagram_link_str.length() < 10) {
                instagram_link.setError(INVALID);
                instagram_link.requestFocus();
                validity = false;
            }
        }

        if (twitter_link_str.length() != 0) {

            if (twitter_link_str.length() < 10) {
                twitter_link.setError(INVALID);
                twitter_link.requestFocus();
                validity = false;
            }
        }

        if (website_url_str.length() != 0) {

            if (website_url_str.length() < 5) {
                website_url.setError(INVALID);
                website_url.requestFocus();
                validity = false;
            }
        }

        if (everyday_open_str.equalsIgnoreCase("select")) {
            everyday_opening_time.requestFocus();
            UImsgs.showToast(mContext, TIMEINAVLID);
            validity = false;
        }

        if (everyday_close_str.equalsIgnoreCase("select")) {
            everyday_closing_time.requestFocus();
            UImsgs.showToast(mContext, TIMEINAVLID);
            validity = false;
        }


        if (constId == null) {
            UImsgs.showToast(mContext, "Please Select Constituency");
            constituency_spinner.requestFocus();
            validity = false;
        }
        if (catId == null) {
            UImsgs.showToast(mContext, "Please Select Category");
            categories_spinner.requestFocus();
            validity = false;
        }
        if (coverPhotStringImageUrl1.length() <= 0 || coverPhotStringImageUrl1 == null) {
            UImsgs.showToast(mContext, "Please Select Cover photo");
            cover_photo.requestFocus();
            validity = false;
        }

        /*if (bannerBase64.size() <= 2) {
            UImsgs.showToast(mContext, "Please Select banner photos");
            banner_photo.requestFocus();
            validity = false;
        }*/


        /* constId, catId,  */


        return validity;

    }

    private Bitmap mergeMultiple(ArrayList<Bitmap> parts) {
        final int IMAGE_MAX_SIZE = 1200000;
        int value = parts.size();
        int height = parts.get(0).getHeight();
        int width = parts.get(0).getWidth();

        double y = Math.sqrt(IMAGE_MAX_SIZE
                / (((double) width) / height));
        double x = (y / height) * width;
        Bitmap result = Bitmap.createScaledBitmap(parts.get(0), (int) x, (int) y, true);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();

        for (int i = 0; i < parts.size(); i++) {
            canvas.drawBitmap(parts.get(i), parts.get(i).getWidth() * (i % 4), parts.get(i).getHeight() * (i / 4), paint);
        }
        return result;
    }

    public void init() {

        otp_str = generateRandomNumber();
        newListName = (EditText) findViewById(R.id.new_list_name);
        location_et = (EditText) findViewById(R.id.location);
        pincode = (EditText) findViewById(R.id.pincode);
        complete_address = (EditText) findViewById(R.id.complet_address);
        landmark = (EditText) findViewById(R.id.landmark);
        std_code = (EditText) findViewById(R.id.std_code);
        landline = (EditText) findViewById(R.id.landline);
        mobile = (EditText) findViewById(R.id.mobile);
        helpline = (EditText) findViewById(R.id.helpLine);
        email = (EditText) findViewById(R.id.email);
        whatsapp = (EditText) findViewById(R.id.whatsapp);
        facebook_link = (EditText) findViewById(R.id.fb_link);
        instagram_link = (EditText) findViewById(R.id.instagram_link);
        website_url = (EditText) findViewById(R.id.website_url);
        twitter_link = (EditText) findViewById(R.id.twitter_link);
        verify_mobile = (EditText) findViewById(R.id.verifymobile);
        //otp = (EditText) findViewById(R.id.otp);

        taptoopenmap = (TextView) findViewById(R.id.map_opener);
        add_it = (TextView) findViewById(R.id.add_one_more);

        fieldsLayout = (LinearLayout) findViewById(R.id.fieldsLayout);
        servicelayout = (LinearLayout) findViewById(R.id.service_n_aminities_layout);
        scroll = (ScrollView) findViewById(R.id.common_fields_scroll);
        timings_list_layout = (LinearLayout) findViewById(R.id.timings_list_layout);
        timing_listView = (ListView) findViewById(R.id.timings_list);

        //next = (Button) findViewById(R.id.next);
        submit = (Button) findViewById(R.id.submit);

        terms = (CheckBox) findViewById(R.id.terms);
        sun = (CheckBox) findViewById(R.id.check_sunday);
        mon = (CheckBox) findViewById(R.id.check_monday);
        tue = (CheckBox) findViewById(R.id.check_tuesday);
        wed = (CheckBox) findViewById(R.id.check_wednesday);
        thu = (CheckBox) findViewById(R.id.check_thursday);
        fri = (CheckBox) findViewById(R.id.check_friday);
        sat = (CheckBox) findViewById(R.id.check_saturday);

        back = (ImageView) findViewById(R.id.back);
        cover_photo = (ImageView) findViewById(R.id.cover_photo);
        add_cover_photo = (ImageView) findViewById(R.id.add_cover_photo);
        banner_photo = (ImageView) findViewById(R.id.banner_photo);

        add_banner_photo = (ImageView) findViewById(R.id.add_banner_photo);


        state_spinner = (Spinner) findViewById(R.id.state_spinner);
        district_spinner = (Spinner) findViewById(R.id.district_spinner);
        constituency_spinner = (Spinner) findViewById(R.id.constituency_spinner);
        categories_spinner = (Spinner) findViewById(R.id.category_spinner);
        everyday_opening_time = (EditText) findViewById(R.id.everyday_opening_spinner);
        everyday_closing_time = (EditText) findViewById(R.id.everyday_closing_spinner);
        holiday_opening_time = (EditText) findViewById(R.id.holiday_opening_spinner);
        holiday_closing_time = (EditText) findViewById(R.id.holiday_closing_spinner);
        referal_id = (EditText) findViewById(R.id.referal_id);
        business_pwd = (EditText) findViewById(R.id.business_pwd);


        states.add("Select");
        categories.add("Select");


        servicesListview = (ListView) findViewById(R.id.services_recycle);
        aminitiesListView = (ListView) findViewById(R.id.amenities_recycle);
        subCategoryListView = (ListView) findViewById(R.id.subCategory);
        subCategoryListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        selectedSubcategoryListView = (ListView) findViewById(R.id.selected_subcategory);

    }

    public String generateRandomNumber() {
        int randomNumber;

        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++) {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        //randomNumber = Integer.parseInt(s);

        return s;
    }


    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    // [END resend_verification]

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                           /* FirebaseUser user = task.getResult().getUser();
                            // [START_EXCLUDE]
                            updateUI(STATE_SIGNIN_SUCCESS, user);*/
                            // [END_EXCLUDE]
                            dataSender();


                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                mVerificationField.setError("Invalid code.");
                                // [END_EXCLUDE]
                            }
                            // [START_EXCLUDE silent]
                            // Update UI
                            //updateUI(STATE_SIGNIN_FAILED);
                            // [END_EXCLUDE]
                        }
                    }
                });
    }
   /* private void signOut() {
        mAuth.signOut();
        updateUI(STATE_INITIALIZED);
    }*/
/*
    private void updateUI(int uiState) {
        updateUI(uiState, mAuth.getCurrentUser(), null);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }*/

    /* private void updateUI(int uiState, FirebaseUser user) {
         updateUI(uiState, user, null);
     }

     private void updateUI(int uiState, PhoneAuthCredential cred) {
         updateUI(uiState, null, cred);
     }

     *//*private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
                // Initialized state, show only the phone number field and start button
                enableViews(mStartButton, mPhoneNumberField);
                disableViews(mVerifyButton, mResendButton, mVerificationField);
                mDetailText.setText(null);
                break;
            case STATE_CODE_SENT:
                // Code sent state, show the verification field, the
                enableViews(mVerifyButton, mResendButton, mPhoneNumberField, mVerificationField);
                disableViews(mStartButton);
                mDetailText.setText(R.string.status_code_sent);
                break;
            case STATE_VERIFY_FAILED:
                // Verification has failed, show all options
                enableViews(mStartButton, mVerifyButton, mResendButton, mPhoneNumberField,
                        mVerificationField);
                mDetailText.setText(R.string.status_verification_failed);
                break;
            case STATE_VERIFY_SUCCESS:
                // Verification has succeeded, proceed to firebase sign in
                disableViews(mStartButton, mVerifyButton, mResendButton, mPhoneNumberField,
                        mVerificationField);
                mDetailText.setText(R.string.status_verification_succeeded);

                // Set the verification text based on the credential
                if (cred != null) {
                    if (cred.getSmsCode() != null) {
                        mVerificationField.setText(cred.getSmsCode());
                    } else {
                        mVerificationField.setText(R.string.instant_validation);
                    }
                }

                break;
            case STATE_SIGNIN_FAILED:
                // No-op, handled by sign-in check
                mDetailText.setText(R.string.status_sign_in_failed);
                break;
            case STATE_SIGNIN_SUCCESS:
                // Np-op, handled by sign-in check
                break;
        }

        if (user == null) {
            // Signed out
            mPhoneNumberViews.setVisibility(View.VISIBLE);
            mSignedInViews.setVisibility(View.GONE);

            mStatusText.setText(R.string.signed_out);
        } else {
            // Signed in
            mPhoneNumberViews.setVisibility(View.GONE);
            mSignedInViews.setVisibility(View.VISIBLE);

            enableViews(mPhoneNumberField, mVerificationField);
            mPhoneNumberField.setText(null);
            mVerificationField.setText(null);

            mStatusText.setText(R.string.signed_in);
            mDetailText.setText(getString(R.string.firebase_status_fmt, user.getUid()));
        }
    }*/
    private boolean validatePhoneNumber() {
        String phoneNumber = mPhoneNumberField.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    private void enableViews(View... views) {
        for (View v : views) {
            v.setEnabled(true);
        }
    }

    private void disableViews(View... views) {
        for (View v : views) {
            v.setEnabled(false);
        }
    }


}



