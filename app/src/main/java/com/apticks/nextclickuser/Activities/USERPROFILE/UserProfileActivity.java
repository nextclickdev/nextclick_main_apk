package com.apticks.nextclickuser.Activities.USERPROFILE;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.BookingHistoryActivity;
import com.apticks.nextclickuser.Activities.NotificationActivity;
import com.apticks.nextclickuser.Activities.USERWALLET.MyWalletActivity;
import com.apticks.nextclickuser.Activities.USERWALLET.WalletActivity;
import com.apticks.nextclickuser.checkout.activities.OrderHistoryActivity;
import com.apticks.nextclickuser.support.SupportListActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.apticks.nextclickuser.Activities.ALL_CATEGORIES.AllCategoriesActivity;
import com.apticks.nextclickuser.Activities.CartActivity;
import com.apticks.nextclickuser.Activities.CommonFiledsActivity;
import com.apticks.nextclickuser.Activities.LoginActivity;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Activities.NEWS.NewsActivity;
import com.apticks.nextclickuser.BuildConfig;
import com.apticks.nextclickuser.Fragments.OrderHistory.MainHistoryActivity;
import com.apticks.nextclickuser.Helpers.Validations;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.AddressDataBaseHelper;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.ObjectUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity.location_tv;
import static com.apticks.nextclickuser.Config.Config.USER_PROFILE_R;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.MOBILE;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.*;
import static com.apticks.nextclickuser.SqliteDatabase.CartDBHelper.*;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout vendorCard, myWalletCard, card_history, myProfileCard, logOutCard, privacy_policy_Card, aboutUsCard, customerSupportCard, shareCard;
    PreferenceManager preferenceManager;
    CartDBHelper cartDBHelper;
    AddressDataBaseHelper addressDataBaseHelper;
    Context context;
    TextView name, email, myBalance, myusername;
    CircleImageView profileImage;
    private ImageView back_imageView;
    BottomNavigationView navigation;

    private LinearLayout llBookingHistory,notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        getSupportActionBar().hide();
        context = UserProfileActivity.this;
        initView();
        preferenceManager.putBoolean(getString(R.string.refresh_profile), false);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        try {
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) navigation.getLayoutParams();
        } catch (Exception e) {
            e.printStackTrace();
        }
        navigation.setSelectedItemId(R.id.navigation_profile);

        Window window = getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(UserProfileActivity.this, R.color.profile_back_color));
            }
        }*/


        if (preferenceManager.getString(userName) != null) {
            if (preferenceManager.getInt(wallet) != 0) {
                myBalance.setText(String.valueOf(preferenceManager.getInt(wallet)));
                name.setText(preferenceManager.getString(userName));
                email.setText(preferenceManager.getString(emailId));
                myusername.setText(preferenceManager.getString(uniqueId));
                if (Validations.isValidURL(preferenceManager.getString(profilepic)
                        .replaceAll("\\/\\/", "////")
                        .replaceAll("\\/", "//"))) {

                    Picasso.get().load(preferenceManager.getString(profilepic))
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(profileImage);
                } else {
                    try {
                        String logintype = preferenceManager.getString("login");
                        if (logintype.equalsIgnoreCase("") || logintype == null || logintype.equalsIgnoreCase("fb")) {
                            //profileImage.setImageBitmap(Validations.getFacebookProfilePicture(context));
                            String facebookImageUrl = preferenceManager.getString("fb_img_url");
                            if (facebookImageUrl != null) {
                                if (!facebookImageUrl.isEmpty()) {
                                    Picasso.get().load(facebookImageUrl)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .into(profileImage);
                                }
                            }
                        } else {
                            String imageUrl = preferenceManager.getString("gmail_image_url");
                            if (imageUrl != null) {
                                if (!imageUrl.isEmpty()) {
                                    Picasso.get().load(imageUrl)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .into(profileImage);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        String imageUrl = preferenceManager.getString("gmail_image_url");
                        if (imageUrl != null) {
                            if (!imageUrl.isEmpty()) {
                                Picasso.get().load(preferenceManager.getString("gmail_image_url"))
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    }
                }
            } else {
                name.setText(preferenceManager.getString(userName));
                myusername.setText(preferenceManager.getString(uniqueId));
                email.setText(preferenceManager.getString(emailId));
                try {

                } catch (NullPointerException nullPointerException) {

                }


                    if (Validations.isValidURL(preferenceManager.getString(profilepic)
                            .replaceAll("\\/\\/", "////")
                            .replaceAll("\\/", "//"))) {

                        Toast.makeText(context, "valid", Toast.LENGTH_SHORT).show();
                        Picasso.get().load(preferenceManager.getString(profilepic))
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .into(profileImage);
                    } else {
                        try {
                            String logintype = preferenceManager.getString("login");
                            if (logintype.equalsIgnoreCase("") || logintype == null || logintype.equalsIgnoreCase("fb")) {
                                //profileImage.setImageBitmap(Validations.getFacebookProfilePicture(context));
                                String facebookImageUrl = preferenceManager.getString("fb_img_url");
                                if (facebookImageUrl != null) {
                                    if (!facebookImageUrl.isEmpty()) {
                                        Picasso.get().load(facebookImageUrl)
                                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                                .into(profileImage);
                                    }
                                }
                            } else {
                                String imageUrl = preferenceManager.getString("gmail_image_url");
                                if (imageUrl != null) {
                                    if (!imageUrl.isEmpty()) {
                                        Picasso.get().load(imageUrl)
                                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                                .into(profileImage);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String imageUrl = preferenceManager.getString("gmail_image_url");
                            if (imageUrl != null) {
                                if (!imageUrl.isEmpty()) {
                                    Picasso.get().load(imageUrl)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .into(profileImage);
                                }
                            }
                        }
                    }
                    Log.d(profilepic, preferenceManager.getString(profilepic));

            }
        } /*else {
            fetchUserDetails();
        }*/
        fetchUserDetails();
        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    private void initView() {
        llBookingHistory = findViewById(R.id.llBookingHistory);
        notification = findViewById(R.id.notification);
        preferenceManager = new PreferenceManager(getApplicationContext());
        back_imageView = findViewById(R.id.back_imageView);
        profileImage = findViewById(R.id.profileImage_main);
        vendorCard = findViewById(R.id.vendorCard);
        myWalletCard = findViewById(R.id.myWalletCard);
        card_history = findViewById(R.id.card_history);
        myProfileCard = findViewById(R.id.myProfileCard);
        logOutCard = findViewById(R.id.logOutCard);
        privacy_policy_Card = findViewById(R.id.privacy_policy_Card);
        aboutUsCard = findViewById(R.id.aboutUsCard);
        customerSupportCard = findViewById(R.id.customerSupportCard);
        shareCard = findViewById(R.id.shareCard);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email_id);
        myBalance = findViewById(R.id.myBalance);
        myusername = findViewById(R.id.myusername);
        navigation = (BottomNavigationView) findViewById(R.id.nav_view);
        vendorCard.setOnClickListener(this);
        card_history.setOnClickListener(this);
        myWalletCard.setOnClickListener(this);
        myProfileCard.setOnClickListener(this);
        logOutCard.setOnClickListener(this);
        llBookingHistory.setOnClickListener(this);
        notification.setOnClickListener(this);
        privacy_policy_Card.setOnClickListener(this);
        aboutUsCard.setOnClickListener(this);
        customerSupportCard.setOnClickListener(this);
        shareCard.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llBookingHistory:
                goToBookingHistory();
                break;
                case R.id.notification:
                startActivity(new Intent(UserProfileActivity.this, NotificationActivity.class));
                break;
            case R.id.vendorCard:
                String isVendor = preferenceManager.getString(IS_VENDOR);
                if (isVendor != null) {
                    if (!isVendor.equalsIgnoreCase("yes")) {
                        Intent intent = new Intent(UserProfileActivity.this, CommonFiledsActivity.class);
                        startActivity(intent);
                    } else {
                        UImsgs.showCustomToast(context, "You are already a vendor", INFO);
                    }
                } else {
                    Toast.makeText(context, "Something went wrong please try after some time", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.myProfileCard:
                Intent intentProfile = new Intent(UserProfileActivity.this, UserProfileEditActivity.class);
                startActivity(intentProfile);
                break;
            case R.id.myWalletCard:
             //   UImsgs.showToast(context, "Will be available soon");
              //  Intent intentWallet = new Intent(UserProfileActivity.this, MyWalletActivity.class);
                Intent intentWallet = new Intent(UserProfileActivity.this, WalletActivity.class);
                startActivity(intentWallet);
                break;
            case R.id.card_history:
                History();
                break;
            case R.id.privacy_policy_Card:
                privacyPolicyOpener();
                break;
            case R.id.aboutUsCard:
                browserOpener();
                break;
            case R.id.customerSupportCard:
                Intent i=new Intent(UserProfileActivity.this, SupportListActivity.class);
                startActivity(i);
               // openWhatsApp(v);
                break;
            case R.id.shareCard:
                openShareApp(v);
                break;


            case R.id.logOutCard:

                //private void deleteAppData() {
                new AlertDialog.Builder(UserProfileActivity.this)
                        .setTitle("Alert")
                        .setMessage("Are you sure to logout..?")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation

                                cartDBHelper = new CartDBHelper(getApplicationContext());
                                addressDataBaseHelper = new AddressDataBaseHelper(getApplicationContext());
                                cartDBHelper.clearDatabase(FOOD_CART_TABLE_NAME);
                                cartDBHelper.clearDatabase(RECENT_PLACES_TABLE_NAME);
                                cartDBHelper.clearDatabase(ECOM_SEC_CART_TABLE_NAME);
                                addressDataBaseHelper.clearDatabase();
                                MainCategoriesActivity.loginManager.getInstance().logOut();
                                //MainCategoriesActivity.mAuth.getInstance().signOut();
                                try {
                                    LoginActivity.logOut(UserProfileActivity.this);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    System.out.println("aaaaaaaaaa  logout catch  "+e.getMessage());
                                }
                                location_tv.setText("select location");
                                preferenceManager.clear();
                                finish();


                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                //}


                break;
        }
    }

    private void goToBookingHistory() {
        Intent bookingHistoryIntent = new Intent(this, BookingHistoryActivity.class);
        startActivity(bookingHistoryIntent);
    }

    private void openShareApp(View v) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, " Nextclick ");
            String shareMessage = "\nI recommend you to use this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    private void privacyPolicyOpener() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://nextclick.in/?page=AboutUs-privacypolicy"));
        startActivity(browserIntent);
    }

    private void browserOpener() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://nextclick.in/"));
        startActivity(browserIntent);
        /*http://nextclick.in/?page=AboutUs-privacypolicy*/
    }

    public void openWhatsApp(View view) {
        try {
            String text = "";// Replace with your message.

            String toNumber = "919390755642"; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + toNumber + "&text=" + text));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void History() {
      //  startActivity(new Intent(getApplicationContext(), MainHistoryActivity.class));
        startActivity(new Intent(getApplicationContext(), OrderHistoryActivity.class));
        finish();
    }



    @Override
    protected void onResume() {
        super.onResume();

        boolean refreshProfile = preferenceManager.getBoolean(getString(R.string.refresh_profile));
        if (refreshProfile) {
            fetchUserDetails();
            preferenceManager.putBoolean(getString(R.string.refresh_profile), false);
        }

        navigation.setSelectedItemId(R.id.navigation_profile);
        if (preferenceManager.getInt(wallet) != 0) {
            myBalance.setText(String.valueOf(preferenceManager.getInt(wallet)));
            name.setText(preferenceManager.getString(userName));
            email.setText(preferenceManager.getString(emailId));
            email.setText(preferenceManager.getString("unique_id"));
           // email.setText(preferenceManager.getString(uniqueId));
            if (Validations.isValidURL(preferenceManager.getString(profilepic)
                    .replaceAll("\\/\\/", "////")
                    .replaceAll("\\/", "//"))) {
                Picasso.get().load(preferenceManager.getString(profilepic))
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .into(profileImage);
            } else {
                try {
                    String logintype = preferenceManager.getString("login");
                    if (logintype.equalsIgnoreCase("fb")) {
                        //profileImage.setImageBitmap(Validations.getFacebookProfilePicture(context));
                        String facebookImageUrl = preferenceManager.getString("fb_img_url");
                        if (facebookImageUrl != null) {
                            if (!facebookImageUrl.isEmpty()) {
                                Picasso.get().load(facebookImageUrl)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    } else {
                        String imageUrl = preferenceManager.getString("gmail_image_url");
                        if (imageUrl != null) {
                            if (!imageUrl.isEmpty()) {
                                Picasso.get().load(imageUrl)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    String imageUrl = preferenceManager.getString("gmail_image_url");
                    if (imageUrl != null) {
                        if (!imageUrl.isEmpty()) {
                            Picasso.get().load(imageUrl)
                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                                    .into(profileImage);
                        }
                    }
                }
            }

        } else {
            name.setText(preferenceManager.getString(userName));
            myusername.setText(preferenceManager.getString(uniqueId));
            email.setText(preferenceManager.getString(uniqueId));
          //  email.setText(preferenceManager.getString(emailId));


            try {
                if (Validations.isValidURL(preferenceManager.getString(profilepic)
                        .replaceAll("\\/\\/", "////")
                        .replaceAll("\\/", "//"))) {
                    Picasso.get().load(preferenceManager.getString(profilepic))
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(profileImage);
                } else {
                    try {
                        String logintype = preferenceManager.getString("login");
                        if (logintype.equalsIgnoreCase("") || logintype == null || logintype.equalsIgnoreCase("fb")) {
                            //profileImage.setImageBitmap(Validations.getFacebookProfilePicture(context));
                            String facebookImageUrl = preferenceManager.getString("fb_img_url");
                            if (facebookImageUrl != null) {
                                if (!facebookImageUrl.isEmpty()) {
                                    Picasso.get().load(facebookImageUrl)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .into(profileImage);
                                }
                            }
                        } else {
                            String imageUrl = preferenceManager.getString("gmail_image_url");
                            if (imageUrl != null) {
                                if (!imageUrl.isEmpty()) {
                                    Picasso.get().load(imageUrl)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .into(profileImage);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        String imageUrl = preferenceManager.getString("gmail_image_url");
                        if (imageUrl != null) {
                            if (!imageUrl.isEmpty()) {
                                Picasso.get().load(imageUrl)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    String logintype = preferenceManager.getString("login");
                    if (logintype.equalsIgnoreCase("") || logintype == null || logintype.equalsIgnoreCase("fb")) {
                        //profileImage.setImageBitmap(Validations.getFacebookProfilePicture(context));
                        String facebookImageUrl = preferenceManager.getString("fb_img_url");
                        if (facebookImageUrl != null) {
                            if (!facebookImageUrl.isEmpty()) {
                                Picasso.get().load(facebookImageUrl)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    } else {
                        String imageUrl = preferenceManager.getString("gmail_image_url");
                        if (imageUrl != null) {
                            if (!imageUrl.isEmpty()) {
                                Picasso.get().load(preferenceManager.getString("gmail_image_url"))
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                    String imageUrl = preferenceManager.getString("gmail_image_url");
                    if (imageUrl != null) {
                        if (!imageUrl.isEmpty()) {
                            Picasso.get().load(imageUrl)
                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                                    .into(profileImage);
                        }
                    }
                }
            }
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        navigation.setSelectedItemId(R.id.navigation_profile);
        if (preferenceManager.getInt(wallet) != 0) {
            myBalance.setText(String.valueOf(preferenceManager.getInt(wallet)));
            name.setText(preferenceManager.getString(userName));
            email.setText(preferenceManager.getString(uniqueId));
           // email.setText(preferenceManager.getString(emailId));
            myusername.setText(preferenceManager.getString(uniqueId));
            if (Validations.isValidURL(preferenceManager.getString(profilepic)
                    .replaceAll("\\/\\/", "////")
                    .replaceAll("\\/", "//"))) {
                Picasso.get().load(preferenceManager.getString(profilepic))
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .into(profileImage);
            } else {
                try {
                    String logintype = preferenceManager.getString("login");
                    if (logintype.equalsIgnoreCase("") || logintype == null || logintype.equalsIgnoreCase("fb")) {
                        //profileImage.setImageBitmap(Validations.getFacebookProfilePicture(context));
                        String facebookImageUrl = preferenceManager.getString("fb_img_url");
                        if (facebookImageUrl != null) {
                            if (!facebookImageUrl.isEmpty()) {
                                Picasso.get().load(facebookImageUrl)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    } else {
                        String imageUrl = preferenceManager.getString("gmail_image_url");
                        if (imageUrl != null) {
                            if (!imageUrl.isEmpty()) {
                                Picasso.get().load(imageUrl)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    String imageUrl = preferenceManager.getString("gmail_image_url");
                    if (imageUrl != null) {
                        if (!imageUrl.isEmpty()) {
                            Picasso.get().load(imageUrl)
                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                                    .into(profileImage);
                        }
                    }
                }
            }

        } else {
            name.setText(preferenceManager.getString(userName));
            myusername.setText(preferenceManager.getString(uniqueId));
            if (Validations.isValidURL(preferenceManager.getString(profilepic
                    .replaceAll("\\/\\/", "////")
                    .replaceAll("\\/", "//")))) {
                Picasso.get().load(preferenceManager.getString(profilepic))
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .into(profileImage);
            } else {
                try {
                    String logintype = preferenceManager.getString("login");
                    if (logintype.equalsIgnoreCase("") || logintype == null || logintype.equalsIgnoreCase("fb")) {
                        //profileImage.setImageBitmap(Validations.getFacebookProfilePicture(context));
                        String facebookImageUrl = preferenceManager.getString("fb_img_url");
                        if (facebookImageUrl != null) {
                            if (!facebookImageUrl.isEmpty()) {
                                Picasso.get().load(facebookImageUrl)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    } else {
                        String imageUrl = preferenceManager.getString("gmail_image_url");
                        if (imageUrl != null) {
                            if (!imageUrl.isEmpty()) {
                                Picasso.get().load(imageUrl)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    String imageUrl = preferenceManager.getString("gmail_image_url");
                    if (imageUrl != null) {
                        if (!imageUrl.isEmpty()) {
                            Picasso.get().load(preferenceManager.getString("gmail_image_url"))
                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                                    .into(profileImage);
                        }
                    }
                }
            }
        }
    }


    private void fetchUserDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, USER_PROFILE_R, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    String lastname;
                    try {
                        lastname = jsonObjectData.getString("last_name");
                        if (!lastname.equalsIgnoreCase("null")) {
                            preferenceManager.putString(userName, jsonObjectData.getString("first_name") + " " + lastname);
                        } else {
                            preferenceManager.putString(userName, jsonObjectData.getString("first_name"));
                        }

                        String emailId = jsonObjectData.getString("email");
                        String phoneNumber = jsonObjectData.getString("phone");
                        if (!emailId.equalsIgnoreCase("null")) {
                            preferenceManager.putString("email", emailId);
                        }
                        if (!phoneNumber.equalsIgnoreCase("null")) {
                            preferenceManager.putString("number", phoneNumber);
                        }

                        preferenceManager.putString("number", phoneNumber);
                        preferenceManager.putString("email", emailId);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        preferenceManager.putInt(wallet, jsonObjectData.getInt("wallet"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        preferenceManager.putString(uniqueId, jsonObjectData.getString("unique_id"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONObject groupObject = jsonObjectData.getJSONObject("groups");
                        Iterator x = groupObject.keys();
                        JSONArray groupArray = new JSONArray();

                        while (x.hasNext()) {
                            String key = (String) x.next();
                            groupArray.put(groupObject.get(key));
                        }

                        preferenceManager.putString(user_id, groupArray.getJSONObject(0).getString("user_id"));
                        if (!groupArray.getJSONObject(0).getString("name").equalsIgnoreCase("admin") && !groupArray.getJSONObject(0).getString("name").equalsIgnoreCase("user")) {
                            preferenceManager.putString(IS_VENDOR, "yes");
                        } else {
                            preferenceManager.putString(IS_VENDOR, "no");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        preferenceManager.putString(profilepic, jsonObjectData.getString("image"));
                        Log.d(profilepic, jsonObjectData.getString("image"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d(profilepic, "Noprofile");
                    }
                    try {
                        Log.d("mail", jsonObjectData.getString("email"));
                        preferenceManager.putString(emailId, jsonObjectData.getString("email"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    } try {
                        Log.d("phone", jsonObjectData.getString("phone"));
                        preferenceManager.putString(MOBILE, jsonObjectData.getString("phone"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    myBalance.setText(String.valueOf(preferenceManager.getInt(wallet)));
                    name.setText(preferenceManager.getString(userName));
                    email.setText(preferenceManager.getString(uniqueId));
                    //email.setText(preferenceManager.getString(emailId));
                    myusername.setText(preferenceManager.getString(uniqueId));
                    if (Validations.isValidURL(preferenceManager.getString(profilepic))) {
                        Picasso.get().load(preferenceManager.getString(profilepic))
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .into(profileImage);
                    } else {
                        try {
                            String logintype = preferenceManager.getString("login");
                            if (logintype.equalsIgnoreCase("") || logintype == null || logintype.equalsIgnoreCase("fb")) {
                                //profileImage.setImageBitmap(Validations.getFacebookProfilePicture(context));
                                String imageUrl = preferenceManager.getString("fb_img_url");
                                if (imageUrl != null) {
                                    if (!imageUrl.isEmpty()) {
                                        Picasso.get().load(imageUrl)
                                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                                .into(profileImage);
                                    }
                                }
                            } else {
                                String imageUrl = preferenceManager.getString("gmail_image_url");
                                if (imageUrl != null) {
                                    if (!imageUrl.isEmpty()) {
                                        Picasso.get().load(imageUrl)
                                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                                .into(profileImage);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String imageUrl = preferenceManager.getString("gmail_image_url");
                            if (imageUrl != null) {
                                if (!imageUrl.isEmpty()) {
                                    Picasso.get().load(imageUrl)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .into(profileImage);
                                }
                            }
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
           /* @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return super.getBody();
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent mainIntent = new Intent(context, MainCategoriesActivity.class);
                    startActivity(mainIntent);
                    return true;
                case R.id.navigation_news:

                    Intent newsIntent = new Intent(context, NewsActivity.class);
                    startActivity(newsIntent);
                    return true;

                case R.id.navigation_category:
                    Intent categoryIntent = new Intent(context, AllCategoriesActivity.class);
                    startActivity(categoryIntent);
                    return true;
                case R.id.navigation_profile:

                    return true;
                case R.id.navigation_cart:
                    Intent intent = new Intent(context, CartActivity.class);
                    intent.putExtra("Type", "Food");
                    startActivity(intent);

                    return true;


            }

            return false;
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
