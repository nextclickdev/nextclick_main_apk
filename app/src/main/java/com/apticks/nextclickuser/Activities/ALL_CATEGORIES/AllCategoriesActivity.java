package com.apticks.nextclickuser.Activities.ALL_CATEGORIES;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.apticks.nextclickuser.Activities.CartActivity;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Activities.NEWS.NewsActivity;
import com.apticks.nextclickuser.Activities.USERPROFILE.UserProfileActivity;
import com.apticks.nextclickuser.Adapters.All_52_Categories.All_Catgories_52_Adapter;
import com.apticks.nextclickuser.Adapters.SlidingImage_AdapterTemp;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.CategoriesData;
import com.apticks.nextclickuser.Pojo.SlidersModelClass;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CATEGORY_LIST;
import static com.apticks.nextclickuser.Config.Config.Sliders_And_Advertisements;
import static com.apticks.nextclickuser.Constants.Constants.INFO;

public class AllCategoriesActivity extends AppCompatActivity {

    private Context mContext;
    private RecyclerView categories;
    private ArrayList<CategoriesData> categoriesdatalist;
    private TextView back_text;
    BottomNavigationView navigation;
    private ViewPager all_category_pager;
    private CirclePageIndicator all_category_pager_indicator;
    private ImageView all_category_banner;
    private List<SlidersModelClass> slidersModelClassesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);
        getSupportActionBar().hide();
        mContext = AllCategoriesActivity.this;
        init();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        try {
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) navigation.getLayoutParams();
        } catch (Exception e) {
            e.printStackTrace();
        }
        navigation.setSelectedItemId(R.id.navigation_category);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            }
        }*/
        back_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    private void init() {
        categories = findViewById(R.id.categories);
        back_text = findViewById(R.id.back_text);
        navigation = (BottomNavigationView) findViewById(R.id.nav_view);
        all_category_pager = findViewById(R.id.all_category_pager);
        all_category_pager_indicator = findViewById(R.id.all_category_pager_indicator);
        all_category_banner = findViewById(R.id.all_category_banner);
        categoriesFetcher();
        slidersAndAdvertisment();
    }

    public void categoriesFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", response);
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaa allcategorys  "+jsonObject.toString());
                        //JSONObject dataJson = jsonObject.getJSONObject("foodMenuData");
                        JSONArray resultArray = jsonObject.getJSONArray("data");
                        if (resultArray.length() > 0) {
                            categoriesdatalist = new ArrayList<>();
                            for (int i = 0; i < resultArray.length(); i++) {

                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String id = resultObject.getString("id");
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                CategoriesData categoriesData = new CategoriesData();
                                categoriesData.setId(id);
                                categoriesData.setName(name);
                                categoriesData.setImage(image);
                                int j = Integer.parseInt(id);
                             /*   if (j != 1 & j != 2 & j != 3 & j != 4
                                        & j != 5 & j != 6 & j != 8 & j != 12
                                        & j != 13 & j != 14 & j != 20 & j != 26) {
                                    categoriesdatalist.add(categoriesData);
                                }*/
                                categoriesdatalist.add(categoriesData);

                            }
                        }

                        System.out.println("aaaaaaaaa categoriesdatalist  "+categoriesdatalist.size());

                        All_Catgories_52_Adapter all_catgories_52_adapter = new All_Catgories_52_Adapter(AllCategoriesActivity.this, categoriesdatalist);

                        GridLayoutManager layoutManager = new GridLayoutManager(AllCategoriesActivity.this, 3, GridLayoutManager.VERTICAL, false) {

                            @Override
                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(AllCategoriesActivity.this) {

                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                    @Override
                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                        return SPEED / displayMetrics.densityDpi;
                                    }

                                };
                                smoothScroller.setTargetPosition(position);
                                startSmoothScroll(smoothScroller);
                            }

                        };
                        categories.setLayoutManager(layoutManager);
                        categories.setAdapter(all_catgories_52_adapter);


                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent mainIntent = new Intent(mContext, MainCategoriesActivity.class);
                    startActivity(mainIntent);
                    return true;
                case R.id.navigation_news:
                    Intent newsIntent = new Intent(mContext, NewsActivity.class);
                    startActivity(newsIntent);

                    return true;

                case R.id.navigation_category:

                    return true;
                case R.id.navigation_profile:
                    Intent profileIntent = new Intent(mContext, UserProfileActivity.class);
                    startActivity(profileIntent);
                    return true;
                case R.id.navigation_cart:
                    Intent intent = new Intent(mContext, CartActivity.class);
                    intent.putExtra("Type", "Food");
                    startActivity(intent);

                    return true;


            }

            return false;
        }
    };


    private void slidersAndAdvertisment() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Sliders_And_Advertisements, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                Log.d("banners", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject resultArray = jsonObject.getJSONObject("data");
                    //Toast.makeText(mContext, ""+resultArray, Toast.LENGTH_SHORT).show();

                    JSONArray jsonArraysliders = resultArray.getJSONArray("sliders");

                    for (int i = 1; i < jsonArraysliders.length(); i++) {
                        JSONObject jsonObjectSliders = jsonArraysliders.getJSONObject(i);
                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                        slidersModelClass.setId(String.valueOf(i));
                        slidersModelClass.setBanners(jsonObjectSliders.getString("image"));

                        slidersModelClassesList.add(slidersModelClass);
                    }
                    //Toast.makeText(mContext, ""+jsonArraysliders, Toast.LENGTH_SHORT).show();

                    all_category_pager.setAdapter(new SlidingImage_AdapterTemp(mContext, slidersModelClassesList));
                    all_category_pager_indicator.setViewPager(all_category_pager);
                    /*mainBannersSlidingAdapter = new MainBannersSlidingAdapter(mContext, slidersModelClassesList);
                    layoutManager.scrollToPositionWithOffset(2, 20);
                    mainpager.setLayoutManager(layoutManager);
                    mainpager.setAdapter(mainBannersSlidingAdapter);
                    mainpager.getLayoutManager().scrollToPosition(Integer.MAX_VALUE / 2);*/





                    /*//Top Sliders
                    JSONArray jsonArraysliderstop = resultArray.getJSONArray("top");
                    for (int i = 0; i < *//*jsonArraysliderstop.length()*//*1; i++) {
                        JSONObject jsonObjectSliders = jsonArraysliderstop.getJSONObject(i);
                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                        slidersModelClass.setId(String.valueOf(i));
                        slidersModelClass.setBanners(jsonObjectSliders.getString("image"));

                        Picasso.get().load(jsonObjectSliders.getString("image")).into(main_banner_1);

                        *//*slidersModelClassesTopList.add(slidersModelClass);*//*
                    }
                    *//*slider_top.setAdapter(new SlidingImage_AdapterTemp(mContext, slidersModelClassesTopList));
                    indicatorTop.setViewPager(slider_top);*//*

                    //Middle Sliders
                    JSONArray jsonArrayslidersmiddle = resultArray.getJSONArray("middle");
                    for (int i = 0; i < *//*jsonArrayslidersmiddle.length()*//*1; i++) {
                        JSONObject jsonObjectSliders = jsonArrayslidersmiddle.getJSONObject(i);
                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                        slidersModelClass.setId(String.valueOf(i));
                        slidersModelClass.setBanners(jsonObjectSliders.getString("image"));

                        //slidersModelClassesMiddleList.add(slidersModelClass);
                        Picasso.get().load(jsonObjectSliders.getString("image")).into(main_banner_2);
                    }
                   *//* slider_middle.setAdapter(new SlidingImage_AdapterTemp(mContext, slidersModelClassesMiddleList));
                    indicatorMiddle.setViewPager(slider_middle);*//*

                    //Bottom SLiders
                    JSONArray jsonArrayslidersbottom = resultArray.getJSONArray("bottom");
                    for (int i = 0; i < jsonArrayslidersbottom.length(); i++) {
                        JSONObject jsonObjectSliders = jsonArrayslidersbottom.getJSONObject(i);
                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                        slidersModelClass.setId(String.valueOf(i));
                        slidersModelClass.setBanners(jsonObjectSliders.getString("image"));
                        Picasso.get().load(jsonObjectSliders.getString("image")).into(main_banner_3);
                        //slidersModelClassesBottomList.add(slidersModelClass);
                    }*/
                    JSONArray jsonArrayslidersfotter = resultArray.getJSONArray("last");
                    for (int i = 0; i < jsonArrayslidersfotter.length(); i++) {
                        JSONObject jsonObjectSliders = jsonArrayslidersfotter.getJSONObject(i);
                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                        slidersModelClass.setId(String.valueOf(i));
                        slidersModelClass.setBanners(jsonObjectSliders.getString("image"));
                        Picasso.get().load(jsonObjectSliders.getString("image")).into(all_category_banner);
                        //slidersModelClassesBottomList.add(slidersModelClass);
                    }
                   /* slider_bottom.setAdapter(new SlidingImage_AdapterTemp(mContext, slidersModelClassesBottomList));
                    indicatorBottom.setViewPager(slider_bottom);*/
                    /*mainpager.post(new Runnable() {
                        @Override
                        public void run() {
                            // Call smooth scroll
                            for(int i=0 ;i<mainBannersSlidingAdapter.getItemCount();i++) {
                                mainpager.smoothScrollToPosition(i);
                                if(i==mainBannersSlidingAdapter.getItemCount()){
                                    i=0;
                                }
                                Thread thread = new Thread();
                                try {
                                    thread.wait(5);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });*/

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showCustomToast(mContext, "Something went wrong with the banners", INFO);
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @Override
    protected void onResume() {
        super.onResume();
        navigation.setSelectedItemId(R.id.navigation_category);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        navigation.setSelectedItemId(R.id.navigation_category);
    }
}
