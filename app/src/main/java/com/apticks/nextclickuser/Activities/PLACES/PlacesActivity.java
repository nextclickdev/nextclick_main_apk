package com.apticks.nextclickuser.Activities.PLACES;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.apticks.nextclickuser.Adapters.PLACES_ADAPTER.PlacesAdapter;
import com.apticks.nextclickuser.Pojo.PLACES.PlacePojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity.locationStatus;
import static com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity.location_tv;

import static com.apticks.nextclickuser.Config.Config.PLACES_API_KEY;

public class PlacesActivity extends AppCompatActivity implements LocationListener {


    int AUTOCOMPLETE_REQUEST_CODE = 1;

    PlacesClient placesClient;
    private PreferenceManager preferenceManager;
    private RecyclerView places_recycler;
    TextView current_location;
    Context context;
    private LocationManager locationManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        getSupportActionBar().hide();

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }

        context = getApplicationContext();
        current_location = findViewById(R.id.current_location);
        places_recycler = findViewById(R.id.places_recycler);
        preferenceManager = new PreferenceManager(PlacesActivity.this);
        CartDBHelper cartDBHelper = new CartDBHelper(context);
        List<PlacePojo> placesList = cartDBHelper.getAllPlacesList();


        for(int i=0;i<placesList.size();i++){
            System.out.println("Name :"+placesList.get(i).getId());
        }

        if(placesList.size()>0){
            PlacesAdapter placesAdater = new PlacesAdapter(PlacesActivity.this,placesList);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context,  LinearLayoutManager.VERTICAL, false) {

                @Override
                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(context) {

                        private static final float SPEED = 300f;// Change this value (default=25f)

                        @Override
                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                            return SPEED / displayMetrics.densityDpi;
                        }

                    };
                    smoothScroller.setTargetPosition(position);
                    startSmoothScroll(smoothScroller);
                }

            };
            places_recycler.setLayoutManager(layoutManager);
            places_recycler.setAdapter(placesAdater);
        }


        if(!Places.isInitialized()){
            Places.initialize(getApplicationContext(), PLACES_API_KEY);
        }

        current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();

            }
        });

        placesClient = Places.createClient(PlacesActivity.this);

        final AutocompleteSupportFragment autocompleteSupportFragment =
                (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteSupportFragment.setPlaceFields(Arrays.asList(Place.Field.ID,Place.Field.LAT_LNG,Place.Field.NAME));

        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                final LatLng latLng = place.getLatLng();

                /*Toast.makeText(PlacesActivity.this, place.getName(), Toast.LENGTH_SHORT).show();*/
                Log.d("Lat",place.getLatLng()+"");
                Log.d("Name",place.getName()+"");
                Log.d("id",place.getId()+"");
                Log.d("address",place.getAddress()+"");
                String latLang = place.getLatLng()+"".trim();
                String [] sep = latLang.split(":");
                String [] coordinates = sep[1].split(",");
                preferenceManager.putString("lat", String.valueOf(coordinates[0]).substring(2,coordinates[0].length()));
                Log.d("lat", String.valueOf(coordinates[0]).substring(2,coordinates[0].length()));
                String lat =String.valueOf(coordinates[0]).substring(2,coordinates[0].length());
                preferenceManager.putString("lang", String.valueOf(coordinates[1]).substring(0,coordinates[1].length()-1));
                Log.d("lang", String.valueOf(coordinates[1]).substring(0,coordinates[1].length()-1));
                String lang = String.valueOf(coordinates[1]).substring(0,coordinates[1].length()-1);
                preferenceManager.putString("place", place.getName());
                location_tv.setText(place.getName());
                locationStatus = true;
                //SubCategoriesActivity.location_tv.setText(place.getName());
                CartDBHelper cartDBHelper = new CartDBHelper(getApplicationContext());
                if(cartDBHelper.insertPlace(place.getId(),lat,lang,place.getName())){
                    Toast.makeText(context, "Added", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onError(@NonNull Status status) {

            }
        });



    }


    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        final ProgressDialog progressDialog = new ProgressDialog(PlacesActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        try {

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
            progressDialog.dismiss();
            finish();
        } catch (SecurityException e) {
            e.printStackTrace();
            Log.d("Location Error", e.toString());
            progressDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            //Toast.makeText(mContext, arr.length+"", Toast.LENGTH_SHORT).show();


            //lat1 = location.getLatitude();
            preferenceManager.putString("lat",location.getLatitude()+"");
           //lang1 = location.getLongitude();
            preferenceManager.putString("lang",location.getLongitude()+"");

            //location_tv.setText(arr[3]);
            Log.d("getLocality()",addresses.get(0).getLocality()+"");
            Log.d("getAddressLine()",addresses.get(0).getAddressLine(0)+"");
            Log.d("getAdminArea()",addresses.get(0).getAdminArea()+"");
            Log.d("getSubAdminArea()",addresses.get(0).getSubAdminArea()+"");
            Log.d("getSubLocality()",addresses.get(0).getSubLocality()+"");
            Log.d("getFeatureName()",addresses.get(0).getFeatureName()+"");

            location_tv.setText(addresses.get(0).getSubLocality()+"");
            preferenceManager.putString("place",addresses.get(0).getSubLocality()+"");
            locationStatus = true;



        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

//Location End




}
