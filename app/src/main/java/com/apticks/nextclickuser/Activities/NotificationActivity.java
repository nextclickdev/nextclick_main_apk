package com.apticks.nextclickuser.Activities;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.NotificationsAdapter;
import com.apticks.nextclickuser.Constants.Constants;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Pojo.notificationDetailsResponse.NotificationDetails;
import com.apticks.nextclickuser.Pojo.notificationDetailsResponse.NotificationDetailsResponse;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.interfaces.HttpReqResCallBack;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.NOTIFICATIONS;
import static com.apticks.nextclickuser.Constants.Constants.APP_ID;
import static com.apticks.nextclickuser.Constants.Constants.APP_ID_VALUE;
import static com.apticks.nextclickuser.Constants.Constants.AUTH_TOKEN;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


public class NotificationActivity extends AppCompatActivity implements View.OnClickListener,
        HttpReqResCallBack {

    private TextView tvError;
    private ImageView ivBackArrow;
    private RecyclerView rvNotificationDetails;

    private LinkedList<NotificationDetails> listOFNotificationDetails;

    private String token = "";
    private CustomDialog mCustomDialog;
    private Context mContext;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_notifications);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void initializeUi() {
        mContext= NotificationActivity.this;
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.img_back);
        rvNotificationDetails = findViewById(R.id.rvNotificationDetails);
        mCustomDialog=new CustomDialog(NotificationActivity.this);
        listOFNotificationDetails = new LinkedList<>();
        TextView tv_header= findViewById(R.id.tv_header);
        tv_header.setText("Notification Details");
        FrameLayout cart_layout=findViewById(R.id.cart_layout);
        cart_layout.setVisibility(View.GONE);
        getNotifications();
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
     //   LoadingDialog.loadDialog(this);
        token = new PreferenceManager(this).getString(TOKEN_KEY);
      //  NotificationApiCall.serviceCallToGetNotifications(this, null, null, token);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.img_back:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_NOTIFICATIONS:
                if (jsonResponse != null) {
                    NotificationDetailsResponse notificationDetailsResponse = new Gson().fromJson(jsonResponse, NotificationDetailsResponse.class);
                    if (notificationDetailsResponse != null) {
                        listOFNotificationDetails = notificationDetailsResponse.getListOfNotificationDetails();
                        if (listOFNotificationDetails != null && listOFNotificationDetails.size() > 0) {
                            listIsFull();
                            initializeAdapter();
                        } else {
                            listIsEmpty();
                        }
                    }
                }
                mCustomDialog.dismiss();
                break;
            default:
                break;
        }
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvNotificationDetails.setVisibility(View.VISIBLE);
    }

    private void initializeAdapter() {
        NotificationsAdapter notificationsAdapter = new NotificationsAdapter(this, listOFNotificationDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvNotificationDetails.setLayoutManager(layoutManager);
        rvNotificationDetails.setItemAnimator(new DefaultItemAnimator());
        rvNotificationDetails.setAdapter(notificationsAdapter);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvNotificationDetails.setVisibility(View.GONE);
    }

    public void getNotifications(){
        listOFNotificationDetails.clear();
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, NOTIFICATIONS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject notification  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONArray jsonArray=jsonObject.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject dataobj=jsonArray.getJSONObject(i);
                                    String id=dataobj.getString("id");
                                    String title=dataobj.getString("title");
                                    NotificationDetails notificationDetails=new NotificationDetails();
                                    notificationDetails.setId(dataobj.getString("id"));
                                    notificationDetails.setEcom_order_id(dataobj.getString("ecom_order_id"));
                                    notificationDetails.setTitle(dataobj.getString("title"));
                                    notificationDetails.setMessage(dataobj.getString("message"));
                                    notificationDetails.setNotification_code(dataobj.getString("notification_code"));
                                    notificationDetails.setNotification_name(dataobj.getString("notification_name"));
                                    notificationDetails.setStatus(dataobj.getString("status"));

                                    listOFNotificationDetails.add(notificationDetails);
                                }

                                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                            private static final float SPEED = 300f;// Change this value (default=25f)

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }

                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }

                                };
                                rvNotificationDetails.setLayoutManager(layoutManager);
                                NotificationsAdapter notificationsAdapter = new NotificationsAdapter(mContext, listOFNotificationDetails);
                                rvNotificationDetails.setAdapter(notificationsAdapter);
                                if (listOFNotificationDetails.isEmpty()) {
                                    tvError.setVisibility(View.VISIBLE);
                                    rvNotificationDetails.setVisibility(View.GONE);
                                }
                                else {
                                    tvError.setVisibility(View.GONE);
                                    rvNotificationDetails.setVisibility(View.VISIBLE);
                                }

                            }

                        } catch (JSONException e) {
                            tvError.setVisibility(View.VISIBLE);
                            rvNotificationDetails.setVisibility(View.GONE);
                            Toast.makeText(mContext, "No orders found", Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                tvError.setVisibility(View.VISIBLE);
                rvNotificationDetails.setVisibility(View.GONE);
                mCustomDialog.dismiss();
                //  Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
              //  HashMap<String, String> headers = new HashMap<>();
                map.put(AUTH_TOKEN, new PreferenceManager(mContext).getString(TOKEN_KEY));
                map.put(APP_ID,APP_ID_VALUE);
                System.out.println("aaaaaaaa headers  "+map.toString());
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
