package com.apticks.nextclickuser.Activities.SUB_CATEGORIES;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.products.activities.AddCartActivity;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.apticks.nextclickuser.Adapters.Brands.BrandsAdapter;
import com.apticks.nextclickuser.Adapters.SliderAdapter;
import com.apticks.nextclickuser.Adapters.SubCatAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.Brands.BrandsPojo;
import com.apticks.nextclickuser.Pojo.SubCategoryItemPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import static com.apticks.nextclickuser.Config.Config.CATEGORY_BANNERS;

import static com.apticks.nextclickuser.Config.Config.CATEGORY_LIST;
//import static com.grepthor.nextclickuser.Config.Config.SUB_CAT_VENDOR_LIST;

import static com.apticks.nextclickuser.Constants.Constants.CAT_ID;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;

public class SubCategoriesActivity extends AppCompatActivity/* implements LocationListener*/ {

    private FrameLayout cart_layout;
    private String category_id;
    private LocationManager locationManager;
    public static TextView location_tv,category_name,tv_popular_brands;
    private RecyclerView sub_cat_recycler,sub_cat_brands_recycler;
    //private RecyclerView  ;
    private ImageView banner, sub_cat_banner_1,sub_cat_back_img;
    private ArrayList<SubCategoryItemPojo> subCatList;
    private ArrayList<BrandsPojo> brandsList;
    private ArrayList<String> bannersList;
    private CirclePageIndicator indicator;
    private static ViewPager mPager;
    private Context mContext;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private Double lat1;
    private Double lang1;
    private ShimmerFrameLayout sub_cat_shimmer;
    int isHavingLeadManagement;//0- not to show lead button ... 1- need to show lead button

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_categories);
        getSupportActionBar().hide();
        mContext = SubCategoriesActivity.this;
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.Iconblue));
            }
        }*/
        category_id = getIntent().getStringExtra(CAT_ID);
        init();
        sub_cat_shimmer.setVisibility(View.VISIBLE);
        sub_cat_shimmer.startShimmer();
        mContext = getApplicationContext();
        sub_cat_back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    private void init() {
        category_name = findViewById(R.id.tv_header);
        category_name.setText(getIntent().getStringExtra("Category_Name"));
        category_name.setAllCaps(true);
        location_tv = findViewById(R.id.location);
        //getLocation();
        sub_cat_recycler = findViewById(R.id.sub_cat_recycler);
        sub_cat_brands_recycler = findViewById(R.id.sub_cat_brands_recycler);
        tv_popular_brands = findViewById(R.id.tv_popular_brands);
        mPager = findViewById(R.id.sub_banner_pager);
        indicator = findViewById(R.id.sub_banner_pager_indicator);
        sub_cat_banner_1 = findViewById(R.id.sub_cat_banner_1);
        sub_cat_back_img = findViewById(R.id.img_back);
        sub_cat_shimmer = findViewById(R.id.sub_cat_shimmer);
        cart_layout = findViewById(R.id.cart_layout);
        bannersFetcher();
        subCatgeoriesFetcher();

        cart_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext, CartAddActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initSlider(ArrayList<String> IMAGES) {
        for (int i = 0; i < IMAGES.size(); i++)
            ImagesArray.add(i);
        mPager.setAdapter(new SliderAdapter(mContext, IMAGES));
        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(3 * density);

        NUM_PAGES = IMAGES.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }try {
                    mPager.setCurrentItem(currentPage++, true);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    private void bannersFetcher() {


        //DialogOpener.dialogOpener(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(SubCategoriesActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_BANNERS + category_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    //DialogOpener.dialog.dismiss();
                    Log.d("sub_catres",response);
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {
                                JSONObject dataObject = jsonObject.getJSONObject("data");

                                Iterator x = dataObject.keys();
                                JSONArray jsonArray = new JSONArray();

                                while (x.hasNext()) {
                                    String key = (String) x.next();
                                    jsonArray.put(dataObject.get(key));
                                }

                                bannersList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length() - 1; i++) {

                                    JSONObject bannerObject = jsonArray.getJSONObject(i);
                                    bannersList.add(bannerObject.getString("image"));
                                    //DialogOpener.dialog.dismiss();
                                }

                                if (bannersList.size() > 0)
                                    initSlider(bannersList);
                                Picasso.get().load(dataObject.getString("cat_bottom_banners")).into(sub_cat_banner_1);
                                //DialogOpener.dialog.dismiss();


                            }
                            //DialogOpener.dialog.dismiss();

                        }
                        //DialogOpener.dialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //DialogOpener.dialog.dismiss();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(mContext,"Something went wrong",ERROR);

            }
        });
        requestQueue.add(stringRequest);
    }

    private void subCatgeoriesFetcher() {
        //DialogOpener.dialogOpener(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(SubCategoriesActivity.this);

        Log.d("sub_cat_url",CATEGORY_LIST + category_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST + category_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            category_name.setText(dataObject.getString("name"));
                            isHavingLeadManagement = dataObject.getInt("is_having_lead_managemet");
                            if (!dataObject.get("sub_categories").getClass().toString().equalsIgnoreCase(str.getClass().toString())) {
                                try {
                                    JSONArray subCatArray = dataObject.getJSONArray("sub_categories");
                                    subCatList = new ArrayList<>();

                                    //DialogOpener.dialog.dismiss();
                                    for (int i = 0; i < subCatArray.length(); i++) {

                                            JSONObject subCatObj = subCatArray.getJSONObject(i);
                                            SubCategoryItemPojo subCategoryItemPojo = new SubCategoryItemPojo();
                                            subCategoryItemPojo.setCat_id(subCatObj.getString("cat_id"));
                                            subCategoryItemPojo.setId(subCatObj.getString("id"));
                                            subCategoryItemPojo.setName(subCatObj.getString("name"));
                                            subCategoryItemPojo.setImage(subCatObj.getString("image"));
                                            subCatList.add(subCategoryItemPojo);

                                    }

                                    SubCatAdapter subCatAdapter = new SubCatAdapter(SubCategoriesActivity.this, subCatList,isHavingLeadManagement);
                                    GridLayoutManager layoutManager = new GridLayoutManager(SubCategoriesActivity.this, 3,LinearLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(SubCategoriesActivity.this) {
                                                // Change this value (default=25f)
                                                private static final float SPEED = 300f;

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }
                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }
                                    };
                                    sub_cat_recycler.setLayoutManager(layoutManager);
                                    sub_cat_recycler.setAdapter(subCatAdapter);
                                    //DialogOpener.dialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    //DialogOpener.dialog.dismiss();
                                }
                                try{

                                    if (dataObject.getJSONObject("brands")!=null){
                                        JSONObject brandsObject = dataObject.getJSONObject("brands");
                                        Iterator x = brandsObject.keys();

                                        JSONArray jsonArray = new JSONArray();

                                        while (x.hasNext()) {
                                            String key = (String) x.next();
                                            jsonArray.put(brandsObject.get(key));
                                        }

                                        brandsList = new ArrayList<>();
                                        for(int i=0; i<jsonArray.length();i++){
                                            JSONObject brandObject = jsonArray.getJSONObject(i);
                                            BrandsPojo brandsPojo = new BrandsPojo();
                                            brandsPojo.setId(brandObject.getString("id"));
                                            brandsPojo.setCatID(brandObject.getString("cat_id"));
                                            brandsPojo.setName(brandObject.getString("name"));
                                            try{
                                                brandsPojo.setImage(brandObject.getString("image"));
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                            brandsList.add(brandsPojo);

                                        }
                                        BrandsAdapter brandsAdapter = new BrandsAdapter(SubCategoriesActivity.this, brandsList);
                                        GridLayoutManager layoutManager = new GridLayoutManager(SubCategoriesActivity.this,  3,LinearLayoutManager.VERTICAL, false) {

                                            @Override
                                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(SubCategoriesActivity.this) {
                                                    // Change this value (default=25f)
                                                    private static final float SPEED = 300f;

                                                    @Override
                                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                        return SPEED / displayMetrics.densityDpi;
                                                    }
                                                };
                                                smoothScroller.setTargetPosition(position);
                                                startSmoothScroll(smoothScroller);
                                            }
                                        };

                                        sub_cat_brands_recycler.setLayoutManager(layoutManager);
                                        sub_cat_brands_recycler.setAdapter(brandsAdapter);
                                        if(brandsList!=null && brandsList.size()>0)
                                        {
                                            tv_popular_brands.setVisibility(View.VISIBLE);
                                        }

                                    }

                                    //DialogOpener.dialog.dismiss();

                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                            //DialogOpener.dialog.dismiss();
                        }
                        //DialogOpener.dialog.dismiss();
                    } catch (JSONException e) {
                        Log.e("JSONEXCEPTION", "" + e);

                    }
                    sub_cat_shimmer.stopShimmer();
                    sub_cat_shimmer.setVisibility(View.GONE);
                }
                //DialogOpener.dialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(mContext,"Something went wrong",ERROR);
            }
        });
        requestQueue.add(stringRequest);

    }



}
