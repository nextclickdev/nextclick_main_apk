package com.apticks.nextclickuser.Activities.addtocart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.AddToCartExpandable;
import com.apticks.nextclickuser.Adapters.SectionItemsModelOuterRecycler;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.CartChildModel;
import com.apticks.nextclickuser.Pojo.CartParentModel;
import com.apticks.nextclickuser.Pojo.SectionItemsRadioModel;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.Money;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static com.apticks.nextclickuser.Config.Config.FOOD_SECTIONS;
import static com.apticks.nextclickuser.Config.Config.FoodSingleItem;

public class AddToCartActivityNew extends AppCompatActivity {

    private RecyclerView my_recycle_radio;
    private TextView nameTvFoodSection,descTv,total_price_tv;
    private RelativeLayout progressDialog;
    private Context mContext;
    String restaurant_menu_item_id, restaurantVendorId,nameString, imageString, priceString;
    private int quantity = 0,priceInt=0;
    SectionItemsAdapter sectionItemsAdapter;
    ArrayList<CartModel> sectionItemsCheckModelsArrayListInnerTemp = new ArrayList<>();
    LinkedHashMap<Integer, ArrayList<CartModel>> sectionItemsCheckModelsLinkedListInner = new LinkedHashMap<Integer, ArrayList<CartModel>>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart_new);
        init();

        singleFoodItem();
    }

    public void init(){
        mContext=AddToCartActivityNew.this;
        my_recycle_radio=findViewById(R.id.my_recycle_radio);
        nameTvFoodSection = findViewById(R.id.name_tv_food_section);
        descTv = findViewById(R.id.desc_tv);
        total_price_tv = findViewById(R.id.total_price_tv);
        progressDialog = findViewById(R.id.progressDialog);

        my_recycle_radio.setLayoutManager(new GridLayoutManager(this,1));

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            restaurant_menu_item_id = getIntent().getStringExtra("food_section");
            restaurantVendorId = getIntent().getStringExtra("food_section");
            quantity = getIntent().getIntExtra("totalQuantity", 0);
            //Toast.makeText(this, String.valueOf(restaurant_menu_item_id), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show();
        }


    }

    private void singleFoodItem() {
        progressDialog.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        Log.v("Menu Id", FoodSingleItem + restaurant_menu_item_id);
        System.out.println("aaaaaa menuid "+FoodSingleItem + restaurant_menu_item_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FoodSingleItem +
                restaurant_menu_item_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                try {
                    Log.v("Menu ID Here", "" + FoodSingleItem + restaurant_menu_item_id);
                    JSONObject jsonObject = new JSONObject(response);

                    System.out.println("aaaaaaaaa response single  "+jsonObject.toString());
                    //jsonObject.getJSONObject("data");
                    //Toast.makeText(mContext, ""+jsonObject.getJSONObject("data"), Toast.LENGTH_SHORT).show();
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");

                    nameString = jsonObjectData.getString("name");
                    imageString = jsonObjectData.getString("image");
                    if (jsonObjectData.getInt("discount") == 0) {
                        priceString = String.valueOf(jsonObjectData.getInt("price"));
                        priceInt = (jsonObjectData.getInt("price"));
                        //Toast.makeText(mContext, "IF", Toast.LENGTH_SHORT).show();
                    } else {
                        priceString = String.valueOf(jsonObjectData.getInt("discount_price"));
                        priceInt = (jsonObjectData.getInt("discount_price"));
                        //Toast.makeText(mContext, "ELSE", Toast.LENGTH_SHORT).show();
                    }


                    nameTvFoodSection.setText(nameString);
                    //descTv.setText(jsonObjectData.getString("desc"));
                    descTv.setText(Html.fromHtml(jsonObjectData.getString("desc")).toString());


                    /*total_price_tv.setText(checkDiscount(jsonObjectData.getString("discount"),
                            jsonObjectData.getString("desc"),
                            priceString));*/
                    try {
                        checkDiscount(jsonObjectData.getInt("discount"),
                                priceInt,
                                priceString);
                    } catch (NumberFormatException e) {
                        //Toast.makeText(mContext, "Number "+e, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, "JSON " + e, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {

                    }
                    foodSectionSingle(restaurant_menu_item_id);//Food Section Adapter
                    progressDialog.setVisibility(View.GONE);


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private void checkDiscount(int discount, int desc, String priceString) {

        try {
            if (discount != 0) {//Checking Food Discount


                String sellCostString = Money.rupees(
                        BigDecimal.valueOf(Long.valueOf(desc))).toString()
                        + "     ";

                total_price_tv.setText(sellCostString, TextView.BufferType.SPANNABLE);

            } else {
                String sellCostString = Money.rupees(
                        BigDecimal.valueOf(Long.valueOf(desc))).toString()
                        + "     ";
                total_price_tv.setText(sellCostString, TextView.BufferType.SPANNABLE);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
    }
    private void foodSectionSingle(String id) {
        {
            Log.v("Food Sections", id);
            //Toast.makeText(mContext, " ID "+ id, Toast.LENGTH_SHORT).show();
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            Log.d("urlllllllll", FOOD_SECTIONS + id);
            System.out.println("aaaaaaaa foodsectionsingle   "+FOOD_SECTIONS + id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, FOOD_SECTIONS
                    + /*"1"*/id, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaa response foodsection  "+jsonObject.toString());
                            JSONObject dataJson = jsonObject.getJSONObject("data");
                            JSONArray resultObject0 = dataJson.getJSONArray("result");

                            JSONObject jo = new JSONObject();
                            JSONArray ja = new JSONArray();
                            // populate the array
                            jo.put("arrayName", ja);
                            HashMap<String, Object> stringObjectOuterHashMap = new HashMap<>();

                            if (resultObject0.length() > 0) {
                                ArrayList<CartModel> cartlist=new ArrayList<>();
                                for (int i = 0; i < resultObject0.length(); i++) {
                                    JSONObject resultObject = resultObject0.getJSONObject(i);
                                    CartChildModel menuItemExtraModel = new CartChildModel();
                                    String id = resultObject.getString("id");
                                    String name = resultObject.getString("name");
                                    String required = String.valueOf(resultObject.getInt("required"));
                                    String itemFieldSymbol = String.valueOf(resultObject.getInt("item_field"));
                                    int secPrice = resultObject.getInt("sec_price");
                                    JSONArray itemFieldSecItems = resultObject.getJSONArray("sec_items");

                                    if (itemFieldSecItems.length() >= 1) {
                                        CartModel cartModel=new CartModel();
                                        cartModel.setId(id);
                                        cartModel.setName(name);
                                        cartModel.setRequired(required);
                                        cartModel.setSec_price(""+secPrice);


                                        stringObjectOuterHashMap.put("name", name);
                                        stringObjectOuterHashMap.put("item_field", itemFieldSymbol);

                                        ArrayList<SecItems> secitemslist=new ArrayList<>();
                                        //inner For loop
                                        for (int j = 0; j < itemFieldSecItems.length(); j++) {
                                            JSONObject jsonObjectInner = itemFieldSecItems.getJSONObject(j);

                                            SecItems secItems=new SecItems();
                                            secItems.setId(jsonObjectInner.getString("id"));
                                            secItems.setName(jsonObjectInner.getString("name"));
                                            secItems.setDescription(jsonObjectInner.getString("desc"));
                                            secItems.setPrice(jsonObjectInner.getString("price"));
                                            secItems.setStatus(jsonObjectInner.getString("status"));
                                            secItems.setSec_item_status(jsonObjectInner.getString("sec_item_status"));
                                            secitemslist.add(secItems);
                                        }
                                        cartModel.setSectionitemlist(secitemslist);
                                        cartlist.add(cartModel);
                                        sectionItemsCheckModelsLinkedListInner.put(i, cartlist);
                                    } else {

                                    }

                                }



                                //End Of for Loop Outer
                                Log.d("priceint", priceInt + "");

                               /* sectionItemsAdapter = new SectionItemsAdapter(mContext,sectionItemsCheckModelsArrayListInnerTemp, stringObjectOuterHashMap, cartlist, sectionItemsCheckModelsLinkedListInner.entrySet(),
                                        priceInt,
                                        total_price_tv);*/
                                sectionItemsAdapter=new SectionItemsAdapter(mContext,cartlist, priceInt,
                                        total_price_tv);
                                my_recycle_radio.setAdapter(sectionItemsAdapter);


                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "Catch " + String.valueOf(e), Toast.LENGTH_SHORT).show();

                        }

                        //FoodItemsAdapter foodItemsAdapter = new FoodItemsAdapter(mContext, foodItemData);

                        //foodItemsRecycler.setLayoutManager(new GridLayoutManager(mContext, 2,GridLayoutManager.VERTICAL, false));


                        //foodItemsRecycler.setAdapter(foodItemsAdapter);

                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    UImsgs.showToast(mContext, "Error " + error.toString());
                    Log.d("error", error.toString());
                }
            });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);

        }
    }
}