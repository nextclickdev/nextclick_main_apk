package com.apticks.nextclickuser.Activities.Beauty_Spa;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.BS_Adapters.BSVendorAdapter;
import com.apticks.nextclickuser.Helpers.NetworkUtil;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.BSPojo.BSVendorsPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.VENDOR_LIST;

public class Vendors_BS extends AppCompatActivity {

    RecyclerView bs_vendors_recycler;
    private Context context;
    private ImageView banner;
    ArrayList<BSVendorsPojo> bsvendors_list;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendors__bs);
        getSupportActionBar().hide();
        context = getApplicationContext();
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(Vendors_BS.this, R.color.colorPrimary));
        }
        init();
        if (NetworkUtil.isNetworkConnected(Vendors_BS.this)) {
            bs_dataFetcher();
        } else {
            Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
        }

    }

    private void init(){
        bs_vendors_recycler = findViewById(R.id.bs_vendors_recycler);
        banner = findViewById(R.id.bannerImage_bs);
        Picasso.get().load("http://cineplant.com/nextclick/uploads/advertisements_image/advertisements_9.png").into(banner);
    }


    public void bs_dataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_LIST + "7/10/0", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("result");
                        if (resultArray.length() > 0) {
                            bsvendors_list = new ArrayList<>();
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                int vendorId = resultObject.getInt("id");
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                BSVendorsPojo bsVendorsPojo = new BSVendorsPojo();

                                bsVendorsPojo.setId(vendorId+"".trim());
                                bsVendorsPojo.setName(name);
                                bsVendorsPojo.setImage(image);

                                bsvendors_list.add(bsVendorsPojo);

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    BSVendorAdapter bsVendorAdapter = new BSVendorAdapter(context, bsvendors_list);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(context) {

                                private static final float SPEED = 300f;// Change this value (default=25f)

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };
                    bs_vendors_recycler.setLayoutManager(layoutManager);

                    bs_vendors_recycler.setAdapter(bsVendorAdapter);


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(context, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}
