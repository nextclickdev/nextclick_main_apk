package com.apticks.nextclickuser.Activities.MAIN;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.apticks.nextclickuser.R;
import com.bumptech.glide.Glide;

/**
 * Created by deepshikha on 6/7/17.
 */
public class MyFragment extends Fragment {

    View view;
    ImageView iv_image;

    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String IMAGE = "EXTRA_IMAGE";

    String images;

    public static final MyFragment newInstance(String message, String int_image)

    {

      //  this.images=int_image;

        MyFragment f = new MyFragment();

        Bundle bdl = new Bundle(1);
        bdl.putString(EXTRA_MESSAGE, message);
        bdl.putString(IMAGE, int_image);

        f.setArguments(bdl);

        return f;

    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        String message = getArguments().getString(EXTRA_MESSAGE);
        String int_image = getArguments().getString(IMAGE);
      //  int int_image = getArguments().getInt(IMAGE);

        view = inflater.inflate(R.layout.myfragment_layout, container, false);

        TextView messageTextView = (TextView) view.findViewById(R.id.textView);

        ImageView iv_image = (ImageView) view.findViewById(R.id.iv_image);

      //  System.out.println("aaaaaaaa  images  "+int_image);
        Glide.with(getContext())
                .load(int_image)
                .into(iv_image);

       // iv_image.setImageResource(int_image);
        messageTextView.setText(message);

        return view;

    }

}
