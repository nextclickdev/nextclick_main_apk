package com.apticks.nextclickuser.Activities;

/*
 * @author : sarath
 *
 * @desc : Home screen with needy details
 *
 * */


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Activities.ui.home.EcomHomeFragment;
import com.apticks.nextclickuser.Fragments.CartFragment;
import com.apticks.nextclickuser.Fragments.Dasboard_Fragment;
import com.apticks.nextclickuser.Fragments.LogoutFragment;
import com.apticks.nextclickuser.Pojo.DisplayCategory;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;

public class HomeScreenActivity extends AppCompatActivity {

    Fragment selectedFragment = null;
    Context mContext;

    ArrayList<String> hsName = new ArrayList<>();
    ArrayList<String> hsImage = new ArrayList<>();
    ArrayList<DisplayCategory> data = new ArrayList<>();

    /*@Nullable
    @Override
    public View onCreateView(@Nullable View parent, @NonNull String name, @NonNull Context context, @NonNull AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }*/

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        getSupportActionBar().hide();
        mContext = getApplicationContext();

        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(HomeScreenActivity.this, R.color.colorPrimary));
        }
        // init();
        BottomNavigationView navView = findViewById(R.id.nav_view);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_shop, R.id.navigation_profile, R.id.navigation_cart, R.id.navigation_logout)
                .build();
      /*  NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);*/

        String typeFrag = getIntent().getStringExtra("type");

        if(typeFrag.equalsIgnoreCase("ecom")){
            selectedFragment = new EcomHomeFragment(getApplicationContext());
        }
        else if(typeFrag.equalsIgnoreCase("food")){
            selectedFragment = new Dasboard_Fragment(getApplicationContext(),"4");
        }
        else if(typeFrag.equalsIgnoreCase("hospital")){
            selectedFragment = new Dasboard_Fragment(getApplicationContext(),"1");
        }
        else if(typeFrag.equalsIgnoreCase("home")){
            selectedFragment = new Dasboard_Fragment(getApplicationContext(),"0");
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, selectedFragment).commit();
        BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
                = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.navigation_home:
                        /*Intent intent = new Intent (Dashboard.this, Add_Executive_Reg.class);
                        intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity (intent);
                        finish();
                        return true;*/
                        selectedFragment = new Dasboard_Fragment(getApplicationContext());
                        navView.setVisibility(View.VISIBLE);
                        break;

                    case R.id.navigation_shop:
                        //return true;
                        /*Intent ecommerceIntent = new Intent(HomeScreenActivity.this, EcommerceHomeActivity.class);

                        startActivity (ecommerceIntent);*/

                        selectedFragment = new EcomHomeFragment(getApplicationContext());
                        //navView.setVisibility(View.INVISIBLE);
                        break;

                    case R.id.navigation_profile:
                        Intent intent2 = new Intent(HomeScreenActivity.this, Cars_Travels_Vendors.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent2);
                        finish();
                        //return true;
                        /*  selectedFragment = new ProfileFragment(getApplicationContext());*/
                        navView.setVisibility(View.VISIBLE);
                        break;

                    case R.id.navigation_cart:

                        //selectedFragment = new CartFragment(mContext);

                       /* CartFragment cartFragment = new CartFragment(mContext);
                        FragmentTransaction ft=getFragmentManager().beginTransaction();
                        //SecondFrag secondFrag=new SecondFrag();
                        ft.addToBackStack("addAddressFrag");
                        ft.replace(R.id.nav_host_fragment,cartFragment);
                        ft.commit();*/

                        selectedFragment = new CartFragment(getApplicationContext());
                        navView.setVisibility(View.VISIBLE);
                        break;


                    case R.id.navigation_logout:
                       /* Intent intent1 = new Intent (Dashboard.this, Executive_List.class);
                        intent1.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity (intent1);
                        finish();*/
                        //return true;
                        selectedFragment = new LogoutFragment(getApplicationContext());
                        navView.setVisibility(View.VISIBLE);
                        break;
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, selectedFragment).commit();
                return true;
            }
        };

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.nav_view);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //hospitalDataFetcher();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onResume() {
        super.onResume();
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(HomeScreenActivity.this, R.color.colorPrimary));
        }
    }

    /*@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onStart() {
        super.onStart();
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(HomeScreenActivity.this, R.color.colorPrimary));
        }
    }*/

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onRestart() {
        super.onRestart();
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.orange));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(HomeScreenActivity.this, R.color.colorPrimary));
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainCategoriesActivity.class);
        startActivity(intent);
        finish();
    }
}
