package com.apticks.nextclickuser.Activities.addtocart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Adapters.SectionItemsModelInnerRecycler;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.Money;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.apticks.nextclickuser.Activities.AddToCartActivity.sectionItemsRadioSelect;

public class SectionAdapter extends RecyclerView.Adapter<SectionAdapter.ViewModel> {

    Context mContext;
    ArrayList<SecItems> sectionlist;
    int secPrice;
    Boolean radioCheckBoolean;
    private int lastSelectedPosition = -1;
    private int countRadio = 0;
    private int lastSelectedPositionUnselect = 0;

    public SectionAdapter(Context mContext, ArrayList<SecItems> sectionlist,int secPrice,Boolean radioCheckBoolean) {
        this.mContext=mContext;
        this.sectionlist=sectionlist;
        this.secPrice=secPrice;
        this.radioCheckBoolean=radioCheckBoolean;
    }

    @NonNull
    @Override
    public SectionAdapter.ViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inner_recycle,parent,false);
        mContext = parent.getContext();

        return  new SectionAdapter.ViewModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SectionAdapter.ViewModel holder, int position) {
        SecItems  sectionItemsRadioModel = sectionlist.get(position);
        if (secPrice==3) {
            holder.price.setVisibility(View.GONE);
        }else {
           holder.price.setVisibility(View.VISIBLE);
        }
        holder.textViewName.setText(sectionItemsRadioModel.getName());

        String buyMRP = Money.rupees(
                BigDecimal.valueOf(Long.valueOf(sectionItemsRadioModel.getPrice()
                ))).toString();
        holder.price.setText(buyMRP);
        if (radioCheckBoolean){
            holder.radioButton.setVisibility(View.VISIBLE);
            holder.checkBox.setVisibility(View.GONE);

            holder.radioButton.setChecked(lastSelectedPosition == position);
            /*if (lastSelectedPositionUnselect != lastSelectedPosition){
                //Toast.makeText(mContext, " Size " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                if (lastSelectedPosition>-1 */
            /*&& lastSelectedPositionUnselect==0*//*){
                    if (lastSelectedPositionUnselect==-1){
                        onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(0).getPrice());
                        Toast.makeText(mContext, " Size -1 " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                    }else{
                        onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(lastSelectedPositionUnselect).getPrice());
                        Toast.makeText(mContext, " Above -1 Size  " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                    }
                    //onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                    //lastSelectedPositionUnselect = lastSelectedPosition;
                }else{
                    //onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                    //lastSelectedPositionUnselect = lastSelectedPosition;
                    //UImsgs.showToast(mContext,"Else Here");
                }
            }else {
                Toast.makeText(mContext, " Else " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
            }*/

            if (countRadio==0){
                countRadio++;
                holder.radioButton.setChecked(0 == position);
              //  onItemCheckListener.onItemCheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
            }

            if (holder.radioButton.isChecked()) {
                //Toast.makeText(mContext, "Checked "+holder.radioButton.isChecked(), Toast.LENGTH_SHORT).show();
                holder.radioButton.setClickable(false);
                holder.radioButton.setEnabled(false);
            }else {
                //UImsgs.showToast(mContext, "Un - Checked "+holder.radioButton.isChecked());
                holder.radioButton.setClickable(true);
                holder.radioButton.setEnabled(true);
            }

            holder.radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //lastSelectedPositionUnselect=lastSelectedPosition;
                    lastSelectedPosition = position;
                    notifyDataSetChanged();
                    sectionItemsRadioSelect = sectionItemsRadioModel.getId();

                  //  onItemCheckListener.onItemCheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());


                    if (lastSelectedPositionUnselect != lastSelectedPosition){
                        //Toast.makeText(mContext, " Size " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                        if (lastSelectedPosition>-1 /*&& lastSelectedPositionUnselect==0*/){
                            /*if (lastSelectedPositionUnselect==-1){
                                onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(0).getPrice());
                                Toast.makeText(mContext, " Size -1 " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                            }else{
                                onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(lastSelectedPositionUnselect).getPrice());
                                Toast.makeText(mContext, " Above -1 Size  " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                            }*/
                        //    onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(lastSelectedPositionUnselect).getPrice());
                            lastSelectedPositionUnselect=lastSelectedPosition;
                        }else{

                        }
                    }else {
                        //Toast.makeText(mContext, " Else " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                    }
                }
            });



        }else {
            holder.radioButton.setVisibility(View.GONE);
            holder.checkBox.setVisibility(View.VISIBLE);
            //Validations For Check Box

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*((ViewModel) holder).checkBox.setChecked(
                            !((ViewModel) holder).checkBox.isChecked());*/
                  /*  if (((SectionItemsModelInnerRecycler.ViewModel) holder).checkBox.isChecked()) {
                     //   onItemCheckListener.onItemCheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                        //UImsgs.showToast(mContext,String.valueOf(sectionItemsRadioModel.getId()));
                    } else {
                     //   onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                        //UImsgs.showToast(mContext,String.valueOf(sectionItemsRadioModel.getId()));
                    }*/
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return sectionlist.size();
    }

    public void setrefresh(ArrayList<SecItems> sectionlist, int secPrice,Boolean radioCheckBoolean) {
        this.secPrice=secPrice;
        this.sectionlist=sectionlist;
        this.radioCheckBoolean=radioCheckBoolean;
        notifyDataSetChanged();
    }

    class ViewModel extends RecyclerView.ViewHolder{
        RadioButton radioButton;
        CheckBox checkBox;
        TextView textViewName ,price;
        public ViewModel(@NonNull View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.radio_select_inner);
            checkBox = itemView.findViewById(R.id.check_select_inner);
            textViewName = itemView.findViewById(R.id.title_name_inner);
            price = itemView.findViewById(R.id.description_price_inner);
        }
    }
}
