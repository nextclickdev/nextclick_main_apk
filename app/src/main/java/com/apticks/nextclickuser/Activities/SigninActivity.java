package com.apticks.nextclickuser.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.R;
import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Activities.LoginActivity.fb_button;

public class SigninActivity extends AppCompatActivity {

    LoginButton loginButton;
    CallbackManager callbackManager;
    ImageView imageView;
    TextView txtUsername, txtEmail;
    String TAG = "SigninActivitytag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        loginButton = findViewById(R.id.login_button);
        imageView = findViewById(R.id.imageView);
        txtUsername = findViewById(R.id.txtUsername);
        txtEmail = findViewById(R.id.txtEmail);
        FacebookSdk.sdkInitialize(getApplicationContext());



        boolean loggedOut = AccessToken.getCurrentAccessToken() == null;

        if (!loggedOut) {
            Glide.with(SigninActivity.this).load(Profile.getCurrentProfile().getProfilePictureUri(200, 200))
                    .into(imageView);
           // Picasso.with(this).load(Profile.getCurrentProfile().getProfilePictureUri(200, 200)).into(imageView);
            Log.d("TAG", "Username is: " + Profile.getCurrentProfile().getName());

            //Using Graph API
            getUserProfile(AccessToken.getCurrentAccessToken());
        }
       /* AccessTokenTracker fbTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken2) {
                if (accessToken2 == null) {
                    txtUsername.setText(null);
                    txtEmail.setText(null);
                    imageView.setImageResource(0);
                    Toast.makeText(getApplicationContext(),"User Logged Out.",Toast.LENGTH_LONG).show();
                }
            }
        };
        fbTracker.startTracking();*/

        try{
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.apticks.nextclickuser",
                    PackageManager.GET_SIGNATURES);
            for(Signature signature: packageInfo.signatures){
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.d("Key Hash", Base64.encodeToString(messageDigest.digest(),Base64.DEFAULT));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
      //  loginButton.setReadPermissions(Arrays.asList("EMAIL"));
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();

      /*  LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        System.out.println("aaaaaaa  loginresult "+loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        System.out.println("aaaaaaa  cancel ");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        System.out.println("aaaaaaa  exception ");
                    }
                });*/



        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                //loginResult.getAccessToken();
                //loginResult.getRecentlyDeniedPermissions()
                //loginResult.getRecentlyGrantedPermissions()
                System.out.println("aaaaaaaa  "+loginResult.getAccessToken());
                boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
                if (!loggedOut) {
                    Log.d("TAG", "Username is: " + Profile.getCurrentProfile().getName());

                    //Using Graph API0
                    getUserProfile(AccessToken.getCurrentAccessToken());
                }
                Log.d("API123", loggedIn + " ??");

            }

            @Override
            public void onCancel() {
                System.out.println("aaaaaaaa  cancle ");
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("aaaaaaaa  cancle "+exception.getMessage());
                // App code
            }
        });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbCallBack();
            }
        });
    }

    public void fbCallBack() {
        // Callback registration
       /* fb_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(SigninActivity.this, Arrays.asList("email", "user_photos", "public_profile"));
            }
        });*/
        LoginManager.getInstance().logInWithReadPermissions(SigninActivity.this, Arrays.asList("email", "user_photos", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                //handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                Log.d(TAG, "facebook:onCancel");
                // [START_EXCLUDE]
                //updateUI(null);
                // [END_EXCLUDE]
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, "facebook:onError", exception);
            }
        });


        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        //Log.d(TAG, "facebook:loginmanager" + loginResult.toString());
                        //final AccessToken accessToken = loginResult.getAccessToken();
                        //handleFacebookAccessToken(loginResult.getAccessToken());

                        GraphRequest graphRequest = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject jsonObject, GraphResponse response) {
                                        if (response.getError() != null) {
                                            Toast.makeText(SigninActivity.this, "Something went wrong please try after some time", Toast.LENGTH_SHORT).show();
                                        } else {
                                            try {
                                                String id = jsonObject.getString("id");
                                                String facebookName = jsonObject.optString("name");
                                                String facebookEmailId = jsonObject.optString("email");
                                                String facebookDateOfBirth = jsonObject.optString("birthday");
                                                String facebookProfilePic = "";
                                                if (jsonObject.has("picture")) {
                                                    facebookProfilePic = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");
                                                }
                                                facebookProfilePic = "https://graph.facebook.com/" + id + "/picture?type=large";


                                                Map<Object, Object> fbmap = new HashMap<>();
                                                fbmap.put("auth_id", id);
                                                fbmap.put("auth_token", "");

                                                fbmap.put("mail", facebookEmailId);
                                                fbmap.put("mobile", "");
                                                fbmap.put("name", facebookName);

                                            } catch (Exception exception) {
                                                exception.printStackTrace();
                                            }
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,link,email,first_name,last_name,gender,location,birthday,picture");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d(TAG, "facebook:loginmanager cancelled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d(TAG, "facebook:loginmanager", exception);
                    }
                });
    }
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    private void getUserProfile(AccessToken currentAccessToken) {
        System.out.println("aaaaaaaaa accesstoken  "+currentAccessToken);
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                          //  String email = object.getString("email");
                            String id = object.getString("id");
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                            System.out.println("aaaaaaaaaa  exp "+first_name+"  "+last_name+"  "+image_url);
                            txtUsername.setText("First Name: " + first_name);
                            txtEmail.setText(last_name);
                            Glide.with(SigninActivity.this).load(image_url)
                                    .into(imageView);
                           // Picasso.with(SigninActivity.this).load(image_url).into(imageView);

                        } catch (JSONException e) {
                            System.out.println("aaaaaaaaaa  exp "+e.getMessage());
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }




}