package com.apticks.nextclickuser.Activities.USERPROFILE;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Constants.IErrors;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.Utility;
import com.apticks.nextclickuser.Helpers.Validations;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.OrderHistoryActivity;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.bumptech.glide.Glide;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.Gravity.CENTER;
import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.Constants.MOBILE;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.*;

public class UserProfileEditActivity extends AppCompatActivity {
    EditText first_name_edit, last_name_edit, emailid_edit, phone_number_edit;
    TextView myusername, text_edit_profile, text_cancel_edit_profile, text_subit_profile;
    CircleImageView profileImage;
    LinearLayout linear_edit_profile, linear_submit_profile;
    char imageSelection = 'c';
    Context mContext;
    PreferenceManager preferenceManager;
    Validations validations;
    Animation animation;
    private CustomDialog mCustomDialog;

    public int flag = 0;
    private String userChoosenTask, profileString;
    private static final int CAMERA_REQUEST = 8;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 15, SELECT_MULTIPLE_FILE = 1;
    private ImageView back_imageView;

    int IMG_WIDTH = 400, IMG_HEIGHT = 400;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_edit);
        mContext = getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);
        validations = new Validations();
        animation = AnimationUtils.loadAnimation(mContext, R.anim.shake);

        getSupportActionBar().hide();

        initView();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        /*//window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            }
        }*/
        back_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


        fetchUserDetails();
        textEditProfileClick();
        textSubitProfileClick();
        textCancelEditProfileClick();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
//Image Selection Start


    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(UserProfileEditActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(UserProfileEditActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK && imageSelection == 'c') {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            profileImage.setImageBitmap(photo);
            Bitmap bitmap = ((BitmapDrawable) profileImage.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            profileString = resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT));
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }

    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);


        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);

        if (imageSelection == 'c') {

            profileImage.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) profileImage.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            profileString = resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT));
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }

    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void textCancelEditProfileClick() {
        text_cancel_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear_submit_profile.setVisibility(View.GONE);
                linear_edit_profile.setVisibility(View.VISIBLE);

                profileImage.setEnabled(false);
                first_name_edit.setEnabled(false);
                last_name_edit.setEnabled(false);
                emailid_edit.setEnabled(false);
                phone_number_edit.setEnabled(false);

                fetchUserDetails();
            }
        });
    }

    private void textSubitProfileClick() {
        text_subit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    Map<String, String> maptoparse = new HashMap<>();
                    maptoparse.put("first_name", first_name_edit.getText().toString());
                    maptoparse.put("last_name", last_name_edit.getText().toString());
                    maptoparse.put("email", emailid_edit.getText().toString());
                    maptoparse.put("mobile", phone_number_edit.getText().toString());
                    maptoparse.put("unique_id", myusername.getText().toString());
                    try {
                        Bitmap bitmap = ((BitmapDrawable) profileImage.getDrawable()).getBitmap();

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                        byte[] byteArray = baos.toByteArray();
                        profileString = Base64.encodeToString(byteArray, Base64.DEFAULT);
                        maptoparse.put("image", profileString);
                        Log.d("image", profileString);
                    } catch (Exception e) {
                        e.printStackTrace();
                        profileString = "";
                        maptoparse.put("image", profileString);
                        Log.d("image", profileString);

                    }
                    JSONObject jsonObject = new JSONObject(maptoparse);

                    postUpdateProfile(jsonObject);
                }
            }
        });
    }

    private void postUpdateProfile(JSONObject jsonObject) {

        final String data = jsonObject.toString();
        final ProgressDialog progressDialog = new ProgressDialog(UserProfileEditActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while updating your profile.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        final String dataStr = jsonObject.toString();
        Log.d("VolleyResponse", "USER_PROFILE_U request: " + dataStr);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, USER_PROFILE_U, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, "", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

                try {
                    JSONObject   jsonObject = new JSONObject(response);
                    Log.d("VolleyResponse", "USER_PROFILE_U: " + response);
                    System.out.println("aaaaaaaa jsonobject  " + jsonObject.toString());
                    boolean status = jsonObject.getBoolean("status");
                    if (status) {
                        preferenceManager.putString("email",emailid_edit.getText().toString());
                        preferenceManager.putString("number",phone_number_edit.getText().toString());
                        succesDialog("Your Profile has been updated sucessfully");
                    }
                    else
                        showErrorDialog(jsonObject.getString("data"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                UImsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void showErrorDialog(String errorMessage) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_error_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        TextView tv_success= (TextView) dialog.findViewById(R.id.tv_success);
        tv_success.setText(errorMessage);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private boolean validate() {
        if (validations.isBlank(first_name_edit)) {
            first_name_edit.setError(IErrors.EMPTY);
            first_name_edit.setAnimation(animation);
            return false;
        } else if (validations.isValidEmail(emailid_edit.getText().toString())) {
            emailid_edit.setError(IErrors.INVALID_EMAIL);
            emailid_edit.setAnimation(animation);
            return false;
        } else if (validations.isBlank(phone_number_edit)) {
            phone_number_edit.setError(IErrors.EMPTY);
            phone_number_edit.setAnimation(animation);
            return false;
        } else if (validations.isValidNumber(phone_number_edit)) {
            phone_number_edit.setError(IErrors.EMPTY);
            phone_number_edit.setAnimation(animation);
            return false;
        }
        return true;
    }

    private void textEditProfileClick() {
        text_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear_submit_profile.setVisibility(View.VISIBLE);
                linear_edit_profile.setVisibility(View.GONE);

                profileImage.setEnabled(true);
                first_name_edit.setEnabled(true);
                last_name_edit.setEnabled(true);
                emailid_edit.setEnabled(true);
                phone_number_edit.setEnabled(true);
                profileImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean selecting = selectImage();
                        if (selecting) {

                        }
                    }
                });
            }
        });
    }

    private void fetchUserDetails() {
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, USER_PROFILE_R, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("editres", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    try {
                        myusername.setText(jsonObjectData.getString("unique_id"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    String firstName = jsonObjectData.getString("first_name");
                    String lastName = jsonObjectData.getString("last_name");
                    String emailId = jsonObjectData.getString("email");
                    String phoneNumber = jsonObjectData.getString("phone");

                    if (!firstName.equalsIgnoreCase("null"))
                        first_name_edit.setText(firstName);
                    if (!lastName.equalsIgnoreCase("null"))
                        last_name_edit.setText(lastName);
                    if (!emailId.equalsIgnoreCase("null")) {
                        preferenceManager.putString("email", emailId);
                        emailid_edit.setText(emailId);
                    }
                    if (!phoneNumber.equalsIgnoreCase("null")) {
                        preferenceManager.putString("number", phoneNumber);
                        phone_number_edit.setText(phoneNumber);
                    }

                    if (jsonObjectData.getString("image") !=null) {
                        String imageUrl = jsonObjectData.getString("image");
                        if (imageUrl != null && !imageUrl.isEmpty()) {
                            Picasso.get().load(imageUrl)
                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                                    .into(profileImage);
                        }
                        else
                        {
                            profileImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_navigate));
                        }
                    }else {
                        try {
                            String logintype = preferenceManager.getString("login");
                            if (logintype!=null && logintype.equalsIgnoreCase("fb")) {
                                //profileImage.setImageBitmap(Validations.getFacebookProfilePicture(context));
                                String facebookImageUrl = preferenceManager.getString("fb_img_url");
                                if (facebookImageUrl != null) {
                                    if (!facebookImageUrl.isEmpty()) {
                                        Picasso.get().load(facebookImageUrl).into(profileImage);
                                    }
                                }
                                else
                                {
                                    profileImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_navigate));
                                }
                            } else {
                                String imageUrl = preferenceManager.getString("gmail_image_url");
                                if (imageUrl != null) {
                                    if (!imageUrl.isEmpty()) {
                                        Picasso.get().load(imageUrl).into(profileImage);
                                    }
                                }
                                else
                                {
                                    profileImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_navigate));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String imageUrl = preferenceManager.getString("gmail_image_url");
                            if (imageUrl != null) {
                                if (!imageUrl.isEmpty()) {
                                    Picasso.get().load(imageUrl).into(profileImage);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                finally {
                   mCustomDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               mCustomDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                Log.d("edit_X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }
        };
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void initView() {
        mCustomDialog=new CustomDialog(this);
        //Edit Text
        first_name_edit = findViewById(R.id.first_name_edit);
        last_name_edit = findViewById(R.id.last_name_edit);
        emailid_edit = findViewById(R.id.emailid_edit);
        phone_number_edit = findViewById(R.id.phone_number_edit);

        //Text View
        myusername = findViewById(R.id.myusername);
        text_edit_profile = findViewById(R.id.text_edit_profile);
        text_cancel_edit_profile = findViewById(R.id.text_cancel_edit_profile);
        text_subit_profile = findViewById(R.id.text_subit_profile);

        //Image View
        profileImage = findViewById(R.id.profileImage_edit);
        back_imageView = findViewById(R.id.back_imageView);

        //Linear Layout
        linear_submit_profile = findViewById(R.id.linear_submit_profile);
        linear_edit_profile = findViewById(R.id.linear_edit_profile);
    }

    private void succesDialog(String message){
        ImageView success_gif;
        Button success_ok_btn;
        TextView tv_ordermessage;
        final Dialog dialog = new Dialog(this);
        Window window = dialog.getWindow();
        //window.requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.order_success_layout);
        dialog.setCancelable(false);
        window.setGravity(CENTER);

        success_ok_btn = dialog.findViewById(R.id.success_ok_btn);
        success_gif = dialog.findViewById(R.id.success_gif);
        tv_ordermessage = dialog.findViewById(R.id.tv_ordermessage);

        tv_ordermessage.setText(message);
        // tv_ordermessage.setText(""+message);
        Glide.with(mContext)
                .load(R.drawable.ordsucess)
                .into(success_gif);

        success_ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


                linear_submit_profile.setVisibility(View.GONE);
                linear_edit_profile.setVisibility(View.VISIBLE);

                profileImage.setEnabled(false);
                first_name_edit.setEnabled(false);
                last_name_edit.setEnabled(false);
                emailid_edit.setEnabled(false);
                phone_number_edit.setEnabled(false);
                preferenceManager.putString(userName, first_name_edit.getText().toString() + " " + last_name_edit.getText().toString());
                preferenceManager.putString(MOBILE, phone_number_edit.getText().toString());

                fetchUserDetails();
            }
        });


        window.setGravity(CENTER);
        dialog.show();
    }
}
