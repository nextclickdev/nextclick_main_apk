package com.apticks.nextclickuser.Helpers;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Random;

public class TimeStamp {


    public String timeStampFetcher() {
        String timeStamp = "";
        //Date object
        Date date = new Date();
        //getTime() returns current time in milliseconds
        long time = date.getTime();
        //Passed the milliseconds to constructor of Timestamp class
        Timestamp ts = new Timestamp(time);
        timeStamp = generateRandom() + ts;
        return "?"+timeStamp;
    }


    public String generateRandom() {

        // create instance of Random class
        Random rand = new Random();
        // Generate random integers in range 0 to 999
        int rand_int1 = rand.nextInt(1000);
        return rand_int1 + "";
    }

}
