package com.apticks.nextclickuser.Helpers;

import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

public class CustomItemClickListener extends AppCompatActivity  {

    public CustomItemClickListener(Context context) {

    }
    public CustomItemClickListener() {

    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        try {
            if (count == 0) {
                super.onBackPressed();
                //additional code
            } else {
                getSupportFragmentManager().popBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();

            Log.v("On Back Pressed", String.valueOf(e));
        }
    }
}