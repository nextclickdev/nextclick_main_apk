package com.apticks.nextclickuser.utils;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Message;

public abstract class MyLocationManager {

    private static MyLocationManager myLocationManager = null;
    protected static boolean useGooglePlayLocationSerevices = true;
    protected static Context mContext;

    public static byte MYLOC_ECODE_PERMISSION_DENIED 	= 1;
    public static byte MYLOC_ECODE_POSITION_UNAVAILABLE = 2;

    public static String MYLOC_EMSG_POSITION_UNAVAILABLE = "POSITION_UNAVAILABLE";



    public static void SetContext(Context context) {
        mContext = context;
    }

    public static MyLocationManager getInstance() {
        if(myLocationManager == null){
            if(useGooglePlayLocationSerevices && MyGooglePlayLocationManager.isGooglePlayServicesAvailable(mContext))
                myLocationManager = new MyGooglePlayLocationManager();
            else
                myLocationManager = new MyAndroidLocationManager();
        }
        return myLocationManager;
    }

    public abstract void getCurrentPosition(MyLocationListener listener);

    public abstract void startLocationUpdates(MyLocationListener listener);

    public abstract void removeLocationUpdates(MyLocationListener listener);

    public void executeOnLocationReceived(MyLocationListener listener, Location location){
        if(location!=null && listener != null) {
            listener.onLocationReceived(location);
        }
    }
    public void executeOnLocationUpdated(MyLocationListener listener, Location location){
        if(location!=null &&listener != null) {
            listener.onLocationUpdated(location);
        }
    }
    public void executeErrorCallback(MyLocationListener listener,int code, String message) {
        if(listener != null) {
            listener.onLocationFailed(code,message);
        }
    }
}
