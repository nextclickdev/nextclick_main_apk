package com.apticks.nextclickuser.utils;

import android.content.Context;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.apticks.nextclickuser.R;
import com.bumptech.glide.Glide;

public class ImageUtil {

    public static void LoadImageFromUrl(Context mContext, String imgPath,
                                        int defaultImage, ImageView imgView)
    {
        if (URLUtil.isValidUrl(imgPath)) {
            Glide.with(mContext)
                    .load(imgPath)
                    .placeholder(R.drawable.loader_gif)
                    .into(imgView);
        } else {
            Glide.with(mContext)
                    .load(defaultImage)
                    .into(imgView);
        }
    }
}
