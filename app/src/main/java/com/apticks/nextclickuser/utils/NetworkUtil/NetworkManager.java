package com.apticks.nextclickuser.utils.NetworkUtil;

import android.content.Context;
import android.util.Log;

import com.apticks.nextclickuser.Config.Config;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkManager {

    Context context;
    ResponseListner responseListner;

    public NetworkManager(Context context, ResponseListner responseListner) {
        this.context = context;
        this.responseListner = responseListner;
    }

    public void callWebServices(ServiceMethods serviceMethods, Map<String, String> headers, Object request) {
        final ResponseDO responseDO = new ResponseDO();
        responseDO.setServiceMethods(serviceMethods);

        //Adding Autherization
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        //for timeout
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.writeTimeout(60, TimeUnit.SECONDS);




        if (headers != null) {
            System.out.println("aaaaaaaa headers call ");
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    System.out.println("aaaaaaaa headers call 111111 ");
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder();
                    Iterator iterator = headers.entrySet().iterator();
                    while (iterator.hasNext()) {
                        System.out.println("aaaaaaaa headers call 22222");
                        Map.Entry me2 = (Map.Entry) iterator.next();
                        requestBuilder.header(me2.getKey().toString(), me2.getValue().toString());
                      //  requestBuilder.method(original.method(),original.body());
                        System.out.println("aaaaaaaa headers  "+me2.getKey().toString()+"  "+me2.getValue().toString());
                    }
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        OkHttpClient client = httpClient.build();

        //Using Retrofit and RxJava
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();

        Api api = retrofit.create(Api.class);


        Observable<Response<ResponseBody>> observable = null;
        //GetObservable based on methods
        switch (serviceMethods) {
            case WS_LOGIN:
                observable = api.login((Map<String, Object>) request);
                break;
            case FCM:
                observable = api.fcmLogin((Map<String, Object>) request);
                break;
            case Sliders_And_Advertisements:
                observable = api.Sliders();
                break;
            case PROFILE_READ:
                observable = api.getUserProfile((Map<String, Object>) request);
                break;
            case PROFILE_UPDATE:
                observable = api.profileUpdate((Map<String, Object>) request);
                break;
            case FORGOT_PASSWORD:
                observable = api.forgotPassword((Map<String, Object>) request);
                break;
            case MANAGE_LOGIN_SESSION:
                observable = api.manageLoginSession((Map<String, Object>) request);
                break;
            case UPDATE_CURRENT_LOCATION:
                observable = api.updateDeliveryBoyLocation((Map<String, Object>) request);
                break;
            case GET_NEAREST_VENDORS:
                observable = api.getNearestVendors((Map<String, Object>) request);
                break;
            case GET_NOTIFICATIONS:
                observable = api.getDeliveryNotifications((Map<String, Object>) request);
                break;
            case SET_NOTIFICATION_ACCEPT:
                observable = api.acceptNotification((Map<String, Object>) request);
                break;
            case GET_ORDERS_LIST:
                observable = api.getDeliveryOrdersList();
                break;
            case CHANGE_DELIVERY_ORDER_STATUS:
                observable = api.changeDeliveryOrderStatus((Map<String, Object>) request);
                break;
            case INVOKE_DELIVERY_ORDER:
                observable = api.orderDelivery((Map<String, Object>) request);
                break;
            case ORDERDETAILS:
                observable = api.getOrderDetails((Map<String, Object>) request);
                break;
            case WALLETHISTORY:
                observable = api.getPaymentWallet((Map<String, Object>) request);
                break;
            case GET_DASHBOARD_DETAILS:
                observable = api.getDashboardDetails();
                break;
        }

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.e("onSubscribe", "onSubscribe" + d.isDisposed());
                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        try {
                            if (value != null && value.body() != null) {
                                String response = value.body().string();
                                response = response.replace("&quot;", "\"");
                                responseDO.setError(false);
                                responseDO.setResponse(response);
                                responseDO.setCode(value.raw().code());
                                Log.e("Response", response);

                            } else if (value.errorBody() != null) {
                                responseDO.setError(true);
                                responseDO.setErrorCode(value.raw().code());
                                if (value.errorBody().source() != null)
                                    responseDO.setResponse(value.errorBody().source().toString().toString());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.e("onError", "onError");
                        responseDO.setError(true);
                        responseDO.setResponse("" + (e != null ? e.getMessage() : "Something went wrong."));
                        responseListner.onResponseReceived(responseDO);
                    }

                    @Override
                    public void onComplete() {
                        Log.e("onComplete", "onComplete");
                        responseListner.onResponseReceived(responseDO);
                    }
                });
    }


}
