package com.apticks.nextclickuser.utils.NetworkUtil;

public class ResponseDO {
    private ServiceMethods serviceMethods;
    private String response;
    private boolean isError = false;
    private int errorCode = -1;
    private int code = -1;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ServiceMethods getServiceMethods() {
        return serviceMethods;
    }

    public void setServiceMethods(ServiceMethods serviceMethods) {
        this.serviceMethods = serviceMethods;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}