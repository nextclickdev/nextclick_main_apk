package com.apticks.nextclickuser.utils.NetworkUtil;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Api {


    @POST("auth/api/auth/login")
    Observable<Response<ResponseBody>> login(@Body Map<String, Object> request);

    @POST("general/api/fcm_notify/grant_fcm_permission")
    Observable<Response<ResponseBody>> fcmLogin(@Body Map<String, Object> request);

    @GET("general/api/master/sliders")
    Observable<Response<ResponseBody>> Sliders();

    @POST("user/master/profile/r")
    Observable<Response<ResponseBody>> getUserProfile(@Body Map<String, Object> request);

    @POST("user/master/profile/u")
    Observable<Response<ResponseBody>> profileUpdate(@Body Map<String, Object> request);

    @POST("/auth/api/auth/forgot_password")
    Observable<Response<ResponseBody>> forgotPassword(@Body Map<String, Object> request);

    @POST("delivery/api/delivery/manage_login_session")
    Observable<Response<ResponseBody>> manageLoginSession(@Body Map<String, Object> request);

    @POST("delivery/api/delivery/current_location/set")
    Observable<Response<ResponseBody>> updateDeliveryBoyLocation(@Body Map<String, Object> request);

    @POST("delivery/api/delivery/near_by_vendors")
    Observable<Response<ResponseBody>> getNearestVendors(@Body Map<String, Object> request);

    @POST("delivery/api/delivery/notifications/r")
    Observable<Response<ResponseBody>> getDeliveryNotifications(@Body Map<String, Object> request);

    @POST("delivery/api/delivery/notifications/accept")
    Observable<Response<ResponseBody>> acceptNotification(@Body Map<String, Object> request);

    @POST("delivery/api/orders/delivery_orders/r")
    Observable<Response<ResponseBody>> getDeliveryOrdersList();

    @POST("delivery/api/orders/delivery_orders/change_status")
    Observable<Response<ResponseBody>> changeDeliveryOrderStatus(@Body Map<String, Object> request);

    @POST("delivery/api/orders/delivery_orders/deliver")
    Observable<Response<ResponseBody>> orderDelivery(@Body Map<String, Object> request);

    @POST("vendor/api/ecom/ecom_orders/order_details")
    Observable<Response<ResponseBody>> getOrderDetails(@Body Map<String, Object> request);

    @POST("payment/api/payment/wallet")
    Observable<Response<ResponseBody>> getPaymentWallet(@Body Map<String, Object> request);


    @GET("delivery/api/delivery/dashboard")
    Observable<Response<ResponseBody>> getDashboardDetails();
}

