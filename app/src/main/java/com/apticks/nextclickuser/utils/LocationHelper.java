package com.apticks.nextclickuser.utils;

import android.content.Context;

public class LocationHelper {
    public static void SetContext(Context context)
    {
        MyLocationManager.SetContext(context);
    }

    public static void getCurrentPosition(MyLocationListener listener) {
        MyLocationManager.getInstance().getCurrentPosition(listener);
    }
    public static void startLocationUpdates(MyLocationListener listener) {
        MyLocationManager.getInstance().startLocationUpdates(listener);
    }
    public static void removeLocationUpdates(MyLocationListener listener) {
        MyLocationManager.getInstance().removeLocationUpdates(listener);
    }
}
