package com.apticks.nextclickuser.products.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.ProductDescriptionActivity;
import com.apticks.nextclickuser.products.activities.ProductListActivity;
import com.apticks.nextclickuser.products.fragmants.BottomVarientsFragment;
import com.apticks.nextclickuser.products.fragmants.ProductsFragment;
import com.apticks.nextclickuser.products.model.ImagesModel;
import com.apticks.nextclickuser.products.model.ProductsModel;
import com.apticks.nextclickuser.products.model.VarientModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Constants.Constants.ADDRESS;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isNull;
import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isnullboolean;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder>   {

    ArrayList<ImagesModel> imagelist;
    Context context;

    public ImagesAdapter(Context mContext, ArrayList<ImagesModel> imagelist) {
        this.context=mContext;
        this.imagelist=imagelist;
    }


    @NonNull
    @Override
    public ImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.imagelist, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new ImagesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesAdapter.ViewHolder holder, int position) {
        Glide.with(context)
                .load(imagelist.get(position).getImage())
                .placeholder(R.drawable.loader_gif)
                .into(holder.img_product);

        if (imagelist.get(position).isIsselect()){
            holder.layout_background.setBackgroundColor(context.getResources().getColor(R.color.orange));
        }else {
            holder.layout_background.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        holder.layout_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProductDescriptionActivity)context).setimagerefresh(position,imagelist.get(position));
            }
        });
    }


    public void setrefresh(ArrayList<ImagesModel> imagelist){
        this.imagelist=imagelist;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return imagelist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_product;
        private LinearLayout layout_background;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_product=itemView.findViewById(R.id.img_product);
            layout_background=itemView.findViewById(R.id.layout_background);

        }
    }
}
