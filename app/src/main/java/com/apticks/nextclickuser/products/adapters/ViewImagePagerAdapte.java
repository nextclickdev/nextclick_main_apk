package com.apticks.nextclickuser.products.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.ProductDescriptionActivity;
import com.apticks.nextclickuser.products.model.ImagesModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ViewImagePagerAdapte extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<ImagesModel> imagelist;
    public ViewImagePagerAdapte(Context context, ArrayList<ImagesModel> imageslist) {
        this.context = context;
        this.imagelist = imageslist;
    }

    @Override
    public int getCount() {
        return imagelist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.imagedisplaylayout, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

        System.out.println("aaaaaaaaa mages "+imagelist.get(position).getImage());

        Glide.with(context)
                .load(imagelist.get(position).getImage())
                .placeholder(R.drawable.loader_gif)
                .into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProductDescriptionActivity)context).setviewpageimageclick(imagelist.get(position).getImage(),imagelist);
            }
        });
        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}
