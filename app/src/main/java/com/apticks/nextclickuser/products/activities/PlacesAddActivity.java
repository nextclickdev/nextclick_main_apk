package com.apticks.nextclickuser.products.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.NetworkUtil;
import com.apticks.nextclickuser.Pojo.PLACES.PlacePojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.products.model.Constiuencies;
import com.apticks.nextclickuser.products.model.Districts;
import com.apticks.nextclickuser.products.model.SaveAddress;
import com.apticks.nextclickuser.products.model.States;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity.locationStatus;
import static com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity.location_tv;
import static com.apticks.nextclickuser.Config.Config.ADDRESSADD;
import static com.apticks.nextclickuser.Config.Config.ADDRESSUPDATE;
import static com.apticks.nextclickuser.Config.Config.PLACES_API_KEY;
import static com.apticks.nextclickuser.Config.Config.URL_States;
import static com.apticks.nextclickuser.Constants.Constants.AUTH_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.USER_TOKEN;
import static com.apticks.nextclickuser.Constants.IErrors.EMPTY;
import static com.apticks.nextclickuser.Constants.IErrors.INVALID_PHONE;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class PlacesAddActivity extends AppCompatActivity implements LocationListener {


    int AUTOCOMPLETE_REQUEST_CODE = 1;

    PlacesClient placesClient;
    Spinner spinner_state,spinner_distict,spinner_constitunesy;
    TextView current_location,tv_address,tv_title;
    private ImageView image_back;
    Context context;
    private LocationManager locationManager;
    private EditText et_currentaddress,et_land_mark,et_pincode,et_name,et_mobilenumber,et_email;
    private LinearLayout layout_editcurrentadd;
    AutocompleteSupportFragment autocompleteSupportFragment;
    int whichposition;
    String st_current_address="",st_landmark="",st_pincode="",st_name="",st_mobilenumber,st_mail;
    Double latitude,longitude;
    CustomDialog customDialog;
    private ArrayList<States> statelist;
    private ArrayList<Districts> districtlist;
    private ArrayList<Constiuencies> constiencieslist;
    private String stateid="",districtid="",constitueid="";
    PreferenceManager preferenceManager;
    int isupdate=0;
    SaveAddress saveAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_placess);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        TextView tv_header= findViewById(R.id.tv_header);
        tv_header.setText("Address");
        tv_header.setAllCaps(true);

        FrameLayout cart_layout =  findViewById(R.id.cart_layout);
        cart_layout.setVisibility(View.GONE);

        context = PlacesAddActivity.this;
        current_location = findViewById(R.id.current_location);
        et_currentaddress = findViewById(R.id.et_currentaddress);
        layout_editcurrentadd = findViewById(R.id.layout_editcurrentadd);
        tv_address = findViewById(R.id.tv_address);
        et_land_mark = findViewById(R.id.et_land_mark);
        et_pincode = findViewById(R.id.et_pincode);
        et_name = findViewById(R.id.et_name);
        et_mobilenumber = findViewById(R.id.et_mobilenumber);
        et_email = findViewById(R.id.et_email);
        image_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        spinner_state = findViewById(R.id.spinner_state);
        spinner_distict = findViewById(R.id.spinner_distict);
        spinner_constitunesy = findViewById(R.id.spinner_constitunesy);

        preferenceManager=new PreferenceManager(PlacesAddActivity.this);
          autocompleteSupportFragment =
                (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        customDialog=new CustomDialog(PlacesAddActivity.this);
        if(!Places.isInitialized()){
            Places.initialize(getApplicationContext(), PLACES_API_KEY);
        }
         whichposition=getIntent().getIntExtra("whichposition",0);
        statelist=new ArrayList<States>();
        districtlist=new ArrayList<Districts>();
        constiencieslist=new ArrayList<Constiuencies>();

        try{
            autocompleteSupportFragment.getView().setVisibility(View.GONE);
             saveAddress= (SaveAddress) getIntent().getSerializableExtra("data");
            System.out.println("aaaaaaaaa   saveaddress  "+saveAddress.getAddress()+" "+saveAddress.getName()+" "+saveAddress.getPincode());
            et_name.setText(saveAddress.getName());
            et_currentaddress.setText(saveAddress.getAddress());
            et_email.setText(saveAddress.getEmail());
            et_land_mark.setText(saveAddress.getLandmark());
            et_mobilenumber.setText(saveAddress.getPhone());
            et_pincode.setText(saveAddress.getPincode());
            tv_address.setText("Update Address");
            tv_title.setText("Update Address");
            isupdate=1;
        }catch (NullPointerException e ){

        }

        if (NetworkUtil.isNetworkConnected(context)) {
            statelist.clear();
            // getStates();
            getStates();
            //  getCategorys();
        } else {
            Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
        }

        if (whichposition==1){
            autocompleteSupportFragment.getView().setVisibility(View.GONE);
          //  layout_editcurrentadd.setVisibility(View.VISIBLE);
            getLocation();
        }else if (whichposition==0){
            try {
                st_current_address=getIntent().getStringExtra("address");
                latitude=Double.parseDouble(getIntent().getStringExtra("latitude"));
                longitude=Double.parseDouble(getIntent().getStringExtra("longitude"));
                et_currentaddress.setText(st_current_address);
                autocompleteSupportFragment.getView().setVisibility(View.GONE);
            }catch (NullPointerException e){

            }

           // layout_editcurrentadd.setVisibility(View.GONE);
        }else{

        }
        current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });

        autocompleteSupportFragment.setHint("Search Address");

        autocompleteSupportFragment.getView().setBackground(getResources().getDrawable(R.drawable.background_blue_line));
        placesClient = Places.createClient(PlacesAddActivity.this);



        autocompleteSupportFragment.setPlaceFields(Arrays.asList(Place.Field.ID,Place.Field.LAT_LNG,Place.Field.NAME));

        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                final LatLng latLng = place.getLatLng();

                /*Toast.makeText(PlacesActivity.this, place.getName(), Toast.LENGTH_SHORT).show();*/
                Log.d("Lat",place.getLatLng()+"");
                Log.d("Name",place.getName()+"");
                Log.d("id",place.getId()+"");
                Log.d("address",place.getAddress()+"");
                String latLang = place.getLatLng()+"".trim();
                String [] sep = latLang.split(":");
                String [] coordinates = sep[1].split(",");
                Log.d("lat", String.valueOf(coordinates[0]).substring(2,coordinates[0].length()));
                String lat =String.valueOf(coordinates[0]).substring(2,coordinates[0].length());
                Log.d("lang", String.valueOf(coordinates[1]).substring(0,coordinates[1].length()-1));
                String lang = String.valueOf(coordinates[1]).substring(0,coordinates[1].length()-1);

               // st_current_address=place.getAddress();
                autocompleteSupportFragment.setText(""+place.getAddress());
                latitude=place.getLatLng().latitude;
                longitude=place.getLatLng().longitude;

            }

            @Override
            public void onError(@NonNull Status status) {

            }
        });

        tv_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                st_current_address=et_currentaddress.getText().toString().trim();
                st_name=et_name.getText().toString().trim();
                st_mobilenumber=et_mobilenumber.getText().toString().trim();
                st_mail=et_email.getText().toString().trim();
                st_landmark=et_land_mark.getText().toString().trim();
                st_pincode=et_pincode.getText().toString().trim();

                if (isValid()) {
                    Map<String, String> uploadMap = new HashMap<>();

                    uploadMap.put("name", st_name);
                    uploadMap.put("mobile", st_mobilenumber);
                    uploadMap.put("email", st_mail);
                    uploadMap.put("landmark", st_landmark);
                    uploadMap.put("pincode", ""+st_pincode);
                    uploadMap.put("address", ""+st_current_address);
                    uploadMap.put("latitude", ""+latitude);
                    uploadMap.put("longitude", ""+longitude);
                    uploadMap.put("state_id", ""+stateid);
                    uploadMap.put("district_id", ""+districtid);
                    uploadMap.put("constituency_id", ""+constitueid);
                    uploadMap.put("geo_location", ""+st_current_address);

                    if (isupdate==1){
                        uploadMap.put("id", ""+saveAddress.getId());

                    }

                    JSONObject json = new JSONObject(uploadMap);

                    sendAddress(json);

                    String  data = json.toString();

                    System.out.println("aaaaaaa address  "+data);

                }
               /* if (st_name.isEmpty()){
                   // Toast.makeText(context, "Please enter Name", Toast.LENGTH_SHORT).show();
                    et_name.setError("Enter Name");
                }else {
                    if (st_mobilenumber.isEmpty()){
                        et_mobilenumber.setError("Enter Mobile Number");
                    }else {
                        if (st_current_address.isEmpty()){
                            et_currentaddress.setError("enter Address");
                        }else {
                            if (st_pincode.isEmpty()){
                                et_pincode.setError("enter pincode");
                            }else {
                                Map<String, String> uploadMap = new HashMap<>();
                                uploadMap.put("name", st_name);
                                uploadMap.put("mobile", st_mobilenumber);
                                uploadMap.put("email", st_mail);
                                uploadMap.put("landmark", st_landmark);
                                uploadMap.put("pincode", ""+st_pincode);
                                uploadMap.put("address", ""+st_current_address);
                                uploadMap.put("latitude", ""+latitude);
                                uploadMap.put("longitude", ""+longitude);

                                JSONObject json = new JSONObject(uploadMap);

                                String  data = json.toString();

                                System.out.println("aaaaaaa address  "+data);
                                Intent intent=new Intent();
                                intent.putExtra("addressname",st_current_address);
                               // intent.putExtra("id",saveAddress.getId());
                                setResult(101,intent);
                                finish();
                            }
                        }
                    }
                }*/
            }
        });
        image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (statelist.size()==0){

                }else {
                    if (position!=0){
                        if (NetworkUtil.isNetworkConnected(PlacesAddActivity.this)) {
                            spinner_distict.setEnabled(true);
                            spinner_distict.setClickable(true);
                            spinner_constitunesy.setEnabled(false);
                            spinner_constitunesy.setClickable(false);
                            stateid=statelist.get(position-1).getId();
                            districtlist.clear();
                            getDistricts(statelist.get(position-1).getId());
                            // getDistricts(statelist.get(position-1).getStateID());
                        } else {
                            Toast.makeText(PlacesAddActivity.this, "No internet connection! ", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        spinner_distict.setEnabled(false);
                        spinner_distict.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        spinner_constitunesy.setClickable(false);
                        ArrayList<String> selectlist=new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(PlacesAddActivity.this,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_distict.setAdapter(ad);
                        spinner_constitunesy.setAdapter(ad);
                        // Toast.makeText(PlacesAddActivity.this, "Please Select State", Toast.LENGTH_SHORT).show();

                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_distict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtlist.size()==0){
                    // Toast.makeText(PlacesAddActivity.this, "Please Select District", Toast.LENGTH_SHORT).show();
                }else {
                    if (position!=0){

                        if (NetworkUtil.isNetworkConnected(context)) {
                            spinner_distict.setEnabled(true);
                            spinner_distict.setClickable(true);
                            spinner_constitunesy.setClickable(true);
                            spinner_constitunesy.setEnabled(true);
                            districtid=districtlist.get(position-1).getId();
                            constiencieslist.clear();
                            // getConstituencis(districtlist.get(position-1).getDistrictID());
                            getConstituencis(stateid,districtlist.get(position-1).getId());
                        } else {
                            Toast.makeText(PlacesAddActivity.this, "No internet connection! ", Toast.LENGTH_SHORT).show();
                        }

                    }else {
                        spinner_constitunesy.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        ArrayList<String> selectlist=new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(PlacesAddActivity.this,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner_constitunesy.setAdapter(ad);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_constitunesy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtid.isEmpty()){
                  //  Toast.makeText(context, "Please select District", Toast.LENGTH_SHORT).show();
                }else {
                    if (position!=0){
                        constitueid=constiencieslist.get(position-1).getId();
                    }else{
                      //  Toast.makeText(context, "Please Select Constiency", Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        customDialog.show();
        customDialog.setMessage("Fetching Current Location \n Please wait");
        try {

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (LocationListener) this);
          //  customDialog.dismiss();

        } catch (SecurityException e) {
            e.printStackTrace();
            Log.d("Location Error", e.toString());
           // customDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            //Toast.makeText(mContext, arr.length+"", Toast.LENGTH_SHORT).show();


            //lat1 = location.getLatitude();
           //lang1 = location.getLongitude();

            //location_tv.setText(arr[3]);
            Log.d("getLocality()",addresses.get(0).getLocality()+"");
            Log.d("getAddressLine()",addresses.get(0).getAddressLine(0)+"");
            Log.d("getAdminArea()",addresses.get(0).getAdminArea()+"");
            Log.d("getSubAdminArea()",addresses.get(0).getSubAdminArea()+"");
            Log.d("getSubLocality()",addresses.get(0).getSubLocality()+"");
            Log.d("getFeatureName()",addresses.get(0).getFeatureName()+"");

         //   System.out.println("aaaaaaaaa getLocality "+addresses.get(0).getLocality());
            System.out.println("aaaaaaaaa getAddressLine "+addresses.get(0).getAddressLine(0));
         //   System.out.println("aaaaaaaaa getAddressLine1 "+addresses.get(0).getAddressLine(1));
          //  System.out.println("aaaaaaaaa getAdminArea "+addresses.get(0).getAdminArea());
          //  System.out.println("aaaaaaaaa getSubAdminArea "+addresses.get(0).getSubAdminArea());
          //  System.out.println("aaaaaaaaa getSubLocality "+addresses.get(0).getSubLocality());
          //  System.out.println("aaaaaaaaa getFeatureName "+addresses.get(0).getFeatureName());


           // autocompleteSupportFragment.setText(""+addresses.get(0).getAddressLine(0));

            customDialog.dismiss();

            latitude=location.getLatitude();
            longitude=location.getLongitude();

            st_current_address=addresses.get(0).getAddressLine(0);

            et_currentaddress.setText(""+st_current_address);

            et_currentaddress.setEnabled(false);
            et_currentaddress.setClickable(false);

           /* AppCompatEditText originEditText = (AppCompatEditText) autocompleteSupportFragment.getView().findViewById(R.id.autocomplete_fragment);
            originEditText.setEnabled(false);
            originEditText.setClickable(false);

            originEditText.setText(""+addresses.get(0).getAddressLine(0));*/

        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private boolean isValid() {
        boolean valid = true;

        if (et_name.getText().toString().trim().isEmpty()) {
            et_name.setError(EMPTY);
            valid = false;
        }  else if (et_mobilenumber.getText().toString().trim().isEmpty()) {
            et_mobilenumber.setError(EMPTY);
            valid = false;
        } else if (et_mobilenumber.getText().toString().trim().length() <10) {
            et_mobilenumber.setError(INVALID_PHONE);
            valid = false;
        }else if (!isValidPhoneNumber(et_mobilenumber.getText().toString().trim())){
            et_mobilenumber.setError( "Invalid Mobile Number");
            valid = false;
        }
        else if (et_currentaddress.getText().toString().trim().isEmpty()) {
            et_currentaddress.setError(EMPTY);
            valid = false;
        } else if (constitueid.isEmpty()) {
            Toast.makeText(context, "Please select constiencie", Toast.LENGTH_SHORT).show();
            valid = false;
        }else if (et_pincode.getText().toString().trim().isEmpty()) {
            et_pincode.setError(EMPTY);
            valid = false;
        }else if (et_pincode.getText().toString().trim().length() <6) {
            et_pincode.setError("Invalid Pincode");
            valid = false;
        }
        return valid;

    }
    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    private void getStates() {
        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_States,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    States states=new States();
                                    states.setId(jsonObject1.getString("id"));
                                    states.setName(jsonObject1.getString("name"));
                                    states.setCreated_user_id(jsonObject1.getString("created_user_id"));
                                    states.setUpdated_user_id(jsonObject1.getString("updated_user_id"));
                                    states.setCreated_at(jsonObject1.getString("created_at"));
                                    states.setUpdated_at(jsonObject1.getString("updated_at"));
                                    states.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    states.setStatus(jsonObject1.getString("status"));

                                    statelist.add(states);
                                }
                                ArrayList<String> statenames=new ArrayList<String>();
                                statenames.add("Select");
                                for (int k=0;k<statelist.size();k++){
                                    statenames.add(statelist.get(k).getName());
                                    System.out.println("aaaaaa state "+statelist.get(k).getId()+"  "+statelist.get(k).getName());
                                }

                                ArrayAdapter ad = new ArrayAdapter(PlacesAddActivity.this,
                                        android.R.layout.simple_spinner_item, statenames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_state.setAdapter(ad);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(PlacesAddActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(PlacesAddActivity.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getDistricts(String stateID) {
        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_States+stateID,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("districts");
                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);
                                    Districts districts=new Districts();
                                    districts.setId(jsonObject.getString("id"));
                                    districts.setName(jsonObject.getString("name"));
                                    districts.setState_id(jsonObject.getString("state_id"));
                                    districtlist.add(districts);
                                }
                                ArrayList<String> districtnames=new ArrayList<String>();
                                districtnames.add("Select");
                                System.out.println("aaaaaaa districtlist size "+districtlist.size());
                                for (int k=0;k<districtlist.size();k++){
                                    districtnames.add(districtlist.get(k).getName());
                                }
                                ArrayAdapter ad = new ArrayAdapter(context,
                                        android.R.layout.simple_spinner_item, districtnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_distict.setAdapter(ad);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(context, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(PlacesAddActivity.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getConstituencis(String stateID,String districtid) {
        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_States+stateID+"/"+districtid,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        System.out.println("aaaaaaa response const  "+response.toString());
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("constituenceis");

                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);

                                    Constiuencies constiuencies=new Constiuencies();
                                    constiuencies.setId(jsonObject.getString("id"));
                                    constiuencies.setDistrict_id(jsonObject.getString("district_id"));
                                    constiuencies.setName(jsonObject.getString("name"));

                                    constiencieslist.add(constiuencies);
                                }
                                ArrayList<String> constnames=new ArrayList<String>();
                                constnames.add("Select");
                                for (int k=0;k<constiencieslist.size();k++){
                                    constnames.add(constiencieslist.get(k).getName());
                                }
                                ArrayAdapter ad = new ArrayAdapter(context,
                                        android.R.layout.simple_spinner_item, constnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_constitunesy.setAdapter(ad);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(context, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(context, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void sendAddress(JSONObject dataObject) {
        final String dataStr = dataObject.toString();
       customDialog.show();
       customDialog.setMessage("Address Saving..");
       String url="";
       if (isupdate==1){
           url=ADDRESSUPDATE;
       }else {
           url=ADDRESSADD;
       }
        RequestQueue requestQueue = Volley.newRequestQueue(PlacesAddActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        if (response != null) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa jsonobject  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    String meassage=jsonObject.getString("message");
                                    SaveAddress saveAddress=new SaveAddress();

                                    saveAddress.setName(st_name);
                                    saveAddress.setPhone(st_mobilenumber);
                                    saveAddress.setEmail(st_mail);
                                    saveAddress.setLandmark(st_landmark);
                                    saveAddress.setPincode(st_pincode);
                                    saveAddress.setAddress(st_current_address);
                                    saveAddress.setLandmark(st_landmark);

                                    Toast.makeText(context, ""+meassage, Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent();
                                    intent.putExtra("addressname",saveAddress);
                                    // intent.putExtra("id",saveAddress.getId());
                                    setResult(101,intent);
                                    finish();
                                } else {

                                    System.out.println("aaaaaaa  222 ");
                                }


                            } catch (Exception e) {
                                System.out.println("aaaaaa catch  "+e.getMessage());
                                System.out.println("aaaaaaa  333 "+e.getMessage());
                                e.printStackTrace();

                            }

                        } else {
                            customDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                System.out.println("aaaaaaaa error "+error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}
