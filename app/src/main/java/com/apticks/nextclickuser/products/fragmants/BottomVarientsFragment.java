package com.apticks.nextclickuser.products.fragmants;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;

import android.text.Html;
import android.text.PrecomputedText;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.ProductDescriptionActivity;
import com.apticks.nextclickuser.products.adapters.VarientsAdapter;
import com.apticks.nextclickuser.products.adapters.ViewImagePagerAdapte;
import com.apticks.nextclickuser.products.model.ImagesModel;
import com.apticks.nextclickuser.products.model.ProductDetails;
import com.apticks.nextclickuser.products.model.ProductVarients;
import com.apticks.nextclickuser.products.model.ProductsModel;
import com.apticks.nextclickuser.products.model.VarientModel;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.XMLConstants;

import static com.apticks.nextclickuser.Config.Config.USERPRODUCTS;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


@SuppressLint("ValidFragment")
public class BottomVarientsFragment extends BottomSheetDialogFragment {

    private Context context;
    private RecyclerView varient_recycleview;
    private CustomDialog customDialog;
    String sub_cat_id,menuid,vendoruserid,brand_id,id,search="pass",minimum_price="";
    private TextView tv_productname,tv_price,tv_add;
    private ArrayList<VarientModel> varientlist;
    VarientModel varientModel;
    VarientsAdapter varientsAdapter;
    ProductsFragment productsFragment;
    int position;
    PreferenceManager preferenceManager;
    double totalamount;
    int whichactivity=0;
    ProductsModel productsModel;
    public BottomVarientsFragment(Context context, String sub_cat_id, String menuid, String vendoruserid, String s,
                                  ProductsModel productsModel, ProductsFragment productsFragment, int position) {
        this.context=context;
        this.sub_cat_id=sub_cat_id;
        this.menuid=menuid;
        this.vendoruserid=vendoruserid;
        this.brand_id=s;
        this.id=productsModel.getId();
        this.minimum_price=productsModel.getMin_price();
        this.productsFragment=productsFragment;
        this.productsModel=productsModel;
        this.position=position;
        customDialog=new CustomDialog(context);
        preferenceManager=new PreferenceManager(context);
    }

    public BottomVarientsFragment(Context mContext, ArrayList<VarientModel> varientlist,int whichactivity,String minimum_price) {
        this.context=mContext;
        this.whichactivity=whichactivity;
        this.varientlist=varientlist;
        this.minimum_price=minimum_price;
    }

    public BottomVarientsFragment(Context context, ProductsModel productsModel, int whichactivity, ProductsFragment productsFragment) {
        this.context=context;
        this.whichactivity=whichactivity;
        this.productsModel=productsModel;
        this.productsFragment=productsFragment;
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NonNull Dialog dialog, int style)
    {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.bottomvarientlayout, null);
        context = contentView.getContext();
        dialog.setContentView(contentView);
        //tv_title.setText(getString(R.string.app_name)); R.style.AppBottomSheetDialogTheme

        varient_recycleview=dialog.findViewById(R.id.varient_recycleview);
        tv_productname=dialog.findViewById(R.id.tv_productname);
        tv_price=dialog.findViewById(R.id.tv_price);
        tv_add=dialog.findViewById(R.id.tv_add);

        varient_recycleview.setLayoutManager(new GridLayoutManager(context,1));


        if (whichactivity==1){
            this.varientlist=varientlist;
           // varientModel=varientlist.get(0);

            for (int i=0;i<varientlist.size();i++){

                if (minimum_price.equalsIgnoreCase(varientlist.get(i).getPrice())){
                    varientModel=varientlist.get(i);
                    varientModel.setChecked(true);
                }else {
                    varientlist.get(i).setChecked(false);
                }
            }
            tv_productname.setText(""+varientModel.getSection_item_name());
            if (!varientModel.getDiscount().equalsIgnoreCase("0")){

                double totalprice =((Double.parseDouble(varientModel.getPrice()) / 100.0f) * Integer.parseInt(
                        varientModel.getDiscount()));
                // fooditemdisount.setText(""+varientobj.getString("discount")+" % off"+"(-"+String.format("%.2f",totalprice)+" )");
                // fooditemdisount.setText(""+varientobj.getString("discount")+" % off (Save "+totalprice+" )");
                totalamount=Double.parseDouble(varientModel.getPrice())-totalprice;
                System.out.println("aaaaaa total "+totalamount+"  "+totalprice);

                tv_price.setText("tem total ₹"+totalamount);
            }
            else {
                tv_price.setText("Item total ₹ "+varientModel.getPrice());
            }
        }else {
           // varientlist=new ArrayList<VarientModel>();
          //  productsFetcher();

           // this.varientlist=varientlist;
            for (int i=0;i<productsModel.getProductvarientlist().size();i++){
                System.out.println("aaaaaaaaaaa minprice  "+productsModel.getMin_price()+"   "+productsModel.getProductvarientlist().get(i).getPrice());
                if(productsModel.getMin_price().equalsIgnoreCase(productsModel.getProductvarientlist().get(i).getPrice())){
                    varientModel=productsModel.getProductvarientlist().get(i);
                    varientModel.setChecked(true);
                    break;
                }
            }

          varientlist=productsModel.getProductvarientlist();

            tv_productname.setText(""+productsModel.getName());
            if (!varientModel.getDiscount().equalsIgnoreCase("0")){

                double totalprice =((Double.parseDouble(varientModel.getPrice()) / 100.0f) * Integer.parseInt(
                        varientModel.getDiscount()));
                // fooditemdisount.setText(""+varientobj.getString("discount")+" % off"+"(-"+String.format("%.2f",totalprice)+" )");
                // fooditemdisount.setText(""+varientobj.getString("discount")+" % off (Save "+totalprice+" )");
                totalamount=Double.parseDouble(varientModel.getPrice())-totalprice;
                System.out.println("aaaaaa total "+totalamount+"  "+totalprice);

                tv_price.setText("item total ₹"+totalamount);
            }
            else {
                tv_price.setText("Item total ₹ "+varientModel.getPrice());
            }
        }

        varientsAdapter=new VarientsAdapter(context,varientlist,BottomVarientsFragment.this,2);
        varient_recycleview.setAdapter(varientsAdapter);


        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        int maxHeight = (int) (height*0.44); //custom height of bottom sheet

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        ((BottomSheetBehavior) behavior).setPeekHeight(maxHeight);  //changed default peek height of bottom sheet

        if (behavior != null && behavior instanceof BottomSheetBehavior)
        {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
            {

                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState)
                {
                    String state = "";
                    switch (newState)
                    {
                        case BottomSheetBehavior.STATE_DRAGGING: {
                            //imgBtnClose.setVisibility(View.INVISIBLE);
                            state = "DRAGGING";
                            break;
                        }
                        case BottomSheetBehavior.STATE_SETTLING: {
                            // imgBtnClose.setVisibility(View.INVISIBLE);
                            state = "SETTLING";
                            break;
                        }
                        case BottomSheetBehavior.STATE_EXPANDED: {
                            // imgBtnClose.setVisibility(View.VISIBLE);
                            state = "EXPANDED";
                            break;
                        }
                        case BottomSheetBehavior.STATE_COLLAPSED: {
                            //imgBtnClose.setVisibility(View.INVISIBLE);
                            state = "COLLAPSED";
                            break;
                        }
                        case BottomSheetBehavior.STATE_HIDDEN: {
                            // imgBtnClose.setVisibility(View.INVISIBLE);
                            dismiss();
                            state = "HIDDEN";
                            break;
                        }
                    }
                    Log.i("BottomSheetFrag", "onStateChanged: "+ state);
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                }
            });
        }

        tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (whichactivity==1){
                    ((ProductDescriptionActivity)context).addproduct(varientModel,1,totalamount);
                }else {
                    productsFragment.addProduct(varientModel);
                }

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

  /*  @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.bottomvarientlayout, container, false);
        varient_recycleview=view.findViewById(R.id.varient_recycleview);
        tv_productname=view.findViewById(R.id.tv_productname);

        varient_recycleview.setLayoutManager(new GridLayoutManager(context,1));

        varientlist=new ArrayList<VarientModel>();
         varientsAdapter=new VarientsAdapter(context,varientlist);
        varient_recycleview.setAdapter(varientsAdapter);
        productsFetcher();
        return view;

    }*/

    private void productsFetcher() {


        String url = "";

        url = USERPRODUCTS  + "?sub_cat_id=" +sub_cat_id +"&menu_id=" + menuid + "&brand_id=" + brand_id
                + "&q= "+ search +
                "&vendor_user_id="+vendoruserid+"&item_id="+id;
        // url = USERPRODUCTS + limit + "/" + offset + SUBCATID +sub_cat_id +MENUID + menuid + BRANDID + brandId  +SEARCH + search + VENDORUSERID+"29";

        Log.d("Url", url);
        System.out.println("aaaaaaaaaa  url "+url);

        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("brand vendors response", response);
                    customDialog.dismiss();
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                            ProductDetails productsModel=new ProductDetails();

                            productsModel.setId(jsonObject1.getString("id"));
                            productsModel.setProduct_code(jsonObject1.getString("product_code"));
                            productsModel.setName(jsonObject1.getString("name"));
                            productsModel.setDesc(jsonObject1.getString("desc"));
                            productsModel.setItem_type(jsonObject1.getString("item_type"));
                            productsModel.setAvailability(jsonObject1.getString("availability"));
                            productsModel.setMenu_id(jsonObject1.getString("menu_id"));
                            productsModel.setSub_cat_id(jsonObject1.getString("sub_cat_id"));
                            productsModel.setBrand_id(jsonObject1.getString("brand_id"));

                            tv_productname.setText(""+(jsonObject1.getString("name")));

                            JSONArray varientarray=jsonObject1.getJSONArray("vendor_product_varinats");

                            for (int j=0;j< varientarray.length();j++){
                                JSONObject varientobj=varientarray.getJSONObject(j) ;
                                VarientModel varientModel=new VarientModel();
                                varientModel.setId(varientobj.getString("id"));
                                varientModel.setItem_id(varientobj.getString("item_id"));
                                varientModel.setSection_id(varientobj.getString("section_id"));
                                varientModel.setSection_item_id(varientobj.getString("section_item_id"));
                                varientModel.setSku(varientobj.getString("sku"));
                                varientModel.setPrice(varientobj.getString("price"));
                                varientModel.setStock(varientobj.getString("stock"));
                                varientModel.setDiscount(varientobj.getString("discount"));
                                varientModel.setList_id(varientobj.getString("list_id"));
                                varientModel.setVendor_user_id(varientobj.getString("vendor_user_id"));
                                varientModel.setStatus(varientobj.getString("status"));
                                varientModel.setSection_item_name(varientobj.getString("section_item_name"));
                                varientModel.setWeight(varientobj.getString("weight"));
                                varientModel.setSection_item_code(varientobj.getString("section_item_code"));
                                varientModel.setDesc(varientobj.getString("desc"));
                              //  if (check==1){
                                   // System.out.println("aaaaaaaa "+varientobj.getString("price")+"  "+minimum_price+varientobj.getString("discount"));
                                    if (varientobj.getString("price").equalsIgnoreCase(""+minimum_price)){
                                        varientModel.setChecked(true);
                                    }

                                    /*if (!varientobj.getString("discount").equalsIgnoreCase("0")){
                                        System.out.println("aaaaaaa dsicount  "+varientobj.getString("discount")+" price "+varientobj.getString("price"));

                                        double totalprice =((Double.parseDouble(varientobj.getString("price")) / 100.0f) * Integer.parseInt(
                                                varientobj.getString("discount")));
                                       // fooditemdisount.setText(""+varientobj.getString("discount")+" % off"+"(-"+String.format("%.2f",totalprice)+" )");
                                        // fooditemdisount.setText(""+varientobj.getString("discount")+" % off (Save "+totalprice+" )");
                                        double total=Double.parseDouble(varientobj.getString("price"))-totalprice;
                                        System.out.println("aaaaaa total "+total+"  "+totalprice);
                                        tv_price.setText(""+total);
                                    }*/
                              /*  }else {
                                    varientModel.setChecked(false);
                                }*/

                                // productsModel.setVarientlist(varientModel);
                                varientlist.add(varientModel);

                            }
                            try {
                               // imageslist=new ArrayList<>();
                                JSONArray imagesArray=jsonObject1.getJSONArray("item_images");
                                for (int i=0;i<imagesArray.length();i++){
                                    JSONObject imgjsonobject=imagesArray.getJSONObject(i);
                                    String id=imgjsonobject.getString("id");
                                    // String image=jsonObject1.getString("image");
                                    // System.out.println("aaaaaaa image "+id+" "+imgjsonobject.getString("image"));
                                    ImagesModel imagesModel=new ImagesModel();
                                    imagesModel.setId(id);
                                    imagesModel.setImage(imgjsonobject.getString("image"));
                                   // imageslist.add(imagesModel);

                                }
                            }catch (Exception e){
                                System.out.println("aaaaaa catch img "+e.getMessage());
                            }

                            tv_productname.setText(""+jsonObject1.getString("name"));
                          //  tv_price.setText("Item total ₹ "+minimum_price);
                            //  cartprice.setText(""+varientlist.get(0).getPrice());
                          /*  if (varientlist.get(0).getStock().equalsIgnoreCase("0")){
                                tvAvailableQuantity.setText("Not Available");
                                totalquantity=0;
                            }else {
                                totalquantity=Integer.parseInt(varientlist.get(0).getStock());
                                tvAvailableQuantity.setText("Stock : "+varientlist.get(0).getStock());
                            }*/
                           /* if (check!=1) {
                                if (varientlist.get(0).getDiscount().equalsIgnoreCase("0")) {
                                    layout_discount.setVisibility(View.GONE);
                                } else {
                                    layout_discount.setVisibility(View.VISIBLE);
                                    tv_pdprice.setText(varientlist.get(0).getPrice());
                                    tv_pdprice.setPaintFlags(tv_pdprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                    double totalprice = ((Double.parseDouble(varientlist.get(0).getPrice()) / 100.0f) *
                                            Integer.parseInt(
                                                    varientlist.get(0).getDiscount()));
                                    fooditemdisount.setText(""+varientlist.get(0).getDiscount()+" % off"+"(-"+String.format("%.2f",totalprice)+" )");
                                    // fooditemdisount.setText("" + varientlist.get(0).getDiscount() + " % off (Save " + totalprice + " )");
                                    double total = Double.parseDouble(varientlist.get(0).getPrice()) - totalprice;
                                    cartprice.setText("" + total);

                                }
                            }*/

                          //  if (check!=1){
                                //varientlist.get(0).setChecked(true);
                          //  }
                            for (int i=0;i<varientlist.size();i++){
                                if (varientlist.get(i).getPrice().equalsIgnoreCase(""+minimum_price)){
                                  //  varientModel.setChecked(true);
                                    varientModel=varientlist.get(i);
                                }
                            }

                            if (!varientModel.getDiscount().equalsIgnoreCase("0")){

                                double totalprice =((Double.parseDouble(varientModel.getPrice()) / 100.0f) * Integer.parseInt(
                                        varientModel.getDiscount()));
                                // fooditemdisount.setText(""+varientobj.getString("discount")+" % off"+"(-"+String.format("%.2f",totalprice)+" )");
                                // fooditemdisount.setText(""+varientobj.getString("discount")+" % off (Save "+totalprice+" )");
                                totalamount=Double.parseDouble(varientModel.getPrice())-totalprice;
                                System.out.println("aaaaaa total "+totalamount+"  "+totalprice);

                                tv_price.setText("Item total ₹"+totalamount);
                            }
                            varientsAdapter.setrefresh(varientlist);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Currently this product not available", Toast.LENGTH_SHORT).show();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                      //  finish();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void setdd(int position,VarientModel varientModel) {
       this.varientModel=varientModel;
        for (int k=0;k<varientlist.size();k++){
            varientlist.get(k).setChecked(false);
        }
        varientlist.get(position).setChecked(true);

        if (!varientModel.getDiscount().equalsIgnoreCase("0")){

            double totalprice =((Double.parseDouble(varientModel.getPrice()) / 100.0f) * Integer.parseInt(
                    varientModel.getDiscount()));
           // fooditemdisount.setText(""+varientmodel.getDiscount()+" % off"+"(-"+String.format("%.2f",totalprice)+" Saved)");

            // fooditemdisount.setText(""+varientmodel.getDiscount()+" % off (Save "+totalprice+" )");
            totalamount=Double.parseDouble(varientModel.getPrice())-totalprice;
          //  cartprice.setText(""+total);

            tv_price.setText("Item total ₹ "+totalamount);

        }else {
            tv_price.setText("Item total ₹ "+varientModel.getPrice());
        }

        varientsAdapter.setrefresh(varientlist);
    }
}
