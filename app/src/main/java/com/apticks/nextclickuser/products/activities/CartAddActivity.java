package com.apticks.nextclickuser.products.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.VENDORS.VendorsActivity;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.products.adapters.CartAdapter;
import com.apticks.nextclickuser.products.adapters.WishlistAdapter;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.ProductCartModel;
import com.apticks.nextclickuser.products.model.ProductImages;
import com.apticks.nextclickuser.products.model.VarientDetails;
import com.apticks.nextclickuser.products.model.VendorModel;
import com.apticks.nextclickuser.products.model.VendorvarientProducts;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CARTCHANGESTATUS;
import static com.apticks.nextclickuser.Config.Config.CARTDELETE;
import static com.apticks.nextclickuser.Config.Config.CARTREAD;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class CartAddActivity extends AppCompatActivity {

    RecyclerView recycler_cart_items,recycler_wishlist_items;
    private TextView tv_checkout,tv_wishlist;
    private ImageView img_back;
    RelativeLayout layout_cart,layout_wishlist;
    private ArrayList<CartModel> cartlist;
    private ArrayList<CartModel> wishlist;
    private CartAdapter cartAdapter;
    private WishlistAdapter wishlistAdapter;
    private CustomDialog customDialog;
    private Context mContext;
    private RelativeLayout relative_empty_cart;
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_add);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        mContext=CartAddActivity.this;
        TextView tv_header= findViewById(R.id.tv_header);
        tv_header.setText("My Cart");
        tv_header.setAllCaps(true);
        FrameLayout cart_layout=findViewById(R.id.cart_layout);
        cart_layout.setVisibility(View.GONE);

        cart_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, VendorsActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Intent intent1=new Intent(CartAddActivity.this,VendorsActivity.class);
                    intent1.putExtra("checkposition",1);
                    startActivity(intent1);
                  //  startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(CartAddActivity.this).toBundle());
                } else {
                    startActivity(intent);
                }
            }
        });
        LinearLayout layout_continue_shopping = findViewById(R.id.layout_continue_shopping);
        LinearLayout layout_checkout=findViewById(R.id.layout_checkout);
        layout_continue_shopping.setVisibility(View.VISIBLE);
        layout_checkout.setVisibility(View.GONE);

        tv_wishlist= findViewById(R.id.tv_wishlist);
        recycler_cart_items = findViewById(R.id.recycler_cart_items);
        recycler_wishlist_items = findViewById(R.id.recycler_wishlist_items);
        layout_cart = findViewById(R.id.layout_cart);
        layout_wishlist = findViewById(R.id.layout_wishlist);
        img_back = findViewById(R.id.img_back);
        tv_checkout = findViewById(R.id.tv_checkout);
        relative_empty_cart = findViewById(R.id.relative_empty_cart);
        customDialog=new CustomDialog(CartAddActivity.this);
        cartlist=new ArrayList<>();
        wishlist=new ArrayList<>();
        preferenceManager=new PreferenceManager(mContext);

        System.out.println("aaaaaaaaa token  "+preferenceManager.getString(TOKEN_KEY));

        LinearLayoutManager layoutManager = new LinearLayoutManager(CartAddActivity.this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(CartAddActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);


        cartAdapter=new CartAdapter(CartAddActivity.this,cartlist,0);

        wishlistAdapter=new WishlistAdapter(CartAddActivity.this,wishlist,0);

        recycler_wishlist_items.setAdapter(wishlistAdapter);
        recycler_wishlist_items.setLayoutManager(linearLayoutManager);

        recycler_cart_items.setAdapter(cartAdapter);
        recycler_cart_items.setLayoutManager(layoutManager);



        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tv_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson=new Gson();
                String cartliststring = gson.toJson(cartlist);
                Intent intent=new Intent(CartAddActivity.this, CheckOutPageActivity.class);
                intent.putExtra("cartlist",cartliststring);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        tv_wishlist.setVisibility(View.GONE);
        cartlist.clear();
        wishlist.clear();
        cartFetcher();
    }
    /* public void saveitems(String id,String item_id,String qty,String price,String discount,String product_code,String desc){
        CartModel cartModel=new CartModel();
        cartModel.setId(id);
        cartModel.setItem_id(item_id);
        cartModel.setQty(qty);
        cartModel.setPrice(price);
        cartModel.setDiscount(discount);
        cartModel.setProduct_code(product_code);
        cartModel.setDesc(desc);
        cartModel.setName(item_id);

        cartlist.add(cartModel);

    }


    private List<VendorModel> buildItemList() {
        List<VendorModel> itemList = new ArrayList<>();
        for (int i=0; i<10; i++) {
            if (i==0){
                VendorModel item = new VendorModel("Vendor "+i, cartlist,true);
                itemList.add(item);
            }else {
                VendorModel item = new VendorModel("Vendor "+i, cartlist,false);
                itemList.add(item);
            }

        }
        return itemList;
    }*/

    private List<CartModel> buildSubItemList() {
        List<CartModel> subItemList = new ArrayList<>();
       /* for (int i=0; i<3; i++) {
            CartModel subItem = new CartModel("Product "+i, "Description "+i);
            subItemList.add(subItem);
        }*/
        return subItemList;
    }

    private void cartFetcher() {
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTREAD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    customDialog.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            Log.v("data",jsonArray.toString());
                            for (int i=0;i<jsonArray.length();i++){

                                JSONObject jsonObject1=jsonArray.getJSONObject(i);

                                if (!jsonObject1.getString("qty").equalsIgnoreCase("0")){
                                    CartModel cartModel=new CartModel();

                                    cartModel.setId(jsonObject1.getString("id"));
                                    cartModel.setVendor_user_id(jsonObject1.getString("vendor_user_id"));
                                    cartModel.setItem_id(jsonObject1.getString("item_id"));
                                    cartModel.setQty(jsonObject1.getString("qty"));
                                    cartModel.setVendor_product_varinat_id(jsonObject1.getString("vendor_product_variant_id"));
                                    cartModel.setStatus(jsonObject1.getString("status"));

                                    VendorModel vendorModel=new VendorModel();

                                    vendorModel.setId(jsonObject1.getJSONObject("vendor").getString("id"));
                                    vendorModel.setVendor_user_id(jsonObject1.getJSONObject("vendor").getString("vendor_user_id"));
                                    vendorModel.setName(jsonObject1.getJSONObject("vendor").getString("name"));
                                    vendorModel.setDesc(jsonObject1.getJSONObject("vendor").getString("desc"));

                                    cartModel.setVendorModel(vendorModel);

                                    ProductCartModel productCartModel=new ProductCartModel();
                                    productCartModel.setId(jsonObject1.getJSONObject("item").getString("id"));
                                    productCartModel.setName(jsonObject1.getJSONObject("item").getString("name"));
                                    productCartModel.setDesc(jsonObject1.getJSONObject("item").getString("desc"));

                                    cartModel.setProductCartModel(productCartModel);

                                    VendorvarientProducts vendorvarientProducts=new VendorvarientProducts();
                                    vendorvarientProducts.setId(jsonObject1.getJSONObject("vendor_product_variant").getString("id"));
                                    vendorvarientProducts.setSection_item_id(jsonObject1.getJSONObject("vendor_product_variant").getString("section_item_id"));
                                    vendorvarientProducts.setStock(jsonObject1.getJSONObject("vendor_product_variant").getString("stock"));
                                    vendorvarientProducts.setPrice(jsonObject1.getJSONObject("vendor_product_variant").getString("price"));
                                    vendorvarientProducts.setDiscount(jsonObject1.getJSONObject("vendor_product_variant").getString("discount"));
                                    vendorvarientProducts.setStatus(jsonObject1.getJSONObject("vendor_product_variant").getString("status"));

                                    VarientDetails varientDetails=new VarientDetails();

                                    varientDetails.setName(jsonObject1.getJSONObject("vendor_product_variant").getJSONObject("details").getString("name"));
                                    varientDetails.setWeight(jsonObject1.getJSONObject("vendor_product_variant").getJSONObject("details").getString("weight"));

                                    vendorvarientProducts.setVarientDetails(varientDetails);
                                    cartModel.setVendorvarientProducts(vendorvarientProducts);


                                    ProductImages productImages=new ProductImages();
                                    ArrayList<ProductImages> productimagelist=new ArrayList<>();
                                    try{
                                        JSONArray imagesarrsy=jsonObject1.getJSONArray("item_images");
                                        for (int k=0;k<imagesarrsy.length();k++){
                                            JSONObject imageobject=imagesarrsy.getJSONObject(k);
                                            productImages.setId(imageobject.getString("id"));
                                            productImages.setItem_id(imageobject.getString("item_id"));
                                            productImages.setExt(imageobject.getString("ext"));
                                            productImages.setImage(imageobject.getString("image"));
                                            productimagelist.add(productImages);
                                        }
                                    }catch (JSONException e){
                                        productImages.setImage("");
                                    }

                                    cartModel.setProductImages(productimagelist);


                                    if (cartModel.getStatus().equalsIgnoreCase("1")){
                                        cartlist.add(cartModel);
                                    }else {
                                        wishlist.add(cartModel);
                                    }
                                }
                            }


                            if (cartlist.size()!=0){
                                relative_empty_cart.setVisibility(View.GONE);
                                tv_checkout.setVisibility(View.VISIBLE);
                                recycler_cart_items.setVisibility(View.VISIBLE);
                                cartAdapter.setrefresh(cartlist);
                              //  recycler_cart_items.setAdapter(cartAdapter);
                            }else {
                                relative_empty_cart.setVisibility(View.VISIBLE);
                                tv_checkout.setVisibility(View.GONE);
                                recycler_cart_items.setVisibility(View.GONE);
                            }

                            if (wishlist.size()!=0){
                                recycler_wishlist_items.setVisibility(View.VISIBLE);
                                wishlistAdapter.setrefresh(wishlist);
                                tv_wishlist.setVisibility(View.VISIBLE);
                                //  recycler_cart_items.setAdapter(cartAdapter);
                            }else {
                              //  relative_empty_cart.setVisibility(View.VISIBLE);
                                recycler_wishlist_items.setVisibility(View.GONE);
                                tv_wishlist.setVisibility(View.GONE);
                            }

                        }else {
                            relative_empty_cart.setVisibility(View.VISIBLE);
                            tv_checkout.setVisibility(View.GONE);
                            recycler_cart_items.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        relative_empty_cart.setVisibility(View.VISIBLE);
                        tv_checkout.setVisibility(View.GONE);
                        recycler_cart_items.setVisibility(View.GONE);
                        e.printStackTrace();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                    }
                }else {
                    customDialog.dismiss();
                    relative_empty_cart.setVisibility(View.VISIBLE);
                    tv_checkout.setVisibility(View.GONE);
                    recycler_cart_items.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                relative_empty_cart.setVisibility(View.VISIBLE);
                tv_checkout.setVisibility(View.GONE);
                recycler_cart_items.setVisibility(View.GONE);
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void removeProduct(CartModel cartModel) {
            customDialog.show();

            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTDELETE+"/"+cartModel.getId(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    customDialog.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response " + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            System.out.println("aaaaaaaaaa  response  "+response.toString());
                            cartlist.remove(cartModel);
                            if (cartlist.size()==0){
                                relative_empty_cart.setVisibility(View.VISIBLE);
                                tv_checkout.setVisibility(View.GONE);
                                recycler_cart_items.setVisibility(View.GONE);
                            }else {
                                cartAdapter.setrefresh(cartlist);
                                relative_empty_cart.setVisibility(View.GONE);
                                tv_checkout.setVisibility(View.VISIBLE);
                                recycler_cart_items.setVisibility(View.VISIBLE);
                            }


                        }
                    }catch (JSONException e){

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    customDialog.cancel();
                }
            }){
                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                    return map;
                }
            };
            requestQueue.add(stringRequest);
    }

    public void saveLatter(CartModel cartModel,String status) {
        cartChangeestatus(cartModel,status);
    }

    public void cartChangeestatus(CartModel cartModel,String status) {
        customDialog.show();
        Map<String,String> countMap = new HashMap<>();
        countMap.put("cart_id", cartModel.getId());
        countMap.put("status", status);

        final String data = new JSONObject(countMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTCHANGESTATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("aaaaaaa response " + jsonObject.toString());
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status /*&& http_code == 200*/) {
                        cartlist.clear();
                        wishlist.clear();
                        cartFetcher();
                    }
                }catch (JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

        };
        requestQueue.add(stringRequest);


    }
}