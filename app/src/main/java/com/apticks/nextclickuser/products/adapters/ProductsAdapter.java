package com.apticks.nextclickuser.products.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.AddToCartActivity;
import com.apticks.nextclickuser.Activities.CartActivity;
import com.apticks.nextclickuser.Activities.HospitalityActivities.HospitalityMainActivity;
import com.apticks.nextclickuser.Activities.OnDemandsActivities.OnDemandsMainActivity;
import com.apticks.nextclickuser.Activities.PROFILE.ProfileActivity;
import com.apticks.nextclickuser.Activities.VENDORS.VendorsActivity;
import com.apticks.nextclickuser.Activities.VENDOR_SUB_CATEGORIES.VendorsSubCategoriesActivity;
import com.apticks.nextclickuser.Helpers.TimeStamp;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.Pojo.VENDORSPOJO.VendorsPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.interfaces.CloseCallBack;
import com.apticks.nextclickuser.products.activities.ProductDescriptionActivity;
import com.apticks.nextclickuser.products.activities.ProductListActivity;
import com.apticks.nextclickuser.products.fragmants.BottomVarientsFragment;
import com.apticks.nextclickuser.products.fragmants.ProductsFragment;
import com.apticks.nextclickuser.products.model.ProductsModel;
import com.apticks.nextclickuser.products.model.VarientModel;
import com.apticks.nextclickuser.utilities.BlurImageView;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mapbox.mapboxsdk.Mapbox;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;
import static com.apticks.nextclickuser.Config.Config.LEAD_GENERATE_ARRAY;
import static com.apticks.nextclickuser.Config.Config.MAP_BOX_ACCESS_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.ADDRESS;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.SUCCESS;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;
import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isNull;
import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isnullboolean;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder>   {

    ArrayList<ProductsModel> productlist;
    String weight;
    Context context;
    private String sub_cat_id,brandId,vendoruserid,menuid,shopaddress,restarentname;
    BottomVarientsFragment bottomVarientsFragment;
    ProductsFragment productsFragment;
    VarientModel varientModel;
    public ProductsAdapter(Context context, ArrayList<ProductsModel> productlist,
                           String sub_cat_id, String brandId, String vendoruserid, String menuid, String shopaddress,
                           String restarentname, ProductsFragment productsFragment,String weight) {
        this.context=context;
        this.productlist=productlist;
        this.sub_cat_id=sub_cat_id;
        this.brandId=brandId;
        this.vendoruserid=vendoruserid;
        this.menuid=menuid;
        this.shopaddress=shopaddress;
        this.restarentname=restarentname;
        this.productsFragment=productsFragment;
        this.weight=weight;

    }


    @NonNull
    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.product_list_two, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new ProductsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsAdapter.ViewHolder holder, int position) {
        ProductsModel productsModel = productlist.get(position);

        if (productsModel.getQuantity().equalsIgnoreCase("0")){
            holder.tv_add.setVisibility(View.VISIBLE);
            holder.layout_plusminus.setVisibility(View.GONE);
        }else {
            holder.tv_add.setVisibility(View.GONE);
            holder.layout_plusminus.setVisibility(View.VISIBLE);
          //  System.out.println("aaaaaaaaa  quantti  "+productsModel.getQuantity());
            holder.tv_quantity.setText(""+productsModel.getQuantity());
        }

            for (int i=0;i<productsModel.getProductvarientlist().size();i++){
                if (productsModel.getMin_price().equalsIgnoreCase(productsModel.getProductvarientlist().get(i).getPrice())){
                    holder.tv_weight.setText(isNull(productsModel.getProductvarientlist().get(i).getSection_item_name()));
                    break;
                }
            }



      /*  String[] splited = productsModel.getName().split(" ");
        System.out.println("aaaaaaaaaa name spilt "+productsModel.getName());
        String name;
        if (splited.length<=2){
            if (splited.length<=1){
                name=splited[0];
            }else {
                name=splited[1];
            }

            System.out.println("aaaaaaaa spilt 1 "+splited[splited.length-1]+" "+splited[splited.length-2]);
        }else {
            name=splited[splited.length-2]+" "+splited[splited.length-1];
            System.out.println("aaaaaaaa spilt else  "+splited[splited.length-2]+" "+splited[splited.length-1]);
        }

        holder.tv_textname.setText(name);*/
      //  holder.tv_textname.setRotation(90);

        String output = productsModel.getName().substring(0, 1).toUpperCase() + productsModel.getName().substring(1);
        holder.fooditemname.setText(output);

        Spanned description=Html.fromHtml(productsModel.getDesc());

        String output1 = description.toString().substring(0, 1).toUpperCase() +description.toString().substring(1);

        System.out.println("aaaaaaaaaa bold  "+output1);
      //  holder.fooditemname.setText(productsModel.getName());
      //  Spanned description=Html.fromHtml(output1);
        holder.fooditemdesc.setText(isNull(""+output1));

        holder.tv_pdprice.setText(isNull(productsModel.getMin_price()));
        holder.tv_pprice.setText(isNull(productsModel.getMin_price()));

        /*String weight="";
        for (int k=0;k<productsModel.getProductvarientlist().size();k++){
            if (k==0){
                weight=isNull(productsModel.getProductvarientlist().get(k).getWeight());
            }else {
                weight=weight+" | "+isNull(productsModel.getProductvarientlist().get(k).getWeight());
            }
        }*/

      //  holder.tv_weight.setText(isNull(weight));

        try{
            if (isnullboolean(productsModel.getAvg_discount())){
                if (!productsModel.getAvg_discount().equalsIgnoreCase("0")){
                    holder.layout_discount.setVisibility(View.VISIBLE);
                    holder.tv_pprice.setPaintFlags(holder.tv_pprice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
                    double totalprice =((Double.parseDouble(productsModel.getMin_price()) / 100.0f) * Double.parseDouble(
                            productsModel.getAvg_discount()));
                    // holder.tv_pdiscount.setText(""+productsModel.getAvg_discount()+" % off /*(Save "+totalprice+" )*/");
                    holder.tv_pdiscount.setText(""+productsModel.getAvg_discount()+" % off");
                    double total=Double.parseDouble(productsModel.getMin_price())-totalprice;
                    holder.tv_pdprice.setText(""+total);
                }else {
                    holder.layout_discount.setVisibility(View.GONE);
                }
            }else {

                holder.layout_discount.setVisibility(View.GONE);

            }
        }catch (NumberFormatException e){

        }


        Glide.with(context)
                .load(productsModel.getImage())
                .placeholder(R.drawable.loader_gif)
                .into(holder.img_product);

        holder.card_item_adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, ProductDescriptionActivity.class);
                i.putExtra("sub_cat_id",sub_cat_id);
                i.putExtra("menu_id",menuid);
                i.putExtra("vendor_id",vendoruserid);
                i.putExtra("brand_id",brandId);
                i.putExtra("product_id",productsModel.getId());
                i.putExtra("minimum_price",productsModel.getMin_price());
                i.putExtra("quantity",Integer.parseInt(holder.tv_quantity.getText().toString()));
                i.putExtra(ADDRESS,shopaddress);
                i.putExtra(reastaurantName,restarentname);
                context.startActivity(i);
            }
        });

        holder.tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*bottomVarientsFragment = new BottomVarientsFragment(context, sub_cat_id, menuid, vendoruserid, "" + brandId,
                        productsModel,productsFragment,position);*/

                if (productsModel.getProductvarientlist().size()==1){
                    productsFragment.addProduct(productsModel.getProductvarientlist().get(0));
                }else {
                    bottomVarientsFragment = new BottomVarientsFragment(context, productlist.get(position),2,productsFragment);
                    bottomVarientsFragment.show(((ProductListActivity)context).getSupportFragmentManager(), bottomVarientsFragment.getTag());
                }
            }
        });

        holder.tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productsModel.getProductvarientlist().size()==1){
                    productsFragment.ProductUpdate(productsModel.getProductvarientlist().get(0),holder.tv_quantity.getText().toString());
                }else {
                    productsFragment.setminusclick(holder.tv_quantity.getText().toString(),productlist.get(position));
                }

               /* if (holder.tv_quantity.getText().toString().equalsIgnoreCase("1")){
                    holder.layout_plusminus.setVisibility(View.GONE);
                    holder.tv_add.setVisibility(View.VISIBLE);
                    productsFragment.setdelete(productlist.get(position));
                }else {
                    int qty=Integer.parseInt(holder.tv_quantity.getText().toString())+1;
                    try{
                        productsFragment.setupdate(productlist.get(position),""+qty,varientModel,holder.tv_pdprice.getText().toString());
                    }catch (NullPointerException e){

                    }

                }*/

            }
        });holder.tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productsModel.getProductvarientlist().size()==1){
                    productsFragment.addProduct(productsModel.getProductvarientlist().get(0));
                }else {
                    bottomVarientsFragment = new BottomVarientsFragment(context, productlist.get(position),2,productsFragment);
                    bottomVarientsFragment.show(((ProductListActivity)context).getSupportFragmentManager(), bottomVarientsFragment.getTag());
                }
               /* bottomVarientsFragment = new BottomVarientsFragment(context, sub_cat_id, menuid, vendoruserid, "" + brandId,
                        productsModel,productsFragment,position);
                bottomVarientsFragment.show(((ProductListActivity)context).getSupportFragmentManager(), bottomVarientsFragment.getTag());
*/
            }
        });

    }

    boolean contains(ServicesPojo list, String name) {
      //  for (ServicesPojo item : list) {
            if (list.getId().equals(name)) {
                return true;
            }
     //   }
        return false;
    }
    public void setrefresh(ArrayList<ProductsModel> productlist,String weight){
        this.productlist=productlist;
        this.weight=weight;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return productlist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}

    public void setrefreshvarient(ArrayList<ProductsModel> productlist, VarientModel varientModel) {
        this.productlist=productlist;
        this.varientModel=varientModel;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_product;
        private CardView card_item_adapter;
        private TextView tv_textname,discountfooditem,fooditemname,fooditemdesc,tv_pprice,tv_pdiscount,tv_pdprice,tv_weight,tv_minus,tv_quantity,tv_plus;
        private LinearLayout layout_discount,layout_plusminus,tv_add;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_product=itemView.findViewById(R.id.img_product);
            discountfooditem=itemView.findViewById(R.id.discountfooditem);
            fooditemname=itemView.findViewById(R.id.fooditemname);
            fooditemdesc=itemView.findViewById(R.id.fooditemdesc);
            tv_pprice=itemView.findViewById(R.id.tv_pprice);
            tv_pdiscount=itemView.findViewById(R.id.tv_pdiscount);
            card_item_adapter=itemView.findViewById(R.id.card_item_adapter);
            tv_pdprice=itemView.findViewById(R.id.tv_pdprice);
            layout_discount=itemView.findViewById(R.id.layout_discount);
            tv_weight=itemView.findViewById(R.id.tv_weight);
            tv_add=itemView.findViewById(R.id.tv_add);
            layout_plusminus=itemView.findViewById(R.id.layout_plusminus);
            tv_minus=itemView.findViewById(R.id.tv_minus);
            tv_quantity=itemView.findViewById(R.id.tv_quantity);
            tv_plus=itemView.findViewById(R.id.tv_plus);
            tv_textname=itemView.findViewById(R.id.tv_textname);


        }
    }
}
