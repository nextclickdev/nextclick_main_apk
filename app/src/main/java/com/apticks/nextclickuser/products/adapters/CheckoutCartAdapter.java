package com.apticks.nextclickuser.products.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Fragments.CartEcomerce;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.models.Products;
import com.apticks.nextclickuser.dialogs.RemoveProduct;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.apticks.nextclickuser.products.activities.ProductDescriptionActivity;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CARTUPDATE;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;
import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isNull;

public class CheckoutCartAdapter extends RecyclerView.Adapter<CheckoutCartAdapter.ViewHolder> {


    Context context;
    CustomDialog customDialog;
    PreferenceManager preferenceManager;
    RemoveProduct removeProduct;
    CartEcomerce cartEcomerce;
    int whichclass;
    ArrayList<Products> productslist;
    ArrayList<CartModel> cartlist;
    public CheckoutCartAdapter(Context mContext, ArrayList<Products> productslist, ArrayList<CartModel> cartlist) {
        this.context=mContext;
        this.productslist=productslist;
        this.cartlist=cartlist;
        customDialog=new CustomDialog(context);
        preferenceManager=new PreferenceManager(context);
    }

    @NonNull
    @Override
    public CheckoutCartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.cart_adpter_layout, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new CheckoutCartAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckoutCartAdapter.ViewHolder holder, int position) {
        CartModel cartModel = cartlist.get(position);

        String output = cartModel.getProductCartModel().getName().substring(0, 1).toUpperCase() + cartModel.getProductCartModel().getName().substring(1);
        holder.tv_pname.setText(output);

      //  holder.tv_pname.setText(cartModel.getProductCartModel().getName());
        holder.tv_quantity.setText(cartModel.getQty());
        holder.tv_stock.setText("Stock : "+cartModel.getVendorvarientProducts().getStock());
        holder.tv_pdescription.setText(isNull(cartModel.getVendorvarientProducts().getVarientDetails().getName()));
        holder.tv_pdprice.setText(cartModel.getVendorvarientProducts().getPrice());
        holder.tv_pdiscount.setText(cartModel.getVendorvarientProducts().getDiscount()+" % Off");
        holder.tv_pprice.setText(cartModel.getVendorvarientProducts().getPrice());

        try{
            Glide.with(context)
                    .load(cartModel.getProductImages().get(0).getImage())
                    .placeholder(R.drawable.loader_gif)
                    .into(holder.img_product);
        }catch (IndexOutOfBoundsException e){

        }



        if (cartModel.getVendorvarientProducts().getDiscount().equalsIgnoreCase("0")){
            holder.layout_discount.setVisibility(View.GONE);

        }else {
            holder.layout_discount.setVisibility(View.VISIBLE);
            holder.tv_pprice.setPaintFlags(holder.tv_pdprice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);

            double discount=Double.parseDouble(cartModel.getVendorvarientProducts().getDiscount());
            double price=Double.parseDouble(cartModel.getVendorvarientProducts().getPrice());

            double totalprice =price- ((price / 100.0f) * discount);

            holder.tv_pdprice.setText(""+totalprice);

        }

        holder.tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int stock=Integer.parseInt(cartModel.getVendorvarientProducts().getStock());
                int  qty=Integer.parseInt(holder.tv_quantity.getText().toString().trim());
                holder.tv_plus.setClickable(true);
                holder.tv_plus.setEnabled(true);

                if (qty==1){
                   // holder.tv_minus.setEnabled(false);
                  //  holder.tv_minus.setClickable(false);
                        removeProduct=new RemoveProduct(context,cartModel,1);
                        removeProduct.showDialog();

                }else {
                    holder.tv_quantity.setText(""+(qty-1));
                    ProductUpdate(cartModel,position, ""+(qty-1),0,holder.tv_plus,holder.tv_minus);
                }

            }
        });holder.tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int stock=Integer.parseInt(cartModel.getVendorvarientProducts().getStock());
                int  qty=Integer.parseInt(holder.tv_quantity.getText().toString().trim());
                ProductUpdate(cartModel,position, ""+(qty+1),1,holder.tv_plus,holder.tv_minus);
                if (qty<stock){
                   // holder.tv_quantity.setText(""+(qty+1));
                  //  holder.tv_minus.setEnabled(true);
                 //   holder.tv_minus.setClickable(true);
                  //  ProductUpdate(cartModel,position, ""+(qty+1),1,holder.tv_plus,holder.tv_minus);
                }else {
                    Toast.makeText(context, "Out Of Stock", Toast.LENGTH_SHORT).show();
                  //  holder.tv_plus.setClickable(false);
                  //  holder.tv_plus.setEnabled(false);
                }

            }
        }); holder.img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    removeProduct=new RemoveProduct(context,cartModel,1);
                    removeProduct.showDialog();
            }
        });
        holder.tv_savelatter.setVisibility(View.GONE);
        holder.tv_savelatter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (whichclass==0){
                  //  ((CartAddActivity)context).saveLatter(cartModel,""+2);
                }else if (whichclass==1){
                  //  cartEcomerce.saveLatter(cartModel,""+2);
                }

            }
        });

        holder.card_item_adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, ProductDescriptionActivity.class);
                i.putExtra("vendor_id",cartModel.getVendor_user_id());
                i.putExtra("product_id",cartModel.getProductCartModel().getId());
                i.putExtra("quantity",Integer.parseInt(cartModel.getQty()));
                i.putExtra("minimum_price",cartModel.getVendorvarientProducts().getPrice());
                i.putExtra("varient_id",cartModel.getVendor_product_varinat_id());
                context.startActivity(i);
            }
        });


    }

    public void setrefresh(ArrayList<Products> productlist){
        this.productslist=productlist;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return productslist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_product,img_cancel;
        private TextView tv_pname,tv_pdescription,tv_pdprice,tv_pdiscount,tv_pprice,tv_minus,tv_quantity,tv_plus,tv_stock,tv_savelatter;
        protected LinearLayout layout_discount;
        CardView card_item_adapter;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_product=itemView.findViewById(R.id.img_product);
            tv_pname=itemView.findViewById(R.id.tv_pname);
            tv_pdescription=itemView.findViewById(R.id.tv_pdescription);
            layout_discount=itemView.findViewById(R.id.layout_discount);
            tv_pdprice=itemView.findViewById(R.id.tv_pdprice);
            tv_pdiscount=itemView.findViewById(R.id.tv_pdiscount);
            tv_pprice=itemView.findViewById(R.id.tv_pprice);
            tv_minus=itemView.findViewById(R.id.tv_minus);
            tv_quantity=itemView.findViewById(R.id.tv_quantity);
            img_cancel=itemView.findViewById(R.id.img_cancel);
            tv_plus=itemView.findViewById(R.id.tv_plus);
            tv_stock=itemView.findViewById(R.id.tv_stock);
            tv_savelatter=itemView.findViewById(R.id.tv_savelatter);
            card_item_adapter=itemView.findViewById(R.id.card_item_adapter);

        }
    }

    private void ProductUpdate(CartModel cartModel, int position,String qty,int whichclick,TextView tv_plus,TextView tv_minus ) {

        customDialog.show();
        Map<String,String> countMap = new HashMap<>();
        countMap.put("id", cartModel.getId());
        countMap.put("item_id",cartModel.getItem_id());
        countMap.put("vendor_product_variant_id",cartModel.getVendor_product_varinat_id());
        countMap.put("qty",""+qty);
        countMap.put("vendor_user_id",cartModel.getVendor_user_id());

        final String data = new JSONObject(countMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTUPDATE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customDialog.dismiss();
                System.out.println("aaaaaaaaaa  response  "+response.toString());
                if (whichclick==0){
                    tv_plus.setClickable(true);
                    tv_plus.setEnabled(true);
                    if (Integer.parseInt(qty)==0){
                        tv_minus.setEnabled(false);
                        tv_minus.setClickable(false);

                    }else {
                        cartModel.setQty(qty);
                    }
                }else {
                    int stock=Integer.parseInt(cartModel.getVendorvarientProducts().getStock());
                    int  quantity=Integer.parseInt(qty);
                    if (quantity<=stock){
                        tv_minus.setEnabled(true);
                        tv_minus.setClickable(true);
                        cartModel.setQty(qty);
                        System.out.println("aaaaaaa qty "+qty);
                    }else {
                        Toast.makeText(context, "Out Of Stock", Toast.LENGTH_SHORT).show();
                        tv_plus.setClickable(false);
                        tv_plus.setEnabled(false);
                    }
                }

                notifyItemChanged(position);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

        };
        requestQueue.add(stringRequest);

    }




}

