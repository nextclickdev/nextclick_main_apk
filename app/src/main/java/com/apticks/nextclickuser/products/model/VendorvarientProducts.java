package com.apticks.nextclickuser.products.model;

public class VendorvarientProducts {
    String  id,section_item_id,stock,price,discount,status;
    VarientDetails varientDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSection_item_id() {
        return section_item_id;
    }

    public void setSection_item_id(String section_item_id) {
        this.section_item_id = section_item_id;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public VarientDetails getVarientDetails() {
        return varientDetails;
    }

    public void setVarientDetails(VarientDetails varientDetails) {
        this.varientDetails = varientDetails;
    }
}
