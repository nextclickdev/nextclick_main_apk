package com.apticks.nextclickuser.products.fragmants;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Pojo.FoodMenuPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.SqliteDatabase.CartDataBaseHelper;
import com.apticks.nextclickuser.SqliteDatabase.CartDatabase;
import com.apticks.nextclickuser.SqliteDatabase.DataBaseHelper;
import com.apticks.nextclickuser.checkout.activities.CheckOutNewPageActivity;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.apticks.nextclickuser.products.activities.ProductListActivity;
import com.apticks.nextclickuser.products.adapters.ProductsAdapter;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.DatabaseProductsModel;
import com.apticks.nextclickuser.products.model.ProductCartModel;
import com.apticks.nextclickuser.products.model.ProductImages;
import com.apticks.nextclickuser.products.model.ProductVarients;
import com.apticks.nextclickuser.products.model.ProductsModel;
import com.apticks.nextclickuser.products.model.VarientDetails;
import com.apticks.nextclickuser.products.model.VarientModel;
import com.apticks.nextclickuser.products.model.VendorModel;
import com.apticks.nextclickuser.products.model.VendorvarientProducts;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CARTADD;
import static com.apticks.nextclickuser.Config.Config.CARTDELETE;
import static com.apticks.nextclickuser.Config.Config.CARTREAD;
import static com.apticks.nextclickuser.Config.Config.CARTUPDATE;
import static com.apticks.nextclickuser.Config.Config.USERPRODUCTS;
import static com.apticks.nextclickuser.Constants.Constants.AUTH_TOKEN;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class ProductsFragment extends Fragment {
    Context mContext;
    int position;
    FoodMenuPojo foodMenuPojo;
    private LinearLayout no_vendors_data;
    private RecyclerView productRecycler;
    View view;
    LinearLayoutManager layoutManager;
    private String sub_cat_id,brandId="",menuid,search="pass",vendoruserid,shopaddress,restarentname;
    private ArrayList<ProductsModel> productlist;
    private ProductsAdapter productsAdapter;
    int main_offset = 0, main_count = 10, remaining_count = 0, level_count = 1;
    private boolean isScrolling=false;
    int currentItems, totalItems, scrollOutItems;
    private ProgressBar vendors_progress;
    int vendorsRange = 0;
    CartDataBaseHelper cartDBHelper;
    ArrayList<DatabaseProductsModel> databaseproductlist;
    private LinearLayout layout_cartitems;
    private TextView tv_items,tv_price,tv_viewcart;
    private CustomDialog customDialog;
    private PreferenceManager preferenceManager;
    private ArrayList<CartModel> cartlist;
    ArrayList<VarientModel> productvarientlist;
    String st_weight,minimum_price;
    public ProductsFragment(Context mContext, int i, FoodMenuPojo foodMenuPojo, String sub_cat_id, String menuid, String brandId,
                            String vendoruserid,String shopaddress,String restarentname) {
        this.mContext=mContext;
        this.position=i;
        this.foodMenuPojo=foodMenuPojo;
        this.sub_cat_id=sub_cat_id;
        this.menuid=foodMenuPojo.getId();
        this.brandId=brandId;
        this.vendoruserid=vendoruserid;
        this.shopaddress=shopaddress;
        this.restarentname=restarentname;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view= inflater.inflate(R.layout.fragment_products, container, false);
         init();
        return view;
    }
    public void init(){
        productRecycler=view.findViewById(R.id.productRecycler);
        vendors_progress = view.findViewById(R.id.vendors_progress);
        no_vendors_data = view.findViewById(R.id.no_vendors_data);
        layout_cartitems = view.findViewById(R.id.layout_cartitems);
        tv_price = view.findViewById(R.id.tv_price);
        tv_items = view.findViewById(R.id.tv_items);
        tv_viewcart = view.findViewById(R.id.tv_viewcart);

        layoutManager = new LinearLayoutManager(mContext);
        cartDBHelper = new CartDataBaseHelper(mContext);
        customDialog=new CustomDialog(mContext);
        cartlist=new ArrayList<>();

        preferenceManager=new PreferenceManager(mContext);

        tv_viewcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson=new Gson();
                String cartliststring = gson.toJson(cartlist);
               // Intent intent=new Intent(mContext, CheckOutNewPageActivity.class);
                Intent intent=new Intent(mContext, CheckOutPageActivity.class);
                intent.putExtra("cartlist",cartliststring);
                startActivity(intent);
            }
        });
        databaseproductlist = cartDBHelper.getAllFoodItems();
        productlist=new ArrayList<>();

       // setcart();
       /* if (databaseproductlist.size()!=0){
            System.out.println("aaaaaaaa  size  "+databaseproductlist.size());
            double totalamount=0.0;
            for (int k=0;k<databaseproductlist.size();k++){
                totalamount=totalamount+Double.parseDouble(databaseproductlist.get(k).getProductPrice());
            }
            layout_cartitems.setVisibility(View.VISIBLE);
            tv_items.setText("Total items "+databaseproductlist.size());
            tv_price.setText(""+databaseproductlist.size());

        }else {
            layout_cartitems.setVisibility(View.GONE);
        }
*/

        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };

       // menuid="1";

     //   productsFetcher(main_count, main_offset);

      //  productRecycler.setLayoutManager(layoutManager);
        productRecycler.setLayoutManager(new GridLayoutManager(getContext(),1));
        productsAdapter = new ProductsAdapter( mContext, productlist,sub_cat_id,brandId,vendoruserid,menuid,shopaddress,
                restarentname,ProductsFragment.this,st_weight);
        productRecycler.setAdapter(productsAdapter);

        cartFetcher();

        productRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    remaining_count = remaining_count - (level_count * 10);
                    if (remaining_count > 0) {
                        main_offset = main_count + 1;
                        main_count = main_count + 10;
                        vendors_progress.setVisibility(View.VISIBLE);
                        productsFetcher(10, main_offset,0);
                        level_count++;

                    }
                }
            }
        });
    }

    private void productsFetcher(int limit, int offset,int check) {

        if (check==1){
            productlist.clear();
        }


        String url = "";

        url = USERPRODUCTS + limit + "/" + offset + "?sub_cat_id=" +sub_cat_id +"&menu_id=" + menuid + "&brand_id=" + brandId
        +"&vendor_user_id="+vendoruserid/*+ "&q="+ search*/ ;
        // url = USERPRODUCTS + limit + "/" + offset + SUBCATID +sub_cat_id +MENUID + menuid + BRANDID + brandId  +SEARCH + search + VENDORUSERID+"29";

        Log.d("Url", url);
        System.out.println("aaaaaaaaaa  url product "+url);


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response  product "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            if (!dataObject.get("result").getClass().toString().equalsIgnoreCase(str.getClass().toString())) {
                                JSONArray resultArray = dataObject.getJSONArray("result");
                                remaining_count = dataObject.getInt("count");
                                vendorsRange++;

                                for (int i=0;i<resultArray.length();i++){
                                    JSONObject jsonObject1=resultArray.getJSONObject(i);
                                    ProductsModel productsModel=new ProductsModel();

                                    productsModel.setId(jsonObject1.getString("id"));
                                    productsModel.setSub_cat_id(jsonObject1.getString("sub_cat_id"));
                                    productsModel.setMenu_id(jsonObject1.getString("menu_id"));
                                    productsModel.setBrand_id(jsonObject1.getString("brand_id"));
                                    productsModel.setProduct_code(jsonObject1.getString("product_code"));
                                    productsModel.setName(jsonObject1.getString("name"));
                                    productsModel.setDesc(jsonObject1.getString("desc"));
                                    productsModel.setItem_type(jsonObject1.getString("item_type"));
                                    productsModel.setMin_price(jsonObject1.getString("min_price"));
                                    productsModel.setMax_stock(jsonObject1.getString("max_stock"));
                                    productsModel.setAvg_discount(jsonObject1.getString("avg_discount"));
                                    productsModel.setVendor_user_id(jsonObject1.getString("vendor_user_id"));
                                    productsModel.setStatus(jsonObject1.getString("status"));
                                    productsModel.setImage_id(jsonObject1.getString("image_id"));
                                    productsModel.setExt(jsonObject1.getString("ext"));
                                    productsModel.setImage(jsonObject1.getString("image"));
                                    productsModel.setQuantity(jsonObject1.getString("cart_qty"));
                                    minimum_price=jsonObject1.getString("min_price");

                                    try {
                                        JSONArray  varientsaray = jsonObject1.getJSONArray("vendor_varinats");
                                        productvarientlist=new ArrayList<>();
                                        for (int k=0;k<varientsaray.length();k++){
                                            VarientModel productVarients =new VarientModel();
                                            JSONObject varientobj=varientsaray.getJSONObject(k);
                                            productVarients.setId(varientobj.getString("id"));
                                            productVarients.setItem_id(varientobj.getString("item_id"));
                                            productVarients.setSection_id(varientobj.getString("section_id"));
                                            productVarients.setSection_item_id(varientobj.getString("section_item_id"));
                                            productVarients.setSku(varientobj.getString("sku"));
                                            productVarients.setPrice(varientobj.getString("price"));
                                            productVarients.setStock(varientobj.getString("stock"));
                                            productVarients.setDiscount(varientobj.getString("discount"));
                                            productVarients.setList_id(varientobj.getString("list_id"));
                                            productVarients.setVendor_user_id(varientobj.getString("vendor_user_id"));
                                            productVarients.setStatus(varientobj.getString("status"));

                                            JSONObject itemobj=varientobj.getJSONObject("section_item");

                                            productVarients.setSection_item_name(itemobj.getString("name"));
                                            productVarients.setWeight(itemobj.getString("weight"));
                                            if (varientobj.getString("price").equalsIgnoreCase(""+jsonObject1.getString("min_price"))){
                                                productVarients.setChecked(true);
                                                st_weight=itemobj.getString("name");
                                            }
                                            productvarientlist.add(productVarients);
                                            }
                                        productsModel.setProductvarientlist(productvarientlist);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    productlist.add(productsModel);
                                    }
                                }
                                no_vendors_data.setVisibility(View.GONE);
                                productRecycler.setVisibility(View.VISIBLE);
                                productsAdapter.setrefresh(productlist,minimum_price);

                                vendors_progress.setVisibility(View.GONE);

                                //   productsAdapter.setrefresh(productlist);
                                //  productsAdapter.notifyDataSetChanged();
                            }else {
                                System.out.println("aaaaaaaa  else");
                            }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                        if (vendorsRange == 0) {
                            no_vendors_data.setVisibility(View.VISIBLE);
                            productRecycler.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void addProduct(VarientModel varientModel) {


        try{
            Map<String, String> uploadMap = new HashMap<>();
            uploadMap.put("item_id", varientModel.getItem_id());
            uploadMap.put("vendor_product_variant_id", varientModel.getId());
            uploadMap.put("qty", ""+1);
            uploadMap.put("vendor_user_id", varientModel.getVendor_user_id());

            JSONObject json = new JSONObject(uploadMap);
            String data=json.toString();
            if (!data.equalsIgnoreCase("1")){
                callAddtocart(data);
            }else {
                cartFetcher();
            }
            System.out.println("aaaaaaaaa data "+data);
            /*if (cartlist.size()!=0){
                for (int i=0;i<cartlist.size();i++){
                    if (!cartlist.get(i).getVendor_user_id().equalsIgnoreCase(varientModel.getVendor_user_id())){
                        int finalI = i;
                        new AlertDialog.Builder(mContext)
                                .setTitle("Replace cart item")
                                .setMessage("Your cart contains items have other vendor please remove and add ?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                       // for (int j=0;j<cartlist.size();j++){
                                            removeProduct("",data);
                                       // }
                                    }
                                })

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        break;
                    }else {
                        callAddtocart(data);
                        break;
                    }
                }
            }else {
                callAddtocart(data);
            }*/

        }catch (NullPointerException e){
            Toast.makeText(mContext, "Out Of Stock", Toast.LENGTH_SHORT).show();
        }

       /* if (databaseproductlist.size()!=0){

            for (int i=0;i<databaseproductlist.size();i++){
                if (databaseproductlist.get(i).getVendorvarientId().equalsIgnoreCase(productsModel.getVendor_user_id())){
                    if (databaseproductlist.get(i).getProductId().equalsIgnoreCase(productsModel.getId())){
                        if (cartDBHelper.updateFoodItem(productsModel,""+quantity,varientModel,""+totalamount)){
                            System.out.println("aaaaaaa databse update ");
                        }
                        break;
                    }else{
                        long id=cartDBHelper.insert(productsModel,varientModel.getId(),quantity,""+totalamount);
                        if (id!=-1){
                            Toast.makeText(mContext, "product Added", Toast.LENGTH_SHORT).show();
                        }else {
                            System.out.println("aaaaaaa databse error ");
                        }
                        break;
                    }
                }else {
                    new AlertDialog.Builder(mContext)
                            .setTitle("Replace cart item")
                            .setMessage("Your cart contains items have other vendor please remove and add ?")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Continue with delete operation
                                   int delete= cartDBHelper.deleteFoodItem(varientModel.getId());
                                    System.out.println("aaaaaa  delete "+delete+"  "+varientModel.getId());
                                    setcart();
                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }

            }
        }else {
            long id=cartDBHelper.insert(productsModel,varientModel.getId(),quantity,""+totalamount);
            if (id!=-1){
                Toast.makeText(mContext, "product Added", Toast.LENGTH_SHORT).show();
            }else {
                System.out.println("aaaaaaa databse error ");
            }
        }
        setcart();

        productlist.get(position).setQuantity(quantity);
        productsAdapter.setrefreshvarient(productlist,varientModel);*/

    }

    public void setdelete(ProductsModel productsModel) {
       // cartDBHelper.deleteFoodItem(productsModel.getId());

       // setcart();
    }
    public void setcart(){
        databaseproductlist = cartDBHelper.getAllFoodItems();
        System.out.println("aaaaaaaaa dbsize  "+databaseproductlist.size());
        if (databaseproductlist.size()!=0){
            double totalamount=0.0;
            for (int k=0;k<databaseproductlist.size();k++){
                totalamount=totalamount+Double.parseDouble(databaseproductlist.get(k).getProductPrice());
            }
            layout_cartitems.setVisibility(View.VISIBLE);
            tv_items.setText("Total items "+databaseproductlist.size());
            tv_price.setText("₹ "+String.format("%.2f",totalamount)+"");

          //  tv_price.setText(""+totalamount);
        }else {
            layout_cartitems.setVisibility(View.GONE);
        }
    }

    public void setupdate(ProductsModel productsModel, String quantity, VarientModel varientModel, String totalamount) {
        if (cartDBHelper.updateFoodItem(productsModel,quantity,varientModel,totalamount)){
            System.out.println("aaaaaaa databse upated ");
        }
        setcart();
    }

    private void callAddtocart(final String data) {

        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTADD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            Log.d("cc_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa  jsonobject  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    /*if (check==0){
                                        Toast.makeText(mContext, "Product Added Successfully", Toast.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(mContext, "Product Updated Successfully", Toast.LENGTH_SHORT).show();
                                    }*/
                                    cartFetcher();
                                } else {
                                    System.out.println("aaaaaaa else  ");
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch  "+e.getMessage());
                                customDialog.cancel();
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaa error  "+error.getMessage());
                customDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void cartFetcher() {
        cartlist.clear();
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTREAD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    customDialog.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response cart "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            Log.v("data",jsonArray.toString());
                            for (int i=0;i<jsonArray.length();i++){


                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                if (!jsonObject1.getString("qty").equalsIgnoreCase("0")){
                                    CartModel cartModel=new CartModel();

                                    cartModel.setId(jsonObject1.getString("id"));
                                    cartModel.setVendor_user_id(jsonObject1.getString("vendor_user_id"));
                                    cartModel.setItem_id(jsonObject1.getString("item_id"));
                                    cartModel.setQty(jsonObject1.getString("qty"));
                                    cartModel.setVendor_product_varinat_id(jsonObject1.getString("vendor_product_variant_id"));
                                    cartModel.setStatus(jsonObject1.getString("status"));

                                    VendorModel vendorModel=new VendorModel();

                                    vendorModel.setId(jsonObject1.getJSONObject("vendor").getString("id"));
                                    vendorModel.setVendor_user_id(jsonObject1.getJSONObject("vendor").getString("vendor_user_id"));
                                    vendorModel.setName(jsonObject1.getJSONObject("vendor").getString("name"));
                                    vendorModel.setDesc(jsonObject1.getJSONObject("vendor").getString("desc"));

                                    cartModel.setVendorModel(vendorModel);

                                    ProductCartModel productCartModel=new ProductCartModel();
                                    productCartModel.setId(jsonObject1.getJSONObject("item").getString("id"));
                                    productCartModel.setName(jsonObject1.getJSONObject("item").getString("name"));
                                    productCartModel.setDesc(jsonObject1.getJSONObject("item").getString("desc"));

                                    cartModel.setProductCartModel(productCartModel);

                                    VendorvarientProducts vendorvarientProducts=new VendorvarientProducts();
                                    vendorvarientProducts.setId(jsonObject1.getJSONObject("vendor_product_variant").getString("id"));
                                    vendorvarientProducts.setSection_item_id(jsonObject1.getJSONObject("vendor_product_variant").getString("section_item_id"));
                                    vendorvarientProducts.setStock(jsonObject1.getJSONObject("vendor_product_variant").getString("stock"));
                                    vendorvarientProducts.setPrice(jsonObject1.getJSONObject("vendor_product_variant").getString("price"));
                                    vendorvarientProducts.setDiscount(jsonObject1.getJSONObject("vendor_product_variant").getString("discount"));
                                    vendorvarientProducts.setStatus(jsonObject1.getJSONObject("vendor_product_variant").getString("status"));

                                    VarientDetails varientDetails=new VarientDetails();

                                    varientDetails.setName(jsonObject1.getJSONObject("vendor_product_variant").getJSONObject("details").getString("name"));
                                    varientDetails.setWeight(jsonObject1.getJSONObject("vendor_product_variant").getJSONObject("details").getString("weight"));

                                    vendorvarientProducts.setVarientDetails(varientDetails);
                                    cartModel.setVendorvarientProducts(vendorvarientProducts);

                                    ProductImages productImages=new ProductImages();
                                    ArrayList<ProductImages> productimagelist=new ArrayList<>();
                                    try{
                                        JSONArray imagesarrsy=jsonObject1.getJSONArray("item_images");
                                        for (int k=0;k<imagesarrsy.length();k++){
                                            JSONObject imageobject=imagesarrsy.getJSONObject(k);
                                            productImages.setId(imageobject.getString("id"));
                                            productImages.setItem_id(imageobject.getString("item_id"));
                                            productImages.setExt(imageobject.getString("ext"));
                                            productImages.setImage(imageobject.getString("image"));
                                            productimagelist.add(productImages);
                                        }
                                    }catch (JSONException e){
                                        productImages.setImage("");
                                    }

                                    cartModel.setProductImages(productimagelist);


                                    if (cartModel.getStatus().equalsIgnoreCase("1")){
                                        cartlist.add(cartModel);
                                    }else {

                                    }
                                }




                            }
                            int qty=0;
                            double totalprice=0.0;
                            for (int i=0;i<cartlist.size();i++){
                                 qty=Integer.parseInt(cartlist.get(i).getQty())+qty;
                                if (!cartlist.get(i).getVendorvarientProducts().getDiscount().equalsIgnoreCase("0")){

                                    double discount=Double.parseDouble(cartlist.get(i).getVendorvarientProducts().getDiscount());
                                    double price=Double.parseDouble(cartlist.get(i).getVendorvarientProducts().getPrice());

                                    double totalprice1 =price- ((price / 100.0f) * discount);
                                    totalprice=totalprice+(totalprice1*Integer.parseInt(cartlist.get(i).getQty()));
                                  //  totalprice=totalprice+totalprice1;

                                }else {
                                    totalprice=totalprice+(Double.parseDouble(cartlist.get(i).getVendorvarientProducts().getPrice()) * Integer.parseInt(cartlist.get(i).getQty()));
                                }
                              //  System.out.println("aaaaaaaa qty  "+qty);
                            }

                            tv_items.setText("Total items "+qty);

                            if (cartlist.size()!=0){
                                layout_cartitems.setVisibility(View.VISIBLE);
                                tv_price.setText("₹ "+String.format("%.2f",totalprice)+"");

                              //  tv_price.setText(" ₹ "+totalprice);
                            }else {
                                layout_cartitems.setVisibility(View.GONE);
                            }


                        }else {
                            layout_cartitems.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        layout_cartitems.setVisibility(View.GONE);
                        e.printStackTrace();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                    }
                }else {
                    layout_cartitems.setVisibility(View.GONE);
                    customDialog.dismiss();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                layout_cartitems.setVisibility(View.GONE);
                customDialog.dismiss();
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
        productsFetcher(main_count,main_offset,1);
    }

    public void removeProduct(String id,String data) {
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        System.out.println("aaaaaaa  dlele  "+CARTDELETE+"/"+id);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTDELETE+"/"+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("aaaaaaa response " + jsonObject.toString());
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {

                        System.out.println("aaaaaaaaaa  response  "+jsonObject.toString());
                       // cartlist.remove(cartModel);
                        if (!data.equalsIgnoreCase("1")){
                            callAddtocart(data);
                        }else {
                            cartFetcher();
                        }

                       /* if (cartlist.size()==0){
                           layout_cartitems.setVisibility(View.GONE);
                            callAddtocart(data);
                        }else {
                            layout_cartitems.setVisibility(View.VISIBLE);
                        }*/


                    }
                }catch (JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }
        };
        requestQueue.add(stringRequest);
    }

    public void ProductUpdate(VarientModel varientmodel, String qty) {
        customDialog.show();
        String cartid="";
        for (int i=0;i<cartlist.size();i++){
            if (cartlist.get(i).getVendor_product_varinat_id().equalsIgnoreCase(varientmodel.getId())){
                cartid=cartlist.get(i).getId();
                break;
            }
        }
        Map<String,String> countMap = new HashMap<>();
        countMap.put("id", cartid);
        countMap.put("item_id",varientmodel.getItem_id());
        countMap.put("vendor_product_variant_id",varientmodel.getId());
        countMap.put("qty",""+(Integer.parseInt(qty)-1));
        countMap.put("vendor_user_id",varientmodel.getVendor_user_id());

        final String data = new JSONObject(countMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTUPDATE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customDialog.dismiss();
                try {
                    JSONObject jsonObject1=new JSONObject(response);
                    System.out.println("aaaaaaaaaa  response  "+jsonObject1.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


               cartFetcher();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

        };
        requestQueue.add(stringRequest);

    }

    public void setminusclick(String s, ProductsModel productsModel) {
        if (s.equalsIgnoreCase("1")){
          //  removeProduct();
            for (int i=0;i<productsModel.getProductvarientlist().size();i++){
                if (productsModel.getProductvarientlist().get(i).isChecked()){
                    ProductUpdate(productsModel.getProductvarientlist().get(i),"1");
                }
            }
        }else {
            new AlertDialog.Builder(mContext)
                    .setTitle("Remove item from cart")
                    .setMessage("This item has multiple customizations added.\n Proceed to cart to remove item")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Gson gson=new Gson();
                            String cartliststring = gson.toJson(cartlist);
                           // Intent intent=new Intent(mContext, CheckOutNewPageActivity.class);
                            Intent intent=new Intent(mContext, CheckOutPageActivity.class);
                            intent.putExtra("cartlist",cartliststring);
                            startActivity(intent);
                        }
                    })

                    // A null listener allows the button to dismiss the dialog and take no further action.
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }

    }
}