package com.apticks.nextclickuser.products.adapters;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.apticks.nextclickuser.Pojo.FoodMenuPojo;
import com.apticks.nextclickuser.products.fragmants.ProductsFragment;

import java.util.ArrayList;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class MenusPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    //private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3, R.string.tab_text_4};
    //private static final int[] TAB_TITLES = new int[]{1, 2, 3, 4};
    private static final int[] TAB_TITLES = new int[]{1, 2, 3, 4};
    public  ArrayList<FoodMenuPojo> foodMenuDataName = new ArrayList<>();
    private final Context mContext;
    private String sub_cat_id,menuid,brandId,vendoruserid,shopaddress,restarentname;

    private boolean doNotifyDataSetChangedOnce = false;

    public MenusPagerAdapter(Context mContext, FragmentManager fm, String sub_cat_id, String menuid, String brandId,
                             String vendoruserid, String shopaddress, String restarentname, ArrayList<FoodMenuPojo> foodMenuDataName) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.mContext = mContext;
        this.sub_cat_id = sub_cat_id;
        this.menuid = menuid;
        this.brandId = brandId;
        this.vendoruserid = vendoruserid;
        this.shopaddress = shopaddress;
        this.restarentname = restarentname;
        this.foodMenuDataName = foodMenuDataName;
       // System.out.println("aaaaaaaaaa menu create");
    }

    @Override
    public ProductsFragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if (doNotifyDataSetChangedOnce) {
            doNotifyDataSetChangedOnce = false;
            notifyDataSetChanged();
        }
        FoodMenuPojo foodMenuPojo = foodMenuDataName.get(position);
        //foodMenuPojo.getId();
        return new ProductsFragment(mContext,position + 1,foodMenuPojo,sub_cat_id,menuid,brandId,vendoruserid,shopaddress,restarentname);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        //return mContext.getResources().getString(TAB_TITLES[position]);
        //return String.valueOf(TAB_TITLES[position]);
        FoodMenuPojo foodMenuPojo = foodMenuDataName.get(position);
        return String.valueOf(foodMenuPojo.getName());
    }

    @Override
    public int getCount() {
        //return TAB_TITLES.length;
        return foodMenuDataName.size();
    }
}