package com.apticks.nextclickuser.products.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.ProductDescriptionActivity;
import com.apticks.nextclickuser.products.fragmants.BottomVarientsFragment;
import com.apticks.nextclickuser.products.model.VarientModel;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isnullboolean;

public class VarientsAdapter extends RecyclerView.Adapter<VarientsAdapter.ViewHolder>  {

    ArrayList<VarientModel> productlist;
    Context context;
    int which;
    BottomVarientsFragment bottomVarientsFragment;
    String quantityst;
    public VarientsAdapter(Context context, ArrayList<VarientModel> productdetailslist) {
        this.context=context;
        this.productlist=productdetailslist;

    }
    public VarientsAdapter(Context context, int which) {
        this.context=context;
        this.which=which;

    }

    public VarientsAdapter(Context context, ArrayList<VarientModel> varientlist, BottomVarientsFragment bottomVarientsFragment, int i) {
        this.context=context;
        this.which=i;
        this.productlist=varientlist;
        this.bottomVarientsFragment=bottomVarientsFragment;

    }


    @NonNull
    @Override
    public VarientsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.varientlayout, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new VarientsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VarientsAdapter.ViewHolder holder, int position) {
        if (which==1){

        }else {
            VarientModel productsModel = productlist.get(position);

            if (productsModel.isChecked()){
              //  holder.layout_change.setBackgroundColor(context.getResources().getColor(R.color.transparent_orange));
                holder.cardview_item.setBackground(context.getResources().getDrawable(R.drawable.orange_boarder_line));
                holder.rb_button.setChecked(true);
            }else {
                holder.cardview_item.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.rb_button.setChecked(false);
            }

            holder.item_price.setText(productsModel.getPrice());
            String name =productsModel.getSection_item_name().substring(0, 1).toUpperCase() + productsModel.getSection_item_name().substring(1);
            holder.item_name.setText(name);


            if (which==2){
                holder.item_stock.setText(" "+productsModel.getDiscount()+" % off");
            }else {
                holder.item_stock.setText("Stock  "+productsModel.getStock());
            }

            if (isnullboolean(productsModel.getWeight())){
                holder.item_weight.setVisibility(View.VISIBLE);
                holder.item_weight.setText("Weight  "+productsModel.getWeight());
            }else {
                holder.item_weight.setVisibility(View.GONE);
            }


            if (productsModel.getDiscount().equalsIgnoreCase("0")){
                holder.item_discount.setVisibility(View.GONE);
            }else {
                holder.item_discount.setText("Discount "+productsModel.getDiscount()+" %off");
            }

            holder.cardview_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ProductDescriptionActivity)context).setRefresh(position,productsModel);
                }
            });
            holder.rb_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (which==2){
                        bottomVarientsFragment.setdd(position,productlist.get(position));
                    }else {
                        ((ProductDescriptionActivity)context).setRefresh(position,productsModel);
                    }
                   // ((ProductDescriptionActivity)context).setRefresh(position,productsModel);
                }
            });
            holder.layout_change.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (which==2){
                        bottomVarientsFragment.setdd(position,productlist.get(position));
                    }else {
                        ((ProductDescriptionActivity)context).setRefresh(position,productsModel);
                    }
                }
            });
        }



    }
    boolean contains(ServicesPojo list, String name) {
      //  for (ServicesPojo item : list) {
            if (list.getId().equals(name)) {
                return true;
            }
     //   }
        return false;
    }
    public  void setrefresh(ArrayList<VarientModel> productlist){
        this.productlist=productlist;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return productlist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView item_name,item_price,item_stock,item_discount,item_weight;
        private CardView cardview_item;
        private RadioButton rb_button;
        private LinearLayout layout_change;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_price=itemView.findViewById(R.id.item_price);
            item_name=itemView.findViewById(R.id.item_name);
            item_stock=itemView.findViewById(R.id.item_stock);
            item_discount=itemView.findViewById(R.id.item_discount);
            cardview_item=itemView.findViewById(R.id.cardview_item);
            rb_button=itemView.findViewById(R.id.rb_button);
            layout_change=itemView.findViewById(R.id.layout_change);
            item_weight=itemView.findViewById(R.id.item_weight);

        }
    }
}
