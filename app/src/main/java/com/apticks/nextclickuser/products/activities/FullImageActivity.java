package com.apticks.nextclickuser.products.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.adapters.ViewImagePagerAdapte;
import com.apticks.nextclickuser.products.adapters.ViewPagerAdapter;
import com.apticks.nextclickuser.products.model.ImagesModel;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class FullImageActivity extends AppCompatActivity {

    ImageView  back;
    String image;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    ArrayList<ImagesModel> imagelist;
    private Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        getSupportActionBar().hide();
        viewPager = findViewById(R.id.viewPager);
        back = findViewById(R.id.back);

        gson=new Gson();
       // imagelist=new ArrayList<>();
      //  imagelist=gson.fromJson(getIntent().getStringExtra("imageliststring"), (Type) ImagesModel.class);

        String carListAsString = getIntent().getStringExtra("imageliststring");
        String selectimage = getIntent().getStringExtra("selectimage");

        Type type = new TypeToken<List<String>>(){}.getType();
       // imagelist = gson.fromJson(carListAsString, type);
        System.out.println("aaaaaaaaa  size  "+carListAsString);
        imagelist=new ArrayList<>();
      /*  try {
            JSONArray jsonArray=new JSONArray(carListAsString);
            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject=jsonArray.getJSONObject(i);
                ImagesModel imagesModel=new ImagesModel();
                imagesModel.setId(jsonObject.getString("id"));
                imagesModel.setImage(jsonObject.getString("image"));
                imagelist.add(imagesModel);

            }
          //  viewPagerAdapter = new ViewPagerAdapter(FullImageActivity.this,imagelist);
            viewPagerAdapter = new ViewPagerAdapter(FullImageActivity.this,selectimage);

            viewPager.setAdapter(viewPagerAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        viewPagerAdapter = new ViewPagerAdapter(FullImageActivity.this,selectimage);

        viewPager.setAdapter(viewPagerAdapter);

       /*
        String carListAsString = getIntent().getStringExtra("imageliststring");
        Type type = new TypeToken<List<String>>(){}.getType();
        imagelist = gson.fromJson(carListAsString, type);

        ArrayList<ImagesModel> myList = (ArrayList<ImagesModel>) getIntent().getSerializableExtra("imageliststring");

*/


      /*  Glide.with(this)
                .load(getIntent().getStringExtra("image"))
                .placeholder(R.color.codeGray)
                .into(myImage);*/

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0,0);
            }
        });
    }
}