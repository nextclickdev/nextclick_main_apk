package com.apticks.nextclickuser.products.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.products.adapters.CartAdapter;
import com.apticks.nextclickuser.products.model.CartModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.CARTREAD;

public class AddCartActivity extends AppCompatActivity {

    private ImageView img_back;
    private RecyclerView recycler_cart_items;
    private RelativeLayout relative_empty_cart;
    private TextView tv_checkout;
    private Context mContext;
    private CustomDialog customDialog;
    private ArrayList<CartModel> cartlist;
    private CartAdapter cartAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cart);
        getSupportActionBar().hide();
        mContext=AddCartActivity.this;
        img_back=findViewById(R.id.img_back);
        recycler_cart_items=findViewById(R.id.recycler_cart_items);
        relative_empty_cart=findViewById(R.id.relative_empty_cart);
        tv_checkout=findViewById(R.id.tv_checkout);

        customDialog=new CustomDialog(mContext);
        cartlist=new ArrayList<>();
        recycler_cart_items.setLayoutManager(new GridLayoutManager(AddCartActivity.this,1));

        saveitems("1","A","10","50","0","01","nice");
        saveitems("2","B","5","150","2","01","nice");
        saveitems("3","C","30","70","4","01","nice");
        saveitems("4","D","80","30","8","01","nice");
        saveitems("5","E","100","20","5","01","nice");
        saveitems("6","F","90","50","0","01","nice");
        saveitems("7","G","5","10","10","01","nice");

        relative_empty_cart.setVisibility(View.GONE);
        tv_checkout.setVisibility(View.VISIBLE);
        recycler_cart_items.setVisibility(View.VISIBLE);

        cartAdapter=new CartAdapter(AddCartActivity.this,cartlist,0);
        recycler_cart_items.setAdapter(cartAdapter);
       // cartFetcher();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tv_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(AddCartActivity.this, CheckOutPageActivity.class);
                startActivity(intent);
            }
        });
    }

    public void saveitems(String id,String item_id,String qty,String price,String discount,String product_code,String desc){
        CartModel cartModel=new CartModel();
        cartModel.setId(id);
        cartModel.setItem_id(item_id);
        cartModel.setQty(qty);
       /* cartModel.setPrice(price);
        cartModel.setDiscount(discount);
        cartModel.setProduct_code(product_code);
        cartModel.setDesc(desc);
        cartModel.setName(item_id);*/
        cartlist.add(cartModel);

    }

    private void cartFetcher() {
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CARTREAD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    customDialog.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                CartModel cartModel=new CartModel();
                                cartModel.setId(jsonObject1.getString("id"));
                                cartModel.setItem_id(jsonObject1.getString("item_id"));
                                cartModel.setQty(jsonObject1.getString("qty"));
                              /*  cartModel.setName(jsonObject1.getString("name"));
                                cartModel.setPrice(jsonObject1.getString("price"));
                                cartModel.setDiscount(jsonObject1.getString("discount"));
                                cartModel.setProduct_code(jsonObject1.getString("product_code"));
                                cartModel.setDesc(jsonObject1.getString("desc"));*/
                                cartlist.add(cartModel);
                            }


                            if (cartlist.size()!=0){
                                relative_empty_cart.setVisibility(View.GONE);
                                tv_checkout.setVisibility(View.VISIBLE);
                                recycler_cart_items.setVisibility(View.VISIBLE);
                            }else {
                                relative_empty_cart.setVisibility(View.VISIBLE);
                                tv_checkout.setVisibility(View.GONE);
                                recycler_cart_items.setVisibility(View.GONE);
                            }



                        }else {
                            relative_empty_cart.setVisibility(View.VISIBLE);
                            tv_checkout.setVisibility(View.GONE);
                            recycler_cart_items.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        relative_empty_cart.setVisibility(View.VISIBLE);
                        tv_checkout.setVisibility(View.GONE);
                        recycler_cart_items.setVisibility(View.GONE);
                        e.printStackTrace();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                    }
                }else {
                    customDialog.dismiss();
                    relative_empty_cart.setVisibility(View.VISIBLE);
                    tv_checkout.setVisibility(View.GONE);
                    recycler_cart_items.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                relative_empty_cart.setVisibility(View.VISIBLE);
                tv_checkout.setVisibility(View.GONE);
                recycler_cart_items.setVisibility(View.GONE);
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}