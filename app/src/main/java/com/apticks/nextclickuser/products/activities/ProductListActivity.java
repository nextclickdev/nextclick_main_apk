package com.apticks.nextclickuser.products.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.SectionsPagerAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Pojo.FoodMenuPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.adapters.MenusPagerAdapter;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.FOOD_MENUS;

import static com.apticks.nextclickuser.Constants.Constants.ADDRESS;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;

public class ProductListActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView back_page;
    private TextView shop_name;
    private TabLayout foodiemenutablayout;
    private ViewPager viewpager;

    private String sub_cat_id,brandId="",menuid,search="pass",vendoruserid,restarentname,shopaddress="";

    private PreferenceManager preferenceManager;
    LinearLayoutManager layoutManager;
    private LinearLayout no_vendors_data;

    private Context mContext;
    ArrayList<FoodMenuPojo> foodMenuData = new ArrayList<>();
    MenusPagerAdapter menusPagerAdapter;
    private FrameLayout cart_layout;
    public  ArrayList<FoodMenuPojo> foodMenuDataName = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        getSupportActionBar().hide();

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        
        init();

       // adapterSetter();

    }

    public void init(){
        mContext=ProductListActivity.this;
        back_page=findViewById(R.id.img_back);
        shop_name=findViewById(R.id.tv_header);
        shop_name.setAllCaps(true);
        foodiemenutablayout=findViewById(R.id.foodiemenutablayout);
        viewpager=findViewById(R.id.viewpager);
        cart_layout=findViewById(R.id.cart_layout);
        no_vendors_data=findViewById(R.id.no_vendors_data);
        layoutManager = new LinearLayoutManager(this);
        preferenceManager = new PreferenceManager(ProductListActivity.this);
        back_page.setOnClickListener(this);
        cart_layout.setOnClickListener(this);

        foodiemenutablayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        try {
            sub_cat_id = getIntent().getStringExtra("sub_cat_id");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            restarentname = getIntent().getStringExtra(reastaurantName);
            shop_name.setText(restarentname);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            vendoruserid = getIntent().getStringExtra("vendor_user_id");
            System.out.println("aaaaaaa vendorid p "+vendoruserid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            shopaddress = getIntent().getStringExtra(ADDRESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //All menu Available in Restaurant of Food
        MenuFetcher();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{
            menusPagerAdapter = new MenusPagerAdapter(mContext, getSupportFragmentManager(),sub_cat_id,menuid,brandId,vendoruserid,shopaddress,restarentname,foodMenuDataName);
            viewpager.setAdapter(menusPagerAdapter);
            foodiemenutablayout.setupWithViewPager(viewpager);
        }catch (NullPointerException e){

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:
                finish();
                break;
            case R.id.cart_layout:
               // startActivity(new Intent(mContext,AddCartActivity.class));
                startActivity(new Intent(mContext,CartAddActivity.class));
                break;
        }
    }

    public void MenuFetcher() {
        foodMenuData.clear();
        foodMenuDataName.clear();

        DialogOpener.dialogOpener(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        Log.d("item_url",FOOD_MENUS +/*"1"*//* againVendorId*/  vendoruserid+"?sub_cat_id="+ sub_cat_id);
        System.out.println("aaaaaaaa urlmenufood "+FOOD_MENUS +/*"1"*//* againVendorId*/  vendoruserid+"?sub_cat_id="+ sub_cat_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FOOD_MENUS +/*"1"*//* againVendorId*/  vendoruserid+"?sub_cat_id="+ sub_cat_id , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, ""+str_vender, Toast.LENGTH_SHORT).show();
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaaa response "+jsonObject.toString());
                        //JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = jsonObject.getJSONArray("data");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                String id = resultObject.getString("id");
                                String desc = resultObject.getString("desc");
                                FoodMenuPojo foodMenuPojo = new FoodMenuPojo();

                                foodMenuPojo.setName(name);
                                foodMenuPojo.setId(id);
                                foodMenuPojo.setDesc(desc);
                                foodMenuPojo.setImage(image);
                                foodMenuData.add(foodMenuPojo);

                                foodMenuDataName.add(foodMenuPojo);
                            }
                        }
                        menusPagerAdapter = new MenusPagerAdapter(mContext, getSupportFragmentManager(),sub_cat_id,menuid,brandId,vendoruserid,shopaddress,restarentname,foodMenuDataName);
                        viewpager.setAdapter(menusPagerAdapter);
                        foodiemenutablayout.setupWithViewPager(viewpager);
                        DialogOpener.dialog.dismiss();
                        DialogOpener.dialog.dismiss();
                    } catch (Exception e) {
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                        e.printStackTrace();
                        DialogOpener.dialog.dismiss();
                        no_vendors_data.setVisibility(View.VISIBLE);
                        viewpager.setVisibility(View.GONE);
                        //UImsgs.showCustomToast(mContext, "Oops! Something went wrong",WARNING);
                    }



                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                no_vendors_data.setVisibility(View.VISIBLE);
                viewpager.setVisibility(View.GONE);
                System.out.println("aaaaaaaaa error "+error.getMessage());
                //UImsgs.showCustomToast(mContext, "Oops! Something went wrong ",ERROR);

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}