package com.apticks.nextclickuser.products.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.FullImageActivity;
import com.apticks.nextclickuser.products.activities.ProductDescriptionActivity;
import com.apticks.nextclickuser.products.model.ImagesModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<ImagesModel> imagelist;
    private String selectimage;
    public ViewPagerAdapter(Context context, ArrayList<ImagesModel> imageslist) {
        this.context = context;
        this.imagelist = imageslist;
    }

    public ViewPagerAdapter(FullImageActivity context, String selectimage) {
        this.context = context;
        this.selectimage = selectimage;

    }

    @Override
    public int getCount() {
        return 1;
      //  return imagelist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.zoomimage, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);


      /*  Glide.with(context)
                .load(imagelist.get(position).getImage())
                .placeholder(R.drawable.loader_gif)
                .into(imageView);*/


        Glide.with(context)
                .load(selectimage)
                .placeholder(R.drawable.loader_gif)
                .into(imageView);


        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}
