package com.apticks.nextclickuser.products.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActivityOptions;
import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.dialogs.RemoveProduct;
import com.apticks.nextclickuser.products.adapters.SaveAddressAdapter;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.Districts;
import com.apticks.nextclickuser.products.model.SaveAddress;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.ADDRESSDELETE;
import static com.apticks.nextclickuser.Config.Config.ADDRESSREAD;
import static com.apticks.nextclickuser.Config.Config.CARTREAD;
import static com.apticks.nextclickuser.Config.Config.URL_States;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class ChangeAddressActivity extends AppCompatActivity {

    private RecyclerView recycle_address;
    SaveAddressAdapter saveAddressAdapter;
    private ImageView img_back;
    private LinearLayout layout_addaddress;
    private ImageView current_location;
    AutocompleteSupportFragment autocompleteSupportFragment;
    ArrayList<SaveAddress> addresslist;
    CustomDialog customDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_address);

        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        TextView tv_header= findViewById(R.id.tv_header);
        tv_header.setText("DElIVERY ADDRESS");
        tv_header.setAllCaps(true);
        FrameLayout cart_layout=findViewById(R.id.cart_layout);
        cart_layout.setVisibility(View.GONE);

        recycle_address=findViewById(R.id.recycle_address);
        img_back=findViewById(R.id.img_back);
        layout_addaddress=findViewById(R.id.layout_addaddress);
        current_location=findViewById(R.id.current_location);
        mContext=ChangeAddressActivity.this;
        customDialog=new CustomDialog(ChangeAddressActivity.this);
        preferenceManager=new PreferenceManager(mContext);
        autocompleteSupportFragment =
                (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteSupportFragment.setHint("Search Address");
        autocompleteSupportFragment.setPlaceFields(Arrays.asList(Place.Field.ID,Place.Field.LAT_LNG,Place.Field.NAME));

        addresslist=new ArrayList<>();

        addressecth();

        recycle_address.setLayoutManager(new GridLayoutManager(ChangeAddressActivity.this,1));
        saveAddressAdapter=new SaveAddressAdapter(ChangeAddressActivity.this,addresslist);
        recycle_address.setAdapter(saveAddressAdapter);

        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                final LatLng latLng = place.getLatLng();

                /*Toast.makeText(PlacesActivity.this, place.getName(), Toast.LENGTH_SHORT).show();*/
                Log.d("Lat",place.getLatLng()+"");
                Log.d("Name",place.getName()+"");
                Log.d("id",place.getId()+"");
                Log.d("address",place.getAddress()+"");
                String latLang = place.getLatLng()+"".trim();
                String [] sep = latLang.split(":");
                String [] coordinates = sep[1].split(",");
                Log.d("lat", String.valueOf(coordinates[0]).substring(2,coordinates[0].length()));
                String lat =String.valueOf(coordinates[0]).substring(2,coordinates[0].length());
                Log.d("lang", String.valueOf(coordinates[1]).substring(0,coordinates[1].length()-1));
                String lang = String.valueOf(coordinates[1]).substring(0,coordinates[1].length()-1);

                // st_current_address=place.getAddress();
                autocompleteSupportFragment.setText(""+place.getAddress());
              //  latitude=place.getLatLng().latitude;
              //  longitude=place.getLatLng().longitude;
                System.out.println("aaaaaaaa address "+place.getAddress()+" "+place.getName()+" "+place.getAddressComponents());

                Intent intent=new Intent(ChangeAddressActivity.this,PlacesAddActivity.class);
                intent.putExtra("whichposition",0);
                intent.putExtra("address",place.getName());
                intent.putExtra("latitude",""+place.getLatLng().latitude);
                intent.putExtra("longitude",""+place.getLatLng().longitude);
                startActivityForResult(intent,101);

            }

            @Override
            public void onError(@NonNull Status status) {

            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        layout_addaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChangeAddressActivity.this, PlacesAddActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(ChangeAddressActivity.this).toBundle());
                } else {
                    startActivity(intent);
                }
            }
        });
        current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ChangeAddressActivity.this,PlacesAddActivity.class);
                intent.putExtra("whichposition",1);
                startActivityForResult(intent,101);
            }
        });

    }

    public void saveAddressClick(SaveAddress saveAddress) {
        Intent intent=new Intent();
        intent.putExtra("addressname",saveAddress);
      //  intent.putExtra("id",saveAddress.getId());
        setResult(2,intent);

        finish();
    }


    public void saveaddres(String id,String address,String name,String email,String mobile,String landmark,String pincode,String status){
        SaveAddress saveAddress=new SaveAddress();
        saveAddress.setAddress(address);
        saveAddress.setName(name);
        saveAddress.setEmail(email);
        saveAddress.setPhone(mobile);
        saveAddress.setLandmark(landmark);
        saveAddress.setPincode(pincode);
        saveAddress.setStatus(status);
        addresslist.add(saveAddress);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==101){
            try{
              //  String address = data.getStringExtra("addressname");
                SaveAddress saveAddress= (SaveAddress) data.getSerializableExtra("addressname");

                System.out.println("aaaaaaaa  carListAsString "+saveAddress.getAddress());
                Intent intent=new Intent();
                intent.putExtra("addressname",saveAddress);
               // intent.putExtra("id",saveAddress.getId());
                setResult(2,intent);

                finish();

            }catch (NullPointerException e){

            }
        }
    }

    private void addressecth() {
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ADDRESSREAD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    customDialog.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                SaveAddress saveAddress=new SaveAddress();
                                saveAddress.setId(jsonObject1.getString("id"));
                                saveAddress.setPhone(jsonObject1.getString("phone"));
                                saveAddress.setEmail(jsonObject1.getString("email"));
                                saveAddress.setName(jsonObject1.getString("name"));
                                saveAddress.setLandmark(jsonObject1.getString("landmark"));
                                saveAddress.setPincode(jsonObject1.getString("pincode"));
                                saveAddress.setAddress(jsonObject1.getString("address"));
                                saveAddress.setFirst_name(jsonObject1.getString("first_name"));
                                saveAddress.setLast_name(jsonObject1.getString("last_name"));
                                saveAddress.setStatuscheck("0");

                                addresslist.add(saveAddress);
                            }
                            saveAddressAdapter.setChange(addresslist);

                        }else {

                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                    }
                }else {
                    customDialog.dismiss();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void setDelete(SaveAddress saveAddress, int position) {
        RemoveProduct removeProduct =new RemoveProduct(ChangeAddressActivity.this,saveAddress);
        removeProduct.showDialog();
    }

    public void removeProduct(SaveAddress saveAddress) {

        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ADDRESSDELETE+saveAddress.getId(),
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){
                                String message=jsonObject1.getString("message");
                                Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();
                                addresslist.clear();
                                addressecth();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}