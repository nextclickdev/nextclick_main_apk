package com.apticks.nextclickuser.products.model;

import java.util.ArrayList;

public class CartModel {

    String id,qty,status,vendor_user_id,item_id,vendor_product_varinat_id;

    VendorModel vendorModel;
    ProductCartModel productCartModel;
    VendorvarientProducts vendorvarientProducts;

    ArrayList<ProductImages> productImages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVendor_user_id() {
        return vendor_user_id;
    }

    public void setVendor_user_id(String vendor_user_id) {
        this.vendor_user_id = vendor_user_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getVendor_product_varinat_id() {
        return vendor_product_varinat_id;
    }

    public void setVendor_product_varinat_id(String vendor_product_varinat_id) {
        this.vendor_product_varinat_id = vendor_product_varinat_id;
    }

    public VendorModel getVendorModel() {
        return vendorModel;
    }

    public void setVendorModel(VendorModel vendorModel) {
        this.vendorModel = vendorModel;
    }

    public ProductCartModel getProductCartModel() {
        return productCartModel;
    }

    public void setProductCartModel(ProductCartModel productCartModel) {
        this.productCartModel = productCartModel;
    }

    public VendorvarientProducts getVendorvarientProducts() {
        return vendorvarientProducts;
    }

    public void setVendorvarientProducts(VendorvarientProducts vendorvarientProducts) {
        this.vendorvarientProducts = vendorvarientProducts;
    }

    public ArrayList<ProductImages> getProductImages() {
        return productImages;
    }

    public void setProductImages(ArrayList<ProductImages> productImages) {
        this.productImages = productImages;
    }
}
