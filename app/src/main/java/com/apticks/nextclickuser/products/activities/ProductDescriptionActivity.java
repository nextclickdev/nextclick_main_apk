package com.apticks.nextclickuser.products.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutNewPageActivity;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.products.adapters.ImagesAdapter;
import com.apticks.nextclickuser.products.adapters.RelatedProductsAdapter;
import com.apticks.nextclickuser.products.adapters.VarientsAdapter;
import com.apticks.nextclickuser.products.adapters.ViewImagePagerAdapte;
import com.apticks.nextclickuser.products.fragmants.BottomVarientsFragment;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.ImagesModel;
import com.apticks.nextclickuser.products.model.ProductCartModel;
import com.apticks.nextclickuser.products.model.ProductDetails;
import com.apticks.nextclickuser.products.model.ProductImages;
import com.apticks.nextclickuser.products.model.ProductsModel;
import com.apticks.nextclickuser.products.model.VarientDetails;
import com.apticks.nextclickuser.products.model.VarientModel;
import com.apticks.nextclickuser.products.model.VendorModel;
import com.apticks.nextclickuser.products.model.VendorvarientProducts;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.apticks.nextclickuser.utils.ImageUtil;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CARTADD;
import static com.apticks.nextclickuser.Config.Config.CARTREAD;
import static com.apticks.nextclickuser.Config.Config.CARTUPDATE;
import static com.apticks.nextclickuser.Config.Config.USERPRODUCTS;
import static com.apticks.nextclickuser.Constants.Constants.ADDRESS;
import static com.apticks.nextclickuser.Constants.Constants.AUTH_TOKEN;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;
import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isNull;

public class ProductDescriptionActivity extends AppCompatActivity  implements View.OnClickListener {

    private ViewPager viewPager;
    private ViewImagePagerAdapte viewImagePagerAdapte;
    private ImageView img_product;
    private FrameLayout item_counter;
    private TextView productname,cartminus,cartquantity,cartplus,cartprice,tvAvailableQuantity,tv_addtocart,tv_buynow,shop_name,
            tv_pdprice,fooditemdisount,tv_descrition,tv_category,tv_menu,tv_brand,tv_options;
    LinearLayout sliderDotspanel,layout_discount,tv_add,layout_main;
    private ImageView[] dots;
    private RecyclerView recycler_sect_items,recycle_images,recycle_related;
    private ImagesAdapter imagesAdapter;
    private String sub_cat_id,brandId,vendoruserid,menuid,product_id,search="pass",shopaddress,shopname,
            minimum_price="0",selectedimage,token;
    private Context mContext;
    private ArrayList<VarientModel> varientlist;
    private VarientsAdapter VarientsAdapter;
    private ImageView img_card,img_back;
    private ArrayList<ImagesModel> imageslist;
    private int quantity=0,dotscount;
    private String varient_id,st_weight="",product_name;
    Gson gson;
    private CustomDialog customDialog;
    VarientModel varientmodel;
    private PreferenceManager preferenceManager;
    private  int totalAvailableStock =0,check=0;
    private LinearLayout layout_cartplusminus;
    BottomVarientsFragment bottomVarientsFragment;
    private ArrayList<CartModel> cartlist=new ArrayList<>();
    private ArrayList<ProductsModel> productlist=new ArrayList<>();
    ArrayList<VarientModel> productvarientlist=new ArrayList<>();
    private RelatedProductsAdapter relatedproducts;
    boolean isimageclikc=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_description);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        init();

    }
    public void init(){
        mContext=ProductDescriptionActivity.this;
        layout_main =  findViewById(R.id.layout_main);
        sliderDotspanel =  findViewById(R.id.SliderDots);
        productname =  findViewById(R.id.productname);
        cartminus =  findViewById(R.id.cartminus);
        cartquantity =  findViewById(R.id.cartquantity);
        cartplus =  findViewById(R.id.cartplus);
        cartprice =  findViewById(R.id.cartprice);
        tvAvailableQuantity =  findViewById(R.id.tvAvailableQuantity);
        recycler_sect_items =  findViewById(R.id.recycler_sect_items);
        viewPager =  findViewById(R.id.viewPager);
        img_card =  findViewById(R.id.img_card);
        img_back =  findViewById(R.id.img_back);
        tv_addtocart =  findViewById(R.id.tv_addtocart);
        tv_buynow =  findViewById(R.id.tv_buynow);
        shop_name =  findViewById(R.id.tv_header);
        item_counter =  findViewById(R.id.cart_layout);
        layout_discount =  findViewById(R.id.layout_discount);
        tv_pdprice =  findViewById(R.id.tv_pdprice);
        fooditemdisount =  findViewById(R.id.fooditemdisount);
        tv_descrition =  findViewById(R.id.tv_descrition);
        layout_cartplusminus =  findViewById(R.id.layout_cartplusminus);
        tv_add =  findViewById(R.id.tv_add);
        recycle_images =  findViewById(R.id.recycle_images);
        tv_category =  findViewById(R.id.tv_category);
        tv_menu =  findViewById(R.id.tv_menu);
        tv_brand =  findViewById(R.id.tv_brand);
        tv_options =  findViewById(R.id.tv_options);
        recycle_related =  findViewById(R.id.recycle_related);
        img_product =  findViewById(R.id.img_product);

      //  tvAvailableQuantity.setVisibility(View.GONE);

        sub_cat_id=getIntent().getStringExtra("sub_cat_id");
        brandId=getIntent().getStringExtra("brand_id");
        vendoruserid=getIntent().getStringExtra("vendor_id");
        menuid=getIntent().getStringExtra("menu_id");
        product_id=getIntent().getStringExtra("product_id");
        minimum_price=getIntent().getStringExtra("minimum_price");
        quantity=getIntent().getIntExtra("quantity",0);
        shopaddress=getIntent().getStringExtra(ADDRESS);
        shopname=getIntent().getStringExtra(reastaurantName);

        recycle_images.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
        recycle_related.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
        imageslist=new ArrayList<>();
        imagesAdapter=new ImagesAdapter(mContext,imageslist);
        recycle_images.setAdapter(imagesAdapter);
        relatedproducts=new RelatedProductsAdapter(mContext,productlist,sub_cat_id,brandId,menuid);
        recycle_related.setAdapter(relatedproducts);
        try{
            quantity=Integer.parseInt(getIntent().getStringExtra("quantity"));
            varient_id=getIntent().getStringExtra("varient_id");
            cartquantity.setText(""+quantity);
            check=1;
        }catch (NumberFormatException e){

        }catch (NullPointerException e1){

        }
        shop_name.setText(shopname);

        if (quantity==0){
            tv_add.setVisibility(View.VISIBLE);
            layout_cartplusminus.setVisibility(View.GONE);
        }else {
            tv_add.setVisibility(View.GONE);
            layout_cartplusminus.setVisibility(View.VISIBLE);
            cartquantity.setText(""+quantity);
        }


        customDialog=new CustomDialog(mContext);
        preferenceManager=new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);

        varientlist=new ArrayList<VarientModel>();
        VarientsAdapter=new VarientsAdapter(ProductDescriptionActivity.this,varientlist);
        recycler_sect_items.setAdapter(VarientsAdapter);
        recycler_sect_items.setLayoutManager(new GridLayoutManager(ProductDescriptionActivity.this,1));
        recycler_sect_items.setAdapter(imagesAdapter);
        cartplus.setOnClickListener(this::onClick);
        cartminus.setOnClickListener(this);
        img_back.setOnClickListener(this);
        tv_addtocart.setOnClickListener(this);
        tv_buynow.setOnClickListener(this);
        item_counter.setOnClickListener(this);


        recycler_sect_items.setVisibility(View.GONE);
        tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomVarientsFragment = new BottomVarientsFragment(mContext,varientlist,1,minimum_price);
                bottomVarientsFragment.show(getSupportFragmentManager(), bottomVarientsFragment.getTag());

            }
        });
        img_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isimageclikc=true;
                setviewpageimageclick(selectedimage,imageslist);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isimageclikc){
            isimageclikc=false;
        }else {
            productsFetcher();
          //  cartFetcher();
        }


    }

    public void setdots(){
        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);
            dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    for (int i = 0; i < dotscount; i++) {
                        dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));
                    }

                    dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    private void productsFetcher() {

        varientlist.clear();

        String url = "";

        url = USERPRODUCTS  + "?sub_cat_id=" +sub_cat_id +"&menu_id=" + menuid + "&brand_id=" + brandId
                + "&q= "+ search +
                "&vendor_user_id="+vendoruserid+"&item_id="+product_id;
        // url = USERPRODUCTS + limit + "/" + offset + SUBCATID +sub_cat_id +MENUID + menuid + BRANDID + brandId  +SEARCH + search + VENDORUSERID+"29";

        Log.d("Url", url);
        System.out.println("aaaaaaaaaa  url "+url);

        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("brand vendors response", response);
                   // customDialog.dismiss();
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    ProductDetails productsModel=new ProductDetails();

                                    productsModel.setId(jsonObject1.getString("id"));
                                    productsModel.setProduct_code(jsonObject1.getString("product_code"));
                                    productsModel.setName(jsonObject1.getString("name"));
                                    productsModel.setDesc(jsonObject1.getString("desc"));
                                    productsModel.setItem_type(jsonObject1.getString("item_type"));
                                    productsModel.setAvailability(jsonObject1.getString("availability"));
                                    productsModel.setMenu_id(jsonObject1.getString("menu_id"));
                                    productsModel.setSub_cat_id(jsonObject1.getString("sub_cat_id"));
                                    productsModel.setBrand_id(jsonObject1.getString("brand_id"));

                                  tv_descrition.setText("Description : "+Html.fromHtml(jsonObject1.getString("desc")));

                                  try{
                                      JSONObject menuobj=jsonObject1.getJSONObject("menu");
                                      tv_menu.setText(menuobj.getString("name"));
                                  }catch (JSONException e){

                                  }

                            try{
                                JSONObject menuobj=jsonObject1.getJSONObject("brand");
                                tv_brand.setText(menuobj.getString("name"));
                            }catch (JSONException e){

                            }
                            try{
                                JSONObject menuobj=jsonObject1.getJSONObject("sub_category");
                                tv_category.setText(menuobj.getString("name"));
                            }catch (JSONException e){

                            }

                            JSONArray varientarray=jsonObject1.getJSONArray("vendor_product_varinats");

                                    for (int j=0;j< varientarray.length();j++){
                                        JSONObject varientobj=varientarray.getJSONObject(j) ;
                                        VarientModel varientModel=new VarientModel();
                                        varientModel.setId(varientobj.getString("id"));
                                        varientModel.setItem_id(varientobj.getString("item_id"));
                                        varientModel.setSection_id(varientobj.getString("section_id"));
                                        varientModel.setSection_item_id(varientobj.getString("section_item_id"));
                                        varientModel.setSku(varientobj.getString("sku"));
                                        varientModel.setPrice(varientobj.getString("price"));
                                        varientModel.setStock(varientobj.getString("stock"));
                                        varientModel.setDiscount(varientobj.getString("discount"));
                                        varientModel.setList_id(varientobj.getString("list_id"));
                                        varientModel.setVendor_user_id(varientobj.getString("vendor_user_id"));
                                        varientModel.setStatus(varientobj.getString("status"));
                                        varientModel.setSection_item_name(varientobj.getString("section_item_name"));
                                        varientModel.setWeight(varientobj.getString("weight"));
                                        varientModel.setSection_item_code(varientobj.getString("section_item_code"));
                                        varientModel.setDesc(varientobj.getString("desc"));
                                        if (j==0){
                                            st_weight=isNull(varientobj.getString("weight"));
                                        }else {
                                            st_weight=st_weight+" , "+isNull(varientobj.getString("weight"));
                                        }
                                        if (check==1){
                                            System.out.println("aaaaaaaa "+varientobj.getString("id")+"  "+varient_id+varientobj.getString("discount"));
                                            if (varientobj.getString("id").equalsIgnoreCase(""+varient_id)){
                                                varientModel.setChecked(true);
                                            }

                                            if (!varientobj.getString("discount").equalsIgnoreCase("0")){
                                                System.out.println("aaaaaaa dsicount  "+varientobj.getString("discount")+" price "+varientobj.getString("price"));
                                                layout_discount.setVisibility(View.VISIBLE);
                                                tv_pdprice.setText(varientobj.getString("price"));
                                                tv_pdprice.setPaintFlags(tv_pdprice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
                                                double totalprice =((Double.parseDouble(varientobj.getString("price")) / 100.0f) * Integer.parseInt(
                                                        varientobj.getString("discount")));
                                                fooditemdisount.setText(""+varientobj.getString("discount")+" % off"+"(-"+String.format("%.2f",totalprice)+" )");
                                                fooditemdisount.setText(""+varientobj.getString("discount")+" % off (Save "+totalprice+" )");
                                                double total=Double.parseDouble(varientobj.getString("price"))-totalprice;
                                                System.out.println("aaaaaa total "+total+"  "+totalprice);
                                                cartprice.setText("₹ "+total);
                                            }
                                        }else {
                                            varientModel.setChecked(false);
                                        }

                                       // productsModel.setVarientlist(varientModel);
                                        varientlist.add(varientModel);

                                    }
                                    tv_options.setText(""+st_weight);
                            try {
                                imageslist.clear();
                                Object aObj = jsonObject1.get("item_images");
                                if (aObj instanceof JSONArray) {
                                    JSONArray imagesArray = jsonObject1.getJSONArray("item_images");
                                    for (int i = 0; i < imagesArray.length(); i++) {
                                        JSONObject imgjsonobject = imagesArray.getJSONObject(i);
                                        String id = imgjsonobject.getString("id");
                                        // String image=jsonObject1.getString("image");
                                        // System.out.println("aaaaaaa image "+id+" "+imgjsonobject.getString("image"));
                                        ImagesModel imagesModel = new ImagesModel();
                                        imagesModel.setId(id);
                                        imagesModel.setImage(imgjsonobject.getString("image"));
                                        imageslist.add(imagesModel);
                                    }
                                } else if (aObj instanceof JSONObject) {
                                    JSONObject imgjsonobject = jsonObject1.getJSONObject("item_images");
                                    String id = imgjsonobject.getString("id");
                                    ImagesModel imagesModel = new ImagesModel();
                                    imagesModel.setId(id);
                                    imagesModel.setImage(imgjsonobject.getString("image"));
                                    imageslist.add(imagesModel);
                                }
                                try {

                                    if (imageslist != null) {
                                        imageslist.get(0).setIsselect(true);
                                        selectedimage = imageslist.get(0).getImage();
                                    }
                                    ImageUtil.LoadImageFromUrl(mContext, selectedimage, R.drawable.no_image, img_product);
                                } catch (NullPointerException e) {

                                }

                                viewImagePagerAdapte = new ViewImagePagerAdapte(mContext, imageslist);

                                //   viewPager.setAdapter(viewImagePagerAdapte);
                                if (imageslist != null && imageslist.size() > 1) {
                                    //for one image no need of viewpager
                                    imagesAdapter.setrefresh(imageslist);
                                    recycle_images.setVisibility(View.VISIBLE);
                                } else {
                                    recycle_images.setVisibility(View.GONE);
                                }
                                //   dotscount = viewImagePagerAdapte.getCount();
                                //  dots = new ImageView[dotscount];
                                //   setdots();
                            }catch (Exception e){
                                System.out.println("aaaaaa catch img "+e.getMessage());
                            }
                            product_name=jsonObject1.getString("name");
                            productname.setText(""+jsonObject1.getString("name"));
                          //  cartprice.setText(""+varientlist.get(0).getPrice());

                            for (int l=0;l<varientlist.size();l++){
                                System.out.println("aaaaaaaaa minimum priec "+minimum_price+ "  "+varientlist.get(l).getPrice());
                                if (minimum_price.equalsIgnoreCase(varientlist.get(l).getPrice())){
                                    varientmodel=varientlist.get(l);
                                    productname.setText(product_name+" ( "+varientmodel.getSection_item_name()+" )");
                                    if (varientmodel.getStock().equalsIgnoreCase("0")){
                                        tvAvailableQuantity.setText("Not Available");
                                        totalAvailableStock =0;
                                    }else {
                                        totalAvailableStock =Integer.parseInt(varientmodel.getStock());
                                        tvAvailableQuantity.setText("Available Stock : "+varientmodel.getStock());
                                    }
                                    if (check!=1) {
                                        if (varientmodel.getDiscount().equalsIgnoreCase("0")) {
                                            layout_discount.setVisibility(View.GONE);
                                            cartprice.setText("₹ " + varientmodel.getPrice());
                                        } else {
                                            layout_discount.setVisibility(View.VISIBLE);
                                            tv_pdprice.setText(varientmodel.getPrice());
                                            tv_pdprice.setPaintFlags(tv_pdprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                            double totalprice = ((Double.parseDouble(varientmodel.getPrice()) / 100.0f) *
                                                    Integer.parseInt(
                                                            varientmodel.getDiscount()));
                                            fooditemdisount.setText(""+varientmodel.getDiscount()+" % off"+"(-"+String.format("%.2f",totalprice)+" )");
                                            fooditemdisount.setText("" + varientmodel.getDiscount() + " % off (Save " + totalprice + " )");
                                            double total = Double.parseDouble(varientmodel.getPrice()) - totalprice;
                                            cartprice.setText("₹ " + total);

                                        }
                                    }
                                    break;
                                }
                            }
                            if (check!=1){
                                varientlist.get(0).setChecked(true);
                            }
                            //varientmodel=varientlist.get(0);
                            VarientsAdapter.setrefresh(varientlist);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, "Currently this product not available", Toast.LENGTH_SHORT).show();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                        finish();
                    }
                    finally {
                        customDialog.dismiss();
                        layout_main.setVisibility(View.VISIBLE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                layout_main.setVisibility(View.VISIBLE);
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
        relatedproducts(10,0);
        cartFetcher();
    }

    public void setRefresh(int position,VarientModel varientmodel) {
        this.varientmodel=varientmodel;
        productname.setText(product_name+" ( "+varientmodel.getSection_item_name()+" )");
        for (int k=0;k<varientlist.size();k++){
            varientlist.get(k).setChecked(false);
        }
        varientlist.get(position).setChecked(true);
      //  varientmodel.setChecked(true);
      //  cartprice.setText(""+varientmodel.getPrice());
        if (varientmodel.getStock().equalsIgnoreCase("0")){
            totalAvailableStock =0;
            tvAvailableQuantity.setText("Not Available");
        }else {
            totalAvailableStock =Integer.parseInt(varientmodel.getStock());
            tvAvailableQuantity.setText("Available Stock :  "+varientmodel.getStock());
        }
        tv_pdprice.setText("₹ "+varientmodel.getPrice());
        cartprice.setText("₹ "+varientmodel.getPrice());

        System.out.println("aaaaaaaa cartprice  "+varientmodel.getPrice());
        if (varientmodel.getDiscount().equalsIgnoreCase("0")){
            layout_discount.setVisibility(View.GONE);

        }else {
            layout_discount.setVisibility(View.VISIBLE);

            tv_pdprice.setPaintFlags(tv_pdprice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
            double totalprice =((Double.parseDouble(varientmodel.getPrice()) / 100.0f) * Integer.parseInt(
                    varientmodel.getDiscount()));
            fooditemdisount.setText(""+varientmodel.getDiscount()+" % off"+"(-"+String.format("%.2f",totalprice)+" Saved)");
           // fooditemdisount.setText(""+varientmodel.getDiscount()+" % off (Save "+totalprice+" )");
            double total=Double.parseDouble(varientmodel.getPrice())-totalprice;
            cartprice.setText("₹ "+total);
            System.out.println("aaaaaaaa cartprice total "+total);
        }
      //  tvAvailableQuantity.setText(""+varientmodel.getStock());
        VarientsAdapter.setrefresh(varientlist);
        System.out.println("aaaaaaaaaa stock  "+varientmodel.getStock());
        if (position!=0){
            if (Integer.parseInt(varientmodel.getStock())<=0){

                System.out.println("aaaaaaaaa qty 444 ");
                cartquantity.setText("0");
                cartplus.setEnabled(false);
            }else {
                cartquantity.setText(""+quantity);
                cartplus.setEnabled(true);
            }
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cartplus:
                try{


                    if (varientlist.size()==1){
                        //ProductUpdate(varientmodel,"1");
                        addproduct(varientmodel,1,0);

                    }else {
                        bottomVarientsFragment = new BottomVarientsFragment(mContext,varientlist,1,minimum_price);
                        bottomVarientsFragment.show(getSupportFragmentManager(), bottomVarientsFragment.getTag());
                    }

                   /* if (totalquantity>(Integer.parseInt(cartquantity.getText().toString()))){
                        cartminus.setEnabled(true);
                        cartminus.setClickable(true);
                        quantity=quantity+1;
                        cartquantity.setText(""+quantity);
                    }else {
                        cartplus.setEnabled(false);
                        cartplus.setClickable(false);
                    }*/

                }catch (NumberFormatException e){

                }

                break;
            case R.id.cartminus:
                try {
                    if (varientlist.size()==1){
                        //cartminus.setEnabled(false);
                        //cartminus.setClickable(false);
                        //ProductUpdate(varientmodel,"1");

                        ProductUpdate(varientmodel,cartquantity.getText().toString());

                    }else {
                      //  quantity=quantity-1;
                        new AlertDialog.Builder(mContext)
                                .setTitle("Remove item from cart")
                                .setMessage("This item has multiple customizations added.\n Proceed to cart to remove item")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Gson gson=new Gson();
                                        String cartliststring = gson.toJson(cartlist);
                                        Intent intent=new Intent(mContext, CartAddActivity.class);
                                        //  Intent intent=new Intent(mContext, CheckOutPageActivity.class);
                                        //intent.putExtra("cartlist",cartliststring);
                                        startActivity(intent);
                                    }
                                })

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                    cartplus.setEnabled(true);
                    cartplus.setClickable(true);
                    System.out.println("aaaaaaaaa 11 quantity  "+quantity);
                   // cartquantity.setText(""+quantity);
                }catch (NumberFormatException e){

                }

                break;
            case R.id.img_back:
                finish();
                break;
            case R.id.tv_addtocart:
                if (cartquantity.getText().toString().trim().equalsIgnoreCase("0")){
                    Toast.makeText(mContext, "Out Of Stock", Toast.LENGTH_SHORT).show();
                }else {
                    try{
                        Map<String, String> uploadMap = new HashMap<>();
                        uploadMap.put("item_id", varientmodel.getItem_id());
                        uploadMap.put("vendor_product_variant_id", varientmodel.getId());
                        uploadMap.put("qty", ""+cartquantity.getText().toString().trim());
                        uploadMap.put("vendor_user_id", varientmodel.getVendor_user_id());

                        JSONObject json = new JSONObject(uploadMap);
                        String data=json.toString();
                        System.out.println("aaaaaaaaa data "+data);
                      //  callAddtocart(data, varientModel);
                    }catch (NullPointerException e){
                        Toast.makeText(mContext, "Out Of Stock", Toast.LENGTH_SHORT).show();
                    }

                }

                break;
            case R.id.tv_buynow:
                if (cartquantity.getText().toString().equalsIgnoreCase("0")){
                    Toast.makeText(mContext, "Please Select Quantity", Toast.LENGTH_SHORT).show();
                }else {
                    try{
                      //  Toast.makeText(mContext, "Comming soon", Toast.LENGTH_SHORT).show();
                        Gson gson=new Gson();
                        String cartliststring = gson.toJson(varientmodel);
                        Intent intent=new Intent(ProductDescriptionActivity.this, CheckOutPageActivity.class);
                        intent.putExtra("variantlist",  varientmodel);
                        intent.putExtra("quantity",cartquantity.getText().toString().trim());
                        intent.putExtra("whichactivity",1);
                        startActivity(intent);
                        /*int quan=Integer.parseInt(cartquantity.getText().toString().trim());
                        Intent intent=new Intent(ProductDescriptionActivity.this, CheckOutPageActivity.class);
                        intent.putExtra(ADDRESS,shopaddress);
                        intent.putExtra(reastaurantName,shopname);
                        startActivity(intent);*/
                    }catch (NumberFormatException e){

                    }


                }

                break;
            case R.id.cart_layout:
                Intent intent =new Intent(ProductDescriptionActivity.this,CartAddActivity.class);
               // Intent intent =new Intent(ProductDescriptionActivity.this,AddCartActivity.class);
                startActivity(intent);

        }
    }

    public void setviewpageimageclick(String image, ArrayList<ImagesModel> imagelist) {
        gson=new Gson();
        String myJson = gson.toJson(imagelist);
        String jsonCars = gson.toJson(imagelist);
        Intent intent=new Intent(ProductDescriptionActivity.this,FullImageActivity.class);
        intent.putExtra("imageliststring",jsonCars);
        intent.putExtra("selectimage",selectedimage);
        startActivity(intent);
    }

    private void callAddtocart(final String data, VarientModel varientModel) {

        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTADD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            Log.d("cc_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa  jsonobject  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    quantity=quantity+1;
                                    System.out.println("aaaaaaaaaa quantity  "+quantity);
                                    cartquantity.setText(""+quantity);
                                    layout_cartplusminus.setVisibility(View.VISIBLE);
                                    tv_add.setVisibility(View.GONE);
                                    setRefresh(0,varientModel);
                                    if (check==0){
                                      //  Toast.makeText(mContext, "Product Added Successfully", Toast.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(mContext, "Product Updated Successfully", Toast.LENGTH_SHORT).show();
                                    }

                                  //  finish();
                                } else {
                                    System.out.println("aaaaaaa else  ");
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch  "+e.getMessage());
                                customDialog.cancel();
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaa error  "+error.getMessage());
                customDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void addproduct(VarientModel varientModel, int i, double totalamount) {
        try{

            if (Integer.parseInt(varientModel.getStock()) > (Integer.parseInt(cartquantity.getText().toString()))){
                Map<String, String> uploadMap = new HashMap<>();
                uploadMap.put("item_id", varientModel.getItem_id());
                uploadMap.put("vendor_product_variant_id", varientModel.getId());
                uploadMap.put("qty", "" + (i));
                uploadMap.put("vendor_user_id", varientModel.getVendor_user_id());

                JSONObject json = new JSONObject(uploadMap);
                String data = json.toString();
                System.out.println("aaaaaaaaa data " + data);
                callAddtocart(data, varientModel);

            }else {
                showErrorDialog("Out Of Stock");
            }
        }catch (NullPointerException e){
            Toast.makeText(mContext, "Out Of Stock", Toast.LENGTH_SHORT).show();
        }
    }

    private void showErrorDialog(String errorMessage) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.layout_error_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        TextView tv_success= (TextView) dialog.findViewById(R.id.tv_success);
        tv_success.setText(errorMessage);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    public void ProductUpdate(VarientModel varientmodel, String qty) {
        customDialog.show();
        String cartid="";
        for (int i=0;i<cartlist.size();i++){
            if (cartlist.get(i).getVendor_product_varinat_id().equalsIgnoreCase(varientmodel.getId())){
                cartid=cartlist.get(i).getId();
                break;
            }
        }
        Map<String,String> countMap = new HashMap<>();
        countMap.put("id", cartid);
        countMap.put("item_id",varientmodel.getItem_id());
        countMap.put("vendor_product_variant_id",varientmodel.getId());
        countMap.put("qty",""+(Integer.parseInt(qty)-1));
        countMap.put("vendor_user_id",varientmodel.getVendor_user_id());

        final String data = new JSONObject(countMap).toString();
        System.out.println("aaaaaaaaa request cartupdate "+data);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTUPDATE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customDialog.dismiss();
                try {
                    JSONObject jsonObject1=new JSONObject(response);
                    System.out.println("aaaaaaaaaa  response  "+jsonObject1.toString());

                    boolean status = jsonObject1.getBoolean("status");
                    int http_code = jsonObject1.getInt("http_code");
                    if (status) {
                        quantity = quantity - 1;
                        System.out.println("aaaaaaaaaa quantity  " + quantity);
                        cartquantity.setText("" + quantity);
                        if(quantity>=1) {
                            layout_cartplusminus.setVisibility(View.VISIBLE);
                            tv_add.setVisibility(View.GONE);
                        }
                        else
                        {
                            layout_cartplusminus.setVisibility(View.GONE);
                            tv_add.setVisibility(View.VISIBLE);
                        }
                        setRefresh(0, varientmodel);
                        if (check == 0) {
                            //  Toast.makeText(mContext, "Product Added Successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "Product Updated Successfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cartFetcher();
              //  cartFetcher();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

        };
        requestQueue.add(stringRequest);

    }
    private void cartFetcher() {
        cartlist.clear();
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTREAD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    customDialog.dismiss();
                    try {
                        boolean checkqty=false;
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response cart "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            Log.v("data",jsonArray.toString());
                            for (int i=0;i<jsonArray.length();i++){


                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                if (!jsonObject1.getString("qty").equalsIgnoreCase("0")){
                                    checkqty=true;
                                    CartModel cartModel=new CartModel();

                                    cartModel.setId(jsonObject1.getString("id"));
                                    cartModel.setVendor_user_id(jsonObject1.getString("vendor_user_id"));
                                    cartModel.setItem_id(jsonObject1.getString("item_id"));
                                    cartModel.setQty(jsonObject1.getString("qty"));
                                    cartModel.setVendor_product_varinat_id(jsonObject1.getString("vendor_product_variant_id"));
                                    cartModel.setStatus(jsonObject1.getString("status"));

                                    VendorModel vendorModel=new VendorModel();

                                    vendorModel.setId(jsonObject1.getJSONObject("vendor").getString("id"));
                                    vendorModel.setVendor_user_id(jsonObject1.getJSONObject("vendor").getString("vendor_user_id"));
                                    vendorModel.setName(jsonObject1.getJSONObject("vendor").getString("name"));
                                    vendorModel.setDesc(jsonObject1.getJSONObject("vendor").getString("desc"));

                                    cartModel.setVendorModel(vendorModel);

                                    ProductCartModel productCartModel=new ProductCartModel();
                                    productCartModel.setId(jsonObject1.getJSONObject("item").getString("id"));
                                    productCartModel.setName(jsonObject1.getJSONObject("item").getString("name"));
                                    productCartModel.setDesc(jsonObject1.getJSONObject("item").getString("desc"));

                                    cartModel.setProductCartModel(productCartModel);

                                    VendorvarientProducts vendorvarientProducts=new VendorvarientProducts();
                                    vendorvarientProducts.setId(jsonObject1.getJSONObject("vendor_product_variant").getString("id"));
                                    vendorvarientProducts.setSection_item_id(jsonObject1.getJSONObject("vendor_product_variant").getString("section_item_id"));
                                    vendorvarientProducts.setStock(jsonObject1.getJSONObject("vendor_product_variant").getString("stock"));
                                    vendorvarientProducts.setPrice(jsonObject1.getJSONObject("vendor_product_variant").getString("price"));
                                    vendorvarientProducts.setDiscount(jsonObject1.getJSONObject("vendor_product_variant").getString("discount"));
                                    vendorvarientProducts.setStatus(jsonObject1.getJSONObject("vendor_product_variant").getString("status"));

                                    VarientDetails varientDetails=new VarientDetails();

                                    varientDetails.setName(jsonObject1.getJSONObject("vendor_product_variant").getJSONObject("details").getString("name"));
                                    varientDetails.setWeight(jsonObject1.getJSONObject("vendor_product_variant").getJSONObject("details").getString("weight"));

                                    vendorvarientProducts.setVarientDetails(varientDetails);
                                    cartModel.setVendorvarientProducts(vendorvarientProducts);

                                    ProductImages productImages=new ProductImages();
                                    ArrayList<ProductImages> productimagelist=new ArrayList<>();
                                    try{
                                        JSONArray imagesarrsy=jsonObject1.getJSONArray("item_images");
                                        for (int k=0;k<imagesarrsy.length();k++){
                                            JSONObject imageobject=imagesarrsy.getJSONObject(k);
                                            productImages.setId(imageobject.getString("id"));
                                            productImages.setItem_id(imageobject.getString("item_id"));
                                            productImages.setExt(imageobject.getString("ext"));
                                            productImages.setImage(imageobject.getString("image"));
                                            productimagelist.add(productImages);
                                        }
                                    }catch (JSONException | NullPointerException e){
                                        productImages.setImage("");
                                    }

                                    cartModel.setProductImages(productimagelist);


                                    if (cartModel.getStatus().equalsIgnoreCase("1")){
                                        cartlist.add(cartModel);
                                    }else {

                                    }
                                }else{
                                    checkqty=false;
                                }

                            }
                            /*if (checkqty){

                                layout_cartplusminus.setVisibility(View.VISIBLE);
                                tv_add.setVisibility(View.GONE);
                                cartquantity.setText(""+quantity);
                            }else {
                                quantity=0;
                                cartquantity.setText("0");
                                layout_cartplusminus.setVisibility(View.GONE);
                                tv_add.setVisibility(View.VISIBLE);
                            }*/
                            int qty=0;
                            double totalprice=0.0;
                            for (int i=0;i<cartlist.size();i++){
                                qty=Integer.parseInt(cartlist.get(i).getQty())+qty;
                                if (!cartlist.get(i).getVendorvarientProducts().getDiscount().equalsIgnoreCase("0")){

                                    double discount=Double.parseDouble(cartlist.get(i).getVendorvarientProducts().getDiscount());
                                    double price=Double.parseDouble(cartlist.get(i).getVendorvarientProducts().getPrice());

                                    double totalprice1 =price- ((price / 100.0f) * discount);
                                    totalprice=totalprice+(totalprice1*Integer.parseInt(cartlist.get(i).getQty()));
                                    //  totalprice=totalprice+totalprice1;

                                }else {
                                    totalprice=totalprice+(Double.parseDouble(cartlist.get(i).getVendorvarientProducts().getPrice()) * Integer.parseInt(cartlist.get(i).getQty()));
                                }
                                //  System.out.println("aaaaaaaa qty  "+qty);
                            }

                          //  tv_items.setText("Total items "+qty);

                           /* if (cartlist.size()!=0){
                                layout_cartitems.setVisibility(View.VISIBLE);
                                tv_price.setText(""+String.format("%.2f",totalprice)+"");

                                //  tv_price.setText(" ₹ "+totalprice);
                            }else {
                                layout_cartitems.setVisibility(View.GONE);
                            }*/


                        }else {
                          //  layout_cartitems.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                      //  layout_cartitems.setVisibility(View.GONE);
                        e.printStackTrace();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                    }
                }else {
                  //  layout_cartitems.setVisibility(View.GONE);
                    customDialog.dismiss();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  layout_cartitems.setVisibility(View.GONE);
                customDialog.dismiss();
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                System.out.println("aaaaaaaaaa acccestoken  "+preferenceManager.getString(TOKEN_KEY));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void relatedproducts(int limit, int offset) {
        productlist.clear();
        String url = "";

        url = USERPRODUCTS + limit + "/" + offset + "?sub_cat_id=" +sub_cat_id +"&menu_id=" + menuid + "&brand_id=" + brandId
                +"&vendor_user_id="+vendoruserid/*+ "&q="+ search*/ ;
        // url = USERPRODUCTS + limit + "/" + offset + SUBCATID +sub_cat_id +MENUID + menuid + BRANDID + brandId  +SEARCH + search + VENDORUSERID+"29";

        Log.d("Url", url);
        System.out.println("aaaaaaaaaa  url product "+url);


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response  product "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            if (!dataObject.get("result").getClass().toString().equalsIgnoreCase(str.getClass().toString())) {
                                JSONArray resultArray = dataObject.getJSONArray("result");

                                for (int i=0;i<resultArray.length();i++){
                                    JSONObject jsonObject1=resultArray.getJSONObject(i);
                                    ProductsModel productsModel=new ProductsModel();

                                    productsModel.setId(jsonObject1.getString("id"));
                                    productsModel.setSub_cat_id(jsonObject1.getString("sub_cat_id"));
                                    productsModel.setMenu_id(jsonObject1.getString("menu_id"));
                                    productsModel.setBrand_id(jsonObject1.getString("brand_id"));
                                    productsModel.setProduct_code(jsonObject1.getString("product_code"));
                                    productsModel.setName(jsonObject1.getString("name"));
                                    productsModel.setDesc(jsonObject1.getString("desc"));
                                    productsModel.setItem_type(jsonObject1.getString("item_type"));
                                    productsModel.setMin_price(jsonObject1.getString("min_price"));
                                    productsModel.setMax_stock(jsonObject1.getString("max_stock"));
                                    productsModel.setAvg_discount(jsonObject1.getString("avg_discount"));
                                    productsModel.setVendor_user_id(jsonObject1.getString("vendor_user_id"));
                                    productsModel.setStatus(jsonObject1.getString("status"));
                                    productsModel.setImage_id(jsonObject1.getString("image_id"));
                                    productsModel.setExt(jsonObject1.getString("ext"));
                                    productsModel.setImage(jsonObject1.getString("image"));
                                    productsModel.setQuantity(jsonObject1.getString("cart_qty"));

                                    try {
                                        JSONArray  varientsaray = jsonObject1.getJSONArray("vendor_varinats");
                                        productvarientlist=new ArrayList<>();
                                        for (int k=0;k<varientsaray.length();k++){
                                            VarientModel productVarients =new VarientModel();
                                            JSONObject varientobj=varientsaray.getJSONObject(k);
                                            productVarients.setId(varientobj.getString("id"));
                                            productVarients.setItem_id(varientobj.getString("item_id"));
                                            productVarients.setSection_id(varientobj.getString("section_id"));
                                            productVarients.setSection_item_id(varientobj.getString("section_item_id"));
                                            productVarients.setSku(varientobj.getString("sku"));
                                            productVarients.setPrice(varientobj.getString("price"));
                                            productVarients.setStock(varientobj.getString("stock"));
                                            productVarients.setDiscount(varientobj.getString("discount"));
                                            productVarients.setList_id(varientobj.getString("list_id"));
                                            productVarients.setVendor_user_id(varientobj.getString("vendor_user_id"));
                                            productVarients.setStatus(varientobj.getString("status"));

                                            JSONObject itemobj=varientobj.getJSONObject("section_item");

                                            productVarients.setSection_item_name(itemobj.getString("name"));
                                            productVarients.setWeight(itemobj.getString("weight"));
                                            if (varientobj.getString("price").equalsIgnoreCase(""+jsonObject1.getString("min_price"))){
                                                productVarients.setChecked(true);
                                                st_weight=jsonObject1.getString("min_price");
                                            }
                                            productvarientlist.add(productVarients);
                                        }
                                        productsModel.setProductvarientlist(productvarientlist);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    if (!jsonObject1.getString("id").equalsIgnoreCase(product_id)){
                                        productlist.add(productsModel);
                                    }

                                }
                            }

                            relatedproducts.setrefresh(productlist);

                        }else {
                            System.out.println("aaaaaaaa  else");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void setrelative(ProductsModel productsModel) {
        product_id=productsModel.getId();
        productsFetcher();

    }

    public void setimagerefresh(int position,ImagesModel imagesModel) {
        for (int i=0;i<imageslist.size();i++){
            imageslist.get(i).setIsselect(false);
        }
        imageslist.get(position).setIsselect(true);
        Glide.with(mContext)
                .load(imagesModel.getImage())
                .placeholder(R.drawable.loader_gif)
                .into(img_product);
        selectedimage=imagesModel.getImage();
        imagesAdapter.setrefresh(imageslist);
    }
}