package com.apticks.nextclickuser.products.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.ChangeAddressActivity;
import com.apticks.nextclickuser.products.activities.PlacesAddActivity;
import com.apticks.nextclickuser.products.model.SaveAddress;
import com.mapbox.mapboxsdk.style.layers.Property;

import java.util.ArrayList;

public class SaveAddressAdapter extends RecyclerView.Adapter<SaveAddressAdapter.Myvieholder> {


    private Context context;
    String category[],add[];
    boolean check=false;
    int whichclass;
    ArrayList<SaveAddress> addresslist;

    public SaveAddressAdapter(Context context, ArrayList<SaveAddress> addresslist) {
        this.context= context;
        this.addresslist=addresslist;
    }

    @Override
    public SaveAddressAdapter.Myvieholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View var3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_layout, (ViewGroup) null);

        return new SaveAddressAdapter.Myvieholder(var3);
    }
    @Override
    public void onBindViewHolder(final SaveAddressAdapter.Myvieholder holder, final int position) {

        holder.address.setText(addresslist.get(position).getAddress());
        holder.tv_mobileno.setText(""+addresslist.get(position).getPhone());
        holder.tv_email.setText(""+addresslist.get(position).getEmail());

        /*
        holder.tv_mobileno.setText("Mobile No : "+addresslist.get(position).getPhone());
        holder.tv_email.setText("Email : "+addresslist.get(position).getEmail());
         */


        String name= addresslist.get(position).getName().substring(0, 1).toUpperCase() + addresslist.get(position).getName().substring(1);

        holder.tv_namepostcode.setText(name.toUpperCase()+", "+addresslist.get(position).getPincode());

        if (addresslist.get(position).getStatuscheck().equalsIgnoreCase("1")){
            holder.layoutvisible.setBackgroundColor(context.getResources().getColor(R.color.Trello_Orange_100));
        }else {
            holder.layoutvisible.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        holder.visiblelayout.setVisibility(View.VISIBLE);

            holder.remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("aaaaaaa delete click ");
                    ((ChangeAddressActivity)context).setDelete(addresslist.get(position),position);
                }
            });
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, PlacesAddActivity.class);
                    intent.putExtra("data", addresslist.get(position));
                    intent.putExtra("whichposition", 2);
                    context.startActivity(intent);
                  /*  ((SavedAdddress)context).setEdit(saveaddresslist,position);
                       ((SavedAdddress)context).setEdit(saveaddresslist.get(position).getAddress(),saveaddresslist.get(position).getLatitude(),
                               saveaddresslist.get(position).getLongitude(),saveaddresslist.get(position).getId());*/
                }
            });

       /* if (addresslist.get(position).getStatus().equals("1")){
            holder.rb_check.setChecked(true);
        }else {
            holder.rb_check.setChecked(false);
        }*/
        holder.layoutvisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i=0;i<addresslist.size();i++){
                    addresslist.get(i).setStatuscheck("0");
                }
                addresslist.get(position).setStatuscheck("1");
               // notifyDataSetChanged();
                ((ChangeAddressActivity)context).saveAddressClick(addresslist.get(position));
            }
        });
    }
    @Override
    public int getItemCount() {
        return addresslist.size();
    }
    public void delete(){
        notifyDataSetChanged();
    }

    public void setChange(ArrayList<SaveAddress> addresslist) {
        this.addresslist=addresslist;
        notifyDataSetChanged();
    }

    public class Myvieholder extends RecyclerView.ViewHolder {
        public TextView address,remove,edit,tv_namepostcode,tv_mobileno,tv_email;
        LinearLayout visiblelayout,layoutvisible;


        public Myvieholder(View view) {
            super(view);
            address = view.findViewById(R.id.address);
            tv_namepostcode = view.findViewById(R.id.tv_namepostcode);
            remove = view.findViewById(R.id.remove);
            edit = view.findViewById(R.id.edit);
            visiblelayout = view.findViewById(R.id.visiblelayout);
            layoutvisible = view.findViewById(R.id.layoutvisible);
            tv_mobileno = view.findViewById(R.id.tv_mobileno);
            tv_email = view.findViewById(R.id.tv_email);
        }
    }

}
