package com.apticks.nextclickuser.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apticks.nextclickuser.Fragments.CartEcomerce;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutNewPageActivity;
import com.apticks.nextclickuser.checkout.models.Products;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.apticks.nextclickuser.products.activities.ChangeAddressActivity;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.SaveAddress;


public class RemoveProduct {
    private Context context;
    private Dialog dialog;
    private RelativeLayout main;
    private TextView tv_cancel,tv_remove,tv_productname,tv_title;
    private int whichactivity;


    public RemoveProduct(Context ctx, int whichactivity, CartModel cartModel) {
        this.context = ctx;
        this.whichactivity=whichactivity;

        dialog = new Dialog(context);
        //note_list=new ArrayList<>();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.remove_product);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        tv_remove=dialog.findViewById(R.id.tv_remove);
        tv_cancel=dialog.findViewById(R.id.tv_cancel);
        tv_productname=dialog.findViewById(R.id.tv_productname);

        tv_productname.setText("Are you sure do you want to remove "+cartModel.getItem_id());
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });tv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public RemoveProduct(Context context, CartModel cartModel,int whichactivity) {
        this.context = context;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.remove_product);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        tv_remove=dialog.findViewById(R.id.tv_remove);
        tv_cancel=dialog.findViewById(R.id.tv_cancel);
        tv_productname=dialog.findViewById(R.id.tv_productname);

        String sourceString = "Are you sure do you want to remove <b> "+ cartModel.getProductCartModel().getName() + "</b> ";
        tv_productname.setText(Html.fromHtml(sourceString));


       // tv_productname.setText("Are You Sure Want To Remove "+cartModel.getProductCartModel().getName());
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });tv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (whichactivity==0){
                    ((CartAddActivity)context).removeProduct(cartModel);
                }else {
                    ((CheckOutNewPageActivity)context).removeProduct(cartModel);
                }


            }
        });
    }

    public RemoveProduct(ChangeAddressActivity context, SaveAddress saveAddress) {
        this.context = context;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.remove_product);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        tv_remove=dialog.findViewById(R.id.tv_remove);
        tv_cancel=dialog.findViewById(R.id.tv_cancel);
        tv_productname=dialog.findViewById(R.id.tv_productname);
        tv_title=dialog.findViewById(R.id.tv_title);

        String sourceString = " Remove "+"<b>" + saveAddress.getName() + "</b> ";
        tv_title.setText(Html.fromHtml(sourceString));

        String sourceString1 = "Are You Sure Want To Remove "+"<b>" + saveAddress.getAddress() + "</b> ";
        tv_productname.setText(Html.fromHtml(sourceString1));

      //  tv_title.setText("Remove "+saveAddress.getName());
       // tv_productname.setText("Are You Sure Want To Remove this address /n "+saveAddress.getAddress());
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });tv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((ChangeAddressActivity)context).removeProduct(saveAddress);

            }
        });
    }

    public RemoveProduct(Context context, CartModel cartModel, CartEcomerce cartEcomerce) {
        this.context = context;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.remove_product);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        tv_remove=dialog.findViewById(R.id.tv_remove);
        tv_cancel=dialog.findViewById(R.id.tv_cancel);
        tv_productname=dialog.findViewById(R.id.tv_productname);

        String sourceString = "Are You Sure Want To Remove <b> "+ cartModel.getProductCartModel().getName() + "</b> ";
        tv_productname.setText(Html.fromHtml(sourceString));

     //   tv_productname.setText("Are You Sure Want To Remove "+cartModel.getProductCartModel().getName());
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });tv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                cartEcomerce.removeProduct(cartModel);

            }
        });
    }

    public void showDialog() {
        dialog.show();
        dialog.setCancelable(false);
    }
    public void cancle(){
        dialog.dismiss();
    }

}
