package com.apticks.nextclickuser.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apticks.nextclickuser.Fragments.CartEcomerce;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.OrderHistoryActivity;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.apticks.nextclickuser.products.activities.ChangeAddressActivity;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.SaveAddress;


public class Filterorder {
    private Context context;
    private Dialog dialog;
    private ImageView img_cancel;

    private TextView tv_today,tv_yesterday,tv_lastweek,tv_lastmonth,tv_this_year,tv_last_year;

    public Filterorder(Context ctx) {
        this.context = ctx;

        dialog = new Dialog(context);
        //note_list=new ArrayList<>();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.filter_order);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        tv_yesterday=dialog.findViewById(R.id.tv_yesterday);
        tv_today=dialog.findViewById(R.id.tv_today);
        tv_lastweek=dialog.findViewById(R.id.tv_lastweek);
        tv_lastmonth=dialog.findViewById(R.id.tv_lastmonth);
        tv_this_year=dialog.findViewById(R.id.tv_this_year);
        tv_last_year=dialog.findViewById(R.id.tv_last_year);
        img_cancel=dialog.findViewById(R.id.img_cancel);

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        tv_today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                ((OrderHistoryActivity)context).getOrders("0","0","0");
            }
        });

        tv_yesterday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                ((OrderHistoryActivity)context).getOrders("1","0","0");
            }
        });

        tv_lastweek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                ((OrderHistoryActivity)context).getOrders("7","0","0");
            }
        });

        tv_lastmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                ((OrderHistoryActivity)context).getOrders("31","0","0");
            }
        });

        tv_this_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                ((OrderHistoryActivity)context).getOrders("365","0","0");
            }
        });

        tv_last_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                ((OrderHistoryActivity)context).getOrders("1","1","0");
            }
        });

    }


    public void showDialog() {
        dialog.show();
        dialog.setCancelable(false);
    }
    public void cancle(){
        dialog.dismiss();
    }

}
