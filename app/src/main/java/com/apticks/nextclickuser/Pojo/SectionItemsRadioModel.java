package com.apticks.nextclickuser.Pojo;

import com.apticks.nextclickuser.Activities.addtocart.SecItems;

import java.util.ArrayList;

public class SectionItemsRadioModel {
    private String id;
    private String name;
    private String desc;
    private int price;
    private String status;
    private String image;
    private String secStatus;
    private String itemField;
    private String required;
    private int secPrice;

    ArrayList<SecItems> inneritemslist;

    public int getSecPrice() {
        return secPrice;
    }

    public void setSecPrice(int secPrice) {
        this.secPrice = secPrice;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getItemField() {
        return itemField;
    }

    public void setItemField(String itemField) {
        this.itemField = itemField;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSecStatus() {
        return secStatus;
    }

    public void setSecStatus(String secStatus) {
        this.secStatus = secStatus;
    }

    public ArrayList<SecItems> getInneritemslist() {
        return inneritemslist;
    }

    public void setInneritemslist(ArrayList<SecItems> inneritemslist) {
        this.inneritemslist = inneritemslist;
    }
}
