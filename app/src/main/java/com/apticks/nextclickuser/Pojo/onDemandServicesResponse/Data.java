package com.apticks.nextclickuser.Pojo.onDemandServicesResponse;

import com.apticks.nextclickuser.Pojo.doctorDetailsResponse.ServiceTiming;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("od_service_id")
    @Expose
    private Integer odServiceId;
    @SerializedName("od_cat_id")
    @Expose
    private Integer odCatId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("service_duration")
    @Expose
    private Integer serviceDuration;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("od_category")
    @Expose
    private OdCategory odCategory;
    @SerializedName("service_timings")
    @Expose
    private LinkedList<ServiceTiming> listOfServiceTiming;
    @SerializedName("image")
    @Expose
    private String image;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOdServiceId() {
        return odServiceId;
    }

    public void setOdServiceId(Integer odServiceId) {
        this.odServiceId = odServiceId;
    }

    public Integer getOdCatId() {
        return odCatId;
    }

    public void setOdCatId(Integer odCatId) {
        this.odCatId = odCatId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getServiceDuration() {
        return serviceDuration;
    }

    public void setServiceDuration(Integer serviceDuration) {
        this.serviceDuration = serviceDuration;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public OdCategory getOdCategory() {
        return odCategory;
    }

    public void setOdCategory(OdCategory odCategory) {
        this.odCategory = odCategory;
    }

    public LinkedList<ServiceTiming> getListOfServiceTiming() {
        return listOfServiceTiming;
    }

    public void setListOfServiceTiming(LinkedList<ServiceTiming> listOfServiceTiming) {
        this.listOfServiceTiming = listOfServiceTiming;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
