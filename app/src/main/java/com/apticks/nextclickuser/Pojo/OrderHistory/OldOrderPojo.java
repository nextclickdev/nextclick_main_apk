package com.apticks.nextclickuser.Pojo.OrderHistory;

public class OldOrderPojo {
    int id;
    int Discount;
    int Delivery_fee;
    int Tax;
    int Total;
    int Deal_id;
    int Order_track;
    String Order_status;
    String created_at;
    private String name;
    private String vendorEmailId = "";
    private String vendorMobileNumber = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiscount() {
        return Discount;
    }

    public void setDiscount(int discount) {
        Discount = discount;
    }

    public int getDelivery_fee() {
        return Delivery_fee;
    }

    public void setDelivery_fee(int delivery_fee) {
        Delivery_fee = delivery_fee;
    }

    public int getTax() {
        return Tax;
    }

    public void setTax(int tax) {
        Tax = tax;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int total) {
        Total = total;
    }

    public int getDeal_id() {
        return Deal_id;
    }

    public void setDeal_id(int deal_id) {
        Deal_id = deal_id;
    }

    public int getOrder_track() {
        return Order_track;
    }

    public void setOrder_track(int order_track) {
        Order_track = order_track;
    }

    public String getOrder_status() {
        return Order_status;
    }

    public void setOrder_status(String order_status) {
        Order_status = order_status;
    }

    public String getVendorEmailId() {
        return vendorEmailId;
    }

    public void setVendorEmailId(String vendorEmailId) {
        this.vendorEmailId = vendorEmailId;
    }

    public String getVendorMobileNumber() {
        return vendorMobileNumber;
    }

    public void setVendorMobileNumber(String vendorMobileNumber) {
        this.vendorMobileNumber = vendorMobileNumber;
    }
}


