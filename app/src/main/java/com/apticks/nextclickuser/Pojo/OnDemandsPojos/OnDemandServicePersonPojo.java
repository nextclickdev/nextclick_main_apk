package com.apticks.nextclickuser.Pojo.OnDemandsPojos;

import java.util.ArrayList;

public class OnDemandServicePersonPojo {

    String id;
    String vendor_id;
    String od_cat_id;
    String name;
    String desc;
    String service_duration;
    int price;
    int discount;
    int status;
    String od_category_name;
    ArrayList<String> service_timing_start;
    ArrayList<String> service_timing_end;
    String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getOd_cat_id() {
        return od_cat_id;
    }

    public void setOd_cat_id(String od_cat_id) {
        this.od_cat_id = od_cat_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getService_duration() {
        return service_duration;
    }

    public void setService_duration(String service_duration) {
        this.service_duration = service_duration;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getOd_category_name() {
        return od_category_name;
    }

    public void setOd_category_name(String od_category_name) {
        this.od_category_name = od_category_name;
    }

    public ArrayList<String> getService_timing_start() {
        return service_timing_start;
    }

    public void setService_timing_start(ArrayList<String> service_timing_start) {
        this.service_timing_start = service_timing_start;
    }

    public ArrayList<String> getService_timing_end() {
        return service_timing_end;
    }

    public void setService_timing_end(ArrayList<String> service_timing_end) {
        this.service_timing_end = service_timing_end;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
