package com.apticks.nextclickuser.Pojo.VENDORSPOJO;

import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;

import java.util.ArrayList;

public class VendorsPojo {

    String id;
    String name;
    String email;
    String address;
    String landmark;
    String image;
    String is_having_lead;
    Double lattitude;
    Double longitude;
    int vendor_user_id;
    double distance;
    int availability ;
    private ArrayList<ServicesPojo> serviceList;

    public int getVendor_user_id() {
        return vendor_user_id;
    }

    public void setVendor_user_id(int vendor_user_id) {
        this.vendor_user_id = vendor_user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }

    public ArrayList<ServicesPojo> getServiceList() {
        return serviceList;
    }

    public void setServiceList(ArrayList<ServicesPojo> serviceList) {
        this.serviceList = serviceList;
    }

    public String getIs_having_lead() {
        return is_having_lead;
    }

    public void setIs_having_lead(String is_having_lead) {
        this.is_having_lead = is_having_lead;
    }
}
