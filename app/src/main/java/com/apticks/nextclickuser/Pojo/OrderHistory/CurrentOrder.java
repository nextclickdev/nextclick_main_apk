package com.apticks.nextclickuser.Pojo.OrderHistory;

import java.util.ArrayList;

public class CurrentOrder {
    private String order_id;
    private String order_track;
    private double discount;
    private double deliveryPrice;
    private String orderStatus;
    private double tax;
    private double total;
    private int priceOrderItems;
    private int quantityOrderItems;
    private double priceSubOrderItems;
    private int quantitySubOrderItems;
    private String created_at;
    private String otp;
    private String name = "";
    private String vendorEmailId = "";
    private String vendorMobileNumber = "";


    private ArrayList<CurrentOrder> currentOrdersArrayListInnerItems = new ArrayList<>();
    private ArrayList<CurrentOrder> currentOrdersArrayListInnerSubItems = new ArrayList<>();

    public ArrayList<CurrentOrder> getCurrentOrdersArrayListInnerSubItems() {
        return currentOrdersArrayListInnerSubItems;
    }

    public void setCurrentOrdersArrayListInnerSubItems(ArrayList<CurrentOrder> currentOrdersArrayListInnerSubItems) {
        this.currentOrdersArrayListInnerSubItems = currentOrdersArrayListInnerSubItems;
    }

    public ArrayList<CurrentOrder> getCurrentOrdersArrayListInnerItems() {
        return currentOrdersArrayListInnerItems;
    }

    public void setCurrentOrdersArrayListInnerItems(ArrayList<CurrentOrder> currentOrdersArrayListInnerItems) {
        this.currentOrdersArrayListInnerItems = currentOrdersArrayListInnerItems;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getOrder_track() {
        return order_track;
    }

    public void setOrder_track(String order_track) {
        this.order_track = order_track;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(double deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getPriceOrderItems() {
        return priceOrderItems;
    }

    public void setPriceOrderItems(int priceOrderItems) {
        this.priceOrderItems = priceOrderItems;
    }

    public int getQuantityOrderItems() {
        return quantityOrderItems;
    }

    public void setQuantityOrderItems(int quantityOrderItems) {
        this.quantityOrderItems = quantityOrderItems;
    }

    public double getPriceSubOrderItems() {
        return priceSubOrderItems;
    }

    public void setPriceSubOrderItems(double priceSubOrderItems) {
        this.priceSubOrderItems = priceSubOrderItems;
    }

    public int getQuantitySubOrderItems() {
        return quantitySubOrderItems;
    }

    public void setQuantitySubOrderItems(int quantitySubOrderItems) {
        this.quantitySubOrderItems = quantitySubOrderItems;
    }

    public String getVendorEmailId() {
        return vendorEmailId;
    }

    public void setVendorEmailId(String vendorEmailId) {
        this.vendorEmailId = vendorEmailId;
    }

    public String getVendorMobileNumber() {
        return vendorMobileNumber;
    }

    public void setVendorMobileNumber(String vendorMobileNumber) {
        this.vendorMobileNumber = vendorMobileNumber;
    }
}
