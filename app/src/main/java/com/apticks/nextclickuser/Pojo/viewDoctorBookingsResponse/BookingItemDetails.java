package com.apticks.nextclickuser.Pojo.viewDoctorBookingsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingItemDetails {

    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("service_id")
    @Expose
    private Integer serviceId;
    @SerializedName("service_item_id")
    @Expose
    private Integer serviceItemId;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("booking_date")
    @Expose
    private String bookingDate;
    @SerializedName("service_timing_id")
    @Expose
    private Integer serviceTimingId;
    @SerializedName("service_item")
    @Expose
    private ServiceItem serviceItem;
    @SerializedName("service_timings")
    @Expose
    private ServiceTimings serviceTimings;


    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getServiceItemId() {
        return serviceItemId;
    }

    public void setServiceItemId(Integer serviceItemId) {
        this.serviceItemId = serviceItemId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Integer getServiceTimingId() {
        return serviceTimingId;
    }

    public void setServiceTimingId(Integer serviceTimingId) {
        this.serviceTimingId = serviceTimingId;
    }

    public ServiceItem getServiceItem() {
        return serviceItem;
    }

    public void setServiceItem(ServiceItem serviceItem) {
        this.serviceItem = serviceItem;
    }

    public ServiceTimings getServiceTimings() {
        return serviceTimings;
    }

    public void setServiceTimings(ServiceTimings serviceTimings) {
        this.serviceTimings = serviceTimings;
    }
}
