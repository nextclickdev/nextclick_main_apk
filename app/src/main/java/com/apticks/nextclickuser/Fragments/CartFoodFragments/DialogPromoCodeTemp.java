package com.apticks.nextclickuser.Fragments.CartFoodFragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.CartFoodAdapter.PromoCodeAdapter;
import com.apticks.nextclickuser.Pojo.CartPojoModel.DialogPromoModel;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.DataBaseHelper;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.Food_Promo;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class DialogPromoCodeTemp extends DialogFragment implements View.OnClickListener {
    RecyclerView recyclerViewPromoCode;
    ImageView imageback;
    EditText promo_code;
    TextView apply;
    Animation animation;
    private DataBaseHelper dataBaseHelper;
    Context mContext;
    Activity activity;
    ArrayList<DialogPromoModel> arrayListDialogPromoModel;
    View root;
    OnHeadlineSelectedListener callback;

    PreferenceManager preferenceManager;
    String token;

    public static DialogPromoCodeTemp newInstance(){

        DialogPromoCodeTemp dialogPromoCodeTemp = new DialogPromoCodeTemp();

        return dialogPromoCodeTemp;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.dialog_promo_code,container,false);

        //return super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
        initView();
        arrayListDialogPromoModel = new ArrayList<>();
        dataBaseHelper = new DataBaseHelper(mContext);

        imageback.setOnClickListener(this);
        apply.setOnClickListener(this);
        recyclerPromoCode();

    }

    private void recyclerPromoCode() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Food_Promo, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response!=null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if(jsonObject.getBoolean("status")){
                            JSONArray jsonArraydata = jsonObject.getJSONArray("data");

                            for (int i = 0; i<=jsonArraydata.length();i++){
                                JSONObject jsonObjectData = jsonArraydata.getJSONObject(i);
                                DialogPromoModel dialogPromoModel = new DialogPromoModel();
                                dialogPromoModel.setId(jsonObjectData.getInt("id"));
                                dialogPromoModel.setPromo_title(jsonObjectData.getString("promo_title"));
                                dialogPromoModel.setPromo_code(jsonObjectData.getString("promoCode"));
                                dialogPromoModel.setPromo_type(jsonObjectData.getInt("promo_type"));
                                dialogPromoModel.setPromo_label(jsonObjectData.getString("promo_label"));
                                dialogPromoModel.setValid_from(jsonObjectData.getString("valid_from"));
                                dialogPromoModel.setValid_to(jsonObjectData.getString("valid_to"));
                                dialogPromoModel.setDiscount_type(jsonObjectData.getInt("discount_type"));
                                dialogPromoModel.setDiscount(jsonObjectData.getInt("discount"));
                                dialogPromoModel.setUses(jsonObjectData.getInt("uses"));
                                dialogPromoModel.setStatus(jsonObjectData.getInt("status"));
                                arrayListDialogPromoModel.add(dialogPromoModel);
                            }

                            checkRecyclerPromoCode();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        checkRecyclerPromoCode();
                    }
                }
                //PromoCodeAdapter
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(mContext, ""+error , Toast.LENGTH_SHORT).show();
                checkRecyclerPromoCode();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", token);


                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void checkRecyclerPromoCode() {

        if (arrayListDialogPromoModel.size()!=0){
            recyclerViewPromoCode.setVisibility(View.VISIBLE);
            PromoCodeAdapter promoCodeAdapter = new PromoCodeAdapter(arrayListDialogPromoModel, promo_code);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                @Override
                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                        private static final float SPEED = 300f;// Change this value (default=25f)

                        @Override
                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                            return SPEED / displayMetrics.densityDpi;
                        }

                    };
                    smoothScroller.setTargetPosition(position);
                    startSmoothScroll(smoothScroller);
                }

            };
            recyclerViewPromoCode.setLayoutManager(layoutManager);
            recyclerViewPromoCode.setAdapter(promoCodeAdapter);
        }else {
            recyclerViewPromoCode.setVisibility(View.GONE);
        }

    }

    private void initView() {

        recyclerViewPromoCode = root.findViewById(R.id.recycler_promo);
        imageback = root.findViewById(R.id.imageback);
        promo_code = root.findViewById(R.id.et_promo_code);
        apply =root.findViewById(R.id.apply);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.imageback):
            {
                dismiss();
                return;
            }
            case(R.id.apply):
            {
                //Toast.makeText(mContext, "Apply", Toast.LENGTH_SHORT).show();
                try {
                    EditNameDialogListenerTemp editNameDialogListenerTemp
                            = (EditNameDialogListenerTemp)getActivity();
                    editNameDialogListenerTemp.onFinishEditDialog("Some Text");
                } catch (ClassCastException e) {
                    e.printStackTrace();
                    //throw new ClassCastException(mContext.toString() + "Needed");
                    //Toast.makeText(mContext, "Exception  " + e, Toast.LENGTH_SHORT).show();
                }

                return;
            }
        }
    }

    public interface EditNameDialogListenerTemp{
        void onFinishEditDialog(String inputText);
    }

    public void setOnHeadlineSelectedListener(OnHeadlineSelectedListener callback) {
        this.callback = callback;
    }
    public interface OnHeadlineSelectedListener {
        public void onArticleSelected(int position);
    }
}
