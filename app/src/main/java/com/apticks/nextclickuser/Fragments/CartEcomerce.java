package com.apticks.nextclickuser.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.ConfirmPayment;
import com.apticks.nextclickuser.Adapters.ItemsCartEcommerceAdapter;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.CartPojoModel.CartPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.apticks.nextclickuser.products.adapters.CartAdapter;
import com.apticks.nextclickuser.products.adapters.WishlistAdapter;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.ProductCartModel;
import com.apticks.nextclickuser.products.model.ProductImages;
import com.apticks.nextclickuser.products.model.VarientDetails;
import com.apticks.nextclickuser.products.model.VendorModel;
import com.apticks.nextclickuser.products.model.VendorvarientProducts;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CARTCHANGESTATUS;
import static com.apticks.nextclickuser.Config.Config.CARTDELETE;
import static com.apticks.nextclickuser.Config.Config.CARTREAD;
import static com.apticks.nextclickuser.Config.Config.ECOM_CART_R;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


public class CartEcomerce extends Fragment {

    RecyclerView recycler_cart_items,recycler_wishlist_items;
    private TextView tv_checkout;
    private ImageView img_back;
    RelativeLayout layout_cart,layout_wishlist;
    private ArrayList<CartModel> cartlist;
    private ArrayList<CartModel> wishlist;
    private CartAdapter cartAdapter;
    private WishlistAdapter wishlistAdapter;
    private CustomDialog customDialog;
    private Context mContext;
    private RelativeLayout relative_empty_cart;
    PreferenceManager preferenceManager;
    private RelativeLayout layout_actionbar;
    View view;

    public CartEcomerce(Context applicationContext) {
        mContext=applicationContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_cart_add, container, false);

        initView();

        return view;

    }


    private void initView() {

        recycler_cart_items = view.findViewById(R.id.recycler_cart_items);
        recycler_wishlist_items = view.findViewById(R.id.recycler_wishlist_items);
        layout_cart =view. findViewById(R.id.layout_cart);
        layout_wishlist = view.findViewById(R.id.layout_wishlist);
        img_back = view.findViewById(R.id.img_back);
        tv_checkout = view.findViewById(R.id.tv_checkout);
        relative_empty_cart =view. findViewById(R.id.relative_empty_cart);
        //layout_actionbar =view. findViewById(R.id.layout_actionbar);
        //  layout_actionbar.setVisibility(View.GONE);

        customDialog=new CustomDialog(getContext());
        cartlist=new ArrayList<>();
        wishlist=new ArrayList<>();
        preferenceManager=new PreferenceManager(mContext);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);


        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);


        cartAdapter=new CartAdapter(getContext(),cartlist,1,CartEcomerce.this);

        wishlistAdapter=new WishlistAdapter(getContext(),wishlist,1,CartEcomerce.this);

        recycler_wishlist_items.setAdapter(wishlistAdapter);
        recycler_wishlist_items.setLayoutManager(linearLayoutManager);

        recycler_cart_items.setAdapter(cartAdapter);
        recycler_cart_items.setLayoutManager(layoutManager);

        cartFetcher();


        tv_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson=new Gson();
                String cartliststring = gson.toJson(cartlist);
                Intent intent=new Intent(getContext(), CheckOutPageActivity.class);
                intent.putExtra("cartlist",cartliststring);
                startActivity(intent);
            }
        });
    }

    private void cartFetcher() {
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTREAD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    customDialog.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 200) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            Log.v("data",jsonArray.toString());
                            for (int i=0;i<jsonArray.length();i++){

                                JSONObject jsonObject1=jsonArray.getJSONObject(i);

                                CartModel cartModel=new CartModel();

                                cartModel.setId(jsonObject1.getString("id"));
                                cartModel.setVendor_user_id(jsonObject1.getString("vendor_user_id"));
                                cartModel.setItem_id(jsonObject1.getString("item_id"));
                                cartModel.setQty(jsonObject1.getString("qty"));
                                cartModel.setVendor_product_varinat_id(jsonObject1.getString("vendor_product_variant_id"));
                                cartModel.setStatus(jsonObject1.getString("status"));

                                VendorModel vendorModel=new VendorModel();

                                vendorModel.setId(jsonObject1.getJSONObject("vendor").getString("id"));
                                vendorModel.setVendor_user_id(jsonObject1.getJSONObject("vendor").getString("vendor_user_id"));
                                vendorModel.setName(jsonObject1.getJSONObject("vendor").getString("name"));
                                vendorModel.setDesc(jsonObject1.getJSONObject("vendor").getString("desc"));

                                cartModel.setVendorModel(vendorModel);

                                ProductCartModel productCartModel=new ProductCartModel();
                                productCartModel.setId(jsonObject1.getJSONObject("item").getString("id"));
                                productCartModel.setName(jsonObject1.getJSONObject("item").getString("name"));
                                productCartModel.setDesc(jsonObject1.getJSONObject("item").getString("desc"));

                                cartModel.setProductCartModel(productCartModel);

                                VendorvarientProducts vendorvarientProducts=new VendorvarientProducts();
                                vendorvarientProducts.setId(jsonObject1.getJSONObject("vendor_product_variant").getString("id"));
                                vendorvarientProducts.setSection_item_id(jsonObject1.getJSONObject("vendor_product_variant").getString("section_item_id"));
                                vendorvarientProducts.setStock(jsonObject1.getJSONObject("vendor_product_variant").getString("stock"));
                                vendorvarientProducts.setPrice(jsonObject1.getJSONObject("vendor_product_variant").getString("price"));
                                vendorvarientProducts.setDiscount(jsonObject1.getJSONObject("vendor_product_variant").getString("discount"));
                                vendorvarientProducts.setStatus(jsonObject1.getJSONObject("vendor_product_variant").getString("status"));

                                VarientDetails varientDetails=new VarientDetails();

                                varientDetails.setName(jsonObject1.getJSONObject("vendor_product_variant").getJSONObject("details").getString("name"));
                                varientDetails.setWeight(jsonObject1.getJSONObject("vendor_product_variant").getJSONObject("details").getString("weight"));

                                vendorvarientProducts.setVarientDetails(varientDetails);
                                cartModel.setVendorvarientProducts(vendorvarientProducts);


                                ProductImages productImages=new ProductImages();

                                JSONArray imagesarrsy=jsonObject1.getJSONArray("item_images");
                                ArrayList<ProductImages> productimagelist=new ArrayList<>();
                                for (int k=0;k<imagesarrsy.length();k++){
                                    JSONObject imageobject=imagesarrsy.getJSONObject(k);
                                    productImages.setId(imageobject.getString("id"));
                                    productImages.setItem_id(imageobject.getString("item_id"));
                                    productImages.setExt(imageobject.getString("ext"));
                                    productImages.setImage(imageobject.getString("image"));
                                    productimagelist.add(productImages);
                                }

                                cartModel.setProductImages(productimagelist);

                                if (cartModel.getStatus().equalsIgnoreCase("1")){
                                    cartlist.add(cartModel);
                                }else {
                                    wishlist.add(cartModel);
                                }


                            }


                            if (cartlist.size()!=0){
                                relative_empty_cart.setVisibility(View.GONE);
                                tv_checkout.setVisibility(View.VISIBLE);
                                recycler_cart_items.setVisibility(View.VISIBLE);
                                cartAdapter.setrefresh(cartlist);
                                //  recycler_cart_items.setAdapter(cartAdapter);
                            }else {
                                relative_empty_cart.setVisibility(View.VISIBLE);
                                tv_checkout.setVisibility(View.GONE);
                                recycler_cart_items.setVisibility(View.GONE);
                            }

                            if (wishlist.size()!=0){
                                recycler_wishlist_items.setVisibility(View.GONE);
                                recycler_wishlist_items.setVisibility(View.VISIBLE);
                                wishlistAdapter.setrefresh(wishlist);
                                //  recycler_cart_items.setAdapter(cartAdapter);
                            }else {
                                //  relative_empty_cart.setVisibility(View.VISIBLE);
                                recycler_wishlist_items.setVisibility(View.GONE);
                            }

                        }else {
                            relative_empty_cart.setVisibility(View.VISIBLE);
                            tv_checkout.setVisibility(View.GONE);
                            recycler_cart_items.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        relative_empty_cart.setVisibility(View.VISIBLE);
                        tv_checkout.setVisibility(View.GONE);
                        recycler_cart_items.setVisibility(View.GONE);
                        e.printStackTrace();
                        System.out.println("aaaaaaaaa catch "+e.getMessage());
                    }
                }else {
                    customDialog.dismiss();
                    relative_empty_cart.setVisibility(View.VISIBLE);
                    tv_checkout.setVisibility(View.GONE);
                    recycler_cart_items.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                relative_empty_cart.setVisibility(View.VISIBLE);
                tv_checkout.setVisibility(View.GONE);
                recycler_cart_items.setVisibility(View.GONE);
                System.out.println("aaaaaaaaa error "+error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void removeProduct(CartModel cartModel) {
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTDELETE+"/"+cartModel.getId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("aaaaaaa response " + jsonObject.toString());
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {

                        System.out.println("aaaaaaaaaa  response  "+response.toString());
                        cartlist.remove(cartModel);
                        if (cartlist.size()==0){
                            relative_empty_cart.setVisibility(View.VISIBLE);
                            tv_checkout.setVisibility(View.GONE);
                            recycler_cart_items.setVisibility(View.GONE);
                        }else {
                            cartAdapter.setrefresh(cartlist);
                            relative_empty_cart.setVisibility(View.GONE);
                            tv_checkout.setVisibility(View.VISIBLE);
                            recycler_cart_items.setVisibility(View.VISIBLE);
                        }


                    }
                }catch (JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }
        };
        requestQueue.add(stringRequest);
    }

    public void saveLatter(CartModel cartModel,String status) {
        cartChangeestatus(cartModel,status);
    }
    public void cartChangeestatus(CartModel cartModel,String status) {
        customDialog.show();
        Map<String,String> countMap = new HashMap<>();
        countMap.put("cart_id", cartModel.getId());
        countMap.put("status", status);

        final String data = new JSONObject(countMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTCHANGESTATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("aaaaaaa response " + jsonObject.toString());
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status /*&& http_code == 200*/) {
                        cartlist.clear();
                        wishlist.clear();
                        cartFetcher();
                    }
                }catch (JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

        };
        requestQueue.add(stringRequest);


    }
}
