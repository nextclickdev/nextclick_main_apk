package com.apticks.nextclickuser.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
//import android.app.Fragment;

import com.apticks.nextclickuser.R;

public class AddAddressFragment extends Fragment {
    Context mContext;

    public AddAddressFragment(Context mContext) {
        this.mContext = mContext;
    }

    public AddAddressFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_add_address, container, false);

        return view;
    }

    public static AddAddressFragment newInstance() {
        return new AddAddressFragment();
    }
}
