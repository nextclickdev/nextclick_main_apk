package com.apticks.nextclickuser.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.ConfirmPayment;
import com.apticks.nextclickuser.Adapters.ItemsCartEcommerceAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.CartPojoModel.CartPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.ECOM_CART_R;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EcommerceCartFragment.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link EcommerceCartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EcommerceCartFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    View view;
    Context mContext;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private TextView ecomProceedToPay;
    private RecyclerView ecomCartItemsRecycler;

    PreferenceManager preferenceManager;
    String token;

    //Array List ModelData
    ArrayList<CartPojo> data = new ArrayList<>();

    public EcommerceCartFragment(Context context) {
        // Required empty public constructor
        this.mContext=context;
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
    }

    public EcommerceCartFragment() {

    }

    // TODO: Rename and change types and number of parameters
    public static EcommerceCartFragment newInstance(String param1, String param2) {
        EcommerceCartFragment fragment = new EcommerceCartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ecommerce_cart, container, false);

        initView();

        cartReadEcommDetails();

        ecomProceedToPayClick();
        return view;
    }

    private void cartReadEcommDetails() {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ECOM_CART_R, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {
                        //UImsgs.showToast(mContext, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            //JSONObject dataJson = jsonObject.getJSONObject("status");
                            //Toast.makeText(mContext, String.valueOf(dataJson), Toast.LENGTH_SHORT).show();
                            JSONArray resultArray = jsonObject.getJSONArray("data");

                            if (resultArray.length() > 0) {
                                for (int i = 0; i < resultArray.length(); i++) {
                                    JSONObject resultObject = resultArray.getJSONObject(i);
                                    String id = resultObject.getString("id");
                                    String name = resultObject.getString("user_id");
                                    String image = resultObject.getString("product_id");
                                    String quatity = resultObject.getString("qty");
                                    JSONObject productJsonObject = resultObject.getJSONObject("product");
                                    //UImsgs.showToast(mContext, String.valueOf(resultObject.getJSONObject("product")));
                                    String productId = productJsonObject.getString("id");
                                    String productName = productJsonObject.getString("name");
                                    String productMrp = productJsonObject.getString("mrp");
                                    String productOfferPrice = productJsonObject.getString("offer_price");
                                    String productDesc = productJsonObject.getString("desc");
                                    //UImsgs.showToast(mContext, String.valueOf(productName));

                                    CartPojo categoriesData = new CartPojo();
                                    categoriesData.setId(id);
                                    categoriesData.setName(productName);
                                    categoriesData.setPrice(productMrp);
                                    categoriesData.setProductOfferPrice(productOfferPrice);
                                    categoriesData.setSpecification(productDesc);
                                    categoriesData.setQuantity(quatity);
                                    //categoriesData.setImage(image);
                                    data.add(categoriesData);
                                }
                                ItemsCartEcommerceAdapter allCategoriesAdapter = new ItemsCartEcommerceAdapter(mContext, data);
                                ecomCartItemsRecycler.setAdapter(allCategoriesAdapter);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            UImsgs.showToast(mContext,String.valueOf(e));
                        }


                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    UImsgs.showToast(mContext, error.toString());
                    Log.d("error", error.toString());
                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put("X_AUTH_TOKEN", token);


                    return map;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
    }

    private void initView() {
        //Text View
        ecomProceedToPay = view.findViewById(R.id.ecom_proceedtopay);

        //REcycle View
        ecomCartItemsRecycler = view.findViewById(R.id.ecomcart_items_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false);
        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        ecomCartItemsRecycler.setLayoutManager(linearLayoutManager);

    }

    private void ecomProceedToPayClick() {
        ecomProceedToPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ConfirmPayment.class);
                startActivity(intent);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/
    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
