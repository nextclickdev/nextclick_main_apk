package com.apticks.nextclickuser.Fragments.HMS_FRAGMENTS;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.HMS_ADAPTERS.DoctorsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.HMS_POJO.DoctorsPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.DOCTORS;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DoctorsFragment_HMS.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link DoctorsFragment_HMS#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DoctorsFragment_HMS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String vendor_Id;
    View root;
    Context context;
    private ArrayList<DoctorsPojo> doctorsList;
    RecyclerView doctorsRecycler;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DoctorsFragment_HMS() {
        // Required empty public constructor
    }

    public DoctorsFragment_HMS(String vendor_id) {
        this.vendor_Id = vendor_id;
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DoctorsFragment_HMS.
     */
    // TODO: Rename and change types and number of parameters
    public static DoctorsFragment_HMS newInstance(String param1, String param2) {
        DoctorsFragment_HMS fragment = new DoctorsFragment_HMS();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_doctors_fragment_hms, container, false);
        context = getActivity();
        doctorsRecycler = root.findViewById(R.id.doctors_recycler_hms);
        doctorsDataFetcher();
        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void doctorsDataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DOCTORS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.getClass().toString())) {
                            JSONArray dataArray = jsonObject.getJSONArray("data");
                            doctorsList = new ArrayList<>();
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObject = dataArray.getJSONObject(i);
                                DoctorsPojo doctorsPojo = new DoctorsPojo(dataObject.getString("id"), dataObject.getString("name"),
                                        dataObject.getString("qualification"), dataObject.getString("start_time"),
                                        dataObject.getString("end_time"), dataObject.getString("experience"),
                                        dataObject.getJSONArray("doctor_specialization").getJSONObject(0).getString("name"),dataObject.getString("image"));

                                doctorsList.add(doctorsPojo);

                            }

                            DoctorsAdapter doctorsAdapter = new DoctorsAdapter(context, doctorsList);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) {

                                @Override
                                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(context) {

                                        private static final float SPEED = 300f;// Change this value (default=25f)

                                        @Override
                                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                            return SPEED / displayMetrics.densityDpi;
                                        }

                                    };
                                    smoothScroller.setTargetPosition(position);
                                    startSmoothScroll(smoothScroller);
                                }

                            };
                            doctorsRecycler.setLayoutManager(layoutManager);
                            doctorsRecycler.setAdapter(doctorsAdapter);


                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(context, error.toString());
                Log.d("error", error.toString());
            }
        });

        requestQueue.add(stringRequest);

    }


}
