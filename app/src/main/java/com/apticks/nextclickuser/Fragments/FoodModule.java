package com.apticks.nextclickuser.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.CartActivity;
import com.apticks.nextclickuser.Activities.FoodActivity;
import com.apticks.nextclickuser.Adapters.FoodItemsAdapter;
import com.apticks.nextclickuser.Adapters.FoodMenuAdapter;
import com.apticks.nextclickuser.Adapters.SectionsPagerAdapter;
import com.apticks.nextclickuser.Fragments.CartFoodFragments.FoodCartFragment;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Pojo.FoodItemPojo;
import com.apticks.nextclickuser.Pojo.FoodMenuPojo;
import com.apticks.nextclickuser.R;
import com.devsmart.android.ui.HorizontalListView;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.FOOD_ITEMS;
import static com.apticks.nextclickuser.Config.Config.FOOD_MENUS;
import static com.apticks.nextclickuser.Config.Config.USERPRODUCTS;
import static com.apticks.nextclickuser.Constants.CategoriesId.foodItemTypeVegNon;
import static com.apticks.nextclickuser.Constants.CategoriesId.listGrid;
import static com.apticks.nextclickuser.Fragments.PlaceholderFragment.vegNonVeg;

//import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FoodModule.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link FoodModule#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FoodModule extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TextView proceed, restaurantName;

    Animation animation;

    SectionsPagerAdapter sectionsPagerAdapter;


    // TODO: Rename and change types of parameters
    private Context mContext;
    private String mParam1;
    private String restaurantNameText, nameREstaurant;
    private String mParam2;
    ArrayList<FoodMenuPojo> foodMenuData = new ArrayList<>();
    ArrayList<String> foodMenuDataName = new ArrayList<>();
    ArrayList<FoodItemPojo> foodItemData = new ArrayList<>();
    RecyclerView foodItemsRecycler;
    HorizontalListView foodMenuList;
    TabLayout foodiemenutablayout;
    Switch nonveg;
    View root;
    ViewPager viewpager;
    private LinearLayout veg_non_veg_linear;
    ImageView back_page;
    String str_vender;
    private int listGridIntSpanCount;
    private int limit,offset;
    //String againVendorId;
    private FoodCartFragment.OnFragmentInteractionListener mListener;

    public FoodModule() {
        // Required empty public constructor
    }

    public FoodModule(Context context, String nameREstaurant, String Vender_Data, String sub_cat_id) {
        //Required empty public constructor
        this.mContext = context;
        this.nameREstaurant = nameREstaurant;
        this.str_vender = Vender_Data;
        //this.againVendorId = str;

        Log.d("kwiky", "" + str_vender);
        //Log.e("MYVender", "" + againVendorId);

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FoodModule.
     */
    // TODO: Rename and change types and number of parameters
    public static FoodModule newInstance(String param1, String param2) {
        FoodModule fragment = new FoodModule();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            restaurantNameText = getArguments().getString("nameReastaurant");
//            vender = getArguments().getString("vendor_user_id");
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }

        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_food_module, container, false);
        back_page  = (ImageView)root.findViewById(R.id.back_page);
        back_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent i = new Intent(getActivity(), ProfileActivity.class);
                startActivity(i);*/
                getActivity().finish();
            }
        });

        if (getArguments() != null) {
            restaurantNameText = getArguments().getString("nameReastaurant");
//            vender = getArguments().getString("vendor_user_id");
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //for Setting List View Count
        if (listGrid){
            listGridIntSpanCount =1;
        }else{
            listGridIntSpanCount =2;
        }

       ViewInit();

        //Setting Visibility of Veg- Non - Veg Option
        if (foodItemTypeVegNon){
            veg_non_veg_linear.setVisibility(View.GONE);
        }else {
            veg_non_veg_linear.setVisibility(View.VISIBLE);
        }

        //All menu Available in Restaurant of Food
        FoodMenuFetcher();

        // TAB Clicked
        tabnLayoutClick();

        nonVegSwitchChange();//Switch Click for VEG -NON VEG
        //Food Item Under Selection
        FoodItemsFetcher("1", "");
        //FoodItemsFetcher(foodMenuData.get(foodMenuList.getItemIdAtPosition(0)).getId());
        foodMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                /*foodItemData.clear();
                FoodItemsFetcher(foodMenuData.get(position).getId(), "");*/
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CartActivity.class);
                intent.putExtra("Type", "Food");
                startActivity(intent);
            }
        });

        return root;
    }

   private void ViewInit(){
       //Switch
       nonveg = root.findViewById(R.id.nonveg);

       //Linear Layout
       veg_non_veg_linear = root.findViewById(R.id.veg_non_veg_linear);

       foodItemsRecycler = (RecyclerView) root.findViewById(R.id.foodItemsRecycler);
       foodMenuList = (HorizontalListView) root.findViewById(R.id.foodiemenurecycler);
       proceed = (TextView) root.findViewById(R.id.food_proceedtocart);
       restaurantName = (TextView) root.findViewById(R.id.restaurant_name);
       setNameOfRestaurant();//Name OF Restaurant

       //tab Layout
       foodiemenutablayout = root.findViewById(R.id.foodiemenutablayout);
       foodiemenutablayout.setTabMode(TabLayout.MODE_SCROLLABLE);

       //View
       viewpager = root.findViewById(R.id.viewpager);
   }

    private void setNameOfRestaurant() {
        restaurantName.setText(nameREstaurant);
    }

    private void nonVegSwitchChange() {
        {
            //foodMenuList.getSelectedItemPosition();
            //Toast.makeText(mContext, foodMenuList.getSelectedItemPosition()-1+"", Toast.LENGTH_SHORT).show();
            nonveg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        foodiemenutablayout.getSelectedTabPosition();

                        vegNonVeg = "1";
                        /*SectionsPagerAdapter */sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), getFragmentManager(), "1");

                        viewpager.setAdapter(sectionsPagerAdapter);
                        //foodiemenutablayout.setupWithViewPager(viewpager);
                        foodiemenutablayout.setupWithViewPager(viewpager);
                        //foodiemenutablayout.
                    } else {

                        vegNonVeg = "";
                        /*SectionsPagerAdapter*/ sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), getFragmentManager(), "");
                        viewpager.setAdapter(sectionsPagerAdapter);
                        //foodiemenutablayout.setupWithViewPager(viewpager);
                        foodiemenutablayout.setupWithViewPager(viewpager);
                    }
                }
            });
        }
    }

    private void tabnLayoutClick() {
        foodiemenutablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                foodItemData.clear();
                FoodItemsFetcher(foodMenuData.get(tab.getPosition()).getId(), "");

                //PlaceholderFragment.id =foodMenuData.get(tab.getPosition()).getId();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/


    public void FoodMenuFetcher() {
        SectionsPagerAdapter.foodMenuDataName.clear();

        DialogOpener.dialogOpener(mContext);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        String sub_cat_id = FoodActivity.sub_cat_id;
        Log.d("item_url",FOOD_MENUS +/*"1"*//* againVendorId*/  str_vender+"?sub_cat_id="+ sub_cat_id);
        System.out.println("aaaaaaaa urlmenufood "+FOOD_MENUS +/*"1"*//* againVendorId*/  str_vender+"?sub_cat_id="+ sub_cat_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FOOD_MENUS +/*"1"*//* againVendorId*/  str_vender+"?sub_cat_id="+ sub_cat_id , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, ""+str_vender, Toast.LENGTH_SHORT).show();
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        //JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = jsonObject.getJSONArray("data");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                String id = resultObject.getString("id");
                                String desc = resultObject.getString("desc");
                                FoodMenuPojo foodMenuPojo = new FoodMenuPojo();

                                foodMenuPojo.setName(name);
                                foodMenuPojo.setId(id);
                                foodMenuPojo.setDesc(desc);
                                foodMenuPojo.setImage(image);
                                foodMenuData.add(foodMenuPojo);

                                foodMenuDataName.add(name);
                                SectionsPagerAdapter.foodMenuDataName.add(foodMenuPojo);
                            }
                        }
                        DialogOpener.dialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                        DialogOpener.dialog.dismiss();
                        //UImsgs.showCustomToast(mContext, "Oops! Something went wrong",WARNING);
                    }
                    FoodMenuAdapter foodItemsAdapter = new FoodMenuAdapter(mContext, R.layout.menuitemsupporter, foodMenuData);
                    //foodMenuList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

                    //foodMenuList.setAdapter(foodItemsAdapter);

                    //ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity());
                    sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(), getFragmentManager());
                    viewpager.setAdapter(sectionsPagerAdapter);
                    //foodiemenutablayout.setupWithViewPager(viewpager);
                    foodiemenutablayout.setupWithViewPager(viewpager);
                    //foodiemenutablayout.

                    DialogOpener.dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                //UImsgs.showCustomToast(mContext, "Oops! Something went wrong ",ERROR);

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void FoodItemsFetcher(String menuid, String id) {
        Log.v("FOOD_ITEMS", FOOD_ITEMS + menuid + "?item_type=" + id);
        System.out.println("aaaaaaa  FOOD_ITEMS   "+FOOD_ITEMS + menuid + "?item_type=" + id);

       // DialogOpener.dialogOpener(mContext);

        String sub_cat_id = FoodActivity.sub_cat_id;
        String menu_id=menuid;
        String vendor_user_id=str_vender;

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, USERPRODUCTS + menuid + "?item_type=" + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    Log.d("FOOD_ITEMS_RES",response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("result");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                String id = resultObject.getString("id");
                                String price = resultObject.getString("price");
                                FoodItemPojo foodItemPojo = new FoodItemPojo();

                                foodItemPojo.setName(name);
                                foodItemPojo.setImage(image);
                                foodItemPojo.setId(id);
                                foodItemPojo.setPrice(price);
                                foodItemData.add(foodItemPojo);

                            }
                        }
                        //DialogOpener.dialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                        //UImsgs.showCustomToast(mContext, "Oops! Something went wrong",WARNING);
                        //DialogOpener.dialog.dismiss();
                    }

                    FoodItemsAdapter foodItemsAdapter = new FoodItemsAdapter(mContext, foodItemData);

                    foodItemsRecycler.setLayoutManager(new GridLayoutManager(mContext, listGridIntSpanCount, GridLayoutManager.VERTICAL, false));

                   /* final LayoutAnimationController controller =
                            AnimationUtils.loadLayoutAnimation(foodItemsRecycler.getContext(), R.anim.item_animation_fall_down);
                    foodItemsRecycler.setLayoutAnimation(controller);*/
                    foodItemsRecycler.setAdapter(foodItemsAdapter);

                    /*//ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity());
                    SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(),getFragmentManager());
                    viewpager.setAdapter(sectionsPagerAdapter);
                    //foodiemenutablayout.setupWithViewPager(viewpager);
                    foodiemenutablayout.setupWithViewPager(viewpager);*/

                    //DialogOpener.dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
                //UImsgs.showCustomToast(mContext, "Oops! Something went wrong",ERROR);
               // DialogOpener.dialog.dismiss();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

//    private void ViewInit()){
//
//        //Switch
//        nonveg = root.findViewById(R.id.nonveg);
//
//        //Linear Layout
//        veg_non_veg_linear = root.findViewById(R.id.veg_non_veg_linear);
//
//        foodItemsRecycler = (RecyclerView) root.findViewById(R.id.foodItemsRecycler);
//        foodMenuList = (HorizontalListView) root.findViewById(R.id.foodiemenurecycler);
//        proceed = (TextView) root.findViewById(R.id.food_proceedtocart);
//        restaurantName = (TextView) root.findViewById(R.id.restaurant_name);
//        setNameOfRestaurant();//Name OF Restaurant
//
//        //tab Layout
//        foodiemenutablayout = root.findViewById(R.id.foodiemenutablayout);
//        foodiemenutablayout.setTabMode(TabLayout.MODE_SCROLLABLE);
//
//        //View
//        viewpager = root.findViewById(R.id.viewpager);
//
//
//    }

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
