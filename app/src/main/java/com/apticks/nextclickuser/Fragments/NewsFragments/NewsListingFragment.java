package com.apticks.nextclickuser.Fragments.NewsFragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.NEWS.NewsActivity;
import com.apticks.nextclickuser.Adapters.NewsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.NewsData;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.LOCAL_NEWS;
import static com.apticks.nextclickuser.Config.Config.LOCAL_NEWS_LOCATION_BASED;
import static com.apticks.nextclickuser.Config.Config.LONGITUDE;
import static com.apticks.nextclickuser.Config.Config.MAIN_NEWS;
import static com.apticks.nextclickuser.Config.Config.NEWS;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewsListingFragment.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link NewsListingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsListingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String newsSubCatID;
    private View root;
    private Context mContext;
    private RecyclerView mainNewsRecycler;
    private ArrayList<NewsData> newsDataList;
    private int newsType;
    PreferenceManager preferenceManager;
    NewsActivity newsActivity;

    private OnFragmentInteractionListener mListener;

    public NewsListingFragment(NewsActivity activity,int type) {
        // Required empty public constructor


    }

    public NewsListingFragment() {

    }

    public NewsListingFragment(NewsActivity newsActivity,String newsSubCatID, int newsType) {
        // Required empty public constructor
        this.newsSubCatID = newsSubCatID;
        this.newsActivity = newsActivity;
        this.newsType = newsType;




    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewsListingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewsListingFragment newInstance(String param1, String param2) {
        NewsListingFragment fragment = new NewsListingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_news_listing, container, false);
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        mainNewsRecycler = root.findViewById(R.id.main_news_recycler);
        Log.d("type_frag", newsType + "");



        newsDataFetcher();

        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"-*+
     * -*-+
     * +
     * <p>
     * <p>
     * <p>
     * <p>
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void newsDataFetcher() {

        String newsUrl = "";
        int newsTYPE = preferenceManager.getInt("newsType");

        if(newsTYPE == LOCAL_NEWS){
            String lat, lang;
            lat = preferenceManager.getString("lat");
            lang = preferenceManager.getString("lang");
            newsUrl = LOCAL_NEWS_LOCATION_BASED+newsSubCatID+"?latitude" + lat + LONGITUDE + lang;
        }

        if(newsTYPE == MAIN_NEWS) {
            newsUrl = NEWS + newsSubCatID;
        }



        Log.d("newsType_in_frag", newsTYPE + "".trim());
        Log.d("news url", newsUrl);



        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, newsUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("news_res",response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {
                            JSONObject dataJson = jsonObject.getJSONObject("data");
                            try {
                                JSONArray resultArray = dataJson.getJSONArray("result");
                                if (resultArray.length() > 0) {
                                    newsDataList = new ArrayList<>();
                                    for (int i = 0; i < resultArray.length(); i++) {
                                        JSONObject resultObject = resultArray.getJSONObject(i);
                                        String id = resultObject.getString("id");
                                        String title = resultObject.getString("title");
                                        String video_link = resultObject.getString("video_link");
                                        String news = resultObject.getString("news");

                                        String news_date="";
                                        try{
                                            news_date = resultObject.getString("news_date");
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        String image = resultObject.getString("image");
                                        String views_count = "0";
                                        String times_ago = "";
                                        try {
                                            times_ago = resultObject.getString("times_ago");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            views_count = resultObject.getString("views_count");
                                            Log.d(title + "views_count", views_count);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        NewsData newsData = new NewsData();

                                        newsData.setId(id);
                                        newsData.setTitle(title);
                                        newsData.setVideo_link(video_link);
                                        newsData.setNews(news);
                                        newsData.setNews_date(news_date);
                                        newsData.setImage(image);
                                        newsData.setView_count(views_count);
                                        newsData.setTimes_ago(times_ago);
                                        newsDataList.add(newsData);

                                    }

                                    NewsAdapter newsAdapter = new NewsAdapter(mContext, newsDataList, newsTYPE,newsSubCatID);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                    mainNewsRecycler.setLayoutManager(layoutManager);

                                    mainNewsRecycler.setAdapter(newsAdapter);
                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }


}
