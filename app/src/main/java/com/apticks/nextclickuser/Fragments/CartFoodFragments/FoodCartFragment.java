package com.apticks.nextclickuser.Fragments.CartFoodFragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.AddToCartActivity;
import com.apticks.nextclickuser.Adapters.CartFoodAdapter.FoodCartAdapter;
import com.apticks.nextclickuser.Adapters.CartFoodAdapter.PromoCodeAdapter;
import com.apticks.nextclickuser.Constants.IErrors;
import com.apticks.nextclickuser.Helpers.Validations;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.LocationHelper.GpsTracker;
import com.apticks.nextclickuser.Fragments.CommonModules.PaymentGateways;
import com.apticks.nextclickuser.Pojo.CartPojoModel.CartPojo;
import com.apticks.nextclickuser.Pojo.CartPojoModel.DialogPromoModel;
import com.apticks.nextclickuser.Pojo.FoodOrderPostModel;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.AddressDataBaseHelper;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.SqliteDatabase.DataBaseHelper;
import com.apticks.nextclickuser.Activities.LoginActivity;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static android.view.Gravity.CENTER;
import static android.view.Gravity.TOP;
import static com.apticks.nextclickuser.Config.Config.*;

import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.*;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FoodCartFragment.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link FoodCartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FoodCartFragment extends Fragment implements EditNameDialogListenerTemp/*implements DialogPromoCodeTemp.EditNameDialogListenerTemp*/ {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Context mContext;
    int cartValue, cartSectValue, walletAmount = 0;
    ArrayList<CartPojo> cartList;
    ArrayList<FoodOrderPostModel> foodOrderPostModelsArray = new ArrayList<>();
    RecyclerView foodcartItemsRecycler;
    TextView foodProceedToPay, foodChangeAddress, itemTotal, add_address, item_tax, wallet_image_remove;
    TextView wallet_code, customer_address, vendor_name, vendor_address, item_delivery_charge, items_tax, wallet_text_view, item_wallet_used, items_count;
    static TextView text_coupan, promo_code, remove_promo_code, item_item_grand_total_total, item_promo_apllied;
    ImageView imageBack, wallet_sode;
    static ImageView coupn_sode;
    EditText inst_text_cart;
    LinearLayout linearLayout, linearAddress;
    RelativeLayout relativeEmptyCart, relativeProceedToPay, relative_wallet_layout, relative_toggle, relative_delivery_charge;
    static RelativeLayout relative_promo_code;
    ToggleButton toggle_delivery;
    Dialog dialog;
    DataBaseHelper dataBaseHelper;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public static boolean selectAddresToggle = false;//true When Delivery
    private String totalAmount;
    static private String promoCodeString = "";
    //static private String promoDiscountValue = "";
    public static double promoDiscountValue;
    String item;
    private View root;
    private Cursor rs, rsSect;
    private PreferenceManager preferenceManager;
    private Validations validations;
    private LoginActivity loginActivity;
    private CartDBHelper cartDBHelper;
    private AddressDataBaseHelper databaseHelper;
    private GpsTracker gpsTracker;
    private DialogVendorNotReady dialogVendorNotReady;
    ListView listView;
    Animation animation;

    // For String Getting Current Latitude or Longitude
    static int userAddressID;
    static String userSelectedAddressID;
    String s1, s2;
    public static ArrayList<HashMap<String, Object>> foodOrderPostItemArrayHashMap = new ArrayList<>();
    public static ArrayList<HashMap<String, Object>> foodOrderPostModelsArrayHashMap = new ArrayList<>();

    public ArrayList<DialogPromoModel> arrayListDialogPromoModel = new ArrayList<>();
    private OnFragmentInteractionListener mListener;
    private FoodCartAdapter newsAdapter;
    ProgressBar wallet_progress;

    public static int deliverChargeStatic = 0;
    public static double grandTotalWithDel = 0, grandTotalWithoutDel = 0;
    public static int vendorTax = 0;
    //int walletUsedStatus =0;//1 is for used && 0 is for not used
    public static int walletUsedStatus = 0;//1 is for used && 0 is for not used
    public static int promoUsedStatus = 0;// 1 is for used && 0 is for not used
    static int promoId = 0;
    // String for Insert data in SqliteDatabase
    String name, email, contact, address, password, conpassword;
    public static String strPromoCode = "";

    public static int deliveryBoy = 1;
    public static int selfPickup = 2;
    public static int deliveryType = selfPickup;
    public static double taxOfVendor = 0.0;

    public FoodCartFragment() {
        // Required empty public constructor
    }

    public FoodCartFragment(Context context) {
        // Required empty public constructor
        this.mContext = context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FoodCartFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FoodCartFragment newInstance(String param1, String param2) {
        FoodCartFragment fragment = new FoodCartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_food_cart, container, false);
        //mContext = getActivity();
        cartDBHelper = new CartDBHelper(mContext);
        preferenceManager = new PreferenceManager(mContext);
        validations = new Validations();
        promoId = 0;
        promoUsedStatus = 0;
        promoCodeString = "";
        selectAddresToggle = false;
        userAddressID = 0;
        userSelectedAddressID = "0";
        vendorTax = 0;
        //promoDiscountValue = "";
        initView();
        customer_address.setVisibility(View.GONE);
        foodChangeAddress.setVisibility(View.GONE);
        dataBaseHelper = new DataBaseHelper(mContext);

        if (walletUsedStatus == 0) {
            wallet_image_remove.setVisibility(View.GONE);
        } else {
            wallet_image_remove.setVisibility(View.VISIBLE);
        }

        if (promoUsedStatus == 0) {
            remove_promo_code.setVisibility(View.GONE);
            relative_promo_code.setVisibility(View.GONE);
        } else {
            remove_promo_code.setVisibility(View.VISIBLE);
            relative_promo_code.setVisibility(View.VISIBLE);
        }


        // For chnage Cart Address
        animation = AnimationUtils.loadAnimation(mContext, R.anim.shake);
        //UImsgs.showToast(mContext, "ON Create View");
        //String itemId =  getArguments().getString("item_id");
        //String quantity =  getArguments().getString("quantity");
        //Toast.makeText(mContext, quantity, Toast.LENGTH_SHORT).show();
        totalAmount = String.valueOf(dataBaseHelper.getTotalCartAmount());
        item_tax.setText(totalAmount);
        onAdapterChange();
        rs = cartDBHelper.getAllFoodItems();
        rsSect = cartDBHelper.getAllSectFoodItems();
        Map<String, ArrayList<FoodOrderPostModel>> mapItems = new HashMap<>();

        /*ArrayList<String , HashMap<String, Object>> stringArray = new ArrayList<>();*/
        //HashMap<ArrayList,> HashMap = new HashMap<>();

        //On CLick Of Bottom Proceed To Pay
        vendorSettingDeliveryCharges();
        proceedtoPayClick();

        // when Click on image for Back
        imageBackPressed();

        //Set Address
        setAddressDefault();
        //Will Show Dialog Box
        changeAddressDialog();

        //Promo Code Recycler View
        recyclerPromoCode();
        cartDBHelper = new CartDBHelper(getActivity());
        databaseHelper = new AddressDataBaseHelper(getActivity());

        // For showing Top of Change Cart Address
        fetchChangeAddress();

        //Toggle Button Changed
        relativeToggleChange();


        wallet_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (promoUsedStatus == 0) {
                    wallet_progress.setVisibility(View.VISIBLE);
                    if (preferenceManager.getInt(wallet) != 0) {
                        //Wallet Amount Setting
                        walletUsedStatus = 1;
                        wallet_progress.setVisibility(View.GONE);
                        wallet_sode.setVisibility(View.GONE);
                        wallet_code.setVisibility(View.GONE);
                        wallet_image_remove.setVisibility(View.VISIBLE);
                        wallet_text_view.setVisibility(View.VISIBLE);
                        walletAmount = preferenceManager.getInt(wallet);
                        wallet_text_view.setText(String.valueOf(walletAmount));

                        //Setting Grant Total Price After deduction
                        relative_wallet_layout.setVisibility(View.VISIBLE);
                        totalAmount = String.valueOf(dataBaseHelper.getTotalCartAmount());

                        if (deliverChargeStatic >= 0) {
                            double tempGrandTotal = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic;

                            if (walletAmount < tempGrandTotal) {
                                item_item_grand_total_total.setText(String.valueOf(tempGrandTotal - walletAmount));
                                item_wallet_used.setText(String.valueOf(walletAmount));
                            } else if (walletAmount > tempGrandTotal) {
                                double tempGrand = walletAmount - tempGrandTotal;
                                tempGrand = tempGrand - tempGrand + (-tempGrand);
                                item_item_grand_total_total.setText(String.valueOf(deliverChargeStatic));
                                item_wallet_used.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
                            }
                        } else {
                            double tempGrandTotal = dataBaseHelper.getTotalCartAmount();
                            if (walletAmount < tempGrandTotal) {
                                item_item_grand_total_total.setText(String.valueOf(tempGrandTotal - walletAmount));
                                item_wallet_used.setText(String.valueOf(walletAmount));
                            } else if (walletAmount > tempGrandTotal) {
                                double tempGrand = walletAmount - tempGrandTotal;
                                tempGrand = tempGrand - tempGrand + (-tempGrand);
                                item_item_grand_total_total.setText(String.valueOf(deliverChargeStatic));
                                item_wallet_used.setText(String.valueOf(tempGrand));
                            }
                        }

                    } else {
                        UImsgs.showCustomToast(mContext, "You don't have enough balance in your wallet", INFO);
                        wallet_progress.setVisibility(View.GONE);
                    }
                } else {
                    wallet_progress.setVisibility(View.VISIBLE);
                    if (preferenceManager.getInt(wallet) != 0) {
                        //Wallet Amount Setting
                        walletUsedStatus = 1;
                        wallet_progress.setVisibility(View.GONE);
                        wallet_sode.setVisibility(View.GONE);
                        wallet_code.setVisibility(View.GONE);
                        wallet_image_remove.setVisibility(View.VISIBLE);
                        wallet_text_view.setVisibility(View.VISIBLE);
                        walletAmount = preferenceManager.getInt(wallet);
                        wallet_text_view.setText(String.valueOf(walletAmount));

                        //Setting Grant Total Price After deduction
                        relative_wallet_layout.setVisibility(View.VISIBLE);
                        totalAmount = String.valueOf(dataBaseHelper.getTotalCartAmount());

                        if (deliverChargeStatic >= 0) {
                            double tempGrandTotal = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic;

                            if (walletAmount < tempGrandTotal) {
                                item_item_grand_total_total.setText(String.valueOf(tempGrandTotal - walletAmount));
                                item_wallet_used.setText(String.valueOf(walletAmount));
                            } else if (walletAmount > tempGrandTotal) {
                                double tempGrand = walletAmount - tempGrandTotal;
                                tempGrand = tempGrand - tempGrand + (-tempGrand);
                                item_item_grand_total_total.setText(String.valueOf(deliverChargeStatic));
                                item_wallet_used.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
                            }
                        } else {
                            double tempGrandTotal = dataBaseHelper.getTotalCartAmount();
                            if (walletAmount < tempGrandTotal) {
                                item_item_grand_total_total.setText(String.valueOf(tempGrandTotal - walletAmount));
                                item_wallet_used.setText(String.valueOf(walletAmount));
                            } else if (walletAmount > tempGrandTotal) {
                                double tempGrand = walletAmount - tempGrandTotal;
                                tempGrand = tempGrand - tempGrand + (-tempGrand);
                                item_item_grand_total_total.setText(String.valueOf(deliverChargeStatic));
                                item_wallet_used.setText(String.valueOf(tempGrand));
                            }
                        }
                    } else {
                        Toast.makeText(mContext, "You don't have enough balance in your wallet", Toast.LENGTH_SHORT).show();
                        wallet_progress.setVisibility(View.GONE);
                    }
                }

            }
        });
        promo_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ApplyPromoCode();
                //dialogPromoCode.promoCodeDialog();
                FragmentManager fragmentManager = getFragmentManager();
                //DialogPromoCodeTemp dialogPromoCodeTemp = DialogPromoCodeTemp.newInstance();
                //DialogPromoCodeTemp dialogPromoCodeTemp = new DialogPromoCodeTemp();
                //DialogInnerPromo dialogPromoCodeTemp = DialogInnerPromo.newInstance();
                DialogInnerPromo dialogPromoCodeTemp = new DialogInnerPromo();
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                /*FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.show(dialogPromoCodeTemp);
                fragmentTransaction.commit();*/
                dialogPromoCodeTemp.show(fragmentManager, "Dialog Promo");
            }
        });

        remove_promo_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                promoUsedStatus = 0;
                promo_code.setVisibility(View.VISIBLE);
                text_coupan.setVisibility(View.GONE);
                remove_promo_code.setVisibility(View.GONE);
                coupn_sode.setVisibility(View.VISIBLE);

                //Relative Visibility Gone
                relative_promo_code.setVisibility(View.GONE);
            }
        });

        wallet_image_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                walletUsedStatus = 0;
                wallet_sode.setVisibility(View.VISIBLE);
                wallet_code.setVisibility(View.VISIBLE);
                wallet_image_remove.setVisibility(View.GONE);
                wallet_text_view.setVisibility(View.GONE);


                //Relative Price Remove
                relative_wallet_layout.setVisibility(View.GONE);
                totalAmount = String.valueOf(dataBaseHelper.getTotalCartAmount());

                if (deliverChargeStatic >= 0) {
                    double tempGrandTotal = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic;
                    item_item_grand_total_total.setText(String.valueOf(tempGrandTotal));
                } else {
                    item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
                }

            }
        });
        return root;
    }

    private void relativeToggleChange() {
        toggle_delivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (toggle_delivery.isChecked()) {
                    //Toast.makeText(mContext, "True", Toast.LENGTH_SHORT).show();
                    deliveryType = deliveryBoy;
                    customer_address.setVisibility(View.VISIBLE);
                    foodChangeAddress.setVisibility(View.VISIBLE);
                    relative_delivery_charge.setVisibility(View.VISIBLE);
                    selectAddresToggle = toggle_delivery.isChecked();//(Boolean) Check Weather Toogle is OFF or ON.. ..
                    Log.d("type=delivery", String.valueOf(grandTotalWithDel + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()))));
                    item_item_grand_total_total.setText(String.valueOf(grandTotalWithDel + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()))));

                } else {
                    //Toast.makeText(mContext, "False", Toast.LENGTH_SHORT).show();
                    deliveryType = selfPickup;
                    deliverChargeStatic = 0;
                    customer_address.setVisibility(View.GONE);
                    foodChangeAddress.setVisibility(View.GONE);
                    relative_delivery_charge.setVisibility(View.GONE);
                    selectAddresToggle = toggle_delivery.isChecked();
                    Log.d("type=self", String.valueOf(grandTotalWithoutDel + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()))));
                    item_item_grand_total_total.setText(String.valueOf(grandTotalWithoutDel + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()))));

                }
            }
        });
    }

    private void setAddressDefault() {

    }

    private void vendorSettingDeliveryCharges() {

        double tempGrantTotal = deliverChargeStatic + (dataBaseHelper.getTotalCartAmount());
        item_item_grand_total_total.setText(String.valueOf(tempGrantTotal));

        //int vendorId = AddToCartActivity.vendorId;
        int vendorId = AddToCartActivity.vendorId;
        String vendorIdString = vendorIdFetchDatabse();
        //Toast.makeText(mContext, " Vendor Id  " + vendorIdString, Toast.LENGTH_SHORT).show();
        Log.d("vendor setting url", Food_Orders_Vendor_Setting + vendorIdFetchDatabse());
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Food_Orders_Vendor_Setting + vendorIdFetchDatabse(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("vendor setting resp", response);
                        if (jsonObject.getBoolean("status")) {
                            try {
                                Boolean jsonObjectElse = jsonObject.getBoolean("data");

                                if (!jsonObjectElse) {
                                    dialogVendorNotReady = new DialogVendorNotReady();
                                    View.OnClickListener onClickOK = new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            getActivity().finish();
                                        /*linearLayout.setVisibility(View.GONE);
                                        relativeEmptyCart.setVisibility(View.VISIBLE);
                                        relativeProceedToPay.setVisibility(View.GONE);*/
                                            //UImsgs.showToast(mContext, " Boolean "+jsonObjectElse);
                                        }
                                    };
                                    dialogVendorNotReady.alertDialogNotReady(mContext, onClickOK);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                //UImsgs.showToast(mContext, " Here "+e);
                            }


                            JSONObject jsonObjectData = (jsonObject.getJSONObject("data"));
                            //Toast.makeText(mContext, " + " + jsonObject.getJSONObject("data"), Toast.LENGTH_SHORT).show();

                            //vendorTax  = jsonObjectData.getInt("tax");
                            JSONObject jsonObjectVendor = jsonObjectData.getJSONObject("vendor");
                            String vendorName = jsonObjectVendor.getString("name");
                            vendor_name.setText(vendorName);
                            vendor_address.setText(jsonObjectVendor.getString("address") + "\n" + jsonObjectVendor.getString("landmark"));
                            int deliverChargeTemp = jsonObjectData.getInt("min_delivery_fee");
                            double tempGrantTotal = 0.0;
                            /*if (deliverChargeTemp !=0){
                                //Toast.makeText(mContext, " + " +deliverChargeTemp, Toast.LENGTH_SHORT).show();
                                item_delivery_charge.setText(String.valueOf(deliverChargeTemp));
                                deliverChargeStatic = deliverChargeTemp;
                                tempGrantTotal = deliverChargeTemp + (dataBaseHelper.getTotalCartAmount()) ;
                                item_item_grand_total_total.setText(String.valueOf(tempGrantTotal));
                                grandTotalWithDel =tempGrantTotal;
                                grandTotalWithoutDel=dataBaseHelper.getTotalCartAmount();
                                Log.d("vs grandTotalWithoutDel",grandTotalWithoutDel+"");
                            }else{*/
                            deliverChargeStatic = 0;
                            tempGrantTotal = /*deliverChargeTemp +*/ (dataBaseHelper.getTotalCartAmount());
                            Log.d("grnd ttl", String.valueOf(tempGrantTotal));
                            item_item_grand_total_total.setText(String.valueOf(tempGrantTotal));
                            /*}*/

                            try {
                                int tax = jsonObjectData.getInt("tax");
                                if (tax > 0) {

                                    taxOfVendor = tax;
                                    double temptax = ((tax) * (0.01)) * (dataBaseHelper.getTotalCartAmount());
                                    double totalwithtax = Math.ceil(tempGrantTotal + temptax);
                                    items_tax.setText(Math.ceil(temptax) + "(" + tax + "%)");
                                    Log.d("grnd ttl", String.valueOf(totalwithtax));
                                    item_item_grand_total_total.setText(String.valueOf(totalwithtax));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            JSONObject jsonObjectPermission = jsonObjectData.getJSONObject("permission");
                            if (jsonObjectPermission.getInt("order_selfpickup") == 2) {
                                //relative_toggle.setVisibility(View.GONE);

                            } else if (jsonObjectPermission.getInt("order_selfpickup") == 0) {
                                //relative_toggle.setVisibility(View.GONE);
                                //relative_toggle.setVisibility(View.VISIBLE);
                            }
                            if (jsonObjectPermission.getInt("order_booking") == 3) {
                                relative_toggle.setVisibility(View.GONE);
                            }
                            if (jsonObjectPermission.getInt("order_delivery") == 1) {
                                //relative_toggle.setVisibility(View.VISIBLE);
                                customer_address.setVisibility(View.VISIBLE);
                                foodChangeAddress.setVisibility(View.VISIBLE);
                                relative_delivery_charge.setVisibility(View.VISIBLE);
                                relative_toggle.setVisibility(View.VISIBLE);
                                toggle_delivery.setVisibility(View.VISIBLE);
                                selectAddresToggle = toggle_delivery.isChecked();//(Boolean) Check Weather Toogle is OFF or ON.. ..
                            }


                        } else {
                            //Toast.makeText(mContext, "Else", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        //Toast.makeText(mContext, " + "+ e, Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    // for click on image for back page
    private void imageBackPressed() {
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    // When click on Pay button
    private void proceedtoPayClick() {
        foodProceedToPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("asd", "asd");
                showAlertDialogButtonClicked(v);
            }
        });
    }


    public void showAlertDialogButtonClicked(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View customLayout = getLayoutInflater().inflate(R.layout.layout_delivery_type, null);
        builder.setView(customLayout);

        RadioGroup radioGroup = customLayout.findViewById(R.id.radioGroup);
        RadioButton rbSelfPickUp = customLayout.findViewById(R.id.rbSelfPickUp);
        RadioButton rbDeliveryBoy = customLayout.findViewById(R.id.rbDeliveryBoy);

        // add a button 
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (rbSelfPickUp.isChecked()) {
                    loginActivity = new LoginActivity(mContext);
                    if (preferenceManager.getString(TOKEN_KEY) == null) {
                        //UImsgs.showToast(mContext,preferenceManager.getString(TOKEN_KEY));
                        //loginActivity.alertDialogLogin();
                    }
                    if (selectAddresToggle) {
                        if (!validateAddress()) {
                            UImsgs.showSnackBar(view, "Please Select Address To Be Delivered");
                        } else {
                            rs = cartDBHelper.getAllFoodItems();
                            rsSect = cartDBHelper.getAllSectFoodItems();
                            cartFetcherFromDatabase();
                            proceedtoPayClickPaymentGatewayIntent();
                        }
                    } else {
                        rs = cartDBHelper.getAllFoodItems();
                        rsSect = cartDBHelper.getAllSectFoodItems();
                        cartFetcherFromDatabase();
                        proceedtoPayClickPaymentGatewayIntent();
                    }
                    if (dialog != null)
                        dialog.dismiss();
                } else if (rbDeliveryBoy.isChecked()) {
                    if (dialog != null)
                        dialog.dismiss();
                    showComingSoonAlertDialog();
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void showComingSoonAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View customLayout = getLayoutInflater().inflate(R.layout.layout_coming_soon, null);
        builder.setView(customLayout);

        // add a button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null)
                    dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void proceedtoPayClickPaymentGatewayIntent() {
        //UImsgs.showToast(mContext, "Is Not There");
        Map<String, Object> mainData = new HashMap<>();
        mainData.put("discount", "0");
        mainData.put("delivery_fee", deliverChargeStatic);
        mainData.put("tax", taxOfVendor);
        mainData.put("total", item_item_grand_total_total.getText().toString());
        mainData.put("promo_id", promoId);
        mainData.put("promo_code", promoCodeString);
        //mainData.put("promo_discount", "");
        mainData.put("payment_method_id", "1");
        mainData.put("address_id", userSelectedAddressID);//If Self Pickup -> 0
        mainData.put("delivery", deliveryType);
        mainData.put("instructions", inst_text_cart.getText().toString());
        //mainData.put("vendor_id", "1");
        mainData.put("vendor_id", vendorIdFetchDatabse()/*AddToCartActivity.vendorId*/);//Vendor ID || Calling static VEndor ID From Add TO Cart Activity
        mainData.put("used_walet", String.valueOf(walletUsedStatus));//When Used Send 1 && When Not Used Send 0
        mainData.put("used_walet_amount", String.valueOf(walletAmount));//Change Here After Using Wallet Amount
                    /*mainData.put("items", foodOrderPostItemArrayHashMap);
                    mainData.put("sec_items", foodOrderPostModelsArrayHashMap);*/
        mainData.put("items", cartFetcherFromDatabase());
        Log.d("items", cartFetcherFromDatabase() + "");
        mainData.put("sec_items", cartSectFetcherFromDatabase());
        Log.d("sec_items", cartSectFetcherFromDatabase() + "");

        JSONObject json = new JSONObject(mainData);

        final String data = json.toString();
        Intent intent = new Intent(mContext, PaymentGateways.class);
        //String str = itemTotal.getText().toString();
        String str = item_item_grand_total_total.getText().toString();
        //int len = str.substring(str.indexOf(".")).length() - 1;
        String decimalPart = str.substring(str.indexOf(".") + 1);
        String realPart = str.substring(0, str.indexOf("."));
        //Toast.makeText(mContext, ""+realPart+decimalPart, Toast.LENGTH_SHORT).show();
        intent.putExtra("mainFoodOrder", data);
        //intent.putExtra("totatlpayamount",realPart+decimalPart);
        double finalPrice = Double.parseDouble(str);

        Math.round(finalPrice);

        intent.putExtra("totatlpayamount", Math.round(finalPrice) + "");
        startActivity(intent);
    }

    private boolean validateAddress() {
        if (validations.isBlank(customer_address.getText().toString())) {
            return false;
        }


        return true;
    }

    private ArrayList<HashMap<String, Object>> cartSectFetcherFromDatabase() {
        ArrayList<HashMap<String, Object>> foodOrderPostModelsArrayHashMapParam = new ArrayList<>();
        int v = cartDBHelper.numberOfSectFoodCartRows();
        rsSect.moveToFirst();
        int i = 0;
        while (rsSect != null && i < v) {
            String id = rsSect.getString(rsSect.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_ID));
            int quantity = rsSect.getInt(rsSect.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_QUANTITY));
            String sectionItem = rsSect.getString(rsSect.getColumnIndex(cartDBHelper.CART_COLUMN_SEC_ITEMS_ID));
            String price = rsSect.getString(rsSect.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_PRICE));
            String itemIdF = rsSect.getString(rsSect.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_ID));


            HashMap<String, Object> stringObjectHashMapPst = new HashMap<>();
            stringObjectHashMapPst.put("item_id", itemIdF);
            stringObjectHashMapPst.put("sec_quantity", quantity);
            stringObjectHashMapPst.put("sec_item_id", sectionItem);
            stringObjectHashMapPst.put("sec_price", price);
            foodOrderPostModelsArrayHashMap.add(stringObjectHashMapPst);
            foodOrderPostModelsArrayHashMapParam.add(stringObjectHashMapPst);
            Log.e("quantityJSONDBCh", String.valueOf(quantity));

            rsSect.moveToNext();
            i++;
        }
        return foodOrderPostModelsArrayHashMapParam;
    }

    private void onAdapterChange() {
        itemTotal.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));

        //foodcartItemsRecycler.addOnItemTouchListener(new RecyclerItemClickListener());
    }

    private void changeAddressDialog() {
        foodChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(mContext);
                Window window = dialog.getWindow();
                //window.requestFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_custom_food_cart);
                dialog.setCancelable(false);
                GetCurrentLocation();
                listView = (ListView) dialog.findViewById(R.id.list_address);

                String suggesation = databaseHelper.showAllDatamain();
                String selectedAddresID = databaseHelper.showSelectAddresID();

                final String[] mydata = suggesation.split("#");
                final String[] myAddressID = selectedAddresID.split("#");
                final ArrayList<String> list = new ArrayList<String>(Arrays.asList(mydata));
                final ArrayList<String> listSelectedAddresID = new ArrayList<String>(Arrays.asList(myAddressID));
                ArrayAdapter arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, list);
                listView.setAdapter(arrayAdapter);

                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                        //listSelectedAddresID.get(position);
                        //Toast.makeText(mContext, ""+databaseHelper.deleteSectCartItems( listSelectedAddresID.get(position)), Toast.LENGTH_SHORT).show();
                        if (databaseHelper.deleteSectCartItems(listSelectedAddresID.get(position)) == 1) {
                            list.remove(position);
                            listSelectedAddresID.remove(position);
                            //listView.invalidate();
                            ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
                            deleteSelectedAddress(listSelectedAddresID.get(position));
                        }
                        return true;
                    }
                });

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        item = (String) listView.getItemAtPosition(position);

                        //Toast.makeText(mContext, " Here "+item, Toast.LENGTH_SHORT).show();
                        //Toast.makeText(mContext, " Here "+listSelectedAddresID.get(position), Toast.LENGTH_SHORT).show();
                        userSelectedAddressID = listSelectedAddresID.get(position);
                        customer_address.setText(item);
                        Log.e("Selected", "" + item);
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.close_dialog).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.add_address).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AddAddressDialog();

                        dialog.cancel();
                    }
                });
                window.setGravity(CENTER);
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                window.setGravity(TOP);
                dialog.show();
            }
        });
    }

    private void deleteSelectedAddress(String addressId) {
        Map<String, String> mapToParse = new HashMap<>();
        mapToParse.put("id", addressId);
        JSONObject jsonObject = new JSONObject(mapToParse);
        final String data = jsonObject.toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, DELETE_USER_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;

            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void AddAddressDialog() {
        EditText et_User_name, et_email_id, et_mobile_no, et_address;

        final Dialog dialog = new Dialog(mContext);
        Window window = dialog.getWindow();
        //window.requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_prompts);
        dialog.setCancelable(false);
        window.setGravity(CENTER);

        et_User_name = (EditText) dialog.findViewById(R.id.et_User_name);
        et_email_id = (EditText) dialog.findViewById(R.id.et_email_id);
        et_mobile_no = (EditText) dialog.findViewById(R.id.et_mobile_no);
        et_address = (EditText) dialog.findViewById(R.id.et_address);

        dialog.findViewById(R.id.page_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.tv_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = et_User_name.getText().toString().trim();
                email = et_email_id.getText().toString().trim();
                contact = et_mobile_no.getText().toString().trim();
                address = et_address.getText().toString().trim();
                password = "123";
                conpassword = "123";

                if (!name.equals("")) {
                    if (!email.equals("")) {
                        if (!contact.equals("")) {
                            if (!address.equals("")) {
                                SendCartAddress();
                            } else {
                                et_address.setError("Address Should Not be Empty");
                                et_address.startAnimation(animation);
                            }
                        } else {
                            et_mobile_no.setError("Mobile No. Should Not be Empty");
                            et_mobile_no.startAnimation(animation);
                        }
                    } else {
                        et_email_id.setError("Email-id Should Not be Empty");
                        et_email_id.startAnimation(animation);
                    }
                } else {
                    et_User_name.setError("Name Should Not be Empty");
                    et_User_name.startAnimation(animation);
                }

//                long id = databaseHelper.insert(name, address, contact, email, password, conpassword);
//
//                if (id != -1) {
//                    Toast.makeText(mContext, "sucess", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
//                }
//                GetCurrentLocation();


                dialog.cancel();

            }
        });

        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(TOP);
        dialog.show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onFinishEditDialog(String inputText) {
        Toast.makeText(mContext, "Dialog " + inputText, Toast.LENGTH_SHORT).show();
    }

    /*@Override
    public void onFinishEditDialog(String inputText) {

        Toast.makeText(mContext, " + "+inputText, Toast.LENGTH_SHORT).show();
    }*/

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void initView() {

        foodcartItemsRecycler = (RecyclerView) root.findViewById(R.id.foodcartItemsRecycler);

        //Text View
        foodProceedToPay = root.findViewById(R.id.food_proceedtopay);
        foodChangeAddress = root.findViewById(R.id.food_change_address);
        add_address = root.findViewById(R.id.add_address);
        item_tax = root.findViewById(R.id.item_tax);
        // find Apply to Promo code textview id

        //Edit Text
        inst_text_cart = root.findViewById(R.id.inst_text_cart);

        itemTotal = root.findViewById(R.id.item_total);
        //Image View
        imageBack = root.findViewById(R.id.image_back);
        wallet_sode = root.findViewById(R.id.wallet_sode);
        coupn_sode = root.findViewById(R.id.coupn_sode);
        wallet_image_remove = root.findViewById(R.id.wallet_image_remove);
        text_coupan = root.findViewById(R.id.text_coupan);
        //Relative Layout
        relativeEmptyCart = root.findViewById(R.id.relative_empty_cart);
        relativeProceedToPay = root.findViewById(R.id.relative_proceed_toPay);
        relative_promo_code = root.findViewById(R.id.relative_promo_code);
        relative_wallet_layout = root.findViewById(R.id.relative_wallet_layout);
        relative_toggle = root.findViewById(R.id.relative_toggle);
        relative_delivery_charge = root.findViewById(R.id.relative_delivery_charge);

        //Linear Layout
        linearLayout = root.findViewById(R.id.linear_layout);
        linearAddress = root.findViewById(R.id.linear_address);

        //Text View
        customer_address = (TextView) root.findViewById(R.id.customer_address);
        vendor_name = (TextView) root.findViewById(R.id.vendor_name);
        vendor_address = (TextView) root.findViewById(R.id.vendor_address);
        item_delivery_charge = root.findViewById(R.id.item_delivery_charge);
        items_tax = root.findViewById(R.id.items_tax);
        item_item_grand_total_total = root.findViewById(R.id.item_item_grand_total_total);
        item_promo_apllied = root.findViewById(R.id.item_promo_apllied);
        wallet_text_view = root.findViewById(R.id.wallet_text_view);
        item_wallet_used = root.findViewById(R.id.item_wallet_used);
        items_count = root.findViewById(R.id.items_count);


        promo_code = root.findViewById(R.id.promo_code);
        remove_promo_code = root.findViewById(R.id.remove_promo_code);
        wallet_code = root.findViewById(R.id.wallet_code);

        //Progress Bar
        wallet_progress = root.findViewById(R.id.wallet_progress);

        //Toggle
        toggle_delivery = root.findViewById(R.id.toggle_delivery);
    }


    public String vendorIdFetchDatabse() {

        String vendorId = "";
        int v = cartDBHelper.numberOfFoodCartRows();
        rs.moveToFirst();
        int i = 0;
        while (rs != null && i < v) {

            String id = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_ID));
            vendorId = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_VENDOR_ID));
            String name = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_NAME));
            String image = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_IMAGE));
            String price = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_PRICE));
            String quantity = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_QUANTITY));
            String foodItem = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEMS));
            String sectionItem = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_SEC_ITEMS));
            String totalQuantity = rs.getString(rs.getColumnIndex("totalQunatity"));
            Log.d("asd", totalQuantity);
            Log.e("DatabaseDataVendorId", "Item_Id" + id + " Vendor ID " + vendorId + " Item Name" + name + " Item Image" + image + " item Price" + price + " Item Quantity" + quantity + " Food Name" + foodItem);
            rs.moveToNext();
            i++;
        }
        return vendorId;
    }


    public ArrayList<HashMap<String, Object>> cartFetcherFromDatabase() {
        ArrayList<HashMap<String, Object>> foodOrderPostItemArrayHashMapParam = new ArrayList<>();

//        Cursor rs = cartDBHelper.getAllFoodItems();
        int v = cartDBHelper.numberOfFoodCartRows();
        rs.moveToFirst();
        int i = 0;
        while (rs != null && i < v) {

            String id = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_ID));
            String name = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_NAME));
            String image = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_IMAGE));
            String price = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_PRICE));
            String quantity = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_QUANTITY));
            String foodItem = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEMS));
            String sectionItem = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_SEC_ITEMS));
            String totalQuantity = rs.getString(rs.getColumnIndex("totalQunatity"));

            Log.d("asd", totalQuantity);
            Log.e("DatabaseData", "Item_Id" + id + "Item Name" + name + "Item Image" + image + "item Price" + price + "Item Quantity: " + quantity + "Food Name" + foodItem);

            //Toast.makeText(mContext, sectionItem, Toast.LENGTH_SHORT).show();
            CartPojo cartPojo = new CartPojo();
            cartPojo.setId(id);
            cartPojo.setName(name);
            cartPojo.setImage(image);
            cartPojo.setPrice(price);
            cartPojo.setQuantity(quantity);
            cartPojo.setTotalQuantity(totalQuantity);

            Cursor rsSecItemd = cartDBHelper.getAllSectionItemsIdRelated(id);
            int vSecItems = cartDBHelper.numberOfSectFoodCartItemsRow(id);
            int j = 0;
            rsSect.moveToFirst();
            /*if (vSecItems>0){
                Toast.makeText(mContext, "If "+vSecItems, Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(mContext, "Else "+vSecItems, Toast.LENGTH_SHORT).show();
            }*/
            ArrayList<CartPojo> secItems = new ArrayList<>();
            while (rsSecItemd != null && j < vSecItems) {
                CartPojo cartPojoArray = new CartPojo();
                String secItemPrice = rsSect.getString(rsSect.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_PRICE));
                String secItemName = rsSect.getString(rsSect.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_NAME));
                cartPojoArray.setPrice(secItemPrice);
                cartPojoArray.setName(secItemName);
                secItems.add(cartPojoArray);
                cartPojo.setSecItems(secItems);
                j++;
                rsSect.moveToNext();
            }
            cartList.add(cartPojo);

            FoodOrderPostModel foodOrderPostModel = new FoodOrderPostModel();
            foodOrderPostModel.setItem_id(Integer.parseInt(id));
            foodOrderPostModel.setPrice(price);
            foodOrderPostModel.setQuantity(quantity);

            foodOrderPostModelsArray.add(foodOrderPostModel);

            HashMap<String, Object> stringObjectItemHashMapPst = new HashMap<>();
            try {
                JSONObject jsonObject1 = new JSONObject(foodItem);
                //Toast.makeText(mContext, ""+jsonObject1, Toast.LENGTH_SHORT).show();


                Log.v("JSON_OBJECT", String.valueOf(jsonObject1));

                String quantityJSON = jsonObject1.getString("item_id");
                String secItemIdJSON = jsonObject1.getString("quantity");
                String secPriceJSON = jsonObject1.getString("price");

                stringObjectItemHashMapPst.put("item_id", quantityJSON);
                stringObjectItemHashMapPst.put("quantity", secItemIdJSON);
                stringObjectItemHashMapPst.put("price", secPriceJSON);

                Log.v("quantityJSON", quantityJSON);

                /*try {
                    String secsItemId = jsonObject1.getString("sec_item_id");
                    stringObjectItemHashMapPst.put("sec_item_id", secsItemId);

                } catch (JSONException ex) {
                    ex.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                foodOrderPostItemArrayHashMap.add(stringObjectItemHashMapPst);
                foodOrderPostItemArrayHashMapParam.add(stringObjectItemHashMapPst);
                //foodOrderPostItemArrayHashMap.remove();
                // }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            HashMap<String, Object> stringObjectHashMapPst = new HashMap<>();
            //stringObjectHashMapPst.put(""+sectionItem,"");
            try {
                JSONObject jsonObject0 = new JSONObject(sectionItem);
                Log.v("JSON_OBJECT", String.valueOf(jsonObject0));
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {

                    /*JSONArray jsonArray = new JSONArray(jsonObject0);
                    UImsgs.showToast(mContext,String.valueOf(jsonArray));
                    Log.v("json_array",String.valueOf(jsonArray));*/

                    String quantityJSON = jsonObject0.getString("sec_quantity");
                    String secItemIdJSON = jsonObject0.getString("sec_item_id");
                    String secPriceJSON = jsonObject0.getString("sec_price");

                    stringObjectHashMapPst.put("sec_quantity", quantityJSON);
                    stringObjectHashMapPst.put("sec_item_id", secItemIdJSON);
                    stringObjectHashMapPst.put("sec_price", secPriceJSON);
                    Log.e("quantityJSON", quantityJSON);

                    //foodOrderPostModelsArrayHashMap.add(stringObjectHashMapPst);
                }
                /*JSONArray jsonArray = jsonObject0.getJSONArray(jsonObject0);
                for (int j =0;j<jsonArray.length();j++){
                    JSONObject jsonObject = new JSONObject(sectionItem);
                    String sec_quantity = jsonObject.getString("sec_quantity");
                    UImsgs.showToast(mContext,sec_quantity);
                }*/
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception", "" + e);
            }
           /* ArrayList<String> stringArray = new ArrayList<String>();
            JSONArray jsonArray = new JSONArray();
            for(int j = 0, count = jsonArray.length(); j< count; j++)
            {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(j);
                    stringArray.add(jsonObject.toString());
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }*/

            rs.moveToNext();
            i++;
        }
        items_count.setText(cartList.size() + "");

        newsAdapter = new FoodCartAdapter(mContext, items_count, cartList, itemTotal, item_tax, deliverChargeStatic, item_item_grand_total_total, items_tax, taxOfVendor,
                new FoodCartAdapter.OnItemQuatityChangeListenerPromo() {
                    @Override
                    public void onItemCheck(String promoString) {
                        //Toast.makeText(mContext, " Tosat "+promoString, Toast.LENGTH_SHORT).show();
                        {
                            Map<String, String> mapToParse = new HashMap<>();
                            mapToParse.put("promo_code", strPromoCode);
                            mapToParse.put("vendor_id", String.valueOf(AddToCartActivity.vendorId));
                            mapToParse.put("total", String.valueOf(dataBaseHelper.getTotalCartAmount()));
                            JSONObject jsonObject = new JSONObject(mapToParse);
                            sendPromoCode(jsonObject);
                        }
                    }
                }
        );
        /*FoodCartFragment foodCartFragment = (FoodCartFragment) cartList.getA;
        newsAdapter.UpdateDataList(FoodCartAdapter.MY_DATA);
        QuickList.invalidateViews();
        QuickList.scrollBy(0, 0);

        newsAdapter.notifyDataSetChanged();*/

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                    private static final float SPEED = 300f;
                    // Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }
                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }
        };
        foodcartItemsRecycler.setLayoutManager(layoutManager);
        foodcartItemsRecycler.setAdapter(newsAdapter);
        /*newsAdapter.detachViewFromParent(int arg0);
        newsAdapter.RecyclerremoveDetachedView*/
        //newsAdapter.
        //foodcartItemsRecycler.notify();

        return foodOrderPostItemArrayHashMapParam;
    }


    @Override
    public void onResume() {
        super.onResume();
        cartList = new ArrayList<>();

        cartValue = cartDBHelper.numberOfFoodCartRows();
        cartSectValue = cartDBHelper.numberOfSectFoodCartRows();

        if (cartValue > 0) {
            linearLayout.setVisibility(View.VISIBLE);
            relativeEmptyCart.setVisibility(View.GONE);
            relativeProceedToPay.setVisibility(View.VISIBLE);
            cartFetcherFromDatabase();
        } else {
            linearLayout.setVisibility(View.GONE);
            relativeEmptyCart.setVisibility(View.VISIBLE);
            relativeProceedToPay.setVisibility(View.GONE);
        }
        if (cartSectValue > 0) {
            cartSectFetcherFromDatabase();//For SecItems
        } else {
            //UImsgs.showToast(mContext, "Nothing Found");
        }
        //UImsgs.showToast(mContext, "IN Resume");

       /* if (userAddressID!=0){
            linearAddress.setVisibility(View.VISIBLE);
        }else{
            linearAddress.setVisibility(View.GONE);
        }*/
    }

    @Override
    public void onStop() {
        super.onStop();
        /*newsAdapter.data.clear();
        newsAdapter.notifyDataSetChanged();*/
        //UImsgs.showToast(mContext, "ON Stop");
    }

    /*@Override
    public void onDestroyView() {
        super.onDestroyView();
        UImsgs.showToast(mContext, "ON Destroy View");
    }*/

    @Override
    public void onStart() {
        super.onStart();
        //UImsgs.showToast(mContext, "ON Start");

    }

    // for Phone Number Validation
    private boolean isValidPhone(String phone) {
        boolean check = false;
        if (!Pattern.matches("[0-10]+", phone)) {
            if (phone.length() < 10) {
                check = false;

            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    private void GetCurrentLocation() {
        gpsTracker = new GpsTracker(getActivity());
        if (gpsTracker.canGetLocation()) {
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();

            Log.e("Lati", "" + String.valueOf(latitude));
            Log.e("Longi", "" + String.valueOf(longitude));
            // Declare Lat or long in String
            s1 = String.valueOf(latitude);
            s2 = String.valueOf(longitude);

        } else {
            gpsTracker.showSettingsAlert();
        }

    }

    private void InsertData(int userAddressID) {
        long id = databaseHelper.insert(name, address, contact, email, password, conpassword, userAddressID);
        if (id != -1) {
            Toast.makeText(mContext, "Sucessfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
        }


    }

    private void SendCartAddress() {
        Map<String, String> maptoparse = new HashMap<>();
        maptoparse.put("name", name);
        maptoparse.put("phone", contact);
        maptoparse.put("email", email);
        maptoparse.put("address", address);
        maptoparse.put("Latitude", s1);
        maptoparse.put("Longitude", s2);
        JSONObject jsonObject = new JSONObject(maptoparse);
        // For Send Headers with Response
        Submission(jsonObject);
    }

    private void Submission(JSONObject dataObject) {
        final String dataStr = dataObject.toString();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ADD_NEW_CART_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Changeresponse", "" + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 201) {
//                        Toast.makeText(getActivity(), "Submitted Successfully", Toast.LENGTH_LONG).show();
                        Log.e("bhdv", "\n" + status + "\n" + http_code);
                        userAddressID = jsonObject.getInt("data");
                        InsertData(userAddressID);
                    } else {
                        Toast.makeText(getActivity(), "Sorry for Inconvenience \n Please submit your Details again", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(getActivity(), "Sorry for Inconvenience \n Please submit your Details again", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(), "Sorry for Inconvenience \n Please submit your review again", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
    }

    private void ApplyPromoCode() {
        dialog = new Dialog(mContext);
        Window window = dialog.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_promo_code);
        dialog.setCancelable(false);
        RecyclerView recyclerViewPromoCode = dialog.findViewById(R.id.recycler_promo);


        dialog.findViewById(R.id.imageback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText promo_code = (EditText) dialog.findViewById(R.id.et_promo_code);
                String str_promo_code = promo_code.getText().toString().trim();

                if (!str_promo_code.equals("")) {
                    sendPromoCode();
                } else {
                    promo_code.setError(IErrors.EMPTY);
                    promo_code.startAnimation(animation);
                }
            }
        });

        if (arrayListDialogPromoModel.size() != 0) {
            recyclerViewPromoCode.setVisibility(View.VISIBLE);
           /* PromoCodeAdapter promoCodeAdapter = new PromoCodeAdapter(arrayListDialogPromoModel);
            recyclerViewPromoCode.setAdapter(promoCodeAdapter);*/
        } else {
            recyclerViewPromoCode.setVisibility(View.GONE);
        }

        window.setGravity(Gravity.CENTER);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(CENTER);
        dialog.show();
    }

    private void recyclerPromoCode() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Food_Promo, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("status")) {
                            JSONArray jsonArraydata = jsonObject.getJSONArray("data");

                            for (int i = 0; i <= jsonArraydata.length(); i++) {
                                JSONObject jsonObjectData = jsonArraydata.getJSONObject(i);
                                DialogPromoModel dialogPromoModel = new DialogPromoModel();
                                dialogPromoModel.setId(jsonObjectData.getInt("id"));
                                dialogPromoModel.setPromo_title(jsonObjectData.getString("promo_title"));
                                dialogPromoModel.setPromo_code(jsonObjectData.getString("promo_Code"));
                                dialogPromoModel.setPromo_type(jsonObjectData.getInt("promo_type"));
                                dialogPromoModel.setPromo_label(jsonObjectData.getString("promo_label"));
                                dialogPromoModel.setValid_from(jsonObjectData.getString("valid_from"));
                                dialogPromoModel.setValid_to(jsonObjectData.getString("valid_to"));
                                dialogPromoModel.setDiscount_type(jsonObjectData.getInt("discount_type"));
                                dialogPromoModel.setDiscount(jsonObjectData.getInt("discount"));
                                dialogPromoModel.setUses(jsonObjectData.getInt("uses"));
                                dialogPromoModel.setStatus(jsonObjectData.getInt("status"));
                                arrayListDialogPromoModel.add(dialogPromoModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                //PromoCodeAdapter
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(mContext, ""+error , Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void sendPromoCode() {
        dialog.dismiss();
    }

    // For displaying Change Address from Cart
    private void fetchChangeAddress() {

        Map<String, String> co_ordinateMap = new HashMap<>();
        co_ordinateMap.put("latitude", preferenceManager.getString("lat"));
        co_ordinateMap.put("longitude", preferenceManager.getString("lang"));
        JSONObject coordinatesObject = new JSONObject(co_ordinateMap);
        final String data = coordinatesObject.toString();


        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest getRequest = new StringRequest(Request.Method.POST, GET_NEW_CART_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Server", "Läuft");
                if (response != null) {

                    //Add Address  To Database Here
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("status")) {
                            //Toast.makeText(mContext, " asd" + jsonObject.getBoolean("status"), Toast.LENGTH_SHORT).show();
                            JSONArray jsonArrayData = jsonObject.getJSONArray("data");
                            if (jsonArrayData.length() > 0) {

                                if (jsonArrayData.length() != databaseHelper.getNumberOfRowCount()) {
                                    databaseHelper.clearDatabase();
                                    for (int i = 0; i < jsonArrayData.length(); i++) {
                                        JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);

                                        long id = databaseHelper.insert(jsonObjectData.getString("name"),
                                                jsonObjectData.getString("address"),
                                                String.valueOf(jsonObjectData.getInt("phone")),
                                                jsonObjectData.getString("email"),
                                                password,
                                                conpassword,
                                                jsonObjectData.getInt("id"));
                                        if (id != -1) {
                                            //Toast.makeText(mContext, "Sucessfully", Toast.LENGTH_SHORT).show();
                                        } else {
                                            //Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                } else {
                                    //Toast.makeText(mContext, " Equal ", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    } catch (Exception e) {

                        //UImsgs.showCustomToast(mContext,"No addresses available",INFO);

                    }
                }//If Block


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Server", "onErrorResponse");

            }
        }) {
            /*@Override
            public String getBodyContentType() {
                return "application/json";
            }*/
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return params;
            }
        };

        getRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(getRequest);
    }

    public static class DialogInnerPromo extends DialogFragment implements View.OnClickListener {
        RecyclerView recyclerViewPromoCode;
        ImageView imageback;
        EditText promoCode;
        TextView apply;
        Animation animation;
        private DataBaseHelper dataBaseHelper;
        private PreferenceManager preferenceManager;
        Context mContext;
        Activity activity;
        ArrayList<DialogPromoModel> arrayListDialogPromoModel;
        View root;

        /*public static DialogInnerPromo newInstance(){

            DialogInnerPromo dialogPromoCodeTemp = new DialogInnerPromo();

            return dialogPromoCodeTemp;
        }*/


        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

            root = inflater.inflate(R.layout.dialog_promo_code, container, false);

            //return super.onCreateView(inflater, container, savedInstanceState);
            return root;
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            mContext = getActivity();
            initView();
            arrayListDialogPromoModel = new ArrayList<>();
            dataBaseHelper = new DataBaseHelper(mContext);
            preferenceManager = new PreferenceManager(mContext);

            imageback.setOnClickListener(this);
            apply.setOnClickListener(this);
            recyclerPromoCode();

        }

        private void recyclerPromoCode() {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Food_Promo, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("status")) {
                                JSONArray jsonArraydata = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObjectData = jsonArraydata.getJSONObject(i);
                                    DialogPromoModel dialogPromoModel = new DialogPromoModel();
                                    dialogPromoModel.setId(jsonObjectData.getInt("id"));
                                    dialogPromoModel.setPromo_title(jsonObjectData.getString("promo_title"));
                                    dialogPromoModel.setPromo_code(jsonObjectData.getString("promo_code"));
                                    dialogPromoModel.setPromo_type(jsonObjectData.getInt("promo_type"));
                                    dialogPromoModel.setPromo_label(jsonObjectData.getString("promo_label"));
                                    dialogPromoModel.setValid_from(jsonObjectData.getString("valid_from"));
                                    dialogPromoModel.setValid_to(jsonObjectData.getString("valid_to"));
                                    dialogPromoModel.setDiscount_type(jsonObjectData.getInt("discount_type"));
                                    dialogPromoModel.setDiscount(jsonObjectData.getInt("discount"));
                                    dialogPromoModel.setUses(jsonObjectData.getInt("uses"));
                                    dialogPromoModel.setStatus(jsonObjectData.getInt("status"));
                                    arrayListDialogPromoModel.add(dialogPromoModel);
                                }

                                checkRecyclerPromoCode();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //checkRecyclerPromoCode();
                            Toast.makeText(mContext, "Error " + e, Toast.LENGTH_SHORT).show();
                        }
                    }
                    //PromoCodeAdapter
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mContext, "Error Responce " + error, Toast.LENGTH_SHORT).show();
                    //checkRecyclerPromoCode();
                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));


                    return map;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }

        private void checkRecyclerPromoCode() {

            if (arrayListDialogPromoModel.size() != 0) {
                recyclerViewPromoCode.setVisibility(View.VISIBLE);
                PromoCodeAdapter promoCodeAdapter = new PromoCodeAdapter(arrayListDialogPromoModel, promoCode);
                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                    @Override
                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                            private static final float SPEED = 300f;// Change this value (default=25f)

                            @Override
                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                return SPEED / displayMetrics.densityDpi;
                            }

                        };
                        smoothScroller.setTargetPosition(position);
                        startSmoothScroll(smoothScroller);
                    }

                };
                recyclerViewPromoCode.setLayoutManager(layoutManager);
                recyclerViewPromoCode.setAdapter(promoCodeAdapter);
            } else {
                recyclerViewPromoCode.setVisibility(View.GONE);
            }

        }

        private void initView() {

            recyclerViewPromoCode = root.findViewById(R.id.recycler_promo);
            imageback = root.findViewById(R.id.imageback);
            promoCode = root.findViewById(R.id.et_promo_code);
            apply = root.findViewById(R.id.apply);

        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case (R.id.imageback): {
                    dismiss();
                    return;
                }
                case (R.id.apply): {
                    //Toast.makeText(mContext, "Apply", Toast.LENGTH_SHORT).show();
                    try {
                        /*EditNameDialogListenerTemp editNameDialogListenerTemp
                                = (EditNameDialogListenerTemp)getActivity();
                        editNameDialogListenerTemp.onFinishEditDialog("Some Text");*/
                        //Toast.makeText(mContext, "Some Text", Toast.LENGTH_SHORT).show();
                        String str_promo_code = promoCode.getText().toString().trim();

                        if (!str_promo_code.equals("")) {
                            Map<String, String> mapToParse = new HashMap<>();
                            mapToParse.put("promo_code", str_promo_code);
                            mapToParse.put("vendor_id", String.valueOf(AddToCartActivity.vendorId));
                            mapToParse.put("total", String.valueOf(dataBaseHelper.getTotalCartAmount()));
                            JSONObject jsonObject = new JSONObject(mapToParse);
                            sendPromoCode(jsonObject, str_promo_code);
                        } else {
                            //Toast.makeText(mContext, "Else", Toast.LENGTH_SHORT).show();
                        }

                    } catch (ClassCastException e) {
                        e.printStackTrace();
                        //throw new ClassCastException(mContext.toString() + "Needed");
                        //Toast.makeText(mContext, "Exception  " + e, Toast.LENGTH_SHORT).show();
                    }

                    return;
                }
            }
        }

        /*public interface EditNameDialogListenerTemp{
            void onFinishEditDialog(String inputText);
        }*/
        private void sendPromoCode(JSONObject jsonObject, String str_promo_code) {
            final String dataStr = jsonObject.toString();
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Food_Check_Promo, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {
                        try {
                            JSONObject jsonObjectResponse = new JSONObject(response);

                            if (jsonObjectResponse.getBoolean("status")) {

                                try {
                                    if (!jsonObjectResponse.getBoolean("data")) {
                                        UImsgs.showToast(mContext, "This is not a Valid PROMO CODE");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                    try {
                                        JSONObject jsonObjectData = (jsonObjectResponse.getJSONObject("data"));
                                        //Toast.makeText(mContext, "" + jsonObjectData.getString("discount"), Toast.LENGTH_SHORT).show();
                                        jsonObjectData.getString("discount");
                                        promoId = jsonObjectData.getInt("promo_id");
                                        promoCodeString = jsonObjectData.getString("promo_code");
                                        dismiss();

                                        promoUsedStatus = 1;

                                        strPromoCode = str_promo_code;

                                        remove_promo_code.setVisibility(View.VISIBLE);
                                        coupn_sode.setVisibility(View.GONE);

                                        promo_code.setVisibility(View.GONE);
                                        text_coupan.setVisibility(View.VISIBLE);
                                        relative_promo_code.setVisibility(View.VISIBLE);

                                        promoDiscountValue = jsonObjectData.getDouble("discount");
                                        item_promo_apllied.setText(String.valueOf(promoDiscountValue));
                                        text_coupan.setText(String.valueOf(promoDiscountValue));

                                        //Check Weather Wallet is Used
                                        if (walletUsedStatus == 1) {//When Used 1 Not used 0
                                            if (selectAddresToggle) {
                                                item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount())) - promoDiscountValue + deliverChargeStatic));
                                                grandTotalWithDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                                grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                            } else {
                                                item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount())) - promoDiscountValue));
                                                grandTotalWithDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                                grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                            }

                                        } else {
                                            if (selectAddresToggle) {
                                                item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount())) - promoDiscountValue + deliverChargeStatic));
                                                grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                                grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                            } else {
                                                item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount())) - promoDiscountValue));
                                                grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                                grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                            }


                                        }

                                    } catch (JSONException e1) {
                                        e.printStackTrace();
                                        Toast.makeText(mContext, "Error Catch " + e1, Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Toast.makeText(mContext, " "+e, Toast.LENGTH_SHORT).show();
                        }
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mContext, " + " + error, Toast.LENGTH_SHORT).show();
                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {

                    try {
                        return dataStr == null ? null : dataStr.getBytes("utf-8");

                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                    //return super.getBody();
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));


                    return map;
                }
            };
            requestQueue.add(stringRequest);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        }
    }

    private void sendPromoCode(JSONObject jsonObject) {
        final String dataStr = jsonObject.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Food_Check_Promo, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);

                        if (jsonObjectResponse.getBoolean("status")) {

                            try {
                                if (!jsonObjectResponse.getBoolean("data")) {
                                    UImsgs.showToast(mContext, "This is not a Valid PROMO CODE");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                                try {
                                    JSONObject jsonObjectData = (jsonObjectResponse.getJSONObject("data"));
                                    //Toast.makeText(mContext, "" + jsonObjectData.getString("discount"), Toast.LENGTH_SHORT).show();
                                    jsonObjectData.getString("discount");
                                    promoId = jsonObjectData.getInt("promo_id");
                                    promoCodeString = jsonObjectData.getString("promo_code");

                                    promoUsedStatus = 1;


                                    remove_promo_code.setVisibility(View.VISIBLE);
                                    coupn_sode.setVisibility(View.GONE);

                                    promo_code.setVisibility(View.GONE);
                                    text_coupan.setVisibility(View.VISIBLE);
                                    relative_promo_code.setVisibility(View.VISIBLE);

                                    promoDiscountValue = jsonObjectData.getDouble("discount");
                                    item_promo_apllied.setText(String.valueOf(promoDiscountValue));
                                    text_coupan.setText(String.valueOf(promoDiscountValue));

                                    //Check Weather Wallet is Used
                                    if (walletUsedStatus == 1) {//When Used 1 Not used 0
                                        if (selectAddresToggle) {
                                            item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount())) - promoDiscountValue + deliverChargeStatic));
                                            grandTotalWithDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                            grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                        } else {
                                            item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount())) - promoDiscountValue));
                                            grandTotalWithDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                            grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                        }

                                    } else {
                                        if (selectAddresToggle) {
                                            item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount())) - promoDiscountValue + deliverChargeStatic));
                                            grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                            grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                        } else {
                                            item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount())) - promoDiscountValue));
                                            grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                            grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                        }


                                    }

                                } catch (JSONException e1) {
                                    e.printStackTrace();
                                    Toast.makeText(mContext, "Error Catch " + e1, Toast.LENGTH_SHORT).show();
                                }

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //Toast.makeText(mContext, " "+e, Toast.LENGTH_SHORT).show();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, " + " + error, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
                //return super.getBody();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));


                return map;
            }
        };
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}
