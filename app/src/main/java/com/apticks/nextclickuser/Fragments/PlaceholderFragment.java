package com.apticks.nextclickuser.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.AddToCartActivity;
import com.apticks.nextclickuser.Adapters.FoodItemsAdapter;
import com.apticks.nextclickuser.Fragments.CartFoodFragments.FoodCartFragment;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.FoodItemPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.DataBaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.FOOD_ITEMS;
import static com.apticks.nextclickuser.Constants.CategoriesId.listGrid;


/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    ArrayList<FoodItemPojo> foodItemData;

    DataBaseHelper dataBaseHelper;

    private PageViewModel pageViewModel;
    public String id;
    Context mContext;
    RecyclerView foodItemsRecycler;
    Switch nonveg;
    TextView itemCount, checkoutAmount, checkOut;
    View layout_cart;//View
    FrameLayout item_counter;

    Animation animation;

    String idTemp;
    static String vegNonVeg = "";
    private int listGridIntSpanCount;


    View root;

    public static PlaceholderFragment newInstance(int index, String id, String vegNonVeg) {

        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        bundle.putString("id", id);
        bundle.putString("vegnonveg", vegNonVeg);
        fragment.setArguments(bundle);
        //fragment.id = id;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkNumberItemCount();//For Cart View
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        //View root = inflater.inflate(R.layout.fooditemsupporter, container, false);

        root = inflater.inflate(R.layout.place_holder_fragment, container, false);
        mContext = getActivity();
        initView();
        //Toast.makeText(mContext, String.valueOf(getArguments().getString("id")), Toast.LENGTH_SHORT).show();
        if (listGrid) {
            listGridIntSpanCount = 1;
        } else {
            listGridIntSpanCount = 2;
        }
        dataBaseHelper = new DataBaseHelper(mContext);

        animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);

        //checkNumberItemCount();//For Cart View


        checkOutCartFragment();//Open New Check Out Fragment

        idTemp = getArguments().getString("id");
        //vegNonVeg = getArguments().getString("vegnonveg");

        if (vegNonVeg == ("1")) {
            //Toast.makeText(mContext,"Veg" + vegNonVeg, Toast.LENGTH_SHORT).show();
            FoodItemsFetcher(idTemp, "?item_type=" + vegNonVeg);
        } else {
            //Toast.makeText(mContext, "NON VEG" + vegNonVeg, Toast.LENGTH_SHORT).show();
            FoodItemsFetcher(idTemp, vegNonVeg);
        }

        nonVegSwitchChange();

        return root;
    }

    private void checkOutCartFragment() {
        item_counter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment selectedFragment = new FoodCartFragment(getActivity());
                getFragmentManager().beginTransaction().replace(R.id.food_nav_host_fragment, selectedFragment).commit();
                //getFragmentManager().beginTransaction().attach(selectedFragment).commit();
                //getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in,R.anim.fade_out).replace(R.id.nav_cart_fragment, selectedFragment).commit();
            }
        });


        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogOpener.dialogOpener(getActivity());
                Fragment selectedFragment = new FoodCartFragment(getActivity());
                getFragmentManager().beginTransaction().replace(R.id.food_nav_host_fragment, selectedFragment).commit();
                DialogOpener.dialog.dismiss();
                //getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in,R.anim.fade_out).replace(R.id.nav_cart_fragment, selectedFragment).commit();
            }
        });


    }

    private void checkNumberItemCount() {
        dataBaseHelper.getTotalNumberOfFoodItems();
        //Toast.makeText(mContext, String.valueOf(dataBaseHelper.getTotalNumberOfFoodItems()), Toast.LENGTH_SHORT).show();
        if (dataBaseHelper.getTotalNumberOfFoodItems() != 0) {
            itemCount.setText(String.valueOf(dataBaseHelper.getTotalNumberOfFoodItems()));
            //Toast.makeText(mContext, String.valueOf(dataBaseHelper.getTotalCartAmount()), Toast.LENGTH_SHORT).show();
            checkoutAmount.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
            layout_cart.setVisibility(View.VISIBLE);
            animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
            layout_cart.setAnimation(animation);
        } else {
            layout_cart.setVisibility(View.GONE);
            animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
            layout_cart.setAnimation(animation);
        }
    }

    private void nonVegSwitchChange() {
        {
            //foodMenuList.getSelectedItemPosition();
            //Toast.makeText(mContext, foodMenuList.getSelectedItemPosition()-1+"", Toast.LENGTH_SHORT).show();
            nonveg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        //Toast.makeText(getActivity(), "Checked", Toast.LENGTH_SHORT).show();
                        foodItemData.clear();
                        //int listPosition = foodMenuList.getSelectedItemPosition()+1;
                        FoodItemsFetcher(idTemp, "?item_type=" + "1");
                    } else {
                        //Toast.makeText(getActivity(), "UN - Chechke", Toast.LENGTH_SHORT).show();
                        foodItemData.clear();
                        //int listPosition = foodMenuList.getSelectedItemPosition()+1;
                        FoodItemsFetcher(idTemp, "");
                    }
                }
            });
        }

    }

    private void initView() {

        //Switch
        nonveg = root.findViewById(R.id.nonveg);
        //Recycle View
        foodItemsRecycler = root.findViewById(R.id.foodItemsRecycler);
        //foodItemsRecycler.setLayoutManager(new GridLayoutManager(mContext, listGridIntSpanCount,GridLayoutManager.VERTICAL, false));

        //TextView
        itemCount = root.findViewById(R.id.item_count);
        checkoutAmount = root.findViewById(R.id.checkout_amount);
        checkOut = root.findViewById(R.id.checkout);
        //Frame Layout
        item_counter = root.findViewById(R.id.item_counter);

        //View
        layout_cart = root.findViewById(R.id.layout_cart);

    }

    public void FoodItemsFetcher(String listPosition, String id) {
        Log.v("Food Items", FOOD_ITEMS + listPosition + id + "/" + AddToCartActivity.vendorId);
        System.out.println("aaaaaaa fooditems  "+FOOD_ITEMS + listPosition + id + "/" + AddToCartActivity.vendorId);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FOOD_ITEMS + listPosition + id + "/" + AddToCartActivity.vendorId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        //Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        System.out.println("aaaaaaaaa datajson  "+dataJson.toString());
                        try {
                            JSONArray resultArray = dataJson.getJSONArray("result");
                            if (resultArray.length() > 0) {
                                foodItemData = new ArrayList<>();
                                for (int i = 0; i < resultArray.length(); i++) {
                                    JSONObject resultObject = resultArray.getJSONObject(i);
                                    String name = resultObject.getString("name");
                                    String desc = resultObject.getString("desc");
                                    String image = resultObject.getString("image");
                                    String id = resultObject.getString("id");
                                    //String price = resultObject.getString("price");
                                    String price = resultObject.getString("price");
                                    int status = resultObject.getInt("status");
                                    int sectionStatus = resultObject.getInt("section_status");

                                    int discount = 0;
                                    double discountPrice = 0.0;
                                    if (resultObject.getInt("discount") != 0) {
                                        discount = resultObject.getInt("discount");
                                        try {
                                            discountPrice = resultObject.getDouble("discount_price");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        //price = Math.floor(Double.parseDouble(resultObject.getString("discount_price")))+"".replace(".0","").trim();
                                    } else {

                                    }

                                    int quantity = resultObject.getInt("quantity");

                                    int itemType = resultObject.getInt("item_type");
                                    FoodItemPojo foodItemPojo = new FoodItemPojo();

                                    foodItemPojo.setName(name);
                                    foodItemPojo.setDesc(desc);
                                    foodItemPojo.setImage(image);
                                    foodItemPojo.setId(id);
                                    foodItemPojo.setPrice(price);
                                    foodItemPojo.setItemType(itemType);
                                    foodItemPojo.setDiscountPrice(discountPrice);
                                    foodItemPojo.setDiscount(discount);
                                    foodItemPojo.setStatus(status);
                                    foodItemPojo.setQuantity(quantity);
                                    foodItemPojo.setSectionStatus(sectionStatus);

                                    foodItemData.add(foodItemPojo);
                                }
                                FoodItemsAdapter foodItemsAdapter = new FoodItemsAdapter(mContext, foodItemData,
                                        new FoodItemsAdapter.OnItemAddListener() {
                                            @Override
                                            public void onItemAdded(boolean b) {
                                                checkNumberItemCount();
                                            }

                                            @Override
                                            public void onItemUncheck(String item, int discount, int secPrice, String valueOf,
                                                                      String s1, String currentSelectedItemsHashMap, String s, int getPrice) {
                                                System.out.println("aaaaaa add "+item+"  "+discount+"  "+secPrice+"  "+valueOf+"  "+
                                                        s1+"  "+currentSelectedItemsHashMap+"  "+s+"  "+getPrice);
                                            }
                                        });

                                foodItemsRecycler.setLayoutManager(new GridLayoutManager(mContext, listGridIntSpanCount, GridLayoutManager.VERTICAL, false));

                   /* final LayoutAnimationController controller =
                            AnimationUtils.loadLayoutAnimation(foodItemsRecycler.getContext(), R.anim.item_animation_fall_down);
                    foodItemsRecycler.setLayoutAnimation(controller);*/
                                foodItemsRecycler.setAdapter(foodItemsAdapter);
                            }
                        } catch (JSONException e) {
                            System.out.println("aaaaaaaaa catch  "+e.getMessage());
                            UImsgs.showToast(mContext, "No products available");
                            e.printStackTrace();
                            //Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaaaa catch 11 "+e.getMessage());
                        UImsgs.showToast(mContext, "No products available");
                        e.printStackTrace();
                        //Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();

                    } finally {

                    }



                    /*//ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity());
                    SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getActivity(),getFragmentManager());
                    viewpager.setAdapter(sectionsPagerAdapter);
                    //foodiemenutablayout.setupWithViewPager(viewpager);
                    foodiemenutablayout.setupWithViewPager(viewpager);*/

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaaa error  "+error.getMessage());
                UImsgs.showToast(mContext, "No products available");
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}