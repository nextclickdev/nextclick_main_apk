package com.apticks.nextclickuser.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.VENDORS.VendorsActivity;
import com.apticks.nextclickuser.Helpers.NetworkUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.apticks.nextclickuser.Activities.Beauty_Spa.Vendors_BS;
import com.apticks.nextclickuser.Activities.MultiCatActivities.MultiCatActivity;
import com.apticks.nextclickuser.Adapters.BS_Adapters.BS_DashBoardAdapter;
import com.apticks.nextclickuser.Adapters.DashBoardCategoriesAdapter;
import com.apticks.nextclickuser.Adapters.HospitalAdapter;
import com.apticks.nextclickuser.Adapters.RestaurantsAdapter;
import com.apticks.nextclickuser.Adapters.SlidingImage_AdapterTemp;
import com.apticks.nextclickuser.EntryActivity;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.BSPojo.BSVendorsPojo;
import com.apticks.nextclickuser.Pojo.CategoriesData;
import com.apticks.nextclickuser.Pojo.DisplayCategory;
import com.apticks.nextclickuser.Pojo.SlidersModelClass;
import com.apticks.nextclickuser.R;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.apticks.nextclickuser.Config.Config.CATEGORY_LIST;
import static com.apticks.nextclickuser.Config.Config.Sliders_And_Advertisements;
import static com.apticks.nextclickuser.Config.Config.VENDOR_LIST;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Dasboard_Fragment.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link Dasboard_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Dasboard_Fragment extends Fragment implements LocationListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String fragID = "0";
    LocationManager locationManager;

    private OnFragmentInteractionListener mListener;

    public Dasboard_Fragment(Context applicationContext) {
        this.mContext = applicationContext;
    }

    public Dasboard_Fragment(Context applicationContext, String fragID) {
        this.mContext = applicationContext;
        this.fragID = fragID;
    }

    ViewPager sliderPager, slider_top, slider_middle, slider_bottom;
    RecyclerView hospitalRecycler, restaurantsRecycler, newsRecycler, bs_vendors_recycler, categoriesRecycler;
    private Context mContext;
    View root;
    TextView newsViewMore, restaurantViewMore, hospitalViewMore, fashionViewMore, trending, location_tv;
    LinearLayout down;
    ImageView viewall;
    FloatingActionButton chat_bot;


    int currentPageSlider = 0;
    int currentPageTop = 0, currentPageMiddle = 0, currentPageBottom = 0;
    int NUM_PAGES = 0;
    int NUM_PAGESTop = 0, NUM_PAGESMiddle = 0, NUM_PAGESBottom = 0;
    ArrayList<String> hsName = new ArrayList<>();
    ArrayList<String> hsImage = new ArrayList<>();

    ArrayList<DisplayCategory> hospitaldata;
    ArrayList<DisplayCategory> restaurantsdata;
    ArrayList<DisplayCategory> bsData;
    ArrayList<CategoriesData> categoriesdatalist;
    ArrayList<BSVendorsPojo> bsvendors_list;
    String catType = "h";

    DashBoardCategoriesAdapter dashBoardCategoriesAdapter;

    CirclePageIndicator indicator, indicatorTop, indicatorMiddle, indicatorBottom;

    private String[] urls = new String[]{
            "https://www.dialmama.com/dailmamavendor/image/cache/catalog/slideshow/home4/slide1-1680x500.jpg",
            "https://www.dialmama.com/dailmamavendor/image/cache/catalog/slideshow/home4/slide2-1680x500.jpg",
            "http://akshayacomputers.com/assets/image/baners/3.jpg"};

    private List<SlidersModelClass> slidersModelClassesList = new ArrayList<>();
    private List<SlidersModelClass> slidersModelClassesTopList = new ArrayList<>();
    private List<SlidersModelClass> slidersModelClassesMiddleList = new ArrayList<>();
    private List<SlidersModelClass> slidersModelClassesBottomList = new ArrayList<>();


    public Dasboard_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Dasboard_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Dasboard_Fragment newInstance(String param1, String param2) {
        Dasboard_Fragment fragment = new Dasboard_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_dasboard_, container, false);//Initializing View
        Window window = getActivity().getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.orange));
        initView();
        categoriesFetcher("l");
        trending = root.findViewById(R.id.trending__);
        location_tv = root.findViewById(R.id.location);
        trending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MultiCatActivity.class);
                intent.putExtra("cat_id", "7");
                startActivity(intent);
            }
        });
        slidersAndAdvertisment();
        if (!fragID.equalsIgnoreCase("0")) {
            swapFragment(fragID);
        }

        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (catType.equalsIgnoreCase("l")) {
                    categoriesFetcher("g");
                    catType = "g";
                }
                if (catType.equalsIgnoreCase("g")) {
                    categoriesFetcher("l");
                    catType = "l";
                }*/
                reviewDialogOpener();
            }
        });
        hospitalViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swapFragment("1");
            }
        });
        /*newsViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewsFragment e_commerceRelatedFragment = new NewsFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.nav_host_fragment, e_commerceRelatedFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/
        restaurantViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swapFragment("4");

               /* VendorsListFragment e_commerceRelatedFragment = new VendorsListFragment(mContext);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.nav_host_fragment, e_commerceRelatedFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();*/
            }
        });
        /*viewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllCategories allCategories = new AllCategories(mContext);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.nav_host_fragment, allCategories);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/
        fashionViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Vendors_BS.class);
                startActivity(intent);
            }
        });

        /*Intent intent = new Intent(mContext, EntryActivity.class);
        startActivity(intent);*/
        chatBotOnCLick();
        return root;
    }

    private void chatBotOnCLick() {
        chat_bot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "BUY NOW", Toast.LENGTH_SHORT).show();
                //apiHit(id);
                Intent intent = new Intent(mContext, EntryActivity.class);
                startActivity(intent);
            }
        });
    }

    private void reviewDialogOpener() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.review_taking_layout, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setTitle("NextClick");
        alert.setMessage("Please provide your review");
        AlertDialog dialog = alert.create();
        dialog.show();
    }


    private void slidersAndAdvertisment() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Sliders_And_Advertisements, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject resultArray = jsonObject.getJSONObject("data");
                    //Toast.makeText(mContext, ""+resultArray, Toast.LENGTH_SHORT).show();

                    JSONArray jsonArraysliders = resultArray.getJSONArray("sliders");

                    for (int i = 0; i < jsonArraysliders.length(); i++) {
                        JSONObject jsonObjectSliders = jsonArraysliders.getJSONObject(i);
                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                        slidersModelClass.setId(String.valueOf(i));
                        slidersModelClass.setBanners(jsonObjectSliders.getString("image"));

                        slidersModelClassesList.add(slidersModelClass);
                    }
                    //Toast.makeText(mContext, ""+jsonArraysliders, Toast.LENGTH_SHORT).show();

                    sliderPager.setAdapter(new SlidingImage_AdapterTemp(mContext, slidersModelClassesList));
                    indicator.setViewPager(sliderPager);


                    //Top Sliders
                    JSONArray jsonArraysliderstop = resultArray.getJSONArray("top");
                    for (int i = 0; i < jsonArraysliderstop.length(); i++) {
                        JSONObject jsonObjectSliders = jsonArraysliderstop.getJSONObject(i);
                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                        slidersModelClass.setId(String.valueOf(i));
                        slidersModelClass.setBanners(jsonObjectSliders.getString("image"));

                        slidersModelClassesTopList.add(slidersModelClass);
                    }
                    slider_top.setAdapter(new SlidingImage_AdapterTemp(mContext, slidersModelClassesTopList));
                    indicatorTop.setViewPager(slider_top);

                    //Middle Sliders
                    JSONArray jsonArrayslidersmiddle = resultArray.getJSONArray("middle");
                    for (int i = 0; i < jsonArrayslidersmiddle.length(); i++) {
                        JSONObject jsonObjectSliders = jsonArrayslidersmiddle.getJSONObject(i);
                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                        slidersModelClass.setId(String.valueOf(i));
                        slidersModelClass.setBanners(jsonObjectSliders.getString("image"));

                        slidersModelClassesMiddleList.add(slidersModelClass);
                    }
                    slider_middle.setAdapter(new SlidingImage_AdapterTemp(mContext, slidersModelClassesMiddleList));
                    indicatorMiddle.setViewPager(slider_middle);

                    //Bottom SLiders
                    JSONArray jsonArrayslidersbottom = resultArray.getJSONArray("bottom");
                    for (int i = 0; i < jsonArrayslidersbottom.length(); i++) {
                        JSONObject jsonObjectSliders = jsonArrayslidersbottom.getJSONObject(i);
                        SlidersModelClass slidersModelClass = new SlidersModelClass();
                        slidersModelClass.setId(String.valueOf(i));
                        slidersModelClass.setBanners(jsonObjectSliders.getString("image"));

                        slidersModelClassesBottomList.add(slidersModelClass);
                    }
                    slider_bottom.setAdapter(new SlidingImage_AdapterTemp(mContext, slidersModelClassesBottomList));
                    indicatorBottom.setViewPager(slider_bottom);

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void swapFragment(String id) {
        VendorsListFragment vendorsListFragment = new VendorsListFragment(mContext, id);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.nav_host_fragment, vendorsListFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    /*  @Override
     *//* public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*//*

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void initView() {


        sliderPager = (ViewPager) root.findViewById(R.id.pager);
        slider_top = (ViewPager) root.findViewById(R.id.slider_top);
        slider_middle = (ViewPager) root.findViewById(R.id.slider_middle);
        slider_bottom = (ViewPager) root.findViewById(R.id.slider_bottom);

        //Floating Action Bar
        chat_bot = root.findViewById(R.id.chat_bot);

        //sliderView();


        indicator = (CirclePageIndicator)
                root.findViewById(R.id.indicator);
        indicatorTop = root.findViewById(R.id.indicator_top);
        indicatorMiddle = root.findViewById(R.id.indicator_middle);
        indicatorBottom = root.findViewById(R.id.indicator_bottom);


        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);
        indicatorTop.setRadius(5 * density);
        indicatorMiddle.setRadius(5 * density);
        indicatorBottom.setRadius(5 * density);

        //NUM_PAGES = urls.length;
        NUM_PAGES = slidersModelClassesList.size();


        getLocation();

        sliderIndicatorSlider();

        sliderIndicatorTop();

        sliderIndicatorMiddle();

        sliderIndicatorBottom();


        hospitalRecycler = (RecyclerView) root.findViewById(R.id.hospitals_recycler);
        restaurantsRecycler = (RecyclerView) root.findViewById(R.id.restaurants_recycler);
        newsRecycler = (RecyclerView) root.findViewById(R.id.latestNewsrecycler);
        bs_vendors_recycler = (RecyclerView) root.findViewById(R.id.bs_recycler);
        categoriesRecycler = (RecyclerView) root.findViewById(R.id.dashboard_category_recycler);

        hospitalViewMore = (TextView) root.findViewById(R.id.hospital_viewmore);
        restaurantViewMore = (TextView) root.findViewById(R.id.restaurant_viewmore);
        newsViewMore = (TextView) root.findViewById(R.id.news_viewmore);
        fashionViewMore = (TextView) root.findViewById(R.id.fashion_viewmore);
        down = (LinearLayout) root.findViewById(R.id.down_category);

        // viewall = (ImageView) root.findViewById(R.id.viewall);

        if (NetworkUtil.isNetworkConnected(mContext)) {
            hospitalDataFetcher();
        } else {
            Toast.makeText(mContext, "No internet connection! ", Toast.LENGTH_SHORT).show();
        }

        restaurantsDataFetcher();
        bs_dataFetcher();
        //fashioItemsDataFetcher();

    }

    private void sliderIndicatorBottom() {
        {
            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPageBottom == NUM_PAGESBottom) {
                        currentPageBottom = 0;
                    }
                    slider_top.setCurrentItem(currentPageBottom++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 3000, 3000);

            // Pager listener over indicator
            indicatorTop.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPageBottom = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });

        }
    }

    private void sliderIndicatorMiddle() {
        {
            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPageMiddle == NUM_PAGESMiddle) {
                        currentPageMiddle = 0;
                    }
                    slider_top.setCurrentItem(currentPageMiddle++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 3000, 3000);

            // Pager listener over indicator
            indicatorTop.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPageMiddle = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });

        }

    }

    private void sliderIndicatorTop() {
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPageTop == NUM_PAGESTop) {
                    currentPageTop = 0;
                }
                slider_top.setCurrentItem(currentPageTop++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicatorTop.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPageTop = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    private void sliderIndicatorSlider() {
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPageSlider == NUM_PAGES) {
                    currentPageSlider = 0;
                }
                sliderPager.setCurrentItem(currentPageSlider++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPageSlider = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    public void typeClassifier(String catType) {
        if (catType.equalsIgnoreCase("l")) {

            dashBoardCategoriesAdapter = new DashBoardCategoriesAdapter(mContext, categoriesdatalist);

            categoriesRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

            categoriesRecycler.setAdapter(dashBoardCategoriesAdapter);


        }
        if (catType.equalsIgnoreCase("g")) {
            dashBoardCategoriesAdapter = new DashBoardCategoriesAdapter(mContext, categoriesdatalist);

            categoriesRecycler.setLayoutManager(new GridLayoutManager(mContext, 4, GridLayoutManager.VERTICAL, false));
            categoriesRecycler.setAdapter(dashBoardCategoriesAdapter);
        }
    }

    public void categoriesFetcher(final String type) {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        //JSONObject dataJson = jsonObject.getJSONObject("foodMenuData");
                        JSONArray resultArray = jsonObject.getJSONArray("data");
                        if (resultArray.length() > 0) {
                            categoriesdatalist = new ArrayList<>();
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String id = resultObject.getString("id");
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                CategoriesData categoriesData = new CategoriesData();
                                categoriesData.setId(id);
                                categoriesData.setName(name);
                                categoriesData.setImage(image);
                                if (id.equalsIgnoreCase("1") || id.equalsIgnoreCase("2") ||
                                        id.equalsIgnoreCase("3") || id.equalsIgnoreCase("4") ||
                                        id.equalsIgnoreCase("5") || id.equalsIgnoreCase("6") ||
                                        id.equalsIgnoreCase("9") || id.equalsIgnoreCase("10") || id.equalsIgnoreCase("61") ||
                                        id.equalsIgnoreCase("7") || id.equalsIgnoreCase("8")) {
                                    categoriesdatalist.add(categoriesData);
                                }

                            }
                        }


                        dashBoardCategoriesAdapter = new DashBoardCategoriesAdapter(getActivity(), categoriesdatalist);

                        categoriesRecycler.setLayoutManager(new ScrollingLinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false, 100));

                        categoriesRecycler.setAdapter(dashBoardCategoriesAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void hospitalDataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_LIST + "1/10/0", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("result");
                        if (resultArray.length() > 0) {
                            hospitaldata = new ArrayList<>();
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                int vendorId = resultObject.getInt("id");
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                DisplayCategory hospital = new DisplayCategory();

                                hospital.setId(vendorId + "".trim());
                                hospital.setName(name);
                                hospital.setImage(image);
                                hospitaldata.add(hospital);

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    HospitalAdapter hospitalAdapter = new HospitalAdapter(mContext, hospitaldata);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                private static final float SPEED = 300f;// Change this value (default=25f)

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };
                    hospitalRecycler.setLayoutManager(layoutManager);

                    hospitalRecycler.setAdapter(hospitalAdapter);


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void restaurantsDataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_LIST + "4/10/0", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("result");
                        if (resultArray.length() > 0) {
                            restaurantsdata = new ArrayList<>();
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);

                                int vendorId = resultObject.getInt("id");
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                                String location_address = resultObject.getString("location_address");
                                int aa = resultObject.getInt("vendor_user_id");

                                DisplayCategory restaurants = new DisplayCategory();

                                //AddToCartActivity.vendorId = resultObject.getInt("id");
                                //restaurants.setLocation(resultObject.getString("address"));
                                restaurants.setVendor_user_Id(vendorId);
                                restaurants.setId(vendorId + "".trim());
                                restaurants.setName(name);
                                restaurants.setImage(image);
                                restaurants.setLocation_address(location_address);
                                restaurants.setVendor_user_Id(aa);
                                restaurantsdata.add(restaurants);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    RestaurantsAdapter restaurantsAdapter = new RestaurantsAdapter(mContext, restaurantsdata);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                                // Change this value (default=25f)
                                private static final float SPEED = 300f;

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };
                    restaurantsRecycler.setLayoutManager(layoutManager);
                    restaurantsRecycler.setAdapter(restaurantsAdapter);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void bs_dataFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_LIST + "7/10/0", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("result");
                        if (resultArray.length() > 0) {
                            bsData = new ArrayList<>();
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                int vendorId = resultObject.getInt("id");
                                String name = resultObject.getString("name");
                                String image = resultObject.getString("image");
                               /* BSVendorsPojo bsVendorsPojo = new BSVendorsPojo();

                                bsVendorsPojo.setId(vendorId+"".trim());
                                bsVendorsPojo.setName(name);
                                bsVendorsPojo.setImage(image);*/
                                DisplayCategory bsVendorsPojo = new DisplayCategory();
                                bsVendorsPojo.setId(vendorId + "".trim());
                                bsVendorsPojo.setName(name);
                                bsVendorsPojo.setImage(image);

                                bsData.add(bsVendorsPojo);

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    BS_DashBoardAdapter bs_dashBoardAdapter = new BS_DashBoardAdapter(mContext, bsData);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                                // Change this value (default=25f)
                                private static final float SPEED = 300f;

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };
                    bs_vendors_recycler.setLayoutManager(layoutManager);

                    bs_vendors_recycler.setAdapter(bs_dashBoardAdapter);


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext, error.toString());
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    class ScrollingLinearLayoutManager extends LinearLayoutManager {
        private final int duration;

        public ScrollingLinearLayoutManager(Context context, int orientation, boolean reverseLayout, int duration) {
            super(context, orientation, reverseLayout);
            this.duration = duration;
            SwipeTask swipeTask;
        }

        @Override
        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state,
                                           int position) {
            View firstVisibleChild = recyclerView.getChildAt(0);
            int itemHeight = firstVisibleChild.getHeight();
            int currentPosition = recyclerView.getChildLayoutPosition(firstVisibleChild);
            int distanceInPixels = Math.abs((currentPosition - position) * itemHeight);
            if (distanceInPixels == 0) {
                distanceInPixels = (int) Math.abs(firstVisibleChild.getY());
            }
            SmoothScroller smoothScroller = new SmoothScroller(recyclerView.getContext(), distanceInPixels, duration);
            smoothScroller.setTargetPosition(position);
            startSmoothScroll(smoothScroller);
        }

        private class SmoothScroller extends LinearSmoothScroller {
            private final float distanceInPixels;
            private final float duration;

            public SmoothScroller(Context context, int distanceInPixels, int duration) {
                super(context);
                this.distanceInPixels = distanceInPixels;
                this.duration = duration;
            }

            @Override
            public PointF computeScrollVectorForPosition(int targetPosition) {
                return ScrollingLinearLayoutManager.this
                        .computeScrollVectorForPosition(targetPosition);
            }

            @Override
            protected int calculateTimeForScrolling(int dx) {
                float proportion = (float) dx / distanceInPixels;
                return (int) (duration * proportion);
            }
        }


        private class SwipeTask extends TimerTask {
            ScrollingLinearLayoutManager scrollingLinearLayoutManager;

            public void run() {
                categoriesRecycler.post(() -> {
                    int nextPage = (scrollingLinearLayoutManager.findFirstVisibleItemPosition() + 1) % dashBoardCategoriesAdapter.getItemCount();
                    categoriesRecycler.smoothScrollToPosition(nextPage);
                });
            }

            SwipeTask swipeTask;
            Timer swipeTimer;

            private void stopScrollTimer() {

                if (null != swipeTimer) {
                    swipeTimer.cancel();
                }
                if (null != swipeTask) {
                    swipeTask.cancel();
                }
            }

            private void resetScrollTimer() {
                stopScrollTimer();
                swipeTask = new SwipeTask();
                swipeTimer = new Timer();
            }

            private void playCarousel() {
                resetScrollTimer();
                swipeTimer.schedule(swipeTask, 0, 10);
            }
        }
    }


    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        try {

            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
            progressDialog.dismiss();
        } catch (SecurityException e) {
            e.printStackTrace();
            Log.d("Location Error", e.toString());
            progressDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            // Toast.makeText(mContext, arr.length+"", Toast.LENGTH_SHORT).show();

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();

            location_tv.setText(arr[3]);


        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

//Location End


}