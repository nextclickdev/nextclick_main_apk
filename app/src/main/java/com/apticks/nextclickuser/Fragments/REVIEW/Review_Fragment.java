package com.apticks.nextclickuser.Fragments.REVIEW;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.ReviewAdapters.ReviewsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.Review_POJO.ReviewsPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Review_Fragment.OnFragmentInteractionListener} profile_back
 * to handle interaction events.
 * Use the {@link Review_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Review_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String vendor_Id;
    private Context context;
    private View root;
    private String token;

    private TextView submitReview;
    private ArrayList<ReviewsPojo> reviewsList;
    private RecyclerView reviewsRecycler;
    private SwipeRefreshLayout reviewsRefresher;
    PreferenceManager preferenceManager;

    private OnFragmentInteractionListener mListener;

    public Review_Fragment() {
        // Required empty public constructor
    }

    public Review_Fragment(String vendor_id) {
        // Required empty public constructor
        this.vendor_Id = vendor_id;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Review_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Review_Fragment newInstance(String param1, String param2) {
        Review_Fragment fragment = new Review_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_review, container, false);
        context = getActivity();
        preferenceManager = new PreferenceManager(context);
        token = preferenceManager.getString(TOKEN_KEY);
        reviewsRecycler = root.findViewById(R.id.review_recycler);
        submitReview = root.findViewById(R.id.submit_review);
        reviewsRefresher = root.findViewById(R.id.review_refresher);
        submitReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewDialogOpener();
            }
        });
        reviewsFetcher();
        reviewsRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reviewsFetcher();
                reviewsRefresher.setRefreshing(false);
            }
        });

        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

  /*  @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This profile_back must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void reviewDialogOpener(){
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.review_taking_layout, null);

        final EditText reviewData = alertLayout.findViewById(R.id.review);
        final RatingBar reviewrating = alertLayout.findViewById(R.id.review_ratingBar);
        final TextView submit = alertLayout.findViewById(R.id.review_submission);

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setTitle("NextClick");
        alert.setMessage("Please provide your review");
        AlertDialog dialog = alert.create();
        dialog.show();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reviewStr = reviewData.getText().toString().trim();
                float rating =  reviewrating.getRating();
                if(reviewStr.length()>2 && rating>=1.0){
                    Map<String,String> maptoparse = new HashMap<>();
                    maptoparse.put("vendor_id",vendor_Id);
                    maptoparse.put("rating",rating+"".trim());
                    maptoparse.put("review",reviewStr);
                    JSONObject jsonObject = new JSONObject(maptoparse);
                    reviewSubmission(jsonObject,dialog);

                }
                else{
                    Toast.makeText(context, "Please provide the data (Review & Rating)", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void reviewSubmission(JSONObject dataObject, AlertDialog dialog){
        final String dataStr =dataObject.toString();
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please wait while submitting.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REVIEW_CREATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if(status && http_code == 201){
                        dialog.dismiss();
                        progressDialog.dismiss();
                        Toast.makeText(context, "Submitted Successfully", Toast.LENGTH_LONG).show();
                    }else{
                        progressDialog.dismiss();
                        Toast.makeText(context, "Sorry for Inconvenience \n Please submit your review again", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    Toast.makeText(context, "Sorry for Inconvenience \n Please submit your review again", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                progressDialog.dismiss();
                Toast.makeText(context, "Sorry for Inconvenience \n Please submit your review again", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X_AUTH_TOKEN", token);
                params.put("content-type", "application/json");
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
    }

    public void reviewsFetcher() {

        Map<String,String> maptoparse = new HashMap<>();
        maptoparse.put("vendor_id",vendor_Id);
        JSONObject jsonObject = new JSONObject(maptoparse);
        final String dataStr = jsonObject.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REVIEW_RETRIEVAL , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if(status && http_code==200){

                            JSONArray dataArray = jsonObject.getJSONArray("data");

                            if (dataArray.length() > 0) {
                                reviewsList = new ArrayList<>();
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject dataObject = dataArray.getJSONObject(i);
                                    ReviewsPojo reviewsPojo = new ReviewsPojo();
                                    reviewsPojo.setReview(dataObject.getString("review"));
                                    reviewsPojo.setRating(dataObject.getString("rating"));
                                    reviewsPojo.setFname(dataObject.getJSONObject("user").getString("first_name"));
                                    reviewsPojo.setLname(dataObject.getJSONObject("user").getString("last_name"));
                                    reviewsPojo.setImage(dataObject.getJSONObject("user").getString("image"));
                                    reviewsList.add(reviewsPojo);
                                }

                                ReviewsAdapter reviewsAdapter = new ReviewsAdapter(context,reviewsList);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) {

                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(context) {

                                            private static final float SPEED = 300f;// Change this value (default=25f)

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }

                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }

                                };
                                reviewsRecycler.setLayoutManager(layoutManager);
                                reviewsRecycler.setAdapter(reviewsAdapter);

                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(context, error.toString());
                Log.d("error", error.toString());
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        requestQueue.add(stringRequest);

    }
}
