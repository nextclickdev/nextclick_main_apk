package com.apticks.nextclickuser.Fragments.OrderHistory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.google.android.material.tabs.TabLayout;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;
import java.util.List;

public class MainHistoryActivity extends AppCompatActivity {
    TabLayout tab_history_user;
    ViewPager view_pager_history;
    ImageView back_history_image,home_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_history);

        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        initView();
              // icon back Pressed for showing previous Page
        findViewById(R.id.back_history_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainHistoryActivity.this, MainCategoriesActivity.class));
                finish();
            }
        });
        home_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainHistoryActivity.this, MainCategoriesActivity.class));
                finish();
            }
        });

    }

    private void initView() {
        tab_history_user = findViewById(R.id.tab_history_user);
        view_pager_history = findViewById(R.id.view_pager_history);
        home_image = findViewById(R.id.home_image);

        AddTabsToFragments();
        tab_history_user.setupWithViewPager(view_pager_history);

    }

    private void AddTabsToFragments() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFrag(new PresentOrder(), getString(R.string.current_order));
        viewPagerAdapter.addFrag(new OldOrderFragment(), getString(R.string.old_order));
        viewPagerAdapter.addFrag(new PendingOrders(), getString(R.string.cacelled_orders));
        view_pager_history.setAdapter(viewPagerAdapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm/*, int behavior*/) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}