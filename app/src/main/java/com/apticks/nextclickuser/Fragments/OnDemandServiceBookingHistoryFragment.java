package com.apticks.nextclickuser.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.OnDemandServiceBookingHistoryFragmentAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Pojo.bookingsDoctorsHistoryResponse.BookingsDoctorsHistoryResponse;
import com.apticks.nextclickuser.Pojo.bookingsDoctorsHistoryResponse.DoctorBookingHistoryDetails;
import com.apticks.nextclickuser.Pojo.getServicesResponse.GetServiceDetails;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.apticks.nextclickuser.utils.UserData;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.DOCTOR_BOOKINGS_HISTORY;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class OnDemandServiceBookingHistoryFragment extends Fragment {

    private TextView tvError;
    private PreferenceManager preferenceManager;
    private RecyclerView rvOnDemandServiceBookingHistoryDetails;

    private LinkedList<GetServiceDetails> listOfGetServiceDetails;
    private LinkedList<DoctorBookingHistoryDetails> listOfDoctorBookingHistoryDetails;

    private int serviceId = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_on_demand_service_booking_history_fragment, container, false);
        initializeUi(view);
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        rvOnDemandServiceBookingHistoryDetails = view.findViewById(R.id.rvOnDemandServiceBookingHistoryDetails);

        if (getActivity() != null)
            preferenceManager = new PreferenceManager(getActivity());
    }

    private void prepareDetails() {
        listOfGetServiceDetails = UserData.getInstance().getListOfGetServiceDetails();
        for (int index = 0; index < listOfGetServiceDetails.size(); index++) {
            GetServiceDetails getServiceDetails = listOfGetServiceDetails.get(index);
            if (getServiceDetails != null) {
                String name = getServiceDetails.getName();
                if (name.equalsIgnoreCase("Services Booking")) {
                    serviceId = getServiceDetails.getId();
                }
            }
        }

        if (serviceId != -1) {
            if (getActivity() != null) {
                serviceCallForOnDemandServiceBookingsHistory();
            }
        }
    }

    private void serviceCallForOnDemandServiceBookingsHistory() {
        DialogOpener.dialogOpener(getActivity());
        String url = DOCTOR_BOOKINGS_HISTORY;
        String token = preferenceManager.getString(TOKEN_KEY);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("service_id", serviceId);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        BookingsDoctorsHistoryResponse bookingsDoctorsHistoryResponse = new Gson().fromJson(response.toString(), BookingsDoctorsHistoryResponse.class);
                        if (bookingsDoctorsHistoryResponse != null) {
                            boolean status = bookingsDoctorsHistoryResponse.getStatus();
                            if (status) {
                                listOfDoctorBookingHistoryDetails = bookingsDoctorsHistoryResponse.getListOfDoctorBookingHistoryDetails();
                                if (listOfDoctorBookingHistoryDetails != null) {
                                    if (listOfDoctorBookingHistoryDetails.size() != 0) {
                                        listIsFull();
                                        initializeAdapter();
                                    } else {
                                        listIsEmpty();
                                    }
                                } else {
                                    listIsEmpty();
                                }
                            }
                        }
                    }
                    DialogOpener.dialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    DialogOpener.dialog.dismiss();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    headers.put("X_AUTH_TOKEN", token);
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            RetryPolicy policy = new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjectRequest.setRetryPolicy(policy);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception exception) {
            exception.printStackTrace();
            DialogOpener.dialog.dismiss();
        }
    }

    private void initializeAdapter() {
        OnDemandServiceBookingHistoryFragmentAdapter onDemandServiceBookingHistoryFragmentAdapter = new OnDemandServiceBookingHistoryFragmentAdapter(getActivity(), listOfDoctorBookingHistoryDetails,serviceId);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvOnDemandServiceBookingHistoryDetails.setLayoutManager(layoutManager);
        rvOnDemandServiceBookingHistoryDetails.setItemAnimator(new DefaultItemAnimator());
        rvOnDemandServiceBookingHistoryDetails.setAdapter(onDemandServiceBookingHistoryFragmentAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvOnDemandServiceBookingHistoryDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvOnDemandServiceBookingHistoryDetails.setVisibility(View.GONE);
    }
}
