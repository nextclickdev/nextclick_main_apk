package com.apticks.nextclickuser.Fragments.OrderHistory;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.UserHistory.OldOrderAdapter;
import com.apticks.nextclickuser.Pojo.OrderHistory.OldOrderPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.*;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class OldOrderFragment extends Fragment {
    private View view;
    private RecyclerView recycler_view_old_order;
    private Context mContext;
    ArrayList<OldOrderPojo> oldOrderPojoArrayList ;
    PreferenceManager preferenceManager;
    String token;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_old_history, container, false);
        mContext = getActivity();

        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
        // set layout in recycleView
        initView();
        //Setting Old Order Details
        oldorderDetails();
        return view;
    }

    private void initView() {
        recycler_view_old_order = view.findViewById(R.id.recycler_view_old_order);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                    // Change this value (default=25f)
                    private static final float SPEED = 300f;
                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }
                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }
        };
        recycler_view_old_order.setLayoutManager(linearLayoutManager);
    }

    private void oldorderDetails() {
        String user_id= preferenceManager.getString("user_id");
        String tempUrl = Food_Orders_History_Past + "past/"+user_id;
        Log.d("old order url",tempUrl);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(tempUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("OldOrder", "" + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("status")) {
                        JSONArray jsonArrayData = jsonObject.getJSONArray("data");
                        if (jsonArrayData.length() > 0) {
                            oldOrderPojoArrayList = new ArrayList<>();
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);

                                OldOrderPojo oldOrderpojo = new OldOrderPojo();

                                oldOrderpojo.setId(jsonObjectData.getInt("id"));
                                oldOrderpojo.setDiscount(jsonObjectData.getInt("discount"));
                                oldOrderpojo.setDelivery_fee(jsonObjectData.getInt("delivery_fee"));
                                oldOrderpojo.setTax(jsonObjectData.getInt("tax"));
                                oldOrderpojo.setTotal(jsonObjectData.getInt("total"));
                                oldOrderpojo.setOrder_track(jsonObjectData.getInt("order_track"));
                                oldOrderpojo.setOrder_status(jsonObjectData.getString("order_status"));
                                oldOrderpojo.setCreated_at(jsonObjectData.getString("created_at"));

                                JSONObject jsonObjectVendor = jsonObjectData.getJSONObject("vendor");
                                oldOrderpojo.setName(jsonObjectVendor.getString("name"));

                                oldOrderpojo.setVendorEmailId(jsonObjectVendor.getString("email"));

                                JSONArray jsonArrayNumber = jsonObjectVendor.getJSONArray("numbers");
                                if (jsonArrayNumber.length() >= 1) {
                                    JSONObject jsonObjectNumber = jsonArrayNumber.getJSONObject(0);
                                    String number = jsonObjectNumber.getString("number");
                                    oldOrderpojo.setVendorMobileNumber(number);
                                }

                                oldOrderPojoArrayList.add(oldOrderpojo);
                                OldOrderAdapter oldOrderAdapter = new OldOrderAdapter(getActivity(), oldOrderPojoArrayList);
                                recycler_view_old_order.setAdapter(oldOrderAdapter);
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.e("JSONException", "" + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
