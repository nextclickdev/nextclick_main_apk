package com.apticks.nextclickuser.support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.apticks.nextclickuser.R;


public class SpinnerSupportTypeAdapter extends BaseAdapter {
    Context context;
    String[] supportedFeedbackTypes;
    LayoutInflater inflter;

    public SpinnerSupportTypeAdapter(Context applicationContext, String[] supportedFeedbackTypes) {
        this.context = applicationContext;
        this.supportedFeedbackTypes = supportedFeedbackTypes;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return supportedFeedbackTypes.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_item_support_type, null);//spinner_item_status_type
        TextView names = (TextView) view.findViewById(R.id.tv_support_type);
        names.setText(supportedFeedbackTypes[i]);
        return view;
    }
}

