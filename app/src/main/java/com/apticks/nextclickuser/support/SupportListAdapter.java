package com.apticks.nextclickuser.support;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.apticks.nextclickuser.R;

import java.util.ArrayList;

public class SupportListAdapter extends RecyclerView.Adapter<SupportListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SupportModel> supportlist;

    public SupportListAdapter(Context context, ArrayList<SupportModel> supportlist) {
        this.context =context;
        this.supportlist = supportlist;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.supportlist, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SupportModel supportModel = supportlist.get(position);
        String title = String.valueOf(Html.fromHtml(supportModel.getTitle()));
        String message =  String.valueOf(Html.fromHtml(supportModel.getMessage()));
        holder.tvTitle.setText(title);
        holder.tvMessage.setText(message);
        holder.tvTicketID.setText(supportModel.getId());
        holder.tv_created_at.setText(supportModel.getCreated_at());
        if(supportModel.getStaus().equalsIgnoreCase("2"))
        {
            holder.tvStatus.setTextColor(0XFF35c534);
            holder.tvStatus.setText("Closed");
        }
        else if (supportModel.getStaus().equalsIgnoreCase("1"))
        {
            holder.tvStatus.setTextColor(0XFFff5251);
            holder.tvStatus.setText("Open");
        }else{
            holder.tvStatus.setTextColor(0XFF35c534);
            holder.tvStatus.setText("Closed");
        }


        holder.cardview_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, SupportActivity.class);
                i.putExtra("position",1);
                i.putExtra("supportmodel",supportlist.get(position));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return supportlist.size();
    }

    public void setrefresh(ArrayList<SupportModel> supportlist) {
        this.supportlist=supportlist;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvTitle,tvMessage,tvTicketID,tvStatus,tv_created_at,tv_short_date;
        private LinearLayout cardview_main;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            cardview_main = itemView.findViewById(R.id.cardview_main);
            tvTicketID= itemView.findViewById(R.id.tvTicketID);
            tvStatus= itemView.findViewById(R.id.tvStatus);
            tv_created_at= itemView.findViewById(R.id.tvDate);
        }
    }
}
