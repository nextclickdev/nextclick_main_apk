package com.apticks.nextclickuser.support;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.REQUESTTYPE;
import static com.apticks.nextclickuser.Config.Config.SUPPORTCREATE;
import static com.apticks.nextclickuser.Config.Config.SUPPORTUODATE;
import static com.apticks.nextclickuser.Constants.Constants.APP_ID_VALUE;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


public class SupportActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    String[] supportedFeedbackTypes={"Payment Issue","Other"};
    private CustomDialog mCustomDialog;
    private PreferenceManager preferenceManager;
    private EditText et_subject,et_message;
    private Button btn_submit;
    Spinner spin;
    private String spinselectedposition="1";
    private Context mContext;
    private ArrayList<String> requesttypelist;
    private ArrayList<SupportRequestModel> requestlist;
    private SupportModel supportModel;
    private int position;
    String supporturl=SUPPORTCREATE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        ImageView img_back=findViewById(R.id.img_back);
        TextView tv_header= findViewById(R.id.tv_header);
        tv_header.setText("Customer Suport");
        tv_header.setAllCaps(true);
        FrameLayout cart_layout=findViewById(R.id.cart_layout);
        cart_layout.setVisibility(View.GONE);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mContext= SupportActivity.this;
         spin = (Spinner) findViewById(R.id.spinner_support_type);
        et_subject=findViewById(R.id.et_subject);
        et_message=findViewById(R.id.et_message);
        btn_submit=findViewById(R.id.btn_submit);
        spin.setOnItemSelectedListener(this);
        requesttypelist=new ArrayList<>();
        requestlist=new ArrayList<>();

        mCustomDialog=new CustomDialog(SupportActivity.this);
        preferenceManager=new PreferenceManager(SupportActivity.this);

         position=getIntent().getIntExtra("position",0);
        if (position==1){
            supportModel= (SupportModel) getIntent().getSerializableExtra("supportmodel");
            et_subject.setText(supportModel.getSubject());
            et_message.setText(supportModel.getMessage());
            supporturl=SUPPORTUODATE;
        }

      //  SpinnerSupportTypeAdapter adapter=new SpinnerSupportTypeAdapter(SupportActivity.this, supportedFeedbackTypes);
       // spin.setAdapter(adapter);

        getRequesttypes();
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_subject.getText().toString().isEmpty()|| et_subject.getText().length()==0){
                    Toast.makeText(SupportActivity.this, "Please enter subject", Toast.LENGTH_SHORT).show();
                }else {
                    if (et_message.getText().length()==0 || et_message.getText().toString().isEmpty()){
                        Toast.makeText(SupportActivity.this, "Please enter Message", Toast.LENGTH_SHORT).show();
                    }else {
                        setsupport();
                    }
                }
            }
        });
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinselectedposition=requestlist.get(position).getId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void setsupport(){
        Map<String, String> uploadMap = new HashMap<>();

        if (position==1){
            uploadMap.put("id", supportModel.getId());
        }
        uploadMap.put("request_type_id", spinselectedposition);
        uploadMap.put("mobile", preferenceManager.getString("number"));
        uploadMap.put("email", ""+preferenceManager.getString("email"));
        uploadMap.put("subject", ""+et_subject.getText().toString().trim());
        uploadMap.put("message", ""+et_message.getText().toString().trim());

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaa request support  "+json.toString());
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(SupportActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, supporturl,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject support "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");


                            if (status){
                                String message=jsonObject.getString("message");
                                Toast.makeText(SupportActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else
                                Toast.makeText(SupportActivity.this, ""+jsonObject.getString("message"), Toast.LENGTH_SHORT).show();


                        } catch (JSONException e) {

                            Toast.makeText(SupportActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                //  Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN",preferenceManager.getString(TOKEN_KEY));
                map.put("APP_ID",APP_ID_VALUE);

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void getRequesttypes(){
        requesttypelist.clear();

        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REQUESTTYPE,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {

                        try {
                            if (mCustomDialog!=null)
                            {
                                if (mCustomDialog.isShowing())
                                {
                                    try{
                                        mCustomDialog.dismiss();
                                    }catch (IllegalArgumentException e){

                                    }

                                }
                            }
                            //  mCustomDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");

                                JSONArray jsonArray=jsonObject.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                    SupportRequestModel supportRequestModel=new SupportRequestModel();
                                    supportRequestModel.setId(jsonObject1.getString("id"));
                                    supportRequestModel.setTitle(jsonObject1.getString("title"));
                                    supportRequestModel.setDesc(jsonObject1.getString("desc"));
                                    supportRequestModel.setCreated_at(jsonObject1.getString("created_at"));
                                    supportRequestModel.setUpdated_at(jsonObject1.getString("updated_at"));
                                    supportRequestModel.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    supportRequestModel.setStatus(jsonObject1.getString("status"));

                                    requestlist.add(supportRequestModel);
                                    requesttypelist.add(jsonObject1.getString("title"));
                                }

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                        android.R.layout.simple_spinner_item, requesttypelist);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spin.setAdapter(adapter);

                                if (position==1){
                                    for (int i = 0; i < requestlist.size(); i++) {
                                        if (supportModel.getRequest_type_id().equalsIgnoreCase(requestlist.get(i).getId())) {
                                            spinselectedposition = requestlist.get(i).getId();
                                            spin.setSelection((i), true);
                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            if (mCustomDialog!=null)
                            {
                                if (mCustomDialog.isShowing())
                                {
                                    try{
                                        mCustomDialog.dismiss();
                                    }catch (IllegalArgumentException e1){

                                    }
                                }
                            }
                            //   mCustomDialog.dismiss();
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mCustomDialog!=null)
                {
                    if (mCustomDialog.isShowing())
                    {
                        try{
                            mCustomDialog.dismiss();
                        }catch (IllegalArgumentException e){

                        }
                    }
                }
                //  mCustomDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN",preferenceManager.getString(TOKEN_KEY));
                map.put("APP_ID",APP_ID_VALUE);

                return map;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}