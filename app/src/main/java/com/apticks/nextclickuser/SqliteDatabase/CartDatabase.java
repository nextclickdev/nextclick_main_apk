package com.apticks.nextclickuser.SqliteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.apticks.nextclickuser.products.model.DatabaseProductsModel;
import com.apticks.nextclickuser.products.model.ProductsModel;

import java.util.ArrayList;

public class CartDatabase extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "NextClickdb";
    public static final String ECOM_CART_TABLE_NAME = "ecommercecart";

    public static final String CART_COLUMN_ITEM_ID = "productId";
    public static final String CART_COLUMN_ITEM_NAME = "productName";
    public static final String CART_COLUMN_ITEM_IMAGE = "productImage";
    public static final String CART_COLUMN_ITEM_PRICE = "productPrice";
    public static final String CART_COLUMN_ITEM_QUANTITY = "productQuantity";
    public static final String CART_COLUMN_INSTRUCTION_FOOD = "vendorvarientId";
    public static final String CART_COLUMN_VENDOR_ID = "vendorId";

    Context context;
    public CartDatabase(Context context) {
        super(context,DATABASE_NAME,null,1);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(
                "create table  " +ECOM_CART_TABLE_NAME+
                        "("+CART_COLUMN_ITEM_ID+" text primary key," +
                        CART_COLUMN_ITEM_NAME+" text," +
                        CART_COLUMN_ITEM_IMAGE+" text," +
                        CART_COLUMN_ITEM_PRICE+" text," +
                        CART_COLUMN_ITEM_QUANTITY+" text," +
                        CART_COLUMN_INSTRUCTION_FOOD +"text," +
                        CART_COLUMN_VENDOR_ID +"text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ECOM_CART_TABLE_NAME);

        onCreate(sqLiteDatabase);
    }

    public boolean insertproduct(ProductsModel productsModel, String vendorvarientid, int totalQuantity,String price) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CART_COLUMN_ITEM_ID, productsModel.getId());
        contentValues.put(CART_COLUMN_VENDOR_ID, productsModel.getVendor_user_id());
        contentValues.put(CART_COLUMN_ITEM_NAME, productsModel.getName());
        contentValues.put(CART_COLUMN_ITEM_IMAGE, productsModel.getImage());
        contentValues.put(CART_COLUMN_ITEM_PRICE, price);
        contentValues.put(CART_COLUMN_ITEM_QUANTITY, ""+totalQuantity);
        contentValues.put(CART_COLUMN_INSTRUCTION_FOOD, vendorvarientid);
        //db.insert("foodcart", null, contentValues);
        System.out.println("aaaaaaaa table insert");

       // db.insert(ECOM_CART_TABLE_NAME, null, contentValues);
        if (db.insert(ECOM_CART_TABLE_NAME, null, contentValues) == -1) {
            return false;
        } else {
            return true;
        }
        //return true;

    }

    public Cursor getFoodItem(String itemId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+ECOM_CART_TABLE_NAME+" where productId=" + itemId + "", null);
        return res;
    }

    public boolean updateFoodItem(String itemId, String itemName,  String itemPrice, String itemQuantity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("productId", itemId);
        contentValues.put("productName", itemName);
        contentValues.put("productPrice", itemPrice);
        contentValues.put("itemQuantity", itemQuantity);
        db.update(ECOM_CART_TABLE_NAME, contentValues, "productId = ? ", new String[]{itemId});
        return true;
    }
    public Integer deleteFoodItem(String itemId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(ECOM_CART_TABLE_NAME,
                "itemId = ? ",
                new String[]{itemId});
    }
    public ArrayList<DatabaseProductsModel> getAllFoodItems() {
        ArrayList<DatabaseProductsModel> array_list = new ArrayList<DatabaseProductsModel>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+ECOM_CART_TABLE_NAME, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            DatabaseProductsModel productsModel=new DatabaseProductsModel();
            productsModel.setProductId(res.getString(res.getColumnIndex(CART_COLUMN_ITEM_ID)));
            productsModel.setProductName(res.getString(res.getColumnIndex(CART_COLUMN_ITEM_NAME)));
            productsModel.setProductImage(res.getString(res.getColumnIndex(CART_COLUMN_ITEM_IMAGE)));
            productsModel.setProductPrice(res.getString(res.getColumnIndex(CART_COLUMN_ITEM_PRICE)));
            productsModel.setProductQuantity(res.getString(res.getColumnIndex(CART_COLUMN_ITEM_QUANTITY)));
            productsModel.setVendorvarientId(res.getString(res.getColumnIndex(CART_COLUMN_INSTRUCTION_FOOD)));
            productsModel.setVendorId(res.getString(res.getColumnIndex(CART_COLUMN_VENDOR_ID)));
            array_list.add(productsModel);
            res.moveToNext();
        }
        return array_list;
    }

    public boolean updateFoodItem(ProductsModel productsModel, String quantity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("productId", productsModel.getId());
        contentValues.put("productName", productsModel.getName());
       // contentValues.put("productPrice", itemPrice);
        contentValues.put("itemQuantity", quantity);
        db.update(ECOM_CART_TABLE_NAME, contentValues, "productId = ? ", new String[]{productsModel.getId()});
        return true;
    }
}
