package com.apticks.nextclickuser.SqliteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.apticks.nextclickuser.products.model.DatabaseProductsModel;

import java.util.ArrayList;

public class AddressDataBaseHelper {
    DataBaseClass databaseHelper;

    public AddressDataBaseHelper(Context context) {
        databaseHelper = new DataBaseClass(context);

    }

    public long insert(String name, String address, String contact, String email, String password, String conpassword,int userAddressID) {
        SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseClass.NAME, name);
        contentValues.put(DataBaseClass.AGE, address);
        contentValues.put(DataBaseClass.CONTACTNUMBER, contact);
        contentValues.put(DataBaseClass.EMAIL, email);
        contentValues.put(DataBaseClass.PASSWORD, password);
        contentValues.put(DataBaseClass.CPASSWORD, conpassword);
        contentValues.put(DataBaseClass.CADDRESSID, userAddressID);

        long id = sqLiteDatabase.insert(DataBaseClass.TABLE_NAME, null, contentValues);
        databaseHelper.close();
        return id;
    }

    public String showAllDatamain() {
        SQLiteDatabase sqLiteDatabase = databaseHelper.getReadableDatabase();

        String[] columns = {DataBaseClass.NAME, DataBaseClass.AGE, DataBaseClass.CONTACTNUMBER, DataBaseClass.EMAIL};
        Cursor cursor = sqLiteDatabase.query(DataBaseClass.TABLE_NAME, columns, null, null, null, null, null);

        StringBuffer stringBuffer = new StringBuffer();

        while (cursor.moveToNext()) {
            String get_name_from_db = cursor.getString(0);
            String get_address_from_db = cursor.getString(1);
            String get_phone_from_db = cursor.getString(2);
            String get_email_from_db = cursor.getString(3);
            stringBuffer.append(" \n Name            : " + get_name_from_db + " \n Address          : " + get_address_from_db + " \n Mobile No.   : " + get_phone_from_db + " \n Email            : " + get_email_from_db + " # ");

            /*  stringBuffer.append(get_un_from_db+" || "+get_pwd_from_db+" ||  "+get_cpwd_from_db+" || "+get_fullname_from_db+" || "+get_phone_db+" || "+get_dob_from_db+" "+get_address_from_db+" ||
          "+get_gender_from_db+" || "+get_usertype_from_db+" # \n") ;
        */
        }
        return stringBuffer.toString();
    }

    public int deleteSectCartItems(String itemSecId) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();


        return db.delete(DataBaseClass.TABLE_NAME,
                "CAdressId = ? ",
                new String[] { itemSecId });



    }

    public String showSelectAddresID() {
        SQLiteDatabase sqLiteDatabase = databaseHelper.getReadableDatabase();

        String[] columns = {DataBaseClass.CADDRESSID};
        Cursor cursor = sqLiteDatabase.query(DataBaseClass.TABLE_NAME, columns, null, null, null, null, null);

        StringBuffer stringBuffer = new StringBuffer();

        while (cursor.moveToNext()) {
            String get_name_from_db = cursor.getString(0);
            stringBuffer.append( get_name_from_db +  " # ");

            /*  stringBuffer.append(get_un_from_db+" || "+get_pwd_from_db+" ||  "+get_cpwd_from_db+" || "+get_fullname_from_db+" || "+get_phone_db+" || "+get_dob_from_db+" "+get_address_from_db+" ||
          "+get_gender_from_db+" || "+get_usertype_from_db+" # \n") ;
        */
        }
        return stringBuffer.toString();
    }



    public long getNumberOfRowCount() {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, DataBaseClass.TABLE_NAME);
        db.close();
        return count;
    }

    public int getRowCount() {
        String countQuery = "SELECT  * FROM " + DataBaseClass.TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void clearDatabase() {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String clearDBQuery = "DELETE FROM "+DataBaseClass.TABLE_NAME;
        db.execSQL(clearDBQuery);
    }

    static class DataBaseClass extends SQLiteOpenHelper {
        private static final String DATABASE_NAME = "REGISTERNEWADDRESS";
        private static final String TABLE_NAME = "REGISTER_ADDRESS";
        private static final String UID = "_id";
        private static final int DATABASE_VERSION = 5;
        private static final String NAME = "Name";
        private static final String AGE = "Age";
        private static final String CONTACTNUMBER = "Contactnumber";
        private static final String EMAIL = "Email";
        private static final String PASSWORD = "Password";
        private static final String CPASSWORD = "CPassword";
        private static final String CADDRESSID = "CAdressId";

        //private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME + " VARCHAR(255)," + AGE + " VARCHAR(255)," + CONTACTNUMBER + " VARCHAR(255)," + EMAIL + " VARCHAR(255)," + PASSWORD + " VARCHAR(255)," + CPASSWORD + " VARCHAR(255));";
        private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME + " VARCHAR(255)," + AGE + " VARCHAR(255)," + CONTACTNUMBER + " VARCHAR(255)," + EMAIL + " VARCHAR(255)," + PASSWORD + " VARCHAR(255)," + CPASSWORD + " VARCHAR(255)," +CADDRESSID + " INTEGER);";
        private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        private Context context;

        public DataBaseClass(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(DROP_TABLE);
                onCreate(db);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /*public void clearDatabase(String TABLE_NAME) {
        SQLiteDatabase db = this.getReadableDatabase();
        String clearDBQuery = "DELETE FROM "+TABLE_NAME;
        db.execSQL(clearDBQuery);
    }*/
}