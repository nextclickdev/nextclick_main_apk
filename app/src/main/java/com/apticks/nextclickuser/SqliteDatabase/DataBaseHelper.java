package com.apticks.nextclickuser.SqliteDatabase;

import android.content.Context;
import android.database.Cursor;

public class DataBaseHelper extends CartDBHelper {
    Context mContext;
    CartDBHelper cartDBHelper ;

    public DataBaseHelper(Context context) {
        super(context);
        this.mContext = context;
        cartDBHelper = new CartDBHelper(mContext);
    }

    public int getTotalNumberOfFoodItems(){
        int numberOfItems = cartDBHelper.numberOfFoodCartRows();

        if (numberOfItems==0){

        }else {

        }
        return numberOfItems;
    }

    public double getTotalCartAmount(){
        Cursor rs = cartDBHelper.getAllFoodItems();
        int v =cartDBHelper.numberOfFoodCartRows();

        Cursor rsSect = cartDBHelper.getAllSectFoodItems();
        int vSect =cartDBHelper.numberOfSectFoodCartRows();


        rs.moveToFirst();
        double tempTotal=0;
        int tempCountTotal=0;
        while (rs!=null && tempCountTotal<v) {
            String price = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_PRICE));
            String quantity = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_QUANTITY));

            double totalSingleItem = Double.parseDouble(price)*Double.parseDouble(quantity);
            //double totalSingleItem = Integer.parseInt(price)*Integer.parseInt(quantity);

            tempTotal=tempTotal+totalSingleItem;

            rs.moveToNext();
            tempCountTotal++;
        }

        tempCountTotal=0;
        rsSect.moveToFirst();
        while (rsSect!=null&&tempCountTotal<vSect){
            String price = rsSect.getString(rsSect.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_PRICE));
            int quantity = rsSect.getInt(rsSect.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_QUANTITY));

            //double totalSingleItem = Double.parseDouble(price)*Double.parseDouble(quantity);
            double totalSingleItem = Double.parseDouble(price);
            //Removed For Removing Section Price Item
            //tempTotal =tempTotal+totalSingleItem;

            rsSect.moveToNext();
            tempCountTotal++;
        }

        return tempTotal;
    }

    public boolean getVendorIdPresence(int vendorIdA){
        Cursor rs = cartDBHelper.getAllFoodItems();
        int v =cartDBHelper.numberOfFoodCartRows();
        rs.moveToFirst();
        //int vendorIdA = rs.getInt(rs.getColumnIndex(cartDBHelper.CART_COLUMN_VENDOR_ID));
        double tempVendorId;
        int tempCountTotal=0;
        while (rs!=null && tempCountTotal<v){
            //String price = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_PRICE));
            int quantity = rs.getInt(rs.getColumnIndex(cartDBHelper.CART_COLUMN_VENDOR_ID));

            tempVendorId = quantity;

            if (vendorIdA!=quantity){
                cartDBHelper.deleteCartTable();
                return true;
            }
            rs.moveToNext();
            tempCountTotal++;
        }
        return true;
    };

    public boolean getItemIdPresence(int vendorIdA){
        Cursor rs = cartDBHelper.getAllFoodItems();
        int v =cartDBHelper.numberOfFoodCartRows();
        rs.moveToFirst();
        //int vendorIdA = rs.getInt(rs.getColumnIndex(cartDBHelper.CART_COLUMN_VENDOR_ID));
        double tempVendorId;
        int tempCountTotal=0;
        while (rs!=null && tempCountTotal<v){
            //String price = rs.getString(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_PRICE));
            int quantity = rs.getInt(rs.getColumnIndex(cartDBHelper.CART_COLUMN_ITEM_ID));

            tempVendorId = quantity;

            if (vendorIdA==quantity){
                //cartDBHelper.deleteCartTable();
                return true;
            }
            rs.moveToNext();
            tempCountTotal++;
        }
        return false;
    };
}
