package com.apticks.nextclickuser.SqliteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.apticks.nextclickuser.products.model.DatabaseProductsModel;
import com.apticks.nextclickuser.products.model.ProductsModel;
import com.apticks.nextclickuser.products.model.VarientModel;

import java.util.ArrayList;

public class CartDataBaseHelper {
    DataBaseClass databaseHelper;

    public CartDataBaseHelper(Context context) {
        databaseHelper = new DataBaseClass(context);

    }
    public ArrayList<DatabaseProductsModel> getAllFoodItems() {
        ArrayList<DatabaseProductsModel> array_list = new ArrayList<DatabaseProductsModel>();

        //hp = new HashMap();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+ DataBaseClass.TABLE_NAME, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            DatabaseProductsModel productsModel=new DatabaseProductsModel();
            productsModel.setProductId(res.getString(0));
            productsModel.setProductName(res.getString(1));
            productsModel.setProductImage(res.getString(2));
            productsModel.setProductPrice(res.getString(3));
            productsModel.setProductQuantity(res.getString(4));
            productsModel.setVendorvarientId(res.getString(5));
            productsModel.setVendorId(res.getString(6));
            array_list.add(productsModel);
            res.moveToNext();
        }
        return array_list;
    }
    public long insert(ProductsModel productsModel, String vendorvarientid, int totalQuantity, String price) {
        SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseClass.PRODUCT_ID, productsModel.getId());
        contentValues.put(DataBaseClass.PRODUCT_NAME, productsModel.getName());
        contentValues.put(DataBaseClass.PRODUCT_IMAGE, productsModel.getImage());
        contentValues.put(DataBaseClass.PRODUCT_PRICE, price);
        contentValues.put(DataBaseClass.PRODUCT_QUANTITY, ""+totalQuantity);
        contentValues.put(DataBaseClass.PRODUCT_VARIENT_ID, vendorvarientid);
        contentValues.put(DataBaseClass.VENDOR_ID, productsModel.getVendor_user_id());


        long id = sqLiteDatabase.insert(DataBaseClass.TABLE_NAME, null, contentValues);
        databaseHelper.close();
        return id;
    }
    public boolean updateFoodItem(ProductsModel productsModel, String quantity, VarientModel varientModel,String totalamount) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseClass.PRODUCT_ID, productsModel.getId());
        contentValues.put(DataBaseClass.PRODUCT_NAME, productsModel.getName());
         contentValues.put(DataBaseClass.PRODUCT_PRICE, totalamount);
        contentValues.put(DataBaseClass.PRODUCT_QUANTITY, quantity);
        db.update(DataBaseClass.TABLE_NAME, contentValues, DataBaseClass.PRODUCT_VARIENT_ID+" = ? ", new String[]{varientModel.getId()});
        return true;
    }
    public Integer deleteFoodItem(String itemId) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        return db.delete(DataBaseClass.TABLE_NAME,
                DataBaseClass.PRODUCT_VARIENT_ID+" = ? ",
                new String[]{itemId});
    }
    public String showAllDatamain() {
        /*SQLiteDatabase sqLiteDatabase = databaseHelper.getReadableDatabase();

        String[] columns = {DataBaseClass.NAME, DataBaseClass.AGE, DataBaseClass.CONTACTNUMBER, DataBaseClass.EMAIL};
        Cursor cursor = sqLiteDatabase.query(DataBaseClass.TABLE_NAME, columns, null, null, null, null, null);

        StringBuffer stringBuffer = new StringBuffer();

        while (cursor.moveToNext()) {
            String get_name_from_db = cursor.getString(0);
            String get_address_from_db = cursor.getString(1);
            String get_phone_from_db = cursor.getString(2);
            String get_email_from_db = cursor.getString(3);
            stringBuffer.append(" \n Name            : " + get_name_from_db + " \n Address          : " + get_address_from_db + " \n Mobile No.   : " + get_phone_from_db + " \n Email            : " + get_email_from_db + " # ");

            *//*  stringBuffer.append(get_un_from_db+" || "+get_pwd_from_db+" ||  "+get_cpwd_from_db+" || "+get_fullname_from_db+" || "+get_phone_db+" || "+get_dob_from_db+" "+get_address_from_db+" ||
          "+get_gender_from_db+" || "+get_usertype_from_db+" # \n") ;
        *//*
        }
        return stringBuffer.toString();*/
        return "";
    }

    public int deleteSectCartItems(String itemSecId) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();


        return db.delete(DataBaseClass.TABLE_NAME,
                "CAdressId = ? ",
                new String[] { itemSecId });

    }

    public String showSelectAddresID() {
        /*SQLiteDatabase sqLiteDatabase = databaseHelper.getReadableDatabase();

        String[] columns = {DataBaseClass.CADDRESSID};
        Cursor cursor = sqLiteDatabase.query(DataBaseClass.TABLE_NAME, columns, null, null, null, null, null);

        StringBuffer stringBuffer = new StringBuffer();

        while (cursor.moveToNext()) {
            String get_name_from_db = cursor.getString(0);
            stringBuffer.append( get_name_from_db +  " # ");

            *//*  stringBuffer.append(get_un_from_db+" || "+get_pwd_from_db+" ||  "+get_cpwd_from_db+" || "+get_fullname_from_db+" || "+get_phone_db+" || "+get_dob_from_db+" "+get_address_from_db+" ||
          "+get_gender_from_db+" || "+get_usertype_from_db+" # \n") ;
        *//*
        }
        return stringBuffer.toString();*/
        return "";
    }



    public long getNumberOfRowCount() {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, DataBaseClass.TABLE_NAME);
        db.close();
        return count;
    }

    public int getRowCount() {
        String countQuery = "SELECT  * FROM " + DataBaseClass.TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void clearDatabase() {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        String clearDBQuery = "DELETE FROM "+ DataBaseClass.TABLE_NAME;
        db.execSQL(clearDBQuery);
    }

    static class DataBaseClass extends SQLiteOpenHelper {
        public static final String PRODUCT_ID = "productId";
        public static final String PRODUCT_NAME = "productName";
        public static final String PRODUCT_IMAGE = "productImage";
        public static final String PRODUCT_PRICE = "productPrice";
        public static final String PRODUCT_QUANTITY = "productQuantity";
        public static final String PRODUCT_VARIENT_ID = "vendorvarientId";
        public static final String VENDOR_ID = "vendorId";

        private static final String DATABASE_NAME = "CARTADD";
        private static final String TABLE_NAME = "ECOMPRODUCTS";
        private static final int DATABASE_VERSION = 5;

        //private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME + " VARCHAR(255)," + AGE + " VARCHAR(255)," + CONTACTNUMBER + " VARCHAR(255)," + EMAIL + " VARCHAR(255)," + PASSWORD + " VARCHAR(255)," + CPASSWORD + " VARCHAR(255));";
        private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + PRODUCT_ID + " VARCHAR(255) , " + PRODUCT_NAME + " VARCHAR(255)," + PRODUCT_IMAGE + " VARCHAR(255)," + PRODUCT_PRICE + " VARCHAR(255)," + PRODUCT_QUANTITY + " VARCHAR(255)," + PRODUCT_VARIENT_ID + " VARCHAR(255) PRIMARY KEY ," + VENDOR_ID + " VARCHAR(255) );";
        private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        private Context context;

        public DataBaseClass(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(DROP_TABLE);
                onCreate(db);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /*public void clearDatabase(String TABLE_NAME) {
        SQLiteDatabase db = this.getReadableDatabase();
        String clearDBQuery = "DELETE FROM "+TABLE_NAME;
        db.execSQL(clearDBQuery);
    }*/
}