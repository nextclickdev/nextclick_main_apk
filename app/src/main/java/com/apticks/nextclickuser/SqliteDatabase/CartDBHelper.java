package com.apticks.nextclickuser.SqliteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.apticks.nextclickuser.Pojo.PLACES.PlacePojo;

public class CartDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "NextClick.db";
    public static final String FOOD_CART_TABLE_NAME = "foodcart";
    public static final String ECOM_CART_TABLE_NAME = "ecommercecart";
    public static final String ECOM_SEC_CART_TABLE_NAME = "items";
    public static final String RECENT_PLACES_TABLE_NAME = "places";

    public static final String CART_COLUMN_ITEM_ID = "itemId";
    public static final String CART_COLUMN_ITEM_NAME = "itemName";
    public static final String CART_COLUMN_ITEM_IMAGE = "itemImage";
    public static final String CART_COLUMN_ITEM_PRICE = "itemPrice";
    public static final String CART_COLUMN_ITEM_QUANTITY = "itemQuantity";
    public static final String CART_COLUMN_ITEMS = "foodItems";
    public static final String CART_COLUMN_SEC_ITEMS = "secItems";
    public static final String CART_COLUMN_SEC_ITEMS_ID = "secItemId";
    public static final String CART_COLUMN_INSTRUCTION_FOOD = "instructions";
    public static final String CART_COLUMN_VENDOR_ID = "vendorId";


    public static final String PLACE_COLUMN_ID = "placeid";
    public static final String PLACE_LATTITUDE = "lattitude";
    public static final String PLACE_LONGITUDE = "longitude";
    public static final String PLACE_NAME = "placename";


    private HashMap hp;
    CartDBHelper dataBaseClass;


    public CartDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table foodcart " +
                        "(itemId text primary key,vendorId INTEGER, itemName text,itemImage text, itemPrice text," +
                        "itemQuantity text,instructions text,foodItems text, secItems text,totalQunatity INTEGER)"
        );
        db.execSQL(
                "create table ecommercecart " +
                        "(itemId text primary key,itemName text,itemImage text, itemPrice text,itemQuantity text)"
        );

        db.execSQL(
                "create table items" +
                        "(itemId text, itemQuantity INTEGER, itemPrice text,secItemId text, itemName text," +
                        "FOREIGN KEY(itemId) REFERENCES foodcart(itemId))"
        );


        //places
        db.execSQL(
                "create table " + RECENT_PLACES_TABLE_NAME +
                        "(id INTEGER primary key AUTOINCREMENT,placeid text, lattitude text,longitude text, placename text)"
        );



        /*primary key*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS foodcart");
        db.execSQL("DROP TABLE IF EXISTS ecommercecart");
        db.execSQL("DROP TABLE IF EXISTS items");
        //places
        db.execSQL("DROP TABLE IF EXISTS " + RECENT_PLACES_TABLE_NAME);

        db.execSQL("DROP TABLE IF EXISTS CREATE_TABLE");
        onCreate(db);
    }

    //places insertion

    public boolean insertPlace(String placeid, String lattitude, String longitude, String placename) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PLACE_COLUMN_ID, placeid);
        contentValues.put(PLACE_LATTITUDE, lattitude);
        contentValues.put(PLACE_LONGITUDE, longitude);
        contentValues.put(PLACE_NAME, placename);


        if (db.insert(RECENT_PLACES_TABLE_NAME, null, contentValues) == -1) {
            return false;
        } else {
            return true;
        }

    }

    //place deletion
    public Integer deletePlace(String placeid) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(RECENT_PLACES_TABLE_NAME,
                PLACE_COLUMN_ID + " = ? ",
                new String[]{placeid});
    }

    //place updation
    public boolean updatePlace(String placeid, String lattitude, String longitude, String placename) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PLACE_COLUMN_ID, placeid);
        contentValues.put(PLACE_LATTITUDE, lattitude);
        contentValues.put(PLACE_LONGITUDE, longitude);
        contentValues.put(PLACE_NAME, placename);
        db.update(RECENT_PLACES_TABLE_NAME, contentValues, PLACE_COLUMN_ID + " = ? ", new String[]{placeid});
        return true;
    }

    // get all places
    public List<PlacePojo> getAllPlacesList() {
        SQLiteDatabase db = this.getWritableDatabase();

        List<PlacePojo> expenses_list = new ArrayList<PlacePojo>();
        String selectQuery = "SELECT * FROM " + RECENT_PLACES_TABLE_NAME;

        Cursor cursor = db.rawQuery(selectQuery, null);
        try {
            if (cursor.moveToLast()) {

                do {
                    String id = cursor.getString(cursor.getColumnIndex(PLACE_COLUMN_ID));
                    String name = cursor.getString(cursor.getColumnIndex(PLACE_NAME));
                    String lattitude = cursor.getString(cursor.getColumnIndex(PLACE_LATTITUDE));
                    String longitude = cursor.getString(cursor.getColumnIndex(PLACE_LONGITUDE));
                    PlacePojo placePojo = new PlacePojo();
                    placePojo.setId(id);
                    placePojo.setName(name);
                    Log.d("dbplace name", name);
                    placePojo.setLattitude(lattitude);
                    placePojo.setLongitude(longitude);
                    expenses_list.add(placePojo);
                } while (cursor.moveToPrevious());
            }
        } finally {
            cursor.close();
        }
        return expenses_list;
    }

    //get single place
    public Cursor getPlace(String placeid) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + RECENT_PLACES_TABLE_NAME + " where " + PLACE_COLUMN_ID + "=" + placeid + "", null);
        return res;
    }


    // Food Cart Creation

    public boolean insertFoodCart(String itemId, int vendorId, String itemName,
                                  String itemImage, String itemPrice, String itemQuantity,
                                  String instructionFoodItems, String foodItems, String secItemsFood, int totalQuantity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("itemId", itemId);
        contentValues.put("vendorId", vendorId);
        contentValues.put("itemName", itemName);
        contentValues.put("itemImage", itemImage);
        contentValues.put("itemPrice", itemPrice);
        contentValues.put("itemQuantity", itemQuantity);
        contentValues.put("instructions", instructionFoodItems);
        contentValues.put("foodItems", foodItems);
        contentValues.put("secItems", secItemsFood);
        contentValues.put("totalQunatity", totalQuantity);
        //db.insert("foodcart", null, contentValues);

        if (db.insert("foodcart", null, contentValues) == -1) {
            return false;
        } else {
            return true;
        }
        //return true;

    }

    public boolean insertFoodSectionItems(String itemId, int itemQuantity, String itemPrice, String secItemId, String itemIdF, String secItemName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("itemId", itemId);
        contentValues.put("itemQuantity", itemQuantity);
        contentValues.put("itemPrice", itemPrice);
        contentValues.put("secItemId", secItemId);
        contentValues.put("itemId", itemIdF);
        contentValues.put("itemName", secItemName);


        if (db.insert("items", null, contentValues) == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getFoodItem(String itemId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from foodcart where itemId=" + itemId + "", null);
        return res;
    }


   /* public int numberOfFoodCartRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, FOOD_CART_TABLE_NAME);
        return numRows;
    }*/

    public boolean updateFoodItem(String itemId, String itemName, String itemImage, String itemPrice, String itemQuantity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("itemId", itemId);
        contentValues.put("itemName", itemName);
        contentValues.put("itemImage", itemImage);
        contentValues.put("itemPrice", itemPrice);
        contentValues.put("itemQuantity", itemQuantity);
        db.update("foodcart", contentValues, "itemId = ? ", new String[]{itemId});
        return true;
    }

    public boolean updateFoodItemQuantity(String itemId, String itemQuantity, String foodItem) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        /*contentValues.put("itemId", itemId);
        contentValues.put("itemName", itemName);
        contentValues.put("itemImage", itemImage);
        contentValues.put("itemPrice", itemPrice);
        contentValues.put("itemQuantity", itemQuantity);
        db.update("foodcart", contentValues, "itemId = ? ", new String[] { itemId } );*/

        //db.execSQL("UPDATE DB_TABLE SET YOUR_COLUMN='newValue' WHERE id=6 ");

        ContentValues newValues = new ContentValues();
        newValues.put("itemQuantity", itemQuantity);
        newValues.put("foodItems", foodItem);

        if (db.update("foodcart", newValues, "itemId = ?", new String[]{itemId}) == 1) {
            return true;
        } else {
            return false;
        }

    }

    public Integer deleteFoodItem(String itemId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("foodcart",
                "itemId = ? ",
                new String[]{itemId});
    }

    public void deleteCartTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + "foodcart");

        return;
        /*return db.delete("foodcart",
                "itemId = ? ",
                new String[] { itemId });*/
    }

    public void deleteSectCartTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + "items");

        return;
        /*return db.delete("foodcart",
                "itemId = ? ",
                new String[] { itemId });*/
    }

    public int deleteSectCartItems(String itemSecId) {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.execSQL("delete from "+ "items");
        //db.execSQL("delete from "+ "items where itemId=" +itemSecId);
        //Cursor res =  db.rawQuery( "select * from items where itemId="+itemId+"", null );
        //Cursor res =  db.rawQuery( "select * from items where itemId="+itemId+"", null );

        return db.delete("items",
                "itemId = ? ",
                new String[]{itemSecId});


        //return ;
        /*return db.delete("foodcart",
                "itemId = ? ",
                new String[] { itemId });*/
    }


    public Cursor getAllFoodItems() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from foodcart", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex("itemName")));
            res.moveToNext();
        }
        return res;
    }

    public Cursor getAllSectFoodItems() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from items", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex("itemQuantity")));
            res.moveToNext();
        }
        return res;
    }

    //Food Sect Items
    public Cursor getAllSectionItemsIdRelated(String itemId) {
        ArrayList<String> array_list = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        //Cursor res =  db.rawQuery( "select * from items", null );
        Cursor res = db.rawQuery("select * from items where itemId=" + itemId + "", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex("itemQuantity")));
            res.moveToNext();
        }
        return res;
    }
    // Food cart End


    // Ecomerce Cart Creation

    public boolean insertEcomcart(String itemId, String itemName, String itemImage, String itemPrice, String itemQuantity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("itemId", itemId);
        contentValues.put("itemName", itemName);
        contentValues.put("itemImage", itemImage);
        contentValues.put("itemPrice", itemPrice);
        contentValues.put("itemQuantity", itemQuantity);
        db.insert("ecommercecart", null, contentValues);
        return true;
    }

    public Cursor getEcomItem(String itemId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from ecommercecart where itemId=" + itemId + "", null);
        return res;
    }

    public int numberOfFoodCartRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, FOOD_CART_TABLE_NAME);
        return numRows;
    }

    public int numberOfSectFoodCartRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, ECOM_SEC_CART_TABLE_NAME);
        return numRows;
    }

    public int numberOfSectFoodCartItemsRow(String itemId) {
        SQLiteDatabase db = this.getReadableDatabase();

        int numRows = (int) DatabaseUtils.queryNumEntries(db, ECOM_SEC_CART_TABLE_NAME, "itemId = ?", new String[]{itemId});
        return numRows;
    }

    public boolean updateEcomItem(String itemId, String itemName, String itemImage, String itemPrice, String itemQuantity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("itemId", itemId);
        contentValues.put("itemName", itemName);
        contentValues.put("itemImage", itemImage);
        contentValues.put("itemPrice", itemPrice);
        contentValues.put("itemQuantity", itemQuantity);
        db.update("ecommercecart", contentValues, "itemId = ? ", new String[]{itemId});
        return true;
    }

    public Integer deleteEcomItem(String itemId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("ecommercecart",
                "itemId = ? ",
                new String[]{itemId});
    }

    public Cursor getAllEcomItems() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from ecommercecart", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex("itemName")));
            res.moveToNext();
        }
        return res;
    }

    // Food cart End

    public void clearDatabase(String TABLE_NAME) {
        SQLiteDatabase db = this.getReadableDatabase();
        String clearDBQuery = "DELETE FROM " + TABLE_NAME;
        db.execSQL(clearDBQuery);
    }
}