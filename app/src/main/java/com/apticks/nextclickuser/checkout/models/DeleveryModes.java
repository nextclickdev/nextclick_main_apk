package com.apticks.nextclickuser.checkout.models;

public class DeleveryModes {
    String id,name,desc,is_having_delivery_fee,status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getIs_having_delivery_fee() {
        return is_having_delivery_fee;
    }

    public void setIs_having_delivery_fee(String is_having_delivery_fee) {
        this.is_having_delivery_fee = is_having_delivery_fee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
