package com.apticks.nextclickuser.checkout.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.adapters.OrderItemsAdapter;
import com.apticks.nextclickuser.checkout.models.ItemModel;
import com.apticks.nextclickuser.checkout.models.ProductDetailsModel;
import com.apticks.nextclickuser.checkout.models.ProductVarientModel;
import com.apticks.nextclickuser.checkout.models.SectionItemModel;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.apticks.nextclickuser.Config.Config.ORDERDETAILS;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;


public class OrderDetailsActivity extends Activity implements View.OnClickListener {

    private Context mContext;
    PreferenceManager preferenceManager;
    private RecyclerView itemsRecycler;
    private TextView tv_trackid,tv_status,tv_time,tv_customername,tv_customerphone,tv_deleverymode,tv_deleverypartnarname,
            payment_type,tv_sub_total,tv_item_grand_total_total,tv_discount,tv_tax,tv_reorder;
    private ImageView refresh_order_,img_back;
    private String orderId;
    private LinearLayout layout_delevery,layout_delevery_one;

    private CustomDialog mCustomDialog;

    ArrayList<ProductDetailsModel> productlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_details_activity);

        init();
        getOrders();
        refresh_order_.setOnClickListener(this);
        img_back.setOnClickListener(this);

    }

    private void init() {
        mContext = OrderDetailsActivity.this;
        orderId = getIntent().getStringExtra("order_id");
        preferenceManager = new PreferenceManager(mContext);
        itemsRecycler = findViewById(R.id.itemsRecycler);
        refresh_order_ = findViewById(R.id.refresh_order_);
        tv_trackid = findViewById(R.id.tv_trackid);
        tv_customername = findViewById(R.id.tv_customername);
        tv_customerphone = findViewById(R.id.tv_customerphone);
        tv_deleverymode = findViewById(R.id.tv_deleverymode);
        tv_deleverypartnarname = findViewById(R.id.tv_deleverypartnarname);
        tv_time = findViewById(R.id.tv_time);
        layout_delevery = findViewById(R.id.layout_delevery);
        layout_delevery_one = findViewById(R.id.layout_delevery_one);
        payment_type = findViewById(R.id.payment_type);
        tv_status = findViewById(R.id.tv_status);
        tv_sub_total = findViewById(R.id.tv_sub_total);
        tv_item_grand_total_total = findViewById(R.id.tv_item_grand_total_total);
        img_back = findViewById(R.id.img_back);
        tv_discount = findViewById(R.id.tv_discount);
        tv_tax = findViewById(R.id.tv_tax);
        tv_reorder = findViewById(R.id.tv_reorder);

        mCustomDialog=new CustomDialog(mContext);
        productlist=new ArrayList<>();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.refresh_order_:
                getOrders();
                break;
            case R.id.img_back:
                finish();
                break;
            case R.id.tv_reorder:
                Gson gson=new Gson();
                String cartliststring = gson.toJson(productlist);
                Intent intent=new Intent(mContext, CheckOutPageActivity.class);
                intent.putExtra("productlist",cartliststring);
                intent.putExtra("whichactivity",2);
                startActivity(intent);
                break;

        }

    }

    public void getOrders(){
        productlist.clear();
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("order_id", orderId);

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+json.toString());
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERDETAILS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONObject dataobj=jsonObject.getJSONObject("data");

                                timezone(dataobj.getString("created_at"));
                               // tv_time.setText(dataobj.getString("created_at"));
                                tv_trackid.setText(dataobj.getString("track_id"));
                                tv_sub_total.setText(dataobj.getString("track_id"));
                                tv_item_grand_total_total.setText(dataobj.getString("total"));

                                if (dataobj.getString("delivery_mode_id").equalsIgnoreCase("1")){
                                    layout_delevery.setVisibility(View.GONE);
                                    layout_delevery_one.setVisibility(View.GONE);
                                }else {
                                    layout_delevery.setVisibility(View.GONE);
                                    layout_delevery_one.setVisibility(View.GONE);
                                }

                                try{
                                    JSONObject shippingobj=dataobj.getJSONObject("shipping_address");
                                    tv_customername.setText(shippingobj.getString("name"));
                                    tv_customerphone.setText(shippingobj.getString("phone"));
                                }catch (JSONException e1){

                                }

                                try{
                                    JSONObject deleveryong=dataobj.getJSONObject("delivery_mode");
                                    tv_deleverymode.setText(""+deleveryong.getString("name"));
                                }catch (JSONException e2){

                                }

                                try{
                                    JSONObject customerobj=dataobj.getJSONObject("customer");

                                }catch (JSONException e2){

                                }
                                try{
                                    JSONObject orderostatusobj=dataobj.getJSONObject("order_status");

                                    tv_status.setText(orderostatusobj.getString("status"));
                                }catch (JSONException e2){

                                }

                                try{
                                    JSONObject paymentobj=dataobj.getJSONObject("payment");
                                    payment_type.setText(paymentobj.getString("txn_id"));
                                }catch (JSONException e2){

                                }

                                JSONArray productarray=dataobj.getJSONArray("ecom_order_details");

                                for (int k=0;k<productarray.length();k++){
                                    JSONObject productonj=productarray.getJSONObject(k);
                                    ProductDetailsModel productDetailsModel=new ProductDetailsModel();

                                    productDetailsModel.setId(productonj.getString("id"));
                                    productDetailsModel.setEcom_order_id(productonj.getString("ecom_order_id"));
                                    productDetailsModel.setId(productonj.getString("item_id"));
                                    productDetailsModel.setVendor_product_variant_id(productonj.getString("vendor_product_variant_id"));
                                    productDetailsModel.setQty(productonj.getString("qty"));
                                    productDetailsModel.setPrice(productonj.getString("price"));
                                    productDetailsModel.setRate_of_discount(productonj.getString("rate_of_discount"));
                                    productDetailsModel.setSub_total(productonj.getString("sub_total"));
                                    productDetailsModel.setDiscount(productonj.getString("discount"));
                                    productDetailsModel.setTax(productonj.getString("tax"));
                                    productDetailsModel.setTotal(productonj.getString("total"));
                                    productDetailsModel.setCancellation_message(productonj.getString("cancellation_message"));
                                    productDetailsModel.setStatus(productonj.getString("status"));


                                    try{
                                        JSONObject itemobj=productonj.getJSONObject("item");
                                        ItemModel itemModel=new ItemModel();
                                        itemModel.setId(itemobj.getString("id"));
                                        itemModel.setName(itemobj.getString("name"));
                                        itemModel.setDesc(itemobj.getString("desc"));

                                        JSONObject varientobj=itemobj.getJSONObject("varinat");
                                        ProductVarientModel varientModel=new ProductVarientModel();
                                        varientModel.setId(varientobj.getString("id"));
                                        varientModel.setSku(varientobj.getString("sku"));
                                        varientModel.setPrice(varientobj.getString("price"));
                                        varientModel.setStock(varientobj.getString("stock"));
                                        varientModel.setDiscount(varientobj.getString("discount"));
                                        varientModel.setTax_id(varientobj.getString("tax_id"));
                                        varientModel.setStatus(varientobj.getString("status"));
                                        varientModel.setSection_item_id(varientobj.getString("section_item_id"));

                                        JSONObject sectionobj=varientobj.getJSONObject("section_item");
                                        SectionItemModel sectionItemModel=new SectionItemModel();
                                        sectionItemModel.setId(sectionobj.getString("id"));
                                        sectionItemModel.setName(sectionobj.getString("name"));
                                        sectionItemModel.setWeight(sectionobj.getString("weight"));

                                        varientModel.setSectionItemModel(sectionItemModel);

                                        itemModel.setVarientModel(varientModel);
                                        productDetailsModel.setItemModel(itemModel);

                                        productlist.add(productDetailsModel);
                                    }catch (JSONException e2){

                                    }

                                }

                                if(productlist.size()>0) {
                                    OrderItemsAdapter orderItemsAdapter = new OrderItemsAdapter(mContext, productlist);

                                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                    itemsRecycler.setLayoutManager(layoutManager);
                                    itemsRecycler.setAdapter(orderItemsAdapter);

                                    double subtotal=0.0,discount=0.0,rateofdiscount=0.0,tax=0.0;

                                    for (int i=0;i<productlist.size();i++){
                                        subtotal=subtotal+Double.parseDouble(productlist.get(i).getSub_total());
                                        discount=discount+Double.parseDouble(productlist.get(i).getDiscount());
                                        rateofdiscount=rateofdiscount+Double.parseDouble(productlist.get(i).getRate_of_discount());
                                        tax=tax+Double.parseDouble(productlist.get(i).getTax());

                                    }
                                    if (discount==0.0){
                                        tv_discount.setVisibility(View.GONE);
                                    }
                                    tv_sub_total.setText(""+subtotal);
                                    tv_discount.setText(""+rateofdiscount+ "% off( "+discount+" )");
                                    tv_tax.setText(""+tax);
                                }

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN",preferenceManager.getString(TOKEN_KEY));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void timezone(String timezone){
        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
           // Date past = format.parse("2016.02.05 AD at 23:59:30");
            Date past = format.parse(timezone);

            Date now = new Date();
            long seconds= TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes=TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days=TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if(seconds<60)
            {
                tv_time.setText(seconds+" seconds ago");
               // System.out.println(seconds+" seconds ago");
            }
            else if(minutes<60)
            {
                tv_time.setText(minutes+" minutes ago");
                System.out.println(minutes+" minutes ago");
            }
            else if(hours<24)
            {
                tv_time.setText(hours+" hours ago");
                System.out.println(hours+" hours ago");
            }
            else
            {
                tv_time.setText(days+" days ago");
                System.out.println(days+" days ago");
            }
        }
        catch (Exception j){
            System.out.println("aaaaa catch  "+j.getMessage());
            j.printStackTrace();
        }
    }
}