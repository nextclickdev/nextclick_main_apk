package com.apticks.nextclickuser.checkout.models;

import java.io.Serializable;

public class DeliveryFeeObject implements Serializable {

    String id,per_km,vehicle_type_id,flat_rate;
    private String bagWeight;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlatRate() {
        return flat_rate;
    }

    public void setFlatRate(String flat_rate) {
        this.flat_rate = flat_rate;
    }

    public String getKMInfo() {
        return per_km;
    }

    public void setKMInfo(String per_km) {
        this.per_km = per_km;
    }

    public String getVehicleTypeId() {
        return vehicle_type_id;
    }

    public void setVehicleTypeId(String vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }

    public String getBagWeight() {
        return bagWeight;
    }

    public void setBagWeight(String bagWeight) {
        this.bagWeight = bagWeight;
    }
}
