package com.apticks.nextclickuser.checkout.adapters;
 import android.content.Context;
        import android.os.Build;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.LinearLayout;
        import android.widget.TextView;

        import androidx.annotation.NonNull;
        import androidx.annotation.RequiresApi;
        import androidx.cardview.widget.CardView;
        import androidx.recyclerview.widget.LinearLayoutManager;
        import androidx.recyclerview.widget.RecyclerView;

        import com.apticks.nextclickuser.R;
        import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
        import com.apticks.nextclickuser.checkout.models.CheckoutModel;
        import com.apticks.nextclickuser.checkout.models.SubOrderObject;

        import org.json.JSONObject;

        import java.util.ArrayList;

public class VendorSubOrdersAdapter  extends RecyclerView.Adapter<VendorSubOrdersAdapter.ItemViewHolder> {

    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    ArrayList<CheckoutModel> cartllist;
    Context context;
    int mExpandedPosition, previousExpandedPosition;
    boolean ischecked;
    private double totalBagWeight;

    public VendorSubOrdersAdapter(Context mContext, ArrayList<CheckoutModel> productslist) {
        this.context = mContext;
        this.cartllist = productslist;
    }

    @NonNull
    @Override
    public VendorSubOrdersAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_vendor_suborder_item, viewGroup, false);
        return new VendorSubOrdersAdapter.ItemViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull VendorSubOrdersAdapter.ItemViewHolder itemViewHolder, int position) {
        CheckoutModel item = cartllist.get(position);

        itemViewHolder.tv_store.setText(item.getVendorDetails().getName());

        // Create layout manager with initial prefetch item count
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                itemViewHolder.rvSubItem.getContext(),
                LinearLayoutManager.VERTICAL,
                false
        );
        SubOrdersAdapter subItemAdapter = new SubOrdersAdapter(context, item.getSubOrdersList(),totalBagWeight);
        itemViewHolder.rvSubItem.setLayoutManager(layoutManager);
        itemViewHolder.rvSubItem.setAdapter(subItemAdapter);
        itemViewHolder.rvSubItem.setRecycledViewPool(viewPool);
    }

    @Override
    public int getItemCount() {
        return cartllist.size();
    }

    public void setrefresh(ArrayList<CheckoutModel> productslist, double totalBagWeight) {
        this.cartllist = productslist;
        this.totalBagWeight=totalBagWeight;
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvSubItem;
        TextView tv_store;

        ItemViewHolder(View itemView) {
            super(itemView);
            rvSubItem = itemView.findViewById(R.id.rv_sub_item);
            tv_store = itemView.findViewById(R.id.tv_store);
        }
    }
}
