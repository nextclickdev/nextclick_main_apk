package com.apticks.nextclickuser.checkout.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutNewPageActivity;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.checkout.models.DeleveryModes;

import java.util.ArrayList;

public class DeleveryModesAdapter extends RecyclerView.Adapter<DeleveryModesAdapter.ViewHolder>   {

    ArrayList<DeleveryModes> deleverymoideslist;
    Context context;
    int which;
    public DeleveryModesAdapter(Context context, ArrayList<DeleveryModes> productdetailslist,int which) {
        this.context=context;
        this.deleverymoideslist=productdetailslist;
        this.which=which;
    }
    @NonNull
    @Override
    public DeleveryModesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.delevery_modes, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new DeleveryModesAdapter.ViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull DeleveryModesAdapter.ViewHolder holder, int position) {

        holder.tv_name.setText(deleverymoideslist.get(position).getName());

        if (deleverymoideslist.get(position).getStatus().equalsIgnoreCase("2")){
            holder.relative_toggle.setBackground(context.getResources().getDrawable(R.drawable.background_blue_full));
            holder.tv_name.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.relative_toggle.setBackground(context.getResources().getDrawable(R.drawable.background_blue_line));
            holder.tv_name.setTextColor(context.getResources().getColor(R.color.black));
        }

        if (deleverymoideslist.get(position).getId().equalsIgnoreCase("1")){
            holder.img_delevery.setImageResource(R.drawable.self_pickup);
        }else if (deleverymoideslist.get(position).getId().equalsIgnoreCase("2")){
            holder.img_delevery.setImageResource(R.drawable.delevery_parrtner);
        }

        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {

                for (int i=0;i<deleverymoideslist.size();i++){
                    if (deleverymoideslist.get(i).getId().equalsIgnoreCase(deleverymoideslist.get(position).getId())){
                        deleverymoideslist.get(position).setStatus("2");
                    }else {
                        deleverymoideslist.get(i).setStatus("1");
                    }
                }

                if (deleverymoideslist.get(position).getId().equalsIgnoreCase("2")){
                    if (which==1){
                        ((CheckOutNewPageActivity)context).setdeleverymodechanged(deleverymoideslist.get(position),true);
                    }else {
                        ((CheckOutPageActivity)context).setdeleverymodechanged(deleverymoideslist.get(position),true);
                    }

                }else {
                    if (which==1){
                        ((CheckOutNewPageActivity)context).setdeleverymodechanged(deleverymoideslist.get(position),false);
                    }else {
                        ((CheckOutPageActivity)context).setdeleverymodechanged(deleverymoideslist.get(position),false);
                    }


                }
                notifyDataSetChanged();
            }
        });


    }

    public void setrefresh(ArrayList<DeleveryModes> productlist){
        this.deleverymoideslist=productlist;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return deleverymoideslist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}


    public class ViewHolder extends RecyclerView.ViewHolder {

        
       private TextView tv_name;
        private LinearLayout layout_address,relative_toggle;
        private ImageView img_delevery;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name=itemView.findViewById(R.id.tv_name);
            layout_address=itemView.findViewById(R.id.layout_address);
            img_delevery=itemView.findViewById(R.id.img_delevery);
            relative_toggle=itemView.findViewById(R.id.relative_toggle);


        }
    }
}
