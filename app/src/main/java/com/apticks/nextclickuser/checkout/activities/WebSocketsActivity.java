package com.apticks.nextclickuser.checkout.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.apticks.nextclickuser.R;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class WebSocketsActivity extends Activity {
    public static WebSocketClient mWebSocketClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_sockets);

        connectWebSocket();

    }

    public void connectWebSocket() {
        URI uri;
        try {
            uri = new URI("ws://xxx.xxx.xxx:xxxx");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                System.out.println("aaaaaaa opened ");
            }

            @Override
            public void onMessage(String s) {
                //final String message = s;
                final String WSmessage = s;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), WSmessage, Toast.LENGTH_SHORT).show();
                        System.out.println("aaaaaaaaaa message "+WSmessage);

                    }
                });
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                System.out.println("aaaaaaaaaa close "+s);
            }

            @Override
            public void onError(Exception e) {
                System.out.println("aaaaaaaaaa close "+e.toString());
            }
        };
        mWebSocketClient.connect();
    }


}