package com.apticks.nextclickuser.checkout.models;

import java.io.Serializable;
import java.util.ArrayList;

public class SubOrderObject implements Serializable {

     String item_id, id, section_id, section_item_id, sku, price, stock, discount, list_id, vendor_user_id, status,
             section_item_name, weight, section_item_code, desc;
     boolean checked;
     int quantity;
     private DeliveryFeeObject deliveryFeeObject;
    private ArrayList<Products> productslist;
    private VehicleObject vehicleObject;

    public int getQuantity() {
         return quantity;
     }

     public void setQuantity(int quantity) {
         this.quantity = quantity;
     }

     public String getItem_id() {
         return item_id;
     }

     public void setItem_id(String item_id) {
         this.item_id = item_id;
     }

     public String getId() {
         return id;
     }

     public void setId(String id) {
         this.id = id;
     }

     public String getSection_id() {
         return section_id;
     }

     public void setSection_id(String section_id) {
         this.section_id = section_id;
     }

     public String getSection_item_id() {
         return section_item_id;
     }

     public void setSection_item_id(String section_item_id) {
         this.section_item_id = section_item_id;
     }

     public String getSku() {
         return sku;
     }

     public void setSku(String sku) {
         this.sku = sku;
     }

     public String getPrice() {
         return price;
     }

     public void setPrice(String price) {
         this.price = price;
     }

     public String getStock() {
         return stock;
     }

     public void setStock(String stock) {
         this.stock = stock;
     }

     public String getDiscount() {
         return discount;
     }

     public void setDiscount(String discount) {
         this.discount = discount;
     }

     public String getList_id() {
         return list_id;
     }

     public void setList_id(String list_id) {
         this.list_id = list_id;
     }

     public String getVendor_user_id() {
         return vendor_user_id;
     }

     public void setVendor_user_id(String vendor_user_id) {
         this.vendor_user_id = vendor_user_id;
     }

     public String getStatus() {
         return status;
     }

     public void setStatus(String status) {
         this.status = status;
     }

     public String getSection_item_name() {
         return section_item_name;
     }

     public void setSection_item_name(String section_item_name) {
         this.section_item_name = section_item_name;
     }

     public String getWeight() {
         return weight;
     }

     public void setWeight(String weight) {
         this.weight = weight;
     }

     public String getSection_item_code() {
         return section_item_code;
     }

     public void setSection_item_code(String section_item_code) {
         this.section_item_code = section_item_code;
     }

     public String getDesc() {
         return desc;
     }

     public void setDesc(String desc) {
         this.desc = desc;
     }

     public boolean isChecked() {
         return checked;
     }

     public void setChecked(boolean checked) {
         this.checked = checked;
     }

     public void setDeliveryFeeObject(DeliveryFeeObject deliveryFeeObject) {
         this.deliveryFeeObject=deliveryFeeObject;
     }
     public DeliveryFeeObject getDeliveryFeeObject() {
         return deliveryFeeObject;
     }

    public ArrayList<Products> getProductslist() {
        return productslist;
    }

    public void setProductslist(ArrayList<Products> productslist) {
        this.productslist = productslist;
    }

    public void setVehicleDetails(VehicleObject vehicleObject) {
        this.vehicleObject=vehicleObject;
    }
    public VehicleObject getVehicleDetails() {
        return vehicleObject;
    }
}
