package com.apticks.nextclickuser.checkout.models;

import java.util.ArrayList;

public class WeightPermissable {
    boolean status;
    ArrayList<Permissionweight> weightlist;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ArrayList<Permissionweight> getWeightlist() {
        return weightlist;
    }

    public void setWeightlist(ArrayList<Permissionweight> weightlist) {
        this.weightlist = weightlist;
    }
}
