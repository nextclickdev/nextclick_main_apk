package com.apticks.nextclickuser.checkout.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.map.MapParser;
import com.apticks.nextclickuser.orderhistory.model.DirectionObject;
import com.apticks.nextclickuser.orderhistory.util.DirectionListener;
import com.apticks.nextclickuser.orderhistory.util.LocationDetailUtil;
import com.apticks.nextclickuser.utils.LocationHelper;
import com.apticks.nextclickuser.utils.MyLocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapTest extends AppCompatActivity implements OnMapReadyCallback, MapParser, MyLocationListener, OnClickListener {

    private GoogleMap mMap;
    TextView tv_delivery_time, address_details;
    EditText et_time,et_status;
    ProgressBar pBar;
    Button btn_change_time, btn_apply_status;

    //17.4834° N, 78.3871° E  --kphb
    //17.4505° N, 78.3790° E - paradise
    //	17.4185,78.4222 - banjara hills

    int deliveryTime=41;
    private Location currentLocation;
    Polyline mPolyline;
    private int deliveryType=1;
    Marker prevMarker1 =null;
    Marker prevMarker2 =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_test);
        tv_delivery_time= findViewById(R.id.tv_delivery_time);
        address_details= findViewById(R.id.address_details);
        et_status= findViewById(R.id.et_status);
        et_time= findViewById(R.id.et_time);
        pBar= findViewById(R.id.pBar);
        btn_change_time= findViewById(R.id.btn_change_time);
        btn_apply_status= findViewById(R.id.btn_apply_status);
        Button btn_change_del_type= findViewById(R.id.btn_change_del_type);
        btn_change_time.setOnClickListener(this);
        btn_apply_status.setOnClickListener(this);
        btn_change_del_type.setOnClickListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCurrentPosition();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_change_time:
                int time = Integer.parseInt(et_time.getText().toString());
                tv_delivery_time.setText("Delivery in " + (41-time) + " mins");
                pBar.setMax(deliveryTime);
                pBar.setProgress(time);
                break;
            case R.id.btn_apply_status:
                int status = Integer.parseInt(et_status.getText().toString());
                showStatus(status);
                break;
            case R.id.btn_change_del_type:
                if (deliveryType == 1)
                    deliveryType = 2;
                else
                    deliveryType = 1;
                status = Integer.parseInt(et_status.getText().toString());
                showStatus(status);
                break;
        }
    }

    private void showStatus(int status) {
        switch (status)
        {
            case 508://comleted
            break;
            case 504:
                showMap(Customer,DeliveryBoy);
                break;
            default://initial
                showMap(Customer,Restraurant);
                break;
        }
    }

    public static  String Customer="Customer";
    public static  String Restraurant="Restraurant";
    public static  String DeliveryBoy="DeliveryBoy";

    private void showMap(String from, String to) {

        if (prevMarker1 != null)
            prevMarker1.setVisible(false);
        if (prevMarker2 != null)
            prevMarker2.setVisible(false);

        if (from.equals(Customer) && to.equals(Restraurant)) {
            //17.4834° N, 78.3871° E  --kphb
            //17.4505° N, 78.3790° E - paradise

            LatLng custLatLong = new LatLng(17.4834, 78.3871);
            LatLng restLatLong = new LatLng(17.4505, 78.3790);

            prevMarker1 = addMarkerObject(custLatLong, "Delivering to Home", R.drawable.home_map_pin);
            prevMarker2 = addMarkerObject(restLatLong, "Paradise restraurant", R.drawable.restraurant_map_pin);

            showtracking(custLatLong, restLatLong);
        }
        if (from.equals(Customer) && to.equals(DeliveryBoy)) {
            //17.4834° N, 78.3871° E  --kphb
            //	17.4185,78.4222--deliveryBoy

            LatLng custLatLong = new LatLng(17.4834, 78.3871);
            LatLng deliveryBoy = new LatLng(17.41855, 78.4222);


            prevMarker1 = addMarkerObject(custLatLong, "Delivering to Home", R.drawable.home_map_pin);
            if (deliveryType == 1)
                prevMarker2 = addMarkerObject(deliveryBoy, "From Delivey Boy", R.drawable.byke_map_pin);
            else
                prevMarker2 = addMarkerObject(deliveryBoy, "From Delivey Boy", R.drawable.auto_map_pin_show);

            showtracking(custLatLong, deliveryBoy);
        }
    }

    private void showtracking(LatLng custLatLong, LatLng restLatLong) {

       /* CameraPosition position = new CameraPosition.Builder()
                .target(custLatLong)
                .zoom(15.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));*/

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(custLatLong);
        builder.include(restLatLong);
        LatLngBounds bounds = builder.build();

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
        mMap.animateCamera(cu, new GoogleMap.CancelableCallback(){
            public void onCancel(){}
            public void onFinish(){
                CameraUpdate zout = CameraUpdateFactory.zoomBy(-1.0f);//-3.0f
                mMap.animateCamera(zout);
            }
        });


        DirectionListener shopDistanceListener = new DirectionListener() {
            @Override
            public void onDirectionResult(DirectionObject result) {
                if (result != null) {
                    if (result.getLineOptions() != null) {
                        if (mPolyline != null) {
                            mPolyline.remove();
                        }
                        mPolyline = mMap.addPolyline(result.getLineOptions());
                    }
                }
            }
        };
        LocationDetailUtil.getTravelDistanceFromCurrentLocation(this, restLatLong, shopDistanceListener,
                custLatLong);
    }

    private void getCurrentPosition() {
        LocationHelper.SetContext(this);
        LocationHelper.getCurrentPosition(this);
    }

    @Override
    public void onPolylineOptionsUpdated(PolylineOptions lineOptions) {

    }

    @Override
    public void onLocationReceived(Location location) {
        if(location==null)
            return;

        currentLocation = location;
    }

    @Override
    public void onLocationUpdated(Location location) {
        if(location==null)
            return;

        currentLocation = location;
    }

    @Override
    public void onLocationFailed(int code, String message) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
    ///


    private Marker addMarkerObject(LatLng latLng, String Address, int id) {

        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(Address)
                .icon(BitmapFromVector(getApplicationContext(), id)));
        marker.showInfoWindow();
        return marker;
    }


    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);
        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

}