package com.apticks.nextclickuser.checkout.models;

public class OrderHistoryModel {
    String id,track_id,order_delivery_otp,preparation_time,payment_id,vendor_user_id,created_at,updated_at,
            order_status_id;
    PaymentModel paymentModel;
    OrderStatus orderStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getOrder_delivery_otp() {
        return order_delivery_otp;
    }

    public void setOrder_delivery_otp(String order_delivery_otp) {
        this.order_delivery_otp = order_delivery_otp;
    }

    public String getPreparation_time() {
        return preparation_time;
    }

    public void setPreparation_time(String preparation_time) {
        this.preparation_time = preparation_time;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getVendor_user_id() {
        return vendor_user_id;
    }

    public void setVendor_user_id(String vendor_user_id) {
        this.vendor_user_id = vendor_user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getOrder_status_id() {
        return order_status_id;
    }

    public void setOrder_status_id(String order_status_id) {
        this.order_status_id = order_status_id;
    }

    public PaymentModel getPaymentModel() {
        return paymentModel;
    }

    public void setPaymentModel(PaymentModel paymentModel) {
        this.paymentModel = paymentModel;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    String total;
    public void setOrderAmount(String total) {
        this.total=total;
    }

    public String getOrderAmount() {
        return total;
    }


    String shopName;
    public void setShopName(String shopName) {
        this.shopName=shopName;
    }

    public String getShopName() {
        return shopName;
    }


    String delFee;
    public void setDeliveryFee(String delFee) {
        this.delFee=delFee;
    }

    public String getDeliveryFee() {
        return delFee;
    }

    String vendorStatus;
    public void setVendorStatus(String vendorStatus) {
        this.vendorStatus=vendorStatus;
    }
    public String getVendorStatus() {
        return vendorStatus;
    }
}
