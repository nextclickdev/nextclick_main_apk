package com.apticks.nextclickuser.checkout.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.checkout.activities.DeliveryFeeInfoActivity;
import com.apticks.nextclickuser.checkout.models.Products;
import com.apticks.nextclickuser.dialogs.RemoveProduct;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CARTUPDATE;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;
import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isNull;

public class SubProductsAdapter extends RecyclerView.Adapter<SubProductsAdapter.ViewHolder> {

    ArrayList<Products> cartlist;
    Context context;
    CustomDialog customDialog;
    PreferenceManager preferenceManager;
    RemoveProduct removeProduct;
    private final double totalBagWeight;
    public SubProductsAdapter(Context context, ArrayList<Products> productlist, double totalBagWeight) {
        this.context=context;
        this.cartlist=productlist;
        this.customDialog=new CustomDialog(context);
        this.preferenceManager=new PreferenceManager(context);
        this.totalBagWeight=totalBagWeight;
    }

    @NonNull
    @Override
    public SubProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.cart_sub_products_adpter, parent, false);
        return new SubProductsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubProductsAdapter.ViewHolder holder, int position) {
        Products cartModel = cartlist.get(position);


        String output = cartModel.getProduct().getName().substring(0, 1).toUpperCase() + cartModel.getProduct().getName().substring(1);
        holder.tv_pname.setText(output);
        holder.tv_variants.setText(isNull(cartModel.getSectionProducts().getName()));
        System.out.println("aaaaaaaaaaaa  weight  "+cartModel.getweight());
       // holder.tv_weight.setText(cartModel.getweight()+" gms");
        try {
            double weight=Double.parseDouble(cartModel.getweight());
            if (weight>=1000){
                holder.tv_weight.setText(String.format("%.2f",(weight/1000)) + " kg");
            }else {
                holder.tv_weight.setText(cartModel.getweight() + " gm");
            }

        }
        catch (Exception ex) {
            holder.tv_weight.setText(cartModel.getweight()+" gm");
        }

        holder.tv_qty.setText(cartModel.getSubOrderQty());
       // holder.tv_qty.setText("Quantity  \n"+cartModel.getQty());
       // holder.tv_pdescription.setText(isNull(cartModel.getSectionProducts().getName()));

        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DeliveryFeeInfoActivity)context).setDelete(cartModel);
            }
        });
    }

    public void setrefresh(ArrayList<Products> productlist){
        this.cartlist=productlist;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return cartlist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_product,img_cancel;
        private TextView tv_pname,tv_pdescription,tv_tax,tv_subtotal,tv_totalprice,tv_qty,tv_stock,tv_discount,tv_weight,tv_variants,progress_tv;
        ProgressBar circular_determinative_pb;
        private LinearLayout img_delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_pname=itemView.findViewById(R.id.tv_pname);
            tv_weight = itemView.findViewById(R.id.tv_weight);
            tv_qty= itemView.findViewById(R.id.tv_qty);
            tv_variants=itemView.findViewById(R.id.tv_variants);
            progress_tv=itemView.findViewById(R.id.progress_tv);
            circular_determinative_pb=itemView.findViewById(R.id.circular_determinative_pb);
            img_delete=itemView.findViewById(R.id.img_delete);
        }
    }
}

