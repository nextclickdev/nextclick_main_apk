package com.apticks.nextclickuser.checkout.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Constants.IErrors;
import com.apticks.nextclickuser.Fragments.CommonModules.PaymentGateways;
import com.apticks.nextclickuser.Fragments.OrderHistory.MainHistoryActivity;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.NetworkUtil;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.Wallet.Transactions;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.adapters.PaymentModesAdapter;
import com.apticks.nextclickuser.checkout.models.CheckoutModel;
import com.apticks.nextclickuser.checkout.models.DeleveryfeeDetails;
import com.apticks.nextclickuser.checkout.models.PaymentModes;
import com.apticks.nextclickuser.checkout.models.Permissionweight;
import com.apticks.nextclickuser.checkout.models.Product;
import com.apticks.nextclickuser.checkout.models.ProductImages;
import com.apticks.nextclickuser.checkout.models.Products;
import com.apticks.nextclickuser.checkout.models.SectionProducts;
import com.apticks.nextclickuser.checkout.models.Tax;
import com.apticks.nextclickuser.checkout.models.VendorDetails;
import com.apticks.nextclickuser.checkout.models.WeightPermissable;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.view.Gravity.CENTER;
import static android.view.View.GONE;
import static com.apticks.nextclickuser.Config.Config.CHECKOUTPRODUCTS;
import static com.apticks.nextclickuser.Config.Config.FoodOrder;
import static com.apticks.nextclickuser.Config.Config.ORDERSEND;
import static com.apticks.nextclickuser.Config.Config.PAYMENTMODES;
import static com.apticks.nextclickuser.Config.Config.PAYMENTSTATUS;
import static com.apticks.nextclickuser.Config.Config.WALLETHISTORY;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class PaymentModesActivity extends AppCompatActivity implements PaymentResultWithDataListener {

    RecyclerView recycle_paymentmodes;
    private ImageView image_back;
    private Context mContext;
    PaymentModesAdapter paymentModesAdapter;
    ArrayList<PaymentModes> paymentModesArrayList;
    private CustomDialog customDialog;
    private RelativeLayout relative_layout_;
    private PreferenceManager preferenceManager;
    ArrayList<CheckoutModel> checkoutlist;
    private Gson gson;
    private String shippingaddressid,deleverymodeid,totalamount,walletamount="",usedWalletamount="";
    String year,month,day,start_date_str,end_date_str;
    private TextView tv_availbal;
    private CheckBox cb_walet;
    String DeliveryMode="";
    String DeliveryMode_SELF="self";
    String DeliveryMode_DELIVERY_PATNER="deliverypatner";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_modes);
        getSupportActionBar().hide();
        Window window = getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        mContext=PaymentModesActivity.this;
        recycle_paymentmodes=findViewById(R.id.recycle_paymentmodes);
        relative_layout_=findViewById(R.id.relative_layout_);
        image_back=findViewById(R.id.img_back);
        TextView tv_header= findViewById(R.id.tv_header);
        tv_header.setText("Payment Modes");
        tv_header.setAllCaps(true);
        FrameLayout cart_layout=findViewById(R.id.cart_layout);
        cart_layout.setVisibility(View.GONE);
        tv_availbal=findViewById(R.id.tv_availbal);
        cb_walet=findViewById(R.id.cb_walet);

        paymentModesArrayList=new ArrayList<>();
        checkoutlist=new ArrayList<>();
        gson=new Gson();
        customDialog=new CustomDialog(mContext);
        preferenceManager=new PreferenceManager(mContext);

        shippingaddressid=getIntent().getStringExtra("shippingaddressid");
        deleverymodeid=getIntent().getStringExtra("deleverymodeid");
        totalamount=getIntent().getStringExtra("totalamount");
        String carListAsString = getIntent().getStringExtra("checkoutlist");
        Type type = new TypeToken<List<CheckoutModel>>(){}.getType();
        checkoutlist = gson.fromJson(carListAsString, type);

        if(deleverymodeid!=null && deleverymodeid.equals("2"))
            DeliveryMode=DeliveryMode_DELIVERY_PATNER;
        else
            DeliveryMode= DeliveryMode_SELF;//1

        System.out.println("aaaaaaaaa checkoutlist  "+checkoutlist.toString());

        recycle_paymentmodes.setLayoutManager(new GridLayoutManager(mContext,1));
        paymentModesAdapter=new PaymentModesAdapter(mContext,paymentModesArrayList);
        recycle_paymentmodes.setAdapter(paymentModesAdapter);
        getDate();
        if (NetworkUtil.isNetworkConnected(mContext)) {
            getPaymentModes();
        } else {
            Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
        }

        cb_walet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    if (Double.parseDouble(walletamount)<=(Double.parseDouble(totalamount))){
                        usedWalletamount="";
                    }else {
                        usedWalletamount= totalamount;
                        setdeleverymodechanged(paymentModesArrayList.get(0));
                    }
                }else {
                    totalamount=getIntent().getStringExtra("totalamount");
                }
            }
        });
        image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    public void sendDataafterpaymentBySubOrders(String payment_id){
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("shipping_address_id", shippingaddressid);
        uploadMap.put("delivery_mode_id", deleverymodeid);
        uploadMap.put("payment_id", ""+payment_id);

        JSONArray jsonArray = new JSONArray();

        for (int i=0; i < checkoutlist.size(); i++) {

            try {

                for (int s = 0; s < checkoutlist.get(i).getSubOrdersList().size(); s++) {

                    JSONObject jsonObject1 = new JSONObject();
                    double totalpricevendor = 0.0;

                    jsonObject1.put("delivery_fee", checkoutlist.get(i).getSubOrdersList().get(s).getDeliveryFeeObject().getFlatRate());
                    jsonObject1.put("used_wallet_amount", usedWalletamount);
                    jsonObject1.put("vendor_user_id", checkoutlist.get(i).getVendorDetails().getVendor_user_id());

                    JSONArray productsArray = new JSONArray();

                    ArrayList<Products> productList = checkoutlist.get(i).getSubOrdersList().get(s).getProductslist();
                    for (int j = 0; j < productList.size(); j++) {
                        double subtotal = (Integer.parseInt(productList.get(j).getSubOrderQty())) *
                                (Double.parseDouble(productList.get(j).getPrice()));
                        int discount = Integer.parseInt(productList.get(j).getDiscount());
                        int tax = Integer.parseInt(productList.get(j).getTax().getRate());


                        double discountamount = (subtotal / 100.0f) * discount;
                        double taxamount = (subtotal / 100.0f) * tax;
                        Double totalprice = subtotal + taxamount - discountamount;

                        JSONObject productObject1 = new JSONObject();
                        productObject1.put("item_id", productList.get(j).getItem_id());
                        productObject1.put("vendor_product_variant_id", productList.get(j).getId());
                        productObject1.put("qty", productList.get(j).getSubOrderQty());
                        productObject1.put("price", productList.get(j).getPrice());
                        productObject1.put("rate_of_discount", productList.get(j).getDiscount());

                        productObject1.put("sub_total", "" + subtotal);
                        productObject1.put("discount", "" + discountamount);
                        productObject1.put("tax", "" + taxamount);
                        productObject1.put("total", "" + totalprice);

                        totalpricevendor = totalpricevendor + totalprice;
                        productsArray.put(productObject1);
                    }
                    totalpricevendor = totalpricevendor + Integer.parseInt(checkoutlist.get(i).getDelivery_fee().getFlat_rate());
                    jsonObject1.put("total", totalpricevendor);
                    jsonObject1.put("products", productsArray);
                    jsonArray.put(jsonObject1);
                }
            } catch (JSONException e) {
                System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
                e.printStackTrace();
            }
        }

        JSONObject json = new JSONObject(uploadMap);
        try {
            json.put("bags",jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendPayment(json.toString());
        System.out.println("aaaaaaaa json data "+json.toString());
    }
    public void sendDataafterpayment(String payment_id){
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("shipping_address_id", shippingaddressid);
        uploadMap.put("delivery_mode_id", deleverymodeid);
        uploadMap.put("payment_id", ""+payment_id);

        JSONArray jsonArray = new JSONArray();

        for (int i=0; i < checkoutlist.size(); i++) {
            try {
                JSONObject jsonObject1=new JSONObject();
                double totalpricevendor=0.0;
                jsonObject1.put("delivery_fee",checkoutlist.get(i).getDelivery_fee().getFlat_rate());
                jsonObject1.put("used_wallet_amount","");
                jsonObject1.put("vendor_user_id",checkoutlist.get(i).getVendorDetails().getVendor_user_id());

                JSONArray productsArray = new JSONArray();

                for (int j=0;j<checkoutlist.get(i).getProductslist().size();j++){
                    double subtotal=(Integer.parseInt(checkoutlist.get(i).getProductslist().get(j).getQty()))*
                            (Double.parseDouble(checkoutlist.get(i).getProductslist().get(j).getPrice()));
                    int discount= Integer.parseInt(checkoutlist.get(i).getProductslist().get(j).getDiscount());
                   int tax=Integer.parseInt(checkoutlist.get(i).getProductslist().get(j).getTax().getRate());

                   double discountamount=(subtotal / 100.0f) * discount;
                   double taxamount=(subtotal / 100.0f) * tax;
                    Double totalprice=subtotal+taxamount-discountamount;

                    JSONObject productObject1=new JSONObject();
                    productObject1.put("item_id",checkoutlist.get(i).getProductslist().get(j).getItem_id());
                    productObject1.put("vendor_product_variant_id",checkoutlist.get(i).getProductslist().get(j).getId());
                    productObject1.put("qty",checkoutlist.get(i).getProductslist().get(j).getQty());
                    productObject1.put("price",checkoutlist.get(i).getProductslist().get(j).getPrice());
                    productObject1.put("rate_of_discount",checkoutlist.get(i).getProductslist().get(j).getDiscount());
                    productObject1.put("sub_total",""+subtotal);
                    productObject1.put("discount",""+discountamount);
                    productObject1.put("tax",""+taxamount);
                    productObject1.put("total",""+totalprice);

                    totalpricevendor=totalpricevendor+totalprice;
                    productsArray.put(productObject1);
                }
                totalpricevendor=totalpricevendor+Integer.parseInt(checkoutlist.get(i).getDelivery_fee().getFlat_rate());
                jsonObject1.put("total",totalpricevendor);
                jsonObject1.put("products",productsArray);

                jsonArray.put(jsonObject1);
            } catch (JSONException e) {
                System.out.println("aaaaaaaaa catch 1  "+e.getMessage());
                e.printStackTrace();
            }
        }


        JSONObject json = new JSONObject(uploadMap);
        try {
            json.put("bags",jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendPayment(json.toString());
        System.out.println("aaaaaaaa json data "+json.toString());
    }

    private void getPaymentModes() {
        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, PAYMENTMODES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + response.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    PaymentModes paymentModes=new PaymentModes();
                                    paymentModes.setId(jsonObject1.getString("id"));
                                    paymentModes.setName(jsonObject1.getString("name"));
                                    paymentModes.setDescription(jsonObject1.getString("description"));
                                    paymentModes.setCreated_user_id(jsonObject1.getString("created_user_id"));
                                    paymentModes.setUpdated_user_id(jsonObject1.getString("updated_user_id"));
                                    paymentModes.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    paymentModes.setCreated_at(jsonObject1.getString("created_at"));
                                    paymentModes.setUpdated_at(jsonObject1.getString("updated_at"));
                                    paymentModes.setStatus(jsonObject1.getString("status"));


                                    paymentModesArrayList.add(paymentModes);
                                }

                                paymentModesAdapter.setrefresh(paymentModesArrayList);

                            }

                        } catch (JSONException e) {
                           // Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
               // Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

        getWallet(start_date_str,end_date_str,"","");
    }
    public void getWallet(String start_date_str, String end_date_str, String s, String s1){

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", start_date_str);
        uploadMap.put("end_date", end_date_str);
        uploadMap.put("type", s);
        uploadMap.put("status", s1);

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+json.toString());
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WALLETHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONObject dataobj=jsonObject.getJSONObject("data");


                                try{
                                    JSONObject paymentobj=dataobj.getJSONObject("user");
                                    walletamount=paymentobj.getString("wallet");
                                    tv_availbal.setText("Available balance ₹ "+paymentobj.getString("wallet"));
                                }catch (JSONException e2){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN",preferenceManager.getString(TOKEN_KEY));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void setdeleverymodechanged(PaymentModes paymentModes) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(paymentModes.getName());
        alertDialogBuilder
                .setMessage("Do You Want To Procced")
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       if (paymentModes.getId().equalsIgnoreCase("1")){
                           dialog.cancel();
                           sendPaymentstatus(""+1,"",""+totalamount,""+1,"");
                       }else {
                           dialog.cancel();
                           razorPayPayment(""+totalamount);
                          /* Intent intent=new Intent(mContext,PaymentGateways.class);
                           intent.putExtra("totatlpayamount", Math.round(1) + "");
                           startActivity(intent);*/
                       }
                    }

                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        cb_walet.setChecked(false);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void razorPayPayment(String totatlAmount) {
        final Checkout co = new Checkout();
        co.setImage(R.drawable.nextclick_icon_receipt);
        Activity activity = this;
        try {
            JSONObject orderRequest = new JSONObject();
            // orderRequest.put("amount", totatlAmount+"00"); // amount in the smallest currency unit
            orderRequest.put("amount", "100"); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", "order_rcptid_11");
            orderRequest.put("payment_capture", false);
            orderRequest.put("image", R.drawable.nextclick_logo_white);

            JSONObject readOnly = new JSONObject();
            readOnly.put("email",true);
            readOnly.put("contact",true);
            orderRequest.put("readOnly", readOnly);

            co.open((Activity) activity, orderRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            UImsgs.showToast(mContext, String.valueOf(e));
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            UImsgs.showToast(mContext, String.valueOf(e));
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        }


    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        relative_layout_.setVisibility(View.VISIBLE);
        View v = getWindow().getDecorView().getRootView();
        UImsgs.showSnackBar(v, IErrors.ORDER_PLACED);
      //  String data = bundle.getString("mainFoodOrder");
        System.out.println("aaaaaaaaaa  data sucess  "+s+paymentData.toString());

        System.out.println("aaaaaaaaaa sucess  "+paymentData.getOrderId()+" "+paymentData.getPaymentId()+" "+paymentData.getSignature()+" "+
                paymentData.getUserContact()+"  "+paymentData.getData().toString()+" "+paymentData.getUserEmail());
       // proceedtoPayClick(data, v);
        sendPaymentstatus(""+2,paymentData.getPaymentId(),""+totalamount,""+2,"");

    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        try{
            System.out.println("aaaaaaaaaa  payment  error  "+paymentData.toString()+"  "+s);
        }catch (NullPointerException e){

        }
        System.out.println("aaaaaaaaaa  payment  error  "+s);
       // UImsgs.showToast(mContext, String.valueOf("Error " + "  " + i + " " + s));
        try {
            JSONObject jsonObject1=new JSONObject(s);
            JSONObject errorobj=jsonObject1.getJSONObject("error");
            String description=errorobj.getString("description");
            sendPaymentstatus(""+3,"1",""+totalamount,""+3,description);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void proceedtoPayClick(final String data) {
        {
            //final String data = json.toString();
            Log.d("PaymentData", "" + data);

            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERSEND, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        System.out.println("aaaaaaaaa  payment response  "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            //
                            int http_code = jsonObject.getInt("http_code");
                            if (http_code == 201) {
                                succesDialog("");
                            }

                        } else {
                          //  UImsgs.showSnackBar(mContext, IErrors.ORDER_NOT_PLACED);
                            Toast.makeText(mContext, " + " + response , Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                       // Toast.makeText(mContext, "Catch "+e, Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("aaaaaaaa error "+error.getMessage());
                   // Toast.makeText(mContext, "Error " + error, Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                    return map;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes("utf-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };
            requestQueue.add(stringRequest);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        }
    }

    private void succesDialog(String message){
        ImageView success_gif;
        Button success_ok_btn;
        TextView tv_ordermessage;
        final Dialog dialog = new Dialog(mContext);
        Window window = dialog.getWindow();
        //window.requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.order_success_layout);
        dialog.setCancelable(false);
        window.setGravity(CENTER);

        success_ok_btn = dialog.findViewById(R.id.success_ok_btn);
        success_gif = dialog.findViewById(R.id.success_gif);
        tv_ordermessage = dialog.findViewById(R.id.tv_ordermessage);

        tv_ordermessage.setText("Your order has been placed successfully");
        // tv_ordermessage.setText(""+message);
        Glide.with(mContext)
                .load(R.drawable.ordsucess)
                .into(success_gif);

        success_ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //Intent intent = new Intent(mContext, MainHistoryActivity.class);
               /* Intent intent = new Intent(mContext, OrderHistoryActivity.class);
                startActivity(intent);
                finish();*/
                Intent intent = new Intent(getApplicationContext(), OrderHistoryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });


        window.setGravity(CENTER);
        dialog.show();
    }

    public void sendPaymentstatus(String paymentid,String paymenttransactionid,String amount,String status,String mesage){
        relative_layout_.setVisibility(GONE);


        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("payment_method_id", paymentid);
        uploadMap.put("payment_gw_txn_id", paymenttransactionid);
        uploadMap.put("amount", totalamount);
        uploadMap.put("status", status);
        uploadMap.put("message", mesage);

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaa json "+json.toString());
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PAYMENTSTATUS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                          //  Toast.makeText(PaymentModesActivity.this, ""+mesage, Toast.LENGTH_SHORT).show();
                            if (status){
                                if (paymentid.equalsIgnoreCase("3")){
                                    Toast.makeText(PaymentModesActivity.this, ""+mesage, Toast.LENGTH_SHORT).show();
                                }else {
                                    JSONObject dataobject = jsonObject.getJSONObject("data");
                                    String payment_id = dataobject.getString("payment_id");

                                    if (DeliveryMode == DeliveryMode_DELIVERY_PATNER)
                                        sendDataafterpaymentBySubOrders(payment_id);
                                    else
                                        sendDataafterpayment(payment_id);
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void sendPayment(String data){

        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERSEND,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");

                               succesDialog(message);
                            }

                        } catch (JSONException e) {
                         //   Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                            System.out.println("aaaaaaaaaa   catch " + response.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
               // Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }

        start_date_str=year+"-"+month+"-"+day;
        end_date_str=year+"-"+month+"-"+day;

       // start_date.setText(""+start_date_str);
      //  end_date.setText(""+end_date_str);

        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calander.getTime());
          /* int cHour = calander.get(Calendar.Ho);
            int cMinute = calander.get(Calendar.MINUTE);
            int cSecond = calander.get(Calendar.SECOND);
    */
    }

}