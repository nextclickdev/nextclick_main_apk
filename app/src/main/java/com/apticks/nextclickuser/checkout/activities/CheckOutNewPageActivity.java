package com.apticks.nextclickuser.checkout.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.NetworkUtil;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.adapters.DeleveryModesAdapter;
import com.apticks.nextclickuser.checkout.adapters.ProductsAdapter;
import com.apticks.nextclickuser.checkout.adapters.VendorAdapter;
import com.apticks.nextclickuser.checkout.models.CheckoutModel;
import com.apticks.nextclickuser.checkout.models.DeleveryModes;
import com.apticks.nextclickuser.checkout.models.DeleveryfeeDetails;
import com.apticks.nextclickuser.checkout.models.Permissionweight;
import com.apticks.nextclickuser.checkout.models.Product;
import com.apticks.nextclickuser.checkout.models.ProductDetailsModel;
import com.apticks.nextclickuser.checkout.models.ProductImages;
import com.apticks.nextclickuser.checkout.models.Products;
import com.apticks.nextclickuser.checkout.models.SectionProducts;
import com.apticks.nextclickuser.checkout.models.Tax;
import com.apticks.nextclickuser.checkout.models.VendorDetails;
import com.apticks.nextclickuser.checkout.models.WeightPermissable;
import com.apticks.nextclickuser.products.activities.ChangeAddressActivity;
import com.apticks.nextclickuser.products.adapters.CheckoutCartAdapter;
import com.apticks.nextclickuser.products.adapters.VarientsAdapter;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.SaveAddress;
import com.apticks.nextclickuser.products.model.VarientModel;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CARTDELETE;
import static com.apticks.nextclickuser.Config.Config.CHECKOUTPRODUCTS;
import static com.apticks.nextclickuser.Config.Config.DELIVERYMODES;
import static com.apticks.nextclickuser.Constants.Constants.ADDRESS;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class CheckOutNewPageActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView add_image_address,image_back;
    private LinearLayout layout_address;
    RelativeLayout layout_wallet,layout_promocode,layout_deliveryfee,layout_promo_code;
    private RadioGroup radio_delivery;
    private TextView change_address,vendor_name,vendor_address,items_count,tv_promo_code,tv_wallet_code,tv_sub_total,tv_discount,
            tv_deliveryfee,items_tax,tv_promo_apllied,tv_item_grand_total_total,customer_address,tv_proceedtopay,tv_subtotaltext;
    private Context mContext;
    private RecyclerView cartItemsRecycler,recycle_deleverymodes;
    private String shopname,shopaddress,deleverymodeid,shippingaddressid="";
    private VarientsAdapter varientsAdapter;
    private CustomDialog customDialog;
    private ArrayList<DeleveryModes> deleverymodeslist;
    private DeleveryModesAdapter deleveryModesAdapter;
    private ArrayList<Products> productslist;
    private VendorAdapter vendorAdapter;
    private PreferenceManager preferenceManager;
    private ArrayList<CartModel> cartlist;
    private Gson gson;
    private int discount,tax,deleveryfee=0,promocodeamount;
    private double totalbill=0.0,subtotal=0.0,discountprice=0.0,taxamount=0.0;
    DeleveryModes deleveryModes;
    ArrayList<CheckoutModel> checkoutlist;
    SaveAddress saveAddress;
    boolean can_i_proceed_to_pay;
    int whichactivity;
    VarientModel varientmodel;
    String quantity;
    ArrayList<ProductDetailsModel> productlistreorder;
  //  ProductsAdapter productsAdapter;
    CheckoutCartAdapter productsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkoutnew);
        getSupportActionBar().hide();

        init();

        whichactivity=getIntent().getIntExtra("whichactivity",0);
        gson=new Gson();

        if (whichactivity==1){
            varientmodel= (VarientModel) getIntent().getSerializableExtra("variantlist");
            quantity=getIntent().getStringExtra("quantity");
        }else if (whichactivity==2){
            productlistreorder=new ArrayList<>();
            String carListAsString = getIntent().getStringExtra("productlist");
            Type type = new TypeToken<List<ProductDetailsModel>>(){}.getType();
            productlistreorder = gson.fromJson(carListAsString, type);
        }
        else {
            cartlist=new ArrayList<>();
            String carListAsString = getIntent().getStringExtra("cartlist");
            Type type = new TypeToken<List<CartModel>>(){}.getType();
            cartlist = gson.fromJson(carListAsString, type);
            System.out.println("aaaaaaaaa  cart list  "+cartlist.toString());
            productsAdapter=new CheckoutCartAdapter(mContext,productslist,cartlist);
            cartItemsRecycler.setAdapter(productsAdapter);
        }

        if (NetworkUtil.isNetworkConnected(mContext)) {
            getDeleverymodes();
        } else {
            Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
        }

    }
    public void init(){
        mContext= CheckOutNewPageActivity.this;
        add_image_address=findViewById(R.id.add_image_address);
        image_back=findViewById(R.id.image_back);
        radio_delivery=findViewById(R.id.radio_delivery);
        layout_address=findViewById(R.id.layout_address);
        change_address=findViewById(R.id.change_address);
        vendor_name=findViewById(R.id.vendor_name);
        vendor_address=findViewById(R.id.vendor_address);
        items_count=findViewById(R.id.items_count);
        cartItemsRecycler=findViewById(R.id.cartItemsRecycler);
        tv_promo_code=findViewById(R.id.tv_promo_code);
        layout_wallet=findViewById(R.id.layout_wallet);
        layout_promocode=findViewById(R.id.layout_promocode);
        tv_wallet_code=findViewById(R.id.tv_wallet_code);
        tv_sub_total=findViewById(R.id.tv_sub_total);
        tv_discount=findViewById(R.id.tv_discount);
        layout_deliveryfee=findViewById(R.id.layout_deliveryfee);
        tv_deliveryfee=findViewById(R.id.tv_deliveryfee);
        items_tax=findViewById(R.id.items_tax);
        layout_promo_code=findViewById(R.id.layout_promo_code);
        tv_promo_apllied=findViewById(R.id.tv_promo_apllied);
        tv_item_grand_total_total=findViewById(R.id.tv_item_grand_total_total);
        customer_address=findViewById(R.id.customer_address);
        tv_proceedtopay=findViewById(R.id.tv_proceedtopay);
        recycle_deleverymodes=findViewById(R.id.recycle_deleverymodes);
        tv_subtotaltext=findViewById(R.id.tv_subtotaltext);

        add_image_address.setOnClickListener(this::onClick);
        change_address.setOnClickListener(this::onClick);
        image_back.setOnClickListener(this::onClick);
        tv_proceedtopay.setOnClickListener(this::onClick);

        customDialog=new CustomDialog(mContext);
        shopname=getIntent().getStringExtra(reastaurantName);
        shopaddress=getIntent().getStringExtra(ADDRESS);

        preferenceManager=new PreferenceManager(mContext);

        vendor_name.setText(shopname);
        vendor_address.setText(shopaddress);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        deleverymodeslist=new ArrayList<>();
        productslist=new ArrayList<>();
        checkoutlist=new ArrayList<>();
        deleveryModesAdapter=new DeleveryModesAdapter(mContext,deleverymodeslist,1);
        recycle_deleverymodes.setAdapter(deleveryModesAdapter);
        recycle_deleverymodes.setLayoutManager(linearLayoutManager);

        cartItemsRecycler.setLayoutManager(new GridLayoutManager(CheckOutNewPageActivity.this,1));
        varientsAdapter=new VarientsAdapter(mContext,1);

      /*  vendorAdapter=new VendorAdapter(mContext,checkoutlist);
        cartItemsRecycler.setAdapter(vendorAdapter);*/


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.change_address:
                Intent intent=new Intent(mContext, ChangeAddressActivity.class);
                startActivityForResult(intent, 2);
                break;
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_proceedtopay:
                if (deleverymodeid.equalsIgnoreCase("2")){
                    try{
                        if(!saveAddress.getId().isEmpty()){
                            if (can_i_proceed_to_pay){
                                Gson gson=new Gson();
                                String checkoutstring = gson.toJson(checkoutlist);
                                Intent i = new Intent(mContext, PaymentModesActivity.class);
                                i.putExtra("checkoutlist",checkoutstring);
                                i.putExtra("shippingaddressid",shippingaddressid);
                                i.putExtra("deleverymodeid",deleverymodeid);
                                i.putExtra("totalamount",tv_item_grand_total_total.getText().toString());
                                startActivity(i);
                            }else {
                                String productnames="";
                                for (int i=0;i<checkoutlist.size();i++){
                                    for (int k=0;k<checkoutlist.get(i).getProductslist().size();k++){
                                        String check=checkoutlist.get(i).getProductslist().get(k).getIs_available();
                                        if (check.equalsIgnoreCase("false")){
                                            productnames=productnames+checkoutlist.get(i).getProductslist().get(k).getProduct().getName();                                       }
                                    }
                                }

                                Toast.makeText(mContext, ""+productnames+" not available", Toast.LENGTH_SHORT).show();
                            }

                        }else {
                            Toast.makeText(mContext, "Please Add Delevery Address", Toast.LENGTH_SHORT).show();
                        }
                    }catch (NullPointerException e){
                        Toast.makeText(mContext, "Please Add Delevery Address", Toast.LENGTH_SHORT).show();
                    }

                }else {
                    Gson gson=new Gson();
                    String checkoutstring = gson.toJson(checkoutlist);
                    Intent i = new Intent(mContext, PaymentModesActivity.class);
                    i.putExtra("checkoutlist",checkoutstring);
                    i.putExtra("shippingaddressid",shippingaddressid);
                    i.putExtra("deleverymodeid",deleverymodeid);
                    i.putExtra("totalamount",tv_item_grand_total_total.getText().toString());
                    startActivity(i);
                }


                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode==2){
                saveAddress= (SaveAddress) data.getSerializableExtra("addressname");

               // String address = data.getStringExtra("addressname");
              //  String id = data.getStringExtra("id");
                System.out.println("aaaaaaaa  carListAsString "+saveAddress.getAddress());
                shippingaddressid=saveAddress.getId();
                customer_address.setText(saveAddress.getAddress());
                getProductspassbody(saveAddress.getId(),deleverymodeid);

            }
        }catch (NullPointerException e){
            System.out.println("aaaaaaaa catch "+e.getMessage());
        }

    }

    private void getDeleverymodes() {
        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DELIVERYMODES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + responsearray.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    DeleveryModes deleveryModes=new DeleveryModes();
                                    deleveryModes.setId(jsonObject1.getString("id"));
                                    deleveryModes.setName(jsonObject1.getString("name"));
                                    deleveryModes.setDesc(jsonObject1.getString("desc"));
                                    deleveryModes.setIs_having_delivery_fee(jsonObject1.getString("is_having_delivery_fee"));

                                    if (jsonObject1.getString("id").equalsIgnoreCase("2")){
                                        deleveryModes.setStatus("2");
                                        deleverymodeid=jsonObject1.getString("id");
                                        if (NetworkUtil.isNetworkConnected(mContext)) {
                                            getProductspassbody("",""+deleverymodeid);
                                        } else {
                                            Toast.makeText(CheckOutNewPageActivity.this, "No internet connection! ", Toast.LENGTH_SHORT).show();
                                        }
                                      //  layout_address.setVisibility(View.VISIBLE);
                                    }else {
                                        deleveryModes.setStatus("1");
                                    }

                                   deleverymodeslist.add(deleveryModes);
                                }

                                deleveryModesAdapter.setrefresh(deleverymodeslist);

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setdeleverymodechanged(DeleveryModes deleveryModes, boolean check) {
        this.deleveryModes=deleveryModes;
        this.deleverymodeid=deleveryModes.getId();
        if (check){
            Animation animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);
            layout_address.startAnimation(animSlideDown);
            layout_address.setVisibility(View.VISIBLE);
            try{
                shippingaddressid=saveAddress.getId();
            }catch (NullPointerException e){

            }
        }else {
            Animation animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_up);
            layout_address.startAnimation(animSlideDown);
            shippingaddressid="";
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    layout_address.setVisibility(View.GONE);
                }
            }, 500);
        }
        deleveryModesAdapter.notifyDataSetChanged();
        getProductspassbody("",deleverymodeid);
    }

    public void getProductspassbody(String shipingaddress,String deleverymodeid){

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("shipping_address_id", shipingaddress);
        uploadMap.put("dalivery_mode_id", deleverymodeid);

        if(whichactivity==1){
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject1=new JSONObject();
            try {
                jsonObject1.put("item_id",varientmodel.getItem_id());
                jsonObject1.put("vendor_product_variant_id",varientmodel.getId());
                jsonObject1.put("qty",quantity);
                jsonObject1.put("vendor_user_id",varientmodel.getVendor_user_id());
                jsonArray.put(jsonObject1);
                JSONObject json = new JSONObject(uploadMap);
                try {
                    json.put("products",jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();

                    System.out.println("aaaaaaaaa catch 2  "+e.getMessage());
                }
                if (NetworkUtil.isNetworkConnected(mContext)) {
                    getProductDetails(json);
                } else {
                    Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
                }
            }catch (JSONException e) {
                e.printStackTrace();
            }


        }else if (whichactivity==2){
            JSONArray jsonArray = new JSONArray();

            for (int i=0; i < productlistreorder.size(); i++) {
                try {
                    JSONObject jsonObject1=new JSONObject();
                    jsonObject1.put("item_id",productlistreorder.get(i).getItem_id());
                    jsonObject1.put("vendor_product_variant_id",productlistreorder.get(i).getVendor_product_variant_id());
                    jsonObject1.put("qty",productlistreorder.get(i).getQty());
                   // jsonObject1.put("vendor_user_id",productlistreorder.get(i).getVendor_user_id());
                    jsonArray.put(jsonObject1);
                } catch (JSONException e) {
                    System.out.println("aaaaaaaaa catch 1  "+e.getMessage());
                    e.printStackTrace();
                }
            }
            JSONObject json = new JSONObject(uploadMap);
            try {
                json.put("products",jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();

                System.out.println("aaaaaaaaa catch 2  "+e.getMessage());
            }
            if (NetworkUtil.isNetworkConnected(mContext)) {
                getProductDetails(json);
            } else {
                Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
            }
        }  else{
            JSONArray jsonArray = new JSONArray();

            for (int i=0; i < cartlist.size(); i++) {
                try {
                    JSONObject jsonObject1=new JSONObject();
                    jsonObject1.put("item_id",cartlist.get(i).getItem_id());
                    jsonObject1.put("vendor_product_variant_id",cartlist.get(i).getVendor_product_varinat_id());
                    jsonObject1.put("qty",cartlist.get(i).getQty());
                    jsonObject1.put("vendor_user_id",cartlist.get(i).getVendor_user_id());
                    jsonArray.put(jsonObject1);
                } catch (JSONException e) {
                    System.out.println("aaaaaaaaa catch 1  "+e.getMessage());
                    e.printStackTrace();
                }
            }
            JSONObject json = new JSONObject(uploadMap);
            try {
                json.put("products",jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();

                System.out.println("aaaaaaaaa catch 2  "+e.getMessage());
            }
            if (NetworkUtil.isNetworkConnected(mContext)) {
                getProductDetails(json);
            } else {
                Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void getProductDetails(JSONObject json) {
        cleardata();
        final String dataStr = json.toString();
        System.out.println("aaaaaaaaaa data "+dataStr);
        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CHECKOUTPRODUCTS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status && http_code==200){
                                JSONObject dataobject=jsonObject.getJSONObject("data");
                                 can_i_proceed_to_pay=dataobject.getBoolean("can_i_proceed_to_pay");

                                JSONArray responsearray=dataobject.getJSONArray("products_group_by_vendor");

                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);

                                    CheckoutModel checkoutModel=new CheckoutModel();

                                    try{
                                        VendorDetails vendorDetails=new VendorDetails();
                                        JSONObject vendorobj=jsonObject1.getJSONObject("vendor_details");

                                        vendorDetails.setId(vendorobj.getString("id"));
                                        vendorDetails.setName(vendorobj.getString("name"));
                                        vendorDetails.setUnique_id(vendorobj.getString("unique_id"));
                                        vendorDetails.setAddress(vendorobj.getString("address"));
                                        vendorDetails.setLandmark(vendorobj.getString("landmark"));
                                        vendorDetails.setPincode(vendorobj.getString("pincode"));
                                        vendorDetails.setAvailability(vendorobj.getString("availability"));
                                        vendorDetails.setVendor_user_id(vendorobj.getString("vendor_user_id"));

                                        checkoutModel.setVendorDetails(vendorDetails);
                                        vendor_address.setText(vendorobj.getString("address"));
                                        vendor_name.setText(vendorobj.getString("name"));
                                    }catch (JSONException e){
                                        System.out.println("aaaaaa catch vendor detail not found");
                                    }

                                    try{
                                        DeleveryfeeDetails deleveryfeeDetails=new DeleveryfeeDetails();
                                        JSONObject deleveryobject=jsonObject1.getJSONObject("delivery_fee");
                                        deleveryfeeDetails.setId(deleveryobject.getString("id"));
                                        deleveryfeeDetails.setFlat_rate(deleveryobject.getString("flat_rate"));
                                        deleveryfeeDetails.setPer_km(deleveryobject.getString("per_km"));
                                        deleveryfeeDetails.setVehicle_type_id(deleveryobject.getString("vehicle_type_id"));
                                        checkoutModel.setDelivery_fee(deleveryfeeDetails);
                                        deleveryfee=deleveryfee+Integer.parseInt(deleveryobject.getString("flat_rate"));
                                        layout_deliveryfee.setVisibility(View.VISIBLE);
                                        tv_deliveryfee.setText(""+deleveryfee);
                                    }catch (JSONException e){
                                        layout_deliveryfee.setVisibility(View.GONE);
                                        System.out.println("aaaaaa catch deleveryfee not found");
                                        DeleveryfeeDetails deleveryfeeDetails=new DeleveryfeeDetails();
                                        deleveryfeeDetails.setId("");
                                        deleveryfeeDetails.setFlat_rate("0");
                                        deleveryfeeDetails.setPer_km("");
                                        deleveryfeeDetails.setVehicle_type_id("");
                                        checkoutModel.setDelivery_fee(deleveryfeeDetails);
                                    }


                                    JSONObject permitsobj=jsonObject1.getJSONObject("is_this_permissible_wight");

                                    WeightPermissable weightPermissable=new WeightPermissable();
                                    weightPermissable.setStatus(permitsobj.getBoolean("status"));
                                    JSONArray weightsarray=permitsobj.getJSONArray("permissible_weight");
                                    ArrayList<Permissionweight> listweights=new ArrayList<>();
                                    for (int m=0;m<weightsarray.length();m++){
                                        Permissionweight permissionweight=new Permissionweight();
                                        JSONObject permssionobj=weightsarray.getJSONObject(m);
                                        permissionweight.setMin_capacity(permssionobj.getString("min_capacity"));
                                        permissionweight.setAs_max_capacity(permssionobj.getString("as_max_capacity"));
                                        listweights.add(permissionweight);
                                    }
                                    weightPermissable.setWeightlist(listweights);

                                    checkoutModel.setWeightPermissable(weightPermissable);

                                    JSONArray productsarray=jsonObject1.getJSONArray("products");
                                    productslist=new ArrayList<>();
                                    for (int j=0;j<productsarray.length();j++){
                                        JSONObject productsobject=productsarray.getJSONObject(j);
                                        Products products=new Products();
                                        products.setId(productsobject.getString("id"));
                                        products.setSku(productsobject.getString("sku"));
                                        products.setPrice(productsobject.getString("price"));
                                        products.setStock(productsobject.getString("stock"));
                                        products.setDiscount(productsobject.getString("discount"));
                                        products.setStatus(productsobject.getString("status"));
                                        products.setItem_id(productsobject.getString("item_id"));
                                        products.setSection_item_id(productsobject.getString("section_item_id"));
                                        products.setQty(productsobject.getString("qty"));
                                        products.setIs_available(productsobject.getString("is_available"));

                                        double productprice=Double.parseDouble(productsobject.getString("price"));
                                        int quantity=Integer.parseInt(productsobject.getString("qty"));
                                        subtotal=subtotal+(productprice * quantity);

                                        discount=discount+Integer.parseInt(productsobject.getString("discount"));

                                        double ddiscount=Double.parseDouble(productsobject.getString("discount"));
                                        double price=Integer.parseInt(productsobject.getString("qty"))*
                                                Double.parseDouble(productsobject.getString("price"));

                                        double totalprice =price- ((price / 100.0f) * ddiscount);
                                        double tprice=price-totalprice;

                                        discountprice=discountprice+tprice;

                                        try{
                                            JSONObject taxobj=productsobject.getJSONObject("tax");
                                            Tax tax=new Tax();
                                            tax.setId(taxobj.getString("id"));
                                            tax.setTax(taxobj.getString("tax"));
                                            tax.setRate(taxobj.getString("rate"));
                                            products.setTax(tax);

                                            double taxrate=Double.parseDouble(taxobj.getString("rate"));
                                            taxamount=taxamount+((price / 100.0f) * taxrate);
                                        }catch (JSONException e){
                                            Tax tax=new Tax();
                                            tax.setId("0");
                                            tax.setTax("0");
                                            tax.setRate("0");
                                            products.setTax(tax);
                                            System.out.println("aaaaaaaaa tax not found");
                                        }



                                        ArrayList<ProductImages> imagelist=new ArrayList<>();
                                        try{
                                            JSONArray imagesarray=productsobject.getJSONArray("item_images");
                                            for (int k=0;k<imagesarray.length();k++){
                                                JSONObject imageobject=imagesarray.getJSONObject(k);
                                                ProductImages productImages=new ProductImages();
                                                productImages.setItem_id(imageobject.getString("item_id"));
                                                productImages.setId(imageobject.getString("id"));
                                                productImages.setExt(imageobject.getString("ext"));
                                                productImages.setImage(imageobject.getString("image"));
                                                imagelist.add(productImages);
                                            }
                                        }catch (JSONException e){
                                            ProductImages productImages=new ProductImages();
                                            productImages.setItem_id("");
                                            productImages.setId("");
                                            productImages.setExt("");
                                            productImages.setImage("");
                                            imagelist.add(productImages);
                                        }

                                        products.setImagelist(imagelist);

                                        JSONObject itemobject=productsobject.getJSONObject("item");
                                        Product product=new Product();
                                        product.setId(itemobject.getString("id"));
                                        product.setProduct_code(itemobject.getString("product_code"));
                                        product.setName(itemobject.getString("name"));
                                        product.setDesc(itemobject.getString("desc"));
                                        product.setStatus(itemobject.getString("status"));
                                        products.setProduct(product);


                                        JSONObject secobject=productsobject.getJSONObject("section_item");

                                        SectionProducts sectionProducts=new SectionProducts();
                                        sectionProducts.setId(secobject.getString("id"));
                                        sectionProducts.setName(secobject.getString("name"));
                                        sectionProducts.setDesc(secobject.getString("desc"));
                                        sectionProducts.setWeight(secobject.getString("weight"));
                                        sectionProducts.setStatus(secobject.getString("status"));

                                        products.setSectionProducts(sectionProducts);

                                        productslist.add(products);

                                    }

                                    checkoutModel.setProductslist(productslist);
                                    //tv_subtotaltext.setText("SubTotal ("+productslist.size()+" Items )");

                                    checkoutlist.add(checkoutModel);
                                }
                                paymentcalculation();
                                productsAdapter.setrefresh(productslist);
                                eachevendorsummary();
                              //  vendorAdapter.setrefresh(checkoutlist);

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "Cart is empty", Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                            finish();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void cleardata(){
        productslist.clear();
        checkoutlist.clear();
        discount=0;tax=0;deleveryfee=0;promocodeamount=0;
        totalbill=0.0;subtotal=0.0;discountprice=0.0;taxamount=0.0;
    }
    public void paymentcalculation(){

        tv_subtotaltext.setText("SubTotal ("+productslist.size()+" Items )");
        tv_sub_total.setText(""+subtotal);

        tv_discount.setText(""+discount+" % off"+"(-"+String.format("%.2f",discountprice)+" Saved)");
        items_tax.setText(String.format("%.2f",taxamount));

       // items_tax.setText("+"+taxamount);

        Double totalbill=subtotal-discountprice+deleveryfee+taxamount;

        tv_item_grand_total_total.setText(String.format("%.2f",totalbill));
       // tv_item_grand_total_total.setText(""+totalbill);
    }

    public void setDelete(Products cartModel) {
        for (int i=0;i<cartlist.size();i++){
            if (cartlist.get(i).getItem_id().equalsIgnoreCase(cartModel.getItem_id())){
                cartlist.remove(i);
            }
        }
        System.out.println("aaaaaaaaaa cartlist  "+cartlist.size());
        getProductspassbody("",deleverymodeid);
    }

    public void eachevendorsummary(){
        for (int i=0;i<checkoutlist.size();i++){

            double grandtotal=0.0,totalsub=0.0,taxamount1=0.0,discountamt=0.0,tax=0.0;
            for (int k=0;k<checkoutlist.get(i).getProductslist().size();k++){
                double productprice=Double.parseDouble(checkoutlist.get(i).getProductslist().get(k).getPrice())
                        *(Integer.parseInt(checkoutlist.get(i).getProductslist().get(k).getQty()));

                totalsub=totalsub+productprice;

                if (!checkoutlist.get(i).getProductslist().get(k).getDiscount().equalsIgnoreCase("0")){
                   double discount=Double.parseDouble(checkoutlist.get(i).getProductslist().get(k).getDiscount());
                    discountamt=discountamt+((productprice / 100.0f) * discount);

                  //  System.out.println("aaaaaaaaa discount "+discount+"  "+discountamt+"  "+productprice);
                }

                try{
                    tax=Double.parseDouble(checkoutlist.get(i).getProductslist().get(k).getTax().getRate());
                    taxamount1=taxamount1+((productprice / 100.0f) * tax);
                    //  System.out.println("aaaaaaaaa tax "+tax+"  "+taxamount+"  "+productprice);
                }catch (NullPointerException e){

                }

                grandtotal=totalsub+taxamount1-discountamt;
            }

            try{
                double deleveryfee=Double.parseDouble(checkoutlist.get(i).getDelivery_fee().getFlat_rate());
                grandtotal=grandtotal+deleveryfee;
            }catch (NullPointerException e){

            }


            checkoutlist.get(i).setTax(taxamount1);
            checkoutlist.get(i).setDiscount(discountamt);
            checkoutlist.get(i).setGrandtotal(grandtotal);
            checkoutlist.get(i).setSubtotal(totalsub);

        }
    }

    public void removeProduct(CartModel products) {

        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        System.out.println("aaaaaaa  dlele  "+CARTDELETE+"/"+products.getId());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTDELETE+"/"+products.getId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customDialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("aaaaaaa response " + jsonObject.toString());
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {

                        System.out.println("aaaaaaaaaa  response  "+jsonObject.toString());
                         cartlist.remove(products);
                        getProductspassbody(""+shopaddress,""+deleverymodeid);
                       /* if (cartlist.size()==0){
                           layout_cartitems.setVisibility(View.GONE);
                            callAddtocart(data);
                        }else {
                            layout_cartitems.setVisibility(View.VISIBLE);
                        }*/


                    }
                }catch (JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }
        };
        requestQueue.add(stringRequest);
    }


}