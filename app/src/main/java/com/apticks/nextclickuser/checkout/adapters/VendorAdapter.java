package com.apticks.nextclickuser.checkout.adapters;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.checkout.activities.DeliveryFeeInfoActivity;
import com.apticks.nextclickuser.checkout.activities.PaymentModesActivity;
import com.apticks.nextclickuser.checkout.models.CheckoutModel;
import com.apticks.nextclickuser.checkout.models.Products;
import com.apticks.nextclickuser.products.activities.ChangeAddressActivity;
import com.apticks.nextclickuser.products.model.CartModel;
import com.google.gson.Gson;

import org.apache.commons.lang3.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

public class VendorAdapter extends RecyclerView.Adapter<VendorAdapter.ItemViewHolder> {

    private final CheckOutPageActivity checkOutPageActivity;
    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    ArrayList<CheckoutModel> cartllist;
    Context context;
    int mExpandedPosition,previousExpandedPosition;
    boolean ischecked;

    public VendorAdapter(CheckOutPageActivity checkOutPageActivity, Context mContext, ArrayList<CheckoutModel> productslist) {
        this.checkOutPageActivity=checkOutPageActivity;
        this.context=mContext;
        this.cartllist=productslist;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int position) {
        CheckoutModel item = cartllist.get(position);
        try{
            itemViewHolder.tv_item_title.setText(item.getVendorDetails().getName());
            itemViewHolder.tv_item_description.setText(item.getVendorDetails().getAddress());
        }catch (NullPointerException e){

        }



        // Create layout manager with initial prefetch item count
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                itemViewHolder.rvSubItem.getContext(),
                LinearLayoutManager.VERTICAL,
                false
        );

        final boolean isExpanded = position==mExpandedPosition;
        itemViewHolder.rvSubItem.setVisibility(isExpanded?View.VISIBLE:View.GONE);
        itemViewHolder.itemView.setActivated(isExpanded);

        if (isExpanded)
            previousExpandedPosition = position;

       // if (cartllist.size()==position-1){
            for (int i=0;i<cartllist.size();i++){
                double price=0.0,discount=0.0,tax=0.0,discountamt=0.0,taxamount=0.0,deleveryfee=0.0,totalsub=0;

                for (int k=0;k<cartllist.get(i).getProductslist().size();k++){

                    double productprice=Double.parseDouble(cartllist.get(i).getProductslist().get(k).getPrice())
                            *(Integer.parseInt(cartllist.get(i).getProductslist().get(k).getQty()));
                    totalsub=totalsub+productprice;
                    price=price+Double.parseDouble(cartllist.get(i).getProductslist().get(k).getPrice());

                    if (!cartllist.get(i).getProductslist().get(k).getDiscount().equalsIgnoreCase("0")){
                        discount=Double.parseDouble(cartllist.get(i).getProductslist().get(k).getDiscount());
                        // System.out.println("aaaaaaaaaa discount "+discount+discountamt);
                        discountamt=discountamt+((productprice / 100.0f) * discount);

                    /* discountamt=((productprice / 100.0f) * discount);

                     double totalprice =productprice- ((productprice / 100.0f) * discount);
                     double tprice=productprice-totalprice;

                     discountamt=discountamt+tprice;*/
                        // System.out.println("aaaaaaaaa discount "+discount+"  "+discountamt+"  "+productprice);

                    }
                    try{
                        tax=Double.parseDouble(cartllist.get(i).getProductslist().get(k).getTax().getRate());
                        taxamount=taxamount+((productprice / 100.0f) * tax);
                        //  System.out.println("aaaaaaaaa tax "+tax+"  "+taxamount+"  "+productprice);
                    }catch (NullPointerException e){

                    }

                    try{
                      //  System.out.println("aaaaaaaaa  delevery fee "+cartllist.get(i).getDelivery_fee().getFlat_rate()+ i+" "+position);
                      //  itemViewHolder.tv_deliveryfee.setText(""+cartllist.get(position).getDelivery_fee().getFlat_rate());
                        deleveryfee=Double.parseDouble(cartllist.get(i).getDelivery_fee().getFlat_rate());
                    }catch (NullPointerException e){

                    }

                    double totalamt=totalsub+taxamount-discountamt+deleveryfee;
                }

            }
     //   }

        itemViewHolder.tv_totalsub.setText(""+cartllist.get(position).getSubtotal());
        String deliveryFee=cartllist.get(position).getDelivery_fee().getFlat_rate();
        itemViewHolder.tv_deliveryfee.setText(""+deliveryFee);

        if (deliveryFee ==null || deliveryFee.equals("0")){
            itemViewHolder.img_delivery.setVisibility(View.GONE);
        }else {
            itemViewHolder.img_delivery.setVisibility(View.VISIBLE);
        }

        itemViewHolder.tv_totalamount.setText(""+String.format("%.2f",cartllist.get(position).getGrandtotal()));
      //  itemViewHolder.tv_discount.setText(""+String.format("%.2f",cartllist.get(position).getDiscount()));
        itemViewHolder.tv_discount.setText("- "+cartllist.get(position).getDiscount());
        try{
            itemViewHolder.tv_tax.setText(""+String.format("%.2f",cartllist.get(position).getTax()));
           // itemViewHolder.tv_tax.setText("+ "+cartllist.get(position).getTax());
        }catch (NullPointerException e){
        }

        if (cartllist.size()==1){
            itemViewHolder.plus_checked.setVisibility(View.GONE);
            itemViewHolder.cardview_payment.setVisibility(View.GONE);
        }else {
            itemViewHolder.plus_checked.setVisibility(View.VISIBLE);
            itemViewHolder.cardview_payment.setVisibility(View.VISIBLE);
        }

        itemViewHolder.rvSubItem.setVisibility(View.GONE);
      //  itemViewHolder.cardview_payment.setVisibility(View.GONE);
      //  itemViewHolder.plus_checked.setImageDrawable(context.getResources().getDrawable(R.drawable.plus));
        ischecked=true;
        itemViewHolder.rvSubItem.setVisibility(View.VISIBLE);
       // itemViewHolder.cardview_payment.setVisibility(View.VISIBLE);
     //   itemViewHolder.plus_checked.setImageDrawable(context.getResources().getDrawable(R.drawable.minus));

        layoutManager.setInitialPrefetchItemCount(item.getProductslist().size());

        // Create sub item view adapter
        ProductsAdapter subItemAdapter = new ProductsAdapter(context,item.getProductslist());
        itemViewHolder.rvSubItem.setLayoutManager(layoutManager);
        itemViewHolder.rvSubItem.setAdapter(subItemAdapter);
        itemViewHolder.rvSubItem.setRecycledViewPool(viewPool);

       // itemViewHolder.plus_checked.setImageDrawable(context.getResources().getDrawable(R.drawable.minus));
        itemViewHolder.plus_checked.setForeground(context.getResources().getDrawable(R.drawable.ic_baseline_remove_18));
        itemViewHolder.layoutBg.setBackground(context.getResources().getDrawable(R.drawable.background_gray_line));

        itemViewHolder.layout_delivery_fee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemViewHolder.img_delivery.getVisibility() == View.VISIBLE)
                    checkOutPageActivity.invokeDeliveryFeeInfoActivity(cartllist.get(position).getVendorDetails().getVendor_user_id(), false);
            }
        });


        itemViewHolder.plus_checked.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
               /* mExpandedPosition = isExpanded ? -1:position;
                notifyItemChanged(previousExpandedPosition);
                notifyItemChanged(position);*/

                if (ischecked){
                    itemViewHolder.rvSubItem.setVisibility(View.GONE);
                    itemViewHolder.cardview_payment.setVisibility(View.GONE);
                   // itemViewHolder.plus_checked.setImageDrawable(context.getResources().getDrawable(R.drawable.plus));
                    itemViewHolder.plus_checked.setForeground(context.getResources().getDrawable(R.drawable.ic_baseline_add_18));
                    itemViewHolder.layoutBg.setBackground(context.getResources().getDrawable(R.drawable.background_orange_line));

                    ischecked=false;
                }else {
                    ischecked=true;
                    itemViewHolder.rvSubItem.setVisibility(View.VISIBLE);
                    itemViewHolder.cardview_payment.setVisibility(View.VISIBLE);
                    //itemViewHolder.plus_checked.setImageDrawable(context.getResources().getDrawable(R.drawable.minus));

                    layoutManager.setInitialPrefetchItemCount(item.getProductslist().size());

                    // Create sub item view adapter
                    ProductsAdapter subItemAdapter = new ProductsAdapter(context,item.getProductslist());
                    itemViewHolder.rvSubItem.setLayoutManager(layoutManager);
                    itemViewHolder.rvSubItem.setAdapter(subItemAdapter);
                    itemViewHolder.rvSubItem.setRecycledViewPool(viewPool);
                    //itemViewHolder.plus_checked.setImageDrawable(context.getResources().getDrawable(R.drawable.minus));
                    itemViewHolder.plus_checked.setForeground(context.getResources().getDrawable(R.drawable.ic_baseline_remove_18));
                    itemViewHolder.layoutBg.setBackground(context.getResources().getDrawable(R.drawable.background_gray_line));
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return cartllist.size();
    }

    public void setrefresh(ArrayList<CheckoutModel> productslist) {
        this.cartllist=productslist;
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_item_title,tv_totalsub,tv_discount,tv_tax,tv_deliveryfee,tv_totalamount,tv_item_description;
        private RecyclerView rvSubItem;
        private TextView plus_checked;
        private CardView cardview_payment;
        LinearLayout layout_delivery_fee,layoutBg;
        ImageView img_delivery;


        ItemViewHolder(View itemView) {
            super(itemView);
            tv_item_title = itemView.findViewById(R.id.tv_item_title);
            tv_item_description = itemView.findViewById(R.id.tv_item_description);
            rvSubItem = itemView.findViewById(R.id.rv_sub_item);
            plus_checked = itemView.findViewById(R.id.plus_checked);
            tv_totalsub = itemView.findViewById(R.id.tv_totalsub);
            tv_discount = itemView.findViewById(R.id.tv_discount);
            tv_tax = itemView.findViewById(R.id.tv_tax);
            tv_deliveryfee = itemView.findViewById(R.id.tv_deliveryfee);
            tv_totalamount = itemView.findViewById(R.id.tv_totalamount);
            cardview_payment = itemView.findViewById(R.id.cardview_payment);
            layoutBg= itemView.findViewById(R.id.layoutBg);
            layout_delivery_fee= itemView.findViewById(R.id.layout_delivery_fee);
            img_delivery= itemView.findViewById(R.id.img_delivery);
        }
    }
}
