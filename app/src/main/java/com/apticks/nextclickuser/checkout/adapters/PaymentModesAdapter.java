package com.apticks.nextclickuser.checkout.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.PaymentModesActivity;
import com.apticks.nextclickuser.checkout.models.PaymentModes;

import java.util.ArrayList;

public class PaymentModesAdapter extends RecyclerView.Adapter<PaymentModesAdapter.ViewHolder>   {

    ArrayList<PaymentModes> paymentModesArrayList;
    Context context;
    boolean check;
    public PaymentModesAdapter(Context context, ArrayList<PaymentModes> paymentModesArrayList) {
        this.context=context;
        this.paymentModesArrayList=paymentModesArrayList;
    }
    @NonNull
    @Override
    public PaymentModesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.payments_modes, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new PaymentModesAdapter.ViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull PaymentModesAdapter.ViewHolder holder, int position) {

        holder.tv_name.setText(paymentModesArrayList.get(position).getName());

        if (paymentModesArrayList.get(position).getStatus().equalsIgnoreCase("2")){
            holder.tv_name.setBackground(context.getResources().getDrawable(R.drawable.background_blue_full));
            holder.tv_name.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.tv_name.setBackground(context.getResources().getDrawable(R.drawable.background_blue_line));
            holder.tv_name.setTextColor(context.getResources().getColor(R.color.black));
        }


        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (int i=0;i<paymentModesArrayList.size();i++){
                    if (paymentModesArrayList.get(i).getId().equalsIgnoreCase(paymentModesArrayList.get(position).getId())){
                        paymentModesArrayList.get(position).setStatus("2");
                    }else {
                        paymentModesArrayList.get(i).setStatus("1");
                    }
                }

                ((PaymentModesActivity)context).setdeleverymodechanged(paymentModesArrayList.get(position));

                notifyDataSetChanged();
            }
        });


    }

    public void setrefresh(ArrayList<PaymentModes> productlist){
        this.paymentModesArrayList=productlist;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return paymentModesArrayList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}

    public class ViewHolder extends RecyclerView.ViewHolder {

        
       private TextView tv_name;
        private LinearLayout layout_address;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name=itemView.findViewById(R.id.tv_name);
            layout_address=itemView.findViewById(R.id.layout_address);


        }
    }
}
