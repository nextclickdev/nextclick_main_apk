package com.apticks.nextclickuser.checkout.map;

import com.google.android.gms.maps.model.PolylineOptions;

public interface MapParser {
    public void onPolylineOptionsUpdated(PolylineOptions lineOptions);
}