package com.apticks.nextclickuser.checkout.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Activities.VENDORS.VendorsActivity;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.NetworkUtil;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.adapters.ProductsAdapter;
import com.apticks.nextclickuser.checkout.adapters.VendorAdapter;
import com.apticks.nextclickuser.checkout.models.CheckoutModel;
import com.apticks.nextclickuser.checkout.models.DeleveryfeeDetails;
import com.apticks.nextclickuser.checkout.models.DeliveryFeeObject;
import com.apticks.nextclickuser.checkout.models.Permissionweight;
import com.apticks.nextclickuser.checkout.models.Product;
import com.apticks.nextclickuser.checkout.models.ProductDetailsModel;
import com.apticks.nextclickuser.checkout.models.ProductImages;
import com.apticks.nextclickuser.checkout.models.Products;
import com.apticks.nextclickuser.checkout.models.SectionProducts;
import com.apticks.nextclickuser.checkout.models.SubOrderObject;
import com.apticks.nextclickuser.checkout.models.Tax;
import com.apticks.nextclickuser.checkout.models.VendorDetails;
import com.apticks.nextclickuser.checkout.models.WeightPermissable;
import com.apticks.nextclickuser.orderhistory.model.DirectionObject;
import com.apticks.nextclickuser.orderhistory.util.DirectionListener;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.apticks.nextclickuser.products.activities.ChangeAddressActivity;
import com.apticks.nextclickuser.checkout.adapters.DeleveryModesAdapter;
import com.apticks.nextclickuser.products.adapters.VarientsAdapter;
import com.apticks.nextclickuser.checkout.models.DeleveryModes;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.SaveAddress;
import com.apticks.nextclickuser.products.model.VarientModel;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.apticks.nextclickuser.Config.Config.CHECKOUTPRODUCTS;
import static com.apticks.nextclickuser.Config.Config.DELIVERYMODES;
import static com.apticks.nextclickuser.Constants.Constants.ADDRESS;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;
import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isnullboolean;

public class CheckOutPageActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView add_image_address,image_back;
    private LinearLayout layout_address;
    RelativeLayout layout_wallet,layout_promocode,layout_deliveryfee,layout_promo_code;
    private RadioGroup radio_delivery;
    private TextView change_address,vendor_name,vendor_address,items_count,tv_promo_code,tv_wallet_code,tv_sub_total,tv_discount,
            tv_deliveryfee,items_tax,tv_promo_apllied,tv_item_grand_total_total,customer_address,tv_proceedtopay,tv_subtotaltext;
    private Context mContext;
    private RecyclerView cartItemsRecycler,recycle_deleverymodes;
    private String shopname,shopaddress,deleverymodeid,shippingaddressid="";
    private VarientsAdapter varientsAdapter;
    private CustomDialog customDialog;
    private ArrayList<DeleveryModes> deleverymodeslist;
    private DeleveryModesAdapter deleveryModesAdapter;
    private ArrayList<Products> productslist;
    private VendorAdapter vendorAdapter;
    private PreferenceManager preferenceManager;
    private ArrayList<CartModel> cartlist;
    private Gson gson;
    private int discount,tax,deleveryfee=0,promocodeamount;
    private double totalbill=0.0,subtotal=0.0,discountprice=0.0,taxamount=0.0;
    DeleveryModes deleveryModes;
    ArrayList<CheckoutModel> checkoutlist;
    SaveAddress saveAddress;
    boolean can_i_proceed_to_pay;
    int whichactivity;
    VarientModel varientmodel;
    String quantity;
    ArrayList<ProductDetailsModel> productlistreorder;
    ImageView img_delivery;
    private boolean isAnimStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_page);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        init();

        whichactivity=getIntent().getIntExtra("whichactivity",0);
        gson=new Gson();

        if (whichactivity==1){
            varientmodel= (VarientModel) getIntent().getSerializableExtra("variantlist");
            quantity=getIntent().getStringExtra("quantity");
        }else if (whichactivity==2){
            productlistreorder=new ArrayList<>();
            String carListAsString = getIntent().getStringExtra("productlist");
            Type type = new TypeToken<List<ProductDetailsModel>>(){}.getType();
            productlistreorder = gson.fromJson(carListAsString, type);
        }
        else {
            cartlist=new ArrayList<>();
            String carListAsString = getIntent().getStringExtra("cartlist");
            Type type = new TypeToken<List<CartModel>>(){}.getType();
            cartlist = gson.fromJson(carListAsString, type);
        }

        if (NetworkUtil.isNetworkConnected(mContext)) {
            getDeleverymodes();
        } else {
            Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
        }

       /* radio_delivery.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.rb_selfpickup:
                        layout_address.setVisibility(View.GONE);
                        layout_deliveryfee.setVisibility(View.GONE);
                        break;
                    case R.id.rb_deliveryboy:
                        layout_address.setVisibility(View.VISIBLE);
                        layout_deliveryfee.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });*/
    }
    public void init(){
        mContext=CheckOutPageActivity.this;
        add_image_address=findViewById(R.id.add_image_address);
        TextView tv_header= findViewById(R.id.tv_header);
        tv_header.setText("Check Out");
        tv_header.setAllCaps(true);
        image_back=findViewById(R.id.img_back);
        FrameLayout cart_layout=findViewById(R.id.cart_layout);
        cart_layout.setVisibility(View.GONE);

        cart_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, VendorsActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Intent intent1=new Intent(CheckOutPageActivity.this,VendorsActivity.class);
                    intent1.putExtra("checkposition",1);
                    startActivity(intent1);
                   // startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(CheckOutPageActivity.this).toBundle());
                } else {
                    startActivity(intent);
                }
            }
        });
        LinearLayout layout_continue_shopping = findViewById(R.id.layout_continue_shopping);
        LinearLayout layout_checkout=findViewById(R.id.layout_checkout);
        layout_continue_shopping.setVisibility(View.VISIBLE);
        layout_checkout.setVisibility(View.GONE);

        radio_delivery=findViewById(R.id.radio_delivery);
        layout_address=findViewById(R.id.layout_address);
        change_address=findViewById(R.id.change_address);
        vendor_name=findViewById(R.id.vendor_name);
        vendor_address=findViewById(R.id.vendor_address);
        items_count=findViewById(R.id.items_count);
        cartItemsRecycler=findViewById(R.id.cartItemsRecycler);
        tv_promo_code=findViewById(R.id.tv_promo_code);
        layout_wallet=findViewById(R.id.layout_wallet);
        layout_promocode=findViewById(R.id.layout_promocode);
        tv_wallet_code=findViewById(R.id.tv_wallet_code);
        tv_sub_total=findViewById(R.id.tv_sub_total);
        tv_discount=findViewById(R.id.tv_discount);
        layout_deliveryfee=findViewById(R.id.layout_deliveryfee);
        tv_deliveryfee=findViewById(R.id.tv_deliveryfee);
        items_tax=findViewById(R.id.items_tax);
        layout_promo_code=findViewById(R.id.layout_promo_code);
        tv_promo_apllied=findViewById(R.id.tv_promo_apllied);
        tv_item_grand_total_total=findViewById(R.id.tv_item_grand_total_total);
        customer_address=findViewById(R.id.customer_address);
        tv_proceedtopay=findViewById(R.id.tv_proceedtopay);
        TextView tv_continue_shopping=findViewById(R.id.tv_continue_shopping);
        recycle_deleverymodes=findViewById(R.id.recycle_deleverymodes);
        tv_subtotaltext=findViewById(R.id.tv_subtotaltext);

        add_image_address.setOnClickListener(this::onClick);
        change_address.setOnClickListener(this::onClick);
        image_back.setOnClickListener(this::onClick);
        tv_proceedtopay.setOnClickListener(this::onClick);
        tv_continue_shopping.setOnClickListener(this::onClick);

        customDialog=new CustomDialog(mContext);
        shopname=getIntent().getStringExtra(reastaurantName);
        shopaddress=getIntent().getStringExtra(ADDRESS);

        preferenceManager=new PreferenceManager(mContext);

        vendor_name.setText(shopname);
        vendor_address.setText(shopaddress);
        img_delivery= findViewById(R.id.img_delivery);
        LinearLayout layout_delivery_fee= findViewById(R.id.layout_delivery_fee);


        layout_delivery_fee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(checkoutlist!=null && checkoutlist.size()==1)
                invokeDeliveryFeeInfoActivity(checkoutlist.get(0).getVendorDetails().getVendor_user_id(),true);
            }
        });

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        deleverymodeslist=new ArrayList<>();
        productslist=new ArrayList<>();
        checkoutlist=new ArrayList<>();
        deleveryModesAdapter=new DeleveryModesAdapter(mContext,deleverymodeslist,0);
        recycle_deleverymodes.setAdapter(deleveryModesAdapter);
        recycle_deleverymodes.setLayoutManager(linearLayoutManager);

        cartItemsRecycler.setLayoutManager(new GridLayoutManager(CheckOutPageActivity.this,1));
        varientsAdapter=new VarientsAdapter(mContext,1);

    /*    productsAdapter=new ProductsAdapter(mContext,productslist);
        cartItemsRecycler.setAdapter(productsAdapter);*/

        vendorAdapter=new VendorAdapter(this,mContext,checkoutlist);
        cartItemsRecycler.setAdapter(vendorAdapter);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.change_address:
                Intent intent=new Intent(mContext, ChangeAddressActivity.class);
                startActivityForResult(intent, 2);
                break;
            case R.id.img_back:
                finish();
                break;
            case R.id.tv_continue_shopping:
                Intent vendorItent = new Intent(mContext, VendorsActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Intent intent1=new Intent(CheckOutPageActivity.this,VendorsActivity.class);
                    intent1.putExtra("checkposition",1);
                    startActivity(intent1);
                    //  startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(CartAddActivity.this).toBundle());
                } else {
                    startActivity(vendorItent);
                }
                break;
            case R.id.tv_proceedtopay:
                if(checkoutlist==null ||  checkoutlist.size()==0)
                {
                    Toast.makeText(mContext, "No Products found", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (deleverymodeid.equalsIgnoreCase("2")){
                    try{
                        if(!saveAddress.getId().isEmpty()){
                            if (can_i_proceed_to_pay){
                                Gson gson=new Gson();
                                String checkoutstring = gson.toJson(checkoutlist);
                                Intent i = new Intent(mContext, PaymentModesActivity.class);
                                i.putExtra("checkoutlist",checkoutstring);
                                i.putExtra("shippingaddressid",shippingaddressid);
                                i.putExtra("deleverymodeid",deleverymodeid);
                                i.putExtra("totalamount",tv_item_grand_total_total.getText().toString());
                                startActivity(i);
                            }else {
                                String productnames="";
                                for (int i=0;i<checkoutlist.size();i++){
                                    for (int k=0;k<checkoutlist.get(i).getProductslist().size();k++){
                                        String check=checkoutlist.get(i).getProductslist().get(k).getIs_available();
                                        if (check.equalsIgnoreCase("false")){
                                            productnames=productnames+checkoutlist.get(i).getProductslist().get(k).getProduct().getName();                                       }
                                    }
                                }

                                //Toast.makeText(mContext, ""+productnames+" not available", Toast.LENGTH_SHORT).show();
                                showErrorDialog("The product stock is not available for these products : "+productnames);
                            }

                        }else {
                            Toast.makeText(mContext, "Please Add Delevery Address", Toast.LENGTH_SHORT).show();
                        }
                    }catch (NullPointerException e){
                        Toast.makeText(mContext, "Please Add Delevery Address", Toast.LENGTH_SHORT).show();
                    }

                }else {
                    Gson gson=new Gson();
                    String checkoutstring = gson.toJson(checkoutlist);
                    Intent i = new Intent(mContext, PaymentModesActivity.class);
                    i.putExtra("checkoutlist",checkoutstring);
                    i.putExtra("shippingaddressid",shippingaddressid);
                    i.putExtra("deleverymodeid",deleverymodeid);
                    i.putExtra("totalamount",tv_item_grand_total_total.getText().toString());
                    startActivity(i);
                }


                break;
        }
    }
    private void showErrorDialog(String errorMessage) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.layout_error_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        TextView tv_success= (TextView) dialog.findViewById(R.id.tv_success);
        tv_success.setText(errorMessage);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode==2) {
                saveAddress = (SaveAddress) data.getSerializableExtra("addressname");

                // String address = data.getStringExtra("addressname");
                //  String id = data.getStringExtra("id");
                System.out.println("aaaaaaaa  carListAsString " + saveAddress.getAddress());
                shippingaddressid = saveAddress.getId();
                customer_address.setText(saveAddress.getAddress());
                if (saveAddress.getAddress() != null && !saveAddress.getAddress().isEmpty())
                    change_address.setText("Change Address");
                getProductspassbody(saveAddress.getId(), deleverymodeid);

            }
        }catch (NullPointerException e){
            System.out.println("aaaaaaaa catch "+e.getMessage());
        }

        try {
            if (requestCode==3) {
                String carListAsString = data.getStringExtra("checkoutstring");
                Type type = new TypeToken<List<CartModel>>(){}.getType();
                cartlist = gson.fromJson(carListAsString, type);

                getProductspassbody(saveAddress.getId(), deleverymodeid);

            }
        }catch (NullPointerException e){
            System.out.println("aaaaaaaa catch "+e.getMessage());
        }


    }

    private void getDeleverymodes() {
        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DELIVERYMODES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + responsearray.toString());
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    DeleveryModes deleveryModes=new DeleveryModes();
                                    deleveryModes.setId(jsonObject1.getString("id"));
                                    deleveryModes.setName(jsonObject1.getString("name"));
                                    deleveryModes.setDesc(jsonObject1.getString("desc"));
                                    deleveryModes.setIs_having_delivery_fee(jsonObject1.getString("is_having_delivery_fee"));

                                    if (jsonObject1.getString("id").equalsIgnoreCase("2")){
                                        deleveryModes.setStatus("2");
                                        deleverymodeid=jsonObject1.getString("id");
                                        if (NetworkUtil.isNetworkConnected(mContext)) {
                                            getProductspassbody("",""+deleverymodeid);
                                        } else {
                                            Toast.makeText(CheckOutPageActivity.this, "No internet connection! ", Toast.LENGTH_SHORT).show();
                                        }
                                      //  layout_address.setVisibility(View.VISIBLE);
                                    }else {
                                        deleveryModes.setStatus("1");
                                    }

                                   deleverymodeslist.add(deleveryModes);
                                }

                                deleveryModesAdapter.setrefresh(deleverymodeslist);

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setdeleverymodechanged(DeleveryModes deleveryModes, boolean check) {
        this.deleveryModes=deleveryModes;
        this.deleverymodeid=deleveryModes.getId();
        if (check){
            Animation animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);
            layout_address.startAnimation(animSlideDown);
            layout_address.setVisibility(View.VISIBLE);
            try{
                shippingaddressid=saveAddress.getId();
            }catch (NullPointerException e){

            }
        }else {
            Animation animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_up);
            layout_address.startAnimation(animSlideDown);
            shippingaddressid="";
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    layout_address.setVisibility(View.GONE);
                }
            }, 500);
        }
        deleveryModesAdapter.notifyDataSetChanged();
        getProductspassbody(shippingaddressid,deleverymodeid);
    }

    public void getProductspassbody(String shipingaddress,String deleverymodeid){

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("shipping_address_id", shipingaddress);
        uploadMap.put("dalivery_mode_id", deleverymodeid);

        if(whichactivity==1){
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject1=new JSONObject();
            try {
                jsonObject1.put("item_id",varientmodel.getItem_id());
                jsonObject1.put("vendor_product_variant_id",varientmodel.getId());
                jsonObject1.put("qty",quantity);
                jsonObject1.put("vendor_user_id",varientmodel.getVendor_user_id());
                jsonArray.put(jsonObject1);
                JSONObject json = new JSONObject(uploadMap);
                try {
                    json.put("products",jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();

                    System.out.println("aaaaaaaaa catch 2  "+e.getMessage());
                }
                if (NetworkUtil.isNetworkConnected(mContext)) {
                    getProductDetails(json);
                } else {
                    Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
                }
            }catch (JSONException e) {
                e.printStackTrace();
            }


        }else if (whichactivity==2){
            JSONArray jsonArray = new JSONArray();

            for (int i=0; i < productlistreorder.size(); i++) {
                try {
                    JSONObject jsonObject1=new JSONObject();
                    jsonObject1.put("item_id",productlistreorder.get(i).getItem_id());
                    jsonObject1.put("vendor_product_variant_id",productlistreorder.get(i).getVendor_product_variant_id());
                    jsonObject1.put("qty",productlistreorder.get(i).getQty());
                   // jsonObject1.put("vendor_user_id",productlistreorder.get(i).getVendor_user_id());
                    jsonArray.put(jsonObject1);
                } catch (JSONException e) {
                    System.out.println("aaaaaaaaa catch 1  "+e.getMessage());
                    e.printStackTrace();
                }
            }
            JSONObject json = new JSONObject(uploadMap);
            try {
                json.put("products",jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();

                System.out.println("aaaaaaaaa catch 2  "+e.getMessage());
            }
            if (NetworkUtil.isNetworkConnected(mContext)) {
                getProductDetails(json);
            } else {
                Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
            }
        }  else{
            JSONArray jsonArray = new JSONArray();

            for (int i=0; i < cartlist.size(); i++) {
                try {
                    JSONObject jsonObject1=new JSONObject();
                    jsonObject1.put("item_id",cartlist.get(i).getItem_id());
                    jsonObject1.put("vendor_product_variant_id",cartlist.get(i).getVendor_product_varinat_id());
                    jsonObject1.put("qty",cartlist.get(i).getQty());
                    jsonObject1.put("vendor_user_id",cartlist.get(i).getVendor_user_id());
                    jsonArray.put(jsonObject1);
                } catch (JSONException e) {
                    System.out.println("aaaaaaaaa catch 1  "+e.getMessage());
                    e.printStackTrace();
                }
            }
            JSONObject json = new JSONObject(uploadMap);
            try {
                json.put("products",jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();

                System.out.println("aaaaaaaaa catch 2  "+e.getMessage());
            }
            if (NetworkUtil.isNetworkConnected(mContext)) {
                getProductDetails(json);
            } else {
                Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void getProductDetails(JSONObject json) {
        cleardata();
        final String dataStr = json.toString();
        System.out.println("aaaaaaaaaa data "+dataStr);
        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CHECKOUTPRODUCTS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status && http_code==200) {
                                JSONObject dataobject = jsonObject.getJSONObject("data");
                                can_i_proceed_to_pay = dataobject.getBoolean("can_i_proceed_to_pay");

                                Object aObj = dataobject.get("products_group_by_vendor");
                                productslist = new ArrayList<>();
                                if (aObj instanceof JSONArray) {
                                    JSONArray responsearray = (JSONArray) aObj;
                                    for (int i = 0; i < responsearray.length(); i++) {
                                        JSONObject jsonObject1 = responsearray.getJSONObject(i);

                                        CheckoutModel checkoutModel = new CheckoutModel();

                                        try {
                                            VendorDetails vendorDetails = new VendorDetails();
                                            JSONObject vendorobj = jsonObject1.getJSONObject("vendor_details");

                                            vendorDetails.setId(vendorobj.getString("id"));
                                            vendorDetails.setName(vendorobj.getString("name"));
                                            vendorDetails.setUnique_id(vendorobj.getString("unique_id"));
                                            vendorDetails.setAddress(vendorobj.getString("address"));
                                            vendorDetails.setLandmark(vendorobj.getString("landmark"));
                                            vendorDetails.setPincode(vendorobj.getString("pincode"));
                                            vendorDetails.setAvailability(vendorobj.getString("availability"));
                                            vendorDetails.setVendor_user_id(vendorobj.getString("vendor_user_id"));

                                            checkoutModel.setVendorDetails(vendorDetails);
                                        } catch (JSONException e) {
                                            System.out.println("aaaaaa catch vendor detail not found");
                                        }


                                        DeleveryfeeDetails deleveryfeeDetails = new DeleveryfeeDetails();
                                        int _deleveryfee = 0;
                                        if (jsonObject1.has("delivery_fee")) {
                                            layout_deliveryfee.setVisibility(View.VISIBLE);
                                            Object obj =jsonObject1.get("delivery_fee");
                                            if(obj instanceof  JSONObject) {
                                                try {
                                                    JSONObject deleveryobject = jsonObject1.getJSONObject("delivery_fee");
                                                    deleveryfeeDetails.setId(deleveryobject.getString("id"));
                                                    deleveryfeeDetails.setFlat_rate(deleveryobject.getString("flat_rate"));
                                                    deleveryfeeDetails.setPer_km(deleveryobject.getString("per_km"));
                                                    deleveryfeeDetails.setVehicle_type_id(deleveryobject.getString("vehicle_type_id"));
                                                    _deleveryfee = Integer.parseInt(deleveryobject.getString("flat_rate"));
                                                } catch (Exception ex) {
                                                    try {
                                                        _deleveryfee = Integer.parseInt(jsonObject1.getString("delivery_fee"));
                                                    } catch (Exception e) {
                                                        _deleveryfee = 0;
                                                        layout_deliveryfee.setVisibility(View.GONE);
                                                    } finally {
                                                        deleveryfeeDetails.setId("");
                                                        deleveryfeeDetails.setFlat_rate("" + _deleveryfee);
                                                        deleveryfeeDetails.setPer_km("");
                                                        deleveryfeeDetails.setVehicle_type_id("");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                try {
                                                    _deleveryfee = Integer.parseInt(jsonObject1.getString("delivery_fee"));
                                                    deleveryfeeDetails.setFlat_rate(jsonObject1.getString("delivery_fee"));
                                                }catch (Exception ee) {
                                                    _deleveryfee = 0;
                                                    layout_deliveryfee.setVisibility(View.GONE);
                                                }finally {
                                                    deleveryfeeDetails.setId("");
                                                    deleveryfeeDetails.setFlat_rate("" + _deleveryfee);
                                                    deleveryfeeDetails.setPer_km("");
                                                    deleveryfeeDetails.setVehicle_type_id("");
                                                }
                                            }
                                            checkoutModel.setDelivery_fee(deleveryfeeDetails);
                                            deleveryfee += _deleveryfee;
                                            tv_deliveryfee.setText("" + deleveryfee);
                                            if (deleveryfee > 0) {
                                                startAnimation();
                                            } else {
                                                clearAnimation();
                                                layout_deliveryfee.setVisibility(View.GONE);
                                            }
                                        } else {
                                            clearAnimation();
                                            layout_deliveryfee.setVisibility(View.GONE);
                                        }
                                        checkoutModel.setDelivery_fee(deleveryfeeDetails);

                                        JSONObject permitsobj = jsonObject1.getJSONObject("is_this_permissible_wight");

                                        WeightPermissable weightPermissable = new WeightPermissable();
                                        weightPermissable.setStatus(permitsobj.getBoolean("status"));
                                        JSONArray weightsarray = permitsobj.getJSONArray("permissible_weight");
                                        ArrayList<Permissionweight> listweights = new ArrayList<>();
                                        for (int m = 0; m < weightsarray.length(); m++) {
                                            Permissionweight permissionweight = new Permissionweight();
                                            JSONObject permssionobj = weightsarray.getJSONObject(m);
                                            permissionweight.setMin_capacity(permssionobj.getString("min_capacity"));
                                            permissionweight.setAs_max_capacity(permssionobj.getString("as_max_capacity"));
                                            listweights.add(permissionweight);
                                        }
                                        weightPermissable.setWeightlist(listweights);

                                        checkoutModel.setWeightPermissable(weightPermissable);

                                        JSONArray productsarray = jsonObject1.getJSONArray("products");
                                        productslist = new ArrayList<>();
                                        for (int j = 0; j < productsarray.length(); j++) {
                                            JSONObject productsobject = productsarray.getJSONObject(j);
                                            Products products = new Products();
                                            products.setId(productsobject.getString("id"));
                                            products.setSku(productsobject.getString("sku"));
                                            products.setPrice(productsobject.getString("price"));
                                            products.setStock(productsobject.getString("stock"));
                                            products.setDiscount(productsobject.getString("discount"));
                                            products.setStatus(productsobject.getString("status"));
                                            products.setItem_id(productsobject.getString("item_id"));
                                            products.setSection_item_id(productsobject.getString("section_item_id"));
                                            products.setQty(productsobject.getString("qty"));
                                            products.setIs_available(productsobject.getString("is_available"));

                                            double productprice = Double.parseDouble(productsobject.getString("price"));
                                            int quantity = Integer.parseInt(productsobject.getString("qty"));
                                            subtotal = subtotal + (productprice * quantity);

                                            discount = discount + Integer.parseInt(productsobject.getString("discount"));

                                            double ddiscount = Double.parseDouble(productsobject.getString("discount"));
                                            double price = Integer.parseInt(productsobject.getString("qty")) *
                                                    Double.parseDouble(productsobject.getString("price"));

                                            double totalprice = price - ((price / 100.0f) * ddiscount);
                                            double tprice = price - totalprice;

                                            discountprice = discountprice + tprice;

                                            try {
                                                JSONObject taxobj = productsobject.getJSONObject("tax");
                                                Tax tax = new Tax();
                                                tax.setId(taxobj.getString("id"));
                                                tax.setTax(taxobj.getString("tax"));
                                                tax.setRate(taxobj.getString("rate"));
                                                products.setTax(tax);

                                                double taxrate = Double.parseDouble(taxobj.getString("rate"));
                                                taxamount = taxamount + ((price / 100.0f) * taxrate);
                                            } catch (JSONException e) {
                                                Tax tax = new Tax();
                                                tax.setId("0");
                                                tax.setTax("0");
                                                tax.setRate("0");
                                                products.setTax(tax);
                                                System.out.println("aaaaaaaaa tax not found");
                                            }


                                            ArrayList<ProductImages> imagelist = new ArrayList<>();
                                            try {
                                                JSONArray imagesarray = productsobject.getJSONArray("item_images");
                                                for (int k = 0; k < imagesarray.length(); k++) {
                                                    JSONObject imageobject = imagesarray.getJSONObject(k);
                                                    ProductImages productImages = new ProductImages();
                                                    productImages.setItem_id(imageobject.getString("item_id"));
                                                    productImages.setId(imageobject.getString("id"));
                                                    productImages.setExt(imageobject.getString("ext"));
                                                    productImages.setImage(imageobject.getString("image"));
                                                    imagelist.add(productImages);
                                                }
                                            } catch (JSONException e) {
                                                ProductImages productImages = new ProductImages();
                                                productImages.setItem_id("");
                                                productImages.setId("");
                                                productImages.setExt("");
                                                productImages.setImage("");
                                                imagelist.add(productImages);
                                            }

                                            products.setImagelist(imagelist);

                                            JSONObject itemobject = productsobject.getJSONObject("item");
                                            Product product = new Product();
                                            product.setId(itemobject.getString("id"));
                                            product.setProduct_code(itemobject.getString("product_code"));
                                            product.setName(itemobject.getString("name"));
                                            product.setDesc(itemobject.getString("desc"));
                                            product.setStatus(itemobject.getString("status"));
                                            products.setProduct(product);


                                            JSONObject secobject = productsobject.getJSONObject("section_item");

                                            SectionProducts sectionProducts = new SectionProducts();
                                            sectionProducts.setId(secobject.getString("id"));
                                            sectionProducts.setName(secobject.getString("name"));
                                            sectionProducts.setDesc(secobject.getString("desc"));
                                            sectionProducts.setWeight(secobject.getString("weight"));
                                            sectionProducts.setStatus(secobject.getString("status"));

                                            products.setSectionProducts(sectionProducts);

                                            productslist.add(products);

                                        }

                                        checkoutModel.setProductslist(productslist);
                                        //tv_subtotaltext.setText("SubTotal ("+productslist.size()+" Items )");

                                        //sub orders
                                        try {
                                            JSONArray subOrdersArray = jsonObject1.getJSONArray("sub_orders");
                                            ArrayList<SubOrderObject> subOrdersList = new ArrayList<>();
                                            for (int j = 0; j < subOrdersArray.length(); j++) {
                                                JSONObject subOrder = subOrdersArray.getJSONObject(j);
                                                SubOrderObject subOrderObject = new SubOrderObject();

                                                DeliveryFeeObject deliveryFeeObject = new DeliveryFeeObject();
                                                try {

                                                    if (subOrder.has("delivery_fee")) {
                                                        Object obj =subOrder.get("delivery_fee");
                                                        if(obj instanceof  JSONObject) {
                                                            JSONObject delivery_fee = subOrder.getJSONObject("delivery_fee");
                                                            deliveryFeeObject.setId(delivery_fee.getString("id"));
                                                            deliveryFeeObject.setFlatRate(delivery_fee.getString("flat_rate"));
                                                            deliveryFeeObject.setKMInfo(delivery_fee.getString("per_km"));
                                                            deliveryFeeObject.setVehicleTypeId(delivery_fee.getString("vehicle_type_id"));
                                                        }
                                                        else
                                                        {
                                                            deliveryFeeObject.setFlatRate(subOrder.getString("delivery_fee"));
                                                        }
                                                        subOrderObject.setDeliveryFeeObject(deliveryFeeObject);
                                                    }
                                                } catch (Exception e) {
                                                }

                                                ArrayList<Products> _productList = getSubOrderProducts(subOrder);
                                                subOrderObject.setProductslist(_productList);
                                                subOrdersList.add(subOrderObject);
                                            }
                                            checkoutModel.setSubOrdersList(subOrdersList);
                                        } catch (Exception ex) {
                                        }
                                        //end

                                        checkoutlist.add(checkoutModel);
                                    }

                                }
                                paymentcalculation();
                                // productsAdapter.setrefresh(productslist);
                                eachevendorsummary();
                                /*if(checkoutlist!=null && checkoutlist.size()>1)
                                    img_delivery.setVisibility(View.GONE);
                                else {
                                    img_delivery.setVisibility(View.VISIBLE);
                                }*/
                                vendorAdapter.setrefresh(checkoutlist);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void startAnimation() {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(100); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        img_delivery.startAnimation(anim);
        isAnimStarted= true;
    }
    private void clearAnimation() {
        if (img_delivery != null && isAnimStarted)
            img_delivery.clearAnimation();
        isAnimStarted= false;
    }

    public void cleardata(){
        productslist.clear();
        checkoutlist.clear();
        discount=0;tax=0;deleveryfee=0;promocodeamount=0;
        totalbill=0.0;subtotal=0.0;discountprice=0.0;taxamount=0.0;
    }
    public void paymentcalculation(){

        tv_subtotaltext.setText("SubTotal ("+productslist.size()+" Items )");
        tv_sub_total.setText(""+subtotal);

        tv_discount.setText(""+discount+" % off"+"(-"+String.format("%.2f",discountprice)+" Saved)");

        items_tax.setText(String.format("%.2f",taxamount));
        //items_tax.setText("+"+taxamount);

        Double totalbill=subtotal-discountprice+deleveryfee+taxamount;

        tv_item_grand_total_total.setText(String.format("%.2f",totalbill));
    //    tv_item_grand_total_total.setText(""+totalbill);
    }

    public void setDelete(Products cartModel) {
        for (int i=0;i<cartlist.size();i++){
            if (cartlist.get(i).getItem_id().equalsIgnoreCase(cartModel.getItem_id())){
                cartlist.remove(i);
            }
        }
        System.out.println("aaaaaaaaaa cartlist  "+cartlist.size());
        getProductspassbody(shippingaddressid,deleverymodeid);
    }

    public void eachevendorsummary(){
        for (int i=0;i<checkoutlist.size();i++){

            double grandtotal=0.0,totalsub=0.0,taxamount1=0.0,discountamt=0.0,tax=0.0;
            for (int k=0;k<checkoutlist.get(i).getProductslist().size();k++){
                double productprice=Double.parseDouble(checkoutlist.get(i).getProductslist().get(k).getPrice())
                        *(Integer.parseInt(checkoutlist.get(i).getProductslist().get(k).getQty()));

                totalsub=totalsub+productprice;

                if (!checkoutlist.get(i).getProductslist().get(k).getDiscount().equalsIgnoreCase("0")){
                   double discount=Double.parseDouble(checkoutlist.get(i).getProductslist().get(k).getDiscount());
                    discountamt=discountamt+((productprice / 100.0f) * discount);

                  //  System.out.println("aaaaaaaaa discount "+discount+"  "+discountamt+"  "+productprice);
                }

                try{
                    tax=Double.parseDouble(checkoutlist.get(i).getProductslist().get(k).getTax().getRate());
                    taxamount1=taxamount1+((productprice / 100.0f) * tax);
                    //  System.out.println("aaaaaaaaa tax "+tax+"  "+taxamount+"  "+productprice);
                }catch (NullPointerException e){

                }

                grandtotal=totalsub+taxamount1-discountamt;
            }

            try{
                double deleveryfee=Double.parseDouble(checkoutlist.get(i).getDelivery_fee().getFlat_rate());
                grandtotal=grandtotal+deleveryfee;
            }catch (NullPointerException e){

            }


            checkoutlist.get(i).setTax(taxamount1);
            checkoutlist.get(i).setDiscount(discountamt);
            checkoutlist.get(i).setGrandtotal(grandtotal);
            checkoutlist.get(i).setSubtotal(totalsub);

        }
    }

    public void invokeDeliveryFeeInfoActivity(String  vendor_user_id,boolean showAllVendorsList) {



        if(saveAddress!=null) {

            Intent i = new Intent(mContext, DeliveryFeeInfoActivity.class);
            Gson gson=new Gson();
            String checkoutstring = gson.toJson(cartlist);
            i.putExtra("cartlist",checkoutstring);
            i.putExtra("shipping_address_id",shippingaddressid);
            i.putExtra("dalivery_mode_id",deleverymodeid);
            i.putExtra("vendor_user_id",vendor_user_id);
            i.putExtra("showAllVendorsList",showAllVendorsList);
           // startActivity(i);
            startActivityForResult(i, 3);
            /*
            for (CartModel cartModel : cartlist) {
                if (cartModel.getVendor_user_id().equals(vendor_user_id)) {
                    i.putExtra("item_id", cartModel.getItem_id());
                    i.putExtra("vendor_product_variant_id", cartModel.getVendor_product_varinat_id());
                    i.putExtra("qty", cartModel.getQty());
                    i.putExtra("vendor_user_id", cartModel.getVendor_user_id());
                    i.putExtra("vendor_product_variant_id", cartModel.getVendor_product_varinat_id());
                    i.putExtra("shipping_address_id", saveAddress.getId());
                    i.putExtra("dalivery_mode_id", deleverymodeid);
                    startActivity(i);
                    break;
                }
            }*/
        }
    }



    private ArrayList<Products> getSubOrderProducts(JSONObject jsonObject1) {

        ArrayList<Products> _productList = new ArrayList<>();
        JSONArray productsarray = null;
        try {
            //weight : 1000
            //vendor_product_variant_id : 179
            //qty : 1
            productsarray = jsonObject1.getJSONArray("products");
            // JSONObject  productJsonObject = productJson.getJSONObject(0);//it should not be array
            //  productsarray = productJsonObject.getJSONArray("product_details");

            for (int j = 0; j < productsarray.length(); j++) {
                JSONObject productsobject = productsarray.getJSONObject(j);

                Products products = new Products();

                //new fields
                products.setweight(productsobject.getString("weight"));
                products.setSubOrderQty(productsobject.getString("qty"));
                products.setVendor_product_variant_id(productsobject.getString("vendor_product_variant_id"));

               // JSONObject productsobject = productJson.getJSONObject("product_details");

                products.setId(productsobject.getString("vendor_product_variant_id"));
                products.setPrice(productsobject.getString("price"));
                products.setDiscount(productsobject.getString("discount"));
                products.setItem_id(productsobject.getString("item_id"));

                Product product = new Product();
                product.setName(productsobject.getString("name"));
                products.setProduct(product);

                SectionProducts sectionProducts = new SectionProducts();
                sectionProducts.setName(productsobject.getString("section_item_name"));

                try {
                    JSONObject taxobj = productsobject.getJSONObject("tax");
                    Tax tax = new Tax();
                    tax.setId(taxobj.getString("id"));
                    tax.setTax(taxobj.getString("tax"));
                    tax.setRate(taxobj.getString("rate"));
                    products.setTax(tax);
                } catch (JSONException e) {
                    Tax tax = new Tax();
                    tax.setId("0");
                    tax.setTax("0");
                    tax.setRate("0");
                    products.setTax(tax);
                    System.out.println("aaaaaaaaa tax not found");
                }


                products.setSectionProducts(sectionProducts);

                /*JSONObject productsobject = productJson.getJSONObject("product_details");
                products.setId(productsobject.getString("id"));
                products.setSku(productsobject.getString("sku"));
                products.setPrice(productsobject.getString("price"));
                products.setStock(productsobject.getString("stock"));
                products.setDiscount(productsobject.getString("discount"));
                products.setStatus(productsobject.getString("status"));
                products.setItem_id(productsobject.getString("item_id"));
                products.setSection_item_id(productsobject.getString("section_item_id"));
                products.setQty(productsobject.getString("qty"));
                products.setIs_available(productsobject.getString("is_available"));

                try {
                    JSONObject taxobj = productsobject.getJSONObject("tax");
                    Tax tax = new Tax();
                    tax.setId(taxobj.getString("id"));
                    tax.setTax(taxobj.getString("tax"));
                    tax.setRate(taxobj.getString("rate"));
                    products.setTax(tax);
                } catch (JSONException e) {
                    Tax tax = new Tax();
                    tax.setId("0");
                    tax.setTax("0");
                    tax.setRate("0");
                    products.setTax(tax);
                    System.out.println("aaaaaaaaa tax not found");
                }
                JSONObject itemobject = productsobject.getJSONObject("item");
                Product product = new Product();
                product.setId(itemobject.getString("id"));
                product.setProduct_code(itemobject.getString("product_code"));
                product.setName(itemobject.getString("name"));
                product.setDesc(itemobject.getString("desc"));
                product.setStatus(itemobject.getString("status"));
                products.setProduct(product);


                JSONObject secobject = productsobject.getJSONObject("section_item");
                SectionProducts sectionProducts = new SectionProducts();
                sectionProducts.setId(secobject.getString("id"));
                sectionProducts.setName(secobject.getString("name"));
                sectionProducts.setDesc(secobject.getString("desc"));
                sectionProducts.setWeight(secobject.getString("weight"));
                sectionProducts.setStatus(secobject.getString("status"));
                products.setSectionProducts(sectionProducts);*/

                _productList.add(products);

            }
        } catch (JSONException e) {
            e.printStackTrace();
            System.out.println("Exception in sub orders parser " + e.getMessage());
        }

        return _productList;
    }
    

}