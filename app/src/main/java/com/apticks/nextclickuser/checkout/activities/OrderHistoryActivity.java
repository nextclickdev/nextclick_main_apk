package com.apticks.nextclickuser.checkout.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.adapters.OrdersHistoryAdapter;
import com.apticks.nextclickuser.checkout.models.OrderHistoryModel;
import com.apticks.nextclickuser.checkout.models.OrderStatus;
import com.apticks.nextclickuser.checkout.models.PaymentMethod;
import com.apticks.nextclickuser.checkout.models.PaymentModel;
import com.apticks.nextclickuser.dialogs.Filterorder;
import com.apticks.nextclickuser.orderhistory.model.DeliveryJobObject;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.ORDERHISTORY;
import static com.apticks.nextclickuser.Config.Config.ORDERSEND;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class OrderHistoryActivity extends AppCompatActivity {

    private RecyclerView recycle_history;
    private RelativeLayout layout_nodata;
    private CustomDialog customDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    private ArrayList<OrderHistoryModel> orderslist;
    OrdersHistoryAdapter ordersHistoryAdapter;
    private ImageView img_back;
    private TextView tv_sort;
    FloatingActionButton add_filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        init();
    }
    public void init(){
        recycle_history=findViewById(R.id.recycle_history);
        img_back=findViewById(R.id.img_back);
        tv_sort=findViewById(R.id.tv_sort);
        layout_nodata=findViewById(R.id.layout_nodata);
        add_filter=findViewById(R.id.add_filter);
        mContext=OrderHistoryActivity.this;
        customDialog=new CustomDialog(mContext);
        preferenceManager=new PreferenceManager(mContext);
        orderslist=new ArrayList<>();
        recycle_history.setLayoutManager(new GridLayoutManager(OrderHistoryActivity.this,1));
        ordersHistoryAdapter=new OrdersHistoryAdapter(OrderHistoryActivity.this,orderslist);
        recycle_history.setAdapter(ordersHistoryAdapter);


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(OrderHistoryActivity.this, MainCategoriesActivity.class);
                startActivity(i);
                finish();
            }
        });
        tv_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Filterorder filterorder=new Filterorder(mContext);
                filterorder.showDialog();
            }
        });  add_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Filterorder filterorder=new Filterorder(mContext);
                filterorder.showDialog();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        getOrders("0",""+0,""+0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(OrderHistoryActivity.this, MainCategoriesActivity.class);
        startActivity(i);
        finish();
    }

    public void getOrders(String lastdays, String lastyears, String status){
        orderslist.clear();
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("last_days", lastdays);
        uploadMap.put("last_years", lastyears);
        uploadMap.put("status", "0");

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+json.toString());
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                               try {
                                   JSONArray jsonArray1=jsonObject.getJSONArray("data");
                                   for (int i=0;i<jsonArray1.length();i++){
                                       JSONObject orderobject=jsonArray1.getJSONObject(i);
                                       OrderHistoryModel orderHistoryModel=new OrderHistoryModel();
                                       orderHistoryModel.setId(orderobject.getString("id"));
                                       orderHistoryModel.setTrack_id(orderobject.getString("track_id"));
                                       orderHistoryModel.setOrder_delivery_otp(orderobject.getString("order_delivery_otp"));
                                       orderHistoryModel.setPreparation_time(orderobject.getString("preparation_time"));
                                       orderHistoryModel.setPayment_id(orderobject.getString("payment_id"));

                                       orderHistoryModel.setVendor_user_id(orderobject.getString("vendor_user_id"));

                                       try {
                                           if(orderobject.has("delivery_job_status")) {
                                               orderHistoryModel.setVendorStatus(orderobject.getString("delivery_job_status"));
                                           }
                                       }
                                       catch (Exception ex)
                                       {

                                       }

                                       if(orderobject.has("vendor")) {
                                           JSONObject vendorObject = orderobject.getJSONObject("vendor");
                                           orderHistoryModel.setShopName(vendorObject.getString("name"));
                                       }
                                       orderHistoryModel.setDeliveryFee(orderobject.getString("delivery_fee"));

                                       orderHistoryModel.setCreated_at(orderobject.getString("created_at"));
                                       orderHistoryModel.setUpdated_at(orderobject.getString("updated_at"));
                                       orderHistoryModel.setOrder_status_id(orderobject.getString("order_status_id"));
                                       orderHistoryModel.setOrderAmount(orderobject.getString("total"));

                                       PaymentModel paymentModel=new PaymentModel();
                                       JSONObject paymentobj=orderobject.getJSONObject("payment");
                                       paymentModel.setId(paymentobj.getString("id"));
                                       paymentModel.setTxn_id(paymentobj.getString("txn_id"));
                                       paymentModel.setAmount(paymentobj.getString("amount"));
                                       paymentModel.setCreated_at(paymentobj.getString("created_at"));
                                       paymentModel.setMessage(paymentobj.getString("message"));
                                       paymentModel.setStatus(paymentobj.getString("status"));
                                       paymentModel.setPayment_method_id(paymentobj.getString("payment_method_id"));

                                       PaymentMethod paymentMethod=new PaymentMethod();
                                       JSONObject paymentmethodobj=paymentobj.getJSONObject("payment_method");
                                       paymentMethod.setId(paymentmethodobj.getString("id"));
                                       paymentMethod.setName(paymentmethodobj.getString("name"));
                                       paymentMethod.setDescription(paymentmethodobj.getString("description"));

                                       paymentModel.setPaymentMethod(paymentMethod);
                                       orderHistoryModel.setPaymentModel(paymentModel);

                                       JSONObject oederstatusobj=orderobject.getJSONObject("order_status");
                                       OrderStatus orderStatus=new OrderStatus();
                                       orderStatus.setId(oederstatusobj.getString("id"));
                                       orderStatus.setDelivery_mode_id(oederstatusobj.getString("delivery_mode_id"));
                                       orderStatus.setStatus(oederstatusobj.getString("status"));
                                       orderStatus.setSerial_number(oederstatusobj.getString("serial_number"));

                                       orderHistoryModel.setOrderStatus(orderStatus);
                                       orderslist.add(orderHistoryModel);
                                   }
                               }
                               catch (Exception ex){}
                                if (orderslist.size()==0){
                                    layout_nodata.setVisibility(View.VISIBLE);
                                    recycle_history.setVisibility(View.GONE);
                                }else {
                                    layout_nodata.setVisibility(View.GONE);
                                    recycle_history.setVisibility(View.VISIBLE);
                                    ordersHistoryAdapter.setrefresh(orderslist);
                                }

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}