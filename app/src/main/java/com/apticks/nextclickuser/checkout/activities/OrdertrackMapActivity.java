package com.apticks.nextclickuser.checkout.activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.Utility;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.adapters.OrderItemsAdapter;
import com.apticks.nextclickuser.checkout.map.MapRouteNavigationHelper;
import com.apticks.nextclickuser.checkout.models.ItemModel;
import com.apticks.nextclickuser.checkout.models.ProductDetailsModel;
import com.apticks.nextclickuser.checkout.models.ProductVarientModel;
import com.apticks.nextclickuser.checkout.models.SectionItemModel;
import com.apticks.nextclickuser.checkout.models.VendorDetails;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.api.GoogleAPIException;
import com.google.api.translate.Language;
import com.google.api.translate.Translate;
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions;
import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslateLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslator;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslatorOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.apticks.nextclickuser.Config.Config.ORDERDETAILS;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class OrdertrackMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String orderId;
    Polyline mPolyline;
    private TextView tv_shopname,tv_shopaddress;
    private LinearLayout layout_deleveryboy;
    MapRouteNavigationHelper mapRouteNavigationHelper;
    VendorDetails vendorDetail;
    LatLng originPoint;
    Marker prevMarker =null;
    private CustomDialog mCustomDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    private Double userlatitude,userlongitude,vendorlatitude,vendorlogitude;
    ArrayList<ProductDetailsModel> productlist;
    FirebaseTranslator englishGermanTranslator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordertrack_map);
        init();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        productlist=new ArrayList<>();


        getOrders();
    }

    public void init(){
        mContext=OrdertrackMapActivity.this;
        orderId = getIntent().getStringExtra("order_id");
        preferenceManager=new PreferenceManager(mContext);
        mCustomDialog=new CustomDialog(mContext);
        tv_shopname=findViewById(R.id.tv_shopname);
        tv_shopaddress=findViewById(R.id.tv_shopaddress);
        layout_deleveryboy=findViewById(R.id.layout_deleveryboy);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
     //   addMarkerObject(originPoint,vendorDetail.getName(),R.drawable.nav_home);

    }
    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);
        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void drawRoute(Location location) {
        LatLng myLatlng = new LatLng(location.getLatitude(), location.getLongitude());
        if (prevMarker != null)
            prevMarker.setVisible(false);
      //  prevMarker = addMarkerObject(myLatlng, "My Current Location",R.drawable.motor_pin);
        if (mapRouteNavigationHelper == null)
         //   mapRouteNavigationHelper = new MapRouteNavigationHelper(this, this);
        mapRouteNavigationHelper.drawRoute(myLatlng,originPoint);
    }


    private Marker addMarkerObject(LatLng latLng, String Address, int id) {

        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(Address)
                .icon(BitmapFromVector(getApplicationContext(), id)));


        CameraPosition position = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15.0f)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
       /*
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
        }*/
        return marker;
    }

    public void getOrders(){
        productlist.clear();
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("order_id", orderId);

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+json.toString());
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERDETAILS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONObject dataobj=jsonObject.getJSONObject("data");

                                // tv_time.setText(dataobj.getString("created_at"));
                              /*  tv_trackid.setText(dataobj.getString("track_id"));
                                tv_sub_total.setText(dataobj.getString("track_id"));
                                tv_item_grand_total_total.setText(dataobj.getString("total"));

                                if (dataobj.getString("delivery_mode_id").equalsIgnoreCase("1")){
                                    layout_delevery.setVisibility(View.GONE);
                                    layout_delevery_one.setVisibility(View.GONE);
                                }else {
                                    layout_delevery.setVisibility(View.GONE);
                                    layout_delevery_one.setVisibility(View.GONE);
                                }*/

                                try{
                                    JSONObject shippingobj=dataobj.getJSONObject("shipping_address");
                                    JSONObject locationobj=shippingobj.getJSONObject("location");
                                    userlatitude =Double.parseDouble(locationobj.getString("latitude"));
                                    userlongitude = Double.parseDouble(locationobj.getString("longitude"));
                                    String address=locationobj.getString("address");
                                    String name=shippingobj.getString("name");
                                    LatLng latLng=new LatLng(userlatitude,userlongitude);
                                    addMarkerObject(latLng,name,R.drawable.nav_home);

                                 //   tv_customername.setText(shippingobj.getString("name"));
                                 //   tv_customerphone.setText(shippingobj.getString("phone"));
                                }catch (JSONException e1){

                                }

                                try{
                                    JSONObject shippingobj=dataobj.getJSONObject("vendor");
                                    JSONObject locationobj=shippingobj.getJSONObject("location");
                                    vendorlatitude =Double.parseDouble(locationobj.getString("latitude"));
                                    vendorlogitude= Double.parseDouble(locationobj.getString("longitude"));
                                    String address=locationobj.getString("address");
                                    String name=shippingobj.getString("name");
                                    LatLng latLng=new LatLng(vendorlatitude,vendorlogitude);
                                    addMarkerObject(latLng,name,R.drawable.ic_vendor);
                                    tv_shopname.setText(""+name);
                                    tv_shopaddress.setText(address);
                                    //   tv_customername.setText(shippingobj.getString("name"));
                                    //   tv_customerphone.setText(shippingobj.getString("phone"));
                                }catch (JSONException e1){

                                }

                                try{
                                    JSONObject deleveryobj=dataobj.getJSONObject("delevery_partner");
                                    layout_deleveryboy.setVisibility(View.VISIBLE);
                                }catch (JSONException e){
                                    layout_deleveryboy.setVisibility(View.GONE);
                                }

                                try{
                                    JSONObject deleveryong=dataobj.getJSONObject("delivery_mode");
                                  //  tv_deleverymode.setText(""+deleveryong.getString("name"));
                                }catch (JSONException e2){

                                }

                                try{
                                    JSONObject customerobj=dataobj.getJSONObject("customer");

                                }catch (JSONException e2){

                                }
                                try{
                                    JSONObject orderostatusobj=dataobj.getJSONObject("order_status");

                                  //  tv_status.setText(orderostatusobj.getString("status"));
                                }catch (JSONException e2){

                                }

                                try{
                                    JSONObject paymentobj=dataobj.getJSONObject("payment");
                                 //   payment_type.setText(paymentobj.getString("txn_id"));
                                }catch (JSONException e2){

                                }

                                JSONArray productarray=dataobj.getJSONArray("ecom_order_details");

                                for (int k=0;k<productarray.length();k++){
                                    JSONObject productonj=productarray.getJSONObject(k);
                                    ProductDetailsModel productDetailsModel=new ProductDetailsModel();

                                    productDetailsModel.setId(productonj.getString("id"));
                                    productDetailsModel.setEcom_order_id(productonj.getString("ecom_order_id"));
                                    productDetailsModel.setId(productonj.getString("item_id"));
                                    productDetailsModel.setVendor_product_variant_id(productonj.getString("vendor_product_variant_id"));
                                    productDetailsModel.setQty(productonj.getString("qty"));
                                    productDetailsModel.setPrice(productonj.getString("price"));
                                    productDetailsModel.setRate_of_discount(productonj.getString("rate_of_discount"));
                                    productDetailsModel.setSub_total(productonj.getString("sub_total"));
                                    productDetailsModel.setDiscount(productonj.getString("discount"));
                                    productDetailsModel.setTax(productonj.getString("tax"));
                                    productDetailsModel.setTotal(productonj.getString("total"));
                                    productDetailsModel.setCancellation_message(productonj.getString("cancellation_message"));
                                    productDetailsModel.setStatus(productonj.getString("status"));


                                    try{
                                        JSONObject itemobj=productonj.getJSONObject("item");
                                        ItemModel itemModel=new ItemModel();
                                        itemModel.setId(itemobj.getString("id"));
                                        itemModel.setName(itemobj.getString("name"));
                                        itemModel.setDesc(itemobj.getString("desc"));

                                        JSONObject varientobj=itemobj.getJSONObject("varinat");
                                        ProductVarientModel varientModel=new ProductVarientModel();
                                        varientModel.setId(varientobj.getString("id"));
                                        varientModel.setSku(varientobj.getString("sku"));
                                        varientModel.setPrice(varientobj.getString("price"));
                                        varientModel.setStock(varientobj.getString("stock"));
                                        varientModel.setDiscount(varientobj.getString("discount"));
                                        varientModel.setTax_id(varientobj.getString("tax_id"));
                                        varientModel.setStatus(varientobj.getString("status"));
                                        varientModel.setSection_item_id(varientobj.getString("section_item_id"));

                                        JSONObject sectionobj=varientobj.getJSONObject("section_item");
                                        SectionItemModel sectionItemModel=new SectionItemModel();
                                        sectionItemModel.setId(sectionobj.getString("id"));
                                        sectionItemModel.setName(sectionobj.getString("name"));
                                        sectionItemModel.setWeight(sectionobj.getString("weight"));

                                        varientModel.setSectionItemModel(sectionItemModel);

                                        itemModel.setVarientModel(varientModel);
                                        productDetailsModel.setItemModel(itemModel);

                                        productlist.add(productDetailsModel);
                                    }catch (JSONException e2){

                                    }

                                }

                                if(productlist.size()>0) {
                                    OrderItemsAdapter orderItemsAdapter = new OrderItemsAdapter(mContext, productlist);

                                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                  //  itemsRecycler.setLayoutManager(layoutManager);
                                //    itemsRecycler.setAdapter(orderItemsAdapter);

                                    double subtotal=0.0,discount=0.0,rateofdiscount=0.0,tax=0.0;

                                    for (int i=0;i<productlist.size();i++){
                                        subtotal=subtotal+Double.parseDouble(productlist.get(i).getSub_total());
                                        discount=discount+Double.parseDouble(productlist.get(i).getDiscount());
                                        rateofdiscount=rateofdiscount+Double.parseDouble(productlist.get(i).getRate_of_discount());
                                        tax=tax+Double.parseDouble(productlist.get(i).getTax());

                                    }
                                    if (discount==0.0){
                                    //    tv_discount.setVisibility(View.GONE);
                                    }
                                  //  tv_sub_total.setText(""+subtotal);
                                 //   tv_discount.setText(""+rateofdiscount+ "% off( "+discount+" )");
                                //    tv_tax.setText(""+tax);
                                }

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN",preferenceManager.getString(TOKEN_KEY));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}