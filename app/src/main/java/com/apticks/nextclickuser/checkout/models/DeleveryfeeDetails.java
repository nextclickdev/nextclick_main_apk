package com.apticks.nextclickuser.checkout.models;

public class DeleveryfeeDetails {
    String id,flat_rate,per_km,vehicle_type_id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlat_rate() {
        return flat_rate;
    }

    public void setFlat_rate(String flat_rate) {
        this.flat_rate = flat_rate;
    }

    public String getPer_km() {
        return per_km;
    }

    public void setPer_km(String per_km) {
        this.per_km = per_km;
    }

    public String getVehicle_type_id() {
        return vehicle_type_id;
    }

    public void setVehicle_type_id(String vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }
}
