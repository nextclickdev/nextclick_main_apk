package com.apticks.nextclickuser.checkout.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.models.ProductDetailsModel;

import java.util.ArrayList;
import java.util.List;

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder>  {

    List<ProductDetailsModel> data;
    Context context;

    public OrderItemsAdapter(Context mContext, ArrayList<ProductDetailsModel> productlist) {
        this.context = mContext;
        this.data = productlist;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.order_products, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        ProductDetailsModel orderItems = data.get(position);

        holder.tv_pname.setText(orderItems.getItemModel().getName());
        try{
            holder.tv_pdescription.setText(orderItems.getItemModel().getVarientModel().getSectionItemModel().getName()+"");
        }catch (NullPointerException e){

        }
        /*try{
            Glide.with(context)
                    .load(orderItems.getItemModel().getImagelist().get(0).getImage())
                    .placeholder(R.drawable.loader_gif)
                    .into(holder.img_product);
        }catch (IndexOutOfBoundsException e){

        }*/

        holder.tv_quantity.setText("Quantity : "+orderItems.getQty()+"");

        holder.tv_subtotal.setText("Sub Total : "+orderItems.getSub_total());

        if (orderItems.getDiscount().equalsIgnoreCase("0")) {
            holder.tv_discount.setVisibility(View.GONE);
        }else {
            holder.tv_discount.setVisibility(View.VISIBLE);
            holder.tv_discount.setText("Discount : "+orderItems.getDiscount());
        }

        holder.tv_tax.setText("Tax : "+orderItems.getTax());
        holder.tv_totalprice.setText("total : "+orderItems.getTotal());

    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {return position;}



    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_pname,tv_pdescription,tv_subtotal,tv_discount,tv_tax,tv_quantity,tv_totalprice;
        ImageView img_product;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_product = itemView.findViewById(R.id.img_product);
            tv_pname = itemView.findViewById(R.id.tv_pname);
            tv_pdescription = itemView.findViewById(R.id.tv_pdescription);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
            tv_subtotal = itemView.findViewById(R.id.tv_subtotal);
            tv_discount = itemView.findViewById(R.id.tv_discount);
            tv_tax = itemView.findViewById(R.id.tv_tax);
            tv_totalprice = itemView.findViewById(R.id.tv_totalprice);


            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }


}
