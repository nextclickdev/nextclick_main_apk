package com.apticks.nextclickuser.checkout.models;

public class Permissionweight {
    String min_capacity,as_max_capacity;

    public String getMin_capacity() {
        return min_capacity;
    }

    public void setMin_capacity(String min_capacity) {
        this.min_capacity = min_capacity;
    }

    public String getAs_max_capacity() {
        return as_max_capacity;
    }

    public void setAs_max_capacity(String as_max_capacity) {
        this.as_max_capacity = as_max_capacity;
    }
}
