package com.apticks.nextclickuser.checkout.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.NetworkUtil;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.adapters.PaymentModesAdapter;
import com.apticks.nextclickuser.checkout.adapters.SubOrdersAdapter;
import com.apticks.nextclickuser.checkout.adapters.VendorAdapter;
import com.apticks.nextclickuser.checkout.adapters.VendorSubOrdersAdapter;
import com.apticks.nextclickuser.checkout.models.CheckoutModel;
import com.apticks.nextclickuser.checkout.models.DeleveryfeeDetails;
import com.apticks.nextclickuser.checkout.models.DeliveryFeeObject;
import com.apticks.nextclickuser.checkout.models.PaymentModes;
import com.apticks.nextclickuser.checkout.models.Permissionweight;
import com.apticks.nextclickuser.checkout.models.Product;
import com.apticks.nextclickuser.checkout.models.ProductImages;
import com.apticks.nextclickuser.checkout.models.Products;
import com.apticks.nextclickuser.checkout.models.SectionProducts;
import com.apticks.nextclickuser.checkout.models.SubOrderObject;
import com.apticks.nextclickuser.checkout.models.Tax;
import com.apticks.nextclickuser.checkout.models.VehicleObject;
import com.apticks.nextclickuser.checkout.models.VendorDetails;
import com.apticks.nextclickuser.checkout.models.WeightPermissable;
import com.apticks.nextclickuser.products.activities.ChangeAddressActivity;
import com.apticks.nextclickuser.products.adapters.VarientsAdapter;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.products.model.VarientModel;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CHECKOUTPRODUCTS;
import static com.apticks.nextclickuser.Constants.Constants.ADDRESS;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class DeliveryFeeInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    PaymentModesAdapter paymentModesAdapter;
    ArrayList<PaymentModes> paymentModesArrayList;
    private CustomDialog customDialog;
    private RelativeLayout relative_layout_;
    private PreferenceManager preferenceManager;
    ArrayList<CheckoutModel> checkoutlist;
    private Gson gson;
    private String shippingaddressid, deleverymodeid, totalamount, walletamount;
    private int discount, tax, deleveryfee = 0, promocodeamount;
    private double totalbill = 0.0, subtotal = 0.0, discountprice = 0.0, taxamount = 0.0;
    private RecyclerView cartItemsRecycler;
    VendorSubOrdersAdapter vendorAdapter;
    TextView tv_delivery_fee, tv_distance_info, tv_bag_weight, tv_store, tv_vehicle,progress_tv;
    ImageView img_vehicle;
    private String vendor_user_id;
    NestedScrollView scrollview;
    ArrayList<CartModel> cartlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_fee_info);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        mContext = DeliveryFeeInfoActivity.this;
        customDialog = new CustomDialog(mContext);
        preferenceManager = new PreferenceManager(mContext);
        gson = new Gson();
        init();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                Gson gson=new Gson();
                String checkoutstring = gson.toJson(cartlist);
                Intent intent=new Intent();
                intent.putExtra("checkoutstring",checkoutstring);
                setResult(3,intent);
                finish();
                break;
        }
    }

    private void init() {

        TextView tv_header = findViewById(R.id.tv_header);

       scrollview = findViewById(R.id.scrollview);
        tv_delivery_fee = findViewById(R.id.tv_delivery_fee);
        progress_tv=findViewById(R.id.progress_tv);
        tv_distance_info = findViewById(R.id.tv_distance_info);
        tv_bag_weight = findViewById(R.id.tv_bag_weight);
        tv_vehicle = findViewById(R.id.tv_vehicle);
        img_vehicle= findViewById(R.id.img_vehicle);
        tv_store = findViewById(R.id.tv_store);
       // tv_header.setText("Delivery fee");
        //tv_header.setAllCaps(true);
        ImageView image_back = findViewById(R.id.img_back);
        image_back.setOnClickListener(this::onClick);
        checkoutlist = new ArrayList<>();
        cartItemsRecycler = findViewById(R.id.cartItemsRecycler);
        cartItemsRecycler.setLayoutManager(new GridLayoutManager(DeliveryFeeInfoActivity.this, 1));
        vendorAdapter = new VendorSubOrdersAdapter(mContext, new ArrayList<>());
        cartItemsRecycler.setAdapter(vendorAdapter);

        showAllVendorsList=getIntent().getBooleanExtra("showAllVendorsList",false);
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("shipping_address_id", getIntent().getStringExtra("shipping_address_id"));
        uploadMap.put("dalivery_mode_id", getIntent().getStringExtra("dalivery_mode_id"));
        JSONArray jsonArray = new JSONArray();

        vendor_user_id = getIntent().getStringExtra("vendor_user_id");

         cartlist = new ArrayList<>();
        String carListAsString = getIntent().getStringExtra("cartlist");
        Type type = new TypeToken<List<CartModel>>() {
        }.getType();
        cartlist = gson.fromJson(carListAsString, type);
        for (int i = 0; i < cartlist.size(); i++) {
            try {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("item_id", cartlist.get(i).getItem_id());
                jsonObject1.put("vendor_product_variant_id", cartlist.get(i).getVendor_product_varinat_id());
                jsonObject1.put("qty", cartlist.get(i).getQty());
                jsonObject1.put("vendor_user_id", cartlist.get(i).getVendor_user_id());
                jsonArray.put(jsonObject1);
            } catch (JSONException e) {
                System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
                e.printStackTrace();
            }
        }

        /*try {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("item_id",getIntent().getStringExtra("item_id"));
            jsonObject1.put("vendor_product_variant_id",getIntent().getStringExtra("vendor_product_variant_id"));
            jsonObject1.put("qty", getIntent().getStringExtra("qty"));
            jsonObject1.put("vendor_user_id", getIntent().getStringExtra("vendor_user_id"));
            jsonArray.put(jsonObject1);
        } catch (JSONException e) {
            System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
            e.printStackTrace();
        }*/
        JSONObject json = new JSONObject(uploadMap);
        try {
            json.put("products", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();

            System.out.println("aaaaaaaaa catch 2  " + e.getMessage());
        }
        if (NetworkUtil.isNetworkConnected(mContext)) {
            getProductDetails(json);
        } else {
            Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
        }
    }

 boolean showAllVendorsList = true;
    private void getProductDetails(JSONObject json) {
        final String dataStr = json.toString();
        checkoutlist.clear();
        System.out.println("aaaaaaaaaa data " + dataStr);
        customDialog.show();
        customDialog.setMessage("Loading...");
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CHECKOUTPRODUCTS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {

                            double totalDeliveryFee=0;
                            double totalBagWeight=0;
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject.toString());
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");

                            if (status && http_code == 200) {
                                JSONObject dataobject = jsonObject.getJSONObject("data");

                                JSONArray responsearray = dataobject.getJSONArray("products_group_by_vendor");

                                for (int i = 0; i < responsearray.length(); i++) {
                                    JSONObject jsonObject1 = responsearray.getJSONObject(i);
                                    JSONObject vendorobj = jsonObject1.getJSONObject("vendor_details");

                                    CheckoutModel checkoutModel = new CheckoutModel();
                                    if (!vendorobj.getString("vendor_user_id").equals(vendor_user_id) && !showAllVendorsList) {
                                        continue;
                                    }

                                    VendorDetails vendorDetails=new VendorDetails();
                                    vendorDetails.setName(vendorobj.getString("name"));
                                    vendorDetails.setVendor_user_id(vendorobj.getString("vendor_user_id"));
                                    checkoutModel.setVendorDetails(vendorDetails);

                                    if(!showAllVendorsList)
                                        tv_store.setText("" + vendorobj.getString("name"));
                                    else
                                        tv_store.setVisibility(View.GONE);
                                    //for now
                                    tv_store.setVisibility(View.GONE);

                                    //tv_subtotaltext.setText("SubTotal ("+productslist.size()+" Items )");
                                    //sub orders
                                    try {
                                        JSONArray subOrdersArray = jsonObject1.getJSONArray("sub_orders");
                                        ArrayList<SubOrderObject> subOrdersList = new ArrayList<>();

                                        if (jsonObject1.has("total_weight_of_this_bag")) {
                                            String totalweight = jsonObject1.getString("total_weight_of_this_bag");
                                            try {
                                                totalBagWeight += Double.parseDouble(totalweight);
                                            } catch (Exception ex) {
                                                tv_bag_weight.setText(totalweight + " gms");
                                            }
                                            // tv_bag_weight.setText("Total Weight: " + totalweight);
                                        }


                                        for (int j = 0; j < subOrdersArray.length(); j++) {
                                            JSONObject subOrder = subOrdersArray.getJSONObject(j);
                                            SubOrderObject subOrderObject = new SubOrderObject();

                                            DeliveryFeeObject deliveryFeeObject = new DeliveryFeeObject();
                                            try {


                                                if (subOrder.has("delivery_fee")) {

                                                    Object obj = subOrder.get("delivery_fee");

                                                    if (obj instanceof JSONObject) {
                                                        JSONObject delivery_fee = subOrder.getJSONObject("delivery_fee");
                                                        deliveryFeeObject.setId(delivery_fee.getString("id"));
                                                        deliveryFeeObject.setFlatRate(delivery_fee.getString("flat_rate"));
                                                        deliveryFeeObject.setKMInfo(delivery_fee.getString("per_km"));
                                                        deliveryFeeObject.setVehicleTypeId(delivery_fee.getString("vehicle_type_id"));


                                                        //tv_delivery_fee.setText(deliveryFeeObject.getFlatRate());
                                                        tv_distance_info.setText("for " + deliveryFeeObject.getKMInfo() + " km");

                                                        if (deliveryFeeObject.getVehicleTypeId().equals("2")) {
                                                            img_vehicle.setImageDrawable(getResources().getDrawable(R.drawable.ic_auto));
                                                            //tv_vehicle.setText("Delivery Mode : Auto");
                                                        } else {
                                                            //tv_vehicle.setText("Delivery Mode : 2 Wheeler");
                                                            img_vehicle.setImageDrawable(getResources().getDrawable(R.drawable.ic_byke));
                                                        }
                                                    } else {
                                                        deliveryFeeObject.setFlatRate(subOrder.getString("delivery_fee"));
                                                    }
                                                    subOrderObject.setDeliveryFeeObject(deliveryFeeObject);
                                                }
                                            } catch (Exception e) {
                                            }

                                            ArrayList<Products> _productList = getSubOrderProducts(subOrder);
                                            subOrderObject.setProductslist(_productList);
                                            checkoutModel.setProductslist(_productList);

                                            VehicleObject vehicleObject = new VehicleObject();
                                            if (subOrder.has("vehicle")) {
                                                JSONObject vehicle = subOrder.getJSONObject("vehicle");
                                                vehicleObject.setName(vehicle.getString("name"));
                                                vehicleObject.setDesc(vehicle.getString("desc"));
                                            }
                                            subOrderObject.setVehicleDetails(vehicleObject);

                                            if (subOrder.has("bag_weight")) {
                                                deliveryFeeObject.setBagWeight(subOrder.getString("bag_weight"));
                                            }
                                            totalDeliveryFee += Double.parseDouble(deliveryFeeObject.getFlatRate());
                                            subOrdersList.add(subOrderObject);
                                        }
                                        checkoutModel.setSubOrdersList(subOrdersList);
                                        tv_delivery_fee.setText("" + totalDeliveryFee);

                                        if (totalBagWeight >= 1000)
                                            tv_bag_weight.setText(String.format("%.2f", (totalBagWeight / 1000)) + "kgs");
                                        else
                                            tv_bag_weight.setText(String.format("%.2f", totalBagWeight) + "gms");
                                        progress_tv.setText((totalBagWeight / 1000) + " Kgs");
                                    } catch (Exception ex) {
                                        System.out.println("Exception in sub orders parser " + ex.getMessage());
                                    }

                                    checkoutlist.add(checkoutModel);
                                }

                               /* if(!showAllVendorsList) {
                                    vendorAdapter.setrefresh(checkoutlist.get(0).getSubOrdersList());
                                }
                                else {
                                    ArrayList<SubOrderObject> list = new ArrayList<>();
                                    for (int i = 0; i < checkoutlist.size(); i++)
                                        list.addAll(checkoutlist.get(i).getSubOrdersList());
                                    vendorAdapter.setrefresh(list);
                                }*/

                                vendorAdapter.setrefresh(checkoutlist,totalBagWeight);

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                        finally {
                            scrollview.setVisibility(View.VISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private ArrayList<Products> getSubOrderProducts(JSONObject jsonObject1) {

        ArrayList<Products> _productList = new ArrayList<>();
        JSONArray productsarray = null;
        try {
            //weight : 1000
            //vendor_product_variant_id : 179
            //qty : 1
            productsarray = jsonObject1.getJSONArray("products");
            // JSONObject  productJsonObject = productJson.getJSONObject(0);//it should not be array
            //  productsarray = productJsonObject.getJSONArray("product_details");

            for (int j = 0; j < productsarray.length(); j++) {
                JSONObject productsobject = productsarray.getJSONObject(j);

                Products products = new Products();

                //new fields
                products.setweight(productsobject.getString("weight"));
                products.setSubOrderQty(productsobject.getString("qty"));
                products.setVendor_product_variant_id(productsobject.getString("vendor_product_variant_id"));
                products.setItem_id(productsobject.getString("item_id"));
                //


               // JSONObject productsobject = productJson.getJSONObject("product_details");

                Product product = new Product();
                product.setName(productsobject.getString("name"));
                products.setProduct(product);

                SectionProducts sectionProducts = new SectionProducts();
                sectionProducts.setName(productsobject.getString("section_item_name"));

                products.setSectionProducts(sectionProducts);

                _productList.add(products);

            }
        } catch (JSONException e) {
            e.printStackTrace();
            System.out.println("Exception in sub orders parser " + e.getMessage());
        }

        return _productList;
    }

    public void setDelete(Products cartModel) {
        boolean finished = false;
        for (int i=0;i<checkoutlist.size();i++){
            if (!finished){
                for (int j=0;j<checkoutlist.get(i).getSubOrdersList().size();j++){
                    if (!finished) {
                        for (int k = 0; k < checkoutlist.get(i).getSubOrdersList().get(j).getProductslist().size(); k++) {
                            if (checkoutlist.get(i).getSubOrdersList().get(j).getProductslist().get(k).getItem_id().equalsIgnoreCase(cartModel.getItem_id())) {
                                checkoutlist.get(i).getSubOrdersList().get(j).getProductslist().remove(k);
                                finished = true;
                                break;
                            }
                        }
                    }


                }
            }


        }
        for (int i=0;i<cartlist.size();i++){
            if (cartlist.get(i).getItem_id().equalsIgnoreCase(cartModel.getItem_id())){
                if (cartlist.get(i).getQty().equalsIgnoreCase("1")){
                    cartlist.remove(i);
                }else {
                    int updateqty=Integer.parseInt(cartlist.get(i).getQty())-1;
                   // CartModel cartModel1=new CartModel();
                   // cartModel1.setQty(""+updateqty);
                  //  cartlist.set(i,cartModel1);
                    cartlist.get(i).setQty(""+updateqty);
                }

            }
        }
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("shipping_address_id", getIntent().getStringExtra("shipping_address_id"));
        uploadMap.put("dalivery_mode_id", getIntent().getStringExtra("dalivery_mode_id"));
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < checkoutlist.size(); i++) {
            for (int j=0;j<checkoutlist.get(i).getSubOrdersList().size();j++){
                for (int k=0;k<checkoutlist.get(i).getSubOrdersList().get(j).getProductslist().size();k++){
                    try {
                        JSONObject jsonObject1 = new JSONObject();
                        jsonObject1.put("item_id", checkoutlist.get(i).getSubOrdersList().get(j).getProductslist().get(k).getItem_id());
                        jsonObject1.put("vendor_product_variant_id", checkoutlist.get(i).getSubOrdersList().get(j).getProductslist().get(k).getVendor_product_variant_id());
                        jsonObject1.put("qty", checkoutlist.get(i).getSubOrdersList().get(j).getProductslist().get(k).getSubOrderQty());
                        jsonObject1.put("vendor_user_id", checkoutlist.get(i).getVendorDetails().getVendor_user_id());
                        jsonArray.put(jsonObject1);
                    } catch (JSONException e) {
                        System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
                        e.printStackTrace();
                    }
                }

            }

        }

        /*try {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("item_id",getIntent().getStringExtra("item_id"));
            jsonObject1.put("vendor_product_variant_id",getIntent().getStringExtra("vendor_product_variant_id"));
            jsonObject1.put("qty", getIntent().getStringExtra("qty"));
            jsonObject1.put("vendor_user_id", getIntent().getStringExtra("vendor_user_id"));
            jsonArray.put(jsonObject1);
        } catch (JSONException e) {
            System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
            e.printStackTrace();
        }*/
        JSONObject json = new JSONObject(uploadMap);
        try {
            json.put("products", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();

            System.out.println("aaaaaaaaa catch 2  " + e.getMessage());
        }
        if (NetworkUtil.isNetworkConnected(mContext)) {
            getProductDetails(json);
        } else {
            Toast.makeText(this, "No internet connection! ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Gson gson=new Gson();
        String checkoutstring = gson.toJson(cartlist);
        Intent intent=new Intent();
        intent.putExtra("checkoutstring",checkoutstring);
        setResult(3,intent);
        finish();

    }
}