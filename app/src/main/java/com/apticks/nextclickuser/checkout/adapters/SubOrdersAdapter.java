package com.apticks.nextclickuser.checkout.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.checkout.models.CheckoutModel;
import com.apticks.nextclickuser.checkout.models.SubOrderObject;

import org.json.JSONObject;

import java.util.ArrayList;

public class SubOrdersAdapter  extends RecyclerView.Adapter<SubOrdersAdapter.ItemViewHolder> {

    private final double totalBagWeight;
    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    ArrayList<SubOrderObject> cartllist;
    Context context;
    int mExpandedPosition,previousExpandedPosition;
    boolean ischecked;

    

    public SubOrdersAdapter(Context mContext, ArrayList<SubOrderObject> productslist, double totalBagWeight) {
        this.context=mContext;
        this.cartllist=productslist;
        this.totalBagWeight=totalBagWeight;
    }

    @NonNull
    @Override
    public SubOrdersAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_suborder_item, viewGroup, false);
        return new SubOrdersAdapter.ItemViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull SubOrdersAdapter.ItemViewHolder itemViewHolder, int position) {
        SubOrderObject item = cartllist.get(position);

        itemViewHolder.tv_delivery_fee.setText(item.getDeliveryFeeObject().getFlatRate());
       // itemViewHolder.tv_distance_info.setText("for "+item.getDeliveryFeeObject().getKMInfo()+" km");

        try {
            double weight=Double.parseDouble(item.getDeliveryFeeObject().getBagWeight());
            itemViewHolder.tv_bag_weight.setText(String.format("%.2f",(weight/1000)) + " kgs");
        }
        catch (Exception ex) {
            itemViewHolder.tv_bag_weight.setText(item.getDeliveryFeeObject().getBagWeight()+" gms");
        }

        if (item.getVehicleDetails()!=null && item.getVehicleDetails().getName().equals("Two wheeler") /*item.getDeliveryFeeObject().getFlatRate().equals("20")*/) {
            itemViewHolder.img_vehicle.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_byke));
            //tv_vehicle.setText("Delivery Mode : Auto");
        }
        else {
            //tv_vehicle.setText("Delivery Mode : 2 Wheeler");
            itemViewHolder.img_vehicle.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_auto));
        }

        // Create layout manager with initial prefetch item count
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                itemViewHolder.rvSubItem.getContext(),
                LinearLayoutManager.VERTICAL,
                false
        );
        SubProductsAdapter subItemAdapter = new SubProductsAdapter(context,item.getProductslist(),totalBagWeight);
        itemViewHolder.rvSubItem.setLayoutManager(layoutManager);
        itemViewHolder.rvSubItem.setAdapter(subItemAdapter);
        itemViewHolder.rvSubItem.setRecycledViewPool(viewPool);
    }

    @Override
    public int getItemCount() {
        return cartllist.size();
    }

    public void setrefresh(ArrayList<SubOrderObject> productslist) {
        this.cartllist=productslist;
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvSubItem;
        TextView tv_delivery_fee,tv_distance_info,tv_bag_weight;

        ImageView img_vehicle;
        ItemViewHolder(View itemView) {
            super(itemView);
            rvSubItem = itemView.findViewById(R.id.rv_sub_item);
            tv_delivery_fee = itemView.findViewById(R.id.tv_delivery_fee);
            tv_distance_info = itemView.findViewById(R.id.tv_distance_info);
            img_vehicle = itemView.findViewById(R.id.img_vehicle);
            tv_bag_weight= itemView.findViewById(R.id.tv_bag_weight);
        }
    }
}
