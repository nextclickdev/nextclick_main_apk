package com.apticks.nextclickuser.checkout.models;

import com.apticks.nextclickuser.products.model.ImagesModel;

import java.util.ArrayList;

public class ItemModel {

    String id,name,desc;
    ArrayList<ImagesModel> imagelist;
    ProductVarientModel varientModel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<ImagesModel> getImagelist() {
        return imagelist;
    }

    public void setImagelist(ArrayList<ImagesModel> imagelist) {
        this.imagelist = imagelist;
    }

    public ProductVarientModel getVarientModel() {
        return varientModel;
    }

    public void setVarientModel(ProductVarientModel varientModel) {
        this.varientModel = varientModel;
    }
}
