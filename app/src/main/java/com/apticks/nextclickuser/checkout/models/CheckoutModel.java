package com.apticks.nextclickuser.checkout.models;

import java.util.ArrayList;

public class CheckoutModel {

    DeleveryfeeDetails delivery_fee;
    ArrayList<Products> productslist;
    ArrayList<SubOrderObject> subOrderslist;
    VendorDetails vendorDetails;
    WeightPermissable weightPermissable;
    boolean isexpand;

    double discount,tax,subtotal,grandtotal;

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getGrandtotal() {
        return grandtotal;
    }

    public void setGrandtotal(double grandtotal) {
        this.grandtotal = grandtotal;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public WeightPermissable getWeightPermissable() {
        return weightPermissable;
    }

    public void setWeightPermissable(WeightPermissable weightPermissable) {
        this.weightPermissable = weightPermissable;
    }

    public VendorDetails getVendorDetails() {
        return vendorDetails;
    }

    public void setVendorDetails(VendorDetails vendorDetails) {
        this.vendorDetails = vendorDetails;
    }

    public DeleveryfeeDetails getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(DeleveryfeeDetails delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public ArrayList<Products> getProductslist() {
        return productslist;
    }

    public void setProductslist(ArrayList<Products> productslist) {
        this.productslist = productslist;
    }

    public boolean isIsexpand() {
        return isexpand;
    }

    public void setIsexpand(boolean isexpand) {
        this.isexpand = isexpand;
    }


    public ArrayList<SubOrderObject> getSubOrdersList() {
        return subOrderslist;
    }

    public void setSubOrdersList(ArrayList<SubOrderObject> subOrderslist) {
        this.subOrderslist = subOrderslist;
    }
}


