package com.apticks.nextclickuser.checkout.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.CustomDialog;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.checkout.models.Products;
import com.apticks.nextclickuser.dialogs.RemoveProduct;
import com.apticks.nextclickuser.products.activities.CartAddActivity;
import com.apticks.nextclickuser.products.model.CartModel;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.CARTUPDATE;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.profilepic;
import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isNull;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    ArrayList<Products> cartlist;
    Context context;
    CustomDialog customDialog;
    PreferenceManager preferenceManager;
    RemoveProduct removeProduct;
    public ProductsAdapter(Context context, ArrayList<Products> productlist) {
        this.context=context;
        this.cartlist=productlist;
        this.customDialog=new CustomDialog(context);
        this.preferenceManager=new PreferenceManager(context);
    }

    @NonNull
    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.cart_products_adpter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new ProductsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsAdapter.ViewHolder holder, int position) {
        Products cartModel = cartlist.get(position);


        String output = cartModel.getProduct().getName().substring(0, 1).toUpperCase() + cartModel.getProduct().getName().substring(1);
        holder.tv_pname.setText(output);

      //  holder.tv_pname.setText(cartModel.getProductCartModel().getName());
        holder.tv_qty.setText("Quantity  \n"+cartModel.getQty());
        holder.tv_stock.setText("Stock  \n"+cartModel.getStock());
        holder.tv_pdescription.setText(isNull(cartModel.getSectionProducts().getName()));
        holder.tv_subtotal.setText("Sub Total : "+cartModel.getPrice());
        holder.tv_tax.setText("Tax : "+cartModel.getTax().getRate()+" %off");
        holder.tv_discount.setText("Discount : "+cartModel.getDiscount()+" % Off");

        if (cartModel.getStock().equalsIgnoreCase("0")){
            holder.tv_stock.setTextColor(context.getResources().getColor(R.color.red));
        }else {
            holder.tv_stock.setTextColor(context.getResources().getColor(R.color.LimeGreen));
        }

        try{
            Glide.with(context)
                    .load(cartModel.getImagelist().get(0).getImage())
                    .placeholder(R.drawable.loader_gif)
                    .into(holder.img_product);
        }catch (IndexOutOfBoundsException e){

        }


        if (cartModel.getDiscount().equalsIgnoreCase("0")){
            holder.tv_discount.setVisibility(View.GONE);
            try{
                holder.tv_tax.setVisibility(View.VISIBLE);
                holder.tv_subtotal.setPaintFlags(holder.tv_subtotal.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
                double price=Double.parseDouble(cartModel.getPrice());
                double tax=Double.parseDouble(cartModel.getTax().getRate());

                double taxamount=((price / 100.0f) * tax);
                double totalprice =price+taxamount;
                holder.tv_totalprice.setText("Price : "+totalprice);
            }catch (NullPointerException ex){
                holder.tv_subtotal.setVisibility(View.GONE);
                holder.tv_tax.setVisibility(View.GONE);
                holder.tv_totalprice.setText("Price : "+cartModel.getPrice());
            }
        }else {
            try{

                holder.tv_discount.setVisibility(View.VISIBLE);
                holder.tv_tax.setVisibility(View.VISIBLE);
                holder.tv_subtotal.setPaintFlags(holder.tv_subtotal.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);

                double discount=Double.parseDouble(cartModel.getDiscount());
                double price=Double.parseDouble(cartModel.getPrice());
                double tax=Double.parseDouble(cartModel.getTax().getRate());

                double discounta=((price / 100.0f) * discount);
                double taxamount=((price / 100.0f) * tax);

                double totalprice =price- discounta+taxamount;

               // System.out.println("aaaaaaaa price  "+discounta+"  "+taxamount+"  "+totalprice);

                holder.tv_totalprice.setText("Toal Price : "+totalprice);

            }catch (NullPointerException ex){
                holder.tv_discount.setVisibility(View.VISIBLE);
                holder.tv_tax.setVisibility(View.GONE);
                holder.tv_subtotal.setPaintFlags(holder.tv_subtotal.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);

                double discount=Double.parseDouble(cartModel.getDiscount());
                double price=Double.parseDouble(cartModel.getPrice());

                double totalprice =price- ((price / 100.0f) * discount);

                holder.tv_totalprice.setText("Toal Price : "+totalprice);
            }
        }

        holder.img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((CheckOutPageActivity)context).setDelete(cartModel);
              //   removeProduct=new RemoveProduct(context,cartModel);
              //  removeProduct.showDialog();
            }
        });

    }

    public void setrefresh(ArrayList<Products> productlist){
        this.cartlist=productlist;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return cartlist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_product,img_cancel;
        private TextView tv_pname,tv_pdescription,tv_tax,tv_subtotal,tv_totalprice,tv_qty,tv_stock,tv_discount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_product=itemView.findViewById(R.id.img_product);
            tv_pname=itemView.findViewById(R.id.tv_pname);
            tv_pdescription=itemView.findViewById(R.id.tv_pdescription);
            tv_tax=itemView.findViewById(R.id.tv_tax);
            img_cancel=itemView.findViewById(R.id.img_cancel);
            tv_stock=itemView.findViewById(R.id.tv_stock);
            tv_qty=itemView.findViewById(R.id.tv_qty);
            tv_subtotal=itemView.findViewById(R.id.tv_subtotal);
            tv_totalprice=itemView.findViewById(R.id.tv_totalprice);
            tv_discount=itemView.findViewById(R.id.tv_discount);

        }
    }

    private void ProductUpdate(CartModel cartModel, int position,String qty,int whichclick,TextView tv_plus,TextView tv_minus ) {

        customDialog.show();
        Map<String,String> countMap = new HashMap<>();
        countMap.put("id", cartModel.getId());
        countMap.put("item_id",cartModel.getItem_id());
        countMap.put("vendor_product_variant_id",cartModel.getVendor_product_varinat_id());
        countMap.put("qty",""+qty);
        countMap.put("vendor_user_id",cartModel.getVendor_user_id());

        final String data = new JSONObject(countMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CARTUPDATE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customDialog.dismiss();
                System.out.println("aaaaaaaaaa  response  "+response.toString());
                if (whichclick==0){
                    tv_plus.setClickable(true);
                    tv_plus.setEnabled(true);
                    if (Integer.parseInt(qty)==0){
                        tv_minus.setEnabled(false);
                        tv_minus.setClickable(false);

                    }else {
                        cartModel.setQty(qty);
                    }
                }else {
                    int stock=Integer.parseInt(cartModel.getVendorvarientProducts().getStock());
                    int  quantity=Integer.parseInt(qty);
                    if (quantity<=stock){
                        tv_minus.setEnabled(true);
                        tv_minus.setClickable(true);
                        cartModel.setQty(qty);
                        System.out.println("aaaaaaa qty "+qty);
                    }else {
                        Toast.makeText(context, "Out Of Stock", Toast.LENGTH_SHORT).show();
                        tv_plus.setClickable(false);
                        tv_plus.setEnabled(false);
                    }
                }
                notifyItemChanged(position);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",  preferenceManager.getString(TOKEN_KEY));

                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

        };
        requestQueue.add(stringRequest);

    }




}

