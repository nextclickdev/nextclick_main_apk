package com.apticks.nextclickuser.checkout.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.checkout.activities.CheckOutPageActivity;
import com.apticks.nextclickuser.checkout.activities.OrderDetailsActivity;
import com.apticks.nextclickuser.checkout.activities.OrderHistoryActivity;
import com.apticks.nextclickuser.checkout.activities.OrderTracking;
import com.apticks.nextclickuser.checkout.activities.OrdertrackMapActivity;
import com.apticks.nextclickuser.checkout.models.DeleveryModes;
import com.apticks.nextclickuser.checkout.models.OrderHistoryModel;
import com.apticks.nextclickuser.orderhistory.OrderDetailHistroy;

import java.util.ArrayList;
import java.util.Calendar;

import static com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs.isnullboolean;
import static java.util.Objects.isNull;

public class OrdersHistoryAdapter extends RecyclerView.Adapter<OrdersHistoryAdapter.ViewHolder>   {

    Context context;
    ArrayList<OrderHistoryModel> orderslist;
    public OrdersHistoryAdapter(OrderHistoryActivity context, ArrayList<OrderHistoryModel> orderslist) {
        this.context=context;
        this.orderslist=orderslist;
    }

    @NonNull
    @Override
    public OrdersHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.order_details, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new OrdersHistoryAdapter.ViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull OrdersHistoryAdapter.ViewHolder holder, int position) {

        holder.tv_orderid.setText("#"+orderslist.get(position).getTrack_id());
        holder.total_amount.setText("₹ "+orderslist.get(position).getOrderAmount());
        //holder.total_amount.setText("₹ "+orderslist.get(position).getPaymentModel().getAmount());

        /*try
        {
          String paymentMethod="";
            switch (orderslist.get(position).getPaymentModel().getPaymentMethod().getName())
            {
                case "COD":
                    paymentMethod="COD";
                    break;
                default:
                    paymentMethod="Online";
            }
            holder.tv_payment_type.setText(paymentMethod);
        }
        catch (Exception ex)
        {

        }*/

        //holder.total_amount.setText("Total \n ₹ "+orderslist.get(position).getPaymentModel().getAmount());
        //holder.tv_paymentmethod.setText("Payment type : "+orderslist.get(position).getPaymentModel().getPaymentMethod().getName());
        String status=  orderslist.get(position).getVendorStatus();
        if(status ==null || status.isEmpty()|| status.equals("null")) {
            status = orderslist.get(position).getOrderStatus().getStatus();
        }
        else {
            switch (orderslist.get(position).getVendorStatus()) {
                case "500":
                    status = "Rejected";
                    break;
                case "501":
                    status = "Received";
                    break;
                case "502":
                    status = "Accepted";
                    break;
                case "503":
                    status = "Cancelled";
                    break;
                case "504":
                    status = "Reached to pickup point";
                    break;
                case "505":
                    status = " Picked the order";
                    break;
                case "506":
                    status = "Reached to delivery point";
                    break;
                case "507":
                    status = "Delivery is on hold";
                    break;
                case "508":
                    status = "Delivered";
                    break;
                default:
                    status = "Received";
                    break;
            }
        }
        holder.tv_status.setText("Status : "+status);
        if (orderslist.get(position).getOrderStatus().getId().equalsIgnoreCase("16")) {
            //holder.tv_status.setTextColor(context.getResources().getColor(R.color.red));
        }
        else if(orderslist.get(position).getOrderStatus().getId().equalsIgnoreCase("5"))
        {
           // holder.tv_status.setTextColor(0XFF25d38a);
        }
        else {
          //  holder.tv_status.setTextColor(0XFFfd6e3c);
        }

        System.out.println("aaaaaaaaa status id  "+orderslist.get(position).getOrderStatus().getId());





        holder.tv_shopname.setText(orderslist.get(position).getShopName());//"IMRAN STORE, Hitech City, Hyderabad-500081"
        // holder.tv_shopname.setText("IMRAN STORE, G1, New Mark House, Patrika Nagar, Madhapur, Hitech City, Hyderabad-500081");

        String currentString = orderslist.get(position).getCreated_at();
        String[] separated = currentString.split(" ");
        String date=separated[0];
        String time=separated[1];

        String[] datespilt=date.split("-");
        int datest=Integer.parseInt(datespilt[2]);
        int month=Integer.parseInt(datespilt[1]);
        int year=Integer.parseInt(datespilt[0]);

        String[] timespilt=time.split(":");
        String hour=timespilt[0];
        String minitu=timespilt[1];
        String sec=timespilt[2];

        Calendar calendar=Calendar.getInstance();

        //calendar.set(Integer.parseInt(year),Integer.parseInt(month),Integer.parseInt(datest));

       /* calendar.set(Calendar.YEAR, Integer.parseInt(year));
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(month));
        calendar.set(Calendar.DATE, Integer.parseInt(datest));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
        calendar.set(Calendar.MINUTE, Integer.parseInt(minitu));
        calendar.set(Calendar.SECOND, Integer.parseInt(sec));*/

       // calendar.set(Calendar.MILLISECOND, 0);

        calendar.set(year, month, datest,
                Integer.parseInt(hour), Integer.parseInt(minitu), Integer.parseInt(sec));

        System.out.println("aaaaaa time "+datest+" "+month+" "+year+" "+hour+" "+minitu+" "+sec+"  "+calendar.get(Calendar.MONTH));

       String text= getFormattedDate(calendar.getTimeInMillis());
        System.out.println("aaaaaaa text "+text);
        holder.tv_cerate.setText(""+text.trim());
       // holder.tv_cerate.setText(""+orderslist.get(position).getCreated_at());

       /* if (isnullboolean(orderslist.get(position).getOrder_delivery_otp())){
           // holder.layout_otp.setVisibility(View.VISIBLE);
            holder.tv_otp.setText(orderslist.get(position).getOrder_delivery_otp());
        }else {
           // holder.layout_otp.setVisibility(View.GONE);
            holder.tv_otp.setText("NA");
        }*/

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(context, OrderDetailsActivity.class);
                /*Intent intent = new Intent(context, OrdertrackMapActivity.class);
                intent.putExtra("order_id", orderslist.get(position).getId());
                context.startActivity(intent);*/

                Intent intent = new Intent(context, OrderDetailHistroy.class);
                intent.putExtra("order_id", orderslist.get(position).getId());
                intent.putExtra("order_track_id", orderslist.get(position).getTrack_id());
                context.startActivity(intent);

               /* Intent i=new Intent(context, OrderTracking.class);
                i.putExtra("orderStatus",""+position);
                context.startActivity(i);*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return orderslist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}

    public void setrefresh(ArrayList<OrderHistoryModel> orderslist) {
        this.orderslist=orderslist;
        notifyDataSetChanged();
    }

    public String getFormattedDate(long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMilis);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEE, MMM d, h:mm aa";
        final long HOURS = 60 * 60 * 60;
        System.out.println("aaaaaaaa check "+now.get(Calendar.DATE) +"  "+smsTime.get(Calendar.DATE));

        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE) ) {
            return "Today " + DateFormat.format(timeFormatString, smsTime);
           // return "Today \n" + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1  ){
            return "Yesterday " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            smsTime.set(Calendar.MONTH,smsTime.get(Calendar.MONTH)-1);
            return DateFormat.format(dateTimeFormatString+"\n", smsTime).toString();
        } else {
            return DateFormat.format("MMM dd yyyy, h:mm aa", smsTime).toString();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout card_view;
        LinearLayout layout_otp;
        private TextView tv_orderid,total_amount,tv_paymentmethod,tv_status,tv_otp,tv_cerate,tv_shopname,tv_payment_type;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            card_view=itemView.findViewById(R.id.card_view);
            tv_orderid=itemView.findViewById(R.id.tv_orderid);
            total_amount=itemView.findViewById(R.id.total_amount);
            tv_paymentmethod=itemView.findViewById(R.id.tv_paymentmethod);
            tv_status=itemView.findViewById(R.id.tv_status);
            tv_otp=itemView.findViewById(R.id.tv_otp);
            layout_otp=itemView.findViewById(R.id.layout_otp);
            tv_cerate=itemView.findViewById(R.id.tv_cerate);
            tv_shopname=itemView.findViewById(R.id.tv_shopname);
          //  tv_payment_type=itemView.findViewById(R.id.tv_payment_type);
        }
    }
}
