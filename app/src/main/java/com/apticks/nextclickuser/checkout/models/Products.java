package com.apticks.nextclickuser.checkout.models;

import java.util.ArrayList;

public class Products {
    String id,sku,price,stock,discount,status,item_id,section_item_id,qty,is_available;
    ArrayList<ProductImages> imagelist;
    Product product;
    SectionProducts sectionProducts;
    Tax tax;


    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getSection_item_id() {
        return section_item_id;
    }

    public void setSection_item_id(String section_item_id) {
        this.section_item_id = section_item_id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getIs_available() {
        return is_available;
    }

    public void setIs_available(String is_available) {
        this.is_available = is_available;
    }

    public ArrayList<ProductImages> getImagelist() {
        return imagelist;
    }

    public void setImagelist(ArrayList<ProductImages> imagelist) {
        this.imagelist = imagelist;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public SectionProducts getSectionProducts() {
        return sectionProducts;
    }

    public void setSectionProducts(SectionProducts sectionProducts) {
        this.sectionProducts = sectionProducts;
    }



    String weight,vendor_product_variant_id,_qty;

    public String getVendor_product_variant_id() {
        return vendor_product_variant_id;
    }

    public void setVendor_product_variant_id(String vendor_product_variant_id) {
        this.vendor_product_variant_id = vendor_product_variant_id;
    }

    public String getweight() {
        return weight;
    }

    public void setweight(String weight) {
        this.weight = weight;
    }

    public String getSubOrderQty() {
        return _qty;
    }

    public void setSubOrderQty(String _qty) {
        this._qty = _qty;
    }

}
