package com.apticks.nextclickuser.checkout.models;

public class ProductVarientModel {

    String id,sku,price,stock,discount,tax_id,status,section_item_id;

    SectionItemModel sectionItemModel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTax_id() {
        return tax_id;
    }

    public void setTax_id(String tax_id) {
        this.tax_id = tax_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSection_item_id() {
        return section_item_id;
    }

    public void setSection_item_id(String section_item_id) {
        this.section_item_id = section_item_id;
    }

    public SectionItemModel getSectionItemModel() {
        return sectionItemModel;
    }

    public void setSectionItemModel(SectionItemModel sectionItemModel) {
        this.sectionItemModel = sectionItemModel;
    }
}
