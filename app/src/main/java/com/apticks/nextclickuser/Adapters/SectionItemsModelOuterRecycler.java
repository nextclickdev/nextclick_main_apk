package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.SectionItemsRadioModel;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.Money;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class SectionItemsModelOuterRecycler extends RecyclerView.Adapter<SectionItemsModelOuterRecycler.ViewModel> {
    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListInner = new ArrayList<>();
    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListInnerName = new ArrayList<>();
    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListOuter = new ArrayList<>();
    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListInnerHashMap = new ArrayList<>();
    public List<SectionItemsRadioModel> currentSelectedItemsRadio = new ArrayList<>();
    public List<SectionItemsRadioModel> currentSelectedSectionItems = new ArrayList<>();
    HashMap<String,Object> stringObjectOuterHashMap = new HashMap<>();
    Set entries;
    int priceInt;
    public int priceIntTotal,checkBoxPrice;
    TextView total_price_tv;
    Context mContext;
    public SectionItemsModelOuterRecycler(ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListInner,
                                          HashMap<String, Object> stringObjectOuterHashMap,
                                          ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListOuter,
                                          Set entries,
                                          int priceInt,
                                          TextView total_price_tv) {

        this.sectionItemsCheckModelsArrayListInner = sectionItemsCheckModelsArrayListInner;
        this.stringObjectOuterHashMap = stringObjectOuterHashMap;
        this.sectionItemsCheckModelsArrayListOuter = sectionItemsCheckModelsArrayListOuter;
        this.entries = entries;
        this.priceInt = priceInt;
        this.total_price_tv = total_price_tv;
        priceIntTotal = priceInt;
        sectionItemsCheckModelsArrayListInnerName = (ArrayList<SectionItemsRadioModel>) stringObjectOuterHashMap.get("0");
    }

    @NonNull
    @Override
    public SectionItemsModelOuterRecycler.ViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.outer_recycle,parent,false);
        mContext = parent.getContext();

        return  new SectionItemsModelOuterRecycler.ViewModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SectionItemsModelOuterRecycler.ViewModel holder, int position) {
        SectionItemsRadioModel  sectionItemsRadioModel = sectionItemsCheckModelsArrayListOuter.get(position);
        holder.foodSectionHeader.setText(sectionItemsRadioModel.getName());
        //sectionItemsRadioModel.getItemField();
        //sectionItemsCheckModelsArrayListInnerName = (ArrayList<SectionItemsRadioModel>)stringObjectOuterHashMap.get(position);
        sectionItemsCheckModelsArrayListInnerHashMap = (ArrayList<SectionItemsRadioModel>) stringObjectOuterHashMap.get(String.valueOf(position));
        //Toast.makeText(mContext, String.valueOf(entries), Toast.LENGTH_SHORT).show();

        if (!sectionItemsRadioModel.getItemField().equals("2")) {
            //Radio Button
            //Toast.makeText(mContext, "2", Toast.LENGTH_SHORT).show();
            //SectionItemsModelInnerRecycler sectionItemsModelInnerRecycler = new SectionItemsModelInnerRecycler(sectionItemsCheckModelsArrayListInner,true);
            String required = sectionItemsRadioModel.getRequired();
            if (sectionItemsCheckModelsArrayListInnerHashMap!=null)
                if(sectionItemsCheckModelsArrayListInnerHashMap.size()!=0) {
                    int secPrice = sectionItemsRadioModel.getSecPrice();
                    SectionItemsModelInnerRecycler sectionItemsModelInnerRecycler = new SectionItemsModelInnerRecycler(sectionItemsCheckModelsArrayListInnerHashMap, true, new SectionItemsModelInnerRecycler.OnItemCheckListener() {
                        @Override
                        public void onItemCheck(SectionItemsRadioModel item,int secPrice, int price) {
                            currentSelectedItemsRadio = new ArrayList<>();
                            currentSelectedItemsRadio.add(item);
                            //currentSelectedSectionItems.add(item);
                            if (secPrice ==3){

                            }else if (secPrice==1){
                                //total_price_tv.setText( String.valueOf(priceInt+price));
                                //total_price_tv.setText( String.valueOf(priceIntTotal+price));
                                total_price_tv.setText(Money.rupees(
                                        BigDecimal.valueOf(Long.valueOf(priceIntTotal+price
                                        ))).toString()
                                        + "     ");

                                priceIntTotal+=price;
                                //checkBoxPrice+=price;
                                //UImsgs.showToast(mContext,String.valueOf(priceInt+price) + "  AND " +priceInt +" AND  " + price);
                            }else if (secPrice==2) {
                                //total_price_tv.setText( String.valueOf(price));
                                if (checkBoxPrice == 0){
                                    total_price_tv.setText(Money.rupees(
                                            BigDecimal.valueOf(Long.valueOf(price
                                            ))).toString()
                                            + "     ");
                                    priceIntTotal = price;
                                }else{
                                    priceIntTotal = checkBoxPrice+price;
                                    total_price_tv.setText(Money.rupees(
                                            BigDecimal.valueOf(Long.valueOf(priceIntTotal
                                            ))).toString()
                                            + "     ");
                                }
                            }
                        }

                        @Override
                        public void onItemUncheck(SectionItemsRadioModel item, int secPrice, int price) {
                            if (secPrice ==3){

                            }else if (secPrice==1){
                                //total_price_tv.setText( String.valueOf(priceIntTotal-price));
                                total_price_tv.setText( Money.rupees(
                                        BigDecimal.valueOf(Long.valueOf(priceIntTotal-price
                                        ))).toString()
                                        + "     ");
                                priceIntTotal-=price;
                                //UImsgs.showToast(mContext,String.valueOf(priceInt+price) + "  REMOVE " +priceIntTotal +" REMOVE  " + price);
                            }else if (secPrice==2){
                                //total_price_tv.setText( String.valueOf(priceInt));
                            /*total_price_tv.setText( Money.rupees(
                                    BigDecimal.valueOf(Long.valueOf(price
                                    ))).toString()
                                    + "     ");*/
                            }

                        }
                    },secPrice);
                    holder.recyclerViewInner.setAdapter(sectionItemsModelInnerRecycler);
                }
        }else {
            //Check Box
            //Toast.makeText(mContext, "1", Toast.LENGTH_SHORT).show();
            //SectionItemsModelInnerRecycler sectionItemsModelInnerRecycler = new SectionItemsModelInnerRecycler(sectionItemsCheckModelsArrayListInner,false);
            String required = sectionItemsRadioModel.getRequired();
            if (sectionItemsCheckModelsArrayListInnerHashMap!=null)
                if (sectionItemsCheckModelsArrayListInnerHashMap.size()!=0) {
                    int secPrice = sectionItemsRadioModel.getSecPrice();
                    SectionItemsModelInnerRecycler sectionItemsModelInnerRecycler = new SectionItemsModelInnerRecycler(sectionItemsCheckModelsArrayListInnerHashMap, false, new SectionItemsModelInnerRecycler.OnItemCheckListener() {
                        @Override
                        public void onItemCheck(SectionItemsRadioModel item,int secPrice, int price) {
                            currentSelectedSectionItems.add(item);
                            if (secPrice ==3){

                            }else if (secPrice==1){
                                //total_price_tv.setText(String.valueOf(priceInt+price));
                                total_price_tv.setText(String.valueOf(priceIntTotal+price));
                                total_price_tv.setText(Money.rupees(
                                        BigDecimal.valueOf(Long.valueOf(priceIntTotal+price
                                        ))).toString()
                                        + "     ");
                                priceIntTotal+=price;
                                checkBoxPrice+=price;
                                //here
                                //UImsgs.showToast(mContext,String.valueOf(priceInt+price) + "  AND " +priceInt +" AND  " + price);
                            }else if (secPrice==2){
                                total_price_tv.setText( String.valueOf(price));
                            }
                        }

                        @Override
                        public void onItemUncheck(SectionItemsRadioModel item, int secPrice, int price) {
                            currentSelectedSectionItems.remove(item);

                            if (secPrice ==3){

                            }else if (secPrice==1){
                                //total_price_tv.setText( String.valueOf(priceIntTotal-price));
                                total_price_tv.setText( Money.rupees(
                                        BigDecimal.valueOf(Long.valueOf(priceIntTotal-price
                                        ))).toString()
                                        + "     ");
                                priceIntTotal-=price;
                                checkBoxPrice-=price;
                                //UImsgs.showToast(mContext,String.valueOf(priceInt+price) + "  AND " +priceIntTotal +" AND  " + price);
                            }else if (secPrice==2){
                                //total_price_tv.setText( String.valueOf(priceInt));
                                total_price_tv.setText( Money.rupees(
                                        BigDecimal.valueOf(Long.valueOf(priceInt
                                        ))).toString()
                                        + "     ");
                            }
                        }
                    },secPrice);
                    holder.recyclerViewInner.setAdapter(sectionItemsModelInnerRecycler);
                }
        }

        //Toast.makeText(mContext, sectionItemsCheckModelsArrayListInnerName.size()+"", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return sectionItemsCheckModelsArrayListOuter.size();
    }
    class ViewModel extends RecyclerView.ViewHolder{
        TextView foodSectionHeader;
        RecyclerView recyclerViewInner;
        public ViewModel(@NonNull View itemView) {
            super(itemView);
            foodSectionHeader = itemView.findViewById(R.id.food_section);
            recyclerViewInner = itemView.findViewById(R.id.recycle_inner);

            //Recycle View
            RecyclerView.LayoutManager mlayoutmanager=new LinearLayoutManager(mContext);
            recyclerViewInner.setHasFixedSize(true);
            recyclerViewInner.setLayoutManager(mlayoutmanager);
        }
    }
}
