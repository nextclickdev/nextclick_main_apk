package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.ViewDoctorBookingActivity;
import com.apticks.nextclickuser.Pojo.bookingsDoctorsHistoryResponse.DoctorBookingHistoryDetails;
import com.apticks.nextclickuser.R;

import java.util.LinkedList;

public class OnDemandServiceBookingHistoryFragmentAdapter extends RecyclerView.Adapter<OnDemandServiceBookingHistoryFragmentAdapter.ViewHolder> {

    private Context context;

    private LinkedList<DoctorBookingHistoryDetails> listOfDoctorBookingHistoryDetails;

    private int serviceId;

    public OnDemandServiceBookingHistoryFragmentAdapter(Context context, LinkedList<DoctorBookingHistoryDetails> listOfDoctorBookingHistoryDetails, int serviceId) {
        this.context = context;
        this.serviceId = serviceId;
        this.listOfDoctorBookingHistoryDetails = listOfDoctorBookingHistoryDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_doctors_booking_history_fragment_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DoctorBookingHistoryDetails doctorBookingHistoryDetails = listOfDoctorBookingHistoryDetails.get(position);
        if (doctorBookingHistoryDetails != null) {
            int trackId = doctorBookingHistoryDetails.getTrackId();
            int bookingStatus = doctorBookingHistoryDetails.getBookingStatus();
            String createdDate = doctorBookingHistoryDetails.getCreatedAt();
            if (bookingStatus == 0) {
                holder.tvBookingStatus.setText("Cancelled");
            } else if (bookingStatus == 1) {
                holder.tvBookingStatus.setText("Received");
            } else if (bookingStatus == 2) {
                holder.tvBookingStatus.setText("Accepted");
            } else if (bookingStatus == 3) {
                holder.tvBookingStatus.setText("Servicing");
            } else if (bookingStatus == 4) {
                holder.tvBookingStatus.setText("Completed");
            } else if (bookingStatus == 5) {
                holder.tvBookingStatus.setText("Rejected");
            }

            holder.tvBookingId.setText(String.valueOf(trackId));
            holder.tvCreatedDate.setText(createdDate);
        }
    }

    @Override
    public int getItemCount() {
        return listOfDoctorBookingHistoryDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvBookingId, tvBookingStatus, tvCancel, tvCreatedDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCancel = itemView.findViewById(R.id.tvCancel);
            tvBookingId = itemView.findViewById(R.id.tvBookingId);
            tvCreatedDate = itemView.findViewById(R.id.tvCreatedDate);
            tvBookingStatus = itemView.findViewById(R.id.tvBookingStatus);
            tvCancel.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            DoctorBookingHistoryDetails doctorBookingHistoryDetails = listOfDoctorBookingHistoryDetails.get(getLayoutPosition());
            if (id == R.id.tvCancel) {

            } else {
                int bookingId = doctorBookingHistoryDetails.getId();
                Intent viewDoctorBookingIntent = new Intent(context, ViewDoctorBookingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.booking_id), String.valueOf(bookingId));
                bundle.putString(context.getString(R.string.service_id), String.valueOf(serviceId));
                viewDoctorBookingIntent.putExtras(bundle);
                context.startActivity(viewDoctorBookingIntent);
            }
        }
    }
}