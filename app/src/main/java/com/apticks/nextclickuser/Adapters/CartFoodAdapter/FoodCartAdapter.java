package com.apticks.nextclickuser.Adapters.CartFoodAdapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Adapters.FoodSecItemsInnerAdapter;
import com.apticks.nextclickuser.Pojo.CartPojoModel.CartPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.SqliteDatabase.DataBaseHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.apticks.nextclickuser.Fragments.CartFoodFragments.FoodCartFragment.*;

public class FoodCartAdapter extends RecyclerView.Adapter<FoodCartAdapter.ViewHolder> {

    int count = 0;
    CartDBHelper cartDBHelper;
    DataBaseHelper dataBaseHelper;

    public List<CartPojo> data;
    LayoutInflater inflter;
    Context context;
    CartPojo cartPojo;
    OnItemQuatityChangeListenerPromo onItemQuatityChangeListenerPromo;

    TextView itemTotal, item_delivery_charge, item_item_grand_total_total, items_tax, items_count;
    int[] presentcountArray;
    int present_count = 0;
    int deliverCharge;


    public FoodCartAdapter(Context activity, TextView items_count, List<CartPojo> cartItems, TextView itemTotal, TextView item_delivery_charge,
                           int deliverCharge, TextView item_item_grand_total_total,
                           TextView items_tax, double taxOfVendor, @NonNull OnItemQuatityChangeListenerPromo onItemQuatityChangeListenerPromo) {
        this.context = activity;
        this.data = cartItems;
        this.items_count = items_count;
        this.itemTotal = itemTotal;
        this.items_tax = items_tax;
        this.item_delivery_charge = item_delivery_charge;
        this.deliverCharge = deliverCharge;
        this.item_item_grand_total_total = item_item_grand_total_total;
        this.onItemQuatityChangeListenerPromo = onItemQuatityChangeListenerPromo;
        presentcountArray = new int[cartItems.size()];
    }

    public interface OnItemQuatityChangeListenerPromo {
        void onItemCheck(String promoString);
    }


    @NonNull
    @Override
    public FoodCartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.foodcartitemsupporter, parent, false);
        cartDBHelper = new CartDBHelper(context);
        dataBaseHelper = new DataBaseHelper(context);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new FoodCartAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodCartAdapter.ViewHolder holder, int position) {
        cartPojo = data.get(position);
        ArrayList<CartPojo> secItems = new ArrayList<>();

        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(cartPojo.getName());
        holder.displayprice.setText(cartPojo.getPrice());
        holder.foodcartquantity.setText(cartPojo.getQuantity());
        holder.tvAvailableQuantity.setText(cartPojo.getQuantity());
        presentcountArray[position] = Integer.parseInt(cartPojo.getQuantity());
        Picasso.get().load(cartPojo.getImage()).into(holder.displayImage);

        secItems = cartPojo.getSecItems();

        if (secItems != null) {
            if (secItems.size() > 0) {
                FoodSecItemsInnerAdapter foodSecItemsInnerAdapter = new FoodSecItemsInnerAdapter(secItems);
                LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) {

                    @Override
                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(context) {

                            private static final float SPEED = 300f;
                            // Change this value (default=25f)

                            @Override
                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                return SPEED / displayMetrics.densityDpi;
                            }
                        };
                        smoothScroller.setTargetPosition(position);
                        startSmoothScroll(smoothScroller);
                    }
                };
                holder.recycler_sect_items.setLayoutManager(layoutManager);
                holder.recycler_sect_items.setAdapter(foodSecItemsInnerAdapter);
            }
            //Toast.makeText(context, "If " + secItems.size(), Toast.LENGTH_SHORT).show();

        } else {
            //Toast.makeText(context, "Else " + secItems.size(), Toast.LENGTH_SHORT).show();
        }
        /*holder.displayLocation.setText(restaurants.getLocation());*/

        //Toast.makeText(context, cartPojo.getPrice(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayprice, foodcartminus, foodcartplus, foodcartquantity, add, foodcart_sec_name, food_sec_price, tvAvailableQuantity;
        ImageView displayImage, remove_food_items_detail;
        LinearLayout item, linear_sec_items;
        private RecyclerView recycler_sect_items;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.foodcartitemimage);
            displayName = itemView.findViewById(R.id.foodcartItemname);
            displayprice = itemView.findViewById(R.id.foodcartprice);
            foodcartminus = itemView.findViewById(R.id.foodcartminus);
            foodcartplus = itemView.findViewById(R.id.foodcartplus);
            foodcartquantity = itemView.findViewById(R.id.foodcartquantity);
            tvAvailableQuantity = itemView.findViewById(R.id.tvAvailableQuantity);
            /*foodcart_sec_name = itemView.findViewById(R.id.foodcart_sec_name);
            food_sec_price = itemView.findViewById(R.id.food_sec_price);*/

            //Image view
            remove_food_items_detail = itemView.findViewById(R.id.remove_food_items_detail);

            //Linear Layout Sec
            linear_sec_items = itemView.findViewById(R.id.linear_sec_items);
            //discountfooditem = itemView.findViewById(R.id.addfooditem);
            //item = itemView.findViewById(R.id.item);

            //Recycler View
            recycler_sect_items = itemView.findViewById(R.id.recycler_sect_items);


            //remove Items From Cart Imag Click
            remove_food_items_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CartPojo cartPojo = data.get(getAdapterPosition());
                    cartDBHelper.deleteFoodItem(cartPojo.getId());
                    cartDBHelper.deleteSectCartItems(cartPojo.getId());
                    data.remove(getAdapterPosition());
                    items_count.setText(data.size() + "");
                    notifyDataSetChanged();

                    itemTotal.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
                    item_delivery_charge.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));

                    if (cartDBHelper.numberOfFoodCartRows() > 0) {

                        if (promoUsedStatus == 1) {
                            onItemQuatityChangeListenerPromo.onItemCheck("HERE");
                        } else {
                            if (walletUsedStatus == 1) {//When Used 1 Not used 0
                                if (selectAddresToggle) {
                                    double tempGrandTotal = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic;
                                    double totalwithTax = tempGrandTotal + Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount());
                                    double temptax = ((taxOfVendor) * (0.01)) * (dataBaseHelper.getTotalCartAmount());
                                    items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(totalwithTax));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(temptax);
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(temptax);
                                } else {
                                    double tempGrandTotal = dataBaseHelper.getTotalCartAmount();
                                    double totalwithTax = tempGrandTotal + Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount());
                                    double temptax = ((taxOfVendor) * (0.01)) * (dataBaseHelper.getTotalCartAmount());
                                    items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(totalwithTax));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(temptax);
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(temptax);
                                }
                            } else {
                                //IF Wallet Not Used
                                if (selectAddresToggle) {
                                    double tempGrandTotal = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic;
                                    double totalwithTax = tempGrandTotal + Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount());
                                    double temptax = ((taxOfVendor) * (0.01)) * (dataBaseHelper.getTotalCartAmount());
                                    items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(totalwithTax));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(temptax);
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(temptax);
                                } else {
                                    double tempGrandTotal = dataBaseHelper.getTotalCartAmount();
                                    double totalwithTax = tempGrandTotal + Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount());
                                    double temptax = ((taxOfVendor) * (0.01)) * (dataBaseHelper.getTotalCartAmount());
                                    items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(totalwithTax));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(temptax);
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(temptax);
                                }
                            }
                        }
                    } else {
                        //(Fragment)context.
                        ((Activity) itemView.getContext()).finish();
                    }
                }
            });

            foodcartminus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (presentcountArray[getAdapterPosition()] == 1) {
                        foodcartminus.setClickable(false);
                    } else {
                        foodcartminus.setClickable(true);
                        //present_count--;
                        presentcountArray[getAdapterPosition()] = --presentcountArray[getAdapterPosition()];
                        //foodcartquantity.setText(String.valueOf(present_count));
                        foodcartquantity.setText(String.valueOf(presentcountArray[getAdapterPosition()]));

                        CartPojo cartPojo = data.get(getAdapterPosition());
                        String idTemp = cartPojo.getId();
                        //cartDBHelper.updateFoodItemQuantity(idTemp,String.valueOf(present_count));
                        Log.d("qty_-_value", String.valueOf(presentcountArray[getAdapterPosition()]));

                        HashMap<String, String> currentSelectedItemsHashMap = new HashMap<>();

                        currentSelectedItemsHashMap.put("item_id", idTemp);
                        currentSelectedItemsHashMap.put("quantity", String.valueOf(presentcountArray[getAdapterPosition()]));
                        currentSelectedItemsHashMap.put("price", cartPojo.getPrice());
                        Log.e("price", "");

                        //Toast.makeText(mContext, "In ELSE", Toast.LENGTH_SHORT).show();

                        JSONObject json = new JSONObject(currentSelectedItemsHashMap);


                        cartDBHelper.updateFoodItemQuantity(idTemp, String.valueOf(presentcountArray[getAdapterPosition()]), json.toString());
                        double temptax = Math.ceil(((taxOfVendor) * (0.01)) * (dataBaseHelper.getTotalCartAmount()));
                        items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                        itemTotal.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
                        item_delivery_charge.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
                        //walletUsedStatus

                        if (promoUsedStatus == 1) {
                            onItemQuatityChangeListenerPromo.onItemCheck("HERE");
                        } else {
                            if (walletUsedStatus == 1) {//When Used 1 Not used 0
                                if (selectAddresToggle) {
                                    double tempGrandTotal = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic;

                                    double totalwithTax = tempGrandTotal + Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount());

                                    //items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(totalwithTax));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                } else {
                                    double tempGrandTotal = dataBaseHelper.getTotalCartAmount();
                                    double totalwithTax = tempGrandTotal + Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount());

                                    //items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(totalwithTax));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                }
                            } else {
                                //IF Wallet Not Used
                                if (selectAddresToggle) {
                                    double tempGrandTotal = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic;
                                    double totalwithTax = tempGrandTotal + Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount());

                                    //items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(totalwithTax));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                } else {
                                    double tempGrandTotal = dataBaseHelper.getTotalCartAmount();
                                    double totalwithTax = tempGrandTotal + Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount());

                                    //items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(totalwithTax));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                }
                            }
                        }
                    }

                }
            });

            foodcartplus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //present_count++;
                    //foodcartquantity.setText(String.valueOf(present_count));

                    presentcountArray[getAdapterPosition()] = ++presentcountArray[getAdapterPosition()];
                    foodcartquantity.setText(String.valueOf(presentcountArray[getAdapterPosition()]));
                    foodcartminus.setClickable(true);

                    CartPojo cartPojo = data.get(getAdapterPosition());
                    String idTemp = cartPojo.getId();
                    //cartDBHelper.updateFoodItemQuantity(idTemp,String.valueOf(present_count));
                    Log.d("qty_+_value", String.valueOf(presentcountArray[getAdapterPosition()]));
                    //Toast.makeText(context, String.valueOf(presentcountArray[getAdapterPosition()])+"", Toast.LENGTH_SHORT).show();


                    HashMap<String, String> currentSelectedItemsHashMap = new HashMap<>();

                    currentSelectedItemsHashMap.put("item_id", idTemp);
                    currentSelectedItemsHashMap.put("quantity", String.valueOf(presentcountArray[getAdapterPosition()]));
                    currentSelectedItemsHashMap.put("price", cartPojo.getPrice());
                    Log.e("price", "");

                    //Toast.makeText(mContext, "In ELSE", Toast.LENGTH_SHORT).show();

                    JSONObject json = new JSONObject(currentSelectedItemsHashMap);


                    cartDBHelper.updateFoodItemQuantity(idTemp, String.valueOf(presentcountArray[getAdapterPosition()]), json.toString());


                    Cursor c = cartDBHelper.getAllFoodItems();

                    if (c != null) {
                        if (c.moveToFirst()) {
                            do {
                                String dir = c.getString(c.getColumnIndex("foodItems"));
                                Log.d("dir_qty", dir);
                            } while (c.moveToNext());
                        }
                    }


                    double temptax = Math.ceil(((taxOfVendor) * (0.01)) * (dataBaseHelper.getTotalCartAmount()));
                    items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                    itemTotal.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));

                    if (promoUsedStatus == 1) {
                        if (walletUsedStatus == 1) {//When Used 1 Not used 0
                            if (selectAddresToggle) {
                                onItemQuatityChangeListenerPromo.onItemCheck("HERE");
                            }
                        } else {
                            onItemQuatityChangeListenerPromo.onItemCheck("HERE");
                        }
                    } else {

                        if (walletUsedStatus == 1) {//When Used 1 Not used 0

                            if (selectAddresToggle) {

                                //items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + temptax - promoDiscountValue /*deliverCharge*/));
                                grandTotalWithDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()))/*deliverCharge*/;
                                grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                            } else {

                                //items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + temptax - promoDiscountValue));
                                grandTotalWithDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()))/*deliverCharge*/;
                                grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() - promoDiscountValue + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                            }
                        } else {//When Wallet Not Used
                            {
                                if (selectAddresToggle) {

                                    //items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + temptax + deliverChargeStatic /*deliverCharge*/));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()))/*deliverCharge*/;
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                } else {

                                    //items_tax.setText(Math.ceil(temptax) + "(" + taxOfVendor + "%)");
                                    item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount() + temptax));
                                    grandTotalWithDel = dataBaseHelper.getTotalCartAmount() + deliverChargeStatic + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()))/*deliverCharge*/;
                                    grandTotalWithoutDel = dataBaseHelper.getTotalCartAmount() + Math.round(Math.ceil((taxOfVendor * 0.01) * dataBaseHelper.getTotalCartAmount()));
                                }
                            }
                        }
                    }
                    item_delivery_charge.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
                    /*if (deliverCharge>=0){
                        double tempGrandTotal = dataBaseHelper.getTotalCartAmount() +deliverCharge;
                        item_item_grand_total_total.setText(String.valueOf(tempGrandTotal));
                    }else{
                        item_item_grand_total_total.setText(String.valueOf(dataBaseHelper.getTotalCartAmount()));
                    }*/
                }
            });

        }
    }


    private boolean validateItemCount() {
        if (present_count <= 0) {
            return false;
        }
        return true;
    }


}