package com.apticks.nextclickuser.Adapters.Wallet;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.apticks.nextclickuser.Pojo.Wallet.Transactions;
import com.apticks.nextclickuser.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.MyViewHolder> {

    private  List<Transactions> moviesList;
    private Context mContext;

    public void setrefresh(ArrayList<Transactions> transactionlist) {
        this.moviesList=transactionlist;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, txnAmount, txnDate, txnRemark,txnType;
        private LinearLayout layout_background;

        MyViewHolder(View view) {
            super(view);

            txnType = (TextView) view.findViewById(R.id.txnType);
            txnRemark = (TextView) view.findViewById(R.id.txnRemark);
            txnDate = (TextView) view.findViewById(R.id.txnDate);
            txnAmount = (TextView) view.findViewById(R.id.txnAmount);
            layout_background =  view.findViewById(R.id.layout_background);
        }
    }


    public TransactionsAdapter(Context mContext, List<Transactions> moviesList) {
        this.moviesList = moviesList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transactions_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Transactions payment = moviesList.get(position);

        String[] spilt=payment.getCreated_at().split(" ");

        String date_after = formateDateFromstring("yyyy-MM-dd", "MMM, dd yyyy", spilt[0]);
        holder.txnDate.setText(date_after);


        holder.txnRemark.setText(payment.getTxn_id());

        holder.txnAmount.setText(payment.getAmount());
        holder.txnType.setText(payment.getType());

        if (payment.getType().equalsIgnoreCase("CREDIT")){
            holder.txnAmount.setTextColor(Color.parseColor("#70E86B"));
            holder.layout_background.setBackgroundColor(mContext.getResources().getColor(R.color.creditcolor));
        }else {
            holder.txnAmount.setTextColor(Color.parseColor("#B22222"));
            holder.layout_background.setBackgroundColor(mContext.getResources().getColor(R.color.debitcolor));
        }



      /*  Transactions transactions = moviesList.get(position);

        if(transactions.getTxnType().equalsIgnoreCase("DEBIT")){
            holder.txnType.setText(transactions.getTxnType());
            holder.txnType.setTextColor(Color.parseColor("#ff0000"));
            holder.txnAmount.setText("-"+transactions.getTxnAmount());
            holder.txnAmount.setTextColor(Color.parseColor("#ff0000"));
        } else if(transactions.getTxnType().equalsIgnoreCase("CREDIT")){
            holder.txnType.setText(transactions.getTxnType());
            holder.txnType.setTextColor(Color.parseColor("#00ff00"));
            holder.txnAmount.setText("+"+transactions.getTxnAmount());
            holder.txnAmount.setTextColor(Color.parseColor("#00ff00"));

        }
        if(transactions.getTxnRemark().equalsIgnoreCase("pending")){
            holder.txnRemark.setTextColor(Color.parseColor("#33358e"));
        } else  if(transactions.getTxnRemark().equalsIgnoreCase("success")){
            holder.txnRemark.setTextColor(Color.parseColor("#00ff00"));
        } else  if(transactions.getTxnRemark().equalsIgnoreCase("failed")){
            holder.txnRemark.setTextColor(Color.parseColor("#ff0000"));
        }
        holder.txnRemark.setText(transactions.getTxnRemark());
        holder.txnDate.setText(transactions.getTxnDate());
*/


        //Input date in String format
       /* String input = transactions.getTxnDate();
        //Date/time pattern of input date
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Date/time pattern of desired output date
        DateFormat outputformat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        Date date;
        String output = null;
        try {
            //Conversion of input String to date
            date = df.parse(input);
            //old date format to new date format
            output = outputformat.format(date);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        holder.txnDate.setText(output);
        holder.txnRemark.setText(transactions.getTxnRemark());
        CharSequence txnType = transactions.getTxnType();
        if (txnType.equals("1")) {
            holder.txnType.setText("CREDIT");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("+ ₹");
            stringBuilder.append(transactions.getTxnAmount());
            holder.txnAmount.setText(stringBuilder.toString());
        } else if (txnType.equals("0")) {
            holder.txnType.setText("DEBIT");
            holder.txnType.setTextColor(Color.parseColor("#ff0000"));

            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("- ₹");
            stringBuilder2.append((-1) * Integer.parseInt(transactions.getTxnAmount()));
            holder.txnAmount.setText(stringBuilder2.toString());
            holder.txnAmount.setTextColor(Color.parseColor("#ff0000"));
        }*/
    }
    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);
        } catch (ParseException e) {
            System.out.println("aaaaa exception  "+e.getMessage());
        }
        return outputDate;
    }
    @Override
    public int getItemCount() {
      //  return moviesList.size();
        return moviesList.size();
    }
}
