package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.OrderPojo.OrderTrackPojo;
import com.apticks.nextclickuser.R;

import java.util.List;

public class DialogTrackOrderAdapter extends RecyclerView.Adapter<DialogTrackOrderAdapter.ViewHolder> {
    List<OrderTrackPojo> orderTrackPojoList ;
    Context mContext;
    int order_status;

    public DialogTrackOrderAdapter(List<OrderTrackPojo> orderTrackPojoList, int order_status){
        this.orderTrackPojoList = orderTrackPojoList;
        this.order_status = order_status;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_track_order_adapter,parent,false);
        mContext = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderTrackPojo orderTrackPojo = orderTrackPojoList.get(position);


        holder.item_group.setText(orderTrackPojo.getName());


        if(order_status!=0 && order_status!=7) {
            if (orderTrackPojo.getId() <= order_status) {

                holder.item_group.setTextColor(Color.parseColor("#32CD32"));
                holder.item_group.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_done_green, 0, 0, 0);
            }
        }
            if(order_status==0 || order_status == 7){

                holder.item_group.setTextColor(Color.parseColor("#FF0000"));
                holder.item_group.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_order_close, 0, 0, 0);
            }
        /*if (orderTrackPojo.getStatus()==1){
            *//*holder.view_track.setBackgroundColor(mContext.getResources().getColor(R.color.GreenYellow));
            holder.pending_track_order.setVisibility(View.GONE);*//*
            holder.item_group.setTextColor(Color.parseColor("#32CD32"));
            holder.item_group.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_done_green,0,0,0);
        }*//*else if (orderTrackPojo.getStatus()==0){
            holder.view_track.setBackgroundColor(mContext.getResources().getColor(R.color.red));
            holder.pending_track_order.setVisibility(View.VISIBLE);
            holder.pending_track_order.setText(" Pending ");
        }*/
    }

    @Override
    public int getItemCount() {
        return orderTrackPojoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView item_group /*name_status,pending_track_order*/;
        /*ImageView tick_accept;
        RelativeLayout relative_tick;
        View view_track;*/
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_group = itemView.findViewById(R.id.item_group);
            /*name_status = itemView.findViewById(R.id.name_status);
            pending_track_order = itemView.findViewById(R.id.pending_track_order);
            tick_accept = itemView.findViewById(R.id.tick_accept);
            relative_tick = itemView.findViewById(R.id.relative_tick);
            view_track = itemView.findViewById(R.id.view_track);*/

        }
    }
}
