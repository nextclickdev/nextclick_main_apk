package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.FoodActivity;
import com.apticks.nextclickuser.Pojo.DisplayCategory;
import com.apticks.nextclickuser.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FoodVendorsListAdapters extends RecyclerView.Adapter<FoodVendorsListAdapters.ViewHolder> {

    List<DisplayCategory> data;

    LayoutInflater inflter;
    Context context;

    public FoodVendorsListAdapters(Context activity, List<DisplayCategory> completData) {
        this.context = activity;
        this.data = completData;
    }


    @NonNull
    @Override
    public FoodVendorsListAdapters.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.vendorscompletelist_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new FoodVendorsListAdapters.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodVendorsListAdapters.ViewHolder holder, int position) {
        DisplayCategory displayCategory = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(displayCategory.getName());
        Picasso.get().load(displayCategory.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(displayCategory.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayLocation;
        CircularImageView displayImage;
        LinearLayout listItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.completecatelistimage);
            displayName = itemView.findViewById(R.id.vendorslistname);
            displayLocation = itemView.findViewById(R.id.vendorslistlocation);
            listItem = itemView.findViewById(R.id.listitem);

            listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DisplayCategory displayCategory = data.get(getAdapterPosition());
                    String tempId = displayCategory.getId();

                    Intent intent = new Intent(context, FoodActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("hostelId",tempId);

                    context.startActivity(intent);
                }
            });


        }
    }
}

