package com.apticks.nextclickuser.Adapters;



/*
 * @author : sarath
 *
 * @desc : Gives complete vendors list
 *
 * */


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.FoodActivity;
import com.apticks.nextclickuser.Activities.HMS_ACTIVITIES.IndividualVendorActivity_HMS;
import com.apticks.nextclickuser.Pojo.DisplayCategory;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.ProductListActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class VendorsCompleteListAdapter extends RecyclerView.Adapter<VendorsCompleteListAdapter.ViewHolder> {

    List<DisplayCategory> data;

    LayoutInflater inflter;
    Context context;
    String id;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public VendorsCompleteListAdapter(Context activity, List<DisplayCategory> completData, String id) {
        this.context = activity;
        this.data = completData;
        this.id = id;
    }


    @NonNull
    @Override
    public VendorsCompleteListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //View itemView = LayoutInflater.from(context).inflate(R.layout.vendorscompletelist_supporter, parent, false);
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vendorscompletelist_supporter, parent, false);
            return new VendorsCompleteListAdapter.ViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new VendorsCompleteListAdapter.ViewHolder(itemView);
        }
        //return new VendorsCompleteListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorsCompleteListAdapter.ViewHolder holder, int position) {
        DisplayCategory displayCategory = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(displayCategory.getName());
        Picasso.get().load(displayCategory.getImage()).into(holder.displayImage);
        if (position % 2 == 0) {
            holder.listItem.setBackgroundColor(Color.parseColor("#E3E3E3"));
        }
        holder.displayLocation.setText(displayCategory.getLocation());


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayLocation;
        ImageView displayImage;
        LinearLayout listItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.completecatelistimage);
            displayName = itemView.findViewById(R.id.vendorslistname);
            displayLocation = itemView.findViewById(R.id.vendorslistlocation);
            listItem = itemView.findViewById(R.id.listitem);

            listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DisplayCategory displayCategory = data.get(getAdapterPosition());
                    String tempId = displayCategory.getId();

                    Intent intent; /*= new Intent(context, IndividualVendorActivity_HMS.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //intent.putExtra("hostelId",tempId);*/

                    if (id.equalsIgnoreCase("4")) {
                        intent = new Intent(context, ProductListActivity.class);
                       // intent = new Intent(context, FoodActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("nameReastaurant", displayCategory.getName());
                        intent.putExtra("vendor_user_id", displayCategory.getVendor_user_Id());

                    } else {
                        intent = new Intent(context, IndividualVendorActivity_HMS.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("vendorId", tempId);
                        // intent.putExtra("hostelId",tempId);
                    }

                    context.startActivity(intent);
                }
            });


        }
    }
}

