package com.apticks.nextclickuser.Adapters.UserHistory;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.ReceiptItemsAdapter;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.OrderHistory.OldOrderPojo;
import com.apticks.nextclickuser.Pojo.ReceiptItemPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.OrderInDetail;
import static com.apticks.nextclickuser.Config.Config.OrderRating;
import static com.apticks.nextclickuser.Constants.Constants.WARNING;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class OldOrderAdapter extends RecyclerView.Adapter<OldOrderAdapter.ViewHolder> {
    ArrayList<OldOrderPojo> oldOrderPojoArrayList;
    LayoutInflater inflter;
    private Context mContext;
    private Date oneWayTripDate;
    PreferenceManager preferenceManager;
    TextView order_no, ordered_from, timing, sub_total, delivery_charges, grand_total, item_text_tv, qty_text_tv, price_text_tv, tax;
    String token;
    int taxValue;

    ArrayList<ReceiptItemPojo> itemPojos;
    RecyclerView receipt_items_recycler;

    public OldOrderAdapter(Context applicationContext, ArrayList<OldOrderPojo> oldOrderPojoArrayList) {
        mContext = applicationContext;
        this.oldOrderPojoArrayList = oldOrderPojoArrayList;
        inflter = (LayoutInflater.from(applicationContext));
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
    }

    @NonNull
    @Override
    public OldOrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.old_order_outer_adapter, parent, false);
        return new OldOrderAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OldOrderAdapter.ViewHolder holder, int position) {
        holder.tv_discount.setText(oldOrderPojoArrayList.get(position).getDiscount() + "");
        holder.tv_delivery_fee.setText(oldOrderPojoArrayList.get(position).getDelivery_fee() + "");
        holder.tv_tax.setText(oldOrderPojoArrayList.get(position).getTax() + "");
        holder.tv_total.setText(oldOrderPojoArrayList.get(position).getTotal() + "");
        holder.tv_order_track.setText(oldOrderPojoArrayList.get(position).getOrder_track() + "");
        holder.tv_order_status.setText(oldOrderPojoArrayList.get(position).getOrder_status());
        holder.tv_vendor_name.setText(oldOrderPojoArrayList.get(position).getName());

        holder.tvVendorName.setText(oldOrderPojoArrayList.get(position).getName());
        holder.tvVendorMobileNumber.setText(oldOrderPojoArrayList.get(position).getVendorMobileNumber());
        holder.tvVendorEmail.setText(oldOrderPojoArrayList.get(position).getVendorEmailId());
        // holder.tv_created.setText(oldOrderPojoArrayList.get(position).getCreated_at());

        // Chnage Date Format comming from Server
        String date = oldOrderPojoArrayList.get(position).getCreated_at();
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd-MMMM-yyyy hh:mm:ss aa");
        try {
            // parse input
            oneWayTripDate = input.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tv_created.setText(output.format(oneWayTripDate));
        holder.tv_order_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View myView = LayoutInflater.from(mContext).inflate(R.layout.dialog_recepit, null);

                order_no = myView.findViewById(R.id.order_no);
                ordered_from = myView.findViewById(R.id.ordered_from);
                timing = myView.findViewById(R.id.timing);
                sub_total = myView.findViewById(R.id.sub_total);
                grand_total = myView.findViewById(R.id.grand_total);
                delivery_charges = myView.findViewById(R.id.delivery_charges);
                item_text_tv = myView.findViewById(R.id.item_name);
                qty_text_tv = myView.findViewById(R.id.item_quantity);
                price_text_tv = myView.findViewById(R.id.item_price);
                tax = myView.findViewById(R.id.tax);
                receipt_items_recycler = myView.findViewById(R.id.receipt_items_recycler);


                orderInDetail(oldOrderPojoArrayList.get(position).getId() + "".trim());

                AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
               /* alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle("Receipt");*/
                alert.setView(myView);
                alert.show();
            }
        });
        holder.tv_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewDialogOpener(oldOrderPojoArrayList.get(position).getId() + "".trim());
            }
        });

        holder.view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.view_details.getText().toString().trim().equalsIgnoreCase("View order Details")) {
                    holder.view_details.setText("Hide order Details");
                    holder.details_card.setVisibility(View.VISIBLE);
                } else {
                    holder.view_details.setText("View order Details");
                    holder.details_card.setVisibility(View.GONE);
                }
            }
        });

    }


    private void reviewDialogOpener(String orderId) {

        View alertLayout = LayoutInflater.from(mContext).inflate(R.layout.order_review_taking_layout, null);

        final EditText reviewData = alertLayout.findViewById(R.id.review);
        final EditText del_review = alertLayout.findViewById(R.id.del_review);
        final RatingBar reviewrating = alertLayout.findViewById(R.id.review_ratingBar);
        final RatingBar del_review_ratingBar = alertLayout.findViewById(R.id.del_review_ratingBar);
        final TextView submit = alertLayout.findViewById(R.id.review_submission);

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setTitle("NextClick");
        alert.setMessage("Please provide your review");
        AlertDialog dialog = alert.create();
        dialog.show();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reviewStr = reviewData.getText().toString().trim();
                String del_reviewStr = del_review.getText().toString().trim();
                float rating = reviewrating.getRating();
                float del_rating = del_review_ratingBar.getRating();
                if (reviewStr.length() > 2 && rating >= 1.0) {
                    Map<String, String> maptoparse = new HashMap<>();
                    maptoparse.put("order_id", orderId);
                    maptoparse.put("rating", rating + "".trim());
                    maptoparse.put("review", reviewStr);
                    maptoparse.put("del_rating", del_rating + "");
                    maptoparse.put("del_review", del_reviewStr);
                    JSONObject jsonObject = new JSONObject(maptoparse);
                    reviewSubmission(jsonObject, dialog);

                } else {
                    UImsgs.showCustomToast(mContext, "Please provide the data (Review & Rating)", WARNING);
                }
            }
        });
    }

    private void reviewSubmission(JSONObject dataObject, AlertDialog dialog) {
        final String dataStr = dataObject.toString();
        ProgressDialog progressDialog = new ProgressDialog(mContext);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please wait while submitting.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, OrderRating, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {
                        dialog.dismiss();
                        progressDialog.dismiss();
                        Toast.makeText(mContext, "Submitted Successfully", Toast.LENGTH_LONG).show();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(mContext, "Sorry for Inconvenience \n Please submit your review again", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    Toast.makeText(mContext, "Sorry for Inconvenience \n Please submit your review again", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                progressDialog.dismiss();
                Toast.makeText(mContext, "Sorry for Inconvenience \n Please submit your review again", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X_AUTH_TOKEN", token);
                params.put("content-type", "application/json");
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataStr == null ? null : dataStr.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private void orderInDetail(String orderId) {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        Log.d("old_order_url", OrderInDetail + orderId);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, OrderInDetail + orderId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("oldorder_detail", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject dataObject = jsonObject.getJSONObject("data");
                    order_no.setText(dataObject.getString("order_track"));
                    grand_total.setText(dataObject.getString("total"));
                    delivery_charges.setText(dataObject.getString("delivery_fee"));
                    taxValue = dataObject.getInt("tax");
                    timing.setText(dataObject.getString("created_at"));
                    try {
                        ordered_from.setText(dataObject.getJSONObject("vendor").getString("name"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String item_text = "", qty_text = "", price_text = "";
                    int price = 0;
                    itemPojos = new ArrayList<>();
                    try {
                        JSONArray order_items_array = dataObject.getJSONArray("order_items");

                        for (int i = 0; i < order_items_array.length(); i++) {

                            ReceiptItemPojo receiptItemPojo = new ReceiptItemPojo();

                            JSONObject orderObject = order_items_array.getJSONObject(i);
                            item_text = item_text + (i + 1) + "." + orderObject.getString("item_name").substring(0, 9) + "..." + "\n\n";
                            qty_text = qty_text + orderObject.getString("quantity") + "\n\n";
                            price = price + (Integer.parseInt(orderObject.getString("quantity")) * (Integer.parseInt(orderObject.getString("price"))));
                            price_text = price_text + (Integer.parseInt(orderObject.getString("price"))) + "\n\n";

                            receiptItemPojo.setSno(i + 1);
                            receiptItemPojo.setItem_name(orderObject.getString("item_name"));
                            receiptItemPojo.setQty(orderObject.getInt("quantity"));
                            receiptItemPojo.setValue((Integer.parseInt(orderObject.getString("quantity")) * (Integer.parseInt(orderObject.getString("price")))));

                            itemPojos.add(receiptItemPojo);


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONArray sub_order_items_array = dataObject.getJSONArray("sub_order_items");
                        for (int i = 0; i < sub_order_items_array.length(); i++) {
                            ReceiptItemPojo receiptItemPojo = new ReceiptItemPojo();
                            JSONObject orderObject = sub_order_items_array.getJSONObject(i);
                            item_text = item_text + i + 1 + "." + orderObject.getString("sec_item_name").substring(0, 9) + "..." + "\n\n";
                            qty_text = qty_text + orderObject.getString("quantity") + "\n\n";
                            price = price + (Integer.parseInt(orderObject.getString("quantity")) * (Integer.parseInt(orderObject.getString("price"))));
                            price_text = price_text + (Integer.parseInt(orderObject.getString("price"))) + "\n\n";

                            receiptItemPojo.setSno(itemPojos.size() + 1);
                            receiptItemPojo.setItem_name(orderObject.getString("item_name"));
                            receiptItemPojo.setQty(orderObject.getInt("quantity"));
                            receiptItemPojo.setValue((Integer.parseInt(orderObject.getString("quantity")) * (Integer.parseInt(orderObject.getString("price")))));

                            itemPojos.add(receiptItemPojo);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {

                        ReceiptItemsAdapter receiptItemsAdapter = new ReceiptItemsAdapter(mContext, itemPojos);

                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                            @Override
                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                    @Override
                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                        return SPEED / displayMetrics.densityDpi;
                                    }

                                };
                                smoothScroller.setTargetPosition(position);
                                startSmoothScroll(smoothScroller);
                            }

                        };
                        receipt_items_recycler.setLayoutManager(layoutManager);
                        receipt_items_recycler.setAdapter(receiptItemsAdapter);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    sub_total.setText(price + "".trim());
                    item_text_tv.setText(item_text);
                    tax.setText(Math.ceil(price * (0.01 * taxValue)) + "(" + taxValue + "%)");
                    qty_text_tv.setText(qty_text);
                    price_text_tv.setText(price_text);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    @Override
    public int getItemCount() {
        return oldOrderPojoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_discount, tv_vendor_name, tv_delivery_fee,
                tv_tax, tv_total, tv_order_track, tv_order_status,
                tv_order_receipt, tv_created, tv_rating, view_details, tvVendorName, tvVendorEmail, tvVendorMobileNumber;
        CardView details_card;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvVendorName = itemView.findViewById(R.id.tvVendorName);
            tvVendorEmail = itemView.findViewById(R.id.tvVendorEmail);
            tvVendorMobileNumber = itemView.findViewById(R.id.tvVendorMobileNumber);
            tv_discount = itemView.findViewById(R.id.tv_discount);
            tv_delivery_fee = itemView.findViewById(R.id.tv_delivery_fee);
            tv_tax = itemView.findViewById(R.id.tv_tax);
            tv_total = itemView.findViewById(R.id.tv_total);
            tv_order_track = itemView.findViewById(R.id.tv_order_track);
            tv_order_status = itemView.findViewById(R.id.tv_order_status);
            tv_vendor_name = itemView.findViewById(R.id.tv_vendor_name);
            tv_order_receipt = itemView.findViewById(R.id.tv_order_receipt);
            tv_created = itemView.findViewById(R.id.tv_created);
            tv_rating = itemView.findViewById(R.id.tv_rating);
            view_details = itemView.findViewById(R.id.view_details);
            details_card = itemView.findViewById(R.id.details_card);
            details_card.setVisibility(View.GONE);
        }
    }
}

