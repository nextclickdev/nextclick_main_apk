package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.AddToCartActivity;
import com.apticks.nextclickuser.Activities.FoodActivity;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.DisplayCategory;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.ProductListActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;

public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantsAdapter.ViewHolder> {

    List<DisplayCategory> data;

    LayoutInflater inflter;
    Context context;

    public RestaurantsAdapter(Context activity, List<DisplayCategory> restaurants) {
        this.context = activity;
        this.data = restaurants;
    }


    @NonNull
    @Override
    public RestaurantsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.individualcategorycompletelist, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new RestaurantsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantsAdapter.ViewHolder holder, int position) {
        DisplayCategory restaurants = data.get(position);
        holder.displayName.setText(restaurants.getName());
        Picasso.get().load(restaurants.getImage()).into(holder.displayImage);
        holder.displayLocation.setText(restaurants.getLocation_address());
        Log.e("sf", "" + restaurants.getVendor_user_Id());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayLocation;
        ImageView displayImage;
        LinearLayout item;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.displayimage);
            displayName = itemView.findViewById(R.id.displayname);
            displayLocation = itemView.findViewById(R.id.displaylocation);
            item = itemView.findViewById(R.id.item);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DisplayCategory selectedItem = data.get(getAdapterPosition());
                    int tempVendorId = selectedItem.getVendor_user_Id();
                    String tempNameRestaurant = selectedItem.getName();
                    int vender_user_id = selectedItem.getVendor_user_Id();

                    String sd = Integer.toString(vender_user_id);
                    Log.e("mahesh", "" +sd);

                    individualVendorDetails(tempVendorId);
                    //AddToCartActivity.vendorId = tempVendorId;
                  //  Intent intent = new Intent(context, FoodActivity.class);
                    Intent intent = new Intent(context, ProductListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    //intent.putExtra("hostelId",tempId);
                    intent.putExtra("nameReastaurant", tempNameRestaurant);
                    intent.putExtra("vendor_user_id", sd);

                    context.startActivity(intent);
                }
            });
        }

        private void individualVendorDetails(int tempVendorId) {
            {
                RequestQueue requestQueue = Volley.newRequestQueue(context);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + tempVendorId, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("individualVendorDetails", "" + response.toString());
                        if (response != null) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject dataObject = jsonObject.getJSONObject("data");

                                int vendor_user_id = dataObject.getInt("vendor_user_id");

                                String s = String.valueOf(vendor_user_id);
                                Log.e("vendor_user_id", "" + s);

                                AddToCartActivity.vendorId = vendor_user_id;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UImsgs.showToast(context, error.toString());
                        Log.d("error", error.toString());
                    }
                });
                requestQueue.add(stringRequest);
            }
        }
    }
}

