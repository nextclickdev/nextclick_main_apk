package com.apticks.nextclickuser.Adapters.GroceryAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.apticks.nextclickuser.Fragments.GroceryFragments.GroceryPlaceholderFragment;
import com.apticks.nextclickuser.Pojo.SubCategoryItemPojo;

import java.util.ArrayList;

public class GrocerryCategoryPagerAdapter extends FragmentPagerAdapter {
    public static ArrayList<SubCategoryItemPojo> groceryCategoriesList = new ArrayList<>();
    public GrocerryCategoryPagerAdapter(@NonNull FragmentManager fm) {
        //super(fm);
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        SubCategoryItemPojo ecomCategories = groceryCategoriesList.get(position);
        //foodMenuPojo.getId();
        return GroceryPlaceholderFragment.newInstance(position + 1,ecomCategories.getId());
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        SubCategoryItemPojo ecomCategories = groceryCategoriesList.get(position);
        ecomCategories.getName();
        return String.valueOf(ecomCategories.getName());
    }

    /*@NonNull
    @Override
    public Fragment getItem(int position) {
        return null;
    }*/

    @Override
    public int getCount() {
        return groceryCategoriesList.size();
    }
}
