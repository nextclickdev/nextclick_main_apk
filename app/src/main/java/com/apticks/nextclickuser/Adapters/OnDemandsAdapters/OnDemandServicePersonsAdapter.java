package com.apticks.nextclickuser.Adapters.OnDemandsAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.GetAppointmentAndGetServiceActivity;
import com.apticks.nextclickuser.Adapters.HospitalityAdapters.HospitalityDoctorsAdapter;
import com.apticks.nextclickuser.Pojo.HospitalityPojos.DoctorPojo;
import com.apticks.nextclickuser.Pojo.OnDemandsPojos.OnDemandServicePersonPojo;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;

public class OnDemandServicePersonsAdapter extends RecyclerView.Adapter<OnDemandServicePersonsAdapter.ViewHolder> {

    private Context context;

    private List<OnDemandServicePersonPojo> data;

    private String specialityId = "";
    private String serviceId = "";

    public OnDemandServicePersonsAdapter(Context activity, List<OnDemandServicePersonPojo> itemPojos, String serviceId, String ods_Id) {
        this.context = activity;
        this.data = itemPojos;
        this.serviceId = serviceId;
        this.specialityId = ods_Id;
    }


    @NonNull
    @Override
    public OnDemandServicePersonsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_ondemand_service_person_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new OnDemandServicePersonsAdapter.ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull OnDemandServicePersonsAdapter.ViewHolder holder, int position) {
        OnDemandServicePersonPojo onDemandServicePersonPojo = data.get(position);

        holder.ods_person_name.setText(onDemandServicePersonPojo.getName());
        holder.ods_duration.setText(onDemandServicePersonPojo.getService_duration() + " day(s)");
        holder.ods_category.setText(onDemandServicePersonPojo.getOd_category_name());
        try {

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < onDemandServicePersonPojo.getService_timing_start().size(); i++) {
                stringBuilder.append(onDemandServicePersonPojo.getService_timing_start().get(i) + " - " + onDemandServicePersonPojo.getService_timing_end().get(i)).append("\n");
            }
            holder.ods_timings.setText(stringBuilder.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        int discount = onDemandServicePersonPojo.getDiscount();
        double discountPrice = onDemandServicePersonPojo.getPrice() * (discount / 100.00);

        try {
            holder.ods_discount.setText("₹" + onDemandServicePersonPojo.getPrice());
            holder.ods_fee.setText("₹" + (onDemandServicePersonPojo.getPrice() - discountPrice));
        } catch (Exception e) {
            e.printStackTrace();
            holder.ods_discount.setText("₹" + "0");
            holder.ods_fee.setText("₹" + (onDemandServicePersonPojo.getPrice()));
        }
        Picasso.get()
                .load(onDemandServicePersonPojo.getImage())
                /*.networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)*/
                .placeholder(R.drawable.loader_gif)
                .into(holder.ods_person_image);


        holder.get_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GetAppointmentAndGetServiceActivity.class);
                intent.putExtra(VENDOR_ID, onDemandServicePersonPojo.getVendor_id());
                intent.putExtra("od_cat_id", specialityId);
                intent.putExtra("s_id", onDemandServicePersonPojo.getId());
                intent.putExtra(context.getString(R.string.service_id), serviceId);
                intent.putExtra("type", context.getString(R.string.on_demand_services));
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView ods_person_name, ods_duration, ods_discount, ods_fee,
                ods_category, ods_timings;
        ImageView ods_person_image;
        CardView get_service;

        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ods_person_name = itemView.findViewById(R.id.ods_person_name);
            ods_duration = itemView.findViewById(R.id.ods_duration);
            ods_discount = itemView.findViewById(R.id.ods_discount);
            ods_fee = itemView.findViewById(R.id.ods_fee);
            ods_category = itemView.findViewById(R.id.ods_category);
            ods_timings = itemView.findViewById(R.id.ods_timings);
            ods_person_image = itemView.findViewById(R.id.ods_person_image);
            get_service = itemView.findViewById(R.id.get_service);


            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }
}

