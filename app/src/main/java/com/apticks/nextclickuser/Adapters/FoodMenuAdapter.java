package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.apticks.nextclickuser.Pojo.FoodMenuPojo;
import com.apticks.nextclickuser.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

public class FoodMenuAdapter  extends ArrayAdapter {

    List<FoodMenuPojo> data;

    LayoutInflater inflter;
    Context context;

    public FoodMenuAdapter(@NonNull Context context, int resource,List<FoodMenuPojo> completData) {
        super(context, resource,completData);
        this.context = context;
        this.data = completData;
    }


    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.menuitemsupporter, null);
        }


        TextView itemName, displayLocation;
        CircularImageView itemImage;
        //itemImage = v.findViewById(R.id.fashionItemImage);
        itemName = v.findViewById(R.id.menu);
        FoodMenuPojo foodMenuPojo = data.get(position);

        itemName.setText(foodMenuPojo.getName());
        //Picasso.get().load(foodMenuPojo.getImage()).into(itemImage);


        return v;

    }
}



/* extends RecyclerView.Adapter<FoodMenuAdapter.ViewHolder> {

    List<FoodMenuPojo> dataArrayList;

    LayoutInflater inflter;
    Context context;
    CustomItemClickListener listener;

    public FoodMenuAdapter(Context activity, List<FoodMenuPojo> menuItems) {
        this.context = activity;
        this.dataArrayList = menuItems;
    }


    @NonNull
    @Override
    public FoodMenuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.menuitemsupporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        final ViewHolder mViewHolder = new ViewHolder(itemView);


        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FoodMenuAdapter.ViewHolder holder, int position) {
        FoodMenuPojo foodMenuPojo = dataArrayList.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(foodMenuPojo.getName());
        //holder.displayprice.setText(foodMenuPojo.getPrice());
        //Picasso.get().load(foodMenuPojo.getImage()).into(holder.displayImage_blurr);
        *//*holder.displayLocation.setText(restaurants.getLocation());*//*


    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayprice;
        ImageView displayImage_blurr;
        LinearLayout item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //displayImage_blurr = itemView.findViewById(R.id.fooditemImage);
            displayName = itemView.findViewById(R.id.menu);
           *//* displayName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FoodMenuPojo foodMenuPojo = dataArrayList.get(getAdapterPosition());

                    String id = foodMenuPojo.getId();
                    Toast.makeText(context, id, Toast.LENGTH_SHORT).show();

                }
            });*//*
            //displayprice = itemView.findViewById(R.id.fooditemPrice);
            //item = itemView.findViewById(R.id.item);


            *//*item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    FoodItemPojo selectedItem = dataArrayList.get(getAdapterPosition());
                    String tempId = selectedItem.getId();

                    Intent intent = new Intent(context, FoodActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("hostelId",tempId);

                    context.startActivity(intent);
                }
            });*//*


        }
    }
}*/