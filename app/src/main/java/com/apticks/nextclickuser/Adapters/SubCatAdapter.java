package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.VENDORS.VendorsActivity;
import com.apticks.nextclickuser.Helpers.TimeStamp;
import com.apticks.nextclickuser.Pojo.SubCategoryItemPojo;
import com.apticks.nextclickuser.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SubCatAdapter extends RecyclerView.Adapter<SubCatAdapter.ViewHolder> {

    List<SubCategoryItemPojo> data;

    LayoutInflater inflter;
    Context context;
    int isHavingLeadManagement;

    public SubCatAdapter(Context activity, List<SubCategoryItemPojo> itemPojos, int isHavingLeadManagement) {
        this.context = activity;
        this.data = itemPojos;
        this.isHavingLeadManagement = isHavingLeadManagement;

    }

    @NonNull
    @Override
    public SubCatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.sub_cat_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new SubCatAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCatAdapter.ViewHolder holder, int position) {
        SubCategoryItemPojo subCategoryItemPojo = data.get(position);
        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();
        holder.displayName.setText(subCategoryItemPojo.getName());
        TimeStamp timeStamp = new TimeStamp();
        Glide.with(context)
                .load(subCategoryItemPojo.getImage()+timeStamp.timeStampFetcher())
                .placeholder(R.drawable.noimage)
                .into(holder.displayImage);
       /* Picasso.get()
                .load(subCategoryItemPojo.getImage()+timeStamp.timeStampFetcher())
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(holder.displayImage);*/
        /*holder.displayLocation.setText(hospital.getLocation());*/

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, VendorsActivity.class);
                intent.putExtra("sub_cat_id",subCategoryItemPojo.getId());//need to pass sub category id
                intent.putExtra("cat_id",subCategoryItemPojo.getCat_id());//need to pass sub category id
                intent.putExtra("lead",isHavingLeadManagement+"".trim());//need to pass sub category id
                //sub_cat_id = subCategoryItemPojo.getId();
                context.startActivity(intent);
            }
        });


    }
    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayLocation;
        ImageView displayImage;
        CardView item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.allcategoryimage);
            displayName = itemView.findViewById(R.id.allcategoryname);
            item = itemView.findViewById(R.id.sub_catitem);

        }
    }
}
