package com.apticks.nextclickuser.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.SectionItemsRadioModel;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;

public class SectionItemsRadioAdapter extends RecyclerView.Adapter<SectionItemsRadioAdapter.ViewModels> {
    ArrayList<SectionItemsRadioModel> sectionItemsRadioModelsArrayList = new ArrayList<>();
    Boolean aBoolean;
    public SectionItemsRadioAdapter(ArrayList<SectionItemsRadioModel> sectionItemsRadioModelsArrayList,Boolean aBoolean) {
        this.sectionItemsRadioModelsArrayList = sectionItemsRadioModelsArrayList;
        this.aBoolean = aBoolean;
    }

    @NonNull
    @Override
    public ViewModels onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate( R.layout.recycle_section_radio,parent,false);

        return new SectionItemsRadioAdapter.ViewModels(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewModels holder, int position) {
        SectionItemsRadioModel sectionItemsRadioModel = sectionItemsRadioModelsArrayList.get(position);
        //sectionItemsRadioModel.getName();
        holder.title_name.setText(sectionItemsRadioModel.getName());
        holder.title_price.setText(sectionItemsRadioModel.getPrice());


    }

    @Override
    public int getItemCount() {
        return sectionItemsRadioModelsArrayList.size();
    }

    public class ViewModels extends RecyclerView.ViewHolder {
        TextView title_name,title_price;
        RadioButton radioButton;
        CheckBox checkSelect;
        public ViewModels(@NonNull View itemView) {
            super(itemView);
            title_name = itemView.findViewById(R.id.title_name);
            title_price = itemView.findViewById(R.id.description_price);
            radioButton = itemView.findViewById(R.id.radio_select);
            checkSelect = itemView.findViewById(R.id.check_select);
            if (aBoolean) {
                radioButton.setVisibility(View.VISIBLE);
                checkSelect.setVisibility(View.GONE);
            }else {
                radioButton.setVisibility(View.GONE);
                checkSelect.setVisibility(View.VISIBLE);
            }

        }
    }
}
