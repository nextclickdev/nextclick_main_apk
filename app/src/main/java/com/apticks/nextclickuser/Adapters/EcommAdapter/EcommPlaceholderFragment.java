package com.apticks.nextclickuser.Adapters.EcommAdapter;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Adapters.ProductListAdapter;
import com.apticks.nextclickuser.Fragments.PageViewModel;
import com.apticks.nextclickuser.Pojo.ProductsPojo;
import com.apticks.nextclickuser.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apticks.nextclickuser.Config.Config.ECOM_PRODUCTS_BASED_ON_SUBCATEGORY;

public class EcommPlaceholderFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;
    View root;

    Context mContext;
    String idTemp;

    ArrayList<ProductsPojo> productList = new ArrayList<>();

    ImageView fashionImageEcomFragment;
    TextView fashionTextEcomFashion;
    RecyclerView productsrecycler;

    public static EcommPlaceholderFragment newInstance(int index, String id) {

        EcommPlaceholderFragment fragment = new EcommPlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        bundle.putString("id", id);
        fragment.setArguments(bundle);
        //fragment.id = id;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.ecomm_place_holder_fragment, container,false);//Change Layout
        //return super.onCreateView(inflater, container, savedInstanceState);
        initView();

        mContext = getActivity();
        idTemp = getArguments().getString("id");

        tabRecycleView(idTemp);
        return root ;
    }

    private void tabRecycleView(String idTemp) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ECOM_PRODUCTS_BASED_ON_SUBCATEGORY +idTemp, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataJson = jsonObject.getJSONObject("data");
                        JSONArray resultArray = dataJson.getJSONArray("result");
                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                JSONObject resultObject = resultArray.getJSONObject(i);
                                String id = resultObject.getString("id");
                                String name = resultObject.getString("name");
                                String mrp = resultObject.getString("mrp");
                                String offer_price = resultObject.getString("offer_price");
                                String image = resultObject.getString("image");

                                ProductsPojo product = new ProductsPojo();

                                product.setId(id);
                                product.setName(name);
                                product.setMrp(mrp);
                                product.setOffer_price(offer_price);
                                product.setImage(image);
                                productList.add(product);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();

                    }
                    ProductListAdapter productListAdapter = new ProductListAdapter(mContext, productList);
                    GridLayoutManager layoutManager = new GridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false) {

                        @Override
                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                private static final float SPEED = 300f;// Change this value (default=25f)

                                @Override
                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                    return SPEED / displayMetrics.densityDpi;
                                }

                            };
                            smoothScroller.setTargetPosition(position);
                            startSmoothScroll(smoothScroller);
                        }

                    };
                    productsrecycler.setLayoutManager(layoutManager);
                    productsrecycler.setAdapter(productListAdapter);
                }
            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void initView() {
        /*fashionImageEcomFragment = root.findViewById(R.id.fashion_image_ecom_fragment);
        fashionTextEcomFashion*/
        productsrecycler = (RecyclerView) root.findViewById(R.id.ecom_sub_products_recycler);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }
}
