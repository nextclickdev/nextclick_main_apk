package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.apticks.nextclickuser.Pojo.CartChildModel;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;

public class FoodSectionsAdapterTemp extends ArrayAdapter {
    ArrayList<CartChildModel> listChildData= new ArrayList<>();
    Context mContext;
    public FoodSectionsAdapterTemp(@NonNull Context context, int resource,ArrayList<CartChildModel> listChildData) {
        super(context, resource);
        this.mContext = context;
        this.listChildData = listChildData;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_item_add_to_cart_child, null);
        }
        TextView radio_btn_item_name =v.findViewById(R.id.radio_btn_item_name);
        RadioButton radio_btn = v.findViewById(R.id.radio_btn);
        CheckBox check_btn = v.findViewById(R.id.check_btn);

        CartChildModel cartChildModel = listChildData.get(position);
        //radio_btn_item_name.setText(cartChildModel.getChild_item_name());

        if (cartChildModel.getSymbol().equals("1")){
            check_btn.setVisibility(View.GONE);
            radio_btn.setVisibility(View.VISIBLE);
        }else if (cartChildModel.getSymbol().equals("2")){
            radio_btn.setVisibility(View.GONE);
            check_btn.setVisibility(View.VISIBLE);
        }

        return v;
    }

    @Override
    public int getCount() {
        return listChildData.size();
    }
}
