package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.BookingTimePojo;
import com.apticks.nextclickuser.Pojo.CategoriesData;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BookingTimeAdapter extends RecyclerView.Adapter<BookingTimeAdapter.ViewHolder> {

    List<BookingTimePojo> data;

    LayoutInflater inflter;
    Context context;
    private int mSelectedItem = -1;

    public BookingTimeAdapter(Context activity, List<BookingTimePojo> completData) {
        this.context = activity;
        this.data = completData;
    }


    @NonNull
    @Override
    public BookingTimeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_booking_time, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new BookingTimeAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BookingTimeAdapter.ViewHolder holder, int position) {

        BookingTimePojo bookingTimePojo = data.get(position);

        holder.time_radio.setChecked(position == mSelectedItem);
        holder.time_string.setText(bookingTimePojo.getStart_time()+"-"+bookingTimePojo.getEnd_time());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView time_string;
        RadioButton time_radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            time_string = itemView.findViewById(R.id.time_string);
            time_radio = itemView.findViewById(R.id.time_radio);

            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                }
            };
            time_radio.setOnClickListener(onClickListener);
            time_string.setOnClickListener(onClickListener);



        }
    }
}

