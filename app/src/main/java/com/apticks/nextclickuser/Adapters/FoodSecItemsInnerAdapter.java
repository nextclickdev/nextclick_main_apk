package com.apticks.nextclickuser.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.CartPojoModel.CartPojo;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;

public class FoodSecItemsInnerAdapter extends RecyclerView.Adapter<FoodSecItemsInnerAdapter.ViewHolder> {
    ArrayList<CartPojo> secItems = new ArrayList<>();
    public FoodSecItemsInnerAdapter(ArrayList<CartPojo> secItems){
        this.secItems = secItems;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_sec_inner_adapter, parent,false);

        //return null;
        return new FoodSecItemsInnerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CartPojo cartPojo = secItems.get(position);

        holder.foodcart_sec_name.setText(cartPojo.getName());
        holder.food_sec_price.setText(cartPojo.getPrice());
    }

    @Override
    public int getItemCount() {
        return secItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView foodcart_sec_name,food_sec_price;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            foodcart_sec_name = itemView.findViewById(R.id.foodcart_sec_name);
            food_sec_price = itemView.findViewById(R.id.food_sec_price);
        }
    }
}
