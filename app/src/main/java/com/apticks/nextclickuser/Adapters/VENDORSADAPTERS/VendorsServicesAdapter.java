package com.apticks.nextclickuser.Adapters.VENDORSADAPTERS;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;
import java.util.List;

public class VendorsServicesAdapter extends RecyclerView.Adapter<VendorsServicesAdapter.ViewHolder>   {

    List<ServicesPojo> data;

    Context context;
    int firstdisplay=0;
    boolean firstdisplaybool;

    public VendorsServicesAdapter(Context context, ArrayList<ServicesPojo> serviceslist) {
        this.context = context;
        this.data = serviceslist;

    }


    @NonNull
    @Override
    public VendorsServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.vendor_services, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new VendorsServicesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorsServicesAdapter.ViewHolder holder, int position) {
        ServicesPojo servicesPojo = data.get(position);

        holder.service_name.setText(servicesPojo.getName());

        holder.service_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();

    }

    @Override
    public int getItemViewType(int position) {return position;}

    public void setrefresh(ArrayList<ServicesPojo> serviceList) {
        this.data.clear();
        this.data=serviceList;
        System.out.println("aaaaaaaaa  data size  "+data.size()+"  "+serviceList.size());
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView service_name;

        CardView item_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
          //  item_layout = itemView.findViewById(R.id.item_layout);
            service_name = itemView.findViewById(R.id.service_name);
        }
    }

}
