package com.apticks.nextclickuser.Adapters.VENDORSADAPTERS;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.FoodActivity;
import com.apticks.nextclickuser.Pojo.VendorSubCatPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.products.activities.ProductListActivity;
import com.bumptech.glide.Glide;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.apticks.nextclickuser.Constants.Constants.ADDRESS;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;

public class VendorSubCatAdapter extends RecyclerView.Adapter<VendorSubCatAdapter.ViewHolder>  {

    List<VendorSubCatPojo> data;

    LayoutInflater inflter;
    Context context;
    String vendor_user_id;



    public VendorSubCatAdapter(Context activity, List<VendorSubCatPojo> itemPojos,String vendor_user_id) {
        this.context = activity;
        this.data = itemPojos;
        this.vendor_user_id = vendor_user_id;

    }

    @NonNull
    @Override
    public VendorSubCatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.vendor_sub_cat_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new VendorSubCatAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorSubCatAdapter.ViewHolder holder, int position) {
        VendorSubCatPojo vendorSubCatPojo = data.get(position);

        System.out.println("aaaaaaaaa vendor_user_id "+vendor_user_id);
        holder.sub_cat_name.setText(vendorSubCatPojo.getName());
        Glide.with(context)
                .load(vendorSubCatPojo.getImage())
                .placeholder(R.drawable.noimage)
                .into(holder.sub_cat_image);

     /*   Picasso.get()
                .load(vendorSubCatPojo.getImage())
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(holder.sub_cat_image);*/

        /*holder.vendor_location_map;*/

       /* holder.vendor_location_map.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {


                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {

                        // Map is set up and the style has loaded. Now you can add data or make other map adjustments
                    }
                });
            }
        });*/
        holder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Intent intent = new Intent(context, FoodActivity.class);
                Intent intent = new Intent(context, ProductListActivity.class);
                intent.putExtra(reastaurantName, vendorSubCatPojo.getVendor_name());
                intent.putExtra("vendor_user_id", vendor_user_id);
               // Log.d("vendor_user_id", vendor_user_id);
                System.out.println("aaaaaa vendorid  "+vendor_user_id);
                intent.putExtra(ADDRESS, vendorSubCatPojo.getLocationModel().getAddress());
                intent.putExtra("sub_cat_id", vendorSubCatPojo.getId());
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {return position;}

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sub_cat_name;
        ImageView sub_cat_image;
        LinearLayout item_layout;
        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sub_cat_name = itemView.findViewById(R.id.sub_cat_name);
            sub_cat_image = itemView.findViewById(R.id.sub_cat_image);
            item_layout = itemView.findViewById(R.id.item_layout);

            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }
}
