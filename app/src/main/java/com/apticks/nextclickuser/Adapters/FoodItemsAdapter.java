package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.apticks.nextclickuser.Activities.AddToCartActivity;
import com.apticks.nextclickuser.Activities.addtocart.AddToCartActivityNew;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.FoodItemPojo;
import com.apticks.nextclickuser.Pojo.SectionItemsRadioModel;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.SqliteDatabase.DataBaseHelper;
import com.apticks.nextclickuser.utilities.BlurImageView;
import com.apticks.nextclickuser.utilities.Money;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import static com.apticks.nextclickuser.Activities.AddToCartActivity.vendorId;
import static com.apticks.nextclickuser.Constants.CategoriesId.*;
import static com.apticks.nextclickuser.Constants.Constants.INFO;


public class FoodItemsAdapter extends RecyclerView.Adapter<FoodItemsAdapter.ViewHolder> {


    public static int ADDED = 1, NOT_ADDED = 2;
    public static int ITEM_IS = NOT_ADDED;
    CartDBHelper cartDBHelper;
    DataBaseHelper dataBaseHelper;

    List<FoodItemPojo> data;
    LayoutInflater inflter;
    Context context;

    boolean listGridIntSpanCount;
    OnItemAddListener onItemAddListener;

    public FoodItemsAdapter(Context activity, List<FoodItemPojo> restaurants,
                            @NonNull OnItemAddListener onItemAddListener) {
        this.context = activity;
        this.data = restaurants;
        this.onItemAddListener = onItemAddListener;
        cartDBHelper = new CartDBHelper(context);
        dataBaseHelper = new DataBaseHelper(context);
    }

    public FoodItemsAdapter(Context activity, List<FoodItemPojo> restaurants) {
        this.context = activity;
        this.data = restaurants;
        cartDBHelper = new CartDBHelper(context);
    }


    @NonNull
    @Override
    public FoodItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        listGridIntSpanCount = listGrid;

        if (listGrid && beautySpa) {
            itemView = LayoutInflater.from(context).inflate(R.layout.fooditemsupporter_list_beauty, parent, false);
        } else if (listGrid) {
            itemView = LayoutInflater.from(context).inflate(R.layout.fooditemsupporter_list, parent, false);
        } else {
            itemView = LayoutInflater.from(context).inflate(R.layout.fooditemsupporter_grid, parent, false);
        }

        //context.setTheme(R.style.Theme_Transparent);

        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new FoodItemsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodItemsAdapter.ViewHolder holder, int position) {
        FoodItemPojo /*foodItemPojo;*/foodItemPojo = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(foodItemPojo.getName());//Food Name
        if (foodItemPojo.getDiscount() != 0) {//Checking Food Discount
            holder.ruppee_symbol.setVisibility(View.GONE);
            String sellCostString = Money.rupees(BigDecimal.valueOf(foodItemPojo.getDiscountPrice())).toString() + "     ";

            Log.d("sellcost", foodItemPojo.getDiscountPrice() + "");
            Log.d("sellcostB", sellCostString);
            String buyMRP = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(foodItemPojo.getPrice()
                    ))).toString();

            Log.d("buyMRP", foodItemPojo.getPrice() + "");
            Log.d("buyMRP_B", buyMRP);

            String costString = sellCostString + buyMRP;

            holder.displayprice.setText(costString, TextView.BufferType.SPANNABLE);

            Spannable spannable = (Spannable) holder.displayprice.getText();
            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);

            //Spannable wordtoSpan = new SpannableString(sellCostString);

            spannable.setSpan(new ForegroundColorSpan(Color.BLACK), 0, sellCostString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannable.setSpan(boldSpan, 0, sellCostString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


            spannable.setSpan(new StrikethroughSpan(), sellCostString.length(),
                    costString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            holder.discountfooditem.setText(foodItemPojo.getDiscount() + " % ");
        } else {
            holder.displayprice.setText(foodItemPojo.getPrice());//Food Price
            holder.discountfooditem.setText(foodItemPojo.getDiscount() + " % ");
            holder.discountfooditem.setVisibility(View.GONE);
        }

        //holder.displayprice.setText(foodItemPojo.getPrice());//Food Price
        //holder.fooditemdesc.setText(foodItemPojo.getDesc());//Description
        holder.fooditemdesc.setText(Html.fromHtml(foodItemPojo.getDesc()).toString());//Description

        Picasso.get().load(foodItemPojo.getImage()).placeholder(R.drawable.loader_gif).into(holder.displayImage_blurr);//Food Image
        Picasso.get().load(foodItemPojo.getImage()).placeholder(R.drawable.loader_gif).into(holder.displayImage_clear);//Food Image
        //Picasso.get().load(foodItemPojo.getImage()).transform((Transformation) new BlurTransformation(context)).into(holder.displayImage_blurr);//Food Image
           /*Glide.with(context)
                    .load(foodItemPojo.getImage())
                    .asBitmap()
                    .transform(new BlurTransformation(context))
                    .into(holder.displayImage_blurr);*/
        // from View
        //Blurry.with(context).capture(itemView).into(holder.displayImage_blurr);

        if (foodItemPojo.getItemType() == 2) {
            holder.foodItemType.setImageResource(R.drawable.non_veg);
        } else {
            holder.foodItemType.setImageResource(R.drawable.veg);
        }

        /*holder.displayLocation.setText(restaurants.getLocation());*/
        if (foodItemPojo.getStatus() == 1) {
            //(Activity)getC.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            //context.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            //context.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            holder.displayImage_clear.setVisibility(View.VISIBLE);
            holder.displayImage_blurr.setVisibility(View.GONE);
            holder.card_food_item_adapter.setEnabled(true);
            holder.displayImage_blurr.setEnabled(true);
            holder.button_add_item.setEnabled(true);

        } else {
            holder.displayImage_clear.setVisibility(View.GONE);
            holder.displayImage_blurr.setVisibility(View.VISIBLE);
            holder.card_food_item_adapter.setEnabled(false);
            holder.displayImage_blurr.setEnabled(false);
            //holder.button_add_item.setEnabled(false);

        }

        if (dataBaseHelper.getItemIdPresence(Integer.parseInt(foodItemPojo.getId()))) {
            holder.button_add_item.setVisibility(View.GONE);
            holder.button_remove_item.setVisibility(View.VISIBLE);
        } else {
            holder.button_add_item.setVisibility(View.VISIBLE);
            holder.button_remove_item.setVisibility(View.GONE);
        }

        holder.button_add_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (foodItemPojo.getStatus() == 1) {

                    FoodItemPojo tempFoodItemPojo = data.get(position);
                    if (tempFoodItemPojo.getSectionStatus() == 0) {

                        if (dataBaseHelper.getVendorIdPresence(vendorId)) {
                            if (cartDBHelper.insertFoodCart(tempFoodItemPojo.getId(), vendorId, tempFoodItemPojo.getName(), tempFoodItemPojo.getImage(),
                                    //priceString,
                                    checkDiscountPrice(tempFoodItemPojo.getPrice(), foodItemPojo.getDiscount(), position),
                                    String.valueOf("1"),
                                    "",
                                    currentSelectedItemsHashMap(tempFoodItemPojo), "", tempFoodItemPojo.getQuantity())) {

                                onItemAddListener.onItemAdded(true);
                                onItemAddListener.onItemUncheck(tempFoodItemPojo.getPrice(), foodItemPojo.getDiscount(), position,
                                        String.valueOf("1"),
                                        "",
                                        currentSelectedItemsHashMap(tempFoodItemPojo), "", tempFoodItemPojo.getQuantity());

                                holder.button_remove_item.setVisibility(View.VISIBLE);
                                holder.button_add_item.setVisibility(View.GONE);

                            } else {
                                UImsgs.showToast(context, "Some Error Occurred");
                            }
                        }
                    } else {
                        String tempfoodItemPojo = foodItemPojo.getId();

                        //foodSectionSingle(tempfoodItemPojo,itemView);
                        holder.button_remove_item.setVisibility(View.VISIBLE);
                        holder.button_add_item.setVisibility(View.GONE);
                        Intent intent = new Intent(context, AddToCartActivity.class);
                       // Intent intent = new Intent(context, AddToCartActivityNew.class);
                        intent.putExtra("food_section", tempfoodItemPojo);
                        intent.putExtra("totalQuantity", tempFoodItemPojo.getQuantity());
                        intent.putExtra("button_remove_item", holder.button_remove_item.getId());
                        intent.putExtra("button_add_item", holder.button_add_item.getId());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        //Toast.makeText(context, "Food Section "+tempfoodItemPojo, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    UImsgs.showCustomToast(context, "Item out of stock", INFO);
                }
            }
        });

        holder.button_remove_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FoodItemPojo tempFoodItemPojo = data.get(position);
                cartDBHelper.deleteFoodItem(tempFoodItemPojo.getId());
                cartDBHelper.deleteSectCartItems(tempFoodItemPojo.getId());
                onItemAddListener.onItemAdded(true);
                holder.button_remove_item.setVisibility(View.GONE);
                holder.button_add_item.setVisibility(View.VISIBLE);
            }
        });


    }

    private String checkDiscountPrice(String price, int discount, int position) {

        if (discount != 0) {//Checking Food Discount

            FoodItemPojo tempFoodItemPojo = data.get(position);


            return String.valueOf(tempFoodItemPojo.getDiscountPrice());


        } else {
            //Food Price
            return price;
        }
    }

    private String currentSelectedItemsHashMap(FoodItemPojo foodItemPojo) {

        HashMap<String, Object> currentSelectedItemsHashMap = new HashMap<>();
        currentSelectedItemsHashMap.put("item_id", foodItemPojo.getId());
        currentSelectedItemsHashMap.put("quantity", 1);
        //currentSelectedItemsHashMap.put("price", foodItemPojo.getPrice());
        if (foodItemPojo.getDiscountPrice() > 0) {
            currentSelectedItemsHashMap.put("price", foodItemPojo.getDiscountPrice());
        } else {
            currentSelectedItemsHashMap.put("price", foodItemPojo.getPrice());
        }

        Log.e("price", "");

        //Toast.makeText(mContext, "In ELSE", Toast.LENGTH_SHORT).show();

        JSONObject json = new JSONObject(currentSelectedItemsHashMap);
        //currentSelectedItemsHashMapString = json.toString();

        return json.toString();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView displayName, displayprice, discountfooditem, fooditemdesc, fooditemdisount, ruppee_symbol;
        ImageView /*displayImage_blurr,*/ foodItemType, displayImage_clear;
        Button button_add_item, button_remove_item;
        BlurImageView displayImage_blurr;
        LinearLayout item;
        CardView card_food_item_adapter;
        RelativeLayout linearTop;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage_blurr = itemView.findViewById(R.id.fooditemImage_blurr);
            displayImage_clear = itemView.findViewById(R.id.fooditemImage_clear);
            button_add_item = itemView.findViewById(R.id.button_add_item);
            button_remove_item = itemView.findViewById(R.id.button_remove_item);
            foodItemType = itemView.findViewById(R.id.fooditemtype);
            displayName = itemView.findViewById(R.id.fooditemname);
            fooditemdesc = itemView.findViewById(R.id.fooditemdesc);
            displayprice = itemView.findViewById(R.id.fooditemPrice);
            fooditemdisount = itemView.findViewById(R.id.fooditemdisount);
            discountfooditem = itemView.findViewById(R.id.discountfooditem);
            ruppee_symbol = itemView.findViewById(R.id.ruppee_symbol);
            card_food_item_adapter = itemView.findViewById(R.id.card_food_item_adapter);
            linearTop = itemView.findViewById(R.id.linear_top);//Linear Layout
            //item = itemView.findViewById(R.id.item);

            if (listGrid && beautySpa) {
                ViewGroup.LayoutParams params = /*(RelativeLayout.LayoutParams)*/ linearTop.getLayoutParams();
                //params.height = 180;
                linearTop.setLayoutParams(params);
            }/*else{
            }*/
            if (listGrid) {
                //displayImage_blurr.
            } else {

            }


            if (foodItemTypeVegNon) {
                foodItemType.setVisibility(View.GONE);
            } else {
                foodItemType.setVisibility(View.VISIBLE);
            }

            /*card_food_item_adapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FoodItemPojo tempFoodItemPojo = data.get(getAdapterPosition());

                    String tempfoodItemPojo = tempFoodItemPojo.getId();

                    //foodSectionSingle(tempfoodItemPojo,itemView);

                    Intent intent = new Intent(context, AddToCartActivity.class);
                    intent.putExtra("food_section",tempfoodItemPojo );
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    //Toast.makeText(context, "Food Section "+tempfoodItemPojo, Toast.LENGTH_SHORT).show();

                }
            });*/


            displayImage_clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FoodItemPojo tempFoodItemPojo = data.get(getAdapterPosition());
                    if (tempFoodItemPojo.getSectionStatus() == 0) {

                        if (dataBaseHelper.getVendorIdPresence(vendorId)) {
                            if (cartDBHelper.insertFoodCart(tempFoodItemPojo.getId(), vendorId, tempFoodItemPojo.getName(), tempFoodItemPojo.getImage(),
                                    //priceString,
                                    checkDiscountPrice(tempFoodItemPojo.getPrice(), tempFoodItemPojo.getDiscount(), getAdapterPosition()),
                                    String.valueOf("1"),
                                    "",
                                    currentSelectedItemsHashMap(tempFoodItemPojo), "", tempFoodItemPojo.getQuantity())) {

                                onItemAddListener.onItemAdded(true);
                                onItemAddListener.onItemUncheck(tempFoodItemPojo.getPrice(), tempFoodItemPojo.getDiscount(), getAdapterPosition(),
                                        String.valueOf("1"),
                                        "",
                                        currentSelectedItemsHashMap(tempFoodItemPojo), "", tempFoodItemPojo.getQuantity());
                                button_remove_item.setVisibility(View.VISIBLE);
                                button_add_item.setVisibility(View.GONE);

                            } else {
                                //UImsgs.showToast(context, "Some Error Occurred");
                            }
                        }
                    } else {
                        FoodItemPojo foodItemPojo = data.get(getAdapterPosition());
                        String tempfoodItemPojo = foodItemPojo.getId();

                        //foodSectionSingle(tempfoodItemPojo,itemView);

                        Intent intent = new Intent(context, AddToCartActivity.class);
                      //  Intent intent = new Intent(context, AddToCartActivityNew.class);
                        intent.putExtra("food_section", tempfoodItemPojo);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        //Toast.makeText(context, "Food Section "+tempfoodItemPojo, Toast.LENGTH_SHORT).show();
                    }
                }
            });
           /* discountfooditem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FoodItemPojo tempFoodItemPojo = data.get(getAdapterPosition());

                    String tempfoodItemPojo = tempFoodItemPojo.getId();

                    //foodSectionSingle(tempfoodItemPojo,itemView);

                    Intent intent = new Intent(context, AddToCartActivity.class);
                    intent.putExtra("food_section",tempfoodItemPojo );
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);


                    if (discountfooditem.getText().toString().trim().equalsIgnoreCase("Add +")) {
                        discountfooditem.setText("Added");
                        count = count + 1;
                        //cartDBHelper.insertFoodCart(tempFoodItemPojo.getId(), tempFoodItemPojo.getName(), tempFoodItemPojo.getImage(), tempFoodItemPojo.getPrice(), "1");
                        //if (cartDBHelper.insertFoodCart(tempFoodItemPojo.getId(), tempFoodItemPojo.getName(), tempFoodItemPojo.getImage(), tempFoodItemPojo.getPrice(), "1")) {
                        if(true){
                            //Toast.makeText(context, "True", Toast.LENGTH_SHORT).show();
                        } else {
                            //Toast.makeText(context, "False", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (count > 0) {
                            count = count - 1;
                            cartDBHelper.deleteFoodItem(tempFoodItemPojo.getId());
                            discountfooditem.setText("Add +");
                        }
                    }
                }
            });*/


        }

        @Override
        public void onClick(View v) {

        }
    }

    public interface OnItemAddListener {
        void onItemAdded(boolean b);

        void onItemUncheck(String item, int discount, int secPrice, String valueOf, String s1, String currentSelectedItemsHashMap, String s, int getPrice);
    }

}