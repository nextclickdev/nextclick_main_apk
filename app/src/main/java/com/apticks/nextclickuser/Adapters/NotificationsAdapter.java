package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.notificationDetailsResponse.NotificationDetails;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.orderhistory.OrderDetailHistroy;

import java.util.LinkedList;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    private Context context;
    private LinkedList<NotificationDetails> listOFNotificationDetails;

    public NotificationsAdapter(Context context, LinkedList<NotificationDetails> listOFNotificationDetails) {
        this.context =context;
        this.listOFNotificationDetails = listOFNotificationDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notifictaion_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationDetails notificationDetails = listOFNotificationDetails.get(position);
        String title = String.valueOf(Html.fromHtml(notificationDetails.getTitle()));
        String message =  String.valueOf(Html.fromHtml(notificationDetails.getMessage()));
        holder.tvTitle.setText(title);
        holder.tvMessage.setText(message);

        holder.cardview_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, OrderDetailHistroy.class);
                i.putExtra("order_id", notificationDetails.getEcom_order_id());
               // i.putExtra("order_track_id", notificationDetails.getTrack_id());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listOFNotificationDetails.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvTitle,tvMessage;
        private CardView cardview_main;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            cardview_main = itemView.findViewById(R.id.cardview_main);
        }
    }
}
