package com.apticks.nextclickuser.Adapters.PLACES_ADAPTER;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity;
import com.apticks.nextclickuser.Activities.PLACES.PlacesActivity;
import com.apticks.nextclickuser.Pojo.PLACES.PlacePojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import java.util.List;

import static com.apticks.nextclickuser.Activities.MAIN.MainCategoriesActivity.locationStatus;

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.ViewHolder> {


    CartDBHelper cartDBHelper;

    List<PlacePojo> data;
    LayoutInflater inflter;
    PlacesActivity placesActivity;
    private PreferenceManager preferenceManager;

    public PlacesAdapter(PlacesActivity activity, List<PlacePojo> places) {
        this.placesActivity = activity;
        this.data = places;
        cartDBHelper = new CartDBHelper(placesActivity);
        preferenceManager = new PreferenceManager(placesActivity);
    }


    @NonNull
    @Override
    public PlacesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(placesActivity).inflate(R.layout.supporter_recent_search_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new PlacesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesAdapter.ViewHolder holder, int position) {
        PlacePojo placePojo = data.get(position);
        holder.recent_search_name.setText(placePojo.getName());
        //Log.d("place",placePojo.getName());

        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

       /* if (!productsPojo.getOffer_price().equals("0")){
            String sellCostString = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(productsPojo.getOffer_price()
                    ))).toString()
                    + "      ";

            String buyMRP = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(productsPojo.getMrp()
                    ))).toString();

            String costString = sellCostString + buyMRP;

            holder.mrp.setText(costString, TextView.BufferType.SPANNABLE);


            Spannable spannable = (Spannable) holder.mrp.getText();
            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
            spannable.setSpan(boldSpan, 0, sellCostString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new StrikethroughSpan(), sellCostString.length(),
                    costString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }else {
            String buyMRP = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(productsPojo.getMrp()
                    ))).toString();
            holder.mrp.setText(buyMRP);

        }
*/


    /*    holder.name.setText(productsPojo.getName());
        Picasso.get().load(productsPojo.getImage()).into(holder.image);*/



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView recent_search_name;

        LinearLayout item;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recent_search_name = itemView.findViewById(R.id.recent_search_name);

            item = itemView.findViewById(R.id.item);


            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* PlacePojo placePojo = data.get(getAdapterPosition());
                    Intent intent = new Intent(context, SingleGroceryProductActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    context.startActivity(intent);*/
                    PlacePojo placePojo = data.get(getAdapterPosition());
                    preferenceManager.putString("lat",placePojo.getLattitude());
                    preferenceManager.putString("lang",placePojo.getLongitude());
                    preferenceManager.putString("place",placePojo.getName());
                    MainCategoriesActivity.location_tv.setText(placePojo.getName());
                    ((PlacesActivity)placesActivity).finish();
                    locationStatus = true;

                }
            });




        }
    }

}