package com.apticks.nextclickuser.Adapters.VENDORSADAPTERS;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.AddToCartActivity;
import com.apticks.nextclickuser.Activities.CartActivity;
import com.apticks.nextclickuser.Activities.HospitalityActivities.HospitalityMainActivity;
import com.apticks.nextclickuser.Activities.OnDemandsActivities.OnDemandsMainActivity;
import com.apticks.nextclickuser.Activities.PROFILE.ProfileActivity;
import com.apticks.nextclickuser.Activities.VENDORS.VendorsActivity;
import com.apticks.nextclickuser.Activities.VENDOR_SUB_CATEGORIES.VendorsSubCategoriesActivity;
import com.apticks.nextclickuser.Helpers.TimeStamp;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.Pojo.VENDORSPOJO.VendorsPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.SqliteDatabase.CartDBHelper;
import com.apticks.nextclickuser.interfaces.CloseCallBack;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.bumptech.glide.Glide;
import com.mapbox.mapboxsdk.Mapbox;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.INDIVIDUAL_VENDOR;
import static com.apticks.nextclickuser.Config.Config.LEAD_GENERATE_ARRAY;
import static com.apticks.nextclickuser.Config.Config.MAP_BOX_ACCESS_TOKEN;
import static com.apticks.nextclickuser.Config.Config.VENDORUSERID;
import static com.apticks.nextclickuser.Constants.Constants.ADDRESS;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.INFO;
import static com.apticks.nextclickuser.Constants.Constants.SHOPNAME;
import static com.apticks.nextclickuser.Constants.Constants.SUCCESS;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class VendorsAdapter extends RecyclerView.Adapter<VendorsAdapter.ViewHolder>   {

    List<VendorsPojo> data;

    LayoutInflater inflter;
    Context context;
    private CartDBHelper cartDBHelper;

    private int vendor_activity = 0;//0-not working , 1-working
    private String comingsoon_image_url;
    private String tempNameVendor, idStringVendor /*For Passing Vendor Data To next Activity */;
    int SHOP_NOW=1,BOOK_NOW=2;
    int SHOP_BOOK ;
    private PreferenceManager preferenceManager;
    private String token = "";
    public VendorsAdapter(Context activity, List<VendorsPojo> itemPojos, int SHOP_BOOK, ArrayList<ServicesPojo> serviceslist) {
        this.context = activity;
        this.data = itemPojos;
        this.SHOP_BOOK = SHOP_BOOK;
        Mapbox.getInstance(context, MAP_BOX_ACCESS_TOKEN);
        Mapbox.setAccessToken(MAP_BOX_ACCESS_TOKEN);
        cartDBHelper = new CartDBHelper(context);
        preferenceManager = new PreferenceManager(context);
        token = preferenceManager.getString("token");
    }


    @NonNull
    @Override
    public VendorsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.vendor_card_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new VendorsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorsAdapter.ViewHolder holder, int position) {
        VendorsPojo vendorsPojo = data.get(position);

        holder.vendor_name.setText(vendorsPojo.getName());
        holder.customer_address.setText(vendorsPojo.getAddress());
        holder.vendor_landmark.setText(vendorsPojo.getLandmark());
        holder.vendor_email.setText(vendorsPojo.getEmail());
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        holder.vendor_distance.setText(df.format(vendorsPojo.getDistance())+" KM away");
        Glide.with(context)
                .load(vendorsPojo.getImage())
                .into(holder.vendor_image);
       /* Picasso.get()
                .load(vendorsPojo.getImage())
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(holder.vendor_image);*/

        /*holder.vendor_location_map;*/

       /* holder.vendor_location_map.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {


                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {

                        // Map is set up and the style has loaded. Now you can add data or make other map adjustments
                    }
                });
            }
        });*/

     /*  if(SHOP_BOOK==BOOK_NOW){
           holder.vendor_view_shop.setText("Book Now");
           holder.vendor_view_shop_card.setVisibility(View.GONE);
       }*/

        holder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(vendorsPojo.getAvailability()==1) {
                    if (cartDBHelper.numberOfFoodCartRows() > 0) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Cart Alert");
                        alertDialogBuilder
                                .setMessage("You have some cart items. Would you like to check or clear")
                                .setCancelable(false)
                                .setPositiveButton("Clear", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        cartDBHelper.deleteCartTable();
                                        UImsgs.showToast(context, "Cart cleared successfully");
                                    }
                                })
                                .setNegativeButton("Check", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(context, CartActivity.class);
                                        intent.putExtra("Type", "Food");
                                        context.startActivity(intent);
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        Intent intent = new Intent(context, ProfileActivity.class);
                        if(SHOP_BOOK==BOOK_NOW) {
                            intent.putExtra(VENDOR_ID, vendorsPojo.getId());//need to pass vendor id (in api response it is named as "id")
                            intent.putExtra("SHOP_BOOK", "BOOK_NOW");
                        }else{
                            intent.putExtra(VENDOR_ID, vendorsPojo.getId());//need to pass vendor id (in api response it is named as "id")
                            intent.putExtra("SHOP_BOOK", "SHOP_NOW");
                        }
                        /*intent.putExtra(VENDOR_ID, vendorsPojo.getId());//need to pass vendor id (in api response it is named as "id")*/
                        AddToCartActivity.vendorId = (vendorsPojo.getVendor_user_id());
                        //Toast.makeText(context, ""+vendorsPojo.getVendor_user_id(), Toast.LENGTH_SHORT).show();
                        context.startActivity(intent);
                    }
                }else{
                    UImsgs.showCustomToast(context,"Sorry! We are closed",INFO);
                }


            }
        });

        holder.vendor_view_shop_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(vendorsPojo.getAvailability()==1) {

                   /* if (cartDBHelper.numberOfFoodCartRows() > 0) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Cart Alert");
                        alertDialogBuilder
                                .setMessage("You have some cart items. Would you like to check or clear")
                                .setCancelable(false)
                                .setPositiveButton("Clear", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        cartDBHelper.deleteCartTable();
                                        UImsgs.showToast(context, "Cart cleared successfully");
                                    }
                                })
                                .setNegativeButton("Check", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(context, CartActivity.class);
                                        intent.putExtra("Type", "Food");
                                        context.startActivity(intent);
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {*/
                        AddToCartActivity.vendorId = (vendorsPojo.getVendor_user_id());
                        profileFetcher(vendorsPojo,0);
                  //  }
                }else{
                    UImsgs.showCustomToast(context,"Sorry! We are closed",INFO);
                }


            }
        });

        if(vendorsPojo.getAvailability()==1){
            holder.vendor_status.setText("Open");
            holder.vendor_status_card.setCardBackgroundColor(Color.parseColor("#008000"));
        }else{
            holder.vendor_status.setText("Closed");
            holder.vendor_status_card.setCardBackgroundColor(Color.parseColor("#ff0000"));
        }
        holder.vendor_view_shop_card.setVisibility(View.INVISIBLE);
        holder.vendor_view_book_card.setVisibility(View.INVISIBLE);
        holder.vendor_view_ondemand_card.setVisibility(View.INVISIBLE);
        holder.layout_getcall.setVisibility(View.INVISIBLE);
        holder.layout_ondemand.setVisibility(View.GONE);
        holder.vendor_view_plus_card.setVisibility(View.GONE);

        boolean is_having_lead = Boolean.parseBoolean(data.get(position).getIs_having_lead());
        if (!is_having_lead){
            holder.layout_getcall.setVisibility(View.GONE);

        }else {
            holder.layout_getcall.setVisibility(View.VISIBLE);

        }

        try{
            int length=data.get(position).getServiceList().size();

            for (int i=0;i<data.get(position).getServiceList().size();i++){

            if (data.get(position).getServiceList().get(i).getId().equalsIgnoreCase("2")){
                holder.vendor_view_shop_card.setVisibility(View.VISIBLE);
                holder.vendor_view_shop.setText(data.get(position).getServiceList().get(i).getName());
            }if (data.get(position).getServiceList().get(i).getId().equalsIgnoreCase("11")){
                holder.vendor_view_book_card.setVisibility(View.VISIBLE);
                holder.vendor_view_book.setText(data.get(position).getServiceList().get(i).getName());
            }if (data.get(position).getServiceList().get(i).getId().equalsIgnoreCase("4")){
                length=length-1;
                holder.layout_getcall.setVisibility(View.VISIBLE);
                holder.tv_leadtext.setText(data.get(position).getServiceList().get(i).getName());
            }if (data.get(position).getServiceList().get(i).getId().equalsIgnoreCase("8")){
                holder.vendor_view_ondemand_card.setVisibility(View.VISIBLE);
                holder.layout_ondemand.setVisibility(View.VISIBLE);
                holder.vendor_view_ondemand.setText(data.get(position).getServiceList().get(i).getName());
            }
            }

        }catch (NullPointerException e){
            System.out.println("aaaaaaa  expa adapter "+e.getMessage());
        }

        holder.vendor_view_ondemand_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Intent intent = new Intent(context, OnDemandsMainActivity.class);
                intent.putExtra(reastaurantName, tempNameVendor);
                intent.putExtra(vendorUserId, idStringVendor);
                intent.putExtra(VENDOR_ID, data.get(position).getId());
               // intent.putExtra(context.getString(R.string.service_id), servicesPojo.getId());
                context.startActivity(intent);
            }
        }); holder.vendor_view_book_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Intent intent = new Intent(context, HospitalityMainActivity.class);
                intent.putExtra(reastaurantName, tempNameVendor);
                intent.putExtra(vendorUserId, idStringVendor);
                intent.putExtra(VENDOR_ID, data.get(position).getId());
               // intent.putExtra(context.getString(R.string.service_id), servicesPojo.getId());
                context.startActivity(intent);
            }
        }); holder.layout_getcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                profileFetcher(data.get(position),1);

            }
        });



    }
    boolean contains(ServicesPojo list, String name) {
      //  for (ServicesPojo item : list) {
            if (list.getId().equals(name)) {
                return true;
            }
     //   }
        return false;
    }
    @Override
    public int getItemCount() {
        return data.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView vendor_name, customer_address,vendor_landmark,vendor_email,vendor_view,vendor_view_shop,vendor_view_book,
                vendor_view_ondemand,vendor_distance,vendor_status,tv_leadtext,vendor_view_more;
        ImageView vendor_image;
        LinearLayout item_layout,layout_getcall,layout_ondemand;
        CardView vendor_status_card,vendor_view_shop_card,vendor_view_book_card,vendor_view_ondemand_card,vendor_view_plus_card;
       // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_layout = itemView.findViewById(R.id.item_layout);
            vendor_image = itemView.findViewById(R.id.vendor_image);
            vendor_name = itemView.findViewById(R.id.vendor_name);
            customer_address = itemView.findViewById(R.id.customer_address);
            vendor_landmark = itemView.findViewById(R.id.vendor_landmark);
            vendor_email = itemView.findViewById(R.id.vendor_email);
            vendor_view = itemView.findViewById(R.id.vendor_view);
            layout_getcall = itemView.findViewById(R.id.layout_getcall);
            tv_leadtext = itemView.findViewById(R.id.tv_leadtext);
            layout_ondemand = itemView.findViewById(R.id.layout_ondemand);
            vendor_view_more = itemView.findViewById(R.id.vendor_view_more);

            vendor_view_shop = itemView.findViewById(R.id.vendor_view_shop);
            vendor_view_book = itemView.findViewById(R.id.vendor_view_book);
            vendor_view_book_card = itemView.findViewById(R.id.vendor_view_book_card);
            vendor_view_plus_card = itemView.findViewById(R.id.vendor_view_plus_card);
            vendor_view_ondemand_card = itemView.findViewById(R.id.vendor_view_ondemand_card);
            vendor_view = itemView.findViewById(R.id.vendor_view);
            vendor_distance = itemView.findViewById(R.id.vendor_distance);
            vendor_view_ondemand = itemView.findViewById(R.id.vendor_view_ondemand);

            vendor_status = itemView.findViewById(R.id.vendor_status);
            vendor_status_card = itemView.findViewById(R.id.vendor_status_card);
            vendor_view_shop_card = itemView.findViewById(R.id.vendor_view_shop_card);
            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);

        }
    }


    private void profileFetcher(VendorsPojo vendorsPojo,int position) {

        DialogOpener.dialogOpener(context);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        System.out.println("aaaaaaaa  url "+INDIVIDUAL_VENDOR + vendorsPojo.getId());
        Log.d("url", INDIVIDUAL_VENDOR + vendorsPojo.getId());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, INDIVIDUAL_VENDOR + vendorsPojo.getId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("vendor response", response);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("aaaaaaaa vendor details"+jsonObject.toString());
                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    if (status && http_code == 200) {

                        ArrayList<Object> leadVendorsList = new ArrayList<>();
                        Map<String, String> leadVendorsMapList = new HashMap<>();
                        if (!jsonObject.get("data").getClass().toString().equalsIgnoreCase(jsonObject.get("status").getClass().toString())) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            idStringVendor = dataObject.getString("vendor_user_id");
                            tempNameVendor = dataObject.getString("name");

                            try {
                                JSONObject category_object = dataObject.getJSONObject("category");
                                vendor_activity = category_object.getInt("status");
                                comingsoon_image_url = category_object.getString("coming_soon_image");
                                if (position==0){
                                    if (vendor_activity != 0) {
                                        System.out.println("aaaaaaaa vendoruserid  "+vendorsPojo.getVendor_user_id());
                                        Intent intent = new Intent(context, VendorsSubCategoriesActivity.class);
                                        intent.putExtra(reastaurantName, tempNameVendor);
                                        intent.putExtra(vendorUserId, idStringVendor);
                                        intent.putExtra(VENDOR_ID, vendorsPojo.getId());
                                        intent.putExtra(ADDRESS, vendorsPojo.getAddress());
                                        intent.putExtra(SHOPNAME, vendorsPojo.getName());
                                      //  intent.putExtra(vendorUserId, vendorsPojo.getVendor_user_id());

                                        context.startActivity(intent);

                                    } else {
                                        comingSoon(comingsoon_image_url);
                                    }
                                }else {
                                    try {
                                        JSONObject jsonObject11 = new JSONObject();
                                        JSONArray jsonArray = new JSONArray();
                                        JSONObject vendorJsonObject = new JSONObject();
                                        vendorJsonObject.put("vendor_id", idStringVendor);
                                        jsonArray.put(vendorJsonObject);
                                        jsonObject11.put("vendors", jsonArray);
                                        leadGenerator(jsonObject11);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    }
                    DialogOpener.dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    DialogOpener.dialog.dismiss();
                    //UImsgs.showToast(context, "Catch " +e);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    //Coming soon
    private void comingSoon(String imageUrl) {
        TimeStamp timeStamp = new TimeStamp();
        ImageView comingsoonImage;
        LayoutInflater inflater = ((VendorsActivity)context).getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.coming_soon_layou, null);
        comingsoonImage = alertLayout.findViewById(R.id.comingsoon);
        Picasso.get()
                .load(imageUrl+timeStamp.timeStampFetcher())
                .into(comingsoonImage);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setView(alertLayout);
        alertDialog.show();
    }

    private void leadGenerator(JSONObject jsonObject) {
        final String data = jsonObject.toString();
        DialogOpener.dialogOpener(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LEAD_GENERATE_ARRAY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 201) {
                            CloseCallBack closeCallBack = (CloseCallBack) context;
                            closeCallBack.close();
                            DialogOpener.dialog.dismiss();
                            UImsgs.showCustomToast(context, "Thank you ! " + jsonObject.getString("message"), SUCCESS);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("aaaaaaaa catch "+e.getMessage());
                        DialogOpener.dialog.dismiss();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                System.out.println("aaaaaaaa error "+error.getMessage());
                UImsgs.showCustomToast(context, "Something Went Wrong", ERROR);

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
