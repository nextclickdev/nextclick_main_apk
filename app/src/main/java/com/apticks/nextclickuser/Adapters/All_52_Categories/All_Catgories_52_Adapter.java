package com.apticks.nextclickuser.Adapters.All_52_Categories;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.SUB_CATEGORIES.SubCategoriesActivity;
import com.apticks.nextclickuser.Helpers.TimeStamp;
import com.apticks.nextclickuser.Pojo.CategoriesData;
import com.apticks.nextclickuser.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.apticks.nextclickuser.Constants.CategoriesId.beautySpa;
import static com.apticks.nextclickuser.Constants.CategoriesId.foodItemTypeVegNon;
import static com.apticks.nextclickuser.Constants.CategoriesId.listGrid;
import static com.apticks.nextclickuser.Constants.Constants.CAT_ID;

public class All_Catgories_52_Adapter extends RecyclerView.Adapter<All_Catgories_52_Adapter.ViewHolder> {

    List<CategoriesData> data;

    LayoutInflater inflter;
    Context context;
    View itemView;

    public All_Catgories_52_Adapter(Context activity, List<CategoriesData> completData) {
        this.context = activity;
        this.data = completData;
    }


    @NonNull
    @Override
    public All_Catgories_52_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(context).inflate(R.layout.supporter_all_categories_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new All_Catgories_52_Adapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull All_Catgories_52_Adapter.ViewHolder holder, int position) {
        CategoriesData categoriesData = data.get(position);

        System.out.println("aaaaaaaaa  categery name "+categoriesData.getName());
        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.all_cat_name.setText(categoriesData.getName());
        TimeStamp timeStamp = new TimeStamp();
        Glide.with(context)
                .load(categoriesData.getImage()+timeStamp.timeStampFetcher())
                .into(holder.all_cat_image);

      //  Picasso.get().load(categoriesData.getImage()+timeStamp.timeStampFetcher()).into(holder.all_cat_image);
        /*holder.displayLocation.setText(categoriesData.getLocation());*/
        holder.category_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SubCategoriesActivity.class);
                intent.putExtra(CAT_ID, categoriesData.getId());
                foodItemTypeVegNon = true;
                beautySpa = true;
                listGrid = false;
                context.startActivity(intent);
            }
        });


    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView all_cat_name;
        ImageView all_cat_image;
        LinearLayout category_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            all_cat_image = itemView.findViewById(R.id.all_cat_image);
            all_cat_name = itemView.findViewById(R.id.all_cat_name);
            category_layout = itemView.findViewById(R.id.all_cat_item);



       /* cardViewRecentAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RecentlyAddedHostelModelData recentlyAddedHostelModelData = dataArrayList.get(getAdapterPosition());
                String tempId = recentlyAddedHostelModelData.getId();

                Intent intent = new Intent(context, SingleViewHostelActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("hostelId",tempId);

                context.startActivity(intent);
            }
        });*/



        }
    }
}

