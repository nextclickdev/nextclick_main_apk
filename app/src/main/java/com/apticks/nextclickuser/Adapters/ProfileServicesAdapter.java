package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Activities.HospitalityActivities.HospitalityMainActivity;
import com.apticks.nextclickuser.Activities.OnDemandsActivities.OnDemandsMainActivity;
import com.apticks.nextclickuser.Activities.PROFILE.ProfileActivity;
import com.apticks.nextclickuser.Activities.VENDOR_SUB_CATEGORIES.VendorsSubCategoriesActivity;
import com.apticks.nextclickuser.Helpers.uiHelpers.DialogOpener;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.MultiCatPojo.ServicesPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.interfaces.CloseCallBack;
import com.apticks.nextclickuser.utilities.PreferenceManager;
import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.LEAD_GENERATE_ARRAY;
import static com.apticks.nextclickuser.Constants.Constants.ERROR;
import static com.apticks.nextclickuser.Constants.Constants.SUCCESS;
import static com.apticks.nextclickuser.Constants.Constants.VENDOR_ID;
import static com.apticks.nextclickuser.Constants.Constants.reastaurantName;
import static com.apticks.nextclickuser.Constants.Constants.vendorUserId;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class ProfileServicesAdapter extends RecyclerView.Adapter<ProfileServicesAdapter.ViewHolder> {

    private Context context;
    private Transformation transformation;
    private PreferenceManager preferenceManager;

    private ArrayList<ServicesPojo> listOfServicesList;

    private String token = "";
    private String vendor_id = "";
    private String tempNameVendor = "";
    private String idStringVendor = "";

    public ProfileServicesAdapter(Context context, ArrayList<ServicesPojo> listOfServicesList, String tempNameVendor, String idStringVendor, String vendor_id) {
        this.context = context;
        this.vendor_id = vendor_id;
        this.tempNameVendor = tempNameVendor;
        this.idStringVendor = idStringVendor;
        this.listOfServicesList = listOfServicesList;
        preferenceManager = new PreferenceManager(context);
        token = preferenceManager.getString("token");

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_profile_services_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ServicesPojo servicesPojo = listOfServicesList.get(position);
        String name = servicesPojo.getName();
        String imageUrl = servicesPojo.getImageUrl();

        holder.tvServiceName.setText(name);

        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Glide.with(context)
                        .load(imageUrl)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .into(holder.ivServicePic);
               /* Picasso.get()
                        .load(imageUrl)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivServicePic);*/
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivServicePic);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.ic_default_place_holder)
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .transform(transformation)
                    .fit().centerCrop()
                    .into(holder.ivServicePic);
        }
    }

    @Override
    public int getItemCount() {
        return listOfServicesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivInfo, ivServicePic;
        private TextView tvServiceName, tvServiceDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivInfo = itemView.findViewById(R.id.ivInfo);
            ivServicePic = itemView.findViewById(R.id.ivServicePic);
            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvServiceDescription = itemView.findViewById(R.id.tvServiceDescription);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            ServicesPojo servicesPojo = listOfServicesList.get(getLayoutPosition());
            if (servicesPojo != null) {
                String id = servicesPojo.getId();
                Intent intent = null;
                if (servicesPojo.getId().equalsIgnoreCase("2")) {
                    intent = new Intent(context, VendorsSubCategoriesActivity.class);
                    intent.putExtra(reastaurantName, tempNameVendor);
                    intent.putExtra(vendorUserId, idStringVendor);
                    intent.putExtra(VENDOR_ID, vendor_id);
                    intent.putExtra(context.getString(R.string.service_id), servicesPojo.getId());
                    context.startActivity(intent);
                } else if (servicesPojo.getId().equalsIgnoreCase("8")) {
                    intent = new Intent(context, OnDemandsMainActivity.class);
                    intent.putExtra(reastaurantName, tempNameVendor);
                    intent.putExtra(vendorUserId, idStringVendor);
                    intent.putExtra(VENDOR_ID, vendor_id);
                    intent.putExtra(context.getString(R.string.service_id), servicesPojo.getId());
                    context.startActivity(intent);
                } else if (servicesPojo.getId().equalsIgnoreCase("11")) {
                    intent = new Intent(context, HospitalityMainActivity.class);
                    intent.putExtra(reastaurantName, tempNameVendor);
                    intent.putExtra(vendorUserId, idStringVendor);
                    intent.putExtra(VENDOR_ID, vendor_id);
                    intent.putExtra(context.getString(R.string.service_id), servicesPojo.getId());
                    context.startActivity(intent);
                } else if (servicesPojo.getId().equalsIgnoreCase("4")) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        JSONArray jsonArray = new JSONArray();
                        JSONObject vendorJsonObject = new JSONObject();
                        vendorJsonObject.put("vendor_id", idStringVendor);
                        jsonArray.put(vendorJsonObject);
                        jsonObject.put("vendors", jsonArray);
                        leadGenerator(jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    private void leadGenerator(JSONObject jsonObject) {
        final String data = jsonObject.toString();
        DialogOpener.dialogOpener(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LEAD_GENERATE_ARRAY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        String str = "";
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        int http_code = jsonObject.getInt("http_code");
                        if (status && http_code == 201) {
                            CloseCallBack closeCallBack = (CloseCallBack) context;
                            closeCallBack.close();
                            DialogOpener.dialog.dismiss();
                            UImsgs.showCustomToast(context, "Thank you ! " + jsonObject.getString("message"), SUCCESS);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        DialogOpener.dialog.dismiss();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogOpener.dialog.dismiss();
                UImsgs.showCustomToast(context, "Something Went Wrong", ERROR);

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(TOKEN_KEY));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
