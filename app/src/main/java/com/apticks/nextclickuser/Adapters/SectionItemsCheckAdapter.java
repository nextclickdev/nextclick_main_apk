package com.apticks.nextclickuser.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.SectionItemsRadioModel;
import com.apticks.nextclickuser.R;

import java.util.ArrayList;

public class SectionItemsCheckAdapter extends RecyclerView.Adapter<SectionItemsCheckAdapter.ViewModels> {
    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayList = new ArrayList<>();
    Boolean aBoolean;
    public SectionItemsCheckAdapter(ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayList,Boolean aBoolean) {
        this.sectionItemsCheckModelsArrayList = sectionItemsCheckModelsArrayList;
        this.aBoolean = aBoolean;
    }
    @NonNull
    @Override
    public ViewModels onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_check_radio,parent,false);
        return new SectionItemsCheckAdapter.ViewModels(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewModels holder, int position) {
        SectionItemsRadioModel sectionItemsRadioModel = sectionItemsCheckModelsArrayList.get(position);
        //sectionItemsRadioModel.getName();
        holder.title_name.setText(sectionItemsRadioModel.getName());
        holder.title_price.setText(sectionItemsRadioModel.getPrice());
    }

    @Override
    public int getItemCount() {
        return sectionItemsCheckModelsArrayList.size();
    }

    public class ViewModels extends RecyclerView.ViewHolder {
        TextView title_name,title_price;
        CheckBox checkButton;
        RadioButton radioSelect;
        public ViewModels(@NonNull View itemView) {
            super(itemView);
            title_name = itemView.findViewById(R.id.title_name);
            title_price = itemView.findViewById(R.id.description_price);
            radioSelect = itemView.findViewById(R.id.radio_select);
            checkButton = itemView.findViewById(R.id.check_select);
            if (aBoolean) {
                radioSelect.setVisibility(View.VISIBLE);
                checkButton.setVisibility(View.GONE);
            }else {
                radioSelect.setVisibility(View.GONE);
                checkButton.setVisibility(View.VISIBLE);
            }

        }
    }
}
