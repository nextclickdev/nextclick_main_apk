package com.apticks.nextclickuser.Adapters;



/*
 * @author : sarath
 *
 * @desc : Helps in displaying items in categories on dashborad..
 *
 * */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Activities.HMS_ACTIVITIES.IndividualVendorActivity_HMS;
import com.apticks.nextclickuser.Pojo.DisplayCategory;
import com.apticks.nextclickuser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HospitalAdapter extends RecyclerView.Adapter<HospitalAdapter.ViewHolder> {

    List<DisplayCategory> data;

    LayoutInflater inflter;
    Context context;

    public HospitalAdapter(Context activity, List<DisplayCategory> listRecentHostel) {
        this.context = activity;
        this.data = listRecentHostel;
    }


    @NonNull
    @Override
    public HospitalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.individualcategorycompletelist, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new HospitalAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HospitalAdapter.ViewHolder holder, int position) {
        DisplayCategory hospital = data.get(position);


        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        holder.displayName.setText(hospital.getName());
        Picasso.get().load(hospital.getImage()).into(holder.displayImage);
        /*holder.displayLocation.setText(hospital.getLocation());*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView displayName, displayLocation;
        ImageView displayImage;
        LinearLayout item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            displayImage = itemView.findViewById(R.id.displayimage);
            displayName = itemView.findViewById(R.id.displayname);
            displayLocation = itemView.findViewById(R.id.displaylocation);
            item = itemView.findViewById(R.id.item);


            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DisplayCategory recentlyAddedHostelModelData = data.get(getAdapterPosition());
                    String tempId = recentlyAddedHostelModelData.getId();

                    Intent intent = new Intent(context, IndividualVendorActivity_HMS.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("vendorId",tempId);

                    context.startActivity(intent);
                }
            });


        }
    }
}

