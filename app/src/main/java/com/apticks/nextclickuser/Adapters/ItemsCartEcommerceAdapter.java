package com.apticks.nextclickuser.Adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apticks.nextclickuser.Helpers.uiHelpers.UImsgs;
import com.apticks.nextclickuser.Pojo.CartPojoModel.CartPojo;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.Money;
import com.apticks.nextclickuser.utilities.PreferenceManager;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.apticks.nextclickuser.Config.Config.ECOM_CART_D;
import static com.apticks.nextclickuser.Constants.PreferenceManagerKey.TOKEN_KEY;

public class ItemsCartEcommerceAdapter extends RecyclerView.Adapter<ItemsCartEcommerceAdapter.ViewHolder> {
    Context mContext;
    ArrayList<CartPojo> dataArrayList;

    PreferenceManager preferenceManager;
    String token;
    public ItemsCartEcommerceAdapter(Context mContext, ArrayList<CartPojo> dataArrayList) {
        this.mContext = mContext;
        this.dataArrayList = dataArrayList;
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
    }

    @NonNull
    @Override
    public ItemsCartEcommerceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_items_cart_ecommerce, parent, false);
        return new ItemsCartEcommerceAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsCartEcommerceAdapter.ViewHolder holder, int position) {
        CartPojo cartPojo = dataArrayList.get(position);
        holder.itemName.setText(cartPojo.getName());
        //holder.itemSpecification.setText(cartPojo.getSpecification());
        holder.quatity.setText(cartPojo.getQuantity());

        if (!cartPojo.getProductOfferPrice().equals("0")){
            String sellCostString = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(cartPojo.getProductOfferPrice()
                    ))).toString()
                    + "      ";

            String buyMRP = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(cartPojo.getPrice()
                    ))).toString();

            String costString = sellCostString + buyMRP;

            holder.itemPrice.setText(costString, TextView.BufferType.SPANNABLE);
            holder.offerPrice.setText(sellCostString);

            Spannable spannable = (Spannable) holder.itemPrice.getText();
            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
            spannable.setSpan(boldSpan, 0, sellCostString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new StrikethroughSpan(), sellCostString.length(),
                    costString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            int totalPrice = Integer.parseInt(cartPojo.getProductOfferPrice())*Integer.parseInt(cartPojo.getQuantity());
            holder.total_cost.setText(String.valueOf(totalPrice));


        }else {
            String buyMRP = Money.rupees(
                    BigDecimal.valueOf(Long.valueOf(cartPojo.getPrice()
                    ))).toString();
            holder.itemPrice.setText(buyMRP);
            holder.offerPrice.setText(buyMRP);

            int totalPrice = Integer.parseInt(cartPojo.getPrice())*Integer.parseInt(cartPojo.getQuantity());
            holder.total_cost.setText(String.valueOf(totalPrice));
        }


       /* holder.itemPrice.setText(cartPojo.getPrice());
        holder.itemNetPrice.setText(cartPojo.getProductOfferPrice());*/
       /*holder.remove_ecom_items_detail.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               deleteFromCart(cartPojo.getId(),position);
           }
       });*/

    }

    private void deleteFromCart(String id, int position) {
        HashMap<String, String> ECOM_CART_D_HASP = new HashMap<>();
        ECOM_CART_D_HASP.put("id",id);
        JSONObject ECOM_CART_D_JSON = new JSONObject(ECOM_CART_D_HASP);
        final String data =  ECOM_CART_D_JSON.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ECOM_CART_D, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                UImsgs.showToast(mContext, response);
                dataArrayList.remove(position);
                notifyDataSetChanged();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName,itemSpecification,itemPrice,itemNetPrice, offerPrice,quatity,total_cost;
        ImageView itemImage,remove_ecom_items_detail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_name);
            itemSpecification = itemView.findViewById(R.id.item_specification);
            itemPrice = itemView.findViewById(R.id.item_price);
            itemNetPrice = itemView.findViewById(R.id.item_net_price);
            offerPrice = itemView.findViewById(R.id.offer_price);
            quatity = itemView.findViewById(R.id.quatity);
            total_cost = itemView.findViewById(R.id.total_cost);
            itemImage = itemView.findViewById(R.id.item_image);
            remove_ecom_items_detail = itemView.findViewById(R.id.remove_ecom_items_detail);

            remove_ecom_items_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //deleteFromCart(cartPojo.getId(),position);
                    CartPojo cartPojo = dataArrayList.get(getAdapterPosition());
                    String id = cartPojo.getId();
                    int position = getAdapterPosition();

                    {
                        HashMap<String, String> ECOM_CART_D_HASP = new HashMap<>();
                        ECOM_CART_D_HASP.put("id",id);
                        JSONObject ECOM_CART_D_JSON = new JSONObject(ECOM_CART_D_HASP);
                        final String data =  ECOM_CART_D_JSON.toString();
                        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, ECOM_CART_D, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                UImsgs.showToast(mContext, response);
                                dataArrayList.remove(position);
                                notifyDataSetChanged();
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }){

                            @Override
                            public String getBodyContentType() {
                                return "application/json";
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String> map = new HashMap<>();
                                map.put("Content-Type", "application/json");
                                map.put("X_AUTH_TOKEN", token);
                                return map;
                            }

                            @Override
                            public byte[] getBody() throws AuthFailureError {
                                try {
                                    return data == null ? null : data.getBytes("utf-8");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        requestQueue.add(stringRequest);
                    }

                }
            });
        }
    }
}
