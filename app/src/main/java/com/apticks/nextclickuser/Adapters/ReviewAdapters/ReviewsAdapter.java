package com.apticks.nextclickuser.Adapters.ReviewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.Review_POJO.ReviewsPojo;
import com.apticks.nextclickuser.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    List<ReviewsPojo> data;

    LayoutInflater inflter;
    Context context;

    public ReviewsAdapter(Context activity, List<ReviewsPojo> reviewsList) {
        this.context = activity;
        this.data = reviewsList;
    }


    @NonNull
    @Override
    public ReviewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.reviews_supporter, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new ReviewsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsAdapter.ViewHolder holder, int position) {

        ReviewsPojo currentReview = data.get(position);
        //String tempImageUrl = BASE_URL_UPLOADS+hostelViewModelData.getImageHostel();

        // holder.amenityName.setText(data.get(position));

        if(currentReview.getLname()!=null) {
            holder.reviewrName.setText(currentReview.getFname() + " " + currentReview.getLname());
        }else{
            holder.reviewrName.setText(currentReview.getFname());
        }
        holder.reviewrReview.setText(currentReview.getReview());
        holder.ratingBar.setRating(Float.parseFloat(currentReview.getRating()));
        holder.rating.setText(currentReview.getRating()+"/5");

        Picasso.get().load(currentReview.getImage()).into(holder.reviewrImage);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView reviewrName,reviewrReview,rating;
        CircularImageView reviewrImage;
        RatingBar ratingBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            reviewrName = itemView.findViewById(R.id.reviewer_name);
            reviewrReview = itemView.findViewById(R.id.reviewr_review);
            rating = itemView.findViewById(R.id.review_rating_text);
            reviewrImage = itemView.findViewById(R.id.reviewer_image);
            ratingBar = itemView.findViewById(R.id.reviewer_rating);


        }
    }
}

