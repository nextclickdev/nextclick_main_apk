package com.apticks.nextclickuser.Adapters;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.apticks.nextclickuser.Fragments.PlaceholderFragment;
import com.apticks.nextclickuser.Pojo.FoodMenuPojo;

import java.util.ArrayList;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    //private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3, R.string.tab_text_4};
    //private static final int[] TAB_TITLES = new int[]{1, 2, 3, 4};
    private static final int[] TAB_TITLES = new int[]{1, 2, 3, 4};
    public static ArrayList<FoodMenuPojo> foodMenuDataName = new ArrayList<>();
    private final Context mContext;
    String vegNonVeg ="";

    public SectionsPagerAdapter(Context context, FragmentManager fm,String vegNonVeg) {
        //super(fm);
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
        this.vegNonVeg = vegNonVeg;
    }

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        //super(fm);
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        FoodMenuPojo foodMenuPojo = foodMenuDataName.get(position);
        //foodMenuPojo.getId();
        return PlaceholderFragment.newInstance(position + 1,foodMenuPojo.getId(),vegNonVeg);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        //return mContext.getResources().getString(TAB_TITLES[position]);
        //return String.valueOf(TAB_TITLES[position]);
        FoodMenuPojo foodMenuPojo = foodMenuDataName.get(position);
        return String.valueOf(foodMenuPojo.getName());
    }

    @Override
    public int getCount() {
        //return TAB_TITLES.length;
        return foodMenuDataName.size();
    }
}