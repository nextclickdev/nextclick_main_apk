package com.apticks.nextclickuser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apticks.nextclickuser.Pojo.SectionItemsRadioModel;
import com.apticks.nextclickuser.R;
import com.apticks.nextclickuser.utilities.Money;
import static com.apticks.nextclickuser.Activities.AddToCartActivity.*;

import java.math.BigDecimal;
import java.util.ArrayList;


public class SectionItemsModelInnerRecycler extends RecyclerView.Adapter<SectionItemsModelInnerRecycler.ViewModel> {
    ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListInnerName = new ArrayList<>();
    Boolean radioCheckBoolean;
    Context mContext;
    @NonNull
    private OnItemCheckListener onItemCheckListener;

    private int lastSelectedPosition = -1;
    private int lastSelectedPositionUnselect = 0;
    /*private int lastSelectedPosition = 0;
    private int lastSelectedPositionUnselect = 0;*/

    private int countRadio = 0;
    private int secPrice;

    public SectionItemsModelInnerRecycler(ArrayList<SectionItemsRadioModel> sectionItemsCheckModelsArrayListInnerName,
                                          Boolean radioCheckBoolean,
                                          @NonNull OnItemCheckListener onItemCheckListener,
                                          int secPrice) {
        this.sectionItemsCheckModelsArrayListInnerName = sectionItemsCheckModelsArrayListInnerName;
        this.radioCheckBoolean = radioCheckBoolean;
        this.onItemCheckListener = onItemCheckListener;
        this.secPrice = secPrice;
    }

    interface OnItemCheckListener {
        void onItemCheck(SectionItemsRadioModel item, int secPrice, int getPrice);
        void onItemUncheck(SectionItemsRadioModel item, int secPrice, int getPrice);
    }

    @NonNull
    @Override
    public ViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inner_recycle,parent,false);
        mContext = parent.getContext();

        return new SectionItemsModelInnerRecycler.ViewModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewModel holder, int position) {
        if (secPrice==3) {
            holder.price.setVisibility(View.GONE);
        }else {
            holder.price.setVisibility(View.VISIBLE);
        }

        SectionItemsRadioModel sectionItemsRadioModel =  sectionItemsCheckModelsArrayListInnerName.get(position);
        holder.textViewName.setText(sectionItemsRadioModel.getName());
        String buyMRP = Money.rupees(
                BigDecimal.valueOf(Long.valueOf(sectionItemsRadioModel.getPrice()
                ))).toString();
        holder.price.setText(buyMRP);

        if (radioCheckBoolean){
            holder.radioButton.setVisibility(View.VISIBLE);
            holder.checkBox.setVisibility(View.GONE);

            holder.radioButton.setChecked(lastSelectedPosition == position);
            /*if (lastSelectedPositionUnselect != lastSelectedPosition){
                //Toast.makeText(mContext, " Size " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                if (lastSelectedPosition>-1 *//*&& lastSelectedPositionUnselect==0*//*){
                    if (lastSelectedPositionUnselect==-1){
                        onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(0).getPrice());
                        Toast.makeText(mContext, " Size -1 " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                    }else{
                        onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(lastSelectedPositionUnselect).getPrice());
                        Toast.makeText(mContext, " Above -1 Size  " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                    }
                    //onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                    //lastSelectedPositionUnselect = lastSelectedPosition;
                }else{
                    //onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                    //lastSelectedPositionUnselect = lastSelectedPosition;
                    //UImsgs.showToast(mContext,"Else Here");
                }
            }else {
                Toast.makeText(mContext, " Else " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
            }*/

            if (countRadio==0){
                countRadio++;
                holder.radioButton.setChecked(0 == position);
                onItemCheckListener.onItemCheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
            }

            if (holder.radioButton.isChecked()) {
                //Toast.makeText(mContext, "Checked "+holder.radioButton.isChecked(), Toast.LENGTH_SHORT).show();
                holder.radioButton.setClickable(false);
                holder.radioButton.setEnabled(false);
            }else {
                //UImsgs.showToast(mContext, "Un - Checked "+holder.radioButton.isChecked());
                holder.radioButton.setClickable(true);
                holder.radioButton.setEnabled(true);
            }

            holder.radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //lastSelectedPositionUnselect=lastSelectedPosition;
                    lastSelectedPosition = position;
                    notifyDataSetChanged();

                    //SectionItemsRadioModel sectionItemsRadioModel =  sectionItemsCheckModelsArrayListInnerName.get(position+1);
                    sectionItemsRadioSelect = sectionItemsRadioModel.getId();
                    //Toast.makeText(mContext, sectionItemsRadioSelect, Toast.LENGTH_SHORT).show();

                    onItemCheckListener.onItemCheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                    //Toast.makeText(mContext, " + " + String.valueOf((sectionItemsRadioModel)), Toast.LENGTH_SHORT).show();


                    //On Item Unchecked

                    if (lastSelectedPositionUnselect != lastSelectedPosition){
                        //Toast.makeText(mContext, " Size " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                        if (lastSelectedPosition>-1 /*&& lastSelectedPositionUnselect==0*/){
                            /*if (lastSelectedPositionUnselect==-1){
                                onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(0).getPrice());
                                Toast.makeText(mContext, " Size -1 " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                            }else{
                                onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(lastSelectedPositionUnselect).getPrice());
                                Toast.makeText(mContext, " Above -1 Size  " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                            }*/
                            onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsCheckModelsArrayListInnerName.get(lastSelectedPositionUnselect).getPrice());
                            lastSelectedPositionUnselect=lastSelectedPosition;
                            //lastSelectedPositionUnselect = lastSelectedPosition;
                        }else{
                            //onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                            //lastSelectedPositionUnselect = lastSelectedPosition;
                            //UImsgs.showToast(mContext,"Else Here");
                        }
                    }else {
                        //Toast.makeText(mContext, " Else " + lastSelectedPosition + "  "+lastSelectedPositionUnselect, Toast.LENGTH_SHORT).show();
                    }
                }
            });



        }else {
            holder.radioButton.setVisibility(View.GONE);
            holder.checkBox.setVisibility(View.VISIBLE);
            //Validations For Check Box

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*((ViewModel) holder).checkBox.setChecked(
                            !((ViewModel) holder).checkBox.isChecked());*/
                    if (((ViewModel) holder).checkBox.isChecked()) {
                        onItemCheckListener.onItemCheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                        //UImsgs.showToast(mContext,String.valueOf(sectionItemsRadioModel.getId()));
                    } else {
                        onItemCheckListener.onItemUncheck(sectionItemsRadioModel,secPrice,sectionItemsRadioModel.getPrice());
                        //UImsgs.showToast(mContext,String.valueOf(sectionItemsRadioModel.getId()));
                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return sectionItemsCheckModelsArrayListInnerName.size();
    }

    class ViewModel extends RecyclerView.ViewHolder{
        RadioButton radioButton;
        CheckBox checkBox;
        TextView textViewName ,price;
        public ViewModel(@NonNull View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.radio_select_inner);
            checkBox = itemView.findViewById(R.id.check_select_inner);
            textViewName = itemView.findViewById(R.id.title_name_inner);
            price = itemView.findViewById(R.id.description_price_inner);

           /* radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();

                    SectionItemsRadioModel sectionItemsRadioModel =  sectionItemsCheckModelsArrayListInnerName.get(getAdapterPosition()+1);
                    sectionItemsRadioSelect = sectionItemsRadioModel.getId();
                    //Toast.makeText(mContext, sectionItemsRadioSelect, Toast.LENGTH_SHORT).show();

                    onItemCheckListener.onItemCheck(sectionItemsRadioModel);
                    //Toast.makeText(mContext, " + " + String.valueOf((sectionItemsRadioModel)), Toast.LENGTH_SHORT).show();
                }
            });*/
        }
        //Check Box Click
        public void setOnClickListener(View.OnClickListener onClickListener) {
            checkBox.setOnClickListener(onClickListener);
        }
    }
}
