package com.apticks.nextclickuser.Constants;

public interface PreferenceManagerKey {
    String userName ="username";
    String wallet ="wallet";
    String uniqueId ="unique_id";
    String emailId ="email";
    String user_id ="user_id";
    String profilepic ="profilepic";
    String TOKEN_KEY = "token";
    String IS_VENDOR = "is_vendor";
}
