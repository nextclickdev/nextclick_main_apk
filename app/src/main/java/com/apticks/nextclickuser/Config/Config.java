package com.apticks.nextclickuser.Config;

import android.text.TextUtils;

public interface Config {

 public String MAP_BOX_ACCESS_TOKEN = "pk.eyJ1IjoiZ3JlcGdzcyIsImEiOiJjazU3dmJpbGIwNThyM2VxaHZzOHRzMTltIn0.f-dzgDCYpBDWuefLzd5hsA";

 //public String CLIENT_ID ="593397926428-7pvmspr9avn38et7582fhd3lnhi4kn32.apps.googleusercontent.com";
 //public String CLIENT_ID = "819536235959-eutadi0gkjnhgh9jt6m6nqov6mlsc4tb.apps.googleusercontent.com";
 public String CLIENT_ID = "80007084454-57206258cf4p8ljp5gupc0it882hrkms.apps.googleusercontent.com";
 //public String PLACES_API_KEY ="AIzaSyCsZMybiAjYhSLGEqP9HO5Rj9RG-NwJVjs";
 //public String PLACES_API_KEY ="AIzaSyAOdS8ksYR1ggYiX0p-zyUHg-mcGkEoctk";
 public String PLACES_API_KEY = "AIzaSyCALiS-pCxXQuxz2kuYuiAoUoL49dmiPjo";


 //public String BASE_URL="http://192.168.1.16/nextclick/";//local
 //public String BASE_URL="https://cineplant.com/next/";//testing


 //public String BASE_URL="http://cineplant.com/nextclick/";//production
 //public String BASE_URL="http://foodynest.com/nextclick/";//mainproduction
 //public String BASE_URL="http://nextclick.apticks.com/";//apticks production
 //public String BASE_URL="https://hsdtraders.com/nextclick/";//apticks production
 //public String BASE_URL="http://vendor.nextclick.in/";//Nextclick production
 //public String BASE_URL="http://apticks.com/nextclick_test/";//apticks_testing

 //public String BASE_URL = "http://apticks.in/nextclick/";//apticks_testing
 //public String BASE_URL = "http://vendor.nextclick.in/";
 // public String BASE_URL = "https://nextclick.in/vendor/";
 // public String BASE_URL = "https://nextclick.in/app/";

 //Live
 //public String BASE_URL = "https://nextclick.in/app/";

 //Test
 //  public String BASE_URL = "http://test.nextclick.in/";

 // Dev
  public String BASE_URL = "http://dev.nextclick.in/";


 public String LATITUDE = "&latitude=";
 public String LONGITUDE = "&longitude=";
 public String SUBCATID = "?sub_cat_id=";
 public String CATID = "&cat_id=";
 public String BRANDID = "?brand_id=";
 public String SEARCH = "?q=";
 public String MENUID = "&menu_id=";
 public String VENDORUSERID = "?vendor_user_id=";


 public String VENDOR_LIST = BASE_URL + "user/master/vendor_list/";
 // public String SUB_CAT_VENDOR_LIST= BASE_URL + "user/master/vendor_list/";

 public String States = BASE_URL + "general/api/master/states/";

 public String VendorCreation = BASE_URL + "executive/vendors/c";
 public String MainAppVendorRegistration = BASE_URL + "executive/vendor_self_reg/c";


 public String CreateUsers = BASE_URL + "/auth/api/auth/create_user";


 public String SEND_OTP = BASE_URL + "auth/api/auth/otp_gen";
 public String VERIFY_OTP = BASE_URL + "auth/api/auth/verify_otp";

 public String LOGIN = BASE_URL + "/auth/api/auth/login";
 public String FCM = BASE_URL + "general/api/fcm_notify/grant_fcm_permission";
 public String FORGOT_PASSWORD = BASE_URL + "/auth/api/auth/forgot_password";
 public String CATEGORY_LIST = BASE_URL + "general/api/master/categories/";
 public String FEATURED_BRANDS = BASE_URL + "general/api/master/featured_brands/";
 public String CATEGORY_BANNERS = BASE_URL + "general/api/master/category_banners/";
 public String NEWS = BASE_URL + "user/news/news/100/0/?cat_id=";
 public String NEWS_ITEM = BASE_URL + "user/news/news/1/0/?news_id=";
 public String LOCAL_NEWS_SINGLE_ITEM = BASE_URL + "user/news/local_news/";
 public String NEWS_VIEW_COUNT_MAKER = BASE_URL + "user/news/view_count/update";
 public String NEWS_CATEGORIES = BASE_URL + "user/news/news_categories/";
 public String LOCAL_NEWS_LOCATION_BASED = BASE_URL + "user/news/local_news/";
 public String INDIVIDUAL_VENDOR = BASE_URL + "user/master/vendor/";
 public String GET_USER_DETAIL = BASE_URL + "general/api/master/user_details";
 public String USER_PROFILE_R = BASE_URL + "user/master/profile/r";
 public String USER_PROFILE_U = BASE_URL + "user/master/profile/u";

 //Food Module
 public String FOOD_MENUS = BASE_URL + "user/food/food_menus/";
 public String FOOD_ITEMS = BASE_URL + "user/food/food_items/";
 public String FOOD_SECTIONS = BASE_URL + "user/food/FoodSections/";
 public String Food_Single_Section_Item = BASE_URL + "user/food/FoodSecItem/";
 public String OrderInDetail = BASE_URL + "user/food/Order/";//order_id needed
 public String Food_Single_Section_ItemS = BASE_URL + "user/food/FoodSecItems/";
 public String Food_Orders_History_Past_Upcoming_cancelled = BASE_URL + "user/food/FoodOrders/";
 public String Food_Orders_History_Past = BASE_URL + "user/food/FoodOrders/";
 public String Food_Orders_Vendor_Setting = BASE_URL + "user/food/FoodSettings/";
 public String Food_Promo = BASE_URL + "user/promos/Promos";
 public String Food_Check_Promo = BASE_URL + "user/promos/CheckPromo";
 public String OrderRating = BASE_URL + "user/food/OrderRating";
 public String SINGLE_ORDER_DETAILS = BASE_URL + "user/food/Order/";
 public String CANCEL_ORDER = BASE_URL + "user/food/order_cancel/";//order_id needed


 //Ecommerce

 public String ECOM_CATEGORIES = BASE_URL + "user/ecom/ecom_categories/";
 public String ECOM_ALL_SUB_CATEGORIES = BASE_URL + "user/ecom/ecom_sub_categories/";
 public String INDIVIDUAL_PRODUCT_ON_PRODUCTID = BASE_URL + "user/ecom/ecom_products/?product_id=";
 public String ECOM_PRODUCTS_BASED_ON_SUBCATEGORY = BASE_URL + "user/ecom/ecom_products?sub_cat_id=";
 public String ECOM_PRODUCTS_BASED_ON_SEARCH = BASE_URL + "user/ecom/ecom_products/?q=";
 public String ECOM_PRODUCT_CHECK_OUT = BASE_URL + "user/ecom/ecom_checkout";

 //Grocery
 public String GROCER_CATEGORIES = BASE_URL + "user/grocery/GroceryCategories/";
 public String GROCER_CATEGORIES_PRODUCTS = BASE_URL + "user/grocery/GroceryProducts?sub_cat_id=";
 public String GROCERY_INDIVIDUAL_PRODUCT_ON_PRODUCTID = BASE_URL + "user/grocery/GroceryProducts/?product_id=";
 public String GROCERY_PRODUCTS_BASED_ON_SUBCATEGORY = BASE_URL + "user/grocery/GroceryProducts?sub_cat_id=";
 public String GROCERY_CHECKOUT = BASE_URL + "user/grocery/GroceryCheckout";

 //Slider sAnd Advertisment
 public String Sliders_And_Advertisements = BASE_URL + "general/api/master/sliders";
 public String FoodCartAdd = BASE_URL + "user/food/FoodCart";//Add To Cart5
 public String FoodSingleItem = BASE_URL + "user/food/food_item/";//Add To Cart5
 public String FoodOrder = BASE_URL + "user/food/FoodOrder";//Add To Cart5


 //Lead To Generate
 public String LEAD_GENERATE_ARRAY = BASE_URL + "user/master/LeadGeneration/array";

 //HMS
 public String DOCTORS = BASE_URL + "hospital/api/hospital/doctor";

 //B&S
 public String BS_PACKAGES = BASE_URL + "beauty/api/beauty/pack/";
 public String BS_PACKAGE_ORDERING = BASE_URL + "beauty/api/beauty/order/c";


 //Reviews
 public String REVIEW_CREATION = BASE_URL + "general/api/master/ratings/c";
 public String REVIEW_RETRIEVAL = BASE_URL + "general/api/master/ratings/r";
 //  public String LEAD_GENERATE_ARRAY  = BASE_URL + "user/master/LeadGeneration/array";
 public String ECOM_CART_C = BASE_URL + "user/ecom/ecom_cart/c";
 public String ECOM_CART_U = BASE_URL + "user/ecom/ecom_cart/u";
 public String ECOM_CART_R = BASE_URL + "user/ecom/ecom_cart/r";
 public String ECOM_CART_D = BASE_URL + "user/ecom/ecom_cart/d";
 public String ECOM_CART_ADD = BASE_URL + "user/ecom/ecom_cart/add";

 //local news
 public String LOCAL_NEWS_C = BASE_URL + "user/news/local_news/c";
 public String LOCAL_NEWS_R = BASE_URL + "user/news/local_news/r";
 public String LOCAL_NEWS_U = BASE_URL + "user/news/local_news/u/";
 public String LOCAL_NEWS_D = BASE_URL + "user/news/local_news/d/";
 //public String LOCAL_NEWS_LOCATION_BASED = BASE_URL + "user/news/local_news?cat_id=";

 public int LOCAL_NEWS = 1;
 public int MAIN_NEWS = 2;


 String ADD_NEW_CART_ADDRESS = BASE_URL + "user/master/user_address/c";
 String GET_NEW_CART_ADDRESS = BASE_URL + "user/master/user_address/r";
 String DELETE_USER_ADDRESS = BASE_URL + "user/master/user_address/d";

 String GET_DAYS = BASE_URL + "general/api/master/days_in_a_week";

 //DOCTORS
 public String GET_DOCTORS = BASE_URL + "vendor/api/bookings/get_vendor_doctors/";//VendorId needed to get doctors
 //OnDemandServices
 public String GET_ONDEMAND_SERVICE_PERSONS = BASE_URL + "vendor/api/bookings/get_vendor_od_services/";//VendorId needed to get doctors

 public String BOOKING_APPOINTMENT_SERVICE = BASE_URL + "vendor/api/bookings/booking/c";

 public String GET_SERVICES = BASE_URL + "general/api/master/booking_services";


 //Gmail login
 String GMAIL_AUTHENTICATION = BASE_URL + "auth/api/auth/social_login/google";
 //Fb Login
 //String FB_AUTHENTICATION = BASE_URL + "auth/api/auth/social_login/google";


 public static final String paytmchecksum = BASE_URL + "payment/paytm/";
 //testing
 // public static String MID = "STQfkY57839774505407";
 public static String MID = "EGODha82055445071520";
 public static String MKEY = "v0#7NHgO1eXT7lXN";
 public static String WEBSITE = "WEBSTAGING";
 public static String INDUSTRY_TYPE_ID = "Retail";
 public static String CALLBACK_URL = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=";

 public String GENERATE_CHECKSUM = BASE_URL + "payment/api/payment/gen_checksum";
 public String VERIFY_CHECKSUM = BASE_URL + "payment/api/payment/verify_checksum";
 public String PAYMENT_SUCCESS = BASE_URL + "payment/api/payment/payment_success/paytm";
 public String PAYMENT_FAILURE = BASE_URL + "payment/api/payment/payment_failure/paytm";
 public String TRANSACTIONS = BASE_URL + "payment/api/payment/wallet/history";
 public String WALLETHISTORY = BASE_URL + "payment/api/payment/wallet";

 public String BOOKINGS_CHECK_OUT = BASE_URL + "vendor/api/bookings/booking/c";

 public String DOCTOR_BOOKINGS_HISTORY = BASE_URL + "vendor/api/bookings/booking/booking_history";
 public String VIEW_BOOKINGS_HISTORY_DETAILS = BASE_URL + "vendor/api/bookings/booking/booking_history/";


 public String USERPRODUCTS = BASE_URL + "user/ecom/products/";


 // cart crud
 public String CARTADD = BASE_URL + "user/ecom/cart/c";
 public String CARTUPDATE = BASE_URL + "user/ecom/cart/u";
 public String CARTDELETE = BASE_URL + "user/ecom/cart/d";
 public String CARTREAD = BASE_URL + "user/ecom/cart/r";
 public String CARTCHANGESTATUS = BASE_URL + "user/ecom/cart/change_status";

 public static final String URL_States = BASE_URL + "general/api/master/states/";

 //Shipping Address
 public String ADDRESSADD = BASE_URL + "user/ecom/shipping_address/c";
 public String ADDRESSUPDATE = BASE_URL + "user/ecom/shipping_address/u";
 public String ADDRESSDELETE = BASE_URL + "user/ecom/shipping_address/d/";
 public String ADDRESSREAD = BASE_URL + "user/ecom/shipping_address/r";

 //CheckOut
 public String DELIVERYMODES = BASE_URL + "user/ecom/delivery_modes";
 public String CHECKOUTPRODUCTS = BASE_URL + "user/ecom/checkout";
 public String PAYMENTSTATUS = BASE_URL + "payment/api/payment/payment_status";
 public String ORDERSEND = BASE_URL + "user/ecom/orders/c";
 public String ORDERDETAILS = BASE_URL + "vendor/api/ecom/ecom_orders/order_details";


 //Payment Modes
 public String PAYMENTMODES = BASE_URL + "user/ecom/payment_methods";

 //Order history
 public String ORDERHISTORY = BASE_URL + "user/ecom/orders/order_history";

 public String GET_DELIVERY_BOY_LOCATION = BASE_URL + "delivery/api/delivery/current_location/get";

 public String CUSTOMER_RATING = BASE_URL + "delivery/api/delivery/customer_rating";
 //Support

 public String SUPPORTCREATE = BASE_URL + "general/api/support/support_queries/c";
 public String REQUESTTYPE = BASE_URL + "user/ecom/request_type/r";
 public String SUPPORTLIST = BASE_URL + "general/api/support/support_queries/r";
 public String SUPPORTUODATE = BASE_URL + "general/api/support/support_queries/u";

 //Notification

 public String NOTIFICATIONS = BASE_URL + "general/api/fcm_notify/notifications/r";
}
